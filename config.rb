require 'compass/import-once/activate'
preferred_syntax = :scss
# output_style = :compact
output_style = :compact
line_comments = true

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "web/assets/css"
sass_dir = "app/Resources/assets/scss"
images_dir = "app/Resources/assets/images"
javascripts_dir = "web/assets/js"

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true
