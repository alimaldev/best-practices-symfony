'use strict';

var gulp = require('gulp');
var glob = require('glob');
var objectAssign = require('object-assign');
var config = require('./config');

/**
 * Dump data in JSON format
 * @param  data
 */
function dump(data) {
  console.log(JSON.stringify(data, null, ' '));
}

/**
 * Iterate Countries and build configs
 * To be used as a callback for glob
 *
 * @param  {Function} callback
 * @param  {Error}   err
 * @param  {Array}   files
 */
function iterateCountries(callback, err, files) {
  if (err) throw err;

  var regex = /config_([a-z]+).js/;
  var countries = [];
  var baseConfig = require('../' + config.paths.app.config + '/config');

  files.forEach(function(file) {
    var match = file.match(regex);
    if (match && match[1]) {
      countries.push({
        code: match[1],
        config: objectAssign(baseConfig, require('../' + file))
      });
    }
  });

  callback(null, countries);
}

/**
 * @return {Stream}
 */
gulp.task('build-config', function() {
  var pattern = config.paths.app.config + '/countries/*.json';

  glob(pattern, iterateCountries.bind(null, function(err, countries) {
    dump({countries: countries});
  }));

});
