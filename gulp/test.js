'use strict';

var gulp = require('gulp');
var Server = require('karma').Server;

gulp.task('test', function(done) {
  startTests(true, done);
});

gulp.task('autotest', function(done) {
  startTests(false , done);
});

function startTests(singleRun, done) {
  var server = new Server({
    configFile: __dirname + '/../karma.conf.js',
    singleRun: !!singleRun
  }, karmaCompleted);
  server.start();

  function karmaCompleted() {
    done();
  }
}
