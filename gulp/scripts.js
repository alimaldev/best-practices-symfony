'use strict';

var gulp = require('gulp');
var config = require('./config');
var rev = require('gulp-custom-rev');
var revision = process.env.REVISION || (new Date()).getTime();
var addsrc = require('gulp-add-src');
var plugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'gulp.*'],
  replaceString: /^gulp(-|\.)/,
  lazy: false
});

var scripts = config.paths.jsDeps.distFiles.concat([
  config.paths.app.js + '/app/**/*.module.js',
  config.paths.app.js + '/app/**/*.js',
  config.paths.app.js + '/linio/**/*.module.js',
  config.paths.app.js + '/linio/**/*.js',
  config.paths.app.js + '/components/**/*.js'
]);

gulp.task('dep-maps', function() {
  return gulp.src(config.paths.jsDeps.maps)
    .pipe(plugins.plumber())
    .pipe(gulp.dest(config.paths.dist.js));
});

gulp.task('scripts', ['jscs', 'dep-maps'], function() {
  return gulp.src(scripts)
    .pipe(plugins.newer(config.paths.tmp.js))
    .pipe(plugins.sourcemaps.init({
      loadMaps: true
    }))
    .pipe(plugins.concat('main.js'))
    .pipe(gulp.dest(config.paths.dist.js))

  // Uglify
  .pipe(plugins.uglify({
      outSourceMap: true,
      sourceRoot: config.paths.app.js,
      mangle: false
    }))
    .pipe(gulp.dest(config.paths.dist.js))

  // Write revision.
  .pipe(rev(revision))
    .pipe(gulp.dest(config.paths.dist.js))

  // Write sourcemaps.
  .pipe(plugins.size({
      title: 'scripts'
    }))
    .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(config.paths.dist.js))

  // Write revision manifest file.
  .pipe(rev.manifest())
    .pipe(gulp.dest(config.paths.dist.js))

  .pipe(addsrc(config.paths.jsDeps.maps))
    .pipe(gulp.dest(config.paths.dist.js));
});

gulp.task('dev:scripts', ['jscs', 'dep-maps'], function() {
  return gulp.src(scripts)
    .pipe(plugins.sourcemaps.init({
      loadMaps: true
    }))
    .pipe(rev(revision))
    .pipe(gulp.dest(config.paths.dist.js))
    .pipe(rev.manifest())
    .pipe(gulp.dest(config.paths.dist.js))
    .pipe(plugins.size({
      title: 'dev:scripts'
    }))

  // Write sourcemaps.
  .pipe(plugins.sourcemaps.write('.'))
    .pipe(gulp.dest(config.paths.dist.js))

  .pipe(addsrc(config.paths.jsDeps.maps))
    .pipe(gulp.dest(config.paths.dist.js));
});
