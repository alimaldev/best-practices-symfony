'use strict';

var gulp = require('gulp');
var config = require('./config');
var size = require('gulp-size');

gulp.task('fonts', function() {
  return gulp.src([config.paths.app.fonts + '/**'])
      .pipe(gulp.dest(config.paths.dist.fonts))
      .pipe(size({
        title: 'fonts'
      }));
});
