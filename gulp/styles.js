'use strict';

var gulp = require('gulp');
var config = require('./config');
var compass = require('gulp-compass');
var rev = require('gulp-custom-rev');
var revision = process.env.REVISION || (new Date()).getTime();
var pluginLoader = require('gulp-load-plugins')({
  lazy: true
});
var plumber = require('gulp-plumber');

gulp.task('styles', ['scsslint'], function() {
  var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
  ];

  return gulp.src([
      config.paths.app.scss + '/main.scss',
    ])
    .pipe(pluginLoader.newer(config.paths.tmp.css))
    .pipe(pluginLoader.sourcemaps.init())
    /* jshint camelcase: false */
    .pipe(compass({
      config_file: './config.rb',
      css: config.paths.dist.css + '/',
      sass: config.paths.app.scss + '/',
      image: config.paths.dist.images + '/',
      require: ['susy', 'breakpoint'],
      time: true,
      logging: true,
      sourcemap: true,
      bundle_exec: true
    }))
    .on('error', function(err) {
      // Would like to catch the error here
      console.log(err);
    })
    .pipe(pluginLoader.autoprefixer(AUTOPREFIXER_BROWSERS))
    /* jshint camelcase: true */
    .pipe(gulp.dest(config.paths.tmp.css))
    .pipe(pluginLoader.if('*.css', pluginLoader.cssnano()))
    .pipe(pluginLoader.size({
      title: 'styles'
    }))
    .pipe(rev(revision))
    .pipe(gulp.dest(config.paths.dist.css))
    .pipe(rev.manifest())
    .pipe(pluginLoader.sourcemaps.write(config.paths.dist.css))
    .pipe(gulp.dest(config.paths.dist.css));
});
