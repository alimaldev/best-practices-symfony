'use strict';

var gulp = require('gulp');
var config = require('./config');
var jscs = require('gulp-jscs');
var xmlFileReporter = require('gulp-jshint-xml-file-reporter');
var pluginLoader = require('gulp-load-plugins')({
  lazy: true
});
var scsslint = require('gulp-scss-lint');

// Paths to be linted
var pathsToLint = [
  config.paths.app.js + '/**/*.js',
  config.paths.gulp.tasks + '/*.js',
  'gulpfile.js'
];

/**
 * Linting code for human beings
 */
gulp.task('jscs', function() {
  return gulp.src(pathsToLint)
    .pipe(pluginLoader.newer('scripts', {extension: '.js'}))
    .pipe(jscs({configPath: '.jscsrc'}))
    .pipe(jscs.reporter())
});

gulp.task('scsslint', function() {
  gulp.src([config.paths.app.scss + '/*.scss', config.paths.app.scss + '/**/*.scss'])
    .pipe(pluginLoader.newer('styles', {
      extension: '.scss'
    }))
    .pipe(scsslint({
        'config': '.scsslint.yml',
        'bundleExec': true,
        'reporterOutputFormat': 'Checkstyle',
        'filePipeOutput': config.paths.ant.buildLogs + '/scss.Report.xml'
      })
      .on('error', function(err) {
        // Would like to catch the error here
        console.log(err);
      }));
});

/**
 * JSCSing files for jenkins
 */
gulp.task('jscs-ci', function() {
  return gulp.src(pathsToLint)
    .pipe(pluginLoader.newer('scripts', {extension: '.js'}))
    .pipe(jscs({configPath: '.jscsrc'}))
    .pipe(jscs.reporter('checkstyle'))
});
