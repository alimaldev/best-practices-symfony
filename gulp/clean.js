'use strict';

var gulp = require('gulp');
var config = require('./config');
var del = require('del');

gulp.task('clean-scripts', del.bind(null, [config.paths.dist.js]));

gulp.task('clean-images', del.bind(null, [config.paths.dist.images]));

gulp.task('clean-fonts', del.bind(null, [config.paths.dist.fonts]));

gulp.task('clean-styles', del.bind(null, [config.paths.dist.css]));

gulp.task('clean', [
  'clean-scripts', 'clean-styles', 'clean-images', 'clean-fonts'
]);
