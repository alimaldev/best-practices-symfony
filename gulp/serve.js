'use strict';

/* exported jshint */
var gulp = require('gulp');
var config = require('./config');
var browserSync = require('browser-sync');
var jshint = require('gulp-jshint');
var reload = browserSync.reload;

gulp.task('serve', ['styles'], function() {
  browserSync({
    notify: false,
    proxy: 'shop-front.dev'
  });

  gulp.watch([
    config.paths.app.scss + '/**/*.{scss,css}'
  ], ['styles', reload]);
  gulp.watch([config.paths.app.js + '/**/*.js'], ['jscs']);
});
