'use strict';

var gulp = require('gulp');
var config = require('./config');
var browserSync = require('browser-sync');

gulp.task('serve:dist', ['default'], function() {
  browserSync({
    notify: false,
    server: config.paths.dist
  });
});
