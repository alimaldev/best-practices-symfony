'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
  browserSync({
    proxy: 'http://shop-front.dev'
  });
});

gulp.task('watch', ['styles', 'dev:scripts', 'images', 'fonts'], function() {
  gulp.watch([
    'app/Resources/assets/scss/**/*.scss',
    'app/Resources/assets/scss/*.scss'
  ], ['styles']);
  gulp.watch([
    'app/Resources/assets/js/*.js',
    'app/Resources/assets/js/**/*.js'
  ], ['dev:scripts']);
  gulp.watch([
    'app/Resources/assets/images/*.png',
    'app/Resources/assets/images/**/*.png'
  ], ['images']);
});

gulp.task('watch:live', ['styles', 'scripts', 'images', 'fonts'], function() {
  gulp.watch([
    'app/Resources/assets/scss/**/*.scss',
    'app/Resources/assets/scss/*.scss'
  ], ['styles']);
  gulp.watch([
    'app/Resources/assets/js/*.js',
    'app/Resources/assets/js/**/*.js'
  ], ['scripts']);
  gulp.watch([
    'app/Resources/assets/images/*.png',
    'app/Resources/assets/images/**/*.png'
  ], ['images']);
});
