'use strict';

/**
 * @fileOverview Here we set up the configs importing from YAML
 *
 * We will iterate the current localizations
 *
 * We will import the translation messages for each l10n,
 *   Importing only the needed strings, pointed out in the 'javascript'
 *   key for on each translation messages file, using the current building
 *   localization
 *
 */

var gulp = require('gulp');
var YAML = require('yamljs');
var config = require('./config');

/**
 * Parses a
 * @param  {Object} yamlObj [description]
 * @return {[type]}         [description]
 */
function parseI18nMessages(yamlObj) {
  var parsedObj = {};
  yamlObj.javascript.forEach(
      function(item) {
        /* jshint evil: true */
        parsedObj[item] = eval('yamlObj.' + item);
        /* jshint evil: false */
      }
  );
  return parsedObj;
}

/**
 * Task to translate yaml to JS
 * @return {[type]} [description]
 */
gulp.task('config-import', function() {
  var yamlObj = YAML.load(
      config.paths.app.root + '/Resources/translations/messages.es.yml'
  );
  var parsedI18n = parseI18nMessages(yamlObj);

  console.log('\n', parsedI18n);
  /*jsonfile.writeFile(config.paths.tmp.js + '/messages.es.js',
   translate, function (err) {
   console.log(err);
   });*/
});
