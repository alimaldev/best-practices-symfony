'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function(cb) {
  // runSequence('styles', ['scripts', 'images', 'fonts'], cb);
  runSequence('clean', 'images', ['fonts', 'scripts', 'styles'], cb);
});
