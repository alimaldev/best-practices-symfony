'use strict';

var gulp = require('gulp');
var config = require('./config');
var pluginLoader = require('gulp-load-plugins')({
  lazy: true
});

gulp.task('images', function() {
  return gulp.src([config.paths.app.images + '/*', config.paths.app.images + '/**/*'])
    .pipe(pluginLoader.newer('images', {
      extension: ['.png', '.jpg', '.gif']
    }))
    // .pipe(pluginLoader.image())
    .pipe(gulp.dest(config.paths.dist.images));
});
