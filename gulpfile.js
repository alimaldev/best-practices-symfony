'use strict';

var config = require('./gulp/config');

// Load custom tasks from the `gulp` directory
require('require-dir')(config.paths.gulp.tasks);
