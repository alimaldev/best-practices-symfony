module.exports = function (config) {
  config.set({
    basePath: './',
    frameworks: ['mocha', 'chai', 'sinon', 'chai-sinon'],
    files: [
      './bower_components/angular/angular.js',
      './bower_components/angular-mocks/angular-mocks.js',
      './bower_components/angular-animate/angular-animate.js',
      './bower_components/angular-cookies/angular-cookies.min.js',
      './bower_components/ngMask/dist/ngMask.min.js',
      './bower_components/angular-animate/angular-animate.js',
      './bower_components/angular-ui-router/release/angular-ui-router.min.js',
      './bower_components/angular-local-storage/dist/angular-local-storage.min.js',
      './bower_components/angular-geohash/dist/angular-geohash.min.js',
      './bower_components/ngDialog/js/ngDialog.js',
      './bower_components/ouibounce/build/ouibounce.js',
      './bower_components/angular-credit-cards/release/angular-credit-cards.js',
      './node_modules/sinon/pkg/sinon.js',
      './node_modules/bardjs/dist/bard.js',
      './node_modules/bardjs/dist/bard-ngRouteTester.js',
      './app/Resources/assets/js/linio/*.module.js',
      './app/Resources/assets/js/linio/**/*.module.js',
      './app/Resources/assets/js/linio/**/*.js',
      './app/Resources/assets/js/test/lib/stubs.js',
      './app/Resources/assets/js/test/**/*.spec.js',
    ],
    proxies: {
      '/': 'http://localhost:8888/'
    },
    preprocessors: {
      'app/Resources/assets/js/linio/**/*.js': 'coverage'
    },
    reporters: ['progress', 'coverage'],
    coverageReporter: {
      type: 'html',
      dir: 'build/frontend-coverage'
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false
  });
};
