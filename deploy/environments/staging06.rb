set :new_relic_api_key, '2dfb320c93c5013cded743e021ece238f4176041c347db1'
set :new_relic_app_name, 'shop-front06'
set :controllers_to_clear, ['app.php', 'app_dev.php']
set :deploy_to, '/shop/shop-front/'
SSHKit.config.command_map[:composer] = "FRONTEND_COUNTRY=mx php #{shared_path.join("composer.phar")}"

server 'web04-06.linio-staging.com', user: 'www-data', roles: %w{app web}
