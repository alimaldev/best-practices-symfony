Rake::Task["symfony:cache:clear"].clear_actions
Rake::Task["symfony:cache:warmup"].clear_actions

namespace :symfony do
  namespace :cache do
    desc "Run app/console cache:clear for the #{fetch(:symfony_env)} environment"
    task :clear do
      on roles(:app) do
        fetch(:countries).each do |country|
          fetch(:default_env).merge!(FRONTEND_COUNTRY: country)
          symfony_console "cache:clear"
          fetch(:default_env).merge!(FRONTEND_COUNTRY: nil)
        end
      end
    end

    desc "Run app/console cache:warmup for the #{fetch(:symfony_env)} environment"
    task :warmup do
      on roles(:app) do
        fetch(:countries).each do |country|
          fetch(:default_env).merge!(FRONTEND_COUNTRY: country)
          symfony_console "cache:warmup"
          fetch(:default_env).merge!(FRONTEND_COUNTRY: nil)
        end
      end
    end
  end
end
