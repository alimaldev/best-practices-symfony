lock '>=3.2'

set :countries, [:ec, :ar]
set :format_options, log_file: 'app/logs/capistrano.log'
set :bin_path, 'app'
set :symfony_console_flags, '--no-debug --no-ansi'

set :linked_files, [
  'app/config/parameters.yml',
  'app/config/parameters.ar.yml',
  'app/config/parameters.cl.yml',
  'app/config/parameters.co.yml',
  'app/config/parameters.ec.yml',
  'app/config/parameters.mx.yml',
  'app/config/parameters.pa.yml',
  'app/config/parameters.pe.yml',
  'app/config/parameters.ve.yml',
]

set :copy_files, fetch(:copy_files) + ['node_modules', 'bower_components']

# Needed to execute composer.
set :default_env, {
  FRONTEND_COUNTRY: :mx,
  REVISION: Time.now.to_i
}

after 'deploy:updated', 'linio:npm:install'
before 'deploy:finished', 'linio:php:fpm:restart'
