Alice 2.0
=========

[![Build Status](https://scrutinizer-ci.com/g/LinioIT/shop-front/badges/build.png?b=master&s=48df4b8bef9063143c45c1d7a30b09d17eeb8a79)](https://scrutinizer-ci.com/g/LinioIT/shop-front/build-status/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/LinioIT/shop-front/badges/quality-score.png?b=master&s=1612ad76b0730d74a3ef5484b162c809e3a84566)](https://scrutinizer-ci.com/g/LinioIT/shop-front/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/LinioIT/shop-front/badges/coverage.png?b=master&s=fcb700fa395b4bfe9351fd9b1dbc890b01cc9c9f)](https://scrutinizer-ci.com/g/LinioIT/shop-front/?branch=master)

> A new front-end for Linio

Getting Started
---------------

Check [the wiki](https://github.com/LinioIT/shop-front/wiki) for installation instructions.

-----

```
$ npm install
$ bower install
$ composer install
$ gulp
$ php app/console assets:install --symlink
$ rm -r .git/hooks
$ ln -s $PWD/docs/hooks .git/hooks
```
