<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

class InstallmentOptionsTest extends \PHPUnit_Framework_TestCase
{
    public function testIsSelectingOption()
    {
        $option1 = new InstallmentOption();
        $option1->setInstallments(1);
        $option2 = new InstallmentOption();
        $option2->setInstallments(2);
        $option3 = new InstallmentOption();
        $option3->setInstallments(3);

        $options = new InstallmentOptions();
        $options->add($option1);
        $options->add($option2);
        $options->add($option3);
        $options->select(2);
        $this->assertEquals($option2, $options->getSelected());
    }

    /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid installment.
     */
    public function testIsNotSelectingInvalidOption()
    {
        $options = new InstallmentOptions();
        $options->select(1);
    }

    public function testIsGettingHighest()
    {
        $option1 = new InstallmentOption();
        $option1->setInstallments(1);
        $option2 = new InstallmentOption();
        $option2->setInstallments(2);
        $option3 = new InstallmentOption();
        $option3->setInstallments(3);

        $options = new InstallmentOptions();
        $options->add($option1);
        $options->add($option2);
        $options->add($option3);

        $this->assertEquals($option3, $options->getHighest());
    }

    public function testIsGettingLowest()
    {
        $option1 = new InstallmentOption();
        $option1->setInstallments(1);
        $option2 = new InstallmentOption();
        $option2->setInstallments(2);
        $option3 = new InstallmentOption();
        $option3->setInstallments(3);

        $options = new InstallmentOptions();
        $options->add($option1);
        $options->add($option2);
        $options->add($option3);

        $this->assertEquals($option1, $options->getLowest());
    }
}
