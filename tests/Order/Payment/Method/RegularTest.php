<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment\Method;

class RegularTest extends \PHPUnit_Framework_TestCase
{
    public function testIsCreatingRegularPaymentMethod()
    {
        $regular = new Regular('foobar');
        $this->assertEquals('foobar', $regular->getName());
        $this->assertTrue($regular->isVisible());
    }

    public function testIsAllowingPaymentMethod()
    {
        $regular = new Regular('foobar');
        $this->assertFalse($regular->isAllowed());
        $regular->allow();
        $this->assertTrue($regular->isAllowed());
    }

    public function testIsAllowingPaymentMethodByCoupon()
    {
        $regular = new Regular('foobar');
        $this->assertFalse($regular->isAllowedByCoupon());
        $regular->allowByCoupon();
        $this->assertTrue($regular->isAllowedByCoupon());
    }

    public function testIsPaymentMethodRequiringBillingAddress()
    {
        $regular = new Regular('foobar');
        $this->assertFalse($regular->isBillingAddressRequired());
        $regular->requireBillingAddress();
        $this->assertTrue($regular->isBillingAddressRequired());
    }
}
