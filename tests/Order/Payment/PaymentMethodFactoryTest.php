<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

use Linio\Frontend\Order\Payment\Method\ExternalCheckout;
use Linio\Frontend\Order\Payment\Method\Regular;

class PaymentMethodFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testIsCreatingRegularPaymentMethod()
    {
        $this->assertInstanceOf(Regular::class, PaymentMethodFactory::fromName('foobar'));
    }

    public function testIsCreatingCustomPaymentMethod()
    {
        $this->assertInstanceOf(ExternalCheckout::class, PaymentMethodFactory::fromName('PayClub_HostedPaymentPage'));
    }
}
