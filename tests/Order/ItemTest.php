<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;

class ItemTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingMainImage()
    {
        $image1 = new Image();
        $image1->setSlug('foobar');

        $image2 = new Image();
        $image2->setMain(true);
        $image2->setSlug('barfoo');

        $product = new Product();
        $product->addImage($image1);
        $product->addImage($image2);
        $item = new Item('123', 1, $product);

        $this->assertEquals('barfoo-cart.jpg', $item->getImage());
    }

    public function testIsGettingAnymage()
    {
        $image = new Image();
        $image->setSlug('foobar');

        $product = new Product();
        $product->addImage($image);
        $item = new Item('123', 1, $product);

        $this->assertEquals('foobar-cart.jpg', $item->getImage());
    }

    public function testIsReturningEmptyStringWhenThereAreNoImages()
    {
        $item = new Item('123', 1, new Product());
        $this->assertEquals('', $item->getImage());
    }
}
