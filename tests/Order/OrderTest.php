<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Type\Money;

class OrderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsDetectingTotalWalletUsage()
    {
        $wallet = new Wallet();
        $wallet->setTotalPointsUsed(new Money(1000));

        $order = new Order('123');
        $order->setGrandTotal(new Money(0));
        $order->setWallet($wallet);

        $this->assertTrue($order->isFullWalletPayment());
    }

    public function testIsNotDetectingTotalWalletUsage()
    {
        $wallet = new Wallet();
        $wallet->setTotalPointsUsed(new Money(1000));

        $order = new Order('123');
        $order->setGrandTotal(new Money(100));

        $this->assertFalse($order->isFullWalletPayment());

        $order->setWallet($wallet);
        $this->assertFalse($order->isFullWalletPayment());
    }
}
