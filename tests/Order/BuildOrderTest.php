<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Frontend\Customer\AddressBook;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Communication\BuildOrder\BuildOrderAdapter;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Storage\Storage;
use Linio\Test\UnitTestCase;

class BuildOrderTest extends UnitTestCase
{
    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../fixtures/order/orders.yml');
    }

    public function testIsAddingProduct()
    {
        $order = new Order('123');
        $recalculatedOrder = new RecalculatedOrder($order);
        $item = new Item('ABC');

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->addProduct($order, $item)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->addProduct($order, $item);
    }

    public function testIsRemovingProduct()
    {
        $order = new Order('123');
        $item = new Item('ABC');
        $order->addItem($item);
        $recalculatedOrder = new RecalculatedOrder($order);

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->removeProduct($order, $item)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->removeProduct($order, $item);
        $this->assertEmpty($order->getItems());
    }

    public function testIsUpdatingProduct()
    {
        $item = new Item('ABC', 12);
        $order = new Order('123');
        $order->addItem($item);
        $recalculatedOrder = new RecalculatedOrder($order);

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->updateQuantity($order, $item)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->updateQuantity($order, $item);

        $this->assertEquals(12, $order->getItems()->get('ABC')->getQuantity());
    }

    /**
     * @expectedException \Linio\Frontend\Order\Exception\OrderException
     * @expectedExceptionMessage order.product_not_found
     */
    public function testIsDetectingNonExistingProductDuringUpdate()
    {
        $order = new Order('123');
        $item = new Item('ABC');
        $recalculatedOrder = new RecalculatedOrder($order);

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->updateQuantity($order, $item)->shouldNotBeCalled();

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldNotBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->updateQuantity($order, $item);
    }

    public function testIsApplyingCoupon()
    {
        $order = new Order('123');
        $recalculatedOrder = new RecalculatedOrder($order);
        $coupon = new Coupon('VALID');

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->applyCoupon($order, $coupon)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->applyCoupon($order, $coupon);
        $this->assertEquals($coupon, $order->getCoupon());
    }

    public function testIsRemovingCoupon()
    {
        $coupon = new Coupon('VALID');
        $order = new Order('123');
        $order->applyCoupon($coupon);
        $recalculatedOrder = new RecalculatedOrder($order);

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->removeCoupon($order)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->removeCoupon($order);
        $this->assertEmpty($order->getCoupon());
    }

    public function testIsSettingShippingAddress()
    {
        $shippingAddress = new Address();
        $order = new Order('123');
        $recalculatedOrder = new RecalculatedOrder($order);

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->setShippingAddress($order, $shippingAddress)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setShippingAddress($order, $shippingAddress);
        $this->assertEquals($shippingAddress, $order->getShippingAddress());
    }

    public function testIsUsingWalletPoints()
    {
        $points = 10;
        $order = new Order('123');
        $order->setWallet(new Wallet());
        $recalculatedOrder = new RecalculatedOrder($order);

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->useWalletPoints($order, $points)->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->useWalletPoints($order, $points);
        $this->assertEquals($points, $order->getWallet()->getTotalPointsUsed()->getAmount());
    }

    public function testIsUpdatingShippingMethods()
    {
        /** @var Order $order */
        $order = $this->fixtures['order1'];

        /** @var RecalculatedOrder $recalculatedOrder */
        $recalculatedOrder = $this->fixtures['recalculated_order_with_express_shipping'];

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->updateShippingMethod($order, 1, 'express')
            ->shouldBeCalled()
            ->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($recalculatedOrder->getOrder())
            ->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());

        $actual = $buildOrder->updateShippingMethod($order, 1, 'express');

        $this->assertSame($recalculatedOrder, $actual);
        $this->assertFalse($recalculatedOrder->getOrder()->getPackages()->get(1)->getShippingQuotes()->get('regular')->isSelected());
        $this->assertTrue($recalculatedOrder->getOrder()->getPackages()->get(1)->getShippingQuotes()->get('express')->isSelected());
    }

    public function testIsSettingPaymentMethod()
    {
        /** @var Order $order */
        $order = $this->fixtures['order1'];

        /** @var RecalculatedOrder $recalculatedOrder */
        $recalculatedOrder = $this->fixtures['recalculated_order1'];

        $paymentMethod = $this->prophesize(PaymentMethod::class)->reveal();

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->updatePaymentMethod($order, $paymentMethod)
            ->shouldBeCalled()
            ->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)
            ->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());

        $actual = $buildOrder->updatePaymentMethod($order, $paymentMethod);

        $this->assertSame($paymentMethod, $order->getPaymentMethod());
        $this->assertSame($recalculatedOrder, $recalculatedOrder);
    }

    public function testIsSelectingInstallmentQuantity()
    {
        /** @var Order $order */
        $order = $this->fixtures['order1'];

        /** @var RecalculatedOrder $recalculatedOrder */
        $recalculatedOrder = $this->fixtures['recalculated_order1'];

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->selectInstallmentQuantity($order, 12)
            ->shouldBeCalled()
            ->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)
            ->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());

        $actual = $buildOrder->selectInstallmentQuantity($order, 12);

        $this->assertSame($recalculatedOrder, $actual);
        $this->assertEquals(12, $order->getSelectedInstallmentQuantity());
    }

    public function testIsSettingPickupStore()
    {
        /** @var Order $order */
        $order = $this->fixtures['order1'];

        /** @var RecalculatedOrder $recalculatedOrder */
        $recalculatedOrder = $this->fixtures['recalculated_order1'];

        /** @var PickupStore $pickupStore */
        $pickupStore = $this->fixtures['pickup_store1'];

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->setPickupStore($order, 1)
            ->shouldBeCalled()
            ->willReturn($recalculatedOrder);

        $storage = $this->prophesize(Storage::class);
        $storage->store($order)
            ->shouldBeCalled();

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());

        $actual = $buildOrder->setPickupStore($order, 1);

        $this->assertSame($recalculatedOrder, $actual);
        $this->assertEquals($pickupStore->getId(), $order->getPickupStores()->getSelected()->getId());
    }

    public function testIsRecalculatingOrder()
    {
        /** @var Order $order */
        $order = $this->fixtures['order1'];

        /** @var RecalculatedOrder $recalculatedOrder */
        $recalculatedOrder = $this->fixtures['recalculated_order1'];

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->recalculateOrder($order)
            ->shouldBeCalled()
            ->willReturn($recalculatedOrder);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($this->fixtures['customer1'])->willReturn([$this->fixtures['address1'], $this->fixtures['address2']]);

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->recalculateOrder($order);

        $this->assertSame($recalculatedOrder, $actual);
    }

    public function testIsGettingOrderInProgress()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order1'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([$this->fixtures['address1'], $this->fixtures['address2']]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->getOrderInProgress($customer);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
        $this->assertSame($this->fixtures['address2'], $actual->getShippingAddress());
    }

    public function testIsGettingOrderInProgressAndNotUpdatingAddresses()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order1'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([$this->fixtures['address1'], $this->fixtures['address2']]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->getOrderInProgress($customer, false);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
        $this->assertNull($actual->getShippingAddress());
    }

    public function testIsGettingOrderInProgressAndOverridingPartialShippingAddress()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order_with_partial_shipping_address'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([$this->fixtures['address1'], $this->fixtures['address2']]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->getOrderInProgress($customer);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
        $this->assertSame($this->fixtures['address2'], $actual->getShippingAddress());
    }

    public function testIsGettingOrderInProgressAndNotOverridingDefaultShippingAddress()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order_with_shipping_address'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([$this->fixtures['address1'], $this->fixtures['address2'], $this->fixtures['address3']]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->getOrderInProgress($customer);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
        $this->assertSame($this->fixtures['address3'], $actual->getShippingAddress());
    }

    public function testIsGetOrderInProgressGeneratingANewOneWhenNotExistent()
    {
        $customer = $this->fixtures['customer1'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willThrow(new OrderException('foo'));

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->getOrderInProgress($customer);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
    }

    public function testIsMergeItemsIgnoringNotExistentOrder()
    {
        $customer = $this->fixtures['customer1'];
        $anonymousLinioId = 'anonymous123';

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($anonymousLinioId)
            ->shouldBeCalled()
            ->willThrow(new OrderException('foo'));

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());

        $buildOrder->mergeItems($anonymousLinioId, $customer);
    }

    public function testIsMergingItems()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Address $address1 */
        $address1 = $this->fixtures['address1'];

        /** @var Address $address2 */
        $address2 = $this->fixtures['address2'];

        /** @var Order $order */
        $order = $this->fixtures['order1'];

        /** @var Order $anonymousOrder */
        $anonymousOrder = $this->fixtures['anonymous_order1'];

        $anonymousLinioId = 'anonymous123';

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($anonymousLinioId)
            ->shouldBeCalled()
            ->willReturn($anonymousOrder);

        $storage->clear($anonymousLinioId)
            ->shouldBeCalled();

        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $storage->store($order)
            ->shouldBeCalled();

        $adapter = $this->prophesize(BuildOrderAdapter::class);
        $adapter->recalculateOrder($order)
            ->willReturn(new RecalculatedOrder($order));

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)
            ->shouldBeCalled()
            ->willReturn([$address1, $address2]);

        $buildOrder = new BuildOrder();
        $buildOrder->setAdapter($adapter->reveal());
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $buildOrder->mergeItems($anonymousLinioId, $customer);

        $this->assertTrue($order->getItems()->hasItem('ORDER1_ITEM1'));
        $this->assertEquals(1, $order->getItems()->get('ORDER1_ITEM1')->getQuantity());

        $this->assertTrue($order->getItems()->hasItem('ORDER1_ITEM2'));
        $this->assertEquals(3, $order->getItems()->get('ORDER1_ITEM2')->getQuantity());

        $this->assertTrue($order->getItems()->hasItem('SHARED_ITEM1'));
        $this->assertEquals(2, $order->getItems()->get('SHARED_ITEM1')->getQuantity());

        $this->assertTrue($order->getItems()->hasItem('SHARED_ITEM2'));
        $this->assertEquals(1, $order->getItems()->get('SHARED_ITEM2')->getQuantity());
    }

    public function testIsMergingOrderWithCache()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order_with_shipping_address'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $partialOrder = [
            'pickupStoreId' => 123,
            'shippingAddressId' => 99,
            'paymentMethod' => 'CreditCard',
            'creditCardBinNumber' => '1234',
            'selectedShippingOptions' => [
                1 => 'express',
            ],
            'installments' => 3,
            'walletPoints' => 10000,
        ];

        $actual = $buildOrder->mergeOrderWithCache($customer, $partialOrder);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
        $this->assertSame($partialOrder['pickupStoreId'], $actual->getPickupStores()->getSelected()->getId());
        $this->assertSame($partialOrder['shippingAddressId'], $actual->getShippingAddress()->getId());
        $this->assertSame($partialOrder['paymentMethod'], $actual->getPaymentMethod()->getName());
        $this->assertEquals($partialOrder['creditCardBinNumber'], $actual->getPaymentMethod()->getBinNumber());
        $this->assertEquals($partialOrder['installments'], $actual->getSelectedInstallmentQuantity());
        $this->assertTrue($actual->getPackages()->get(1)->getShippingQuotes()->get('express')->isSelected());
        $this->assertEquals($partialOrder['walletPoints'], $actual->getWallet()->getTotalPointsUsed()->getAmount());
    }

    public function testIsMergingOrderWithCacheAndIgnoringInvalidInput()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order_with_shipping_address'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $partialOrder = [
            'pickupStoreId' => null,
            'shippingAddressId' => null,
            'paymentMethod' => null,
            'creditCardBinNumber' => null,
            'selectedShippingOptions' => null,
            'installments' => null,
        ];

        $actual = $buildOrder->mergeOrderWithCache($customer, $partialOrder);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($customer, $actual->getCustomer());
        $this->assertEquals(42, $actual->getShippingAddress()->getId());
        $this->assertNull($actual->getPaymentMethod());
        $this->assertCount(1, $actual->getPackages());
    }

    public function testIsReturningOrderFromCacheWhenPartialOrderIsEmpty()
    {
        /** @var Customer $customer */
        $customer = $this->fixtures['customer1'];

        /** @var Order $order */
        $order = $this->fixtures['order_with_shipping_address'];

        $storage = $this->prophesize(Storage::class);
        $storage->retrieve($customer->getLinioId())
            ->shouldBeCalled()
            ->willReturn($order);

        $addressBook = $this->prophesize(AddressBook::class);
        $addressBook->getAddresses($customer)->willReturn([]);

        $buildOrder = new BuildOrder();
        $buildOrder->setStorage($storage->reveal());
        $buildOrder->setAddressBook($addressBook->reveal());

        $actual = $buildOrder->mergeOrderWithCache($customer, null);

        $this->assertEquals($customer->getLinioId(), $actual->getId());
        $this->assertSame($order, $actual);
    }
}
