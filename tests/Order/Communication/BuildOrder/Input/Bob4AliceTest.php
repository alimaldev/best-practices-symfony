<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder\Input;

use Carbon\Carbon;
use DateTime;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Partnership;
use Linio\Frontend\Order\Payment\InstallmentOption;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\Method\ExternalCheckout;
use Linio\Frontend\Order\Payment\Method\ZeroPayment;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\PickupStores;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Linio\Frontend\Order\Wallet;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductService;
use Linio\Type\Money;
use PHPUnit_Framework_TestCase;

class Bob4AliceTest extends PHPUnit_Framework_TestCase
{
    /**
     * This is the minimum we can receive as a response.
     *
     * @var array
     */
    protected $minimalResponseBody = [
        'customer' => [
            'id' => 1,
            'linioPlus' => false,
        ],
        'store' => [
            'id' => 1,
        ],
        'coupon' => [
            'code' => null,
            'discount' => null,
            'available' => [],
        ],
        'loyalty' => [
            'programName' => null,
            'registration' => null,
            'points' => null,
        ],
        'partnership' => [
            'available' => [
            ],
            'discount' => 0,
        ],
        'payment' => [
            'paymentMethod' => [
                'name' => null,
                'available' => [],
                'allowed' => [],
                'billingAddressRequired' => [],
                'allowedByCoupon' => [],
                'resolved' => null,
            ],
            'allowedCreditCards' => [],
            'creditCardBinNumber' => null,
            'installments' => [],
        ],
        'wallet' => [
            'isActive' => false,
            'totalDiscount' => null,
            'totalPointsUsed' => null,
            'pointsBalance' => null,
            'shippingDiscount' => null,
            'pointsUsedShipping' => null,
            'maxPointsForOrder' => null,
            'conversionRate' => null,
        ],
        'tax' => [
            'amount' => null,
        ],
        'shipping' => [
            'postcode' => '33020',
            'amount' => null,
            'originalAmount' => null,
            'linioPlusSavings' => 0,
            'discount' => 0,
            'packages' => [],
            'allowStorePickup' => false,
            'pickupStores' => [],
        ],
        'subtotal' => 17752,
        'grandTotal' => 37752,
        'grandTotalWithoutInterest' => null,
        'isFastLane' => false,
        'items' => [
            '4P479PE04DZI0LAEC-17814' => [
                'name' => 'Config_1',
                'quantity' => 2,
                'availableStock' => 5,
                'maxItemsToSell' => 5,
                'price' => [
                    'unitPrice' => 9876,
                    'originalPrice' => 12345,
                    'paidPrice' => 17752,
                    'totalDiscount' => 2000,
                ],
                'cartRule' => [
                    'discount' => null,
                ],
                'coupon' => [
                    'discount' => null,
                ],
                'linioCreditCard' => [
                    'discount' => null,
                ],
                'wallet' => [
                    'discount' => null,
                    'pointsUsed' => null,
                ],
                'shipping' => [
                    'amount' => null,
                    'discount' => null,
                    'minimumDeliveryDate' => null,
                ],
                'tax' => [
                    'amount' => 2834,
                    'percent' => 1900,
                ],
                'linioPlus' => [
                    'level' => 0,
                    'quantity' => 0,
                ],
            ],
        ],
        'messages' => [
            'error' => [],
            'warning' => [],
            'success' => [],
        ],
    ];

    public function testIsRecalculatingTheCartAndKeepingTheGeohash()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setGeohash('geohash');

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertSame($order->getGeohash(), $newOrder->getGeohash());
        $this->assertSame('geohash', $newOrder->getGeohash());
    }

    public function testIsRecalculatingTheCartAndNotKeepingAGeohashThatIsntSet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertSame($order->getGeohash(), $newOrder->getGeohash());
        $this->assertNull($newOrder->getGeohash());
    }

    public function testIsRecalculatingTheCartAndKeepingTheShippingAddress()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $shippingAddress = new Address();

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setShippingAddress($shippingAddress);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertSame($order->getShippingAddress(), $newOrder->getShippingAddress());
        $this->assertSame($shippingAddress, $newOrder->getShippingAddress());
    }

    public function testIsRecalculatingTheCartAndNotKeepingAShippingAddressThatIsntSet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertSame($order->getShippingAddress(), $newOrder->getShippingAddress());
        $this->assertNull($newOrder->getShippingAddress());
    }

    public function testIsRecalculatingTheCartAndMappingTheCouponCode()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $coupon = new Coupon('code');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->applyCoupon($coupon);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['coupon']['code'] = 'code';
        $responseBody['coupon']['discount'] = 100;

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertSame($coupon->getCode(), $newOrder->getCoupon()->getCode());
        $this->assertEquals(Money::fromCents(100), $newOrder->getCoupon()->getDiscount());
    }

    public function testIsRecalculatingTheCartAndNotMappingACouponCodeThatIsntSet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertSame($order->getCoupon(), $newOrder->getCoupon());
        $this->assertNull($newOrder->getCoupon());
    }

    public function testIsRecalculatingTheCartAndMappingTheAvailablePaymentMethods()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->applyCoupon(new Coupon('code'));

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['available'] = [
            'CreditCard',
            'Zero_Payment',
            'PayClub_HostedPaymentPage',
        ];
        $responseBody['payment']['paymentMethod']['allowed'] = [
            'CreditCard',
            'PayClub_HostedPaymentPage',
        ];
        $responseBody['payment']['paymentMethod']['allowedByCoupon'] = [
            'CreditCard',
        ];
        $responseBody['payment']['paymentMethod']['billingAddressRequired'] = [
            'CreditCard',
            'PayClub_HostedPaymentPage',
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);

        $this->assertInstanceOf(CreditCard::class, $newOrder->getAvailablePaymentMethods()[0]);
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[0]->isAllowed());
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[0]->isAllowedByCoupon());
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[0]->isBillingAddressRequired());

        $this->assertInstanceOf(ZeroPayment::class, $newOrder->getAvailablePaymentMethods()[1]);
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[1]->isAllowed());
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[1]->isAllowedByCoupon());
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[1]->isBillingAddressRequired());

        $this->assertInstanceOf(ExternalCheckout::class, $newOrder->getAvailablePaymentMethods()[2]);
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[2]->isAllowed());
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[2]->isAllowedByCoupon());
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[2]->isBillingAddressRequired());
    }

    public function testIsRecalculatingTheCartAndNotMappingAnEmptySetOfAvailablePaymentMethods()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEmpty($newOrder->getAvailablePaymentMethods());
    }

    public function testIsRecalculatingTheCartAndMappingTheSelectedPaymentMethod()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'Zero_Payment';

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertInstanceOf(ZeroPayment::class, $newOrder->getPaymentMethod());
    }

    public function testIsMappingTheBinNumberIfProvidedForCreditCardOnCartRecalculation()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'CreditCard';
        $responseBody['payment']['creditCardBinNumber'] = '1234';

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertInstanceOf(CreditCard::class, $newOrder->getPaymentMethod());
        $this->assertSame('1234', $newOrder->getPaymentMethod()->getBinNumber());
    }

    public function testIsIgnoringTheBinNumberIfProvidedForCreditCardOnCartRecalculation()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'CreditCard';

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertInstanceOf(CreditCard::class, $newOrder->getPaymentMethod());
        $this->assertNull($newOrder->getPaymentMethod()->getBinNumber());
    }

    public function testIsRecalculatingTheCartAndNotMappingASelectedPaymentMethodWhenOneHasntBeenSelected()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEmpty($newOrder->getPaymentMethod());
    }

    public function testIsRecalculatingTheCartAndMappingInstallmentOptions()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['installments'] = [
            1 => [
                'interestFee' => 100,
                'grandTotal' => 200,
                'amount' => 100,
                'totalInterest' => 100,
                'selected' => false,
            ],
            3 => [
                'paymentMethodName' => 'CreditCard',
                'interestFee' => 100,
                'grandTotal' => 900,
                'amount' => 300,
                'totalInterest' => 100,
                'selected' => true,
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);

        /** @var InstallmentOption $installmentOption1 */
        $installmentOption1 = $newOrder->getInstallmentOptions()->get(1);
        $this->assertSame(1, $installmentOption1->getInstallments());
        $this->assertEquals(Money::fromCents(100), $installmentOption1->getInterestFee());
        $this->assertEquals(Money::fromCents(200), $installmentOption1->getTotal());
        $this->assertEquals(Money::fromCents(100), $installmentOption1->getAmount());
        $this->assertEquals(Money::fromCents(100), $installmentOption1->getTotalInterest());
        $this->assertFalse($installmentOption1->isSelected());

        /** @var InstallmentOption $installmentOption3 */
        $installmentOption3 = $newOrder->getInstallmentOptions()->get(3);
        $this->assertSame(3, $installmentOption3->getInstallments());
        $this->assertEquals(Money::fromCents(100), $installmentOption3->getInterestFee());
        $this->assertEquals(Money::fromCents(900), $installmentOption3->getTotal());
        $this->assertEquals(Money::fromCents(300), $installmentOption3->getAmount());
        $this->assertEquals(Money::fromCents(100), $installmentOption3->getTotalInterest());
        $this->assertSame('CreditCard', $installmentOption3->getPaymentMethodName());
        $this->assertTrue($installmentOption3->isSelected());
    }

    public function testIsRecalculatingTheCartAndContainsNoInstallmentOptionsIfNoneAreProvided()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEmpty($newOrder->getInstallmentOptions());
    }

    public function testIsRecalculatingTheCartAndIsMappingTheShippingAmount()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['amount'] = 50;
        $responseBody['shipping']['originalAmount'] = 100;
        $responseBody['shipping']['discount'] = 50;

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(Money::fromCents(50), $newOrder->getShippingAmount());
        $this->assertEquals(Money::fromCents(100), $newOrder->getOriginalShippingAmount());
        $this->assertEquals(Money::fromCents(50), $newOrder->getShippingDiscountAmount());
    }

    public function testIsRecalculatingTheCartAndIsNotMappingTheShippingAmountIfItDoesNotExist()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertNull($newOrder->getShippingAmount());
        $this->assertNull($newOrder->getOriginalShippingAmount());
        $this->assertEquals(Money::fromCents(0), $newOrder->getShippingDiscountAmount());
    }

    public function testIsRecalculatingTheCartAndIsMappingTheItems()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product1 = new Product();
        $product1->setSku('4P479PE04DZI0LAEC-17814');
        $product1->setName('Item 1 From Cache');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product1);
        $productService->getBySku('SKUNOTINCACHE')->willThrow(ProductNotFoundException::class);
        $productService->getBySku('SKUWITHOUTMINIMUMDELIVERYDATE')->willThrow(ProductNotFoundException::class);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['items'] = [
            '4P479PE04DZI0LAEC-17814' => [
                'sku' => '4P479PE04DZI0LAEC-17814',
                'name' => 'Item 1',
                'quantity' => 1,
                'availableStock' => 2,
                'maxItemsToSell' => 2,
                'price' => [
                    'unitPrice' => 100,
                    'originalPrice' => 100,
                    'paidPrice' => 100,
                    'totalDiscount' => 0,
                ],
                'cartRule' => [
                    'discount' => 100,
                ],
                'coupon' => [
                    'discount' => 100,
                ],
                'linioCreditCard' => [
                    'discount' => 100,
                ],
                'wallet' => [
                    'discount' => 100,
                    'pointsUsed' => 100,
                ],
                'shipping' => [
                    'amount' => 100,
                    'discount' => 0,
                    'minimumDeliveryDate' => '2016-05-02',
                ],
                'tax' => [
                    'amount' => 100,
                    'percent' => 10,
                ],
                'linioPlus' => [
                    'level' => 1,
                    'quantity' => 1,
                ],
            ],
            'SKUNOTINCACHE' => [
                'sku' => 'SKUNOTINCACHE',
                'name' => 'Item 2 Not in Cache',
                'quantity' => 1,
                'availableStock' => 2,
                'maxItemsToSell' => 2,
                'price' => [
                    'unitPrice' => 100,
                    'originalPrice' => 100,
                    'paidPrice' => 100,
                    'totalDiscount' => 0,
                ],
                'cartRule' => [
                    'discount' => 100,
                ],
                'coupon' => [
                    'discount' => 100,
                ],
                'linioCreditCard' => [
                    'discount' => 100,
                ],
                'wallet' => [
                    'discount' => 100,
                    'pointsUsed' => 100,
                ],
                'shipping' => [
                    'amount' => 100,
                    'discount' => 0,
                    'minimumDeliveryDate' => '2016-05-02',
                ],
                'tax' => [
                    'amount' => 100,
                    'percent' => 10,
                ],
                'linioPlus' => [
                    'level' => 1,
                    'quantity' => 1,
                ],
            ],
            'SKUWITHOUTMINIMUMDELIVERYDATE' => [
                'sku' => 'SKUWITHOUTMINIMUMDELIVERYDATE',
                'name' => 'Item 3',
                'quantity' => 1,
                'availableStock' => 2,
                'maxItemsToSell' => 2,
                'price' => [
                    'unitPrice' => 100,
                    'originalPrice' => 100,
                    'paidPrice' => 100,
                    'totalDiscount' => 0,
                ],
                'cartRule' => [
                    'discount' => 100,
                ],
                'coupon' => [
                    'discount' => 100,
                ],
                'linioCreditCard' => [
                    'discount' => 100,
                ],
                'wallet' => [
                    'discount' => 100,
                    'pointsUsed' => 100,
                ],
                'shipping' => [
                    'amount' => 100,
                    'discount' => 0,
                    'minimumDeliveryDate' => null,
                ],
                'tax' => [
                    'amount' => 100,
                    'percent' => 10,
                ],
                'linioPlus' => [
                    'level' => 1,
                    'quantity' => 1,
                ],
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);

        /** @var Item $item1 */
        $item1 = $newOrder->getItems()->get('4P479PE04DZI0LAEC-17814');
        $item1Data = $responseBody['items'][$item1->getSku()];

        $this->assertSame('4P479PE04DZI0LAEC-17814', $item1->getSku());
        $this->assertSame($item1Data['quantity'], $item1->getQuantity());
        $this->assertSame($item1Data['availableStock'], $item1->getAvailableQuantity());
        $this->assertSame($item1Data['maxItemsToSell'], $item1->getMaxItemsToSell());
        $this->assertEquals(Money::fromCents($item1Data['price']['originalPrice']), $item1->getOriginalPrice());
        $this->assertEquals(Money::fromCents($item1Data['price']['paidPrice']), $item1->getPaidPrice());
        $this->assertEquals(Money::fromCents($item1Data['price']['unitPrice']), $item1->getUnitPrice());
        $this->assertEquals(Money::fromCents($item1Data['tax']['amount']), $item1->getTaxAmount());
        $this->assertEquals(Money::fromCents($item1Data['shipping']['amount']), $item1->getShippingAmount());
        $this->assertSame($item1Data['linioPlus']['quantity'], $item1->getLinioPlusEnabledQuantity());
        $this->assertSame($item1Data['linioPlus']['level'], $item1->getLinioPlusLevel());
        $this->assertSame('Item 1 From Cache', $item1->getProduct()->getName());
        $this->assertEquals(new Datetime($item1Data['shipping']['minimumDeliveryDate']), $item1->getMinimumDeliveryDate());
        $this->assertSame($product1, $item1->getProduct());

        /** @var Item $item2 */
        $item2 = $newOrder->getItems()->get('SKUNOTINCACHE');
        $item2Data = $responseBody['items'][$item2->getSku()];

        $this->assertSame('SKUNOTINCACHE', $item2->getSku());
        $this->assertSame($item2Data['quantity'], $item2->getQuantity());
        $this->assertSame($item2Data['availableStock'], $item2->getAvailableQuantity());
        $this->assertSame($item2Data['maxItemsToSell'], $item2->getMaxItemsToSell());
        $this->assertEquals(Money::fromCents($item2Data['price']['originalPrice']), $item2->getOriginalPrice());
        $this->assertEquals(Money::fromCents($item2Data['price']['paidPrice']), $item2->getPaidPrice());
        $this->assertEquals(Money::fromCents($item2Data['price']['unitPrice']), $item2->getUnitPrice());
        $this->assertEquals(Money::fromCents($item2Data['tax']['amount']), $item2->getTaxAmount());
        $this->assertEquals(Money::fromCents($item2Data['shipping']['amount']), $item2->getShippingAmount());
        $this->assertSame($item2Data['linioPlus']['quantity'], $item2->getLinioPlusEnabledQuantity());
        $this->assertSame($item2Data['linioPlus']['level'], $item2->getLinioPlusLevel());
        $this->assertSame('Item 2 Not in Cache', $item2->getProduct()->getName());
        $this->assertEquals(new Datetime($item2Data['shipping']['minimumDeliveryDate']), $item2->getMinimumDeliveryDate());

        /** @var Item $item3 */
        $item3 = $newOrder->getItems()->get('SKUWITHOUTMINIMUMDELIVERYDATE');
        $item3Data = $responseBody['items'][$item3->getSku()];

        $this->assertSame('SKUWITHOUTMINIMUMDELIVERYDATE', $item3->getSku());
        $this->assertSame($item3Data['quantity'], $item3->getQuantity());
        $this->assertSame($item3Data['availableStock'], $item3->getAvailableQuantity());
        $this->assertSame($item3Data['maxItemsToSell'], $item3->getMaxItemsToSell());
        $this->assertEquals(Money::fromCents($item3Data['price']['originalPrice']), $item3->getOriginalPrice());
        $this->assertEquals(Money::fromCents($item3Data['price']['paidPrice']), $item3->getPaidPrice());
        $this->assertEquals(Money::fromCents($item3Data['price']['unitPrice']), $item3->getUnitPrice());
        $this->assertEquals(Money::fromCents($item3Data['tax']['amount']), $item3->getTaxAmount());
        $this->assertEquals(Money::fromCents($item3Data['shipping']['amount']), $item3->getShippingAmount());
        $this->assertSame($item3Data['linioPlus']['quantity'], $item3->getLinioPlusEnabledQuantity());
        $this->assertSame($item3Data['linioPlus']['level'], $item3->getLinioPlusLevel());
        $this->assertSame('Item 3', $item3->getProduct()->getName());
        $this->assertNull($item3->getMinimumDeliveryDate());
    }

    public function testIsRecalculatingTheCartAndMappingPackages()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['packages'] = [
            [
                'id' => 0,
                'fulfillmentType' => 'warehouse',
                'items' => [
                    '4P479PE04DZI0LAEC-17814' => 3,
                ],
                'quotes' => [
                    [
                        'shippingMethod' => 'express',
                        'fee' => 100,
                        'estimatedDeliveryDate' => '2016-06-02',
                        'pickupStoreId' => null,
                        'selected' => false,
                    ],
                    [
                        'shippingMethod' => 'regular',
                        'fee' => 100,
                        'estimatedDeliveryDate' => '2016-06-02',
                        'pickupStoreId' => null,
                        'selected' => true,
                    ],
                    [
                        'shippingMethod' => 'store',
                        'fee' => 100,
                        'estimatedDeliveryDate' => '2016-06-02',
                        'pickupStoreId' => 1,
                        'selected' => false,
                    ],
                ],
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertCount(1, $newOrder->getPackages());
        $this->assertCount(3, $newOrder->getPackages()->get(0)->getShippingQuotes());

        // This is the easiest way to make sure that we've cloned the item
        // Because we change the quantity, we can't do assertEquals
        /** @var Package $package1 */
        $package1 = $newOrder->getPackages()->get(0);
        $this->assertSame(
            $newOrder->getItems()->get('4P479PE04DZI0LAEC-17814')->getProduct(),
            $package1->getItems()->get('4P479PE04DZI0LAEC-17814')->getProduct()
        );

        /** @var ShippingQuote $quote1 */
        $quote1 = $package1->getShippingQuotes()->get($responseBody['shipping']['packages'][0]['quotes'][0]['shippingMethod']);
        $quoteData1 = $responseBody['shipping']['packages'][0]['quotes'][0];
        $this->assertEquals(
            new Carbon($quoteData1['estimatedDeliveryDate']),
            $quote1->getEstimatedDeliveryDate()
        );
        $this->assertEquals(Money::fromCents($quoteData1['fee']), $quote1->getFee());
        $this->assertFalse($quote1->isSelected());

        /** @var ShippingQuote $quote2 */
        $quote2 = $package1->getShippingQuotes()->get($responseBody['shipping']['packages'][0]['quotes'][1]['shippingMethod']);
        $quoteData2 = $responseBody['shipping']['packages'][0]['quotes'][1];
        $this->assertEquals(
            new Carbon($quoteData2['estimatedDeliveryDate']),
            $quote2->getEstimatedDeliveryDate()
        );
        $this->assertEquals(Money::fromCents($quoteData2['fee']), $quote2->getFee());
        $this->assertTrue($quote2->isSelected());

        /** @var ShippingQuote $quote3 */
        $quote3 = $package1->getShippingQuotes()->get($responseBody['shipping']['packages'][0]['quotes'][2]['shippingMethod']);
        $quoteData3 = $responseBody['shipping']['packages'][0]['quotes'][2];
        $this->assertEquals(
            new Carbon($quoteData3['estimatedDeliveryDate']),
            $quote3->getEstimatedDeliveryDate()
        );
        $this->assertEquals(Money::fromCents($quoteData3['fee']), $quote3->getFee());
        $this->assertFalse($quote3->isSelected());
    }

    public function testIsRecalculatingTheCartAndIsNotMappingPackagesIfNoneExist()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEmpty($newOrder->getPackages());
    }

    public function testIsRecalculatingTheCartAndIsMappingAvailablePickupStores()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $pickupStore1 = new PickupStore(1);
        $pickupStore2 = new PickupStore(2);
        $pickupStore2->select();

        $pickupStores = new PickupStores();
        $pickupStores->add($pickupStore1);
        $pickupStores->add($pickupStore2);

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setPickupStores($pickupStores);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['pickupStores'] = [
            [
                'id' => 1,
                'name' => 'Store 1',
                'description' => 'Store 1 Description',
                'postcode' => 'postcode-1',
                'geoHash' => 'geoHash-1',
                'region' => 'region-1',
                'municipality' => 'municipality-1',
                'city' => 'city-1',
                'sublocality' => 'sublocality-1',
                'neighborhood' => 'neighborhood-1',
                'addressLine1' => 'addressLine1-1',
                'addressLine2' => 'addressLine2-1',
                'apartment' => 'apartment-1',
                'referencePoint' => 'referencePoint-1',
                'network' => [
                    'id' => 1,
                    'name' => 'Company 1',
                ],
            ],
            [
                'id' => 2,
                'name' => 'Store 2',
                'description' => 'Store 2 Description',
                'postcode' => 'postcode-2',
                'geoHash' => 'geoHash-2',
                'region' => 'region-2',
                'municipality' => 'municipality-2',
                'city' => 'city-2',
                'sublocality' => 'sublocality-2',
                'neighborhood' => 'neighborhood-2',
                'addressLine1' => 'addressLine2-2',
                'addressLine2' => 'addressLine2-2',
                'apartment' => 'apartment-2',
                'referencePoint' => 'referencePoint-2',
                'network' => [
                    'id' => 2,
                    'name' => 'Company 2',
                ],
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertCount(2, $newOrder->getPickupStores());

        $pickupStoreData1 = $responseBody['shipping']['pickupStores'][0];
        /** @var PickupStore $pickupStore1 */
        $pickupStore1 = $newOrder->getPickupStores()->get($pickupStoreData1['id']);

        $this->assertSame($pickupStoreData1['id'], $pickupStore1->getId());
        $this->assertSame($pickupStoreData1['name'], $pickupStore1->getName());
        $this->assertSame($pickupStoreData1['description'], $pickupStore1->getDescription());
        $this->assertSame($pickupStoreData1['postcode'], $pickupStore1->getPostcode());
        $this->assertSame($pickupStoreData1['geoHash'], $pickupStore1->getGeohash());
        $this->assertSame($pickupStoreData1['region'], $pickupStore1->getRegion());
        $this->assertSame($pickupStoreData1['municipality'], $pickupStore1->getMunicipality());
        $this->assertSame($pickupStoreData1['city'], $pickupStore1->getCity());
        $this->assertSame($pickupStoreData1['sublocality'], $pickupStore1->getSubLocality());
        $this->assertSame($pickupStoreData1['neighborhood'], $pickupStore1->getNeighborhood());
        $this->assertSame($pickupStoreData1['addressLine1'], $pickupStore1->getLine1());
        $this->assertSame($pickupStoreData1['addressLine2'], $pickupStore1->getLine2());
        $this->assertSame($pickupStoreData1['apartment'], $pickupStore1->getApartment());
        $this->assertSame($pickupStoreData1['referencePoint'], $pickupStore1->getReferencePoint());
        $this->assertSame($pickupStoreData1['network']['id'], $pickupStore1->getNetworkId());
        $this->assertSame($pickupStoreData1['network']['name'], $pickupStore1->getNetworkName());
        $this->assertFalse($pickupStore1->isSelected());

        $pickupStoreData2 = $responseBody['shipping']['pickupStores'][1];
        /** @var PickupStore $pickupStore2 */
        $pickupStore2 = $newOrder->getPickupStores()->get($pickupStoreData2['id']);

        $this->assertSame($pickupStoreData2['id'], $pickupStore2->getId());
        $this->assertSame($pickupStoreData2['name'], $pickupStore2->getName());
        $this->assertSame($pickupStoreData2['description'], $pickupStore2->getDescription());
        $this->assertSame($pickupStoreData2['postcode'], $pickupStore2->getPostcode());
        $this->assertSame($pickupStoreData2['geoHash'], $pickupStore2->getGeohash());
        $this->assertSame($pickupStoreData2['region'], $pickupStore2->getRegion());
        $this->assertSame($pickupStoreData2['municipality'], $pickupStore2->getMunicipality());
        $this->assertSame($pickupStoreData2['city'], $pickupStore2->getCity());
        $this->assertSame($pickupStoreData2['sublocality'], $pickupStore2->getSubLocality());
        $this->assertSame($pickupStoreData2['neighborhood'], $pickupStore2->getNeighborhood());
        $this->assertSame($pickupStoreData2['addressLine2'], $pickupStore2->getLine2());
        $this->assertSame($pickupStoreData2['addressLine2'], $pickupStore2->getLine2());
        $this->assertSame($pickupStoreData2['apartment'], $pickupStore2->getApartment());
        $this->assertSame($pickupStoreData2['referencePoint'], $pickupStore2->getReferencePoint());
        $this->assertSame($pickupStoreData2['network']['id'], $pickupStore2->getNetworkId());
        $this->assertSame($pickupStoreData2['network']['name'], $pickupStore2->getNetworkName());
        $this->assertTrue($pickupStore2->isSelected());
    }

    public function testIsRecalculatingTheCartAndIsNotSelectingAPickupStoreThatWasSelectedButNotReturned()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $pickupStore1 = new PickupStore(1);
        $pickupStore2 = new PickupStore(2);
        $pickupStore2->select();

        $pickupStores = new PickupStores();
        $pickupStores->add($pickupStore1);
        $pickupStores->add($pickupStore2);

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setPickupStores($pickupStores);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['pickupStores'] = [
            [
                'id' => 1,
                'name' => 'Store 1',
                'description' => 'Store 1 Description',
                'postcode' => 'postcode-1',
                'geoHash' => 'geoHash-1',
                'region' => 'region-1',
                'municipality' => 'municipality-1',
                'city' => 'city-1',
                'sublocality' => 'sublocality-1',
                'neighborhood' => 'neighborhood-1',
                'addressLine1' => 'addressLine1-1',
                'addressLine2' => 'addressLine2-1',
                'apartment' => 'apartment-1',
                'referencePoint' => 'referencePoint-1',
                'network' => [
                    'id' => 1,
                    'name' => 'Company 1',
                ],
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertCount(1, $newOrder->getPickupStores());

        /** @var PickupStore $pickupStore1 */
        $pickupStore1 = $newOrder->getPickupStores()->get($responseBody['shipping']['pickupStores'][0]['id']);
        $this->assertFalse($pickupStore1->isSelected());

        $this->assertNull($newOrder->getPickupStores()->getSelected());
    }

    public function testIsRecalculatingTheCartAndIsNotMappingPickupStoresIfNoneAreAvailable()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEmpty($newOrder->getPickupStores());
    }

    public function testIsRecalculatingTheCartAndIsMappingTheWallet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['wallet'] = [
            'isActive' => true,
            'totalDiscount' => 10000,
            'totalPointsUsed' => 20000,
            'pointsBalance' => 30000,
            'shippingDiscount' => 5000,
            'pointsUsedShipping' => 5000,
            'maxPointsForOrder' => 30000,
            'conversionRate' => 100,
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(Money::fromCents($responseBody['wallet']['totalDiscount']), $newOrder->getWallet()->getTotalDiscount());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['totalPointsUsed']), $newOrder->getWallet()->getTotalPointsUsed());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['pointsBalance']), $newOrder->getWallet()->getPointsBalance());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['shippingDiscount']), $newOrder->getWallet()->getShippingDiscount());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['pointsUsedShipping']), $newOrder->getWallet()->getPointsUsedForShipping());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['maxPointsForOrder']), $newOrder->getWallet()->getMaxPointsForOrder());
        $this->assertSame($responseBody['wallet']['conversionRate'], $newOrder->getWallet()->getConversionRate());
    }

    public function testIsRecalculatingTheCartAndIsMappingAnEmptyWallet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $responseBody = $this->minimalResponseBody;
        $responseBody['wallet'] = [
            'isActive' => true,
            'totalDiscount' => 0,
            'totalPointsUsed' => 0,
            'pointsBalance' => 0,
            'shippingDiscount' => 0,
            'pointsUsedShipping' => 0,
            'maxPointsForOrder' => 0,
            'conversionRate' => 0,
        ];

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(new Wallet(), $newOrder->getWallet());
    }

    public function testIsRecalculatingTheCartAndIsMappingAvailablePartnerships()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['partnership']['available'] = [
            [
                'name' => 'Partnership 1',
                'code' => 'partnership-1',
                'identifier' => '123456',
                'level' => 'Gold',
                'isActive' => true,
                'appliedDiscount' => true,
            ],
            [
                'name' => 'Partnership 2',
                'code' => 'partnership-2',
                'identifier' => '09872349234',
                'level' => 'Platinum',
                'isActive' => false,
                'appliedDiscount' => false,
            ],
            [
                'name' => 'Partnership 3',
                'code' => 'partnership-3',
                'identifier' => '2342356adsfdsf',
                'level' => 'Gold',
                'isActive' => true,
                'appliedDiscount' => false,
            ],
            [
                'name' => 'Partnership 4',
                'code' => 'partnership-4',
                'identifier' => '09283490sdfjklsfj',
                'level' => null,
                'isActive' => true,
                'appliedDiscount' => true,
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertCount(4, $newOrder->getPartnerships());
        $this->assertContainsOnlyInstancesOf(Partnership::class, $newOrder->getPartnerships());

        $partnershipData1 = $responseBody['partnership']['available'][0];
        $partnership1 = $newOrder->getPartnerships()[0];

        $this->assertSame($partnershipData1['name'], $partnership1->getName());
        $this->assertSame($partnershipData1['code'], $partnership1->getCode());
        $this->assertSame($partnershipData1['identifier'], $partnership1->getAccountNumber());
        $this->assertSame($partnershipData1['level'], $partnership1->getLevel());
        $this->assertSame($partnershipData1['isActive'], $partnership1->isActive());
        $this->assertSame($partnershipData1['appliedDiscount'], $partnership1->isDiscountApplied());

        $partnershipData2 = $responseBody['partnership']['available'][1];
        $partnership2 = $newOrder->getPartnerships()[1];

        $this->assertSame($partnershipData2['name'], $partnership2->getName());
        $this->assertSame($partnershipData2['code'], $partnership2->getCode());
        $this->assertSame($partnershipData2['identifier'], $partnership2->getAccountNumber());
        $this->assertSame($partnershipData2['level'], $partnership2->getLevel());
        $this->assertSame($partnershipData2['isActive'], $partnership2->isActive());
        $this->assertSame($partnershipData2['appliedDiscount'], $partnership2->isDiscountApplied());

        $partnershipData3 = $responseBody['partnership']['available'][2];
        $partnership3 = $newOrder->getPartnerships()[2];

        $this->assertSame($partnershipData3['name'], $partnership3->getName());
        $this->assertSame($partnershipData3['code'], $partnership3->getCode());
        $this->assertSame($partnershipData3['identifier'], $partnership3->getAccountNumber());
        $this->assertSame($partnershipData3['level'], $partnership3->getLevel());
        $this->assertSame($partnershipData3['isActive'], $partnership3->isActive());
        $this->assertSame($partnershipData3['appliedDiscount'], $partnership3->isDiscountApplied());

        $partnershipData4 = $responseBody['partnership']['available'][3];
        $partnership4 = $newOrder->getPartnerships()[3];

        $this->assertSame($partnershipData4['name'], $partnership4->getName());
        $this->assertSame($partnershipData4['code'], $partnership4->getCode());
        $this->assertSame($partnershipData4['identifier'], $partnership4->getAccountNumber());
        $this->assertSame($partnershipData4['level'], $partnership4->getLevel());
        $this->assertSame($partnershipData4['isActive'], $partnership4->isActive());
        $this->assertSame($partnershipData4['appliedDiscount'], $partnership4->isDiscountApplied());
    }

    public function testIsRecalculatingTheCartAndIsNotMappingPartnershipsWhenNoneAreProvided()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEmpty($newOrder->getPartnerships());
    }

    public function testIsRecalculatingTheCartAndIsMappingThePartnershipDiscount()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['partnership']['discount'] = 100;

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($responseBody['partnership']['discount']),
            $newOrder->getPartnershipDiscount()
        );
    }

    public function testIsRecalculatingTheCartAndIsNotMappingThePartnershipDiscountWhenItThereIsntOne()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(new Money(), $newOrder->getPartnershipDiscount());
    }

    public function testIsRecalculatingTheCartAndIsMappingTheTaxAmount()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['tax']['amount'] = 100;

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($responseBody['tax']['amount']),
            $newOrder->getTaxAmount()
        );
    }

    public function testIsRecalculatingTheCartAndIsNotMappingTheTaxAmountWhenItThereIsntOne()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(new Money(), $newOrder->getTaxAmount());
    }

    public function testIsRecalculatingTheCartAndIsMappingTheSubTotal()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromRecalculateCart($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($this->minimalResponseBody['subtotal']),
            $newOrder->getSubTotal()
        );
    }

    public function testIsRecalculatingTheCartAndIsMappingTheGrandTotal()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($this->minimalResponseBody['grandTotal']),
            $newOrder->getGrandTotal()
        );
    }

    public function testIsPlacingTheOrderAndKeepingTheGeohash()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setGeohash('geohash');

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertSame($order->getGeohash(), $newOrder->getGeohash());
        $this->assertSame('geohash', $newOrder->getGeohash());
    }

    public function testIsPlacingTheOrderAndNotKeepingAGeohashThatIsntSet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertSame($order->getGeohash(), $newOrder->getGeohash());
        $this->assertNull($newOrder->getGeohash());
    }

    public function testIsPlacingTheOrderAndKeepingTheShippingAddress()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $shippingAddress = new Address();

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setShippingAddress($shippingAddress);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertSame($order->getShippingAddress(), $newOrder->getShippingAddress());
        $this->assertSame($shippingAddress, $newOrder->getShippingAddress());
    }

    public function testIsPlacingTheOrderAndNotKeepingAShippingAddressThatIsntSet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertSame($order->getShippingAddress(), $newOrder->getShippingAddress());
        $this->assertNull($newOrder->getShippingAddress());
    }

    public function testIsPlacingTheOrderAndMappingTheCouponCode()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $coupon = new Coupon('code');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->applyCoupon($coupon);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['coupon']['code'] = 'code';
        $responseBody['coupon']['discount'] = 100;

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertSame($coupon->getCode(), $newOrder->getCoupon()->getCode());
        $this->assertEquals(Money::fromCents(100), $newOrder->getCoupon()->getDiscount());
    }

    public function testIsPlacingTheOrderAndNotMappingACouponCodeThatIsntSet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertSame($order->getCoupon(), $newOrder->getCoupon());
        $this->assertNull($newOrder->getCoupon());
    }

    public function testIsPlacingTheOrderAndMappingTheAvailablePaymentMethods()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->applyCoupon(new Coupon('code'));

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['available'] = [
            'CreditCard',
            'Zero_Payment',
            'PayClub_HostedPaymentPage',
        ];
        $responseBody['payment']['paymentMethod']['allowed'] = [
            'CreditCard',
            'PayClub_HostedPaymentPage',
        ];
        $responseBody['payment']['paymentMethod']['allowedByCoupon'] = [
            'CreditCard',
        ];
        $responseBody['payment']['paymentMethod']['billingAddressRequired'] = [
            'CreditCard',
            'PayClub_HostedPaymentPage',
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);

        $this->assertInstanceOf(CreditCard::class, $newOrder->getAvailablePaymentMethods()[0]);
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[0]->isAllowed());
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[0]->isAllowedByCoupon());
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[0]->isBillingAddressRequired());

        $this->assertInstanceOf(ZeroPayment::class, $newOrder->getAvailablePaymentMethods()[1]);
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[1]->isAllowed());
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[1]->isAllowedByCoupon());
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[1]->isBillingAddressRequired());

        $this->assertInstanceOf(ExternalCheckout::class, $newOrder->getAvailablePaymentMethods()[2]);
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[2]->isAllowed());
        $this->assertFalse($newOrder->getAvailablePaymentMethods()[2]->isAllowedByCoupon());
        $this->assertTrue($newOrder->getAvailablePaymentMethods()[2]->isBillingAddressRequired());
    }

    public function testIsPlacingTheOrderAndNotMappingAnEmptySetOfAvailablePaymentMethods()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEmpty($newOrder->getAvailablePaymentMethods());
    }

    public function testIsPlacingTheOrderAndMappingTheSelectedPaymentMethod()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'Zero_Payment';

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertInstanceOf(ZeroPayment::class, $newOrder->getPaymentMethod());
    }

    public function testIsMappingTheBinNumberIfProvidedForCreditCardOnPlaceOrder()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'CreditCard';
        $responseBody['payment']['creditCardBinNumber'] = '1234';

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertInstanceOf(CreditCard::class, $newOrder->getPaymentMethod());
        $this->assertSame('1234', $newOrder->getPaymentMethod()->getBinNumber());
    }

    public function testIsIgnoringTheBinNumberIfProvidedForCreditCardOnPlaceOrder()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'CreditCard';

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertInstanceOf(CreditCard::class, $newOrder->getPaymentMethod());
        $this->assertNull($newOrder->getPaymentMethod()->getBinNumber());
    }

    public function testIsPlacingTheOrderAndNotMappingASelectedPaymentMethodWhenOneHasntBeenSelected()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEmpty($newOrder->getPaymentMethod());
    }

    public function testIsPlacingTheOrderAndMappingInstallmentOptions()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['installments'] = [
            1 => [
                'interestFee' => 100,
                'grandTotal' => 200,
                'amount' => 100,
                'totalInterest' => 100,
                'selected' => false,
            ],
            3 => [
                'paymentMethodName' => 'CreditCard',
                'interestFee' => 100,
                'grandTotal' => 900,
                'amount' => 300,
                'totalInterest' => 100,
                'selected' => true,
            ],
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);

        /** @var InstallmentOption $installmentOption1 */
        $installmentOption1 = $newOrder->getInstallmentOptions()->get(1);
        $this->assertSame(1, $installmentOption1->getInstallments());
        $this->assertEquals(Money::fromCents(100), $installmentOption1->getInterestFee());
        $this->assertEquals(Money::fromCents(200), $installmentOption1->getTotal());
        $this->assertEquals(Money::fromCents(100), $installmentOption1->getAmount());
        $this->assertEquals(Money::fromCents(100), $installmentOption1->getTotalInterest());
        $this->assertFalse($installmentOption1->isSelected());

        /** @var InstallmentOption $installmentOption3 */
        $installmentOption3 = $newOrder->getInstallmentOptions()->get(3);
        $this->assertSame(3, $installmentOption3->getInstallments());
        $this->assertEquals(Money::fromCents(100), $installmentOption3->getInterestFee());
        $this->assertEquals(Money::fromCents(900), $installmentOption3->getTotal());
        $this->assertEquals(Money::fromCents(300), $installmentOption3->getAmount());
        $this->assertEquals(Money::fromCents(100), $installmentOption3->getTotalInterest());
        $this->assertSame('CreditCard', $installmentOption3->getPaymentMethodName());
        $this->assertTrue($installmentOption3->isSelected());
    }

    public function testIsPlacingTheOrderAndContainsNoInstallmentOptionsIfNoneAreProvided()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEmpty($newOrder->getInstallmentOptions());
    }

    public function testIsPlacingTheOrderAndIsMappingTheShippingAmount()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['amount'] = 50;
        $responseBody['shipping']['originalAmount'] = 100;
        $responseBody['shipping']['discount'] = 50;

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(Money::fromCents(50), $newOrder->getShippingAmount());
        $this->assertEquals(Money::fromCents(100), $newOrder->getOriginalShippingAmount());
        $this->assertEquals(Money::fromCents(50), $newOrder->getShippingDiscountAmount());
    }

    public function testIsPlacingTheOrderAndIsNotMappingTheShippingAmountIfItDoesNotExist()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertNull($newOrder->getShippingAmount());
        $this->assertNull($newOrder->getOriginalShippingAmount());
        $this->assertEquals(Money::fromCents(0), $newOrder->getShippingDiscountAmount());
    }

    public function testIsPlacingTheOrderAndIsMappingTheItems()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product1 = new Product();
        $product1->setSku('4P479PE04DZI0LAEC-17814');
        $product1->setName('Item 1 From Cache');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product1);
        $productService->getBySku('SKUNOTINCACHE')->willThrow(ProductNotFoundException::class);
        $productService->getBySku('SKUWITHOUTMINIMUMDELIVERYDATE')->willThrow(ProductNotFoundException::class);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['items'] = [
            '4P479PE04DZI0LAEC-17814' => [
                'sku' => '4P479PE04DZI0LAEC-17814',
                'name' => 'Item 1',
                'quantity' => 1,
                'availableStock' => 2,
                'maxItemsToSell' => 2,
                'price' => [
                    'unitPrice' => 100,
                    'originalPrice' => 100,
                    'paidPrice' => 100,
                    'totalDiscount' => 0,
                ],
                'cartRule' => [
                    'discount' => 100,
                ],
                'coupon' => [
                    'discount' => 100,
                ],
                'linioCreditCard' => [
                    'discount' => 100,
                ],
                'wallet' => [
                    'discount' => 100,
                    'pointsUsed' => 100,
                ],
                'shipping' => [
                    'amount' => 100,
                    'discount' => 0,
                    'minimumDeliveryDate' => '2016-05-02',
                ],
                'tax' => [
                    'amount' => 100,
                    'percent' => 10,
                ],
                'linioPlus' => [
                    'level' => 1,
                    'quantity' => 1,
                ],
            ],
            'SKUNOTINCACHE' => [
                'sku' => 'SKUNOTINCACHE',
                'name' => 'Item 2 Not in Cache',
                'quantity' => 1,
                'availableStock' => 2,
                'maxItemsToSell' => 2,
                'price' => [
                    'unitPrice' => 100,
                    'originalPrice' => 100,
                    'paidPrice' => 100,
                    'totalDiscount' => 0,
                ],
                'cartRule' => [
                    'discount' => 100,
                ],
                'coupon' => [
                    'discount' => 100,
                ],
                'linioCreditCard' => [
                    'discount' => 100,
                ],
                'wallet' => [
                    'discount' => 100,
                    'pointsUsed' => 100,
                ],
                'shipping' => [
                    'amount' => 100,
                    'discount' => 0,
                    'minimumDeliveryDate' => '2016-05-02',
                ],
                'tax' => [
                    'amount' => 100,
                    'percent' => 10,
                ],
                'linioPlus' => [
                    'level' => 1,
                    'quantity' => 1,
                ],
            ],
            'SKUWITHOUTMINIMUMDELIVERYDATE' => [
                'sku' => 'SKUWITHOUTMINIMUMDELIVERYDATE',
                'name' => 'Item 3',
                'quantity' => 1,
                'availableStock' => 2,
                'maxItemsToSell' => 2,
                'price' => [
                    'unitPrice' => 100,
                    'originalPrice' => 100,
                    'paidPrice' => 100,
                    'totalDiscount' => 0,
                ],
                'cartRule' => [
                    'discount' => 100,
                ],
                'coupon' => [
                    'discount' => 100,
                ],
                'linioCreditCard' => [
                    'discount' => 100,
                ],
                'wallet' => [
                    'discount' => 100,
                    'pointsUsed' => 100,
                ],
                'shipping' => [
                    'amount' => 100,
                    'discount' => 0,
                    'minimumDeliveryDate' => null,
                ],
                'tax' => [
                    'amount' => 100,
                    'percent' => 10,
                ],
                'linioPlus' => [
                    'level' => 1,
                    'quantity' => 1,
                ],
            ],
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);

        /** @var Item $item1 */
        $item1 = $newOrder->getItems()->get('4P479PE04DZI0LAEC-17814');
        $item1Data = $responseBody['items'][$item1->getSku()];

        $this->assertSame('4P479PE04DZI0LAEC-17814', $item1->getSku());
        $this->assertSame($item1Data['quantity'], $item1->getQuantity());
        $this->assertSame($item1Data['availableStock'], $item1->getAvailableQuantity());
        $this->assertSame($item1Data['maxItemsToSell'], $item1->getMaxItemsToSell());
        $this->assertEquals(Money::fromCents($item1Data['price']['originalPrice']), $item1->getOriginalPrice());
        $this->assertEquals(Money::fromCents($item1Data['price']['paidPrice']), $item1->getPaidPrice());
        $this->assertEquals(Money::fromCents($item1Data['price']['unitPrice']), $item1->getUnitPrice());
        $this->assertEquals(Money::fromCents($item1Data['tax']['amount']), $item1->getTaxAmount());
        $this->assertEquals(Money::fromCents($item1Data['shipping']['amount']), $item1->getShippingAmount());
        $this->assertSame($item1Data['linioPlus']['quantity'], $item1->getLinioPlusEnabledQuantity());
        $this->assertSame($item1Data['linioPlus']['level'], $item1->getLinioPlusLevel());
        $this->assertSame('Item 1 From Cache', $item1->getProduct()->getName());
        $this->assertEquals(new Datetime($item1Data['shipping']['minimumDeliveryDate']), $item1->getMinimumDeliveryDate());
        $this->assertSame($product1, $item1->getProduct());

        /** @var Item $item2 */
        $item2 = $newOrder->getItems()->get('SKUNOTINCACHE');
        $item2Data = $responseBody['items'][$item2->getSku()];

        $this->assertSame('SKUNOTINCACHE', $item2->getSku());
        $this->assertSame($item2Data['quantity'], $item2->getQuantity());
        $this->assertSame($item2Data['availableStock'], $item2->getAvailableQuantity());
        $this->assertSame($item2Data['maxItemsToSell'], $item2->getMaxItemsToSell());
        $this->assertEquals(Money::fromCents($item2Data['price']['originalPrice']), $item2->getOriginalPrice());
        $this->assertEquals(Money::fromCents($item2Data['price']['paidPrice']), $item2->getPaidPrice());
        $this->assertEquals(Money::fromCents($item2Data['price']['unitPrice']), $item2->getUnitPrice());
        $this->assertEquals(Money::fromCents($item2Data['tax']['amount']), $item2->getTaxAmount());
        $this->assertEquals(Money::fromCents($item2Data['shipping']['amount']), $item2->getShippingAmount());
        $this->assertSame($item2Data['linioPlus']['quantity'], $item2->getLinioPlusEnabledQuantity());
        $this->assertSame($item2Data['linioPlus']['level'], $item2->getLinioPlusLevel());
        $this->assertSame('Item 2 Not in Cache', $item2->getProduct()->getName());
        $this->assertEquals(new Datetime($item2Data['shipping']['minimumDeliveryDate']), $item2->getMinimumDeliveryDate());

        /** @var Item $item3 */
        $item3 = $newOrder->getItems()->get('SKUWITHOUTMINIMUMDELIVERYDATE');
        $item3Data = $responseBody['items'][$item3->getSku()];

        $this->assertSame('SKUWITHOUTMINIMUMDELIVERYDATE', $item3->getSku());
        $this->assertSame($item3Data['quantity'], $item3->getQuantity());
        $this->assertSame($item3Data['availableStock'], $item3->getAvailableQuantity());
        $this->assertSame($item3Data['maxItemsToSell'], $item3->getMaxItemsToSell());
        $this->assertEquals(Money::fromCents($item3Data['price']['originalPrice']), $item3->getOriginalPrice());
        $this->assertEquals(Money::fromCents($item3Data['price']['paidPrice']), $item3->getPaidPrice());
        $this->assertEquals(Money::fromCents($item3Data['price']['unitPrice']), $item3->getUnitPrice());
        $this->assertEquals(Money::fromCents($item3Data['tax']['amount']), $item3->getTaxAmount());
        $this->assertEquals(Money::fromCents($item3Data['shipping']['amount']), $item3->getShippingAmount());
        $this->assertSame($item3Data['linioPlus']['quantity'], $item3->getLinioPlusEnabledQuantity());
        $this->assertSame($item3Data['linioPlus']['level'], $item3->getLinioPlusLevel());
        $this->assertSame('Item 3', $item3->getProduct()->getName());
        $this->assertNull($item3->getMinimumDeliveryDate());
    }

    public function testIsPlacingTheOrderAndMappingPackages()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['packages'] = [
            [
                'id' => 0,
                'fulfillmentType' => 'warehouse',
                'items' => [
                    '4P479PE04DZI0LAEC-17814' => 3,
                ],
                'quotes' => [
                    [
                        'shippingMethod' => 'express',
                        'fee' => 100,
                        'estimatedDeliveryDate' => '2016-06-02',
                        'pickupStoreId' => null,
                        'selected' => false,
                    ],
                    [
                        'shippingMethod' => 'regular',
                        'fee' => 100,
                        'estimatedDeliveryDate' => '2016-06-02',
                        'pickupStoreId' => null,
                        'selected' => true,
                    ],
                    [
                        'shippingMethod' => 'store',
                        'fee' => 100,
                        'estimatedDeliveryDate' => '2016-06-02',
                        'pickupStoreId' => 1,
                        'selected' => false,
                    ],
                ],
            ],
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertCount(1, $newOrder->getPackages());
        $this->assertCount(3, $newOrder->getPackages()->get(0)->getShippingQuotes());

        // This is the easiest way to make sure that we've cloned the item
        // Because we change the quantity, we can't do assertEquals
        /** @var Package $package1 */
        $package1 = $newOrder->getPackages()->get(0);
        $this->assertSame(
            $newOrder->getItems()->get('4P479PE04DZI0LAEC-17814')->getProduct(),
            $package1->getItems()->get('4P479PE04DZI0LAEC-17814')->getProduct()
        );

        /** @var ShippingQuote $quote1 */
        $quote1 = $package1->getShippingQuotes()->get($responseBody['shipping']['packages'][0]['quotes'][0]['shippingMethod']);
        $quoteData1 = $responseBody['shipping']['packages'][0]['quotes'][0];
        $this->assertEquals(
            new Carbon($quoteData1['estimatedDeliveryDate']),
            $quote1->getEstimatedDeliveryDate()
        );
        $this->assertEquals(Money::fromCents($quoteData1['fee']), $quote1->getFee());
        $this->assertFalse($quote1->isSelected());

        /** @var ShippingQuote $quote2 */
        $quote2 = $package1->getShippingQuotes()->get($responseBody['shipping']['packages'][0]['quotes'][1]['shippingMethod']);
        $quoteData2 = $responseBody['shipping']['packages'][0]['quotes'][1];
        $this->assertEquals(
            new Carbon($quoteData2['estimatedDeliveryDate']),
            $quote2->getEstimatedDeliveryDate()
        );
        $this->assertEquals(Money::fromCents($quoteData2['fee']), $quote2->getFee());
        $this->assertTrue($quote2->isSelected());

        /** @var ShippingQuote $quote3 */
        $quote3 = $package1->getShippingQuotes()->get($responseBody['shipping']['packages'][0]['quotes'][2]['shippingMethod']);
        $quoteData3 = $responseBody['shipping']['packages'][0]['quotes'][2];
        $this->assertEquals(
            new Carbon($quoteData3['estimatedDeliveryDate']),
            $quote3->getEstimatedDeliveryDate()
        );
        $this->assertEquals(Money::fromCents($quoteData3['fee']), $quote3->getFee());
        $this->assertFalse($quote3->isSelected());
    }

    public function testIsPlacingTheOrderAndIsNotMappingPackagesIfNoneExist()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEmpty($newOrder->getPackages());
    }

    public function testIsPlacingTheOrderAndIsMappingAvailablePickupStores()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $pickupStore1 = new PickupStore(1);
        $pickupStore2 = new PickupStore(2);
        $pickupStore2->select();

        $pickupStores = new PickupStores();
        $pickupStores->add($pickupStore1);
        $pickupStores->add($pickupStore2);

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setPickupStores($pickupStores);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['pickupStores'] = [
            [
                'id' => 1,
                'name' => 'Store 1',
                'description' => 'Store 1 Description',
                'postcode' => 'postcode-1',
                'geoHash' => 'geoHash-1',
                'region' => 'region-1',
                'municipality' => 'municipality-1',
                'city' => 'city-1',
                'sublocality' => 'sublocality-1',
                'neighborhood' => 'neighborhood-1',
                'addressLine1' => 'addressLine1-1',
                'addressLine2' => 'addressLine2-1',
                'apartment' => 'apartment-1',
                'referencePoint' => 'referencePoint-1',
                'network' => [
                    'id' => 1,
                    'name' => 'Company 1',
                ],
            ],
            [
                'id' => 2,
                'name' => 'Store 2',
                'description' => 'Store 2 Description',
                'postcode' => 'postcode-2',
                'geoHash' => 'geoHash-2',
                'region' => 'region-2',
                'municipality' => 'municipality-2',
                'city' => 'city-2',
                'sublocality' => 'sublocality-2',
                'neighborhood' => 'neighborhood-2',
                'addressLine1' => 'addressLine2-2',
                'addressLine2' => 'addressLine2-2',
                'apartment' => 'apartment-2',
                'referencePoint' => 'referencePoint-2',
                'network' => [
                    'id' => 2,
                    'name' => 'Company 2',
                ],
            ],
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertCount(2, $newOrder->getPickupStores());

        $pickupStoreData1 = $responseBody['shipping']['pickupStores'][0];
        /** @var PickupStore $pickupStore1 */
        $pickupStore1 = $newOrder->getPickupStores()->get($pickupStoreData1['id']);

        $this->assertSame($pickupStoreData1['id'], $pickupStore1->getId());
        $this->assertSame($pickupStoreData1['name'], $pickupStore1->getName());
        $this->assertSame($pickupStoreData1['description'], $pickupStore1->getDescription());
        $this->assertSame($pickupStoreData1['postcode'], $pickupStore1->getPostcode());
        $this->assertSame($pickupStoreData1['geoHash'], $pickupStore1->getGeohash());
        $this->assertSame($pickupStoreData1['region'], $pickupStore1->getRegion());
        $this->assertSame($pickupStoreData1['municipality'], $pickupStore1->getMunicipality());
        $this->assertSame($pickupStoreData1['city'], $pickupStore1->getCity());
        $this->assertSame($pickupStoreData1['sublocality'], $pickupStore1->getSubLocality());
        $this->assertSame($pickupStoreData1['neighborhood'], $pickupStore1->getNeighborhood());
        $this->assertSame($pickupStoreData1['addressLine1'], $pickupStore1->getLine1());
        $this->assertSame($pickupStoreData1['addressLine2'], $pickupStore1->getLine2());
        $this->assertSame($pickupStoreData1['apartment'], $pickupStore1->getApartment());
        $this->assertSame($pickupStoreData1['referencePoint'], $pickupStore1->getReferencePoint());
        $this->assertSame($pickupStoreData1['network']['id'], $pickupStore1->getNetworkId());
        $this->assertSame($pickupStoreData1['network']['name'], $pickupStore1->getNetworkName());
        $this->assertFalse($pickupStore1->isSelected());

        $pickupStoreData2 = $responseBody['shipping']['pickupStores'][1];
        /** @var PickupStore $pickupStore2 */
        $pickupStore2 = $newOrder->getPickupStores()->get($pickupStoreData2['id']);

        $this->assertSame($pickupStoreData2['id'], $pickupStore2->getId());
        $this->assertSame($pickupStoreData2['name'], $pickupStore2->getName());
        $this->assertSame($pickupStoreData2['description'], $pickupStore2->getDescription());
        $this->assertSame($pickupStoreData2['postcode'], $pickupStore2->getPostcode());
        $this->assertSame($pickupStoreData2['geoHash'], $pickupStore2->getGeohash());
        $this->assertSame($pickupStoreData2['region'], $pickupStore2->getRegion());
        $this->assertSame($pickupStoreData2['municipality'], $pickupStore2->getMunicipality());
        $this->assertSame($pickupStoreData2['city'], $pickupStore2->getCity());
        $this->assertSame($pickupStoreData2['sublocality'], $pickupStore2->getSubLocality());
        $this->assertSame($pickupStoreData2['neighborhood'], $pickupStore2->getNeighborhood());
        $this->assertSame($pickupStoreData2['addressLine2'], $pickupStore2->getLine2());
        $this->assertSame($pickupStoreData2['addressLine2'], $pickupStore2->getLine2());
        $this->assertSame($pickupStoreData2['apartment'], $pickupStore2->getApartment());
        $this->assertSame($pickupStoreData2['referencePoint'], $pickupStore2->getReferencePoint());
        $this->assertSame($pickupStoreData2['network']['id'], $pickupStore2->getNetworkId());
        $this->assertSame($pickupStoreData2['network']['name'], $pickupStore2->getNetworkName());
        $this->assertTrue($pickupStore2->isSelected());
    }

    public function testIsPlacingTheOrderAndIsNotSelectingAPickupStoreThatWasSelectedButNotReturned()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $pickupStore1 = new PickupStore(1);
        $pickupStore2 = new PickupStore(2);
        $pickupStore2->select();

        $pickupStores = new PickupStores();
        $pickupStores->add($pickupStore1);
        $pickupStores->add($pickupStore2);

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);
        $order->setPickupStores($pickupStores);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['shipping']['pickupStores'] = [
            [
                'id' => 1,
                'name' => 'Store 1',
                'description' => 'Store 1 Description',
                'postcode' => 'postcode-1',
                'geoHash' => 'geoHash-1',
                'region' => 'region-1',
                'municipality' => 'municipality-1',
                'city' => 'city-1',
                'sublocality' => 'sublocality-1',
                'neighborhood' => 'neighborhood-1',
                'addressLine1' => 'addressLine1-1',
                'addressLine2' => 'addressLine2-1',
                'apartment' => 'apartment-1',
                'referencePoint' => 'referencePoint-1',
                'network' => [
                    'id' => 1,
                    'name' => 'Company 1',
                ],
            ],
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertCount(1, $newOrder->getPickupStores());

        /** @var PickupStore $pickupStore1 */
        $pickupStore1 = $newOrder->getPickupStores()->get($responseBody['shipping']['pickupStores'][0]['id']);
        $this->assertFalse($pickupStore1->isSelected());

        $this->assertNull($newOrder->getPickupStores()->getSelected());
    }

    public function testIsPlacingTheOrderAndIsNotMappingPickupStoresIfNoneAreAvailable()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEmpty($newOrder->getPickupStores());
    }

    public function testIsPlacingTheOrderAndIsMappingTheWallet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['wallet'] = [
            'isActive' => true,
            'totalDiscount' => 100,
            'totalPointsUsed' => 200,
            'pointsBalance' => 300,
            'shippingDiscount' => 50,
            'pointsUsedShipping' => 50,
            'maxPointsForOrder' => 300,
            'conversionRate' => 100,
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(Money::fromCents($responseBody['wallet']['totalDiscount']), $newOrder->getWallet()->getTotalDiscount());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['totalPointsUsed']), $newOrder->getWallet()->getTotalPointsUsed());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['pointsBalance']), $newOrder->getWallet()->getPointsBalance());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['maxPointsForOrder']), $newOrder->getWallet()->getMaxPointsForOrder());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['shippingDiscount']), $newOrder->getWallet()->getShippingDiscount());
        $this->assertEquals(Money::fromCents($responseBody['wallet']['pointsUsedShipping']), $newOrder->getWallet()->getPointsUsedForShipping());
        $this->assertSame($responseBody['wallet']['conversionRate'], $newOrder->getWallet()->getConversionRate());
    }

    public function testIsPlacingTheOrderAndIsMappingAnEmptyWallet()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $responseBody = $this->minimalResponseBody;
        $responseBody['wallet'] = [
            'isActive' => true,
            'totalDiscount' => 0,
            'totalPointsUsed' => 0,
            'pointsBalance' => 0,
            'shippingDiscount' => 0,
            'pointsUsedShipping' => 0,
            'maxPointsForOrder' => 0,
            'conversionRate' => 0,
        ];

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(new Wallet(), $newOrder->getWallet());
    }

    public function testIsPlacingTheOrderAndIsMappingAvailablePartnerships()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['partnership']['available'] = [
            [
                'name' => 'Partnership 1',
                'code' => 'partnership-1',
                'identifier' => '123456',
                'level' => 'Gold',
                'isActive' => true,
                'appliedDiscount' => true,
            ],
            [
                'name' => 'Partnership 2',
                'code' => 'partnership-2',
                'identifier' => '09872349234',
                'level' => 'Platinum',
                'isActive' => false,
                'appliedDiscount' => false,
            ],
            [
                'name' => 'Partnership 3',
                'code' => 'partnership-3',
                'identifier' => '2342356adsfdsf',
                'level' => 'Gold',
                'isActive' => true,
                'appliedDiscount' => false,
            ],
            [
                'name' => 'Partnership 4',
                'code' => 'partnership-4',
                'identifier' => '09283490sdfjklsfj',
                'level' => null,
                'isActive' => true,
                'appliedDiscount' => true,
            ],
        ];

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertCount(4, $newOrder->getPartnerships());
        $this->assertContainsOnlyInstancesOf(Partnership::class, $newOrder->getPartnerships());

        $partnershipData1 = $responseBody['partnership']['available'][0];
        $partnership1 = $newOrder->getPartnerships()[0];

        $this->assertSame($partnershipData1['name'], $partnership1->getName());
        $this->assertSame($partnershipData1['code'], $partnership1->getCode());
        $this->assertSame($partnershipData1['identifier'], $partnership1->getAccountNumber());
        $this->assertSame($partnershipData1['level'], $partnership1->getLevel());
        $this->assertSame($partnershipData1['isActive'], $partnership1->isActive());
        $this->assertSame($partnershipData1['appliedDiscount'], $partnership1->isDiscountApplied());

        $partnershipData2 = $responseBody['partnership']['available'][1];
        $partnership2 = $newOrder->getPartnerships()[1];

        $this->assertSame($partnershipData2['name'], $partnership2->getName());
        $this->assertSame($partnershipData2['code'], $partnership2->getCode());
        $this->assertSame($partnershipData2['identifier'], $partnership2->getAccountNumber());
        $this->assertSame($partnershipData2['level'], $partnership2->getLevel());
        $this->assertSame($partnershipData2['isActive'], $partnership2->isActive());
        $this->assertSame($partnershipData2['appliedDiscount'], $partnership2->isDiscountApplied());

        $partnershipData3 = $responseBody['partnership']['available'][2];
        $partnership3 = $newOrder->getPartnerships()[2];

        $this->assertSame($partnershipData3['name'], $partnership3->getName());
        $this->assertSame($partnershipData3['code'], $partnership3->getCode());
        $this->assertSame($partnershipData3['identifier'], $partnership3->getAccountNumber());
        $this->assertSame($partnershipData3['level'], $partnership3->getLevel());
        $this->assertSame($partnershipData3['isActive'], $partnership3->isActive());
        $this->assertSame($partnershipData3['appliedDiscount'], $partnership3->isDiscountApplied());

        $partnershipData4 = $responseBody['partnership']['available'][3];
        $partnership4 = $newOrder->getPartnerships()[3];

        $this->assertSame($partnershipData4['name'], $partnership4->getName());
        $this->assertSame($partnershipData4['code'], $partnership4->getCode());
        $this->assertSame($partnershipData4['identifier'], $partnership4->getAccountNumber());
        $this->assertSame($partnershipData4['level'], $partnership4->getLevel());
        $this->assertSame($partnershipData4['isActive'], $partnership4->isActive());
        $this->assertSame($partnershipData4['appliedDiscount'], $partnership4->isDiscountApplied());
    }

    public function testIsPlacingTheOrderAndIsNotMappingPartnershipsWhenNoneAreProvided()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEmpty($newOrder->getPartnerships());
    }

    public function testIsPlacingTheOrderAndIsMappingThePartnershipDiscount()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['partnership']['discount'] = 100;

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($responseBody['partnership']['discount']),
            $newOrder->getPartnershipDiscount()
        );
    }

    public function testIsPlacingTheOrderAndIsNotMappingThePartnershipDiscountWhenItThereIsntOne()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(new Money(), $newOrder->getPartnershipDiscount());
    }

    public function testIsPlacingTheOrderAndIsMappingTheTaxAmount()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $responseBody = $this->minimalResponseBody;
        $responseBody['tax']['amount'] = 100;

        $newOrder = $input->fromPlaceOrder($order, $responseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($responseBody['tax']['amount']),
            $newOrder->getTaxAmount()
        );
    }

    public function testIsPlacingTheOrderAndIsNotMappingTheTaxAmountWhenItThereIsntOne()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(new Money(), $newOrder->getTaxAmount());
    }

    public function testIsPlacingTheOrderAndIsMappingTheSubTotal()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($this->minimalResponseBody['subtotal']),
            $newOrder->getSubTotal()
        );
    }

    public function testIsPlacingTheOrderAndIsMappingTheGrandTotal()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $product = new Product();
        $product->setSku('4P479PE04DZI0LAEC-17814');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn($product);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $newOrder = $input->fromPlaceOrder($order, $this->minimalResponseBody);

        $this->assertInstanceOf(CompletedOrder::class, $newOrder);
        $this->assertEquals(
            Money::fromCents($this->minimalResponseBody['grandTotal']),
            $newOrder->getGrandTotal()
        );
    }

    public function testIsRecalculatingTheCartAndMappingTheLoyaltyProgram()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn(new Product());

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());
        $responseBody = $this->minimalResponseBody;
        $responseBody['loyalty']['programName'] = 'Club_Premier';
        $responseBody['loyalty']['registration'] = '974703704';
        $responseBody['loyalty']['points'] = 70;

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $loyaltyProgram = $newOrder->getCustomer()->getLoyaltyProgram();
        $this->assertEquals($responseBody['loyalty']['programName'], $loyaltyProgram->getName());
        $this->assertEquals($responseBody['loyalty']['registration'], $loyaltyProgram->getId());
        $this->assertEquals($responseBody['loyalty']['points'], $newOrder->getLoyaltyPointsAccrued());
    }

    public function testIsRecalculatingTheCartAndMappingAllowedCreditCards()
    {
        $customer = new Customer();
        $customer->setLinioId('12345');

        $order = new Order($customer->getLinioId());
        $order->setCustomer($customer);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('4P479PE04DZI0LAEC-17814')->willReturn(new Product());

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());
        $responseBody = $this->minimalResponseBody;
        $responseBody['payment']['paymentMethod']['name'] = 'CreditCard';
        $responseBody['payment']['allowedCreditCards'] = [
            [
                'id' => 1,
                'maskedNumber' => '411111XXXX',
                'holder' => 'Adolfinho Mequetreque',
                'expiration' => '2019-12-22',
                'brand' => 'visa',
            ],
            [
                'id' => 2,
                'maskedNumber' => '521111XXXX',
                'holder' => 'Jaimito El Cartero',
                'expiration' => '2019-12-22',
                'brand' => 'mastercard',
            ],
        ];

        $newOrder = $input->fromRecalculateCart($order, $responseBody);

        $this->assertInstanceOf(Order::class, $newOrder);
        $this->assertInstanceOf(CreditCard::class, $newOrder->getPaymentMethod());

        $cards = $newOrder->getPaymentMethod()->getSavedCards();
        $this->assertEquals('1', $cards[0]->getId());
        $this->assertEquals('Adolfinho Mequetreque', $cards[0]->getCardholderName());
        $this->assertEquals('411111XXXX', $cards[0]->getCardNumber());
        $this->assertEquals('visa', $cards[0]->getBrand());
        $this->assertEquals(new DateTime('2019-12-22'), $cards[0]->getExpirationDate());

        $this->assertEquals('2', $cards[1]->getId());
        $this->assertEquals('Jaimito El Cartero', $cards[1]->getCardholderName());
        $this->assertEquals('521111XXXX', $cards[1]->getCardNumber());
        $this->assertEquals('mastercard', $cards[1]->getBrand());
        $this->assertEquals(new DateTime('2019-12-22'), $cards[1]->getExpirationDate());
    }
}
