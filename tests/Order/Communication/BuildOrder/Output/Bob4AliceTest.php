<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder\Output;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Wallet;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Type\Money;

class Bob4AliceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingToRecalculateCart()
    {
        $expectedOutput = [
            'customer' => [
                'id' => null,
                'assistedSalesOperator' => null,
            ],
            'store' => [
                'id' => 1,
            ],
            'coupon' => [
                'code' => null,
            ],
            'payment' => [
                'paymentMethodName' => null,
                'dependentPaymentMethodName' => null,
                'creditCardBinNumber' => null,
                'installments' => [
                    'quantity' => null,
                ],
            ],
            'shipping' => [
                'postcode' => null,
                'region' => null,
                'municipality' => null,
                'city' => null,
                'packages' => [],
                'pickupStore' => [
                    'id' => null,
                    'geoHash' => null,
                ],
                'fixedShipping' => [
                    'amount' => null,
                    'cost' => null,
                ],
            ],
            'wallet' => [
                'totalPointsUsed' => null,
            ],
            'items' => [],
            'grandTotal' => 0.0,
            'isFastLane' => false,
        ];

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $order = new Order('123');
        $order->setCustomer(new Customer());
        $output = new Bob4Alice($platformInformation);
        $recalculateCartRequest = $output->toRecalculateCart($order);

        $this->assertEquals($expectedOutput, $recalculateCartRequest);
    }

    public function testIsTransformingToRecalculateCartWithCreditCard()
    {
        $expectedOutput = [
            'customer' => [
                'id' => null,
                'assistedSalesOperator' => null,
            ],
            'store' => [
                'id' => 1,
            ],
            'coupon' => [
                'code' => null,
            ],
            'payment' => [
                'paymentMethodName' => 'Foobar',
                'dependentPaymentMethodName' => null,
                'creditCardBinNumber' => '123456',
                'installments' => [
                    'quantity' => null,
                ],
            ],
            'shipping' => [
                'postcode' => null,
                'region' => null,
                'municipality' => null,
                'city' => null,
                'packages' => [],
                'pickupStore' => [
                    'id' => null,
                    'geoHash' => null,
                ],
                'fixedShipping' => [
                    'amount' => null,
                    'cost' => null,
                ],
            ],
            'wallet' => [
                'totalPointsUsed' => null,
            ],
            'items' => [],
            'grandTotal' => 0.0,
            'isFastLane' => false,
        ];

        $order = new Order('123');
        $order->setCustomer(new Customer());

        $creditCard = new CreditCard('Foobar');
        $creditCard->setBinNumber('123456');
        $order->setPaymentMethod($creditCard);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $output = new Bob4Alice($platformInformation);
        $recalculateCartRequest = $output->toRecalculateCart($order);

        $this->assertEquals($expectedOutput, $recalculateCartRequest);
    }

    public function testIsTransformingToRecalculateCartWithWallet()
    {
        $expectedOutput = [
            'customer' => [
                'id' => null,
                'assistedSalesOperator' => null,
            ],
            'store' => [
                'id' => 1,
            ],
            'coupon' => [
                'code' => null,
            ],
            'payment' => [
                'paymentMethodName' => 'Foobar',
                'dependentPaymentMethodName' => null,
                'creditCardBinNumber' => '123456',
                'installments' => [
                    'quantity' => null,
                ],
            ],
            'shipping' => [
                'postcode' => null,
                'region' => null,
                'municipality' => null,
                'city' => null,
                'packages' => [],
                'pickupStore' => [
                    'id' => null,
                    'geoHash' => null,
                ],
                'fixedShipping' => [
                    'amount' => null,
                    'cost' => null,
                ],
            ],
            'wallet' => [
                'totalPointsUsed' => 12000,
            ],
            'items' => [],
            'grandTotal' => 0.0,
            'isFastLane' => false,
        ];

        $order = new Order('123');
        $order->setCustomer(new Customer());

        $creditCard = new CreditCard('Foobar');
        $creditCard->setBinNumber('123456');
        $order->setPaymentMethod($creditCard);

        $wallet = new Wallet();
        $wallet->setTotalPointsUsed(Money::fromCents(12000));
        $order->setWallet($wallet);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $output = new Bob4Alice($platformInformation);
        $recalculateCartRequest = $output->toRecalculateCart($order);

        $this->assertEquals($expectedOutput, $recalculateCartRequest);
    }

    public function testIsTransformingToRecalculateCartWithAssistedSalesOperator()
    {
        $expectedOutput = [
            'customer' => [
                'id' => 1,
                'assistedSalesOperator' => 'agent1',
            ],
            'store' => [
                'id' => 1,
            ],
            'coupon' => [
                'code' => null,
            ],
            'payment' => [
                'paymentMethodName' => null,
                'dependentPaymentMethodName' => null,
                'creditCardBinNumber' => null,
                'installments' => [
                    'quantity' => null,
                ],
            ],
            'shipping' => [
                'postcode' => null,
                'region' => null,
                'municipality' => null,
                'city' => null,
                'packages' => [],
                'pickupStore' => [
                    'id' => null,
                    'geoHash' => null,
                ],
                'fixedShipping' => [
                    'amount' => null,
                    'cost' => null,
                ],
            ],
            'wallet' => [
                'totalPointsUsed' => null,
            ],
            'items' => [],
            'grandTotal' => 0.0,
            'isFastLane' => false,
        ];

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $customer = new Customer();
        $customer->setId(1);
        $customer->impersonate('agent1');

        $order = new Order('123');
        $order->setCustomer($customer);
        $output = new Bob4Alice($platformInformation);
        $recalculateCartRequest = $output->toRecalculateCart($order);

        $this->assertEquals($expectedOutput, $recalculateCartRequest);
    }
}
