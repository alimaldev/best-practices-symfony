<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Communication\BuildOrder\Input\Bob4Alice as Bob4AliceOrderInput;
use Linio\Frontend\Order\Communication\BuildOrder\Output\Bob4Alice as Bob4AliceOrderOutput;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\RecalculatedOrder;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\Test\RecalculateCartResponseFixture;
use Linio\Type\Money;
use Prophecy\Argument;

class Bob4AliceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsRecalculatingOrder()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();
        $bob4AliceResponse->setPaymentMethod([
            'name' => null,
            'available' => ['Foo', 'Bar', 'Baz'],
            'allowed' => ['Foo', 'Bar'],
            'billingAddressRequired' => ['Bar'],
            'allowedByCoupon' => ['Foo'],
            'resolved' => null,
        ]);

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->recalculateOrder($this->createOrder());
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
        $availablePaymentMethods = $recalculatedOrder->getOrder()->getAvailablePaymentMethods();

        // Check allowed payment methods
        $this->assertTrue($availablePaymentMethods[0]->isAllowed());
        $this->assertTrue($availablePaymentMethods[1]->isAllowed());
        $this->assertFalse($availablePaymentMethods[2]->isAllowed());

        // Check payment methods with billing address required
        $this->assertTrue($availablePaymentMethods[0]->isAllowedByCoupon());
        $this->assertFalse($availablePaymentMethods[1]->isAllowedByCoupon());
        $this->assertFalse($availablePaymentMethods[2]->isAllowedByCoupon());

        // Check payment methods with billing address required
        $this->assertFalse($availablePaymentMethods[0]->isBillingAddressRequired());
        $this->assertTrue($availablePaymentMethods[1]->isBillingAddressRequired());
        $this->assertFalse($availablePaymentMethods[2]->isBillingAddressRequired());
    }

    public function testIsAddingProduct()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->addProduct($this->createOrder(), new Item('123'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsDetectingErrorWhenAddingProduct()
    {
        $bob4AliceResponse = new RecalculateCartResponseFixture();
        $bob4AliceResponse->addError('123', 'invalid_product');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $this->expectException(OrderException::class);

        $recalculatedOrder = $adapter->addProduct($this->createOrder(), new Item('123'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsRemovingProduct()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->removeProduct($this->createOrder(), new Item('123'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsUpdatingQuantity()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->updateQuantity($this->createOrder(), new Item('123'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsDetectingErrorWhenUpdatingQuantity()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();
        $bob4AliceResponse->addError('123', 'invalid_quantity');

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $this->expectException(OrderException::class);

        $recalculatedOrder = $adapter->updateQuantity($this->createOrder(), new Item('123'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsApplyingCoupon()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->applyCoupon($this->createOrder(), new Coupon('VALID'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsDetectingErrorWhenApplyingCoupon()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();
        $bob4AliceResponse->addError('VALID', 'invalid_coupon');

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $this->expectException(OrderException::class);

        $recalculatedOrder = $adapter->applyCoupon($this->createOrder(), new Coupon('VALID'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsRemovingCoupon()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->applyCoupon($this->createOrder(), new Coupon('VALID'));
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsSettingShippingAddress()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->setShippingAddress($this->createOrder(), new Address());
        $this->assertRecalculatedOrderWasParsed($recalculatedOrder);
    }

    public function testIsDetectingErrorWhenSettingShippingAddress()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();
        $bob4AliceResponse->addPackage(2, ['ABC-123' => 1]);
        $bob4AliceResponse->addPackage(3, ['ABC-456' => 1]);
        $bob4AliceResponse->addError('ShippingPackage_2', 'ERROR_UNDELIVERABLE_PACKAGE');
        $bob4AliceResponse->addError('ShippingPackage_3', 'ERROR_UNDELIVERABLE_PACKAGE');

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->setShippingAddress($this->createOrder(), new Address());
        $this->assertCount(2, $recalculatedOrder->getUndeliverables());
        $this->assertCount(3, $recalculatedOrder->getOrder()->getPackages());
        $this->assertEquals(['SKU0002-005' => 'ERROR_PRODUCT_OUT_OF_STOCK'], $recalculatedOrder->getErrors());
    }

    public function testIsHandlingNullShippingAmount()
    {
        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku(Argument::any())->willReturn(new Product());

        $bob4AliceResponse = new RecalculateCartResponseFixture();
        $bob4AliceResponse->clearShippingAmount();

        $orderInput = new Bob4AliceOrderInput();
        $orderInput->setProductService($productService->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));
        $adapter->setOrderOutput(new Bob4AliceOrderOutput($platformInformation));
        $adapter->setOrderInput($orderInput);

        $recalculatedOrder = $adapter->recalculateOrder($this->createOrder(), new Item('123'));
        $this->assertNull($recalculatedOrder->getOrder()->getShippingAmount());
    }

    protected function createOrder(): Order
    {
        $order = new Order('123');
        $order->setCustomer(new Customer());

        return $order;
    }

    /**
     * TODO: Refactor this to cover entire object.
     */
    protected function assertRecalculatedOrderWasParsed(RecalculatedOrder $recalculatedOrder)
    {
        $this->assertEquals(['SKU0002-005' => 'ERROR_PRODUCT_OUT_OF_STOCK'], $recalculatedOrder->getErrors());
        $this->assertEquals('VALID_1', $recalculatedOrder->getOrder()->getCoupon()->getCode());
        $this->assertEquals(new Money(177.52), $recalculatedOrder->getOrder()->getSubTotal());
        $this->assertEquals(new Money(377.52), $recalculatedOrder->getOrder()->getGrandTotal());
        $this->assertEquals(new Money(28.34), $recalculatedOrder->getOrder()->getTaxAmount());
        $this->assertEquals(new Money(200), $recalculatedOrder->getOrder()->getShippingAmount());
        $this->assertEquals(new Money(0), $recalculatedOrder->getOrder()->getShippingDiscountAmount());
        $this->assertEquals(new Money(0), $recalculatedOrder->getOrder()->getTotalDiscountAmount());
        $this->assertEquals(new Money(0), $recalculatedOrder->getOrder()->getLinioPlusSavedAmount());
        $this->assertEquals(37, $recalculatedOrder->getOrder()->getLoyaltyPointsAccrued());

        $this->assertCount(1, $recalculatedOrder->getOrder()->getPackages());
        $this->assertCount(1, $recalculatedOrder->getOrder()->getPackages()[1]->getItems());
        $this->assertCount(1, $recalculatedOrder->getOrder()->getItems());
    }

    protected function createMockBob4AliceClient(string $body, int $statusCode = 200): GuzzleClient
    {
        $mock = new MockHandler([new Response($statusCode, [], $body)]);
        $handler = HandlerStack::create($mock);

        return new GuzzleClient(['handler' => $handler]);
    }
}
