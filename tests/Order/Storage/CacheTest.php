<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Storage;

use Carbon\Carbon;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use PHPUnit_Framework_TestCase;

class CacheTest extends PHPUnit_Framework_TestCase
{
    public function testItRetrievesFromCacheForACustomerWithAStoredAddress()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => 1,
            'shippingAddress' => ['region' => 'foobar'],
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertSame($cachedOrder['shippingAddress']['region'], $order->getShippingAddress()->getRegion());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItRetrievesFromCacheForACustomerWithNoAddress()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => 1,
            'shippingAddress' => null,
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertEmpty($order->getShippingAddress());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItRetrievesFromCacheForACustomerWithAddressData()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => 1,
            'shippingAddress' => [
                'postcode' => '06500',
                'region' => 'REGION',
                'municipality' => 'MUNICIPALITY',
                'city' => 'CITY',
            ],
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertSame($cachedOrder['shippingAddress']['postcode'], $order->getShippingAddress()->getPostCode());
        $this->assertSame($cachedOrder['shippingAddress']['region'], $order->getShippingAddress()->getRegion());
        $this->assertSame($cachedOrder['shippingAddress']['municipality'], $order->getShippingAddress()->getMunicipality());
        $this->assertSame($cachedOrder['shippingAddress']['city'], $order->getShippingAddress()->getCity());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItRetrievesFromCacheForACustomerWithPartialAddressData()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => 1,
            'shippingAddress' => [
                'postcode' => null,
                'region' => 'REGION',
                'city' => 'CITY',
            ],
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertNull($order->getShippingAddress()->getPostCode());
        $this->assertNull($order->getShippingAddress()->getMunicipality());
        $this->assertSame($cachedOrder['shippingAddress']['region'], $order->getShippingAddress()->getRegion());
        $this->assertSame($cachedOrder['shippingAddress']['city'], $order->getShippingAddress()->getCity());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItRetrievesFromCacheForAGuestWithAddressData()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => null,
            'shippingAddress' => [
                'postcode' => '06500',
                'region' => 'REGION',
                'municipality' => 'MUNICIPALITY',
                'city' => 'CITY',
            ],
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertSame($cachedOrder['shippingAddress']['postcode'], $order->getShippingAddress()->getPostCode());
        $this->assertSame($cachedOrder['shippingAddress']['region'], $order->getShippingAddress()->getRegion());
        $this->assertSame($cachedOrder['shippingAddress']['municipality'], $order->getShippingAddress()->getMunicipality());
        $this->assertSame($cachedOrder['shippingAddress']['city'], $order->getShippingAddress()->getCity());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItRetrievesFromCacheForACustomerWithAStoredPaymentMethod()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => 1,
            'shippingAddress' => ['region' => 'foobar'],
            'paymentMethod' => 'CashOnDelivery',
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertSame($cachedOrder['shippingAddress']['region'], $order->getShippingAddress()->getRegion());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItRetrievesFromCacheForACustomerWithAStoredBin()
    {
        $cachedOrder = [
            'coupon' => 'CODE',
            'customer' => 1,
            'shippingAddress' => ['region' => 'foobar'],
            'paymentMethod' => 'CreditCard',
            'paymentBin' => '123456',
            'updatedAt' => '2016-04-04 00:00:00',
            'items' => [
                'SKU001-001' => 2,
                'SKU002-002' => 1,
            ],
        ];

        $customer = new Customer();
        $customer->setId($cachedOrder['customer']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn($cachedOrder);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $order = $cache->retrieve('12345');

        $paymentMethod = new CreditCard($cachedOrder['paymentMethod']);
        $paymentMethod->setBinNumber($cachedOrder['paymentBin']);

        $this->assertSame($cachedOrder['coupon'], $order->getCoupon()->getCode());
        $this->assertSame($cachedOrder['shippingAddress']['region'], $order->getShippingAddress()->getRegion());
        $this->assertEquals(new Carbon($cachedOrder['updatedAt']), $order->getUpdatedAt());
        $this->assertCount(2, $order->getItems());
        $this->assertSame($cachedOrder['items']['SKU001-001'], $order->getItems()->get('SKU001-001')->getQuantity());
        $this->assertSame($cachedOrder['items']['SKU002-002'], $order->getItems()->get('SKU002-002')->getQuantity());
    }

    public function testItThrowsAnExceptionWhenNotInCache()
    {
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Cache::ORDER_CACHE_KEY . ':12345')->willReturn(null);

        $cache = new Cache();
        $cache->setCacheService($cacheService->reveal());

        $this->expectException(OrderException::class);

        $cache->retrieve('12345');
    }
}
