<?php

namespace Linio\Frontend\Marketing;

use DateTime;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Security\GuestUser;
use Linio\Type\Money;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class DataLayerServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testSetDataWithEmptyValues()
    {
        $dataLayerService = new DataLayerService();
        $dataLayerService->addData([]);

        $results = $dataLayerService->getData();

        $this->assertEmpty($results);
    }

    public function testSetDataWithDataSets()
    {
        $data1 = [
            'bob' => 'alice',
            'rocket' => 'linio',
        ];

        $dataLayerService = new DataLayerService();
        $dataLayerService->addData($data1);

        $this->assertEquals($data1, $dataLayerService->getData());

        $data2 = [
            'bob' => 'bob',
        ];

        $dataLayerService->addData($data2);

        $this->assertEquals(array_merge($data1, $data2), $dataLayerService->getData());

        $data3 = array_merge($data1, $data2, ['another' => 'value']);

        $dataLayerService->addData($data3);

        $this->assertEquals($data3, $dataLayerService->getData());
    }

    public function testIsPopulatingHome()
    {
        $expectedData = [
            'pageType' => 'home',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 1,
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->populateHome();

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingHomeWithStringToken()
    {
        $expectedData = [
            'pageType' => 'home',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 2,
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new GuestUser());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->populateHome();

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingHomeWithNls()
    {
        $expectedData = [
            'pageType' => 'home',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 2,
            'mail_hash' => 'n/a',
            'nls' => '304834',
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new GuestUser());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);
        $session->get('data_layer/nls')->willReturn('304834');

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->populateHome();

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingBrand()
    {
        $expectedData = [
            'pageType' => 'brand',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'brand' => 'ABC',
            'mail_hash' => 'n/a',
            'nls' => null,
            'boost' => [
                'factor' => 5,
                'keywords' => ['ABC'],
            ],
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $brand = new Brand();
        $brand->setName('ABC');

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->populateBrand($brand);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCategoryWithFullDataSet()
    {
        $expectedData = [
            'pageType' => 'category',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'gender' => 'n/a',
            'c_plist' => ['A', 'B'],
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'ecommerce' => ['impressions' => [
                [
                    'name' => 'test',
                    'id' => 'SKU00A',
                    'price' => 100.0,
                    'brand' => 'Brand',
                    'category' => 'category',
                    'variant' => 'Variant',
                ],
                [
                    'name' => 'test',
                    'id' => 'SKU00B',
                    'price' => 100.0,
                    'brand' => 'Brand',
                    'category' => 'category',
                    'variant' => 'Variant',
                ],
            ]],
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setName('category');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setName('category');
        $category2->setUrlKey('bar');

        $brand = new Brand();
        $brand->setName('Brand');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $productA = new Product();
        $productA->setSku('SKU00A');
        $productA->setName('test');
        $productA->setPrice(new Money(100));
        $productA->setConfigId('A');
        $productA->setBrand($brand);
        $productA->setCategory($category1);
        $productA->setVariationType('Variant');

        $productB = new Product();
        $productB->setSku('SKU00B');
        $productB->setName('test');
        $productB->setPrice(new Money(100));
        $productB->setConfigId('B');
        $productB->setBrand($brand);
        $productB->setCategory($category2);
        $productB->setVariationType('Variant');

        $searchResult = new SearchResult();
        $searchResult->setProducts([$productA, $productB]);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateCategory($category1, $searchResult);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCategoryWithoutBrands()
    {
        $expectedData = [
            'pageType' => 'category',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'gender' => 'n/a',
            'c_plist' => ['A', 'B'],
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'ecommerce' => ['impressions' => [
                [
                    'name' => 'test',
                    'id' => 'SKU00A',
                    'price' => 100.0,
                    'brand' => 'null',
                    'category' => 'category',
                    'variant' => 'Variant',
                ],
                [
                    'name' => 'test',
                    'id' => 'SKU00B',
                    'price' => 100.0,
                    'brand' => 'null',
                    'category' => 'category',
                    'variant' => 'Variant',
                ],
            ]],
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setName('category');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setName('category');
        $category2->setUrlKey('bar');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $productA = new Product();
        $productA->setSku('SKU00A');
        $productA->setName('test');
        $productA->setPrice(new Money(100));
        $productA->setConfigId('A');

        $productA->setCategory($category1);
        $productA->setVariationType('Variant');

        $productB = new Product();
        $productB->setSku('SKU00B');
        $productB->setName('test');
        $productB->setPrice(new Money(100));
        $productB->setConfigId('B');

        $productB->setCategory($category2);
        $productB->setVariationType('Variant');

        $searchResult = new SearchResult();
        $searchResult->setProducts([$productA, $productB]);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateCategory($category1, $searchResult);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCategoryWithoutVariants()
    {
        $expectedData = [
            'pageType' => 'category',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'gender' => 'n/a',
            'c_plist' => ['A', 'B'],
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'ecommerce' => ['impressions' => [
                [
                    'name' => 'test',
                    'id' => 'SKU00A',
                    'price' => 100.0,
                    'brand' => 'Brand',
                    'category' => 'category',
                    'variant' => 'null',
                ],
                [
                    'name' => 'test',
                    'id' => 'SKU00B',
                    'price' => 100.0,
                    'brand' => 'Brand',
                    'category' => 'category',
                    'variant' => 'null',
                ],
            ]],
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setName('category');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setName('category');
        $category2->setUrlKey('bar');

        $brand = new Brand();
        $brand->setName('Brand');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $productA = new Product();
        $productA->setSku('SKU00A');
        $productA->setName('test');
        $productA->setPrice(new Money(100));
        $productA->setConfigId('A');
        $productA->setBrand($brand);
        $productA->setCategory($category1);

        $productB = new Product();
        $productB->setSku('SKU00B');
        $productB->setName('test');
        $productB->setPrice(new Money(100));
        $productB->setConfigId('B');
        $productB->setBrand($brand);
        $productB->setCategory($category2);

        $searchResult = new SearchResult();
        $searchResult->setProducts([$productA, $productB]);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateCategory($category1, $searchResult);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingSearch()
    {
        $expectedData = [
            'pageType' => 'search',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 1,
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $brand = new Brand();
        $brand->setName('ABC');

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->populateSearch($brand);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCategoryBrand()
    {
        $expectedData = [
            'pageType' => 'categoryBrand',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'gender' => 'n/a',
            'brand' => 'ABC',
            'c_plist' => ['A', 'B'],
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'ecommerce' => ['impressions' => [
                [
                    'name' => 'test',
                    'id' => 'SKU00A',
                    'price' => 100.0,
                    'brand' => 'Brand',
                    'category' => 'category',
                    'variant' => 'Variant',
                ],
                [
                    'name' => 'test',
                    'id' => 'SKU00B',
                    'price' => 100.0,
                    'brand' => 'Brand',
                    'category' => 'category',
                    'variant' => 'Variant',
                ],
            ]],
            'mail_hash' => 'n/a',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setName('category');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setName('category');
        $category2->setUrlKey('bar');

        $brand = new Brand();
        $brand->setName('Brand');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $productA = new Product();
        $productA->setSku('SKU00A');
        $productA->setName('test');
        $productA->setPrice(new Money(100));
        $productA->setConfigId('A');
        $productA->setBrand($brand);
        $productA->setCategory($category1);
        $productA->setVariationType('Variant');

        $productB = new Product();
        $productB->setSku('SKU00B');
        $productB->setName('test');
        $productB->setPrice(new Money(100));
        $productB->setConfigId('B');
        $productB->setBrand($brand);
        $productB->setCategory($category2);
        $productB->setVariationType('Variant');

        $searchResult = new SearchResult();
        $searchResult->setProducts([$productA, $productB]);

        $brand = new Brand();
        $brand->setName('ABC');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateCategoryBrand($category1, $searchResult, $brand);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingProduct()
    {
        $expectedData = [
            'pageType' => 'product',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'new_user' => 1,
            'gender' => 'n/a',
            'brand' => 'ABC',
            'sku_simple' => 'SKU00A-1',
            'product_name' => 'Foobar 2000',
            'price' => 100.0,
            'special_price' => 0.0,
            'large_image' => '',
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'msrpPrice' => 100.0,
            'product_id' => 'A',
            'small_image' => '',
            'ecommerce' => [
                'detail' => [
                    'products' => [
                        [
                            'name' => 'Foobar 2000',
                            'id' => 'SKU00A',
                            'price' => 100.0,
                            'brand' => 'ABC',
                            'category' => 'null',
                            'variant' => 'variationA',
                        ],
                    ],
                ],
            ],
            'sku_config' => 'SKU00A',
            'mail_hash' => 'n/a',
            'nls' => null,
            'is_international' => 0,
            'ean_code' => '',
            'is_master' => 0,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setUrlKey('bar');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $productSimple = new Simple();
        $productSimple->setOriginalPrice(new Money(100));
        $productSimple->setSku('SKU00A-1');
        $product = new Product();
        $product->setSku('SKU00A');
        $product->setConfigId('A');
        $product->setName('Foobar 2000');
        $product->setBrand($this->getBrandMock());
        $product->setCategory($category1);
        $product->addSimple($productSimple);
        $product->setVariationType('variationA');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateProduct($product);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingProductWithMarketPlaceParentSKU()
    {
        $expectedData = [
            'pageType' => 'product',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'new_user' => 1,
            'gender' => 'n/a',
            'brand' => 'ABC',
            'sku_simple' => 'SKU00A-1',
            'product_name' => 'Foobar 2000',
            'price' => 100.0,
            'special_price' => 0.0,
            'large_image' => '',
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'msrpPrice' => 100.0,
            'product_id' => 'A',
            'small_image' => '',
            'ecommerce' => [
                'detail' => [
                    'products' => [
                        [
                            'name' => 'Foobar 2000',
                            'id' => 'SKU00A',
                            'price' => 100.0,
                            'brand' => 'ABC',
                            'category' => 'null',
                            'variant' => 'variationA',
                        ],
                    ],
                ],
            ],
            'sku_config' => 'SKU00A',
            'mail_hash' => 'n/a',
            'nls' => null,
            'is_international' => 0,
            'ean_code' => '',
            'is_master' => 1,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setUrlKey('bar');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $productSimple = new Simple();
        $productSimple->setOriginalPrice(new Money(100));
        $productSimple->setSku('SKU00A-1');
        $product = new Product();
        $product->setSku('SKU00A');
        $product->setConfigId('A');
        $product->setName('Foobar 2000');
        $product->setBrand($this->getBrandMock());
        $product->setCategory($category1);
        $product->addSimple($productSimple);
        $product->setVariationType('variationA');
        $product->setMarketplaceParentSku('PARENT-SKU');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateProduct($product);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingProductWithMarketPlaceChildren()
    {
        $expectedData = [
            'pageType' => 'product',
            'mobile_app_log' => 0,
            'is_linio_plus' => 0,
            'new_user' => 1,
            'gender' => 'n/a',
            'brand' => 'ABC',
            'sku_simple' => 'SKU00A-1',
            'product_name' => 'Foobar 2000',
            'price' => 100.0,
            'special_price' => 0.0,
            'large_image' => '',
            'category1' => 'foo',
            'category2' => 'bar',
            'categoryKey1' => 'foo',
            'categoryKey2' => 'foo',
            'category_id' => 1,
            'category_full' => 'foo/bar',
            'msrpPrice' => 100.0,
            'product_id' => 'A',
            'small_image' => '',
            'ecommerce' => [
                'detail' => [
                    'products' => [
                        [
                            'name' => 'Foobar 2000',
                            'id' => 'SKU00A',
                            'price' => 100.0,
                            'brand' => 'ABC',
                            'category' => 'null',
                            'variant' => 'variationA',
                        ],
                    ],
                ],
            ],
            'sku_config' => 'SKU00A',
            'mail_hash' => 'n/a',
            'nls' => null,
            'is_international' => 0,
            'ean_code' => '',
            'is_master' => 1,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setUrlKey('bar');

        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');
        $categoryWithTree->addPath($category1);
        $categoryWithTree->addPath($category2);

        $marketplaceChild = new MarketplaceChild();
        $marketplaceChild->setSku('SKU00A-1');
        $marketplaceChild->setPrice(new Money(0));
        $marketplaceChild->setOriginalPrice(new Money(100));

        $productSimple = new Simple();
        $productSimple->setOriginalPrice(new Money(100));
        $productSimple->setSku('SKU00A-1');
        $product = new Product();
        $product->setSku('SKU00A');
        $product->setConfigId('A');
        $product->setName('Foobar 2000');
        $product->setBrand($this->getBrandMock());
        $product->setCategory($category1);
        $product->addSimple($productSimple);
        $product->setVariationType('variationA');
        $product->setMarketplaceChildren([$marketplaceChild]);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($categoryWithTree);

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateProduct($product);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCartWithEmptyDataset()
    {
        $expectedData = [
            'pageType' => 'cart',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 1,
            'sku_simple_list' => [],
            'cartProducts' => [],
            'cartTotalValue' => 0.0,
            'identifier_hash' => 'n/a',
            'mail_hash' => 'n/a',
            'nls' => null,
            'product_id_list' => [],
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku()->willReturn($this->getProductMock('A'));

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setProductService($productService->reveal());
        $dataLayerService->populateCart(new Order('123'));

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCartWithFullDataset()
    {
        $expectedData = [
            'pageType' => 'cart',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 1,
            'sku_simple_list' => ['A-1'],
            'identifier_hash' => 'n/a',
            'mail_hash' => 'n/a',
            'nls' => null,
            'product_id_list' => ['A'],
            'cartProducts' => [
                [
                    'name' => 'Foobar',
                    'sku' => 'A-1',
                    'sku_config' => 'A',
                    'brand' => 'ABC',
                    'price' => 100.0,
                    'quantity' => 1,
                    'category1' => 'foo',
                    'category2' => 'bar',
                    'category_id' => 1,
                    'categoryKey1' => 'foo',
                    'categoryKey2' => 'foo',
                    'msrpPrice' => 100.0,
                    'special_price' => 90.0,
                    'product_id' => 'A',
                    'category' => 'foo/bar',
                    'attrset' => 'attribute',
                    'tax_cost' => 10,
                    'ean_code' => null,
                    'is_master' => 0,
                ],
            ],
            'cartTotalValue' => 0.0,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn(new Customer());
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $brand = new Brand();
        $brand->setName('ABC');

        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setName('category');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setName('category');
        $category2->setUrlKey('bar');

        $product = new Product();
        $product->setBrand($brand);
        $product->setCategory($category1);
        $product->setName('Foobar');
        $product->setConfigId('A');
        $product->setAttributeSet('attribute');

        $item = new Item('A-1', 1, $product);
        $item->setOriginalPrice(new Money(100));
        $item->setUnitPrice(new Money(90));
        $item->setTaxAmount(new Money(10));

        $order = new Order('123');
        $order->addItem($item);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->buildTree(1)->willReturn($this->getCategoryWithTreeMock());

        $productService = $this->prophesize(ProductService::class);
        $productService->getSkuFromSimple('A-1')->willReturn('A');
        $productService->getBySku('A')->willReturn($this->getProductMock('A'));

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setProductService($productService->reveal());
        $dataLayerService->setCategoryService($categoryService->reveal());
        $dataLayerService->populateCart($order);

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingCheckoutWithEmptyDataset()
    {
        $expectedData = [
            'pageType' => 'checkout',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 1,
            'sku_simple_list' => [],
            'checkoutProducts' => [],
            'orderValue' => 0.0,
            'orderShippingValue' => 0.0,
            'mail_hash' => '81df589b1dceacc2fa7c8f536015fcdf854eee721fdf282a91ed9c4b0c54dc76',
            'nls' => null,
            'identifier_hash' => 'n/a',
            'product_id_list' => [],
            'orderProductsValue' => 0.0,
        ];

        $customer = new Customer();
        $customer->setEmail('mail@example.com');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku()->willReturn($this->getProductMock('A'));

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setProductService($productService->reveal());
        $dataLayerService->populateCheckout(new Order('123'));

        $this->assertEquals($expectedData, $dataLayerService->getData());
    }

    public function testIsPopulatingSuccessWithEmptyDataset()
    {
        $expectedData = [
            'pageType' => 'success',
            'mobile_app_log' => 0,
            'gender' => 'n/a',
            'is_linio_plus' => 0,
            'new_user' => 1,
            'sku_simple_list' => [],
            'transactionProducts' => [],
            'orderValue' => 0.0,
            'orderShippingValue' => 0.0,
            'transactionDate' => date('d/m/Y H:i:s'),
            'transactionPaymentType' => null,
            'transactionShippingMethod' => null,
            'transactionShippingValue' => 0.0,
            'transactionCurrency' => 'USD',
            'order_number' => null,
            'transactionId' => null,
            'transactionAffiliation' => 'linio',
            'transactionTotal' => 0.0,
            'transactionShipping' => 0.0,
            'transactionTax' => 0.0,
            'mail_hash' => '81df589b1dceacc2fa7c8f536015fcdf854eee721fdf282a91ed9c4b0c54dc76',
            'nls' => null,
        ];

        $token = $this->prophesize(TokenInterface::class);
        $address = new Address();
        $address->setId(1);
        $address->setPostcode('12345');
        $customer = new Customer();
        $customer->setShippingAddress($address);
        $customer->setEmail('mail@example.com');
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $session = $this->prophesize(SessionInterface::class);

        $moneyFormatter = new MoneyFormatter('USD', '$', '.', ',', 2);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku()->willReturn($this->getProductMock('A'));

        $dataLayerService = new DataLayerService();
        $dataLayerService->setTokenStorage($tokenStorage->reveal());
        $dataLayerService->setSession($session->reveal());
        $dataLayerService->setMoneyFormatter($moneyFormatter);
        $dataLayerService->setProductService($productService->reveal());

        $completedOrder = new CompletedOrder('123');
        $completedOrder->setCustomer($customer);

        $dataLayerService->populateSuccess($completedOrder);

        $resultData = $dataLayerService->getData();
        $this->assertEquals($expectedData['pageType'], $resultData['pageType']);
        $this->assertEquals($expectedData['mobile_app_log'], $resultData['mobile_app_log']);
        $this->assertEquals($expectedData['gender'], $resultData['gender']);
        $this->assertEquals($expectedData['is_linio_plus'], $resultData['is_linio_plus']);
        $this->assertEquals($expectedData['new_user'], $resultData['new_user']);
        $this->assertEquals($expectedData['sku_simple_list'], $resultData['sku_simple_list']);
        $this->assertEquals($expectedData['transactionProducts'], $resultData['transactionProducts']);

        $date = DateTime::createFromFormat('d/m/Y H:i:s', $resultData['transactionDate']);
        $this->assertInstanceOf(DateTime::class, $date);

        $this->assertEquals($expectedData['transactionPaymentType'], $resultData['transactionPaymentType']);
        $this->assertEquals($expectedData['transactionShippingMethod'], $resultData['transactionShippingMethod']);
        $this->assertEquals($expectedData['transactionShippingValue'], $resultData['transactionShippingValue']);
        $this->assertEquals($expectedData['transactionCurrency'], $resultData['transactionCurrency']);
        $this->assertEquals($expectedData['order_number'], $resultData['order_number']);
        $this->assertEquals($expectedData['transactionId'], $resultData['transactionId']);
        $this->assertEquals($expectedData['transactionAffiliation'], $resultData['transactionAffiliation']);
        $this->assertEquals($expectedData['transactionTotal'], $resultData['transactionTotal']);
        $this->assertEquals($expectedData['transactionShipping'], $resultData['transactionShipping']);
        $this->assertEquals($expectedData['transactionTax'], $resultData['transactionTax']);
        $this->assertEquals($expectedData['mail_hash'], $resultData['mail_hash']);
    }

    protected function getProductMock($name)
    {
        $brand = $this->getBrandMock();
        $categories = $this->getCategoriesMock();
        $simple = $this->getSimpleMock($name);

        $product = new Product();
        $product->setSku($name);
        $product->setName($name . ' ' . rand(1000, 2000));
        $product->setBrand($brand);
        $product->setCategory($categories[0]);
        $product->addSimple($simple);
        $product->setPrice(new Money(90));

        return $product;
    }

    protected function getCategoriesMock()
    {
        $category1 = new Category();
        $category1->setId(1);
        $category1->setSlug('foo');
        $category1->setUrlKey('foo');

        $category2 = new Category();
        $category2->setId(2);
        $category2->setSlug('bar');
        $category2->setUrlKey('bar');

        return [$category1, $category2];
    }

    protected function getBrandMock()
    {
        $brand = new Brand();
        $brand->setId(1);
        $brand->setName('ABC');
        $brand->setSlug('abc');

        return $brand;
    }

    protected function getSimpleMock($name)
    {
        $productSimple = new Simple();
        $productSimple->setPrice(new Money(90));
        $productSimple->setOriginalPrice(new Money(100));
        $productSimple->setSku($name . '-1');

        return $productSimple;
    }

    protected function getCategoryWithTreeMock()
    {
        $categoryWithTree = new Category();
        $categoryWithTree->setSlug('foo');

        $categories = $this->getCategoriesMock();

        foreach ($categories as $category) {
            $categoryWithTree->addPath($category);
        }

        return $categoryWithTree;
    }
}
