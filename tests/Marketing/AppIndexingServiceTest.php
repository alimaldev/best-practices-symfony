<?php

namespace Linio\Frontend\Marketing;

use Linio\Component\Cache\CacheService;

class AppIndexingServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsPopulatingProduct()
    {
        $expectedData = [
            'android' => 'android-app://com.android/linio/mx/d/LINIOA2000?adjust_tracker=android&adjust_campaign=AppIndexing',
            'ios' => 'ios-app://1500/linio/mx/d/LINIOA2000?adjust_tracker=ios&adjust_campaign=AppIndexing',
        ];

        $appIndexingService = new AppIndexingService($this->getCacheService());
        $appIndexingService->setCountryCode('mx');
        $result = $appIndexingService->getProductData('LINIOA2000');

        $this->assertEquals($expectedData, $result);
    }

    public function testIsPopulatingCategory()
    {
        $expectedData = [
            'android' => 'android-app://com.android/linio/mx/c/category-slug?adjust_tracker=android&adjust_campaign=AppIndexing',
            'ios' => 'ios-app://1500/linio/mx/c/category-slug?adjust_tracker=ios&adjust_campaign=AppIndexing',
        ];

        $appIndexingService = new AppIndexingService($this->getCacheService());
        $appIndexingService->setCountryCode('mx');
        $result = $appIndexingService->getCategoryData('category-slug');

        $this->assertEquals($expectedData, $result);
    }

    /**
     * @return CacheService
     */
    protected function getCacheService()
    {
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('mobile')
            ->willReturn(['android_adjust_tracker' => 'android',
                'android_package_id' => 'com.android',
                'ios_adjust_tracker' => 'ios',
                'ios_itunes_id' => '1500', ]);

        return $cacheService->reveal();
    }
}
