<?php

declare(strict_types=1);

namespace Store;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Store\MobileConfigurationService;
use Prophecy\Argument;
use Symfony\Component\Translation\TranslatorInterface;

class MobileConfigurationServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testConfigurationIsLoadedAndMergedRecursively()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->get('store')->willReturn(null);
        $cache->set('store', Argument::type('array'));

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans(Argument::type('string'))->willReturn('Is translated.');

        $fixturesDir = realpath(dirname(__DIR__) . '/fixtures/mobile');
        $service = new MobileConfigurationService($fixturesDir . '/MobileCountryConfig.yml',
            $fixturesDir . '/MobilePaymentConfig.yml');
        $service->setCacheService($cache->reveal());
        $service->setTranslator($translator->reveal());
        $service->setMobileApiMinimumVersion(1);

        $config = $service->getConfigParsedData('ar');

        $this->assertNotEmpty($config);
        $this->assertFalse($config['dynamic_form']['CreditCard']['save']['enabled']);
    }

    public function testConfigurationIsLoadedFromCache()
    {
        $expected = ['config' => 'cache'];
        $cache = $this->prophesize(CacheService::class);
        $cache->get('store')->willReturn($expected);
        $cache->set()->shouldNotBeCalled();

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans()->shouldNotBeCalled();

        $service = new MobileConfigurationService('', '');
        $service->setCacheService($cache->reveal());
        $service->setTranslator($translator->reveal());
        $service->setMobileApiMinimumVersion(1);

        $config = $service->getConfigParsedData('ar');

        $this->assertSame($expected, $config);
    }
}
