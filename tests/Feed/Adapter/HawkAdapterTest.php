<?php

namespace Linio\Frontend\Feed\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\Recommendation\RecommendationResult;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Feed\ResultSetMapper\RecommendationMapper;
use Linio\Frontend\Product\ProductService;
use Linio\Test\UnitTestCase;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class HawkAdapterTest extends UnitTestCase
{
    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../../fixtures/feed/results.yml');
    }

    /**
     * @return string
     */
    protected function getSimilarProductsContent()
    {
        return <<<'EOD'
{
    "items": [
        {
            "title": "Smartwatch H8 Pro Compatible Con Iphone Y Android Bluetooth Rojo",
            "brand": {
                "name": "Faromala"
            },
            "id": "FA131EL17279OLMX-1708250",
            "group_id": "FA131EL17279OLMX",
            "price": {
                "current": 1199
            },
            "url": "www.linio.com.mx/SmartWatch-H8-Pro-Compatible-Con-Iphone-Y-Android-Bluetooth-Rojo-2262327.html",
            "url_referrer": "http://www.linio.com.mx/celulares-y-tablets/smartwatches/?sort=popularity&dir=desc",
            "images": [
                "http://media.linio.com.mx/p/faromala-2767-7232622-1-zoom.jpg"
            ],
            "color": "Negro/ Blanco/ Rojo",
            "categories": [
                "smartwatches"
            ],
            "main_category": "smartwatches",
            "sku": "FA131EL17279OLMX-1708250",
            "skuConfig": "FA131EL17279OLMX"
        },
        {
            "title": "Smartwatch H8 Pro Compatible Con Android Bluetooth Negro",
            "brand": {
                "name": "Faromala"
            },
            "id": "FA131EL17279OLMX-1708251",
            "group_id": "FA131EL172791LMX",
            "price": {
                "previous": 1500,
                "current": 1199
            },
            "url": "www.linio.com.mx/SmartWatch-H8-Pro-Compatible-Con-Android-Bluetooth-Negro-2262327.html",
            "url_referrer": "http://www.linio.com.mx/celulares-y-tablets/smartwatches/?sort=popularity&dir=desc",
            "images": [
                "http://media.linio.com.mx/p/faromala-7232622-1-product.jpg"
            ],
            "color": "Negro/ Blanco/ Rojo",
            "categories": [
                "smartwatches"
            ],
            "main_category": "smartwatches",
            "sku": "FA131EL17279OLMX-1708250",
            "skuConfig": "FA131EL17279OLMX"
        }
    ]
}
EOD;
    }

    public function testIsFindingSimilarProducts()
    {
        $expected = $this->fixtures['recommendation_result_1'];

        $product = new Product();
        $product->setSlug('product-slug');

        $requestMock = $this->prophesize(Request::class);
        $requestMock->getScheme()
            ->willReturn('http');

        $requestStackMock = $this->prophesize(RequestStack::class);
        $requestStackMock->getCurrentRequest()
            ->willReturn($requestMock->reveal());

        $bodyMock = $this->prophesize(StreamInterface::class);
        $bodyMock->getContents()->willReturn($this->getSimilarProductsContent());

        $responseMock = $this->prophesize(Response::class);
        $responseMock->getBody()->willReturn($bodyMock->reveal());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);

        $clientMock->get('morelikethis', ['query' => 'key=&method=default&sku=FA131EL28BVBLMX-1597126', 'timeout' => 3])
            ->willReturn($responseMock->reveal());

        $productServiceMock = $this->prophesize(ProductService::class);
        $productServiceMock->getBySku('FA131EL17279OLMX-1708250')
            ->willReturn($product);

        $recommendationMapper = new RecommendationMapper();
        $recommendationMapper->setRequestStack($requestStackMock->reveal());

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());
        $hawkAdapter->setRecommendationMapper($recommendationMapper);
        $actual = $hawkAdapter->findSimilar('FA131EL28BVBLMX-1597126');

        $this->assertEquals($expected, $actual);
    }

    public function testIsDetectingErrorWhenGetsSimilarProducts()
    {
        $expected = new RecommendationResult();

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);
        $clientMock->get('morelikethis', ['query' => 'key=&method=default&sku=FA131EL28BVBLMX-1597126', 'timeout' => 3])->willThrow(ServerException::class);

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());

        $actual = $hawkAdapter->findSimilar('FA131EL28BVBLMX-1597126');

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindingoverallBestSellers()
    {
        $expected = $this->fixtures['recommendation_result_1'];

        $requestMock = $this->prophesize(Request::class);
        $requestMock->getScheme()
            ->willReturn('http');

        $requestStackMock = $this->prophesize(RequestStack::class);
        $requestStackMock->getCurrentRequest()
            ->willReturn($requestMock->reveal());

        $bodyMock = $this->prophesize(StreamInterface::class);
        $bodyMock->getContents()->willReturn($this->getSimilarProductsContent());

        $responseMock = $this->prophesize(Response::class);
        $responseMock->getBody()->willReturn($bodyMock->reveal());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);

        $clientMock->get('bestsellers', ['query' => 'key=&method=default&from=now-1d&size=10&user_id=fd8sdfx&web_id=fd8sdfx', 'timeout' => 3])
            ->willReturn($responseMock->reveal());

        $recommendationMapper = new RecommendationMapper();
        $recommendationMapper->setRequestStack($requestStackMock->reveal());

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());
        $hawkAdapter->setRecommendationMapper($recommendationMapper);
        $hawkAdapter->setTokenStorage($tokenStorage->reveal());
        $actual = $hawkAdapter->findOverallBestSellers();

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindingBestSellersByCategory()
    {
        $expected = $this->fixtures['recommendation_result_1'];

        $category = new Category();
        $category->setName('sample-category');

        $requestMock = $this->prophesize(Request::class);
        $requestMock->getScheme()
            ->willReturn('http');

        $requestStackMock = $this->prophesize(RequestStack::class);
        $requestStackMock->getCurrentRequest()
            ->willReturn($requestMock->reveal());

        $bodyMock = $this->prophesize(StreamInterface::class);
        $bodyMock->getContents()->willReturn($this->getSimilarProductsContent());

        $responseMock = $this->prophesize(Response::class);
        $responseMock->getBody()->willReturn($bodyMock->reveal());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);

        $clientMock->get('bestsellers', ['query' => 'key=&method=default&from=now-1d&size=10&category=sample-category&user_id=fd8sdfx&web_id=fd8sdfx', 'timeout' => 3])
            ->willReturn($responseMock->reveal());

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $recommendationMapper = new RecommendationMapper();
        $recommendationMapper->setRequestStack($requestStackMock->reveal());

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());
        $hawkAdapter->setRecommendationMapper($recommendationMapper);
        $hawkAdapter->setTokenStorage($tokenStorage->reveal());
        $actual = $hawkAdapter->findBestSellersByCategory($category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSendingPersonalisationCustomerDataWhenFindsBestSellers()
    {
        $expected = $this->fixtures['recommendation_result_1'];

        $category = new Category();
        $category->setName('sample-category');

        $requestMock = $this->prophesize(Request::class);
        $requestMock->getScheme()
            ->willReturn('http');

        $requestStackMock = $this->prophesize(RequestStack::class);
        $requestStackMock->getCurrentRequest()
            ->willReturn($requestMock->reveal());

        $bodyMock = $this->prophesize(StreamInterface::class);
        $bodyMock->getContents()->willReturn($this->getSimilarProductsContent());

        $responseMock = $this->prophesize(Response::class);
        $responseMock->getBody()->willReturn($bodyMock->reveal());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);

        $clientMock->get('bestsellers', ['query' => 'key=&method=default&from=now-1d&size=10&category=sample-category&user_id=fd8sdfx&web_id=fd8sdfx', 'timeout' => 3])
            ->willReturn($responseMock->reveal());

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $requestStack = $this->prophesize(RequestStack::class);
        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $recommendationMapper = new RecommendationMapper();
        $recommendationMapper->setRequestStack($requestStackMock->reveal());

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());
        $hawkAdapter->setRecommendationMapper($recommendationMapper);
        $hawkAdapter->setTokenStorage($tokenStorage->reveal());
        $actual = $hawkAdapter->findBestSellersByCategory($category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindingoverallMostViewed()
    {
        $expected = $this->fixtures['recommendation_result_1'];

        $requestMock = $this->prophesize(Request::class);
        $requestMock->getScheme()
            ->willReturn('http');

        $requestStackMock = $this->prophesize(RequestStack::class);
        $requestStackMock->getCurrentRequest()
            ->willReturn($requestMock->reveal());

        $bodyMock = $this->prophesize(StreamInterface::class);
        $bodyMock->getContents()->willReturn($this->getSimilarProductsContent());

        $responseMock = $this->prophesize(Response::class);
        $responseMock->getBody()->willReturn($bodyMock->reveal());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);

        $clientMock->get('mostviewed', ['query' => 'key=&method=default&from=now-1d&size=10', 'timeout' => 3])
            ->willReturn($responseMock->reveal());

        $recommendationMapper = new RecommendationMapper();
        $recommendationMapper->setRequestStack($requestStackMock->reveal());

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());
        $hawkAdapter->setRecommendationMapper($recommendationMapper);
        $actual = $hawkAdapter->findOverallMostViewed();

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindingMostViewedByCategory()
    {
        $expected = $this->fixtures['recommendation_result_1'];

        $requestMock = $this->prophesize(Request::class);
        $requestMock->getScheme()
            ->willReturn('http');

        $requestStackMock = $this->prophesize(RequestStack::class);
        $requestStackMock->getCurrentRequest()
            ->willReturn($requestMock->reveal());

        $bodyMock = $this->prophesize(StreamInterface::class);
        $bodyMock->getContents()->willReturn($this->getSimilarProductsContent());

        $responseMock = $this->prophesize(Response::class);
        $responseMock->getBody()->willReturn($bodyMock->reveal());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')->willReturn([]);

        $clientMock->get('mostviewed', ['query' => 'key=&method=default&from=now-1d&size=10&category=sample-category', 'timeout' => 3])
            ->willReturn($responseMock->reveal());

        $recommendationMapper = new RecommendationMapper();
        $recommendationMapper->setRequestStack($requestStackMock->reveal());

        $hawkAdapter = new HawkAdapter();
        $hawkAdapter->setClient($clientMock->reveal());
        $hawkAdapter->setRecommendationMapper($recommendationMapper);
        $actual = $hawkAdapter->findMostViewedByCategory('sample-category');

        $this->assertEquals($expected, $actual);
    }
}
