<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Communication\Resolve;

use Linio\Component\Cache\CacheService;
use Linio\Test\UnitTestCase;
use Symfony\Component\HttpFoundation\ParameterBag;

class LegacyTest extends UnitTestCase
{
    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../../../fixtures/address/legacy.yml');
    }

    private function getPostcodeRawData()
    {
        return [
            [
                'country' => 'Country 1',
                'region' => 'Region 1-1',
                'municipality' => 'Municipality 1-1-1',
                'city' => 'City 1-1-1-1',
                'postcode' => '1111',
            ],
            [
                'country' => 'Country 1',
                'region' => 'Region 1-1',
                'municipality' => 'Municipality 1-1-2',
                'city' => 'City 1-1-2-1',
                'postcode' => '1121',
            ],
            [
                'country' => 'Country 1',
                'region' => 'Region 1-2',
                'municipality' => 'Municipality 1-2-3',
                'city' => 'City 1-2-3-1',
                'postcode' => '1231',
            ],
            [
                'country' => 'Country 2',
                'region' => 'Region 2-1',
                'municipality' => 'Municipality 2-1-1',
                'city' => 'City 2-1-1-1',
                'postcode' => '2111',
            ],
        ];
    }

    public function testIsResolvingPostcode()
    {
        $expected = [
            'region' => 'Region 1-1',
            'municipality' => 'Municipality 1-1-2',
            'city' => 'City 1-1-2-1',
            'neighborhoods' => [],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->resolvePostcode('1121');

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingNullOnInvalidPostcode()
    {
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->resolvePostcode('0000');

        $this->assertEquals([], $actual);
    }

    public function testIsGettingCountries()
    {
        $expected = [
            $this->fixtures['country_1'],
            $this->fixtures['country_2'],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->getCountries();

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingAllRegions()
    {
        $expected = [
            $this->fixtures['region_1_1'],
            $this->fixtures['region_1_2'],
            $this->fixtures['region_2_1'],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->getRegions(new ParameterBag());

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingRegionsFromAnSpecificCountry()
    {
        $expected = [
            $this->fixtures['region_1_1'],
            $this->fixtures['region_1_2'],
        ];

        $data = new ParameterBag(['country' => 'Country 1']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->getRegions($data);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingRegionsFromAnSpecificCountryWhenCountryIsNullInCache()
    {
        $expected = [
            $this->fixtures['region_1_1'],
        ];

        $data = new ParameterBag(['country' => 'Country 1']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn([
                [
                    'country' => null,
                    'region' => 'Region 1-1',
                    'municipality' => 'Municipality 1-1-1',
                    'city' => 'City 1-1-1-1',
                    'postcode' => '1111',
                ],
            ]);

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->getRegions($data);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingMunicipalities()
    {
        $expected = [
            $this->fixtures['municipality_1_1_1'],
            $this->fixtures['municipality_1_1_2'],
        ];

        $data = new ParameterBag(['region' => 'Region 1-1']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->getMunicipalities($data);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingCities()
    {
        $expected = [$this->fixtures['city_1_1_2_1']];

        $data = new ParameterBag(['municipality' => 'Municipality 1-1-2']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Legacy::CACHE_KEY)
            ->shouldBeCalled()
            ->willReturn($this->getPostcodeRawData());

        $legacyAdapter = new Legacy();
        $legacyAdapter->setCacheService($cacheService->reveal());

        $actual = $legacyAdapter->getCities($data);

        $this->assertEquals($expected, $actual);
    }
}
