<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Communication\Resolve;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use Psr\Http\Message\ResponseInterface;

class Bob4AliceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsResolvingPostcode()
    {
        $expectedLocation = [
            'region' => 42,
            'municipality' => 42,
            'city' => 42,
            'neighborhoods' => [],
        ];

        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn('{"region": 42, "municipality": 42, "city": 42}');

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/addressing/postcode/resolve', ['json' => ['postcode' => '123456']])->willReturn($response->reveal());

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());
        $this->assertEquals($expectedLocation, $adapter->resolvePostcode('123456'));
    }

    public function testIsNotResolvingPostcodeDueToBadRequest()
    {
        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn('{"code": "foobar"}');

        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn($response->reveal());

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/addressing/postcode/resolve', ['json' => ['postcode' => '123456']])->willThrow($exception->reveal());

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('foobar');

        $adapter->resolvePostcode('123456');
    }

    public function testIsNotResolvingPostcodeDueToServerIssue()
    {
        $response = $this->prophesize(ResponseInterface::class);
        $response->getBody()->willReturn('{"code": "barfoo"}');

        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn($response->reveal());

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/addressing/postcode/resolve', ['json' => ['postcode' => '123456']])->willThrow($exception->reveal());

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('barfoo');

        $adapter->resolvePostcode('123456');
    }
}
