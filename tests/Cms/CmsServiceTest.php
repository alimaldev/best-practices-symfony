<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms;

use Carbon\Carbon;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Cms\ContentType\MenuMapper;
use Linio\Frontend\Cms\ContentType\Mobile\MenuMapper as MobileMenuMapper;
use Linio\Frontend\Cms\ContentType\Mobile\UrlMapper;
use Linio\Frontend\Entity\Catalog\Category;
use PHPUnit_Framework_TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Routing\RouterInterface;

class CmsServiceTest extends PHPUnit_Framework_TestCase
{
    public function testIsReturningSimpleImageContent()
    {
        $imageContent = ['src' => 'src', 'alt_attr' => 'alt', 'title' => 'title'];
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:home:1')->willReturn($imageContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->simpleImage('home', 1, 'desktop');

        $this->assertEquals($actual, $imageContent);
    }

    public function testIsReturningSimpleTextContent()
    {
        $textContent = ['text' => 'text'];
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:global:1')->willReturn($textContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->simpleText('global', '1', 'desktop');

        $this->assertEquals($actual, $textContent);
    }

    public function testIsReturningMultilineTextContent()
    {
        $multilineTextContent = ['text' => 'text'];
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:global:1')->willReturn($multilineTextContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->simpleText('global', '1', 'desktop');

        $this->assertEquals($actual, $multilineTextContent);
    }

    public function testIsReturningBannerContent()
    {
        $bannerContent = [
            'href' => 'href',
            'image' => [
                'src' => 'src',
                'alt_attr' => 'alt',
                'title' => 'title',
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:banner:home:1')->willReturn($bannerContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->banner('home', 1, 'desktop');

        $this->assertEquals($actual, $bannerContent);
    }

    public function testIsReturningCurrentPlacementContent()
    {
        $placementContent = [
            [
                'href' => 'href',
                'image' => [
                    'alt_attr' => 'test1',
                    'src' => 'https',
                    'title_attr' => 'test1',
                ],
                'revision_number' => 1,
                'start_date' => '2015-05-19 19:18',
                'title' => 'Test1',
                'type' => 'customized',
            ],
            [
                'href' => 'href',
                'image' => [
                    'alt_attr' => 'test2',
                    'src' => 'https',
                    'title_attr' => 'test2',
                ],
                'revision_number' => 1,
                'start_date' => '2016-01-13 16:30',
                'title' => 'Test2',
                'type' => 'customized',
            ],
            [
                'href' => 'href',
                'image' => [
                    'alt_attr' => 'test3',
                    'src' => 'https',
                    'title_attr' => 'test3',
                ],
                'revision_number' => 1,
                'start_date' => '2016-05-23 16:30',
                'title' => 'Test3',
                'type' => 'customized',
            ],
        ];

        $timezone = 'UTC';

        $expected = $placementContent[2];
        $expected['start_date'] = new Carbon($expected['start_date'], $timezone);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->getNamespace()->willReturn('mx:cms');
        $cacheService->get('desktop:placement:global:test')->willReturn($placementContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        /* TODO: Check the comment in placement function from CmsService */
        $actual = $service->placement('mx:cms:desktop:placement:global:test');

        $this->assertEquals($expected, $actual);
    }

    public function testIsReturningEmptyPlacementWhenContentDoesNotExists()
    {
        $expected = [];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->getNamespace()->willReturn('mx:cms');
        $cacheService->get('desktop:placement:global:test')->willReturn(null);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->placement('mx:cms:desktop:placement:global:test');

        $this->assertEquals($expected, $actual);
    }

    public function testIsReturningDfaContent()
    {
        $dfaContent = [
            'target_url' => 'target',
            'image_url' => 'image_url',
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:dfa:home:1')->willReturn($dfaContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->dfa('home', 1, 'desktop');

        $this->assertEquals($actual, $dfaContent);
    }

    public function testIsReturningMarkdownContent()
    {
        $markdownContent = [
            'text' => 'markdown_text',
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:markdown_text:global:1')->willReturn($markdownContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->markdownText('global', '1', 'desktop');

        $this->assertEquals($actual, $markdownContent);
    }

    public function testIsReturningMenuContent()
    {
        $menuContent = [
            'type' => 'customized',
            'attributes' => [
                'label' => 'label',
                'href' => 'href',
            ],
            'children' => [],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:menu:global:1')->willReturn($menuContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->menu('global', '1', 'desktop');

        $this->assertEquals($actual, $menuContent);
    }

    public function testIsReturningGridContent()
    {
        $gridContent = [
            'title' => 'grid title',
            'cells' => [
                [
                    'content_type' => 'menu',
                    'cols' => '10',
                    'data' => [
                        'type' => 'customized',
                        'attributes' => [
                            'label' => 'label',
                            'href' => 'href',
                        ],
                        'children' => [],
                    ],
                ],
                [
                    'content_type' => 'dfa',
                    'cols' => '10',
                    'data' => [
                        'target_url' => 'target',
                        'image_url' => 'image_url',
                    ],
                ],
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:grid:home:row-1')->willReturn($gridContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->grid('home', 1, 'desktop');

        $this->assertEquals($actual, $gridContent);
    }

    public function testIsReturningCustomGridContent()
    {
        $gridContent = [
            'title' => 'Custom grid content',
            'cells' => [
                [
                    'content_type' => 'menu',
                    'cols' => '10',
                    'data' => [
                        'type' => 'customized',
                        'attributes' => [
                            'label' => 'label',
                            'href' => 'href',
                        ],
                        'children' => [],
                    ],
                ],
                [
                    'content_type' => 'dfa',
                    'cols' => '10',
                    'data' => [
                        'target_url' => 'target',
                        'image_url' => 'image_url',
                    ],
                ],
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:main_menu_grid:home:row-1')->willReturn($gridContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->mainMenuCustomGrid('1');

        $this->assertEquals($actual, $gridContent);
    }

    public function testIsReturningCategoryGridContent()
    {
        $gridContent = [
            'title' => 'Custom grid content',
            'cells' => [
                [
                    'content_type' => 'menu',
                    'cols' => '10',
                    'data' => [
                        'type' => 'customized',
                        'attributes' => [
                            'label' => 'label',
                            'href' => 'href',
                        ],
                        'children' => [],
                    ],
                ],
                [
                    'content_type' => 'dfa',
                    'cols' => '10',
                    'data' => [
                        'target_url' => 'target',
                        'image_url' => 'image_url',
                    ],
                ],
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:main_menu_grid:category:1928')->willReturn($gridContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->mainMenuCategoryGrid(1928);

        $this->assertEquals($actual, $gridContent);
    }

    public function testIsReturningCarouselContent()
    {
        $carouselContent = [
            'title' => 'carousel title',
            'elements' => [
                [
                    'content_type' => 'banner',
                    'cols' => '10',
                    'data' => [
                        'href' => 'href',
                        'image' => [
                            'src' => 'src',
                            'alt_attr' => 'alt',
                            'title' => 'title',
                        ],
                    ],
                ],
                [
                    'content_type' => 'dfa',
                    'cols' => '10',
                    'data' => [
                        'target_url' => 'target',
                        'image_url' => 'image_url',
                    ],
                ],
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:carousel:home:row-1:3')->willReturn($carouselContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->carousel('home', '1', 'desktop', 3);

        $this->assertEquals($actual, $carouselContent);
    }

    /**
     * @return array
     */
    public function pageContentProvider(): array
    {
        return [
            [[]],
            [
                [
                    'seo' => 'seo',
                    'css_text' => 'css',
                    'page_content' => [
                        [
                            'content_type' => 'menu',
                            'cols' => '10',
                            'data' => [
                                'type' => 'customized',
                                'attributes' => [
                                    'label' => 'label',
                                    'href' => 'href',
                                ],
                                'children' => [],
                            ],
                        ],
                        [
                            'content_type' => 'dfa',
                            'cols' => '10',
                            'data' => [
                                'target_url' => 'target',
                                'image_url' => 'image_url',
                            ],
                        ],
                    ],
                    'js_text' => 'js',
                ],
            ],
        ];
    }

    /**
     * @dataProvider pageContentProvider
     *
     * @param array $expected
     */
    public function testIsReturningStaticPageContent(array $expected)
    {
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:static_page:home:sp1')->willReturn($expected);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->staticPage('sp1', 'home');

        $this->assertEquals($actual, $expected);
    }

    public function testIsReturningGlobalContent()
    {
        $globalContent = [
            'text' => 'global text',
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:markdown_text:global:1')->willReturn($globalContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->getGlobal('1');

        $this->assertEquals($actual, $globalContent);
    }

    public function testIsReturningIconMainMenuContent()
    {
        $iconsMenuContent = [
            [
                'type' => 'customized',
                'attributes' => [
                    'icon' => '¨',
                    'label' => 'label',
                    'href' => 'href',
                    'main_menu_grid_slug' => '1',
                ],
                'children' => [],
            ],
            [
                'type' => 'category',
                'attributes' => [
                    'icon' => '|',
                    'label' => 'label',
                    'category_id' => 232,
                ],
                'children' => [],
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:icons_menu:global')->willReturn($iconsMenuContent);

        $service = new CmsService();
        $service->setCacheService($cacheService->reveal());

        $actual = $service->getMainMenuFromCache();

        $this->assertEquals($actual, $iconsMenuContent);
    }

    public function testIsGettingAutomaticRecommendations()
    {
        $recommendationsContent = [
            'rules' => [],
            'title' => 'Automatic Recommendations',
            'type' => 'automatic',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get('desktop:recommendations:home:row-1')
            ->willReturn($recommendationsContent);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getRecommendationsFromCache('home', 1);

        $this->assertEquals($recommendationsContent['rules'], []);
        $this->assertEquals($recommendationsContent['type'], 'automatic');
    }

    public function testIsGettingManualCategoryRecommendations()
    {
        $categoryRecommendationsContent = [
            'rules' => [
                [
                    'criteria' => 1809,
                    'type' => 'category',
                ],
            ],
            'title' => 'Manual Recommendations',
            'type' => 'manual',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get('desktop:recommendations:home:row-1')
            ->willReturn($categoryRecommendationsContent);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getRecommendationsFromCache('home', 1);

        $this->assertNotEmpty($categoryRecommendationsContent['rules']);
        $this->assertInternalType('array', $categoryRecommendationsContent['rules']);
        $this->assertInternalType('int', $categoryRecommendationsContent['rules'][0]['criteria']);
        $this->assertEquals($categoryRecommendationsContent['rules'][0]['type'], 'category');
        $this->assertEquals($categoryRecommendationsContent['type'], 'manual');
    }

    public function testIsGettingManualSkuRecommendations()
    {
        $manualRecommendationsContent = [
            'rules' => [
                [
                    'criteria' => 'SM791FA0XNNGKLMX',
                    'type' => 'sku',
                ],
            ],
            'title' => 'Manual Recommendations',
            'type' => 'manual',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get('desktop:recommendations:home:row-1')
            ->willReturn($manualRecommendationsContent);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getRecommendationsFromCache('home', 1);

        $this->assertNotEmpty($manualRecommendationsContent['rules']);
        $this->assertInternalType('array', $manualRecommendationsContent['rules']);
        $this->assertInternalType('string', $manualRecommendationsContent['rules'][0]['criteria']);
        $this->assertEquals($manualRecommendationsContent['rules'][0]['type'], 'sku');
        $this->assertEquals($manualRecommendationsContent['type'], 'manual');
    }

    public function testIsGettingEmptyArrayWhenRecommendationsDoesntExist()
    {
        $expected = [];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get('desktop:recommendations:home:row-118872')
            ->willReturn($expected);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getRecommendationsFromCache('home', 118872);

        $this->assertInternalType('array', $expected);
        $this->assertCount(0, $expected);
    }

    public function testIsGettingCategoryGrids()
    {
        $categoryGridsContent = [
            'cells' => [
                [
                    'cols' => 36,
                    'content_type' => 'banner',
                    'data' => [
                        'href' => 'https://beta.linio.com.mx/cm/paypal-400',
                        'image' => [
                            'alt_attr' => 'Paypal 400',
                            'src' => 'https://s3.amazonaws.com/staging-shop-front/0103fc96-840c-11e5-856c-bc764e104ad4.jpeg',
                            'title_attr' => 'Paypal 400',
                        ],
                        'type' => 'customized',
                    ],
                ],
            ],
            'revision_number' => 2,
            'title' => '',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);

        $service = new CmsService();
        $service->setCacheService($cacheServiceMock->reveal());
        $service->getCatalogGrids('category', '96');

        for ($i = 1; $i <= CmsService::CATALOG_MAX_GRID_ROWS; $i++) {
            $key = sprintf('desktop:grid:category:row-%d:96', $i);
            $cacheServiceMock->get($key)
                ->willReturn($categoryGridsContent)
                ->shouldBeCalled();
        }
    }

    public function testIsGettingCampaignGrids()
    {
        $campaignsGridContent = [
            'cells' => [
                [
                    'cols' => 36,
                    'content_type' => 'banner',
                    'data' => [
                        'href' => 'https://beta.linio.com.mx/cm/paypal-400',
                        'image' => [
                            'alt_attr' => 'Paypal 400',
                            'src' => 'https://s3.amazonaws.com/staging-shop-front/0103fc96-840c-11e5-856c-bc764e104ad4.jpeg',
                            'title_attr' => 'Paypal 400',
                        ],
                        'type' => 'customized',
                    ],
                ],
            ],
            'revision_number' => 2,
            'title' => '',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getCatalogGrids('campaign', '96');

        for ($i = 1; $i <= CmsService::CATALOG_MAX_GRID_ROWS; $i++) {
            $key = sprintf('desktop:grid:campaign:row-%d:96', $i);
            $cacheServiceMock->get($key)
                ->willReturn($campaignsGridContent)
                ->shouldBeCalled();
        }
    }

    public function testIsGettingBrandGrids()
    {
        $brandsGridContent = [
            'cells' => [
                [
                    'cols' => 36,
                    'content_type' => 'banner',
                    'data' => [
                        'href' => 'https://beta.linio.com.mx/cm/paypal-400',
                        'image' => [
                            'alt_attr' => 'Paypal 400',
                            'src' => 'https://s3.amazonaws.com/staging-shop-front/0103fc96-840c-11e5-856c-bc764e104ad4.jpeg',
                            'title_attr' => 'Paypal 400',
                        ],
                        'type' => 'customized',
                    ],
                ],
            ],
            'revision_number' => 2,
            'title' => '',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getCatalogGrids('brand', '96');

        for ($i = 1; $i <= CmsService::CATALOG_MAX_GRID_ROWS; $i++) {
            $key = sprintf('desktop:grid:brand:row-%d:96', $i);
            $cacheServiceMock->get($key)
                ->willReturn($brandsGridContent)
                ->shouldBeCalled();
        }
    }

    public function testIsGettingSupplierGrids()
    {
        $suppliersGridContent = [
            'cells' => [
                [
                    'cols' => 36,
                    'content_type' => 'banner',
                    'data' => [
                        'href' => 'https://beta.linio.com.mx/cm/paypal-400',
                        'image' => [
                            'alt_attr' => 'Paypal 400',
                            'src' => 'https://s3.amazonaws.com/staging-shop-front/0103fc96-840c-11e5-856c-bc764e104ad4.jpeg',
                            'title_attr' => 'Paypal 400',
                        ],
                        'type' => 'customized',
                    ],
                ],
            ],
            'revision_number' => 2,
            'title' => '',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->getCatalogGrids('supplier', '96');

        for ($i = 1; $i <= CmsService::CATALOG_MAX_GRID_ROWS; $i++) {
            $key = sprintf('desktop:grid:supplier:row-%d:96', $i);
            $cacheServiceMock->get($key)
                ->willReturn($suppliersGridContent)
                ->shouldBeCalled();
        }
    }

    public function testIsGettingCarouselData()
    {
        $carouselContent = [
            'elements' => [
                [
                    'content_type' => 'banner',
                    'data' => [
                        'href' => 'https://beta.linio.com.mx/cm/paypal-400',
                        'image' => [
                            'alt_attr' => 'Paypal 400',
                            'src' => 'http://static.productreview.com.au/pr.products/t/150x150/nike-logo_4cb26a5a5395d.jpg',
                            'title_attr' => 'Paypal 400',
                        ],
                        'type' => 'customized',
                    ],
                ],
                [
                    'content_type' => 'simple_image',
                    'data' => [
                        'alt_attr' => 'Santander Linio',
                        'src' => 'http://static.productreview.com.au/pr.products/t/150x150/nike-logo_4cb26a5a5395d.jpg',
                        'title_attr' => 'Santander Linio',
                    ],
                ],
                [
                    'content_type' => 'dfa',
                    'data' => [
                        'image_url' => 'http://static.productreview.com.au/pr.products/t/150x150/nike-logo_4cb26a5a5395d.jpg',
                        'target_url' => 'http://www.google.com',
                    ],
                ],
            ],
            'revision_number' => 1,
            'title' => 'Brand Carousel',
        ];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get('desktop:carousel:category:row-1:8934')
            ->willReturn($carouselContent);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheServiceMock->reveal());
        $cmsService->carousel('category', '1', 'desktop', 8934);

        $this->assertNotEmpty($carouselContent['elements']);
    }

    public function testIsReturningEmptyArrayOnNonExistentContent()
    {
        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get(Argument::type('string'))
            ->willReturn(null);

        $service = new CmsService();
        $service->setCacheService($cacheServiceMock->reveal());

        $this->assertEquals([], $service->getMainMenuFromCache());
        $this->assertEquals([], $service->getGlobal('1'));
        $this->assertEquals([], $service->carousel('home', '1', '3'));
        $this->assertEquals([], $service->mainMenuCategoryGrid(1928));
        $this->assertEquals([], $service->mainMenuCustomGrid('1'));
        $this->assertEquals([], $service->grid('home', 1, 'desktop'));
        $this->assertEquals([], $service->menu('global', '1', 'desktop'));
        $this->assertEquals([], $service->markdownText('global', '1', 'desktop'));
        $this->assertEquals([], $service->dfa('home', 1, 'desktop'));
        $this->assertEquals([], $service->banner('home', 1, 'desktop'));
        $this->assertEquals([], $service->simpleText('global', '1', 'desktop'));
        $this->assertEquals([], $service->simpleImage('home', 1, 'desktop'));
    }

    /**
     * @return array
     */
    public function siteDirectoryProvider()
    {
        $homeMenu = [
            [
                'attributes' => [
                    'href' => 'https://www.linio.com.pa/',
                    'icon' => 'Ö',
                    'label' => 'Destacados',
                    'main_menu_grid_slug' => '1',
                ],
                'children' => [],
                'type' => 'customized',
            ],
            [
                'attributes' => [
                    'category_id' => 6588,
                    'icon' => 'Â',
                    'label' => 'Celulares Y Tablets',
                ],
                'children' => [],
                'type' => 'category',
            ],
        ];

        $homeMenuGrid = [
            'grid' => [],
            'menu' => [
                [
                    [
                        'attributes' => [
                            'category_id' => 9368,
                            'label' => 'Alimentos y Gourmet',
                        ],
                        'children' => [
                            [
                                'attributes' => [
                                    'category_id' => 9614,
                                    'label' => 'Café',
                                ],
                                'children' => [],
                                'type' => 'category',
                            ],
                        ],
                        'type' => 'category',
                    ],
                ],
            ],
            'revision_number' => 3,
            'title' => 'Más',
        ];

        $categoryMenuGrid = [
            'grid' => [],
            'menu' => [
                [
                    [
                        'attributes' => [
                            'category_id' => 9368,
                            'label' => 'Alimentos y Gourmet',
                        ],
                        'children' => [
                            [
                                'attributes' => [
                                    'category_id' => 9614,
                                    'label' => 'Café',
                                ],
                                'children' => [],
                                'type' => 'category',
                            ],
                        ],
                        'type' => 'category',
                    ],
                ],
            ],
            'revision_number' => 3,
            'title' => 'Celulares y Tablets',
        ];

        $siteDirectory = [
            [
                'type' => 'customized',
                'label' => 'Destacados',
                'href' => 'https://www.linio.com.pa/',
                'children' => [],
                'menu' => [
                    [
                        'type' => 'category',
                        'label' => 'Alimentos y Gourmet',
                        'href' => '/c/category-2',
                        'children' => [
                            [
                                'type' => 'category',
                                'label' => 'Café',
                                'href' => '/c/category-1',
                                'children' => [],
                            ],
                        ],
                    ],
                ],
            ],
            [
                'type' => 'category',
                'label' => 'Celulares Y Tablets',
                'href' => '/c/category-3',
                'children' => [],
                'menu' => [
                    [
                        'type' => 'category',
                        'label' => 'Alimentos y Gourmet',
                        'href' => '/c/category-2',
                        'children' => [
                            [
                                'type' => 'category',
                                'label' => 'Café',
                                'href' => '/c/category-1',
                                'children' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return [
            [$homeMenu, $homeMenuGrid, $categoryMenuGrid, $siteDirectory],
        ];
    }

    /**
     * @dataProvider siteDirectoryProvider
     *
     * @param array $homeMenu
     * @param array $homeMenuGrid
     * @param array $categoryMenuGrid
     * @param array $siteDirectory
     */
    public function testIsBuildingSiteDirectory(
        array $homeMenu,
        array $homeMenuGrid,
        array $categoryMenuGrid,
        array $siteDirectory
    ) {
        $category1 = new Category();
        $category1->setId(9614);
        $category1->setSlug('category-1');

        $category2 = new Category();
        $category2->setId(9368);
        $category2->setSlug('category-2');

        $category3 = new Category();
        $category3->setId(6588);
        $category3->setSlug('category-3');

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('desktop:icons_menu:global')->willReturn($homeMenu);
        $cacheService->get('desktop:main_menu_grid:home:row-1')->willReturn($homeMenuGrid);
        $cacheService->get('desktop:main_menu_grid:category:6588')->willReturn($categoryMenuGrid);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(9614)->willReturn($category1);
        $categoryService->getCategory(9368)->willReturn($category2);
        $categoryService->getCategory(6588)->willReturn($category3);

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.catalog.index.category',
            ['category' => $category1->getSlug()])->willReturn('/c/category-1');
        $router->generate('frontend.catalog.index.category',
            ['category' => $category2->getSlug()])->willReturn('/c/category-2');
        $router->generate('frontend.catalog.index.category',
            ['category' => $category3->getSlug()])->willReturn('/c/category-3');

        $desktopMenuMapper = new MenuMapper($categoryService->reveal(), $router->reveal());

        $cmsService = new CmsService();
        $cmsService->setDesktopMenuMapper($desktopMenuMapper);
        $cmsService->setCacheService($cacheService->reveal());

        $actual = $cmsService->buildSiteDirectory();

        $this->assertEquals($siteDirectory, $actual);
    }

    public function testIsReturningCategoryMenu()
    {
        $cachedCategoryMenu = [
            '2' => [
                'type' => 'category',
                'attributes' => [
                    'category_id' => 2,
                    'label' => 'Category name label 2',
                ],
                'children' => [],
            ],
            '1' => [
                'type' => 'category',
                'attributes' => [
                    'category_id' => 1,
                    'label' => 'Category name label 1',
                ],
                'children' => [],
            ],
        ];

        $categoryMenu = [
            [
                'label' => 'Category name label 1',
                'href' => '/',
                'children' => [],
            ],
            [
                'label' => 'Category name label 2',
                'href' => '/',
                'children' => [],
            ],
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(sprintf('%s:%s:global', CmsService::CONTENT_MOBILE, CmsService::CONTENT_TYPE_CATEGORY_MENU))
            ->willReturn($cachedCategoryMenu);

        $menuMapper = $this->prophesize(MobileMenuMapper::class);
        $menuMapper->map($cachedCategoryMenu[1])->willReturn($categoryMenu[0]);
        $menuMapper->map($cachedCategoryMenu[2])->willReturn($categoryMenu[1]);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cacheService->reveal());
        $cmsService->setMenuMapper($menuMapper->reveal());

        $actual = $cmsService->getCategoryMenu();

        $this->assertEquals($categoryMenu, $actual);
    }

    /**
     * @return array
     */
    public function mobileContentProvider(): array
    {
        return [
            $this->mobileMenuProvider(),
            $this->mobileGridProvider(),
            $this->mobileCarouselProvider(),
            $this->mobileBannerProvider(),
            $this->mobileHtmlProvider(),
        ];
    }

    /**
     * @return array
     */
    public function mobileMenuProvider(): array
    {
        return
            [
                [
                    'page_content' => [
                        [
                            'content_type' => 'menu',
                            'data' => [
                                [
                                    'type' => 'customized',
                                    'attributes' => [
                                        'image' => 'http://path-to-image',
                                        'label' => 'Awesome product',
                                        'href' => 'https://www.linio.com.mx/p/awesome-product',
                                    ],
                                    'children' => [],
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    [
                        'content_type' => 'menu',
                        'items' => [
                            [
                                'type' => 'product',
                                'label' => 'Awesome product',
                                'image' => 'http://path-to-image',
                                'href' => '/p/awesome-product',
                                'slugs' => [
                                    'product' => 'awesome-product',
                                ],
                                'ids' => [
                                    'product' => 'abc1234',
                                ],
                                'children' => [],
                            ],
                        ],
                    ],
                ],
            ];
    }

    /**
     * @return array
     */
    public function mobileGridProvider(): array
    {
        return [
            [
                'page_content' => [
                    [
                        'content_type' => 'grid',
                        'data' => [
                            'title' => 'Grid title',
                            'cells' => [
                                [
                                    'content_type' => 'carousel',
                                    'cols' => 12,
                                    'rows' => 6,
                                    'data' => [
                                        'title' => 'Carousel title',
                                        'elements' => [
                                            [
                                                'content_type' => 'banner',
                                                'data' => [
                                                    'href' => 'http://www.linio.com/c/category',
                                                    'image' => [
                                                        'src' => 'http://www.images.linio.com/category.jpg',
                                                        'alt_attr' => 'Alt text',
                                                        'title' => 'Category',
                                                    ],
                                                    'title' => 'Category banner',
                                                    'type' => 'customized',
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                [
                    'content_type' => 'grid',
                    'title' => 'Grid title',
                    'cells' => [
                        [
                            'content_type' => 'carousel',
                            'title' => 'Carousel title',
                            'elements' => [
                                [
                                    'content_type' => 'banner',
                                    'href' => '/c/category',
                                    'image' => [
                                        'src' => 'http://www.images.linio.com/category.jpg',
                                        'alt_attr' => 'Alt text',
                                        'title' => 'Category',
                                    ],
                                    'title' => 'Category banner',
                                    'type' => 'category',
                                    'slugs' => [
                                        'category' => 'category',
                                    ],
                                    'ids' => [
                                        'category' => 4,
                                    ],
                                ],
                            ],
                            'cols' => '12',
                            'rows' => '6',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function mobileCarouselProvider(): array
    {
        return [
            [
                'page_content' => [
                    [
                        'content_type' => 'carousel',
                        'data' => [
                            'title' => 'Carousel title',
                            'elements' => [
                                [
                                    'content_type' => 'banner',
                                    'data' => [
                                        'href' => 'http://www.linio.com/c/category',
                                        'image' => [
                                            'src' => 'http://www.images.linio.com/category.jpg',
                                            'alt_attr' => 'Alt text',
                                            'title' => 'Category',
                                        ],
                                        'title' => 'Category banner',
                                        'type' => 'customized',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                [
                    'content_type' => 'carousel',
                    'title' => 'Carousel title',
                    'elements' => [
                        [
                            'content_type' => 'banner',
                            'href' => '/c/category',
                            'image' => [
                                'src' => 'http://www.images.linio.com/category.jpg',
                                'alt_attr' => 'Alt text',
                                'title' => 'Category',
                            ],
                            'title' => 'Category banner',
                            'type' => 'category',
                            'slugs' => [
                                'category' => 'category',
                            ],
                            'ids' => [
                                'category' => 4,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function mobileBannerProvider(): array
    {
        return [
            [
                'page_content' => [
                    [
                        'content_type' => 'banner',
                        'data' => [
                            'href' => 'http://www.linio.com/c/category',
                            'image' => [
                                'src' => 'http://www.images.linio.com/category.jpg',
                                'alt_attr' => 'Alt text',
                                'title' => 'Category',
                            ],
                            'title' => 'Category banner',
                            'type' => 'customized',
                        ],
                    ],
                ],
            ],
            [
                [
                    'content_type' => 'banner',
                    'href' => '/c/category',
                    'image' => [
                        'src' => 'http://www.images.linio.com/category.jpg',
                        'alt_attr' => 'Alt text',
                        'title' => 'Category',
                    ],
                    'title' => 'Category banner',
                    'type' => 'category',
                    'slugs' => [
                        'category' => 'category',
                    ],
                    'ids' => [
                        'category' => 4,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function mobileHtmlProvider(): array
    {
        return [
            [
                'page_content' => [
                    [
                        'content_type' => 'html',
                        'data' => [
                            'text' => '<html><body>testing</body></html>',
                        ],
                    ],
                ],
            ],
            [
                [
                    'data' => '<html><body>testing</body></html>',
                    'content_type' => 'html',
                ],
            ],
        ];
    }

    /**
     * @dataProvider mobileContentProvider
     *
     * @param array $content
     * @param array $expected
     */
    public function testIsBuildingMobileContent(array $content, array $expected)
    {
        $slug = 'page-content';

        $key = sprintf(
            '%s:%s:%s:%s',
            CmsService::CONTENT_MOBILE,
            CmsService::CONTENT_TYPE_PAGE_CONTENT,
            CmsService::CONTENT_NAMESPACE_GLOBAL,
            $slug
        );

        $urlMapper = $this->stubUrlMapper($expected);
        $menuMapper = new MobileMenuMapper($urlMapper);

        $cache = $this->prophesize(CacheService::class);
        $cache->get($key)->willReturn($content);

        $cmsService = new CmsService();
        $cmsService->setMenuMapper($menuMapper);
        $cmsService->setCacheService($cache->reveal());
        $cmsService->setUrlMapper($urlMapper);
        $actual = $cmsService->buildMobilePageContent($slug);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function mobileTopMenuProvider(): array
    {
        return [
            [
                [
                    [
                        'type' => 'customized',
                        'attributes' => [
                            'label' => 'Awesome product',
                            'href' => 'https://www.linio.com.mx/p/awesome-product',
                            'page_content_key' => 'mx:cms:mobile:page_content:global:regalos-express',
                        ],
                        'children' => [],
                    ],
                ],
                [
                    'page_content' => [
                        [
                            'content_type' => 'carousel',
                            'data' => [
                                'title' => 'Carousel title',
                                'elements' => [
                                    [
                                        'content_type' => 'banner',
                                        'data' => [
                                            'href' => 'http://www.linio.com/c/category',
                                            'image' => [
                                                'src' => 'http://www.images.linio.com/category.jpg',
                                                'alt_attr' => 'Alt text',
                                                'title' => 'Category',
                                            ],
                                            'title' => 'Category banner',
                                            'type' => 'customized',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'revision_number' => 1,
                ],
                [
                    [
                        'type' => 'product',
                        'label' => 'Awesome product',
                        'href' => '/p/awesome-product',
                        'slugs' => [
                            'product' => 'awesome-product',
                        ],
                        'ids' => [
                            'product' => 'abc1234',
                        ],
                        'children' => [],
                        'content' => [
                            [
                                'content_type' => 'carousel',
                                'title' => 'Carousel title',
                                'elements' => [
                                    [
                                        'content_type' => 'banner',
                                        'href' => '/c/category',
                                        'image' => [
                                            'src' => 'http://www.images.linio.com/category.jpg',
                                            'alt_attr' => 'Alt text',
                                            'title' => 'Category',
                                        ],
                                        'title' => 'Category banner',
                                        'type' => 'category',
                                        'slugs' => [
                                            'category' => 'category',
                                        ],
                                        'ids' => [
                                            'category' => 4,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider mobileTopMenuProvider
     *
     * @param array $topMenuContent
     * @param array $pageContent
     * @param array $expected
     */
    public function testIsBuildingMobileTopMenu(array $topMenuContent, array $pageContent, array $expected)
    {
        $topMenuKey = sprintf('%s:%s:global',
            CmsService::CONTENT_MOBILE,
            CmsService::CONTENT_TYPE_TOP_MENU
        );

        $pageContentKey = 'mobile:page_content:global:regalos-express';

        $urlMapper = $this->prophesize(UrlMapper::class);
        $urlMapper->map(Argument::type('array'))->willReturn([
            'href' => $expected[0]['href'],
            'type' => $expected[0]['type'],
            'slugs' => $expected[0]['slugs'],
            'ids' => $expected[0]['ids'],
        ]);

        $menuMapper = new MobileMenuMapper($urlMapper->reveal());

        $cache = $this->prophesize(CacheService::class);
        $cache->getNamespace()->willReturn('mx:cms');
        $cache->get($topMenuKey)->willReturn($topMenuContent);
        $cache->get($pageContentKey)->willReturn($pageContent);

        $cmsService = new CmsService();
        $cmsService->setCacheService($cache->reveal());
        $cmsService->setMenuMapper($menuMapper);
        $cmsService->setUrlMapper($this->stubUrlMapper($expected[0]['content']));
        $actual = $cmsService->buildMobileTopMenu();

        $this->assertEquals($expected, $actual);
    }

    /**
     * @param array $data
     *
     * @return UrlMapper
     */
    protected function stubUrlMapper(array $data): UrlMapper
    {
        $mapper = $this->prophesize(UrlMapper::class);

        $this->stubTypes($mapper, $data);

        return $mapper->reveal();
    }

    /**
     * @param ObjectProphecy $mapper
     * @param array $data
     */
    protected function stubTypes(ObjectProphecy $mapper, array $data)
    {
        foreach ($data as $content) {
            switch ($content['content_type']) {
                case 'menu':
                    foreach ($content['items'] as $item) {
                        $this->prophesizeUrlData($mapper, $item);
                    }
                    break;
                case 'grid':
                    $this->stubTypes($mapper, $content['cells']);
                    break;
                case 'carousel':
                    $this->stubTypes($mapper, $content['elements']);
                    break;
                case 'banner':
                    $this->prophesizeUrlData($mapper, $content);
            }
        }
    }

    /**
     * @param ObjectProphecy $mapper
     * @param array $data
     */
    protected function prophesizeUrlData(ObjectProphecy $mapper, array $data)
    {
        $urlData = [
            'type' => $data['type'],
            'href' => $data['href'],
            'slugs' => $data['slugs'],
            'ids' => $data['ids'],
        ];

        $mapper->map(Argument::any())->willReturn($urlData);
    }
}
