<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\ContentType\Mobile;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Cms\Exception\InvalidLinioUrlInContentException;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\SlugResolver\SlugResolverService;
use Prophecy\Argument;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class UrlMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function badUrlTypeProvider(): array
    {
        return [
            [[]],
            [['type' => '']],
            [['type' => 'category']],
            [['type' => 'category', 'href' => '']],
            [['type' => 'cateogyr', 'href' => '/']],
            [['type' => 'cateogyr', 'href' => 'http://localhost/']],
            [['type' => 'cateogyr', 'href' => 'http://linio/']],
            [['type' => 'cateogyr', 'href' => 'linio']],
        ];
    }

    /**
     * @dataProvider badUrlTypeProvider
     *
     * @param array $data
     */
    public function testMapReturnsSameArrayForBadContent(array $data)
    {
        $router = $this->prophesize(RouterInterface::class)->reveal();
        $categoryService = $this->prophesize(CategoryService::class)->reveal();
        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $mapper = new UrlMapper($router, $categoryService, $resolverService);

        $this->assertSame($data, $mapper->map($data));
    }

    /**
     * @return array
     */
    public function badCategoryUrlProvider(): array
    {
        return [
            [['type' => 'category', 'href' => 5], 5],
            [['type' => 'category', 'href' => 'http://linio.com.ec/c/'], 0],
        ];
    }

    /**
     * @dataProvider badCategoryUrlProvider
     *
     * @param array $data
     * @param int $id
     */
    public function testMapCategoryThrowsForMissingCategory(array $data, int $id)
    {
        $router = $this->prophesize(RouterInterface::class)->reveal();
        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory($id)->willThrow(new CategoryNotFoundException('test'));

        $mapper = new UrlMapper($router, $categoryService->reveal(), $resolverService);

        $this->expectException(\DomainException::class);

        $mapper->map($data);
    }

    public function testMapCategoryReturnsSlugAndUrl()
    {
        $categoryId = 5;
        $slug = 'some-slug';
        $href = '/c/' . $slug;

        $category = new Category();
        $category->setId($categoryId);
        $category->setSlug($slug);
        $category->setUrlKey($slug);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory($categoryId)->willReturn($category);

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(Argument::type('string'), ['category' => $slug])->willReturn($href);

        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $mapper = new UrlMapper($router->reveal(), $categoryService->reveal(), $resolverService);

        $actual = $mapper->map(['type' => 'category', 'href' => $categoryId]);

        $expected = [
            'href' => $href,
            'slugs' => [
                'category' => $slug,
            ],
            'urlKey' => $slug,
            'ids' => [
                'category' => $categoryId,
            ],
            'query' => null,
            'type' => 'category',
        ];

        $this->assertSame($expected, $actual);
    }

    public function testBadCustomUrlThrows()
    {
        $router = $this->prophesize(RouterInterface::class);
        $router->match(Argument::type('string'))->willThrow(new ResourceNotFoundException());

        $categoryService = $this->prophesize(CategoryService::class)->reveal();
        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService);

        $this->expectException(InvalidLinioUrlInContentException::class);

        $mapper->map(['type' => 'customized', 'href' => 'http://linio.com.ec/bad/url']);
    }

    public function testUnsupportedCustomUrlMatch()
    {
        $router = $this->prophesize(RouterInterface::class);
        $router->match(Argument::type('string'))->willReturn(['_route' => 'bad.route']);

        $categoryService = $this->prophesize(CategoryService::class)->reveal();
        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService);

        $this->expectException(DomainException::class);

        $mapper->map(['type' => 'customized', 'href' => 'http://linio.com.ec/account/orders']);
    }

    /**
     * @return array
     */
    public function homeUrlProvider(): array
    {
        return [
            [['type' => 'customized', 'href' => 'http://linio.com.ec', 'slugs' => null, 'ids' => null, 'query' => null]],
            [['type' => 'customized', 'href' => 'http://linio.com.ec/', 'slugs' => null, 'ids' => null, 'query' => null]],
            [['type' => 'customized', 'href' => 'http://linio.com/', 'slugs' => null, 'ids' => null, 'query' => null]],
        ];
    }

    /**
     * @dataProvider homeUrlProvider
     *
     * @param array $expected
     */
    public function testCustomUrlReturnsHomeUrl(array $expected)
    {
        $router = $this->prophesize(RouterInterface::class);
        $router->match(Argument::type('string'))->willReturn(['_route' => 'bad.route']);

        $categoryService = $this->prophesize(CategoryService::class)->reveal();
        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService);

        $actual = $mapper->map($expected);

        $this->assertSame($expected, $actual);
    }

    /**
     * @return array
     */
    public function customUrlProvider(): array
    {
        $product = new Product();
        $product->setSku('abcd-1234');
        $productSlug = 'test-product-abcd-1234';

        $seller = new Seller();
        $seller->setSellerId(5);
        $seller->setSlug('test-seller');

        $category = new Category();
        $category->setId(5);
        $category->setSlug('test-category');

        $brand = new Brand();
        $brand->setId(5);
        $brand->setSlug('test-brand');

        $campaign = new Campaign();
        $campaign->setId(5);
        $campaign->setSlug('test-campaign');

        return [
            [
                ['type' => 'customized', 'href' => 'https://linio.com/p/' . $productSlug],
                ['_route' => 'catalog.detail', 'productSlug' => $productSlug],
                [$productSlug],
                [$product],
                [
                    'type' => 'product',
                    'href' => '/p/' . $productSlug,
                    'slugs' => [
                        'product' => $productSlug,
                    ],
                    'ids' => [
                        'product' => $product->getSku(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.com.ec/s/' . $seller->getSlug()],
                ['_route' => 'catalog.seller', 'seller' => $seller->getSlug()],
                [$seller->getSlug()],
                [$seller],
                [
                    'type' => 'seller',
                    'href' => '/s/' . $seller->getSlug(),
                    'slugs' => [
                        'seller' => $seller->getSlug(),
                    ],
                    'ids' => [
                        'seller' => $seller->getSellerId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.cl/c/' . $category->getSlug()],
                ['_route' => 'catalog.category', 'category' => $category->getSlug()],
                [$category->getSlug()],
                [$category],
                [
                    'type' => 'category',
                    'href' => '/c/' . $category->getSlug(),
                    'slugs' => [
                        'category' => $category->getSlug(),
                    ],
                    'ids' => [
                        'category' => $category->getId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://shop-front07-ec.linio-staging.com/b/' . $brand->getSlug()],
                ['_route' => 'catalog.brand', 'brandSlug' => $brand->getSlug()],
                [$brand->getSlug()],
                [$brand],
                [
                    'type' => 'brand',
                    'href' => '/b/' . $brand->getSlug(),
                    'slugs' => [
                        'brand' => $brand->getSlug(),
                    ],
                    'ids' => [
                        'brand' => $brand->getId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://linio.com.ec/p/' . $productSlug],
                ['_route' => 'catalog.detail', 'productSlug' => $productSlug],
                [$productSlug],
                [$product],
                [
                    'type' => 'product',
                    'href' => '/p/' . $productSlug,
                    'slugs' => [
                        'product' => $productSlug,
                    ],
                    'ids' => [
                        'product' => $product->getSku(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.com.ec/s/' . $seller->getSlug()],
                ['_route' => 'catalog.seller', 'seller' => $seller->getSlug()],
                [$seller->getSlug()],
                [$seller],
                [
                    'type' => 'seller',
                    'href' => '/s/' . $seller->getSlug(),
                    'slugs' => [
                        'seller' => $seller->getSlug(),
                    ],
                    'ids' => [
                        'seller' => $seller->getSellerId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.com.ec/c/' . $category->getSlug()],
                ['_route' => 'catalog.category', 'category' => $category->getSlug()],
                [$category->getSlug()],
                [$category],
                [
                    'type' => 'category',
                    'href' => '/c/' . $category->getSlug(),
                    'slugs' => [
                        'category' => $category->getSlug(),
                    ],
                    'ids' => [
                        'category' => $category->getId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.com.ec/b/' . $brand->getSlug()],
                ['_route' => 'catalog.brand', 'brandSlug' => $brand->getSlug()],
                [$brand->getSlug()],
                [$brand],
                [
                    'type' => 'brand',
                    'href' => '/b/' . $brand->getSlug(),
                    'slugs' => [
                        'brand' => $brand->getSlug(),
                    ],
                    'ids' => [
                        'brand' => $brand->getId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://linio.com.ec/cm/' . $campaign->getSlug()],
                ['_route' => 'catalog.campaign', 'campaign' => $campaign->getSlug()],
                [$campaign->getSlug()],
                [$campaign],
                [
                    'type' => 'campaign',
                    'href' => '/cm/' . $campaign->getSlug(),
                    'slugs' => [
                        'campaign' => $campaign->getSlug(),
                    ],
                    'ids' => [
                        'campaign' => $campaign->getId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.com.ec/c/' . $category->getSlug() . '/b/' . $brand->getSlug()],
                ['_route' => 'catalog.category_brand', 'category' => $category->getSlug(), 'brand' => $brand->getSlug()],
                [$category->getSlug(), $brand->getSlug()],
                [$category, $brand],
                [
                    'type' => 'category_brand',
                    'href' => '/c/' . $category->getSlug() . '/b/' . $brand->getSlug(),
                    'slugs' => [
                        'category' => $category->getSlug(),
                        'brand' => $brand->getSlug(),
                    ],
                    'ids' => [
                        'category' => $category->getId(),
                        'brand' => $brand->getId(),
                    ],
                    'query' => null,
                ],
            ],
            [
                ['type' => 'customized', 'href' => 'https://www.linio.com.ec/c/' . $category->getSlug() . '/s/' . $seller->getSlug()],
                ['_route' => 'catalog.category_seller', 'category' => $category->getSlug(), 'seller' => $seller->getSlug()],
                [$category->getSlug(), $seller->getSlug()],
                [$category, $seller],
                [
                    'type' => 'category_seller',
                    'href' => '/c/' . $category->getSlug() . '/s/' . $seller->getSlug(),
                    'slugs' => [
                        'category' => $category->getSlug(),
                        'seller' => $seller->getSlug(),
                    ],
                    'ids' => [
                        'category' => $category->getId(),
                        'seller' => $seller->getSellerId(),
                    ],
                    'query' => null,
                ],
            ],
        ];
    }

    /**
     * @dataProvider customUrlProvider
     *
     * @param array $data
     * @param array $route
     * @param array $slugs
     * @param array $resolved
     * @param array $expected
     */
    public function testMapCustomUrlReturnsSlugAndId(array $data, array $route, array $slugs, array $resolved, array $expected)
    {
        $categoryService = $this->prophesize(CategoryService::class)->reveal();

        $router = $this->prophesize(RouterInterface::class);
        $router->match(Argument::type('string'))->willReturn($route);

        $resolverService = $this->prophesize(SlugResolverService::class);

        foreach ($slugs as $index => $slug) {
            $resolverService->resolve(Argument::type('string'), $slug)->willReturn($resolved[$index]);
        }

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService->reveal());

        $actual = $mapper->map($data);

        $this->assertSame($expected, $actual);
    }

    public function testMapBadTypeThrowsException()
    {
        $router = $this->prophesize(RouterInterface::class)->reveal();
        $categoryService = $this->prophesize(CategoryService::class)->reveal();
        $resolverService = $this->prophesize(SlugResolverService::class)->reveal();

        $mapper = new UrlMapper($router, $categoryService, $resolverService);

        $this->expectException(DomainException::class);

        $mapper->map(['type' => 'product', 'href' => 1234]);
    }

    /**
     * @dataProvider brandIdProvider
     *
     * @param array $data
     * @param array $expected
     */
    public function testMapBrandIdTypeReturnsSlugAndId(array $data, array $expected)
    {
        $slug = 'test-brand';
        $href = '/b/' . $slug;

        $categoryService = $this->prophesize(CategoryService::class)->reveal();

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(Argument::type('string'), ['brandSlug' => $slug])->willReturn($href);

        $resolverService = $this->prophesize(SlugResolverService::class);

        $brand['slug'] = $expected['slugs']['brand'];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Argument::type('string'))->willReturn($brand);

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService->reveal());
        $mapper->setBrandCacheService($cacheService->reveal());

        $actual = $mapper->map($data);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function brandIdProvider(): array
    {
        $brand = new Brand();
        $brand->setId(5);
        $brand->setSlug('test-brand');

        return [
            [
                ['type' => 'brand', 'href' => 5],
                [
                    'type' => 'brand',
                    'href' => '/b/' . $brand->getSlug(),
                    'slugs' => [
                        'brand' => $brand->getSlug(),
                    ],
                    'ids' => [
                        'brand' => $brand->getId(),
                    ],
                    'query' => null,
                ],
            ],
        ];
    }

    /**
     * @dataProvider campaignIdProvider
     *
     * @param array $data
     * @param array $expected
     */
    public function testMapCampaignIdTypeReturnsSlugAndId(array $data, array $expected)
    {
        $slug = 'test-campaign';
        $href = '/cm/' . $slug;

        $categoryService = $this->prophesize(CategoryService::class)->reveal();

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(Argument::type('string'), ['campaign' => $slug])->willReturn($href);

        $resolverService = $this->prophesize(SlugResolverService::class);

        $campaign['slug'] = $expected['slugs']['campaign'];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Argument::type('string'))->willReturn($campaign);

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService->reveal());
        $mapper->setCampaignCacheService($cacheService->reveal());

        $actual = $mapper->map($data);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function campaignIdProvider(): array
    {
        $campaign = new Campaign();
        $campaign->setId(5);
        $campaign->setSlug('test-campaign');

        return [
            [
                ['type' => 'campaign', 'href' => 5],
                [
                    'type' => 'campaign',
                    'href' => '/cm/' . $campaign->getSlug(),
                    'slugs' => [
                        'campaign' => $campaign->getSlug(),
                    ],
                    'ids' => [
                        'campaign' => $campaign->getId(),
                    ],
                    'query' => null,
                ],
            ],
        ];
    }

    /**
     * @dataProvider segmentIdProvider
     *
     * @param array $data
     * @param array $expected
     */
    public function testMapSegmentIdTypeReturnsSlugAndId(array $data, array $expected)
    {
        $slug = 'test-campaign/segment';
        $href = '/cm/' . $slug;

        $categoryService = $this->prophesize(CategoryService::class)->reveal();

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(Argument::type('string'), ['campaign' => $slug])->willReturn($href);

        $resolverService = $this->prophesize(SlugResolverService::class);

        $campaign['slug'] = $expected['slugs']['campaign'];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Argument::type('string'))->willReturn($campaign);

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService->reveal());
        $mapper->setSegmentCacheService($cacheService->reveal());

        $actual = $mapper->map($data);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function segmentIdProvider(): array
    {
        $campaign = new Campaign();
        $campaign->setId(8);
        $campaign->setSlug('test-campaign/segment');

        return [
            [
                ['type' => 'segment', 'href' => 8],
                [
                    'type' => 'segment',
                    'href' => '/cm/' . $campaign->getSlug(),
                    'slugs' => [
                        'campaign' => $campaign->getSlug(),
                    ],
                    'ids' => [
                        'segment' => $campaign->getId(),
                    ],
                    'query' => null,
                ],
            ],
        ];
    }

    /**
     * @dataProvider productSkuProvider
     *
     * @param array $data
     * @param array $expected
     */
    public function testMapProductTypeReturnsSlugAndId(array $data, array $expected)
    {
        $slug = 'this-is-some-product';
        $href = '/p/' . $slug;

        $categoryService = $this->prophesize(CategoryService::class)->reveal();

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(Argument::type('string'), ['productSlug' => $slug])->willReturn($href);

        $resolverService = $this->prophesize(SlugResolverService::class);

        $product['slug'] = $expected['slugs']['product'];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Argument::type('string'))->willReturn($product);

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService->reveal());
        $mapper->setProductCacheService($cacheService->reveal());

        $actual = $mapper->map($data);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function productSkuProvider(): array
    {
        $product = new Product();
        $product->setSku('SOm3SkU');
        $product->setSlug('this-is-some-product');

        return [
            [
                ['type' => 'sku', 'href' => $product->getSku()],
                [
                    'type' => 'sku',
                    'href' => '/p/' . $product->getSlug(),
                    'slugs' => [
                        'product' => $product->getSlug(),
                    ],
                    'ids' => [
                        'product' => $product->getSku(),
                    ],
                    'query' => null,
                ],
            ],
        ];
    }

    /**
     * @dataProvider sellerIdProvider
     *
     * @param array $data
     * @param array $expected
     */
    public function testMapSellerTypeReturnsSlugAndId(array $data, array $expected)
    {
        $slug = 'this-is-some-seller';
        $href = '/s/' . $slug;

        $categoryService = $this->prophesize(CategoryService::class)->reveal();

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(Argument::type('string'), ['seller' => $slug])->willReturn($href);

        $resolverService = $this->prophesize(SlugResolverService::class);

        $seller['slug'] = $expected['slugs']['seller'];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get(Argument::type('string'))->willReturn($seller);

        $mapper = new UrlMapper($router->reveal(), $categoryService, $resolverService->reveal());
        $mapper->setSellerCacheService($cacheService->reveal());

        $actual = $mapper->map($data);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    public function sellerIdProvider(): array
    {
        $seller = new Seller();
        $seller->setSellerId(18);
        $seller->setSlug('this-is-some-seller');

        return [
            [
                ['type' => 'supplier', 'href' => $seller->getSellerId()],
                [
                    'type' => 'supplier',
                    'href' => '/s/' . $seller->getSlug(),
                    'slugs' => [
                        'seller' => $seller->getSlug(),
                    ],
                    'ids' => [
                        'seller' => $seller->getSellerId(),
                    ],
                    'query' => null,
                ],
            ],
        ];
    }
}
