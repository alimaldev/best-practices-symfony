<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\ContentType\Mobile;

use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\SlugResolver\SlugResolverService;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Routing\RouterInterface;

class MenuMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function categoryMenuProvider(): array
    {
        $menuFromCache = [
            'type' => 'category',
            'attributes' => [
                'category_id' => 1,
                'label' => 'Category name label',
            ],
            'url_key' => 'category-name',
            'children' => [],
        ];

        $mappedMenu = [
            'type' => 'category',
            'label' => 'Category name label',
            'href' => null,
            'slugs' => [
                'category' => 'category-name',
            ],
            'urlKey' => 'category-name',
            'ids' => [
                'category' => 1,
            ],
            'query' => null,
            'children' => [],
        ];

        $customTypeFromCache = [
            'attributes' => [
                'href' => 'http://need-deeplink-here?',
                'label' => 'Ver Todo',
            ],
            'type' => 'customized',
        ];

        $mappedCustomType = [
            'type' => 'customized',
            'label' => 'Ver Todo',
            'href' => 'http://need-deeplink-here?',
            'children' => [],
        ];

        return [
            [$mappedMenu, $menuFromCache],
            [$mappedCustomType, $customTypeFromCache],
        ];
    }

    /**
     * @return array
     */
    public function categoryMenuWithNonExistentCategoryProvider()
    {
        $menuFromCache = [
            'type' => 'category',
            'attributes' => [
                'category_id' => 1,
                'label' => 'Category name label',
            ],
            'children' => [],
        ];

        $mappedMenu = [
            'type' => 'category',
            'label' => 'Category name label',
            'categoryId' => 1,
            'slug' => [
                'category' => '/',
            ],
            'children' => [],
        ];

        return [
            [$mappedMenu, $menuFromCache],
        ];
    }

    /**
     * @dataProvider categoryMenuProvider
     *
     * @param array $mappedMenu
     * @param array $menu
     */
    public function testIsMappingCategoryMenu(array $mappedMenu, array $menu)
    {
        $category = new Category();
        $category->setSlug('category-name');
        $category->setUrlKey('category-name');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(1)->willReturn($category);

        $urlMapper = new UrlMapper(
            $this->prophesize(RouterInterface::class)->reveal(),
            $categoryService->reveal(),
            $this->prophesize(SlugResolverService::class)->reveal()
        );

        $mapper = new MenuMapper($urlMapper);

        $actual = $mapper->map($menu);

        $this->assertSame($mappedMenu, $actual);
    }

    /**
     * @dataProvider categoryMenuWithNonExistentCategoryProvider
     *
     * @param array $mappedMenu
     * @param array $menu
     */
    public function testMappingCategoryMenuDetectingCategoryNotFound(array $mappedMenu, array $menu)
    {
        $category = new Category();
        $category->setSlug('category-name');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(1)
            ->willThrow(new CategoryNotFoundException('Category id=(1) not found.'));

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(
            'frontend.default.index')
            ->willReturn('/');

        $urlMapper = new UrlMapper(
            $router->reveal(),
            $categoryService->reveal(),
            $this->prophesize(SlugResolverService::class)->reveal()
        );

        $mapper = new MenuMapper($urlMapper);

        $this->expectException(CategoryNotFoundException::class);

        $actual = $mapper->map($menu);
    }

    /**
     * @return array
     */
    public function customizedMenuProvider(): array
    {
        $menuFromCache = [
            'type' => 'customized',
            'attributes' => [
                'label' => 'Awesome product',
                'href' => 'https://www.linio.com.mx/p/awesome-product',
            ],
            'children' => [],
        ];

        $mappedMenu = [
            'type' => 'product',
            'label' => 'Awesome product',
            'href' => '/p/awesome-product',
            'slugs' => [
                'product' => 'awesome-product',
            ],
            'ids' => [
                'product' => 'abc1234',
            ],
            'children' => [],
            'query' => null,
        ];

        return [
            [$menuFromCache, $mappedMenu],
        ];
    }

    /**
     * @dataProvider customizedMenuProvider
     *
     * @param array $menu
     * @param array $mappedMenu
     */
    public function testIsMappingCustomizedMenu(array $menu, array $mappedMenu)
    {
        $router = $this->prophesize(RouterInterface::class);
        $router->match('/p/awesome-product')->willReturn([
            '_controller' => 'controller.catalog:detailAction',
            'productSlug' => 'awesome-product',
            '_route' => 'frontend.catalog.detail',
        ]);

        $product = new Product('abc1234');
        $product->setSlug('awesome-product');

        $slugResolver = $this->prophesize(SlugResolverService::class);
        $slugResolver->resolve('product', 'awesome-product')->willReturn($product);

        $urlMapper = new UrlMapper(
            $router->reveal(),
            $this->prophesize(CategoryService::class)->reveal(),
            $slugResolver->reveal()
        );

        $mapper = new MenuMapper($urlMapper);

        $actual = $mapper->map($menu);

        $this->assertEquals($mappedMenu, $actual);
    }

    public function testIsMappingCustomizedInMenuSearch()
    {
        $menu = [
            'type' => 'customized',
            'attributes' => [
                'label' => 'search-iphone',
                'href' => 'http://www.linio.com.ec/search?q=iphone&price=100-1000',
            ],
            'children' => [],
        ];

        $mappedMenu = [
            'type' => 'search',
            'label' => 'search-iphone',
            'href' => '/search',
            'slugs' => null,
            'ids' => null,
            'children' => [],
            'query' => ['q' => 'iphone', 'price' => '100-1000'],
        ];

        $router = $this->prophesize(RouterInterface::class);
        $router->match('/search')->willReturn([
            '_controller' => 'controller.catalog:searchAction',
            '_route' => 'frontend.catalog.index.search',
        ]);

        $product = new Product('abc1234');
        $product->setSlug('awesome-product');

        $slugResolver = $this->prophesize(SlugResolverService::class);
        $slugResolver->resolve('search', 'search-iphone')->willReturn($product);

        $urlMapper = new UrlMapper(
            $router->reveal(),
            $this->prophesize(CategoryService::class)->reveal(),
            $slugResolver->reveal()
        );

        $mapper = new MenuMapper($urlMapper);

        $actual = $mapper->map($menu);

        $this->assertEquals($mappedMenu, $actual);
    }
}
