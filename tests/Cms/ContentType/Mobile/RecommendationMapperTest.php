<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\ContentType\Mobile;

use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\SlugResolver\SlugResolverService;
use Linio\Type\Money;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class RecommendationMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function manualSkuRecommendationProvider(): array
    {
        return [
            [
                [
                    'type' => 'manual',
                    'rules' => [
                        [
                            'type' => 'sku',
                            'criteria' => ['sku-simple'],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider manualSkuRecommendationProvider
     *
     * @param array $recommendationData
     */
    public function testIsMappingManualSkuRecommendation(array $recommendationData)
    {
        $image = new Image();
        $product = new Product();
        $product->addImage($image);
        $product->addSimple(new Simple());

        $categoryService = $this->prophesize(CategoryService::class);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySkus(['sku-simple'])->willReturn([]);

        $router = $this->prophesize(RouterInterface::class)->reveal();

        $urlMapper = new UrlMapper(
            $router,
            $categoryService->reveal(),
            $this->prophesize(SlugResolverService::class)->reveal()
        );

        $mapper = new RecommendationMapper($productService->reveal(), $urlMapper, $router);
        $actual = $mapper->map($recommendationData);

        $this->assertContainsOnlyInstancesOf(Product::class, $actual['products']);
    }

    /**
     * @return array
     */
    public function manualCategoryRecommendationProvider(): array
    {
        return [
            [
                [
                    'type' => 'manual',
                    'rules' => [
                        [
                            'type' => 'category',
                            'criteria' => [397],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider manualCategoryRecommendationProvider
     *
     * @param array $recommendationData
     */
    public function testIsMappingManualCategoryRecommendation(array $recommendationData)
    {
        $expected = [
            'href' => '/c/awesome',
            'type' => 'category',
            'slugs' => [
                'category' => 'awesome',
            ],
            'urlKey' => 'awesome',
            'ids' => [
                'category' => 397,
            ],
            'query' => null,
        ];

        $category = new Category();
        $category->setId(397);
        $category->setSlug('awesome');
        $category->setUrlKey('awesome');

        $productService = $this->prophesize(ProductService::class);

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(397)->willReturn($category);

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.catalog.index.category', ['category' => $category->getSlug()])->willReturn('/c/awesome');

        $urlMapper = new UrlMapper(
            $router->reveal(),
            $categoryService->reveal(),
            $this->prophesize(SlugResolverService::class)->reveal()
        );

        $mapper = new RecommendationMapper($productService->reveal(), $urlMapper, $router->reveal());
        $actual = $mapper->map($recommendationData);

        $this->assertEquals($expected, $actual);
    }

    public function testIsMappingManualRecommendationWithProducts()
    {
        $recommendationData = [
            'rules' => [
                [
                    'criteria' => [
                        'PE000FA0S5Z34LAEC',
                        'PE000FA142N6OLAEC',
                    ],
                    'type' => 'sku',
                ],
            ],
            'title' => 'test-manual-sku',
            'type' => 'manual',
        ];

        $expected = [
            'type' => 'product',
            'products' => [
                [
                    'title' => 'Conjunto deportivo Peregrine NEGRO',
                    'brand' => 'peregrine',
                    'originalPrice' => 58.0,
                    'percentageOff' => 0.0,
                    'price' => 58.0,
                    'image' => 'http://media.linio.com.ec/p/peregrine-6810-47308-1-product.jpg',
                    'color' => null,
                    'sku' => 'PE000FA0S5Z34LAEC',
                    'type' => 'product',
                    'href' => '/p/conjunto-deportivo-peregrine-negro-rzx5eq',
                    'slugs' => [
                        'product' => 'conjunto-deportivo-peregrine-negro-rzx5eq',
                    ],
                    'ids' => [
                        'product' => 'PE000FA0S5Z34LAEC',
                    ],
                    'query' => null,
                ],
                [
                    'title' => 'Conjunto deportivo Peregrine NEGRO con VERDE NEON',
                    'brand' => 'peregrine',
                    'originalPrice' => 58.0,
                    'percentageOff' => 0.0,
                    'price' => 58.0,
                    'image' => 'http://media.linio.com.ec/p/peregrine-6810-67308-1-product.jpg',
                    'color' => null,
                    'sku' => 'PE000FA142N6OLAEC',
                    'type' => 'product',
                    'href' => '/p/conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6',
                    'slugs' => [
                        'product' => 'conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6',
                    ],
                    'ids' => [
                        'product' => 'PE000FA142N6OLAEC',
                    ],
                    'query' => null,
                ],
            ],
        ];

        $simple = new Simple();
        $simple->setOriginalPrice(Money::fromCents(5800));
        $simple->setPrice(Money::fromCents(5800));

        $image1 = new Image();
        $image1->setSlug('//media.linio.com.ec/p/peregrine-6810-47308-1');

        $image2 = new Image();
        $image2->setSlug('//media.linio.com.ec/p/peregrine-6810-67308-1');

        $brand = new Brand();
        $brand->setName('peregrine');

        $product1 = new Product('PE000FA0S5Z34LAEC');
        $product1->setName('Conjunto deportivo Peregrine NEGRO');
        $product1->setBrand($brand);
        $product1->setOriginalPrice(Money::fromCents(58));
        $product1->setSlug('conjunto-deportivo-peregrine-negro-rzx5eq');
        $product1->addSimple($simple);
        $product1->addImage($image1);

        $product2 = new Product('PE000FA142N6OLAEC');
        $product2->setName('Conjunto deportivo Peregrine NEGRO con VERDE NEON');
        $product2->setBrand($brand);
        $product2->setOriginalPrice(Money::fromCents(58));
        $product2->setSlug('conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6');
        $product2->addSimple($simple);
        $product2->addImage($image2);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySkus(['PE000FA0S5Z34LAEC', 'PE000FA142N6OLAEC'])->willReturn([$product1, $product2]);

        $router = $this->prophesize(RouterInterface::class);

        $router->generate(
            'frontend.catalog.detail',
            ['productSlug' => $product1->getSlug()],
            RouterInterface::ABSOLUTE_URL
        )->willReturn('http://linio.com/p/conjunto-deportivo-peregrine-negro-rzx5eq');

        $router->generate(
            'frontend.catalog.detail',
            ['productSlug' => $product2->getSlug()],
            RouterInterface::ABSOLUTE_URL
        )->willReturn('http://linio.com/p/conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6');

        $urlData = [
            'type' => 'product',
            'href' => '/p/conjunto-deportivo-peregrine-negro-rzx5eq',
            'slugs' => [
                'product' => 'conjunto-deportivo-peregrine-negro-rzx5eq',
            ],
            'ids' => [
                'product' => 'PE000FA0S5Z34LAEC',
            ],
            'query' => null,
        ];

        $urlData2 = [
            'type' => 'product',
            'href' => '/p/conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6',
            'slugs' => [
                'product' => 'conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6',
            ],
            'ids' => [
                'product' => 'PE000FA142N6OLAEC',
            ],
            'query' => null,
        ];

        $urlMapper = $this->prophesize(UrlMapper::class);

        $urlMapper->map([
            'type' => 'customized',
            'href' => 'http://linio.com/p/conjunto-deportivo-peregrine-negro-rzx5eq',
        ])->willReturn($urlData);

        $urlMapper->map([
            'type' => 'customized',
            'href' => 'http://linio.com/p/conjunto-deportivo-peregrine-negro-con-verde-neon-oougf6',
        ])->willReturn($urlData2);

        $mapper = new RecommendationMapper($productService->reveal(), $urlMapper->reveal(), $router->reveal());

        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('http');

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $mapper->setRequestStack($requestStack->reveal());

        $actual = $mapper->map($recommendationData);

        $this->assertEquals($expected, $actual);
    }
}
