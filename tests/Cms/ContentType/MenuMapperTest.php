<?php

namespace Linio\Frontend\Cms\ContentType;

use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Entity\Catalog\Category;
use PHPUnit_Framework_TestCase;
use Symfony\Component\Routing\RouterInterface;

class MenuMapperTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function categoryMenuProvider()
    {
        $menuFromCache = [
            'type' => 'category',
            'attributes' => [
                'category_id' => 1,
                'label' => 'Category name label',
            ],
            'children' => [],
        ];

        $mappedMenu = [
            'type' => 'category',
            'label' => 'Category name label',
            'href' => '/c/category-name',
            'children' => [],
        ];

        $customTypeFromCache = [
            'attributes' => [
                'href' => 'http://www.linio.com.mx/cm/buen-fin',
                'label' => 'Ver Todo',
            ],
            'type' => 'customized',
        ];

        $mappedCustomType = [
            'type' => 'customized',
            'label' => 'Ver Todo',
            'href' => 'http://www.linio.com.mx/cm/buen-fin',
            'children' => [],
        ];

        return [
            [$mappedMenu, $menuFromCache],
            [$mappedCustomType, $customTypeFromCache],
        ];
    }

    /**
     * @dataProvider categoryMenuProvider
     *
     * @param array $mappedMenu
     * @param array $menu
     */
    public function testIsMappingCategoryMenu(array $mappedMenu, array $menu)
    {
        $category = new Category();
        $category->setSlug('category-name');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(1)->willReturn($category);

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.catalog.index.category', [
            'category' => 'category-name',
        ])->willReturn('/c/category-name');

        $mapper = new MenuMapper(
            $categoryService->reveal(),
            $router->reveal()
        );

        $actual = $mapper->map($menu);

        $this->assertEquals($mappedMenu, $actual);
    }

    /**
     * @return array
     */
    public function categoryMenuWithNonExistentCategoryProvider()
    {
        $menuFromCache = [
            'type' => 'category',
            'attributes' => [
                'category_id' => 1,
                'label' => 'Category name label',
            ],
            'children' => [],
        ];

        $mappedMenu = [
            'type' => 'category',
            'label' => 'Category name label',
            'href' => '/',
            'children' => [],
        ];

        return [
            [$mappedMenu, $menuFromCache],
        ];
    }

    /**
     * @dataProvider categoryMenuWithNonExistentCategoryProvider
     *
     * @param array $mappedMenu
     * @param array $menu
     */
    public function testMappingCategoryMenuDetectingCategoryNotFound(array $mappedMenu, array $menu)
    {
        $category = new Category();
        $category->setSlug('category-name');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(1)
            ->willThrow(new CategoryNotFoundException('Category id=(1) not found.'));

        $router = $this->prophesize(RouterInterface::class);
        $router->generate(
            'frontend.default.index')
            ->willReturn('/');

        $mapper = new MenuMapper($categoryService->reveal(), $router->reveal());

        $actual = $mapper->map($menu);

        $this->assertEquals($mappedMenu, $actual);
    }
}
