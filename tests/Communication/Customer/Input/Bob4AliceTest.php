<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Input;

use DateTime;
use Linio\Frontend\Customer\Order\Invoice\Invoice;
use Linio\Frontend\Customer\Order\Invoice\InvoiceResult;
use Linio\Frontend\Entity\Customer\Wallet\Points;
use Linio\Frontend\Entity\Customer\Wallet\Transaction;
use Linio\Frontend\Entity\Customer\Wallet\TransactionCollection;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;
use PHPUnit_Framework_TestCase;

class Bob4AliceTest extends PHPUnit_Framework_TestCase
{
    public function testIsMappingToAWallet()
    {
        $responseBody = [
            'accountNumber' => '123456',
            'customerId' => 1,
            'balance' => 270000,
            'totalWithdraw' => 30000,
            'totalDeposit' => 300000,
            'totalExpired' => 100000,
            'transactions' => [
                [
                    'id' => 1,
                    'subscriptionId' => 1,
                    'amount' => 1000,
                    'type' => 'deposit',
                    'status' => 'active',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => '2016-07-01 00:00:00',
                ],
                [
                    'id' => 2,
                    'subscriptionId' => 1,
                    'amount' => 2000,
                    'type' => 'deposit',
                    'status' => 'active',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => '2016-07-01 00:00:00',
                ],
                [
                    'id' => 3,
                    'subscriptionId' => 1,
                    'amount' => 100,
                    'type' => 'withdraw',
                    'status' => 'active',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => '2016-07-01 00:00:00',
                ],
                [
                    'id' => 4,
                    'subscriptionId' => 1,
                    'amount' => 200,
                    'type' => 'withdraw',
                    'status' => 'active',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => '2016-07-01 00:00:00',
                ],
                [
                    'id' => 5,
                    'subscriptionId' => 1,
                    'amount' => 1000,
                    'type' => 'deposit',
                    'status' => 'pending',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => '2016-07-01 00:00:00',
                ],
                [
                    'id' => 6,
                    'subscriptionId' => 1,
                    'amount' => 1000,
                    'type' => 'deposit',
                    'status' => 'expired',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => null,
                ],
                [
                    'id' => 7,
                    'subscriptionId' => 1,
                    'amount' => 1000,
                    'type' => 'deposit',
                    'status' => 'reverse',
                    'date' => '2016-06-01 00:00:00',
                    'expirationDate' => null,
                ],
            ],
        ];

        $transactionsCollection = new TransactionCollection();

        $transaction1 = new Transaction(
            new DateTime($responseBody['transactions'][0]['date']),
            Points::fromCents(0),
            Points::fromCents($responseBody['transactions'][0]['amount']),
            $responseBody['transactions'][0]['status'],
            Points::fromCents(1000),
            new DateTime($responseBody['transactions'][0]['expirationDate'])
        );
        $transactionsCollection->add($transaction1);

        $transaction2 = new Transaction(
            new DateTime($responseBody['transactions'][1]['date']),
            Points::fromCents(0),
            Points::fromCents($responseBody['transactions'][1]['amount']),
            $responseBody['transactions'][1]['status'],
            Points::fromCents(3000),
            new DateTime($responseBody['transactions'][1]['expirationDate'])
        );
        $transactionsCollection->add($transaction2);

        $transaction3 = new Transaction(
            new DateTime($responseBody['transactions'][2]['date']),
            Points::fromCents($responseBody['transactions'][2]['amount']),
            Points::fromCents(0),
            $responseBody['transactions'][2]['status'],
            Points::fromCents(2900),
            new DateTime($responseBody['transactions'][2]['expirationDate'])
        );
        $transactionsCollection->add($transaction3);

        $transaction4 = new Transaction(
            new DateTime($responseBody['transactions'][3]['date']),
            Points::fromCents($responseBody['transactions'][3]['amount']),
            Points::fromCents(0),
            $responseBody['transactions'][3]['status'],
            Points::fromCents(2700),
            new DateTime($responseBody['transactions'][3]['expirationDate'])
        );
        $transactionsCollection->add($transaction4);

        $transaction5 = new Transaction(
            new DateTime($responseBody['transactions'][4]['date']),
            Points::fromCents(0),
            Points::fromCents($responseBody['transactions'][4]['amount']),
            'expired',
            Points::fromCents(3700)
        );
        $transactionsCollection->add($transaction5);

        $expected = new Wallet();
        $expected->setAccount($responseBody['accountNumber']);
        $expected->setAccruedPoints(Points::fromCents($responseBody['totalDeposit']));
        $expected->setUsedPoints(Points::fromCents($responseBody['totalWithdraw']));
        $expected->setExpiredPoints(Points::fromCents($responseBody['totalExpired']));
        $expected->setBalance(Points::fromCents($responseBody['balance']));
        $expected->setTransactions($transactionsCollection);

        $input = new Bob4Alice();

        $actual = $input->toWallet($responseBody);

        $this->assertEquals($expected, $actual);
    }

    public function testIsMappingToAnInvoiceResult()
    {
        $invoiceData = [
            2304820 => [
                'statusId' => 1,
                'description' => 'Invoice 1',
                'pdf' => 'pdf contents',
                'xml' => 'eG1sdGVzdA==',
            ],
            20938490238 => [
                'statusId' => 1,
                'description' => 'Invoice 2',
                'pdf' => 'pdf contents',
                'xml' => 'eG1sdGVzdA==',
            ],
            1807438923 => [
                'statusId' => 1,
                'description' => 'Invoice 2',
                'pdf' => 'pdf contents',
                'xml' => 'eG1sdGVzdA==',
            ],
        ];

        $invoiceResult = new InvoiceResult();

        $invoice1 = new Invoice('2304820', 'xmltest', $invoiceData[2304820]['pdf']);
        $invoiceResult->addInvoice($invoice1);

        $invoice2 = new Invoice('20938490238', 'xmltest', $invoiceData[20938490238]['pdf']);
        $invoiceResult->addInvoice($invoice2);

        $invoice3 = new Invoice('1807438923', 'xmltest', $invoiceData[1807438923]['pdf']);
        $invoiceResult->addInvoice($invoice3);

        $input = new Bob4Alice();

        $actual = $input->toInvoiceResult($invoiceData);

        $this->assertEquals($invoiceResult, $actual);
    }
}
