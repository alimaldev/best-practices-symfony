<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Linio\Component\Util\Json;
use Linio\Frontend\Api\Output\AddressOutput;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Communication\Customer\Exception\UnregisteredWalletException;
use Linio\Frontend\Communication\Customer\Input\CustomerInput;
use Linio\Frontend\Customer\Communication\Coupon\Input\CustomerCouponInput;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\Security\OAuth\OauthToken;
use Linio\Frontend\Test\CommunicationTestCase;
use Linio\Frontend\Test\CustomerCouponsResponseFixture;
use Linio\Frontend\Test\CustomerLoginResponseFixture;
use Linio\Frontend\Test\CustomerUpdateResponseFixture;

class Bob4AliceAdapterTest extends CommunicationTestCase
{
    public function testIsUpdatingCustomerInformation()
    {
        $customerFixture = new CustomerUpdateResponseFixture();

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($this->createMockBob4AliceClient($customerFixture->getJson()));

        $adapter->update($customerFixture->getCustomer());
    }

    public function testIsThrowingCustomerExceptionOnClientErrorDuringCustomerUpdating()
    {
        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($this->createMockBob4AliceClientThatThrowsClientException('{"code":"foo"}', '/customer/update'));

        $this->expectException(CustomerException::class);

        $adapter->update((new CustomerUpdateResponseFixture())->getCustomer());
    }

    public function testIsThrowingCustomerExceptionOnServerErrorDuringCustomerUpdating()
    {
        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($this->createMockBob4AliceClientThatThrowsServerException('{"code":"foo"}', '/customer/update'));

        $this->expectException(CustomerException::class);

        $adapter->update((new CustomerUpdateResponseFixture())->getCustomer());
    }

    public function testIsLoggingInUsingOAuth()
    {
        $token = new OauthToken(
            'janis@linio.com',
            'facebook',
            'janis_joplin',
            'Janis',
            'Joplin'
        );

        $bob4AliceResponse = new CustomerLoginResponseFixture();

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($this->createMockBob4AliceClient($bob4AliceResponse->getJson()));

        $actual = $adapter->loginOAuth($token);

        $this->assertEquals($bob4AliceResponse->getCustomer(), $actual);
    }

    public function testIsThrowingAnInvalidLoginCredentialsExceptionWhenAServerErrorOccurs()
    {
        $client = $this->createMockBob4AliceClientThatThrowsServerException('{"code":"foo"}', '/auth/login-oauth');

        $token = new OauthToken(
            'janis@linio.com',
            'facebook',
            'janis_joplin',
            'Janis',
            'Joplin'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(InvalidLoginCredentialsException::class);
        $this->expectExceptionMessage('foo');

        $adapter->loginOAuth($token);
    }

    public function testIsThrowingAnInvalidLoginCredentialsExceptionWhenAClientErrorOccurs()
    {
        $client = $this->createMockBob4AliceClientThatThrowsClientException('{"code":"foo"}', '/auth/login-oauth');

        $token = new OauthToken(
            'janis@linio.com',
            'facebook',
            'janis_joplin',
            'Janis',
            'Joplin'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(InvalidLoginCredentialsException::class);
        $this->expectExceptionMessage('foo');

        $adapter->loginOAuth($token);
    }

    public function testIsReturningCustomerCoupons()
    {
        $customer = new Customer();
        $customer->setId(10);

        $customerCouponsFixture = new CustomerCouponsResponseFixture();
        $customerCouponInput = $this->prophesize(CustomerCouponInput::class);
        $customerCouponInput->toCoupons($customerCouponsFixture->getResponse())
            ->willReturn($customerCouponsFixture->getCoupons());

        $client = $this->createMockBob4AliceClient($customerCouponsFixture->getJson());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setCustomerCouponInput($customerCouponInput->reveal());
        $adapter->setClient($client);

        $actual = $adapter->getCoupons($customer);

        $this->assertEquals($customerCouponsFixture->getCoupons(), $actual);
    }

    public function testIsThrowingCustomerExceptionOnClientErrorWhenGetsCustomerCoupons()
    {
        $customer = new Customer();
        $customer->setId(10);

        $client = $this->createMockBob4AliceClientThatThrowsClientException('{"code":"foo"}', '/coupon/list');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('foo');

        $adapter->getCoupons($customer);
    }

    public function testIsThrowingCustomerExceptionOnServerErrorWhenGetsCustomerCoupons()
    {
        $customer = new Customer();
        $customer->setId(10);

        $client = $this->createMockBob4AliceClientThatThrowsServerException('{"code":"foo"}', '/coupon/list');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('foo');

        $adapter->getCoupons($customer);
    }

    public function testIsGettingWallet()
    {
        $customer = new Customer();
        $customer->setId(1);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'accountNumber' => 12345,
            'totalDeposit' => 1,
            'totalWithdraw' => 1,
            'totalExpired' => 1,
            'balance' => 1,
            'transactions' => [
                [
                    'id' => 1,
                    'amount' => 1,
                    'type' => 'charge',
                    'status' => '',
                    'subscriptionId' => 100,
                    'date' => '2016-05-03 00:00:00',
                    'expirationDate' => '2016-06-03 00:00:00',
                ],
            ],
        ];

        $response = new Response(200, [], json_encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/wallet/statement', ['json' => $requestBody])->shouldBeCalled()->willReturn($response);

        $customerInput = $this->prophesize(CustomerInput::class);
        $customerInput->toWallet($responseBody)->shouldBeCalled();

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());
        $adapter->setCustomerInput($customerInput->reveal());

        $adapter->getWallet($customer);
    }

    public function testGetWalletFailsWhenTheRequestIsInvalid()
    {
        $customer = new Customer();
        $customer->setId(0);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $response = new Response(400, [], json_encode($responseBody));

        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/wallet/statement', ['json' => $requestBody])->shouldBeCalled()->willThrow($exception->reveal());

        $customerInput = $this->prophesize(CustomerInput::class);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());
        $adapter->setCustomerInput($customerInput->reveal());

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->getWallet($customer);
    }

    public function testGetWalletFailsWhenAWalletIsNotFound()
    {
        $customer = new Customer();
        $customer->setId(0);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'code' => 'WALLET_NOT_FOUND',
            'message' => '',
            'errors' => [],
        ];

        $response = new Response(400, [], json_encode($responseBody));

        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/wallet/statement', ['json' => $requestBody])->shouldBeCalled()->willThrow($exception->reveal());

        $customerInput = $this->prophesize(CustomerInput::class);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setClient($client->reveal());
        $adapter->setCustomerInput($customerInput->reveal());
        $adapter->setPlatformInformation($platformInformation);

        $this->expectException(UnregisteredWalletException::class);
        $this->expectExceptionMessage('WALLET_NOT_FOUND');

        $adapter->getWallet($customer);
    }

    public function testGetWalletFailsWhenAServerErrorOccurs()
    {
        $customer = new Customer();
        $customer->setId(0);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $response = new Response(500, [], json_encode($responseBody));

        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/wallet/statement', ['json' => $requestBody])->shouldBeCalled()->willThrow($exception->reveal());

        $customerInput = $this->prophesize(CustomerInput::class);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());
        $adapter->setCustomerInput($customerInput->reveal());

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->getWallet($customer);
    }

    public function testGetsCustomerErrorOnServerErrorWhenCallsResetPassword()
    {
        $client = $this->createMockBob4AliceClientThatThrowsServerException('{"code":"foo"}', '/auth/reset-password');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('foo');

        $adapter->resetPassword('token', 'customer-501@linio.com', 'password');
    }

    public function testGetsCustomerErrorOnClientErrorWhenCallsResetPassword()
    {
        $client = $this->createMockBob4AliceClientThatThrowsClientException('{"code":"foo"}', '/auth/reset-password');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('foo');

        $adapter->resetPassword('token', 'customer-501@linio.com', 'password');
    }

    public function testCallsResetPasswordWithoutErrors()
    {
        $requestBody = [
            'storeId' => 1,
            'email' => 'customer-501@linio.com',
            'resetToken' => 'token',
            'newPassword' => 'password',
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/auth/reset-password', [
            'json' => $requestBody,
        ])->shouldBeCalled();

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());

        $adapter->resetPassword('token', 'customer-501@linio.com', 'password');
    }

    public function testItRequestsAnInvoice()
    {
        $addressBody = [
            'id' => 1,
            'title' => null,
            'type' => null,
            'firstName' => null,
            'lastName' => null,
            'prefix' => null,
            'line1' => null,
            'line2' => null,
            'betweenStreet1' => null,
            'betweenStreet2' => null,
            'streetNumber' => null,
            'apartment' => null,
            'lot' => null,
            'neighborhood' => null,
            'department' => null,
            'municipality' => null,
            'urbanization' => null,
            'city' => null,
            'cityId' => null,
            'cityName' => null,
            'region' => null,
            'regionId' => null,
            'regionCode' => null,
            'regionName' => null,
            'postcode' => null,
            'additionalInformation' => null,
            'phone' => null,
            'mobilePhone' => null,
            'countryId' => null,
            'countryCode' => null,
            'countryName' => null,
            'taxIdentificationNumber' => null,
            'defaultBilling' => null,
            'defaultShipping' => null,
            'maternalName' => null,
            'invoiceType' => null,
            'company' => null,
            'createdAt' => null,
            'updatedAt' => null,
            'isBilling' => true,
        ];

        $requestBody = [
            'orderId' => '123456',
            'address' => $addressBody,
        ];

        $responseBody = [
            2304820 => [
                'statusId' => 1,
                'description' => 'Invoice 1',
                'pdf' => 'pdf contents',
                'xml' => 'base64_encoded xml',
            ],
            20938490238 => [
                'statusId' => 1,
                'description' => 'Invoice 2',
                'pdf' => 'pdf contents',
                'xml' => 'base64_encoded xml',
            ],
            1807438923 => [
                'statusId' => 1,
                'description' => 'Invoice 2',
                'pdf' => 'pdf contents',
                'xml' => 'base64_encoded xml',
            ],
        ];

        $billingAddress = new Address();
        $billingAddress->setId(1);

        $customerInput = $this->prophesize(CustomerInput::class);
        $customerInput->toInvoiceResult($responseBody)->shouldBeCalled();

        $addressOutput = $this->prophesize(AddressOutput::class);
        $addressOutput->fromAddress($billingAddress)->shouldBeCalled()->willReturn($addressBody);

        $response = new Response(200, [], json_encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/invoice/create', ['json' => $requestBody])->shouldBeCalled()->willReturn($response);

        $adapter = new Bob4AliceAdapter();
        $adapter->setCustomerInput($customerInput->reveal());
        $adapter->setAddressOutput($addressOutput->reveal());
        $adapter->setClient($client->reveal());

        $adapter->requestInvoice($requestBody['orderId'], $billingAddress);
    }

    public function testItFailsRequestingAnInvoiceWithAClientError()
    {
        $addressBody = [
            'id' => 1,
            'title' => null,
            'type' => null,
            'firstName' => null,
            'lastName' => null,
            'prefix' => null,
            'line1' => null,
            'line2' => null,
            'betweenStreet1' => null,
            'betweenStreet2' => null,
            'streetNumber' => null,
            'apartment' => null,
            'lot' => null,
            'neighborhood' => null,
            'department' => null,
            'municipality' => null,
            'urbanization' => null,
            'city' => null,
            'cityId' => null,
            'cityName' => null,
            'region' => null,
            'regionId' => null,
            'regionCode' => null,
            'regionName' => null,
            'postcode' => null,
            'additionalInformation' => null,
            'phone' => null,
            'mobilePhone' => null,
            'countryId' => null,
            'countryCode' => null,
            'countryName' => null,
            'taxIdentificationNumber' => null,
            'defaultBilling' => null,
            'defaultShipping' => null,
            'maternalName' => null,
            'invoiceType' => null,
            'createdAt' => null,
            'updatedAt' => null,
            'isBilling' => true,
        ];

        $requestBody = [
            'orderId' => 'invalid',
            'address' => $addressBody,
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => null,
            'errors' => [],
        ];

        $billingAddress = new Address();
        $billingAddress->setId(1);

        $customerInput = $this->prophesize(CustomerInput::class);
        $customerInput->toInvoiceResult($responseBody)->shouldNotBeCalled();

        $addressOutput = $this->prophesize(AddressOutput::class);
        $addressOutput->fromAddress($billingAddress)->shouldBeCalled()->willReturn($addressBody);

        $response = new Response(400, [], json_encode($responseBody));

        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/invoice/create', ['json' => $requestBody])->shouldBeCalled()->willThrow($exception->reveal());

        $adapter = new Bob4AliceAdapter();
        $adapter->setCustomerInput($customerInput->reveal());
        $adapter->setAddressOutput($addressOutput->reveal());
        $adapter->setClient($client->reveal());

        $this->expectException(OrderException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->requestInvoice($requestBody['orderId'], $billingAddress);
    }

    public function testItFailsRequestingAnInvoiceWithAServerError()
    {
        $addressBody = [
            'id' => 1,
            'title' => null,
            'type' => null,
            'firstName' => null,
            'lastName' => null,
            'prefix' => null,
            'line1' => null,
            'line2' => null,
            'betweenStreet1' => null,
            'betweenStreet2' => null,
            'streetNumber' => null,
            'apartment' => null,
            'lot' => null,
            'neighborhood' => null,
            'department' => null,
            'municipality' => null,
            'urbanization' => null,
            'city' => null,
            'cityId' => null,
            'cityName' => null,
            'region' => null,
            'regionId' => null,
            'regionCode' => null,
            'regionName' => null,
            'postcode' => null,
            'additionalInformation' => null,
            'phone' => null,
            'mobilePhone' => null,
            'countryId' => null,
            'countryCode' => null,
            'countryName' => null,
            'taxIdentificationNumber' => null,
            'defaultBilling' => null,
            'defaultShipping' => null,
            'maternalName' => null,
            'invoiceType' => null,
            'company' => null,
            'createdAt' => null,
            'updatedAt' => null,
            'isBilling' => true,
        ];

        $requestBody = [
            'orderId' => 'invalid',
            'address' => $addressBody,
        ];

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => null,
            'errors' => [],
        ];

        $billingAddress = new Address();
        $billingAddress->setId(1);

        $customerInput = $this->prophesize(CustomerInput::class);
        $customerInput->toInvoiceResult($responseBody)->shouldNotBeCalled();

        $addressOutput = $this->prophesize(AddressOutput::class);
        $addressOutput->fromAddress($billingAddress)->shouldBeCalled()->willReturn($addressBody);

        $response = new Response(500, [], json_encode($responseBody));

        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/invoice/create', ['json' => $requestBody])->shouldBeCalled()->willThrow($exception->reveal());

        $adapter = new Bob4AliceAdapter();
        $adapter->setCustomerInput($customerInput->reveal());
        $adapter->setAddressOutput($addressOutput->reveal());
        $adapter->setClient($client->reveal());

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->requestInvoice($requestBody['orderId'], $billingAddress);
    }

    public function testIsCreatingAnItemReturnRequest()
    {
        $customer = new Customer();
        $customer->setId(1);

        $order = new Order(1);
        $order->setOrderNumber('1234567');

        $returnData = [
            'orderNumber' => $order->getOrderNumber(),
            'sku' => 'SKU1-1',
            'quantity' => 2,
            'reason' => 1,
            'action' => 1,
            'comment' => 'COMMENT-1',
        ];

        $requestBody = [
            'storeId' => 1,
            'orderId' => $order->getId(),
            'simpleSku' => $returnData['sku'],
            'quantity' => $returnData['quantity'],
            'reasonId' => $returnData['reason'],
            'actionId' => $returnData['action'],
            'comment' => $returnData['comment'],
        ];

        $response = new Response(200, [], Json::encode([
            'success_automatic',
            'success_manual',
        ]));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/item/return', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation(new PlatformInformation());
        $adapter->setClient($client->reveal());

        $actual = $adapter->createItemReturnRequest($customer, $order, $returnData);

        $this->assertNull($actual);
    }

    public function testIsFailingWhenCreatingAnItemReturnRequestAtLeast1()
    {
        $customer = new Customer();
        $customer->setId(1);

        $order = new Order(1);
        $order->setOrderNumber('1234567');

        $returnData = [
            'orderNumber' => $order->getOrderNumber(),
            'sku' => 'SKU1-1',
            'quantity' => 2,
            'reason' => 1,
            'action' => 1,
            'comment' => 'COMMENT-1',
        ];

        $requestBody = [
            'storeId' => 1,
            'orderId' => $order->getId(),
            'simpleSku' => $returnData['sku'],
            'quantity' => $returnData['quantity'],
            'reasonId' => $returnData['reason'],
            'actionId' => $returnData['action'],
            'comment' => $returnData['comment'],
        ];

        $response = new Response(200, [], Json::encode([
            'success_automatic',
            'fail',
        ]));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/item/return', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation(new PlatformInformation());
        $adapter->setClient($client->reveal());

        $this->expectException(OrderException::class);

        $adapter->createItemReturnRequest($customer, $order, $returnData);
    }

    public function testItFailsWhenCreatingAnItemReturnRequestWithAnInvalidRequest()
    {
        $customer = new Customer();
        $customer->setId(1);

        $order = new Order(1);
        $order->setOrderNumber('1234567');

        $returnData = [
            'orderNumber' => $order->getOrderNumber(),
            'sku' => 'SKU1-1',
            'quantity' => 2,
            'reason' => 1,
            'action' => 1,
            'comment' => 'COMMENT-1',
        ];

        $requestBody = [
            'storeId' => 0,
            'orderId' => $order->getId(),
            'simpleSku' => $returnData['sku'],
            'quantity' => $returnData['quantity'],
            'reasonId' => $returnData['reason'],
            'actionId' => $returnData['action'],
            'comment' => $returnData['comment'],
        ];

        $response = new Response(400, [], Json::encode([
            'code' => 'INVALID_REQUEST',
            'message' => null,
            'errors' => [],
        ]));

        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/item/return', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(0);

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->createItemReturnRequest($customer, $order, $returnData);
    }

    public function testItFailsWhenCreatingAnItemReturnRequestAndAServerErrorOccurs()
    {
        $customer = new Customer();
        $customer->setId(1);

        $order = new Order(1);
        $order->setOrderNumber('1234567');

        $returnData = [
            'orderNumber' => $order->getOrderNumber(),
            'sku' => 'SKU1-1',
            'quantity' => 2,
            'reason' => 1,
            'action' => 1,
            'comment' => 'COMMENT-1',
        ];

        $requestBody = [
            'storeId' => 1,
            'orderId' => $order->getId(),
            'simpleSku' => $returnData['sku'],
            'quantity' => $returnData['quantity'],
            'reasonId' => $returnData['reason'],
            'actionId' => $returnData['action'],
            'comment' => $returnData['comment'],
        ];

        $response = new Response(500, [], Json::encode([
            'code' => 'AN_ERROR_OCCURRED',
            'message' => null,
            'errors' => [],
        ]));

        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/item/return', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $adapter = new Bob4AliceAdapter();
        $adapter->setPlatformInformation(new PlatformInformation());
        $adapter->setClient($client->reveal());

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->createItemReturnRequest($customer, $order, $returnData);
    }
}
