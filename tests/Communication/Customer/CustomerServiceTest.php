<?php

namespace Linio\Frontend\Communication\Customer;

use DateTime;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;
use Linio\Frontend\Event\CustomerPasswordChangedEvent;
use Linio\Frontend\Security\AuthenticationToken;
use Linio\Frontend\Security\OAuth\OauthToken;
use Linio\Frontend\Security\User;
use Prophecy\Argument;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CustomerServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CustomerService
     */
    protected $customerService;

    /**
     * @var CustomerAdapterInterface
     */
    protected $customerAdapterMock;

    /**
     * @return array
     */
    public function providerCustomerData()
    {
        $customer = new Customer();
        $customer->setEmail(sprintf('customer-email-%d@enterprise-%d.de', rand(1, 000), rand(1, 000)));
        $customer->setPassword('customer-password');
        $customer->setFirstName('firstname');
        $customer->setMiddleName('middlename');
        $customer->setLastName('lastname');
        $customer->setBornDate(new DateTime('1970-01-01'));
        $customer->setNationalRegistrationNumber('customer dni');
        $customer->setTaxIdentificationNumber('tax identification number');
        $customer->setAcceptTerms(true);
        $customer->setIpAddress(sprintf('%d.%d.%d.%d', rand(1, 254), rand(1, 254), rand(1, 254), rand(1, 254)));

        $locale = 'es_mx';
        $id_customer = rand(rand(1, 100), rand(101, 1000));
        $created_at = date('Y-m-d h:i:s');
        $updated_at = $created_at;

        $expected = [
            'id_customer' => $id_customer,
            'email' => $customer->getEmail(),
            'increment_id' => rand(100000000, 200000000),
            'first_name' => $customer->getFirstName(),
            'middle_name' => $customer->getMiddleName(),
            'last_name' => $customer->getLastName(),
            'birthday' => $customer->getBornDate()->format('Y-m-d H:i:s'),
            'gender' => $customer->getGender(),
            'password' => md5($customer->getPassword()),
            'is_confirmed' => 1,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'national_registration_number' => $customer->getNationalRegistrationNumber(),
            'tax_identification_number' => $customer->getTaxIdentificationNumber(),
            'accepted_terms' => $customer->hasAcceptTerms(),
            'account_creation' => 1,
            'fk_language' => 1,
            'closed_transactions_cc' => 0,
            'closed_transactions_opm' => 0,
            'fraudlost_transactions' => 0,
            'fastlane_settings' => ['_locale' => $locale],
            'recommendation_settings' => [
                'hashkey' => 'cb54906ba97ffc1d30f600eac07fc2b8',
                'fk_customer' => $id_customer,
                'id_customer_recommendation_settings' => $id_customer,
                'blacklist' => 0,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                '_locale' => $locale,
            ],
            'prudsys_settings' => [
                'fk_customer' => $id_customer,
                '_locale' => $locale,
            ],

            '_locale' => $locale,
        ];

        return [
            [$customer, $expected],
        ];
    }

    public function testIsCreatingCustomerWithSucess()
    {
        $customerAdapterMock = $this->createMock(CustomerAdapterInterface::class);

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapterMock);

        $customerAdapterMock = $this->prophesize(CustomerAdapterInterface::class);
        $customerAdapterMock->create(Argument::type(Customer::class))
            ->shouldBeCalled();

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapterMock->reveal());
        $customerService->create(new Customer());
    }

    public function testIsLoggingWithSuccess()
    {
        $token = new AuthenticationToken('test@test.com', 'secret', 'default', ['ROLE_USER']);

        $customerAdapterMock = $this->createMock(CustomerAdapterInterface::class);

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapterMock);

        $customerAdapterMock
            ->expects($this->once())
            ->method('login')
            ->with($token);

        $customerService->login($token);
    }

    /**
     * @return array
     */
    public function providerCustomerUpdateData()
    {
        $locale = 'es_mx';
        $id_customer = rand(rand(1, 100), rand(101, 1000));
        $created_at = date('Y-m-d h:i:s');
        $updated_at = $created_at;

        $customer = new Customer();
        $customer->setId($id_customer);
        $customer->setFkLanguage(rand(1, 10));
        $customer->setEmail(sprintf('customer-email-%d@enterprise-%d.de', rand(1, 000), rand(1, 000)));
        $customer->setPassword('1234');
        $customer->setGender('m');
        $customer->setFirstName('First');
        $customer->setMiddleName('Middle');
        $customer->setLastName('Last');
        $customer->setBornDate(new DateTime('1984-08-01'));
        $customer->setNationalRegistrationNumber('12345678');
        $customer->setTaxIdentificationNumber('123');
        $customer->setSubscribedToNewsletter(false);
        $customer->setIpAddress(sprintf('%d.%d.%d.%d', rand(1, 254), rand(1, 254), rand(1, 254), rand(1, 254)));

        $expected = [
            'id_customer' => $id_customer,
            'email' => $customer->getEmail(),
            'increment_id' => rand(100000000, 200000000),
            'first_name' => $customer->getFirstName(),
            'middle_name' => $customer->getMiddleName(),
            'last_name' => $customer->getLastName(),
            'birthday' => $customer->getBornDate()->format('Y-m-d H:i:s'),
            'gender' => 'male',
            'password' => md5($customer->getPassword()),
            'is_confirmed' => 1,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            'national_registration_number' => $customer->getNationalRegistrationNumber(),
            'tax_identification_number' => $customer->getTaxIdentificationNumber(),
            'accepted_terms' => $customer->hasAcceptTerms(),
            'account_creation' => 1,
            'fk_language' => $customer->getFkLanguage(),
            'autologin_token' => 'very-large-auto-login-token-hash',
            'closed_transactions_cc' => 0,
            'closed_transactions_opm' => 0,
            'fraudlost_transactions' => 0,
            'address_collection' => [
                rand(1, 10) => [
                    'id_customer_address' => rand(1, 20),
                    'first_name' => $customer->getFirstName(),
                    'last_name' => $customer->getLastName(),
                    'address1' => 'Address 1',
                    'city' => 'Test City',
                    'phone' => '11112222',
                    'fk_customer' => $customer->getId(),
                    'fk_country' => rand(1, 247),
                    'is_default_billing' => true,
                    'is_default_shipping' => true,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                    'municipality' => 'test municipality',
                    'neighborhood' => 'test neighborhood',
                    'mobile_phone' => '33334444',
                    'region' => 'test region',
                    '_locale' => $locale,
                ],
            ],
            'fastlane_settings' => [
                '_locale' => $locale,
            ],
            'recommendation_settings' => [
                'id_customer_recommendation_settings' => $id_customer,
                'hashkey' => 'cb54906ba97ffc1d30f600eac07fc2b8',
                'blacklist' => 0,
                'fk_customer' => $id_customer,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                '_locale' => $locale,
            ],
            'prudsys_settings' => [
                'fk_customer' => $id_customer,
                'rpid' => md5('hash-of-something'),
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                '_locale' => $locale,
            ],
            '_locale' => $locale,
        ];

        return [
            [$customer, $expected],
        ];
    }

    /**
     * @dataProvider providerCustomerUpdateData
     *
     * @param Customer $customer
     * @param array    $expected
     */
    public function testIsUpdatingCustomerWithSucess(Customer $customer, array $expected)
    {
        $customerAdapterMock = $this->createMock(CustomerAdapterInterface::class);

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapterMock);

        $customerAdapterMock
            ->expects($this->once())
            ->method('update')
            ->with($customer)
            ->willReturn($expected);

        $response = $customerService->update($customer);
        $this->assertEquals($expected, $response);
    }

    public function testIsUpdatingCustomerWithFailure()
    {
        $customer = new Customer();
        $exception = new CustomerException('My exception message');

        $customerAdapterMock = $this->createMock(CustomerAdapterInterface::class);

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapterMock);

        $customerAdapterMock
            ->expects($this->once())
            ->method('update')
            ->with($customer)
            ->willThrowException($exception);

        $this->expectException(CustomerException::class);
        $this->expectExceptionMessage('My exception message');

        $customerService->update($customer);
    }

    public function testIsLoggingWithFailure()
    {
        $token = new AuthenticationToken('customer1@domain.com', 'failure', 'default', ['ROLE_USER']);

        $customerAdapterMock = $this->createMock(CustomerAdapterInterface::class);

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapterMock);

        $customerAdapterMock
            ->expects($this->once())
            ->method('login')
            ->willThrowException(new InvalidLoginCredentialsException(InvalidLoginCredentialsException::CUSTOMER_LOGIN_FAILURE));

        $this->expectException(InvalidLoginCredentialsException::class);

        $customerService->login($token);
    }

    public function testIsGettingWallet()
    {
        $customer = new Customer();

        $wallet = $this->prophesize(Wallet::class);

        $customerAdapter = $this->prophesize(CustomerAdapterInterface::class);
        $customerAdapter->getWallet($customer)->willReturn($wallet->reveal());

        $customerService = new CustomerService();
        $customerService->setAdapter($customerAdapter->reveal());

        $actual = $customerService->getWallet($customer);

        $this->assertInstanceOf(Wallet::class, $actual);
    }

    public function testIsCheckingForTaxInformationWhenCountryRequiresIt()
    {
        $customer = new Customer();
        $customerService = new CustomerService();
        $customerService->setCountryRequiresId(true);

        $this->assertTrue($customerService->isTaxInformationNeeded($customer));
    }

    public function testIsCheckingForTaxInformationWhenCountryRequiresItAndUserHasInfo()
    {
        $customer1 = new Customer();
        $customer1->setNationalRegistrationNumber(123);

        $customer2 = new Customer();
        $customer2->setTaxIdentificationNumber(123);

        $customerService = new CustomerService();
        $customerService->setCountryRequiresId(true);

        $this->assertFalse($customerService->isTaxInformationNeeded($customer1));
        $this->assertFalse($customerService->isTaxInformationNeeded($customer2));
    }

    public function testIsCheckingForTaxInformationWhenCountryDoesNotRequireIt()
    {
        $customer = new Customer();
        $customerService = new CustomerService();
        $this->assertFalse($customerService->isTaxInformationNeeded($customer));
    }

    public function testIsResettingCustomerPassword()
    {
        $customer = new User();

        $adapter = $this->prophesize(CustomerAdapterInterface::class);
        $adapter->resetPassword('f7sd9f798sd', 'foo@linio.com', '1234')->shouldBeCalled();
        $adapter->getUser('foo@linio.com')->willReturn($customer);

        $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
        $eventDispatcher->dispatch(CustomerPasswordChangedEvent::NAME, Argument::type(CustomerPasswordChangedEvent::class))->shouldBeCalled();

        $customerService = new CustomerService();
        $customerService->setAdapter($adapter->reveal());
        $customerService->setEventDispatcher($eventDispatcher->reveal());

        $customerService->resetPassword('f7sd9f798sd', 'foo@linio.com', '1234');
    }

    public function testIsChangingCustomerPassword()
    {
        $customer = new User();
        $adapter = $this->prophesize(CustomerAdapterInterface::class);
        $adapter->changePassword($customer, '123', '1234')->shouldBeCalled();

        $eventDispatcher = $this->prophesize(EventDispatcherInterface::class);
        $eventDispatcher->dispatch(CustomerPasswordChangedEvent::NAME, Argument::type(CustomerPasswordChangedEvent::class))->shouldBeCalled();

        $customerService = new CustomerService();
        $customerService->setAdapter($adapter->reveal());
        $customerService->setEventDispatcher($eventDispatcher->reveal());

        $customerService->changePassword($customer, '123', '1234');
    }

    public function testIsLoggingInUsingOAuth()
    {
        $user = new User();
        $token = new OauthToken('customer@linio.com', 'facebook', 'customer', 'melissa', 'Diamond');

        $adapter = $this->prophesize(CustomerAdapterInterface::class);
        $adapter->loginOAuth($token)->willReturn($user);

        $service = new CustomerService();
        $service->setAdapter($adapter->reveal());
        $actual = $service->loginOAuth($token);

        $this->assertEquals($user, $actual);
    }
}
