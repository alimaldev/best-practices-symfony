<?php

namespace Linio\Frontend\Communication\Catalog;

use Linio\Frontend\Communication\Catalog\Exception\RatingSaveFailException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Review;

class CatalogServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsSetRatingReturningTrue()
    {
        $catalogAdapterMock = $this->prophesize(CatalogAdapterInterface::class);

        $review = new Review();
        $customer = new Customer();

        $catalogAdapterMock->setRating($customer, $review)
            ->shouldBecalled()
            ->willReturn(true);

        $catalogService = new CatalogService();
        $catalogService->setAdapter($catalogAdapterMock->reveal());

        $actual = $catalogService->setRating($customer, $review);

        $this->assertTrue($actual);
    }

    public function testIsSetRatingThrowingRatingSaveFailException()
    {
        $catalogAdapterMock = $this->prophesize(CatalogAdapterInterface::class);

        $review = new Review();
        $customer = new Customer();

        $this->expectException(RatingSaveFailException::class);

        $catalogAdapterMock->setRating($customer, $review)
            ->shouldBecalled()
            ->willThrow(new RatingSaveFailException('SOME_EXCEPTION'));

        $catalogService = new CatalogService();
        $catalogService->setAdapter($catalogAdapterMock->reveal());

        $catalogService->setRating($customer, $review);
    }
}
