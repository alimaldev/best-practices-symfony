<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Catalog\Category;

class CategoryResolverTest extends \PHPUnit_Framework_TestCase
{
    public function testIsResolving()
    {
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('1000')
            ->willReturn(['id' => 1000, 'name' => 'Category', 'slug' => 'category-slug']);

        $categoryResolver = new CategoryResolver();
        $categoryResolver->setCacheService($cacheService->reveal());

        $expected = new Category();
        $expected->setId(1000);
        $expected->setName('Category');
        $expected->setSlug('category-slug');

        $actual = $categoryResolver->resolve(['id' => 1000, 'name' => 'Category']);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @expectedException \Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException
     */
    public function testIsThrowingExceptionWhenTryResolveWithInvalidData()
    {
        $brandResolver = new CategoryResolver();
        $brandResolver->resolve(['foo' => 'bar']);
    }
}
