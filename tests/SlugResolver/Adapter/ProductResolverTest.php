<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\ProductService;

class ProductResolverTest extends \PHPUnit_Framework_TestCase
{
    public function testIsResolving()
    {
        $sku = 'SKU123456';
        $expected = new Product();
        $expected->setSku($sku);

        $productServiceMock = $this->createMock(ProductService::class);
        $productServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($sku)
            ->willReturn($expected);

        $productResolver = new ProductResolver();
        $productResolver->setProductService($productServiceMock);

        $actual = $productResolver->resolve(['sku' => $sku]);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @expectedException \Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException
     */
    public function testIsThrowingExceptionWhenTryResolveWithInvalidData()
    {
        $productResolver = new ProductResolver();
        $productResolver->resolve(['foo' => 'bar']);
    }
}
