<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Brand;

class BrandResolverTest extends \PHPUnit_Framework_TestCase
{
    public function testIsResolving()
    {
        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('brand:1000')
            ->willReturn(['id' => 1000, 'name' => 'Brand 1', 'slug' => 'brand-slug']);

        $brandResolver = new BrandResolver();
        $brandResolver->setCacheService($cacheService->reveal());

        $expected = new Brand();
        $expected->setId(1000);
        $expected->setName('Brand');
        $expected->setSlug('brand-slug');

        $actual = $brandResolver->resolve(['id' => 1000, 'name' => 'Brand']);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @expectedException \Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException
     */
    public function testIsThrowingExceptionWhenTryResolveWithInvalidData()
    {
        $brandResolver = new BrandResolver();
        $brandResolver->resolve(['foo' => 'bar']);
    }
}
