<?php

namespace Linio\Frontend\SlugResolver;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Catalog\Redirect;
use Prophecy\Argument;

class SlugResolverServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsResolving()
    {
        $resolverName = 'test';
        $resolverCacheKey = 'route:test';
        $slug = 'test-is-resolving';
        $expected = ['id' => 1000];

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('route:test:test-is-resolving')
            ->willReturn($expected);

        $resolverMock = $this->createMock(ResolverInterface::class);

        $resolverMock->method('getCacheKey')
            ->willReturn($resolverCacheKey);

        $resolverMock->method('getName')
            ->willReturn($resolverName);

        $resolverMock->method('parseSlug')
            ->willReturn($slug);

        $resolverMock->expects($this->once())
            ->method('resolve')
            ->with($expected)
            ->willReturn($expected);

        $slugResolverService = new SlugResolverService();
        $slugResolverService->setCacheService($cacheServiceMock);
        $slugResolverService->addResolver($resolverMock);

        $actual = $slugResolverService->resolve('test', $slug);

        $this->assertEquals($expected, $actual);
    }

    public function testIsAddingResolver()
    {
        $resolverName = 'test';

        $resolverMock = $this->createMock(ResolverInterface::class);
        $resolverMock->method('getName')
            ->willReturn($resolverName);

        $slugResolverService = new SlugResolverService();
        $slugResolverService->addResolver($resolverMock);

        $actual = $slugResolverService->getAvailableResolvers();

        $this->assertContains($resolverName, $actual);
    }

    /**
     * @expectedException \Linio\Frontend\SlugResolver\Exception\ResolverNotRegisteredException
     */
    public function testIsThrowingExceptionWhenUseUnregisteredResolver()
    {
        $slugResolverService = new SlugResolverService();
        $slugResolverService->resolve('unregistered', 'not-registered-resolver');
    }

    /**
     * @expectedException \Linio\Frontend\SlugResolver\Exception\SlugNotFoundException
     */
    public function testIsThrowingExceptionWhenSlugNotFound()
    {
        $resolverName = 'test';

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $resolverMock = $this->createMock(ResolverInterface::class);

        $cacheServiceMock->method('get')
            ->willReturn(null);

        $resolverMock->method('getName')
            ->willReturn($resolverName);

        $slugResolverService = new SlugResolverService();
        $slugResolverService->setCacheService($cacheServiceMock);
        $slugResolverService->addResolver($resolverMock);

        $slugResolverService->resolve($resolverName, 'slug-not-found');
    }

    public function testIsGettingAvailableResolvers()
    {
        $resolver1 = $this->createMock(ResolverInterface::class);
        $resolver1->method('getName')
            ->willReturn('resolver_a');

        $resolver2 = $this->createMock(ResolverInterface::class);
        $resolver2->method('getName')
            ->willReturn('resolver_b');

        $resolver3 = $this->createMock(ResolverInterface::class);
        $resolver3->method('getName')
            ->willReturn('resolver_c');

        $slugResolverService = new SlugResolverService();
        $slugResolverService->addResolver($resolver1);
        $slugResolverService->addResolver($resolver2);
        $slugResolverService->addResolver($resolver3);

        $expected = [
            'resolver_a',
            'resolver_b',
            'resolver_c',
        ];

        $actual = $slugResolverService->getAvailableResolvers();

        $this->assertEquals($expected, $actual);
    }

    public function testIsResolvingLegacy()
    {
        $cacheHit = ['url' => '/c/category/subcategory'];
        $redirect = new Redirect();
        $redirect->setUrl($cacheHit['url']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('route:redirect:category/subcategory')->willReturn($cacheHit);

        $resolver = $this->prophesize(ResolverInterface::class);
        $resolver->getCacheKey()->willReturn('route:redirect');
        $resolver->getName()->willReturn('redirect');
        $resolver->parseSlug('category/subcategory')->willReturn('category/subcategory');
        $resolver->resolve($cacheHit)->willReturn($redirect);

        $slugResolverService = new SlugResolverService();
        $slugResolverService->setCacheService($cacheService->reveal());
        $slugResolverService->addResolver($resolver->reveal());

        $legacyRedirect = $slugResolverService->resolveLegacy('category/subcategory');
        $this->assertEquals($redirect, $legacyRedirect);
    }

    public function testIsResolvingCombinedLegacy()
    {
        $categoryCacheHit = ['url' => '/c/category'];
        $categoryRedirect = new Redirect();
        $categoryRedirect->setUrl($categoryCacheHit['url']);

        $brandCacheHit = ['url' => '/b/brand'];
        $brandRedirect = new Redirect();
        $brandRedirect->setUrl($brandCacheHit['url']);

        $combinedRedirect = new Redirect();
        $combinedRedirect->setUrl($categoryCacheHit['url'] . $brandCacheHit['url']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('route:redirect:category/brand')->willReturn(null);
        $cacheService->get('route:redirect:category')->willReturn($categoryCacheHit);
        $cacheService->get('route:redirect:brand')->willReturn($brandCacheHit);

        $resolver = $this->prophesize(ResolverInterface::class);
        $resolver->getCacheKey()->willReturn('route:redirect');
        $resolver->getName()->willReturn('redirect');
        $resolver->parseSlug(Argument::type('string'))->willReturnArgument(0);
        $resolver->resolve(['url' => '/c/category/b/brand'])->willReturn($combinedRedirect);

        $slugResolverService = new SlugResolverService();
        $slugResolverService->setCacheService($cacheService->reveal());
        $slugResolverService->addResolver($resolver->reveal());

        $legacyRedirect = $slugResolverService->resolveLegacy('category/brand');
        $this->assertEquals($combinedRedirect, $legacyRedirect);
    }

    public function testIsResolvingSubcategoryCombinedLegacy()
    {
        $categoryCacheHit = ['url' => '/c/category'];
        $categoryRedirect = new Redirect();
        $categoryRedirect->setUrl($categoryCacheHit['url']);

        $subcategoryCacheHit = ['url' => '/c/subcategory'];
        $subcategoryRedirect = new Redirect();
        $subcategoryRedirect->setUrl($subcategoryCacheHit['url']);

        $brandCacheHit = ['url' => '/b/brand'];
        $brandRedirect = new Redirect();
        $brandRedirect->setUrl($brandCacheHit['url']);

        $combinedRedirect = new Redirect();
        $combinedRedirect->setUrl($categoryCacheHit['url'] . $subcategoryCacheHit['url'] . $brandCacheHit['url']);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('route:redirect:category/subcategory/brand')->willReturn(null);
        $cacheService->get('route:redirect:category')->willReturn($categoryCacheHit);
        $cacheService->get('route:redirect:subcategory')->willReturn($subcategoryCacheHit);
        $cacheService->get('route:redirect:brand')->willReturn($brandCacheHit);

        $resolver = $this->prophesize(ResolverInterface::class);
        $resolver->getCacheKey()->willReturn('route:redirect');
        $resolver->getName()->willReturn('redirect');
        $resolver->parseSlug(Argument::type('string'))->willReturnArgument(0);
        $resolver->resolve(['url' => '/c/category/subcategory/b/brand'])->willReturn($combinedRedirect);

        $slugResolverService = new SlugResolverService();
        $slugResolverService->setCacheService($cacheService->reveal());
        $slugResolverService->addResolver($resolver->reveal());

        $legacyRedirect = $slugResolverService->resolveLegacy('category/subcategory/brand');
        $this->assertEquals($combinedRedirect, $legacyRedirect);
    }
}
