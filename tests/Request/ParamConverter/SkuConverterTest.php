<?php

namespace Linio\Frontend\Request\ParamConverter;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class SkuConverterTest extends \PHPUnit_Framework_TestCase
{
    public function testIsConverting()
    {
        $parameter = 'product';
        $sku = 'SKU12345';
        $product = new Product($sku);

        $productServiceMock = $this->createMock(ProductService::class);
        $parameterBag = $this->createMock(ParameterBag::class);
        $requestMock = $this->createMock(Request::class);
        $requestMock->attributes = $parameterBag;
        $paramConverterMock = $this->getMockBuilder(ParamConverter::class)
            ->disableOriginalConstructor()
            ->getMock();

        $converter = new SkuConverter();
        $converter->setProductService($productServiceMock);

        $requestMock->method('get')
            ->with($parameter)
            ->willReturn($sku);

        $paramConverterMock->method('getName')
            ->willReturn($parameter);

        $productServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($sku)
            ->willReturn($product);

        $parameterBag->expects($this->once())
            ->method('set')
            ->with($parameter, $product);

        $result = $converter->apply($requestMock, $paramConverterMock);

        $this->assertTrue($result);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testIsThrowingExceptionWhenProductNotFound()
    {
        $productServiceMock = $this->prophesize(ProductService::class);
        $parameterBag = $this->createMock(ParameterBag::class);
        $headerBag = $this->prophesize(ResponseHeaderBag::class);
        $requestMock = $this->createMock(Request::class);
        $requestMock->attributes = $parameterBag;
        $requestMock->headers = $headerBag->reveal();
        $paramConverterMock = $this->getMockBuilder(ParamConverter::class)
            ->disableOriginalConstructor()
            ->getMock();

        $converter = new SkuConverter();
        $converter->setProductService($productServiceMock->reveal());

        $productServiceMock->getBySku('INVALID')->willThrow(ProductNotFoundException::class);

        $converter->apply($requestMock, $paramConverterMock);
    }
}
