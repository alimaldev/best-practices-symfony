<?php

namespace Linio\Frontend\Request\ParamConverter;

use Linio\Frontend\SlugResolver\ResolverInterface;
use Linio\Frontend\SlugResolver\SlugResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class SlugConverterTest extends \PHPUnit_Framework_TestCase
{
    public function testIsConverting()
    {
        $resolverName = 'test';
        $parameter = 'slug';
        $slug = 'test-slug-conversion';
        $expected = 'Resolved Slug';

        $request = new Request([], [], ['slug' => $slug]);

        $paramConverterMock = $this->prophesize(ParamConverter::class);
        $resolverMock = $this->prophesize(ResolverInterface::class);
        $slugResolverServiceMock = $this->prophesize(SlugResolverService::class);

        $paramConverterMock->getName()
            ->willReturn($parameter);

        $paramConverterMock->getOptions()
            ->willReturn(['resolver' => $resolverName]);

        $slugResolverServiceMock->resolve($resolverName, $slug)
            ->shouldBeCalled()
            ->willReturn($expected);

        $resolverMock->getName()
            ->willReturn($resolverName);

        $slugResolverServiceMock->addResolver($resolverMock->reveal());

        $slugConverter = new SlugConverter();
        $slugConverter->setSlugResolverService($slugResolverServiceMock->reveal());

        $actual = $slugConverter->apply($request, $paramConverterMock->reveal());

        $this->assertTrue($actual);
    }

    public function testIsConvertingWithSlash()
    {
        $resolverName = 'test';
        $parameter = 'slug';
        $slug = 'test-slug-conversion/';
        $expected = 'Resolved Slug';

        $request = new Request([], [], ['slug' => $slug]);

        $paramConverterMock = $this->prophesize(ParamConverter::class);
        $slugResolverServiceMock = $this->prophesize(SlugResolverService::class);

        $paramConverterMock->getName()
            ->willReturn($parameter);

        $paramConverterMock->getOptions()
            ->willReturn(['resolver' => $resolverName]);

        $slugResolverServiceMock->resolve($resolverName, rtrim($slug, '/'))
            ->shouldBeCalled()
            ->willReturn($expected);

        $slugConverter = new SlugConverter();
        $slugConverter->setSlugResolverService($slugResolverServiceMock->reveal());

        $actual = $slugConverter->apply($request, $paramConverterMock->reveal());

        $this->assertTrue($actual);
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testIsThrowingExceptionWhenSlugNotFound()
    {
        $request = new Request([], [], ['slug' => '']);

        $paramConverterMock = $this->prophesize(ParamConverter::class);
        $slugResolverServiceMock = $this->prophesize(SlugResolverService::class);

        $slugResolverServiceMock->resolve('INVALID', '')
            ->shouldNotBeCalled();

        $slugConverter = new SlugConverter();
        $slugConverter->setSlugResolverService($slugResolverServiceMock->reveal());

        $slugConverter->apply($request, $paramConverterMock->reveal());
    }

    /**
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function testIsThrowingExceptionWhenSlugNotFoundWithSlash()
    {
        $slug = 'test-slug-conversion/';

        $request = new Request([], [], ['slug' => $slug]);

        $paramConverterMock = $this->prophesize(ParamConverter::class);
        $slugResolverServiceMock = $this->prophesize(SlugResolverService::class);

        $slugResolverServiceMock->resolve('INVALID', '')
            ->shouldNotBeCalled();

        $slugConverter = new SlugConverter();
        $slugConverter->setSlugResolverService($slugResolverServiceMock->reveal());

        $slugConverter->apply($request, $paramConverterMock->reveal());
    }

    /**
     * @dataProvider availableResolversProvider
     *
     * @param array $resolverName
     * @param array $availableResolvers
     * @param array $expected
     */
    public function testIsCheckingAvailableResolvers($resolverName, array $availableResolvers, $expected)
    {
        $paramConverterMock = $this->getMockBuilder(ParamConverter::class)
            ->disableOriginalConstructor()
            ->getMock();

        $paramConverterMock->method('getOptions')
            ->willReturn(['resolver' => $resolverName]);

        $slugResolverServiceMock = $this->createMock(SlugResolverService::class);

        $slugResolverServiceMock->expects($this->once())
            ->method('getAvailableResolvers')
            ->willReturn($availableResolvers);

        $slugConverter = new SlugConverter();
        $slugConverter->setSlugResolverService($slugResolverServiceMock);

        $actual = $slugConverter->supports($paramConverterMock);

        $this->assertEquals($expected, $actual);
    }

    public function testIsConvertingWithMultipleParamsInRequest()
    {
        $resolverName = 'test';
        $parameter = 'slug';
        $slug = 'test-slug-conversion';
        $expected = 'Resolved Slug';

        $request = new Request([], [], ['slug' => [$slug]]);

        $paramConverterMock = $this->prophesize(ParamConverter::class);
        $resolverMock = $this->prophesize(ResolverInterface::class);
        $slugResolverServiceMock = $this->prophesize(SlugResolverService::class);

        $paramConverterMock->getName()
            ->willReturn($parameter);

        $paramConverterMock->getOptions()
            ->willReturn(['resolver' => $resolverName]);

        $slugResolverServiceMock->resolve($resolverName, $slug)
            ->shouldBeCalled()
            ->willReturn($expected);

        $resolverMock->getName()
            ->willReturn($resolverName);

        $slugResolverServiceMock->addResolver($resolverMock->reveal());

        $slugConverter = new SlugConverter();
        $slugConverter->setSlugResolverService($slugResolverServiceMock->reveal());

        $actual = $slugConverter->apply($request, $paramConverterMock->reveal());

        $this->assertTrue($actual);
    }

    public function availableResolversProvider()
    {
        return [
            ['foo', ['foo', 'bar'], true],
            ['foobar', ['foo', 'bar'], false],
        ];
    }
}
