<?php

return [
    'fixtureParseData' => [
        'parameters' => [
            'country_id' => 157,
            'country_code' => 'mx',
            'country_name' => 'Mexico',
            'locale' => 'es_MX',
            'available_payment_methods' => [
                0 => 'payment.method.credit_card',
                1 => 'payment.method.linio_wallet',
                2 => 'payment.method.paypal',
                3 => 'payment.method.cash_on_delivery',
                4 => 'payment.method.credit_card_on_delivery',
                5 => 'payment.method.oxxo',
                6 => 'payment.method.consignment',
                7 => 'payment.method.zero_payment',
                8 => 'payment.method.banorte_referenciado',
            ],
            'default_allowed_payment_methods' => [],
            'display_newsletter_header_cta' => false,
            'display_taxes' => false,
            'display_discounts' => true,
            'display_price_labels' => false,
            'store_cards' => true,
            'shipping_options_enabled' => true,
            'address_adapter' => 'frontend.address.adapter.sepomex',
            'oldbob_adapter_store_id' => 1,
            'loyalty_program' => 'Club_Premier',
            'loyalty_program_show_form_cta' => false,
            'claims_cta_url' => '',
            'industry_and_trade_cta_url' => '',
            'store_pickup_enabled' => false,
            'linio_plus_enabled' => true,
            'linio_plus_skus' => [
                0 => 'LI719EG19CYWCLMX',
                1 => 'LI200EG60EELLMX',
            ],
            'newsletter_frequency' => [
                1 => 'daily mail',
                2 => 'two emails a week',
                3 => 'one email every week',
                4 => 'one email every two weeks',
                5 => 'one email every month',
            ],
            'oauth_fb_id' => '516379241745326',
            'oauth_fb_secret' => '190af36981315b4af6ceb8d0710355f0',
            'oauth_gplus_id' => '859207094519-qkck9gjv05fulq8c7jiptrd5i92a6old.apps.googleusercontent.com',
            'oauth_gplus_secret' => 'PXefnLuKmVgZI1PGZcdrIvSp',
            'oauth_gplus_api_key' => 'AIzaSyBXPBdvtvBRAGROzzb0Tm6LZinrWcEA7Bw',
            'social' => [
                'facebook' => 'https://www.facebook.com/LinioMexico/',
                'twitter' => 'https://twitter.com/LinioMexico/',
                'twitter_account' => '@@liniomexico',
                'youtube' => 'https://www.youtube.com/user/LinioMexico',
                'linkedin' => 'https://www.linkedin.com/company/linio-m-xico/',
                'sharing' => [
                    'facebook' => 'https://www.facebook.com/dialog/feed?',
                    'twitter' => 'https://twitter.com/intent/tweet?',
                ],
            ],
            'linio_contact_phone' => '01 800 925 4646 | DF (55) 8525-1200',
            'gtm_code' => 'D3X3',
            'google_merchant_center_verification_tag' => '-uK5tDdfxlHztCfBdvxJKLe137sIyEOjfWG2HldRbsg',
            'bank_transfer_confirmation_form_enabled' => false,
            'extra_checkout_fields_enabled' => true,
            'request_invoice_enabled' => true,
            'newsletter_reward_amount' => '$50',
            'newsletter_reward_minimum_purchase' => '$500',
            'wallet_enabled' => true,
            'wallet_info_url' => 'http://www.scotiabank.com.mx/tarjetalinio',
            'tax_id_required' => false,
            'telesales_enabled' => true,
            'disclaimer_categories' => [
                0 => 14360,
                1 => 14471,
                2 => 18813,
                3 => 18813,
                4 => 15624,
                5 => 14379,
                6 => 17293,
            ],
        ],
        'formatter' => [
            'date' => [
                'short' => 'd/m/y',
                'long' => 'd/m/Y H:i:s',
            ],
            'currency' => [
                'code' => 'MXN',
                'symbol' => '$',
                'decimal_point' => '.',
                'thousands_separator' => ',',
            ],
        ],
        'dynamic_form' => [
            'extra_checkout_fields' => [
                'invoice_type' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'choices' => [
                            'fisica' => 'Persona física',
                            'moral' => 'Persona moral',
                        ],
                        'label' => 'Tipo de facturación (opcional)',
                        'multiple' => false,
                        'attr' => [
                            'ng-model' => 'extraFields.address_billing.invoice_type',
                            'ng-focus' => '',
                        ],
                    ],
                ],
                'company' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Nombre o razón social (opcional)',
                        'attr' => [
                            'ng-model' => 'extraFields.address_billing.company',
                            'ng-focus' => '',
                        ],
                    ],
                ],
            'tax_identification_number' => [
                'enabled' => true,
                'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                'options' => [
                    'label' => 'RFC (opcional)',
                    'attr' => [
                        'ng-model' => 'extraFields.address_billing.tax_identification_number',
                        'ng-focus' => '',
                        'mask' => 'wwwwwwwwwwwww?',
                  ],
                ],
                'validation' => [
                    'Symfony\Component\Validator\Constraints\Regex' => [
                        'pattern' => '/^[\w]{12}[\w]?$/',
                    ],
                ],
              ],
            ],
            'address' => [
                'first_name' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Nombre',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'addressData.first_name',
                            'ng-focus' => '',
                            'autocomplete' => 'given-name',
                        ],
                    ],
                    'validation' => [
            '           Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'last_name' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Apellidos',
                        'required' => true,
                        'attr' => [
                        'ng-model' => 'addressData.last_name',
                        'ng-focus' => '',
                        'autocomplete' => 'family-name',
                  ],
                ],
                'validation' => [
                    'Symfony\Component\Validator\Constraints\NotBlank' => null,
                ],
                ],
                'mobile_phone' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Teléfono Celular',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'addressData.mobile_phone',
                            'ng-focus' => '',
                            'mask' => '99 9999 9999',
                            'autocomplete' => 'tel',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/^\d{2} \d{4} \d{4}$/',
                        ],
                    ],
                ],
                'phone' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Otro Teléfono',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'addressData.phone',
                            'ng-focus' => '',
                            'mask' => '99 9999 9999?',
                            'autocomplete' => 'tel home',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/^\d{2} \d{4} \d{4}$/',
                        ],
                    ],
                ],
                'address1' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Calle',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'addressData.address1',
                            'ng-focus' => '',
                            'autocomplete' => 'address-line1',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'street_number' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'N° Exterior',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'addressData.street_number',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level3',
                        ],
                    ],
                ],
                'address2' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'N° Interior',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'addressData.address2',
                            'mask' => '*?',
                            'repeat' => '10',
                            'ng-focus' => '',
                            'autocomplete' => 'address-line3',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\Length' => [
                        'max' => 10,
                    ],
                ],
                ],
                'postcode' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Código Postal',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'addressData.postcode',
                            'mask' => 99999,
                            'ng-focus' => '',
                            'autocomplete' => 'postal-code',
                            'ng-change' => 'resolvePostcode()',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/\d{5}/',
                        ],
                    ],
                ],
                'fk_customer_address_region' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'data_provider' => 'region',
                    'options' => [
                        'label' => 'Estado',
                        'required' => true,
                        'empty_value' => 'Seleccione un Estado',
                        'attr' => [
                            'ng-model' => 'addressData.fk_customer_address_region',
                            'ng-change' => 'loadCities()',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level1',
                        ],
                    ],
                ],
                'city' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'Ciudad',
                        'required' => true,
                        'empty_value' => 'Seleccione una Ciudad',
                        'attr' => [
                            'ng-model' => 'addressData.city',
                            'ng-change' => 'loadMunicipalities()',
                            'ng-options' => 'city.id as city.name for city in cities track by city.id',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level2',
                        ],
                    ],
                ],
                'municipality' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'Delegación',
                        'required' => true,
                        'empty_value' => 'Seleccione una Delegación',
                        'attr' => [
                            'ng-model' => 'addressData.municipality',
                            'ng-change' => 'loadNeighborhoods()',
                            'ng-options' => 'municipality.id as municipality.name for municipality in municipalities track by municipality.id',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level2',
                        ],
                    ],
                ],
                'neighborhood' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'Colonia',
                            'required' => true,
                            'empty_value' => 'Seleccione una Colonia',
                            'attr' => [
                                'ng-model' => 'addressData.neighborhood',
                                'ng-options' => 'neighborhood.id as neighborhood.name for neighborhood in neighborhoods track by neighborhood.id',
                                'ng-focus' => '',
                                'autocomplete' => 'address-level2',
                        ],
                    ],
                ],
                'between_street1' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Entre la calle',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'addressData.between_street1',
                            'ng-focus' => '',
                        ],
                    ],
                ],
                'between_street2' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'y la calle',
                        'required' => false,
                    'attr' => [
                        'ng-model' => 'addressData.between_street2',
                        'ng-focus' => '',
                    ],
                ],
                ],
                'additional_information' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Instrucciones especiales (opcional)',
                    'required' => false,
                    'attr' => [
                        'ng-model' => 'addressData.additional_information',
                        'ng-focus' => '',
                  ],
                ],
              ],
            ],
            'cart_shipping_address' => [
                'postcode' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                    'label' => 'Código Postal',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-style col-sm-36 margin-medium-bottom',
                        'ng-model' => 'addressData.postcode',
                        'mask' => 99999,
                        'ng-focus' => '',
                        'autocomplete' => 'postal-code',
                      ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/\d{5}/',
                        ],
                    ],
                ],
            ],
            'registration' => [
                'email' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\EmailType',
                    'options' => [
                        'label' => 'customer.registration.email',
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'Ej: carlos@mail.com',
                            'autocomplete' => 'email',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                        'Symfony\Component\Validator\Constraints\Email' => null,
                    ],
                ],
                'sms_phone' => [
                'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'customer.registration.mobile_phone.optional',
                    'required' => false,
                    'attr' => [
                        'ng-model' => 'registration.phoneNumber',
                        'mask' => '99 9999 9999',
                        'autocomplete' => 'tel',
                      ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/\d{2} \d{4} \d{4}/',
                        ],
                    ],
                ],
                'first_name' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'customer.registration.first_name',
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'Ejemplo: Carlos',
                            'autocomplete' => 'given-name',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'last_name' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'customer.registration.last_name',
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'Ejemplo: López Martínez',
                            'autocomplete' => 'family-name',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
            'password' => [
                'enabled' => true,
                'type' => 'Symfony\Component\Form\Extension\Core\Type\RepeatedType',
                'options' => [
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\PasswordType',
                    'label' => '',
                    'required' => true,
                    'first_options' => [
                        'label' => 'customer.registration.password.password',
                        'attr' => [
                            'placeholder' => '●●●●●●●●',
                        ],
                    ],
                    'second_options' => [
                    'label' => 'customer.registration.password.confirm',
                    'attr' => [
                        'placeholder' => '●●●●●●●●',
                    ],
                  ],
                ],
                'validation' => [
                    'Symfony\Component\Validator\Constraints\NotBlank' => null,
                ],
            ],
            'subscribedToNewsletter' => [
                'enabled' => true,
                'type' => 'Symfony\Component\Form\Extension\Core\Type\CheckboxType',
                'options' => [
                    'label' => 'customer.registration.subscription.newsletter',
                    'data' => true,
                    'required' => false,
                ],
            ],
            ],
            'profile_update' => [
                'first_name' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'profile.name',
                        'required' => true,
                        'attr' => [
                            'class' => 'full-width form-style',
                            'autocomplete' => 'given-name',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'last_name' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'profile.last_name',
                        'required' => true,
                        'attr' => [
                            'class' => 'full-width form-style',
                            'autocomplete' => 'family-name',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'gender' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'profile.sex',
                        'required' => true,
                        'choices' => [
                            'female' => 'profile.female',
                            'male' => 'profile.male',
                        ],
                        'expanded' => true,
                        'multiple' => false,
                        'attr' => [
                            'class' => 'col-lg-18 col-sm-36 margin-small-top',
                            'autocomplete' => 'sex',
                        ],
                    ],
                ],
                'bornDate' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\BirthdayType',
                    'options' => [
                        'label' => 'profile.birthday',
                        'required' => false,
                        'input' => 'string',
                        'widget' => 'single_text',
                        'attr' => [
                            'class' => 'full-width form-style',
                            'autocomplete' => 'bday',
                            'placeholder' => 'AAAA-mm-dd',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'email' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\EmailType',
                    'options' => [
                        'label' => 'Email',
                        'required' => true,
                        'read_only' => true,
                        'attr' => [
                            'class' => 'full-width form-read-only',
                            'autocomplete' => 'email',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                        'Symfony\Component\Validator\Constraints\Email' => null,
                    ],
                ],
            ],
            'billing_address' => [
                'regime' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'choices' => [
                            'fisica' => 'Física',
                            'moral' => 'Moral',
                        ],
                        'label' => 'Selecciona:',
                        'required' => false,
                        'expanded' => false,
                        'multiple' => false,
                        'attr' => [
                            'ng-model' => 'billingAddress.regime',
                        ],
                ],
                ],
                'company' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Razon social',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'billingAddress.company',
                            'ng-focus' => '',
                            'autocomplete' => 'given-name',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'taxIdentificationNumber' => [
                    'enabled' => true,
                        'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                        'options' => [
                            'label' => 'RFC',
                            'required' => true,
                            'attr' => [
                                'ng-model' => 'billingAddress.taxIdentificationNumber',
                                'ng-focus' => '',
                                'mask' => 'wwwwwwwwwwwww?',
                                'autocomplete' => 'tel',
                            ],
                        ],
                ],
                'phone' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Teléfono',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'billingAddress.phone',
                            'ng-focus' => '',
                            'mask' => '99 9999 9999?',
                            'autocomplete' => 'tel home',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/^\d{2} \d{4} \d{4}$/',
                        ],
                    ],
                ],
                'mobilePhone' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Teléfono Celular',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'billingAddress.mobilePhone',
                            'ng-focus' => '',
                            'mask' => '99 9999 9999',
                            'autocomplete' => 'tel',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/^\d{2} \d{4} \d{4}$/',
                        ],
                    ],
                ],
                'address1' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Calle',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'billingAddress.address1',
                            'ng-focus' => '',
                            'autocomplete' => 'address-line1',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
                'street_number' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Número Exterior',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'billingAddress.street_number',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level3',
                        ],
                    ],
                ],
                'address2' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Número Interior',
                        'required' => false,
                        'attr' => [
                            'ng-model' => 'billingAddress.address2',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level3',
                        ],
                    ],
                ],
                'postcode' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                    'options' => [
                        'label' => 'Código Postal',
                        'required' => true,
                        'attr' => [
                            'ng-model' => 'billingAddress.postcode',
                            'mask' => 99999,
                            'ng-focus' => '',
                            'autocomplete' => 'postal-code',
                            'ng-change' => 'resolvePostcode()',
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\Regex' => [
                            'pattern' => '/\d{5}/',
                        ],
                    ],
                ],
                'fk_customer_address_region' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'data_provider' => 'region',
                    'options' => [
                        'label' => 'Estado',
                        'required' => true,
                        'empty_value' => 'Seleccione un Estado',
                        'attr' => [
                        'ng-model' => 'billingAddress.fk_customer_address_region',
                            'ng-change' => 'loadCities()',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level1',
                        ],
                    ],
                ],
                'city' => [
                    'enabled' => true,
                        'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'Ciudad',
                        'required' => true,
                        'empty_value' => 'Seleccione una Ciudad',
                        'attr' => [
                            'ng-model' => 'billingAddress.city',
                            'ng-change' => 'loadMunicipalities()',
                            'ng-options' => 'city.id as city.name for city in cities track by city.id',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level2',
                        ],
                    ],
                ],
                'municipality' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'Delegación',
                        'required' => true,
                        'empty_value' => 'Seleccione una Delegación',
                        'attr' => [
                            'ng-model' => 'billingAddress.municipality',
                            'ng-change' => 'loadNeighborhoods()',
                            'ng-options' => 'municipality.id as municipality.name for municipality in municipalities track by municipality.id',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level2',
                        ],
                    ],
                ],
                'neighborhood' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                    'options' => [
                        'label' => 'Colonia',
                        'required' => true,
                        'empty_value' => 'Seleccione una Colonia',
                        'attr' => [
                            'ng-model' => 'billingAddress.neighborhood',
                            'ng-options' => 'neighborhood.id as neighborhood.name for neighborhood in neighborhoods track by neighborhood.id',
                            'ng-focus' => '',
                            'autocomplete' => 'address-level2',
                        ],
                    ],
                ],
            ],
            'wallet_account' => [
                'account' => [
                    'enabled' => true,
                    'type' => 'Symfony\Component\Form\Extension\Core\Type\RepeatedType',
                    'options' => [
                        'type' => 'Symfony\Component\Form\Extension\Core\Type\TextType',
                        'required' => true,
                        'first_options' => [
                            'label' => 'Número de Linio Wallet',
                            'attr' => [
                                'ng-model' => 'wallet.account.first',
                                'placeholder' => 'Escribe tu número de Linio Wallet',
                            ],
                        ],
                        'invalid_message' => 'Los números de cuenta deben coincidir',
                        'second_options' => [
                            'label' => 'Confirmar tu número de Linio Wallet',
                            'attr' => [
                                'ng-model' => 'wallet.account.second',
                                'placeholder' => 'Escribe nuevamente tu número de Linio Wallet',
                            ],
                        ],
                    ],
                    'validation' => [
                        'Symfony\Component\Validator\Constraints\NotBlank' => null,
                    ],
                ],
            ],
            'bank_transfer_confirmation' => [],
        ],
    ],
];
