<?php

namespace Linio\Frontend\Twig;

use Linio\DynamicFormBundle\Exception\NonExistentFormException;
use Linio\DynamicFormBundle\Form\FormFactory;
use Linio\Frontend\Order\Payment\Method\Regular;
use Symfony\Component\Form\Form;

class PaymentMethodExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingCheckoutTemplates()
    {
        $extension = new PaymentMethodExtension();
        $templates = $extension->getCheckoutTemplates(new Regular('foobar'));

        $expectedTemplates = [
            ':Checkout:payment/foobar/checkout.html.twig',
            ':Checkout:payment/Regular/checkout.html.twig',
            ':Checkout:payment/checkout.html.twig',
        ];
        $this->assertEquals($expectedTemplates, $templates);
    }

    public function testIsGettingSuccessTemplates()
    {
        $extension = new PaymentMethodExtension();
        $templates = $extension->getSuccessTemplates(new Regular('foobar'));

        $expectedTemplates = [
            ':Checkout:payment/foobar/success.html.twig',
            ':Checkout:payment/Regular/success.html.twig',
            ':Checkout:payment/success.html.twig',
        ];
        $this->assertEquals($expectedTemplates, $templates);
    }

    public function testIsGettingPaymentMethodForm()
    {
        $form = $this->prophesize(Form::class);
        $form->createView()->shouldBeCalled();

        $formFactory = $this->prophesize(FormFactory::class);
        $formFactory->createForm('foobar', [], ['csrf_protection' => false])->willReturn($form->reveal());

        $extension = new PaymentMethodExtension();
        $extension->setDynamicFormFactory($formFactory->reveal());
        $extension->getPaymentMethodForm(new Regular('foobar'));
    }

    public function testIsNotGettingPaymentMethodForm()
    {
        $formFactory = $this->prophesize(FormFactory::class);
        $formFactory->createForm('foobar', [], ['csrf_protection' => false])->willThrow(new NonExistentFormException());

        $extension = new PaymentMethodExtension();
        $extension->setDynamicFormFactory($formFactory->reveal());
        $this->assertNull($extension->getPaymentMethodForm(new Regular('foobar')));
    }
}
