<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductService;

class ProductExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsProductAvailable()
    {
        $sku = 'SKU2342343QW-24343';

        $product = new Product();
        $product->setSku($sku);
        $product->setStock(4);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku($sku)->willReturn($product);

        $productExtension = new ProductExtension();
        $productExtension->setProductService($productService->reveal());

        $this->assertEquals(true, $productExtension->isProductAvailable($sku));
    }

    public function testIsNotProductAvailable()
    {
        $sku = 'SKU34545454QW-9898';

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku($sku)->willThrow(ProductNotFoundException::class);

        $productExtension = new ProductExtension();
        $productExtension->setProductService($productService->reveal());

        $this->assertEquals(false, $productExtension->isProductAvailable($sku));
    }
}
