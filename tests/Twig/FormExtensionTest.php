<?php

namespace Linio\Frontend\Twig;

use Symfony\Component\Form\FormView;

class FormExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingRequiredFields()
    {
        $formView = new FormView();

        $child1 = new FormView();
        $child1->vars['required'] = false;
        $formView->children[] = $child1;

        $child2 = new FormView();
        $child2->vars['required'] = true;
        $formView->children[] = $child2;

        $child3 = new FormView();
        $child3->vars['required'] = false;
        $formView->children[] = $child3;

        $extension = new FormExtension();
        $requiredFields = $extension->getRequiredFields($formView);

        $expectedFields = [$child2];
        $this->assertEquals($expectedFields, $requiredFields);
    }

    public function testIsGettingOptionalFields()
    {
        $formView = new FormView();

        $child1 = new FormView();
        $child1->vars['required'] = false;
        $formView->children[] = $child1;

        $child2 = new FormView();
        $child2->vars['required'] = true;
        $formView->children[] = $child2;

        $child3 = new FormView();
        $child3->vars['required'] = false;
        $formView->children[] = $child3;

        $extension = new FormExtension();
        $requiredFields = $extension->getOptionalFields($formView);

        $expectedFields = [$child1, $child3];
        $this->assertEquals($expectedFields, $requiredFields);
    }
}
