<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Carbon\Carbon;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Entity\Catalog\Category;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class ContentTypeExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsReturningCmsPlacement()
    {
        $placementContent = [
            'href' => 'href',
            'image' => [
                'alt_attr' => 'test3',
                'src' => 'https',
                'title_attr' => 'test3',
            ],
            'revision_number' => 1,
            'start_date' => new Carbon('2016-05-23 16:30', 'UTC'),
            'title' => 'Test3',
            'type' => 'customized',
        ];

        /* TODO: Check the comment in placement function from CmsService */
        $cacheKey = 'mx:cms:desktop:placement:global:test';

        $cmsService = $this->prophesize(CmsService::class);
        $cmsService->placement($cacheKey)
            ->shouldBeCalled()
            ->willReturn($placementContent);

        $logger = $this->prophesize(LoggerInterface::class);

        $contentTypeExtension = new ContentTypeExtension($cmsService->reveal(), $logger->reveal());

        $actual = $contentTypeExtension->cmsPlacement($cacheKey);

        $this->assertEquals($placementContent, $actual);
    }

    public function testIsGeneratingUrlWhenCategoryNotFound()
    {
        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(1)
            ->shouldBeCalled()
            ->willThrow(new CategoryNotFoundException('Category id=(1) not found.'));

        $logger = $this->prophesize(LoggerInterface::class);
        $logger->critical('Menu category not found.', ['id' => 1])->shouldBeCalled();

        $extension = new ContentTypeExtension($this->prophesize(CmsService::class)->reveal(), $logger->reveal());
        $extension->setCategoryService($categoryService->reveal());
        $actual = $extension->generateUrlFromCategoryId(1);

        $this->assertEquals('', $actual);
    }

    public function testIsGeneratingUrlWhenCategoryFound()
    {
        $expected = 'http://localhost/c/category-name';

        $category = new Category();
        $category->setId(1991);
        $category->setName('Category Name');
        $category->setSlug('category-name');

        $categoryService = $this->prophesize(CategoryService::class);
        $categoryService->getCategory(1991)->willReturn($category);

        $logger = $this->prophesize(LoggerInterface::class);

        $parameters = ['category' => $category->getSlug()];

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.catalog.index.category', $parameters, UrlGeneratorInterface::ABSOLUTE_URL)
            ->willReturn('http://localhost/c/category-name');

        $extension = new ContentTypeExtension($this->prophesize(CmsService::class)->reveal(), $logger->reveal());
        $extension->setCategoryService($categoryService->reveal());
        $extension->setRouter($router->reveal());

        $actual = $extension->generateUrlFromCategoryId(1991);

        $this->assertEquals($expected, $actual);
    }
}
