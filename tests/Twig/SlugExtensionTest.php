<?php

namespace Linio\Frontend\Twig;

use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class SlugExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGeneratingCategoryUrl()
    {
        $category = new Category();
        $category->setSlug('category-slug-test');

        $routerMock = $this->createMock(RouterInterface::class);
        $requestStack = $this->prophesize(RequestStack::class);
        $request = $this->prophesize(Request::class);
        $queryBag = $this->prophesize(ParameterBag::class);

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($routerMock);
        $slugExtension->setRequestStack($requestStack->reveal());

        $expected = 'http://test.local/c/category-slug-test';

        $routerMock->expects($this->once())
            ->method('generate')
            ->with('frontend.catalog.index.category', ['category' => $category->getSlug()])
            ->willReturn($expected);

        $request->query = $queryBag;
        $queryBag->all()->willReturn([]);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $actual = $slugExtension->getCategoryPath($category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingProductUrl()
    {
        $product = new Product();
        $product->setSlug('product-slug-test');

        $routerMock = $this->createMock(RouterInterface::class);

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($routerMock);

        $expected = 'http://test.local/p/product-slug-test';

        $routerMock->expects($this->once())
            ->method('generate')
            ->with('frontend.catalog.detail', ['productSlug' => $product->getSlug()])
            ->willReturn($expected);

        $actual = $slugExtension->getProductPath($product);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingBrandUrl()
    {
        $brand = new Brand();
        $brand->setSlug('brand-slug');

        $routerMock = $this->createMock(RouterInterface::class);

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($routerMock);

        $expected = 'http://test.local/b/brand-slug';

        $routerMock->expects($this->once())
            ->method('generate')
            ->with('frontend.catalog.index.brand', ['brand' => $brand->getSlug()])
            ->willReturn($expected);

        $actual = $slugExtension->getBrandPath($brand);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingCampaignUrl()
    {
        $campaign = new Campaign();
        $campaign->setSlug('campaign-test');

        $router = $this->prophesize(RouterInterface::class);
        $requestStack = $this->prophesize(RequestStack::class);
        $request = new Request(['page' => '5']);
        $requestStack->getCurrentRequest()->willReturn($request);

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($router->reveal());
        $slugExtension->setRequestStack($requestStack->reveal());

        $expected = 'http://test.local/cm/campaign-test';
        $router->generate('frontend.catalog.index.campaign', ['campaign' => $campaign->getSlug()], 1)
            ->willReturn($expected);

        $actual = $slugExtension->getCampaignPath($campaign);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingCampaignPathFromCampaignAndRequestParameters()
    {
        $campaign = new Campaign();
        $campaign->setSlug('campaign-test');

        $expected = 'http://test.local/cm/campaign-test';

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.catalog.index.campaign', ['campaign' => $campaign->getSlug()], 1)
            ->willReturn($expected);

        $request = new Request(['page' => '5']);

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request);

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($router->reveal());
        $slugExtension->setRequestStack($requestStack->reveal());

        $actual = $slugExtension->getCatalogPath($campaign);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingCategoryBrandPathFromCategoryAndRequestParameters()
    {
        $brand = new Brand();
        $brand->setSlug('brand-slug');

        $category = new Category();
        $category->setSlug('category-slug-test');

        $request = new Request(['page' => '5'], [], ['brand' => $brand]);

        $expected = 'http://test.local/c/category-slug-test/b/brand-slug';

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.catalog.index.category_brand', ['category' => $category->getSlug(), 'brand' => $brand->getSlug()], 1)
            ->willReturn($expected);

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request);

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($router->reveal());
        $slugExtension->setRequestStack($requestStack->reveal());

        $actual = $slugExtension->getCatalogPath($category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingCatalogUrlFromCategory()
    {
        $category = new Category();
        $category->setSlug('category-slug-test');

        $expected = 'http://test.local/c/category-slug-test';

        $routeParameters = [
            '_controller' => 'controller.default.indexAction',
            '_route' => 'frontend.default.index',
        ];

        $router = $this->prophesize(RouterInterface::class);
        $router->match('/')->shouldBeCalled()->willReturn($routeParameters);
        $router->generate('frontend.catalog.index.category', ['category' => $category->getSlug()], 1)
            ->willReturn($expected);

        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->shouldBeCalled()->willReturn('/');
        $request->query = new ParameterBag();

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $slugExtension = new SlugExtension();
        $slugExtension->setRouter($router->reveal());
        $slugExtension->setRequestStack($requestStack->reveal());

        $actual = $slugExtension->getCategoryPath($category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsGeneratingCategorySellerUrl()
    {
        $expected = '/c/laptops?seller=tecdepot';

        $category = new Category();
        $category->setSlug('laptops');

        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->shouldBeCalled()->willReturn('/s/tecdepot');
        $request->query = new ParameterBag();

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->shouldBeCalled()->willReturn($request);

        $routeParameters = [
            '_controller' => 'controller.catalog:sellerAction',
            'seller' => 'tecdepot',
            '_route' => 'frontend.catalog.index.seller',
        ];

        $router = $this->prophesize(RouterInterface::class);
        $router->match('/s/tecdepot')->willReturn($routeParameters);
        $router->generate(
            'frontend.catalog.index.category',
            ['category' => $category->getSlug(), 'seller' => 'tecdepot'],
            1
        )->shouldBeCalled()->willReturn('/c/laptops?seller=tecdepot');

        $extension = new SlugExtension();
        $extension->setRouter($router->reveal());
        $extension->setRequestStack($requestStack->reveal());
        $actual = $extension->getCategoryPath($category);

        $this->assertEquals($expected, $actual);
    }
}
