<?php

namespace Linio\Frontend\Twig;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Catalog\Seo;
use Linio\Frontend\Seo\SeoService;
use Prophecy\Prophecy\Object;
use RuntimeException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;

class SeoExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SeoExtension
     */
    protected $extension;

    /**
     * @var object
     */
    protected $seoServiceMock;

    /**
     * @var object
     */
    protected $cacheServiceMock;

    /**
     * @var object
     */
    protected $seoEntityMock;

    public function testIsGettingFunctions()
    {
        $actual = $this->extension->getFunctions();

        $this->assertInternalType('array', $actual);
        $this->assertInstanceOf('\Twig_SimpleFunction', $actual[0]);
        $this->assertEquals('seo_title', $actual[0]->getName());
        $this->assertEquals([$this->extension, 'seoTitle'], $actual[0]->getCallable());
    }

    public function testIsGettingSeoTitle()
    {
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
        ];

        $this->extension->setSeoService($this->seoServiceMock->reveal());

        $this->seoServiceMock->getSeoEntity()
            ->shouldBeCalled()
            ->willReturn($this->seoEntityMock->reveal());

        $this->seoEntityMock->getTitle()
            ->shouldBeCalled()
            ->willReturn($expected['title']);

        $actual = $this->extension->seoTitle();

        $this->assertEquals($expected['title'], $actual);
    }

    public function testIsGettingSeoDescription()
    {
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
        ];

        $this->extension->setSeoService($this->seoServiceMock->reveal());

        $this->seoServiceMock->getSeoEntity()
            ->shouldBeCalled()
            ->willReturn($this->seoEntityMock->reveal());

        $this->seoEntityMock->getDescription()
            ->shouldBeCalled()
            ->willReturn($expected['description']);

        $actual = $this->extension->seoDescription();

        $this->assertEquals($expected['description'], $actual);
    }

    public function testIsGettingSeoRobots()
    {
        $expected = 'INDEX,FOLLOW';

        $routeContext = $this->prophesize(RequestContext::class);
        $routeContext->getPathInfo()
            ->shouldBeCalled()
            ->willReturn('/cm/some_campaign');

        $router = $this->prophesize(RouterInterface::class);
        $router->getContext()
            ->shouldBeCalled()
            ->willReturn($routeContext->reveal());
        $router->match('/cm/some_campaign')
            ->shouldBeCalled()
            ->willReturn(['_route' => 'frontend.catalog.index.campaign']);

        $this->extension->setRouter($router->reveal());

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()
            ->shouldBeCalled()
            ->willReturn(new Request());

        $this->extension->setRequestStack($requestStack->reveal());

        $actual = $this->extension->seoRobots();

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingSeoRobotsWhenRouteIsNotMatched()
    {
        $expected = 'INDEX,FOLLOW';

        $routeContext = $this->prophesize(RequestContext::class);
        $routeContext->getPathInfo()
            ->shouldBeCalled()
            ->willReturn('/i-am-not-found');

        $router = $this->prophesize(RouterInterface::class);
        $router->getContext()
            ->shouldBeCalled()
            ->willReturn($routeContext->reveal());
        $router->match('/i-am-not-found')
            ->shouldBeCalled()
            ->willThrow(new RuntimeException());

        $requestStack = $this->prophesize(RequestStack::class);

        $this->extension->setRouter($router->reveal());
        $this->extension->setRequestStack($requestStack->reveal());

        $actual = $this->extension->seoRobots();

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingSeoText()
    {
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seoText' => 'hola',
        ];

        $this->extension->setSeoService($this->seoServiceMock->reveal());

        $this->seoServiceMock->getSeoEntity()
            ->shouldBeCalled()
            ->willReturn($this->seoEntityMock->reveal());

        $this->seoEntityMock->getSeoText()
            ->shouldBeCalled()
            ->willReturn($expected['seoText']);

        $actual = $this->extension->seoText();

        $this->assertEquals($expected['seoText'], $actual);
    }

    public function testIsGettingText()
    {
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seoText' => 'hola',
            'text' => 'texto',
        ];

        $this->extension->setSeoService($this->seoServiceMock->reveal());

        $this->seoServiceMock->getSeoEntity()
            ->shouldBeCalled()
            ->willReturn($this->seoEntityMock->reveal());

        $this->seoEntityMock->getText()
            ->shouldBeCalled()
            ->willReturn($expected['text']);

        $actual = $this->extension->Text();

        $this->assertEquals($expected['text'], $actual);
    }

    public function testIsGettingKeywords()
    {
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seoText' => 'hola',
            'text' => 'texto',
        ];

        $this->extension->setSeoService($this->seoServiceMock->reveal());

        $this->seoServiceMock->getSeoEntity()
            ->shouldBeCalled()
            ->willReturn($this->seoEntityMock->reveal());

        $this->seoEntityMock->getKeywords()
            ->shouldBeCalled()
            ->willReturn($expected['keywords']);

        $actual = $this->extension->seoKeywords();

        $this->assertEquals($expected['keywords'], $actual);
    }

    public function testIsGettingCountry()
    {
        $expected = 'AR';

        $this->extension->setCountryCode('ar');
        $actual = $this->extension->country();

        $this->assertEquals($expected, $actual);
    }

    public function testIsGettingCanonicalUrlWhenPageIsGreaterThan1()
    {
        $request = new Request(['page' => '2']);

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request);

        $extension = new SeoExtension();
        $extension->setRequestStack($requestStack->reveal());
        $actual = $extension->getCanonicalUrl();

        $this->assertNull($actual);
    }

    public function testIsGettingCanonicalUrlWhenPageIsOneOrDoesNotExist()
    {
        $query = new ParameterBag();
        $attributes = new ParameterBag(['_route_params' => []]);

        $request = $this->prophesize(Request::class);
        $request->query = $query;
        $request->attributes = $attributes;
        $request->getPathInfo()->willReturn('/c/category-1');

        $context = new RequestContext();
        $context->setPathInfo('/c/category-1');

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $router = $this->prophesize(RouterInterface::class);
        $router->getContext()->willReturn($context);

        $extension = new SeoExtension();
        $extension->setRouter($router->reveal());
        $extension->setRequestStack($requestStack->reveal());
        $actual = $extension->getCanonicalUrl();

        $this->assertEquals('http://localhost/c/category-1', $actual);
    }

    public function setup()
    {
        $this->extension = new SeoExtension();
        $this->seoServiceMock = $this->prophesize(SeoService::class);
        $this->seoEntityMock = $this->prophesize(Seo::class);
        $this->cacheServiceMock = $this->prophesize(CacheService::class);
    }
}
