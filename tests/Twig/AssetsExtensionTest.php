<?php

namespace Linio\Frontend\Twig;

use Linio\Component\Cache\CacheService;
use org\bovigo\vfs\vfsStream;
use Prophecy\Argument;

class AssetsExtensionTest extends \PHPUnit_Framework_TestCase
{
    protected $rootDirectory;

    protected function setUp()
    {
        $cssFile = '{
          "normalize.css": "normalize-30583ed3.css",
          "foundation.css": "foundation-73dbfd18.css",
          "/rocket/shop-front/app/Resources/assets/scss/default.css": "default-fc1454db.css",
          "/rocket/shop-front/app/Resources/assets/scss/foo.css": "foo-2fc54981.css"
        }';

        $jsFile = '{
          "functions.js": "functions-07fb3d81.js"
        }';

        $fileStructure = [
            'css' => [
                'rev-manifest.json' => $cssFile,
            ],
            'js' => [
                'rev-manifest.json' => $jsFile,
            ],
        ];

        $this->rootDirectory = vfsStream::setup('assets', null, $fileStructure);
    }

    public function testGetFunctionsAndTypes()
    {
        $assetsExtension = new AssetsExtension();
        $functions = $assetsExtension->getFunctions();

        $this->assertInternalType('array', $functions);

        foreach ($functions as $function) {
            $this->assertInstanceOf('Twig_SimpleFunction', $function);
        }
    }

    public function testReadCssJsonFile()
    {
        $cssRevisionFile = vfsStream::url('assets/css/rev-manifest.json');

        $cache = $this->prophesize(CacheService::class);
        $cache->contains($cssRevisionFile)->willReturn(false);
        $cache->set(Argument::type('string'), Argument::type('array'))->willReturn(true);

        $assetsExtension = new AssetsExtension();
        $assetsExtension->setCacheService($cache->reveal());
        $assetsExtension->setCssPath(vfsStream::url('assets/css/'));
        $assetsExtension->setCssRevisionFile($cssRevisionFile);

        $cssAssets = $assetsExtension->getCssAssets();

        $this->assertEquals($cssAssets[0], 'vfs://assets/css/normalize-30583ed3.css');
    }

    public function testReadJsJsonFile()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('vfs://assets/js/rev-manifest.json')
            ->willReturn(true);
        $cache->get('vfs://assets/js/rev-manifest.json')
            ->willReturn(['functions-07fb3d81.js']);

        $assetsExtension = new AssetsExtension();
        $assetsExtension->setCacheService($cache->reveal());
        $assetsExtension->setJsPath(vfsStream::url('assets/js/'));
        $assetsExtension->setJsRevisionFile(vfsStream::url('assets/js/rev-manifest.json'));

        $actual = $assetsExtension->getJsAssets();

        $this->assertEquals($actual[0], 'vfs://assets/js/functions-07fb3d81.js');
    }

    public function testIterateJsAssetsFileNotFound()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('assets/js/rev-manifest.json')
            ->willReturn(false);

        $assetsExtension = new AssetsExtension();
        $assetsExtension->setCacheService($cache->reveal());
        $assetsExtension->setJsPath('assets/js/');
        $assetsExtension->setJsRevisionFile('assets/js/rev-manifest.json');

        $jsAssets = ($assetsExtension->getJsAssets());

        $this->assertEmpty($jsAssets);
    }

    public function testIterateCssAssetsFileNotFound()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('assets/css/rev-manifest.json')
            ->willReturn(true);
        $cache->get('assets/css/rev-manifest.json')
            ->willReturn([]);

        $assetsExtension = new AssetsExtension();
        $assetsExtension->setCacheService($cache->reveal());
        $assetsExtension->setCssPath('assets/css/');
        $assetsExtension->setCssRevisionFile('assets/css/rev-manifest.json');

        $cssAssets = $assetsExtension->getCssAssets();

        $this->assertEmpty($cssAssets);
    }

    public function testGetSpecificJs()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('vfs://assets/js/rev-manifest.json')
            ->willReturn(true);
        $cache->get('vfs://assets/js/rev-manifest.json')
            ->willReturn(['functions.js' => 'functions-07fb3d81.js']);

        $assetsExtension = new AssetsExtension();
        $assetsExtension->setCacheService($cache->reveal());
        $assetsExtension->setJsPath(vfsStream::url('assets/js/'));
        $assetsExtension->setJsRevisionFile(vfsStream::url('assets/js/rev-manifest.json'));

        $actual = $assetsExtension->getSpecificJs('functions.js');

        $this->assertEquals($actual, vfsStream::url('assets/js/functions-07fb3d81.js'));
    }

    public function testGetSpecificCss()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('vfs://assets/css/rev-manifest.json')
            ->willReturn(true);
        $cache->get('vfs://assets/css/rev-manifest.json')
            ->willReturn(['main.css' => 'main-30583ed3.css']);

        $assetsExtension = new AssetsExtension();
        $assetsExtension->setCacheService($cache->reveal());
        $assetsExtension->setCssPath(vfsStream::url('assets/css/'));
        $assetsExtension->setCssRevisionFile(vfsStream::url('assets/css/rev-manifest.json'));

        $cssAssets = ($assetsExtension->getSpecificCss('main.css'));

        $this->assertEquals($cssAssets, vfsStream::url('assets/css/main-30583ed3.css'));
    }

    public function testGetSpecificCssWithNoRevisionFile()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('invalid.css')
            ->willReturn(false);

        $assetsExtension = new AssetsExtension();

        $assetsExtension->setCacheService($cache->reveal());
        $this->assertNull($assetsExtension->getSpecificCss('invalid.css'));
    }

    public function testGetSpecificJsWithNoRevisionFile()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->contains('invalid.js')
            ->willReturn(false);

        $assetsExtension = new AssetsExtension();

        $assetsExtension->setCacheService($cache->reveal());
        $this->assertNull($assetsExtension->getSpecificJs('invalid.js'));
    }

    public function testGettingNameOfExtension()
    {
        $assetsExtension = new AssetsExtension();
        $extension = $assetsExtension->getName();
        $this->assertInternalType('string', $extension);
        $this->assertNotEmpty($extension);
    }
}
