<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig_Environment;

class SizeGuideLinkExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsNotRenderingALinkWhenTheCategoryIsInvalid()
    {
        $category = new Category();
        $category->setId(10519);
        $category->setSlug('shoes');
        $category->setUrlKey('shoes');

        $product = new Product();
        $product->setCategory($category);

        $sizeGuideCategories = [1, 2, 3];

        $twig = $this->prophesize(Twig_Environment::class);

        $cmsService = $this->prophesize(CmsService::class);
        $configurationCacheService = $this->prophesize(CacheService::class);
        $configurationCacheService->get('size_guide_categories')->willReturn($sizeGuideCategories);

        $extension = new SizeGuideLinkExtension($cmsService->reveal());
        $extension->setConfigurationCacheService($configurationCacheService->reveal());

        $actual = $extension->getSizeGuideLink($twig->reveal(), $product);

        $this->assertEquals('', $actual);
    }

    public function testIsRenderingADefaultLinkWhenBrandStaticPageIsEmpty()
    {
        $category = new Category();
        $category->setId(10519);
        $category->setSlug('shoes');
        $category->setUrlKey('shoes');

        $brand = new Brand();
        $brand->setId('816');
        $brand->setSlug('puma');

        $product = new Product();
        $product->setBrand($brand);
        $product->setCategory($category);

        $sizeGuideCategories = [10519];

        $link = '<a href="/sp/guia-de-tallas">Default</a>';
        $slug = 'guia-de-tallas-puma';

        $twig = $this->prophesize(Twig_Environment::class);

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.default.static_page', ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_PATH)->willReturn('/sp/guia-de-tallas-puma');

        $twig = $this->prophesize(Twig_Environment::class);
        $twig->render('::partial/components/catalog/size_guide.html.twig', ['link' => '/sp/guia-de-tallas-puma'])->willReturn($link);

        $cmsService = $this->prophesize(CmsService::class);
        $cmsService->staticPage($slug)->willReturn(['static_page' => 'sdasd']);

        $configurationCacheService = $this->prophesize(CacheService::class);
        $configurationCacheService->get('size_guide_categories')->willReturn($sizeGuideCategories);

        $extension = new SizeGuideLinkExtension($cmsService->reveal());

        $extension->setRouter($router->reveal());
        $extension->setConfigurationCacheService($configurationCacheService->reveal());

        $actual = $extension->getSizeGuideLink($twig->reveal(), $product);

        $this->assertEquals($link, $actual);
    }

    public function testIsRenderingSizeGuideLink()
    {
        $slug = 'guia-de-tallas-nike';

        $brand = new Brand();
        $brand->setSlug('nike');
        $category = new Category();
        $category->setId(2);
        $product = new Product();
        $product->setBrand($brand);
        $product->setCategory($category);

        $categories = [1, 2, 3];

        $link = '<a href="/sp/guia-de-tallas-nike">Brand</a>';

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.default.static_page', ['slug' => $slug], UrlGeneratorInterface::ABSOLUTE_PATH)->willReturn('/sp/guia-de-tallas-nike');

        $twig = $this->prophesize(Twig_Environment::class);
        $twig->render('::partial/components/catalog/size_guide.html.twig', ['link' => '/sp/guia-de-tallas-nike'])->willReturn($link);

        $cmsService = $this->prophesize(CmsService::class);
        $cmsService->staticPage($slug)->willReturn(['static_page' => 'sdasd']);

        $configurationCacheService = $this->prophesize(CacheService::class);
        $configurationCacheService->get('size_guide_categories')->willReturn($categories);

        $extension = new SizeGuideLinkExtension($cmsService->reveal());

        $extension->setRouter($router->reveal());
        $extension->setConfigurationCacheService($configurationCacheService->reveal());

        $actual = $extension->getSizeGuideLink($twig->reveal(), $product);

        $this->assertEquals($link, $actual);
    }
}
