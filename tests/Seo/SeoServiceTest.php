<?php

namespace Linio\Frontend\Seo;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Seller;
use Prophecy\Prophecy\Object;

class SeoServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var object
     */
    protected $cacheServiceMock;

    /**
     * @var SeoService
     */
    protected $seoService;

    public function testIsSettingUpCategorySeo()
    {
        $categoryId = 913;
        $key = 'desktop:seo:category:913';
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $cache = $this->prophesize(CacheService::class);
        $cache->get($key)->willReturn($expected);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setUpCategorySeo($categoryId);

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($expected['title'], $seoEntity->getTitle());
        $this->assertEquals($expected['description'], $seoEntity->getDescription());
        $this->assertEquals($expected['robots'], $seoEntity->getRobots());
        $this->assertEquals($expected['keywords'], $seoEntity->getKeywords());
        $this->assertEquals($expected['seo_text'], $seoEntity->getSeoText());
    }

    public function testIsSettingUpSeoDataForCampaignSegment()
    {
        $segmentSeo = [
            'title' => 'Videojuegos',
            'description' => '',
            'robots' => 'noindex,follow',
            'keywords' => 'videojuegos',
            'seo_text' => 'SEO text',
        ];

        $cache = $this->prophesize(CacheService::class);
        $cache->get('desktop:seo:segment:666')
            ->shouldBeCalled()
            ->willReturn($segmentSeo);

        $currentSegment = new Campaign();
        $currentSegment->setId(666);

        $campaign = new Campaign();
        $campaign->setCurrentSegment($currentSegment);

        $service = new SeoService();
        $service->setCacheService($cache->reveal());

        $service->setupCampaignSeo($campaign);
        $seoEntity = $service->getSeoEntity();

        $this->assertEquals($segmentSeo['title'], $seoEntity->getTitle());
        $this->assertEquals($segmentSeo['description'], $seoEntity->getDescription());
        $this->assertEquals($segmentSeo['robots'], $seoEntity->getRobots());
        $this->assertEquals($segmentSeo['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpSeoDataForCampaignWithoutSegment()
    {
        $campaignSeo = [
            'title' => 'Buen fin',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $campaign = new Campaign();
        $campaign->setId(291);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('desktop:seo:campaign:291')
            ->shouldBeCalled()
            ->willReturn($campaignSeo);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());

        $seoService->setupCampaignSeo($campaign);
        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($campaignSeo['title'], $seoEntity->getTitle());
        $this->assertEquals($campaignSeo['description'], $seoEntity->getDescription());
        $this->assertEquals($campaignSeo['robots'], $seoEntity->getRobots());
        $this->assertEquals($campaignSeo['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpSeoDataForCampaign()
    {
        $campaignSeo = [
            'title' => 'Buen fin',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $cache = $this->prophesize(CacheService::class);
        $cache->get('desktop:seo:segment:666')
            ->shouldBeCalled()
            ->willReturn(null);

        $cache->get('desktop:seo:campaign:999')
            ->shouldBeCalled()
            ->willReturn($campaignSeo);

        $currentSegment = new Campaign();
        $currentSegment->setId(666);

        $campaign = new Campaign();
        $campaign->setId(999);
        $campaign->setCurrentSegment($currentSegment);

        $service = new SeoService();
        $service->setCacheService($cache->reveal());

        $service->setupCampaignSeo($campaign);
        $seoEntity = $service->getSeoEntity();

        $this->assertEquals($campaignSeo['title'], $seoEntity->getTitle());
        $this->assertEquals($campaignSeo['description'], $seoEntity->getDescription());
        $this->assertEquals($campaignSeo['robots'], $seoEntity->getRobots());
        $this->assertEquals($campaignSeo['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpBrandSeo()
    {
        $brandId = '9781';
        $key = 'desktop:seo:brand:9781';
        $expected = [
            'title' => 'organix',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $brandMock = $this->prophesize(Brand::class);

        $brandMock->getId()
            ->shouldBeCalled()
            ->willReturn($brandId);

        $cache = $this->prophesize(CacheService::class);
        $cache->get($key)
            ->shouldBeCalled()
            ->willReturn($expected);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setupBrandSeo($brandMock->reveal());

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($expected['title'], $seoEntity->getTitle());
        $this->assertEquals($expected['description'], $seoEntity->getDescription());
        $this->assertEquals($expected['robots'], $seoEntity->getRobots());
        $this->assertEquals($expected['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpSellerSeo()
    {
        $sellerId = '3752';
        $key = 'desktop:seo:supplier:3752';
        $expected = [
            'title' => 'Mega operadora',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $sellerMock = $this->prophesize(Seller::class);

        $sellerMock->getSellerId()
            ->shouldBeCalled()
            ->willReturn($sellerId);

        $cache = $this->prophesize(CacheService::class);
        $cache->get($key)
            ->shouldBeCalled()
            ->willReturn($expected);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setupSellerSeo($sellerMock->reveal());

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($expected['title'], $seoEntity->getTitle());
        $this->assertEquals($expected['description'], $seoEntity->getDescription());
        $this->assertEquals($expected['robots'], $seoEntity->getRobots());
        $this->assertEquals($expected['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpProductSeo()
    {
        $productSku = 'NI499EL94HINLAAR';
        $key = 'desktop:seo:product:NI499EL94HINLAAR';
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => 'noindex,follow',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $productMock = $this->prophesize(Product::class);

        $productMock->getSku()
            ->shouldBeCalled()
            ->willReturn($productSku);

        $productMock->isMarketPlace()
            ->shouldBeCalled()
            ->willReturn(true);

        $cache = $this->prophesize(CacheService::class);
        $cache->get($key)
            ->shouldBeCalled()
            ->willReturn($expected);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setUpProductSeo($productMock->reveal());

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($expected['title'], $seoEntity->getTitle());
        $this->assertEquals($expected['description'], $seoEntity->getDescription());
        $this->assertEquals($expected['robots'], $seoEntity->getRobots());
        $this->assertEquals($expected['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpProductSeoWithProductTypeCero()
    {
        $productSku = 'NI499EL94HINLAAR';
        $key = 'desktop:seo:product:NI499EL94HINLAAR';
        $expected = [
            'title' => 'Linio Argetina',
            'description' => 'Compra tecnologia en la mejor tienda online con envio a todo Argentina',
            'robots' => '',
            'keywords' => '',
            'seo_text' => 'This is an exmaple text',
        ];

        $productMock = $this->prophesize(Product::class);

        $productMock->getSku()
            ->shouldBeCalled()
            ->willReturn($productSku);

        $productMock->isMarketPlace()
            ->shouldBeCalled()
            ->willReturn(false);

        $cache = $this->prophesize(CacheService::class);
        $cache->get($key)
            ->shouldBeCalled()
            ->willReturn($expected);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setUpProductSeo($productMock->reveal());

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($expected['title'], $seoEntity->getTitle());
        $this->assertEquals($expected['description'], $seoEntity->getDescription());
        $this->assertEquals($expected['robots'], $seoEntity->getRobots());
        $this->assertEquals($expected['keywords'], $seoEntity->getKeywords());
    }

    public function testIsSettingUpStaticPageSeo()
    {
        $staticPage = [
            'css_text' => '',
            'js_text' => '',
            'page_content' => [],
            'revision_number' => 3,
            'seo' => [
                'title' => 'Linio Static Page',
                'description' => 'Compra en la mejor tienda online con envio a todo Argentina.',
                'robots' => 'noindex,follow',
                'keywords' => '',
                'seo_text' => 'This is an exmaple text',
            ],
        ];

        $seoService = new SeoService();
        $seoService->setUpStaticPageSeo($staticPage);

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals($staticPage['seo']['title'], $seoEntity->getTitle());
        $this->assertEquals($staticPage['seo']['description'], $seoEntity->getDescription());
        $this->assertEquals($staticPage['seo']['robots'], $seoEntity->getRobots());
        $this->assertEquals($staticPage['seo']['keywords'], $seoEntity->getKeywords());
        $this->assertEquals($staticPage['seo']['seo_text'], $seoEntity->getSeoText());
    }

    public function testIsSettingUpStaticPageSeoWithoutSeoField()
    {
        $staticPage = [
            'css_text' => '',
            'js_text' => '',
            'page_content' => [],
            'revision_number' => 3,
        ];

        $seoService = new SeoService();
        $seoService->setUpStaticPageSeo($staticPage);

        $seoEntity = $seoService->getSeoEntity();

        $this->assertEquals(null, $seoEntity->getTitle());
        $this->assertEquals(null, $seoEntity->getDescription());
        $this->assertEquals(null, $seoEntity->getRobots());
        $this->assertEquals(null, $seoEntity->getKeywords());
        $this->assertEquals(null, $seoEntity->getSeoText());
    }

    public function testIsSettingUpCategoryBrandSeoFields()
    {
        $seoFields = [
            'description' => 'asus / celulares',
            'keywords' => '',
            'revision_number' => 1,
            'robots' => 'INDEX, FOLLOW',
            'seo_text' => '<p>SEO text</p>',
            'title' => 'Los mejores celulares y tablets de ASUS',
        ];

        $category = new Category();
        $category->setId(5022);

        $brand = new Brand();
        $brand->setId(398);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('desktop:seo:category_brand:5022_398')
            ->shouldBeCalled()->willReturn($seoFields);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setUpSeoForCategoryAndBrand($category, $brand);

        $seo = $seoService->getSeoEntity();

        $this->assertEquals($seoFields['description'], $seo->getDescription());
        $this->assertEquals($seoFields['keywords'], $seo->getKeywords());
        $this->assertEquals($seoFields['robots'], $seo->getRobots());
        $this->assertEquals($seoFields['seo_text'], $seo->getSeoText());
        $this->assertEquals($seoFields['title'], $seo->getTitle());
    }

    public function testIsSettingUpNullSeoFieldsWhenCategoryBrandOnesDoNotExist()
    {
        $category = new Category();
        $category->setId(5022);

        $brand = new Brand();
        $brand->setId(398);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('desktop:seo:category_brand:5022_398')
            ->shouldBeCalled()->willReturn([]);

        $seoService = new SeoService();
        $seoService->setCacheService($cache->reveal());
        $seoService->setUpSeoForCategoryAndBrand($category, $brand);

        $seo = $seoService->getSeoEntity();

        $this->assertEquals(null, $seo->getDescription());
        $this->assertEquals(null, $seo->getKeywords());
        $this->assertEquals(null, $seo->getRobots());
        $this->assertEquals(null, $seo->getSeoText());
        $this->assertEquals(null, $seo->getTitle());
    }
}
