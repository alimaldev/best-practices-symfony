<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use GuzzleHttp\Psr7\Request;
use PHPUnit_Framework_TestCase;
use Psr\Http\Message\RequestInterface;

class HttpFixtureFormatterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provideActionRequests
     *
     * @param RequestInterface $request
     * @param string $expectedAction
     */
    public function testGetAction(RequestInterface $request, string $expectedAction)
    {
        $formatter = new HttpFixtureFormatter();

        $this->assertSame($expectedAction, $formatter->getAction($request));
    }

    /**
     * @dataProvider provideRequestParameters
     *
     * @param RequestInterface $request
     * @param array $expectedParameters
     */
    public function testGetRequestParameters(RequestInterface $request, array $expectedParameters)
    {
        $formatter = new HttpFixtureFormatter();

        $this->assertSame($expectedParameters, $formatter->getRequestParameters($request));
    }

    /**
     * @return array
     */
    public function provideActionRequests(): array
    {
        return [
            [new Request('HEAD', ''), HttpFixtureFormatter::DEFAULT_ACTION],
            [new Request('HEAD', 'http://localhost'), HttpFixtureFormatter::DEFAULT_ACTION],
            [new Request('HEAD', '?b=test&q=anotherTest'), HttpFixtureFormatter::DEFAULT_ACTION],
            [new Request('HEAD', 'customer/create'), 'customer_create'],
            [new Request('HEAD', 'customer/create/'), 'customer_create'],
            [new Request('HEAD', 'http://local/host'), 'host'],
            [new Request('HEAD', '/customer/create'), 'customer_create'],
            [new Request('HEAD', '/customer/create/'), 'customer_create'],
            [new Request('HEAD', 'http://localhost.com/customer/create'), 'customer_create'],
            [new Request('HEAD', 'http://localhost.com/customer/login?user=a&pass=b'), 'customer_login'],
        ];
    }

    /**
     * @return array
     */
    public function provideRequestParameters(): array
    {
        $body = [
            'username' => 'test',
            'email' => 'test@test.test',
        ];
        $streamableBody = json_encode($body);
        $expectedBody = ['body' => $streamableBody];

        $uri = '/?b=test&q=anotherTest';
        $query = ['b' => 'test', 'q' => 'anotherTest'];

        $combinedParameters = array_merge($query, $expectedBody);

        return [
            [new Request('POST', '/'), []],
            [new Request('GET', $uri), $query],
            [new Request('POST', $uri), $query],
            [new Request('POST', '/', [], $streamableBody), $expectedBody],
            [new Request('GET', '/', [], $streamableBody), $expectedBody],
            [new Request('GET', $uri, [], $streamableBody), $combinedParameters],
            [new Request('POST', $uri, [], $streamableBody), $combinedParameters],
        ];
    }
}
