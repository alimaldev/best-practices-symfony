<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit_Framework_TestCase;

class WriteFixtureTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    protected $fixtureRoot;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->fixtureRoot = vfsStream::setup('fixtureRoot');
    }

    /**
     * @dataProvider fixtureDataProvider
     *
     * @param string $country
     * @param string $action
     * @param $requestParameters
     * @param int $responseStatus
     * @param $responsePayload
     */
    public function testDisabledWriterDoesNotWrite(
        string $country,
        string $action,
        $requestParameters,
        int $responseStatus,
        $responsePayload
    ) {
        $writer = new WriteFixture($this->fixtureRoot->url(), $country, false);

        $fixturePath = $writer->generateFixtureFilePath($action, $requestParameters);

        $writer->record($action, $requestParameters, $responseStatus, $responsePayload);

        $this->assertFalse($this->fixtureRoot->hasChild($fixturePath));
    }

    /**
     * @dataProvider fixtureDataProvider
     *
     * @param string $country
     * @param string $action
     * @param $requestParameters
     * @param int $responseStatus
     * @param $responsePayload
     * @param string $expectedFixture
     */
    public function testEnabledWriterCreatesFixtureFile(
        string $country,
        string $action,
        $requestParameters,
        int $responseStatus,
        $responsePayload,
        string $expectedFixture
    ) {
        $writer = new WriteFixture($this->fixtureRoot->url(), $country, true);

        $fixturePath = $writer->generateFixtureFilePath($action, $requestParameters);
        $fixturePath = ltrim(str_replace($this->fixtureRoot->url(), '', $fixturePath), '/');

        $writer->record($action, $requestParameters, $responseStatus, $responsePayload);

        $this->assertTrue($this->fixtureRoot->hasChild($fixturePath));

        $actualFixture = file_get_contents($this->fixtureRoot->getChild($fixturePath)->url());

        $this->assertEquals($expectedFixture, $actualFixture);
    }

    /**
     * @dataProvider  fixtureDataProvider
     *
     * @param string $country
     */
    public function testBadPermissionsThrowException(string $country)
    {
        $this->fixtureRoot->chown(vfsStream::OWNER_USER_2);
        $this->fixtureRoot->chmod(0755);

        $this->expectException(\RuntimeException::class);

        $writer = new WriteFixture($this->fixtureRoot->url(), $country, true);
    }

    /**
     * @return array
     */
    public function fixtureDataProvider(): array
    {
        $data = [
            ['mx', 'search', ['q' => 'iphone'], 200, '{ "success": true }'],
            ['ar', 'category', 1, 0, '{ "success": true }'],
            ['pe', 'product', 'abc123', 0, ['success' => true]],
            ['ve', 'order', ['order_number' => '1111'], 0, ['success' => true]],
        ];

        foreach ($data as $key => $parameters) {
            $response = [
                'requestParameters' => $parameters[2],
                'responseStatus' => $parameters[3],
                'responsePayload' => is_string($parameters[4]) ? $parameters[4] : json_encode($parameters[4]),
            ];
            $response = json_encode($response) . "\n";

            $data[$key] = array_merge($data[$key], [$response]);
        }

        return $data;
    }
}
