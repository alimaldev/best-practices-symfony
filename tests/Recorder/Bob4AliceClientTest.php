<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use org\bovigo\vfs\vfsStream;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class Bob4AliceClientTest extends \PHPUnit_Framework_TestCase
{
    public function testRequestGetsRecorded()
    {
        $country = 'mx';

        $mockFileSystem = vfsStream::setup('testRoot');
        $fixtureRecorder = new WriteFixture($mockFileSystem->url(), $country, true, new HttpFixtureFormatter());

        $expectedStatus = 200;
        $expectedPayload = json_encode(['success' => 'we did it']);
        $expectedResponse = new Response($expectedStatus, [], $expectedPayload);

        $handler = new MockHandler([$expectedResponse]);
        $stack = HandlerStack::create($handler);

        $request = new Request();
        $requestStack = new RequestStack();
        $requestStack->push($request);

        $client = new Bob4AliceClient('http://bob07/', $country, $fixtureRecorder, ['handler' => $stack]);
        $client->setRequestStack($requestStack);

        $requestParameters = [
            'username' => 'test@test.com',
            'gender' => 'transcendent',
        ];
        $expectedBody = ['body' => json_encode($requestParameters)];

        $action = '/customer/create';
        $expectedAction = 'customer_create';

        $actualResponse = $client->request('POST', $action, ['json' => $requestParameters]);

        $this->assertSame($expectedResponse, $actualResponse);

        $expectedFixturePath = $fixtureRecorder->generateFixtureFilePath($expectedAction, json_encode($expectedBody));
        $expectedFixturePath = ltrim(str_replace($mockFileSystem->url(), '', $expectedFixturePath), '/');

        $this->assertTrue($mockFileSystem->hasChild($expectedFixturePath));

        $expectedFixture = [
            'requestParameters' => $expectedBody,
            'responseStatus' => $expectedStatus,
            'responsePayload' => $expectedPayload,
        ];

        $this->assertSame(
            json_encode($expectedFixture) . "\n",
            file_get_contents($mockFileSystem->getChild($expectedFixturePath)->url())
        );
    }
}
