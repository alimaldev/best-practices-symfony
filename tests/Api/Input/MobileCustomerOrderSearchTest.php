<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Exception\InputException;

class MobileCustomerOrderSearchTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider properProvider
     *
     * @param int $page
     * @param string $fromDate
     * @param string $toDate
     */
    public function testIsGettingSearchResults($page = null, $fromDate = null, $toDate = null)
    {
        $input = new MobileCustomerOrderSearch();
        $data = ['page' => $page, 'fromDate' => $fromDate, 'toDate' => $toDate];

        $search = $input->fromCustomerOrderSearch($data);
        $this->assertInstanceOf(Search::class, $search);
    }

    /**
     * @dataProvider failsProvider
     *
     * @param int $page
     * @param string $fromDate
     * @param string $toDate
     */
    public function testIsFailingOrderSearch($page = null, $fromDate = null, $toDate = null)
    {
        $input = new MobileCustomerOrderSearch();
        $data = ['page' => $page, 'fromDate' => $fromDate, 'toDate' => $toDate];

        $this->expectException(InputException::class);
        $input->fromCustomerOrderSearch($data);
    }

    /**
     * @return array
     */
    public function properProvider() : array
    {
        $today = new \DateTime();
        $yesterday = new \DateTime('yesterday');

        $propers = [
            [
                'page' => 2,
                'fromDate' => $yesterday->format(DATE_ISO8601),
                'toDate' => $today->format(DATE_ISO8601),
            ],
            [
                'page' => 2,
                'fromDate' => $yesterday->format(DATE_ISO8601),
            ],
            [
                'page' => 2,
                null,
                'toDate' => $yesterday->format(DATE_ISO8601),
            ],
            [
                'page' => 2,
            ],
            [
                'page' => 2,
                'fromDate' => null,
                'toDate' => null,
            ],
            [
                'page' => 2,
                'fromDate' => $today->format(DATE_ISO8601),
                'toDate' => $today->format(DATE_ISO8601),

            ],
        ];

        return $propers;
    }

    /**
     * @return array
     */
    public function failsProvider() : array
    {
        $fails = [
            [
                'page' => null,
            ],
            [
                'page' => 1,
                'fromDate' => 'SomeDate',
                'toDate' => 'AnotherDate',
            ],
            [
                'page' => 1,
                'fromDate' => '2010-03-12',
                'toDate' => '2016-01-01',
            ],
        ];

        return $fails;
    }
}
