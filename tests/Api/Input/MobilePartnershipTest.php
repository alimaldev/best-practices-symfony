<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Test\FormTestCase;

class MobilePartnershipTest extends FormTestCase
{
    public function testIsTransformingFromPartnershipData()
    {
        $data = [
            'code' => 'ab123',
            'accountNumber' => 'x55563454',
            'level' => 'platinum',
        ];

        $input = new MobilePartnership();
        $input->setFormFactory($this->getFormFactory());
        $actual = $input->fromPartnershipData($data);

        $this->assertEquals($data['code'], $actual->getCode());
        $this->assertEquals($data['accountNumber'], $actual->getAccountNumber());
        $this->assertEquals($data['level'], $actual->getLevel());
    }

    public function testIsReturningErrorWhenTransformingInputPartnershipInvalidData()
    {
        $data = [
            'code' => null,
            'accountNumber' => 4,
            'level' => 'platinum',
        ];

        $input = new MobilePartnership();
        $input->setFormFactory($this->getFormFactory(true));

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::INVALID_DATA);

        $input->fromPartnershipData($data);
    }

    public function testIsTransformingFromPartnershipWhenNotSetLevelData()
    {
        $data = [
            'code' => 'ab123',
            'accountNumber' => 'x55563454',
            'level' => '',
        ];

        $input = new MobilePartnership();
        $input->setFormFactory($this->getFormFactory());
        $actual = $input->fromPartnershipData($data);

        $this->assertEquals($data['code'], $actual->getCode());
        $this->assertEquals($data['accountNumber'], $actual->getAccountNumber());
        $this->assertEmpty($actual->getLevel());
    }
}
