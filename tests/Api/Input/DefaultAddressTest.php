<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

class DefaultAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingFromLocationParts()
    {
        $input = [
            'postcode' => 'foobar',
            'region' => null,
            'city' => 'barfoo',
        ];

        $output = new DefaultAddress();
        $address = $output->fromLocationParts($input);
        $this->assertEquals('foobar', $address->getPostcode());
        $this->assertEquals('barfoo', $address->getCity());
    }

    public function testIsTransformingFromPartialAddress()
    {
        $input = [
            'id' => 42211,
            'title' => null,
            'type' => null,
            'firstName' => 'Adolfinho',
            'lastName' => 'Mequetreque',
            'prefix' => '34',
            'line1' => '123123123',
            'line2' => null,
            'betweenStreet1' => null,
            'betweenStreet2' => null,
            'streetNumber' => '123123123',
            'apartment' => 'asdasdasd',
            'lot' => null,
            'neighborhood' => null,
            'department' => null,
            'municipality' => 'asdasdasdasd',
            'urbanization' => null,
            'city' => null,
            'cityId' => null,
            'cityName' => null,
            'region' => 'Buenos Aires',
            'regionId' => null,
            'regionCode' => null,
            'regionName' => null,
            'postcode' => '1231',
            'additionalInformation' => 'asdasdasdasd',
            'phone' => '',
            'mobilePhone' => '212312312',
            'countryId' => 12,
            'countryCode' => 'AR',
            'countryName' => 'Argentina',
            'taxIdentificationNumber' => null,
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => null,
            'invoiceType' => null,
        ];

        $output = new DefaultAddress();
        $address = $output->fromPartialAddress($input);
        $this->assertEquals(42211, $address->getId());
        $this->assertNull($address->getTitle());
        $this->assertNull($address->getType());
        $this->assertEquals('Adolfinho', $address->getFirstName());
        $this->assertEquals('Mequetreque', $address->getLastName());
        $this->assertEquals('34', $address->getPrefix());
        $this->assertEquals('123123123', $address->getLine1());
        $this->assertEquals('123123123', $address->getStreetNumber());
        $this->assertEquals('asdasdasd', $address->getApartment());
        $this->assertEquals('asdasdasdasd', $address->getMunicipality());
        $this->assertEquals('Buenos Aires', $address->getRegion());
        $this->assertEquals('asdasdasdasd', $address->getAdditionalInformation());
        $this->assertEquals('212312312', $address->getMobilePhone());
        $this->assertEquals(12, $address->getCountryId());
        $this->assertEquals('AR', $address->getCountryCode());
        $this->assertEquals('Argentina', $address->getCountryName());
    }
}
