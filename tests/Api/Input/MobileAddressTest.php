<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

class MobileAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingFromLocationParts()
    {
        $input = [
            'postcode' => 'foobar',
            'region' => null,
            'city' => 'barfoo',
        ];

        $output = new MobileAddress();
        $address = $output->fromLocationParts($input);

        $this->assertEquals($input['postcode'], $address->getPostcode());
        $this->assertEquals($input['city'], $address->getCity());
    }

    public function testIsTransformingFromPartialAddress()
    {
        $input = [
            'id' => 42211,
            'title' => null,
            'type' => null,
            'firstName' => 'Adolfinho',
            'lastName' => 'Mequetreque',
            'prefix' => '34',
            'line1' => '123123123',
            'line2' => null,
            'betweenStreet1' => null,
            'betweenStreet2' => null,
            'streetNumber' => '123123123',
            'apartment' => 'asdasdasd',
            'lot' => null,
            'neighborhood' => null,
            'department' => null,
            'municipality' => 'asdasdasdasd',
            'urbanization' => null,
            'city' => null,
            'cityId' => null,
            'cityName' => null,
            'region' => 'Buenos Aires',
            'regionId' => null,
            'regionCode' => null,
            'regionName' => null,
            'postcode' => '1231',
            'additionalInformation' => 'asdasdasdasd',
            'phone' => '',
            'mobilePhone' => '212312312',
            'countryId' => 12,
            'countryCode' => 'AR',
            'countryName' => 'Argentina',
            'taxIdentificationNumber' => null,
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => null,
            'invoiceType' => null,
            'company' => 'company',
        ];

        $output = new MobileAddress();
        $address = $output->fromPartialAddress($input);

        $this->assertEquals($input['id'], $address->getId());
        $this->assertNull($address->getTitle());
        $this->assertNull($address->getType());
        $this->assertEquals($input['firstName'], $address->getFirstName());
        $this->assertEquals($input['lastName'], $address->getLastName());
        $this->assertEquals($input['prefix'], $address->getPrefix());
        $this->assertEquals($input['streetNumber'], $address->getLine1());
        $this->assertEquals($input['streetNumber'], $address->getStreetNumber());
        $this->assertEquals($input['apartment'], $address->getApartment());
        $this->assertEquals($input['municipality'], $address->getMunicipality());
        $this->assertEquals($input['region'], $address->getRegion());
        $this->assertEquals($input['additionalInformation'], $address->getAdditionalInformation());
        $this->assertEquals($input['mobilePhone'], $address->getMobilePhone());
        $this->assertEquals($input['countryId'], $address->getCountryId());
        $this->assertEquals($input['countryCode'], $address->getCountryCode());
        $this->assertEquals($input['countryName'], $address->getCountryName());
        $this->assertEquals($input['company'], $address->getCompany());
    }
}
