<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer\CreditCard;
use Linio\Frontend\Exception\FormValidationException;
use Prophecy\Argument;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class MobileStoredCreditCardTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingCreditCardObjectFromCreditCardData()
    {
        $creditCardData = [
            'cardHolderFirstName' => 'Ana',
            'cardHolderLastName' => 'Gonzalez',
            'cardNumber' => '9999 9999 9999 9999',
            'expirationDate' => '1-05-2020',
        ];

        $creditCard = new CreditCard();
        $creditCard->setCardholderFirstName($creditCardData['cardHolderFirstName']);
        $creditCard->setCardholderLastName($creditCardData['cardHolderLastName']);
        $creditCard->setCardNumber($creditCardData['cardNumber']);
        $creditCard->setExpirationDate($creditCardData['expirationDate']);

        $addCreditCardForm = $this->prophesize(FormInterface::class);

        $addCreditCardForm->submit(Argument::type('array'))->shouldBeCalled();

        $addCreditCardForm->isValid()
            ->shouldBeCalled()
            ->willReturn(true);

        $addCreditCardForm->getData()->shouldBeCalled()->willReturn($creditCard);

        $input = new MobileStoredCreditCard();

        $input->setAddCreditCardForm($addCreditCardForm->reveal());

        $creditCard = $input->fromCreditCard($creditCardData);

        $this->assertInstanceOf(CreditCard::class, $creditCard);
    }

    public function testIsGettingValidationErrorFromCreditCardData()
    {
        $creditCardData = [
            'cardHolderFirstName' => 'Ana',
        ];

        $addCreditCardForm = $this->prophesize(FormInterface::class);

        $addCreditCardForm->submit(Argument::type('array'))->shouldBeCalled();

        $addCreditCardForm->isValid()
            ->shouldBeCalled()
            ->willReturn(false);

        $addCreditCardForm->getName()->shouldBeCalled()->willReturn('Error');

        $formError = new FormError('INVALID_DATA');
        $formError->setOrigin($addCreditCardForm->reveal());

        $addCreditCardForm->getErrors(true, true)
            ->shouldBeCalled()
            ->willReturn([$formError]);

        $input = new MobileStoredCreditCard();

        $input->setAddCreditCardForm($addCreditCardForm->reveal());

        $this->expectException(FormValidationException::class);
        $this->expectExceptionMessage('INVALID_DATA');
        $input->fromCreditCard($creditCardData);
    }
}
