<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Test\FormTestCase;

class MobileLoyaltyProgramTest extends FormTestCase
{
    public function testIsGettingLoyaltyProgramObjectFromLoyaltyProgramData()
    {
        $loyaltyProgramData['loyaltyId'] = 1;

        $loyaltyInput = new MobileLoyaltyProgram();

        $loyaltyInput->setFormFactory($this->getFormFactory());
        $loyaltyInput->setLoyaltyProgram('Club_Premier');

        $loyaltyProgram = $loyaltyInput->fromLoyaltyProgram($loyaltyProgramData);

        $this->assertInstanceOf(LoyaltyProgram::class, $loyaltyProgram);
        $this->assertEquals($loyaltyProgram->getId(), $loyaltyProgramData['loyaltyId']);
    }

    public function testIsGettingFormValidationError()
    {
        $loyaltyProgramData['otherField'] = '';

        $loyaltyInput = new MobileLoyaltyProgram();

        $loyaltyInput->setFormFactory($this->getFormFactory(true));
        $loyaltyInput->setLoyaltyProgram('Club_Premier');

        $this->expectException(FormValidationException::class);
        $this->expectExceptionMessage('INVALID_DATA');
        $loyaltyInput->fromLoyaltyProgram($loyaltyProgramData);
    }
}
