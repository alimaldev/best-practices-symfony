<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\Method\Regular;
use Linio\Type\Money;

class DefaultOrderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingInputIntoOrder()
    {
        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Anabela',
                'lastName' => 'Torrealba',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'billingAddress' => null,
            'installments' => 3,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => null,
            'pickupStoreId' => 1,
            'paymentMethod' => 'CreditCard',
            'dependentPaymentMethod' => 'Foobar',
            'creditCardBinNumber' => '123123',
            'paymentData' => ['foo' => 'bar'],
            'extraFields' => ['baz'],
            'walletPoints' => null,
        ];

        $output = new DefaultOrder();
        $order = $output->fromPlaceOrder($input, new Order('123'));
        $this->assertEquals(new Money(316), $order->getGrandTotal());
        $this->assertEquals($input['extraFields'], $order->getMetadata());
        $this->assertNull($order->getCoupon());

        $paymentMethod = new CreditCard('CreditCard');
        $paymentMethod->setPaymentData($input['paymentData']);
        $paymentMethod->setBinNumber($input['creditCardBinNumber']);
        $this->assertEquals($paymentMethod, $order->getPaymentMethod());
        $this->assertEquals('Foobar', $order->getDependentPaymentMethod());
        $this->assertEquals(3, $order->getSelectedInstallmentQuantity());
        $this->assertSame($input['pickupStoreId'], $order->getPickupStores()->getSelected()->getId());
        $this->assertEquals('100025', $order->getShippingAddress()->getPostCode());
        $this->assertEquals('BOLIVAR', $order->getShippingAddress()->getRegion());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getMunicipality());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getCity());
        $this->assertEquals('regular', $order->getPackages()->get(1)->getSelectedShippingQuote()->getShippingMethod());
        $this->assertEquals(2, $order->getItems()->get('AP004EL0QM8WWLAEC-81213')->getQuantity());
    }

    public function testIsTransformingInputIntoOrderWithPartialAddress()
    {
        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Anabela',
                'lastName' => 'Torrealba',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
            ],
            'billingAddress' => null,
            'installments' => 3,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => null,
            'pickupStoreId' => null,
            'paymentMethod' => 'CashOnDelivery_Payment',
            'creditCardBinNumber' => null,
            'paymentData' => ['foo' => 'bar'],
            'extraFields' => ['baz'],
            'walletPoints' => null,
        ];

        $output = new DefaultOrder();
        $order = $output->fromPlaceOrder($input, new Order('123'));
        $this->assertEquals(new Money(316), $order->getGrandTotal());
        $this->assertEquals($input['extraFields'], $order->getMetadata());
        $this->assertNull($order->getCoupon());

        $paymentMethod = new Regular('CashOnDelivery_Payment');
        $paymentMethod->setPaymentData($input['paymentData']);
        $this->assertEquals($paymentMethod, $order->getPaymentMethod());
        $this->assertEquals(3, $order->getSelectedInstallmentQuantity());
        $this->assertNull($order->getShippingAddress()->getPostCode());
        $this->assertNull($order->getShippingAddress()->getMunicipality());
        $this->assertEquals('BOLIVAR', $order->getShippingAddress()->getRegion());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getCity());
        $this->assertEquals('regular', $order->getPackages()->get(1)->getSelectedShippingQuote()->getShippingMethod());
        $this->assertEquals(2, $order->getItems()->get('AP004EL0QM8WWLAEC-81213')->getQuantity());
    }

    public function testIsTransformingInputIntoOrderWithBillingAddress()
    {
        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Adolfinho',
                'lastName' => 'Mequetreque',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'billingAddress' => [
                'id' => 2,
                'firstName' => 'Adolfinho',
                'lastName' => 'Mequetreque',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'installments' => 3,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => 'FREE',
            'pickupStoreId' => null,
            'paymentMethod' => 'CashOnDelivery_Payment',
            'creditCardBinNumber' => null,
            'paymentData' => null,
            'extraFields' => null,
            'walletPoints' => null,
        ];

        $output = new DefaultOrder();
        $order = $output->fromPlaceOrder($input, new Order('123'));
        $this->assertEquals(new Money(316), $order->getGrandTotal());
        $this->assertEmpty($order->getMetadata());
        $this->assertEquals('FREE', $order->getCoupon()->getCode());

        $paymentMethod = new Regular('CashOnDelivery_Payment');
        $this->assertEquals($paymentMethod, $order->getPaymentMethod());
        $this->assertEquals(3, $order->getSelectedInstallmentQuantity());
        $this->assertEquals('100025', $order->getShippingAddress()->getPostCode());
        $this->assertEquals('BOLIVAR', $order->getShippingAddress()->getRegion());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getMunicipality());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getCity());
        $this->assertEquals('100025', $order->getBillingAddress()->getPostCode());
        $this->assertEquals('BOLIVAR', $order->getBillingAddress()->getRegion());
        $this->assertEquals('CALUMA', $order->getBillingAddress()->getMunicipality());
        $this->assertEquals('CALUMA', $order->getBillingAddress()->getCity());
        $this->assertEquals('regular', $order->getPackages()->get(1)->getSelectedShippingQuote()->getShippingMethod());
        $this->assertEquals(2, $order->getItems()->get('AP004EL0QM8WWLAEC-81213')->getQuantity());
    }

    public function testThrowsErrorIfShippingQuoteIsMissingShippingMethod()
    {
        $this->expectException(InputException::class);

        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Anabela',
                'lastName' => 'Torrealba',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'billingAddress' => null,
            'installments' => 3,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'paymentMethod' => 'regular', // Actual error from mobile app test case.
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => null,
            'pickupStoreId' => 1,
            'paymentMethod' => 'CreditCard',
            'creditCardBinNumber' => '123123',
            'paymentData' => ['foo' => 'bar'],
            'extraFields' => ['baz'],
            'walletPoints' => null,
        ];

        $output = new DefaultOrder();
        $output->fromPlaceOrder($input, new Order('123'));
    }

    public function testIsTransformingInputIntoOrderWithoutInstallments()
    {
        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Anabela',
                'lastName' => 'Torrealba',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'billingAddress' => null,
            'installments' => null,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => null,
            'pickupStoreId' => 1,
            'paymentMethod' => 'CreditCard',
            'dependentPaymentMethod' => 'Foobar',
            'creditCardBinNumber' => '123123',
            'paymentData' => ['foo' => 'bar'],
            'extraFields' => ['baz'],
            'walletPoints' => null,
        ];

        $output = new DefaultOrder();
        $order = $output->fromPlaceOrder($input, new Order('123'));
        $this->assertEquals(new Money(316), $order->getGrandTotal());
        $this->assertEquals($input['extraFields'], $order->getMetadata());
        $this->assertNull($order->getCoupon());

        $paymentMethod = new CreditCard('CreditCard');
        $paymentMethod->setPaymentData($input['paymentData']);
        $paymentMethod->setBinNumber($input['creditCardBinNumber']);
        $this->assertEquals($paymentMethod, $order->getPaymentMethod());
        $this->assertEquals('Foobar', $order->getDependentPaymentMethod());
        $this->assertNull($order->getSelectedInstallmentQuantity());
        $this->assertSame($input['pickupStoreId'], $order->getPickupStores()->getSelected()->getId());
        $this->assertEquals('100025', $order->getShippingAddress()->getPostCode());
        $this->assertEquals('BOLIVAR', $order->getShippingAddress()->getRegion());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getMunicipality());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getCity());
        $this->assertEquals('regular', $order->getPackages()->get(1)->getSelectedShippingQuote()->getShippingMethod());
        $this->assertEquals(2, $order->getItems()->get('AP004EL0QM8WWLAEC-81213')->getQuantity());
    }

    public function testIsTransformingInputIntoOrderWithWalletPoints()
    {
        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Anabela',
                'lastName' => 'Torrealba',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'billingAddress' => null,
            'installments' => null,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => null,
            'pickupStoreId' => 1,
            'paymentMethod' => 'CreditCard',
            'dependentPaymentMethod' => 'Foobar',
            'creditCardBinNumber' => '123123',
            'paymentData' => ['foo' => 'bar'],
            'extraFields' => ['baz'],
            'walletPoints' => 25000,
        ];

        $output = new DefaultOrder();
        $order = $output->fromPlaceOrder($input, new Order('123'));
        $this->assertEquals(new Money(316), $order->getGrandTotal());
        $this->assertEquals($input['extraFields'], $order->getMetadata());
        $this->assertNull($order->getCoupon());

        $paymentMethod = new CreditCard('CreditCard');
        $paymentMethod->setPaymentData($input['paymentData']);
        $paymentMethod->setBinNumber($input['creditCardBinNumber']);
        $this->assertEquals($paymentMethod, $order->getPaymentMethod());
        $this->assertEquals('Foobar', $order->getDependentPaymentMethod());
        $this->assertNull($order->getSelectedInstallmentQuantity());
        $this->assertSame($input['pickupStoreId'], $order->getPickupStores()->getSelected()->getId());
        $this->assertEquals('100025', $order->getShippingAddress()->getPostCode());
        $this->assertEquals('BOLIVAR', $order->getShippingAddress()->getRegion());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getMunicipality());
        $this->assertEquals('CALUMA', $order->getShippingAddress()->getCity());
        $this->assertEquals('regular', $order->getPackages()->get(1)->getSelectedShippingQuote()->getShippingMethod());
        $this->assertEquals(2, $order->getItems()->get('AP004EL0QM8WWLAEC-81213')->getQuantity());
        $this->assertEquals(25000, $order->getWallet()->getTotalPointsUsed()->getAmount());
    }

    public function testFailsTransformingInputIntoOrderIfPaymentMethodIsMissing()
    {
        $input = [
            'grandTotal' => 316,
            'shippingAddress' => [
                'id' => 1,
                'firstName' => 'Anabela',
                'lastName' => 'Torrealba',
                'address1' => 'test',
                'address2' => null,
                'streetNumber' => 'test',
                'city' => 'CALUMA',
                'municipality' => 'CALUMA',
                'neighborhood' => null,
                'region' => 'BOLIVAR',
                'countryId' => 63,
                'mobilePhone' => '823898932983',
                'postcode' => '100025',
            ],
            'billingAddress' => null,
            'installments' => 3,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                    'fee' => 300,
                    'estimatedDeliveryDate' => '2016-04-07',
                    'deliveredByChristmas' => false,
                    'freeShipping' => false,
                    'selected' => true,
                ],
            ],
            'items' => [
                'AP004EL0QM8WWLAEC-81213' => 2,
            ],
            'coupon' => null,
            'pickupStoreId' => 1,
            'paymentMethod' => null,
            'dependentPaymentMethod' => 'Foobar',
            'creditCardBinNumber' => '123123',
            'paymentData' => ['foo' => 'bar'],
            'extraFields' => ['baz'],
            'walletPoints' => null,
        ];

        $transformer = new DefaultOrder();

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::PAYMENT_METHOD_NOT_ALLOWED);

        $transformer->fromPlaceOrder($input, new Order('123'));
    }
}
