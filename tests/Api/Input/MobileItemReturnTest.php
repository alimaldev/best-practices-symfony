<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\Customer\ItemReturnRequestForm;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class MobileItemReturnTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingItemReturn()
    {
        $expected = [
            'orderNumber' => '123',
            'sku' => 'ABC-123',
            'quantity' => 1,
            'reason' => 16,
            'action' => 25,
            'comment' => 'comment',
        ];

        $form = $this->prophesize(FormInterface::class);
        $form->isValid()->willReturn(true);
        $form->submit($expected)->willReturn(null);
        $form->getData()->willReturn($expected);

        $formFactory = $this->prophesize(FormFactory::class);
        $formFactory->create(ItemReturnRequestForm::class, null, ['csrf_protection' => false])->willReturn($form);

        $input = new MobileItemReturn();
        $input->setFormFactory($formFactory->reveal());

        $actual = $input->fromItemReturn($expected);

        $this->assertSame($expected, $actual);
    }

    public function testIsReturningErrorWhenTransformingItemReturnWithInvalidData()
    {
        $expected = [
            'orderNumber' => '123',
            'sku' => 'ABC-123',
            'quantity' => 1,
            'reason' => 16,
            'action' => 25,
        ];

        $form = $this->prophesize(FormInterface::class);
        $form->submit($expected)->willReturn(null);
        $form->isValid()->willReturn(false);
        $form->getErrors(true, true)->willReturn([]);

        $formFactory = $this->prophesize(FormFactory::class);
        $formFactory->create(ItemReturnRequestForm::class, null, ['csrf_protection' => false])->willReturn($form);

        $input = new MobileItemReturn();
        $input->setFormFactory($formFactory->reveal());

        $this->expectException(InputException::class);

        $input->fromItemReturn($expected);
    }
}
