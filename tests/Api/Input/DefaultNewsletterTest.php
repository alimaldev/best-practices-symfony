<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Newsletter\NewsletterSubscription;
use Linio\Frontend\Newsletter\Preference;

class DefaultNewsletterTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingFromNewsletterSettings()
    {
        $preferences = null;

        $data = [
            'frequency' => 3,
            'preferences' => [1 => 1, 2 => 2, 3 => 3, 4 => 5, 5 => 7, 6 => 8, 7 => 9],
            'phoneNumber' => '(555) 555-5555',
            'subscribedToSms' => 'true',
        ];

        $frequencies = [
            1 => 'daily mail',
            2 => 'two emails a week',
            3 => 'one email every weeks',
            4 => 'one email every two weeks',
            5 => 'one email every month',
        ];

        $newsletterPreferences = [
            [
                'id' => 1,
                'name' => "Fotograf\u00eda",
                'enabled' => false,
            ],
            [
                'id' => 2,
                'name' => "TV\/Audio\/Video",
                'enabled' => false,
            ],
            [
                'id' => 3,
                'name' => 'Celulares',
                'enabled' => false,
            ],
            [
                'id' => 4,
                'name' => "Computaci\u00f3n",
                'enabled' => false,
            ],
            [
                'id' => 5,
                'name' => 'Moda',
                'enabled' => false,
            ],
            [
                'id' => 6,
                'name' => "Electrodom\u00e9sticos",
                'enabled' => false,
            ],
            [
                'id' => 7,
                'name' => 'Salud y Belleza',
                'enabled' => false,
            ],
            [
                'id' => 8,
                'name' => 'Hogar',
                'enabled' => false,
            ],
            [
                'id' => 9,
                'name' => "Ni\u00f1os y Bebes",
                'enabled' => false,
            ],
            [
                'id' => 10,
                'name' => 'Libros y Revistas',
                'enabled' => false,
            ],
            [
                'id' => 11,
                'name' => 'Videojuegos',
                'enabled' => false,
            ],
            [
                'id' => 12,
                'name' => 'Deporte',
                'enabled' => false,
            ],
        ];

        foreach ($newsletterPreferences as $preferenceData) {
            $preferences[] = new Preference(
                $preferenceData['id'],
                $preferenceData['name'],
                $preferenceData['enabled']
            );
        }

        $input = new DefaultNewsletter();

        $newsletterSubscription = new NewsletterSubscription('test@test.com');
        $newsletterSubscription->setFrequencies($frequencies);
        $newsletterSubscription->setPreferences($preferences);

        $newsletter = $input->fromNewsletterSettings($data, $newsletterSubscription);

        $this->assertEquals($data['frequency'], $newsletter->getFrequency());
        $this->assertEquals($data['phoneNumber'], $newsletter->getPhoneNumber());
        foreach ($newsletter->getPreferences() as $preference) {
            if (in_array($preference->getId(), $data['preferences'])) {
                $this->assertTrue($preference->isEnabled());
            }
        }
        $this->assertNotEmpty($newsletter->getPreferences());
        $this->assertNotEmpty($newsletter->getFrequencies());
    }
}
