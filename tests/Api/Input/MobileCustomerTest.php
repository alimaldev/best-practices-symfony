<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use DateTime;
use Linio\Frontend\Entity\Customer\BankTransferConfirmation;

class MobileCustomerTest extends \PHPUnit_Framework_TestCase
{
    public function testGetsBankTransferConfirmationFromTransferData()
    {
        $transferData = [
            'bankId' => 1,
            'date' => DateTime::createFromFormat('Y-m-d', '2020-05-01'),
            'transferNumber' => '01234567890',
            'amount' => '100',
        ];

        $bankTransferConfirmation = new BankTransferConfirmation();
        $bankTransferConfirmation->setBankId($transferData['bankId']);
        $bankTransferConfirmation->setDate($transferData['date']);
        $bankTransferConfirmation->setNumber($transferData['transferNumber']);
        $bankTransferConfirmation->setAmount($transferData['amount']);

        $input = new MobileCustomer();

        $actual = $input->fromConfirmBankTransfer($transferData);

        $this->assertEquals($bankTransferConfirmation, $actual);
    }
}
