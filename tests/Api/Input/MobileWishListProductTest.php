<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use DateTime;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;

class MobileWishListProductTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingDataIntoProductToBeAdded()
    {
        $data = [
            'configSku' => 'abc1334',
            'simpleSku' => 'abc1334-abc234',
            'price' => 49900.0,
            'name' => 'some product',
        ];

        $input = new MobileWishListProduct();
        $actual = $input->fromAddProduct($data);

        $this->assertEquals($data['configSku'], $actual->getSelectedSimple()->getConfigSku());
        $this->assertEquals($data['simpleSku'], $actual->getSelectedSimple()->getSku());
        $this->assertEquals($data['price'], $actual->getWishListPrice()->getMoneyAmount());
        $this->assertInstanceOf(DateTime::class, $actual->getAddedOn());
    }

    public function testIsReturningErrorWhenInputDataIsNotValid()
    {
        $data = [
            'sku' => 'abc1334',
        ];

        $input = new MobileWishListProduct();

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::WISH_LIST_INVALID_PRODUCT);

        $input->fromAddProduct($data);
    }

    public function testIsTransformingProductToBeRemoved()
    {
        $data = [
            'simpleSku' => 'abc1334-abc234',
        ];

        $input = new MobileWishListProduct();
        $actual = $input->fromProductToRemove($data);

        $this->assertEquals($data['simpleSku'], $actual->getSelectedSimple()->getSku());
    }

    public function testIsReturningErrorWhenProductToRemoveDataIsInvalid()
    {
        $data = [
            'sku' => 'abc1334',
        ];

        $input = new MobileWishListProduct();

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::WISH_LIST_INVALID_PRODUCT);

        $input->fromProductToRemove($data);
    }
}
