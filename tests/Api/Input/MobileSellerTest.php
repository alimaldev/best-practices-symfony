<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Seller\RatingCollection;
use Linio\Frontend\Entity\Seller\RatingSummary;
use Linio\Frontend\Entity\Seller\Seller;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Form\SellerReviewForm;
use Linio\Frontend\Seller\SellerService;
use Prophecy\Argument;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

class MobileSellerTest extends \PHPUnit_Framework_TestCase
{
    public function testItMapsToASellerReview()
    {
        $data = [
            'sellerId' => 1,
            'rating' => 5,
            'title' => 'Title',
            'detail' => 'This is a review',
        ];

        $ratingSummary = new RatingSummary(1.0, 1, new RatingCollection());
        $seller = new Seller(1, 'Seller Name', 'seller-slug', 'type', $ratingSummary, 'operationType');

        $sellerService = $this->prophesize(SellerService::class);
        $sellerService->get($seller->getId())->shouldBeCalled()->willReturn($seller);

        $sellerReviewInForm = new SellerReview();
        $sellerReviewInForm->setDetail($data['detail']);
        $sellerReviewInForm->setTitle($data['title']);
        $sellerReviewInForm->setRating($data['rating']);

        $form = $this->prophesize(FormInterface::class);
        $form->submit($data)->willReturn(null);
        $form->isValid()->willReturn(true);
        $form->getData()->willReturn($sellerReviewInForm);

        $formFactory = $this->prophesize(FormFactory::class);
        $formFactory->create(SellerReviewForm::class, null, ['csrf_protection' => false])->willReturn($form);

        $input = new DefaultSeller();
        $input->setSellerService($sellerService->reveal());
        $input->setFormFactory($formFactory->reveal());

        $expected = clone $sellerReviewInForm;
        $expected->setSeller($seller);

        $actual = $input->toSellerReview($data);

        $this->assertEquals($expected, $actual);
    }

    public function testItFailsWithInvalidDataWhenMappingToASellerReview()
    {
        $data = [
            'sellerId' => 'number',
            'rating' => 'rating',
            'title' => null,
            'detail' => null,
        ];

        $sellerService = $this->prophesize(SellerService::class);
        $sellerService->get(Argument::any())->shouldNotBeCalled();

        $form = $this->prophesize(FormInterface::class);
        $form->submit($data)->willReturn(null);
        $form->isValid()->willReturn(false);
        $form->getErrors(true, true)->willReturn([]);

        $formFactory = $this->prophesize(FormFactory::class);
        $formFactory->create(SellerReviewForm::class, null, ['csrf_protection' => false])->willReturn($form);

        $input = new DefaultSeller();
        $input->setSellerService($sellerService->reveal());
        $input->setFormFactory($formFactory->reveal());

        $this->expectException(FormValidationException::class);

        $input->toSellerReview($data);
    }
}
