<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Test\FormTestCase;

class MobileWishListTest extends FormTestCase
{
    /**
     * @return array
     */
    public function wishListDataProvider(): array
    {
        return [
            [
                [
                    'name' => 'Default',
                    'description' => 'Simple wish list.',
                ],
                [
                    'name' => 'Default',
                    'description' => 'Simple wish list.',
                ],
            ],
            [
                [
                    'name' => 'Default',
                ],
                [
                    'name' => 'Default',
                    'description' => '',
                ],
            ],
            [
                [
                    'name' => 'Default',
                    'description' => null,
                ],
                [
                    'name' => 'Default',
                    'description' => '',
                ],
            ],
            [
                [
                    'name' => 'Default',
                    'description' => '',
                ],
                [
                    'name' => 'Default',
                    'description' => '',
                ],
            ],
        ];
    }

    /**
     * @dataProvider wishListDataProvider
     *
     * @param array $inputData
     * @param array $wishListData
     */
    public function testIsReturningWishListFromCreateData(array $inputData, array $wishListData)
    {
        $input = new MobileWishList();
        $input->setFormFactory($this->getFormFactory());
        $actual = $input->fromCreateWishList($inputData, new Customer());

        $this->assertEquals($wishListData['name'], $actual->getName());
        $this->assertEquals($wishListData['description'], $actual->getDescription());
        $this->assertInstanceOf(Customer::class, $actual->getOwner());
    }

    public function testIsReturningErrorWhenWishListDataIsInvalid()
    {
        $data = [
            'name' => '',
            'description' => 'Simple wish list.',
        ];

        $formFactory = $this->getFormFactory(true);

        $input = new MobileWishList();
        $input->setFormFactory($formFactory);

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::WISH_LIST_INVALID_DATA);

        $input->fromCreateWishList($data, new Customer());
    }

    public function testIsReturningWishListFromEditData()
    {
        $data = [
            'name' => 'Learning',
            'description' => 'Stuff to learn.',
        ];

        $wishListId = 666;

        $input = new MobileWishList();
        $input->setFormFactory($this->getFormFactory());
        $actual = $input->fromEditWishList($wishListId, $data, new Customer());

        $this->assertEquals($data['name'], $actual->getName());
        $this->assertEquals($data['description'], $actual->getDescription());
        $this->assertEquals($wishListId, $actual->getId());
        $this->assertInstanceOf(Customer::class, $actual->getOwner());
    }

    public function testIsReturningErrorWhenTransformingWishListFromInvalidEditData()
    {
        $data = [
            'name' => '',
            'description' => 'Stuff to learn.',
        ];

        $wishListId = 666;

        $formFactory = $this->getFormFactory(true);

        $input = new MobileWishList();
        $input->setFormFactory($formFactory);

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::WISH_LIST_INVALID_DATA);

        $input->fromEditWishList($wishListId, $data, new Customer());
    }

    public function testIsReturningWishListFromId()
    {
        $wishListId = 666;

        $input = new MobileWishList();
        $actual = $input->fromId($wishListId, new Customer());

        $this->assertEquals($wishListId, $actual->getId());
        $this->assertInstanceOf(Customer::class, $actual->getOwner());
    }
}
