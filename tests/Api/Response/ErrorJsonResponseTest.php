<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Response;

class ErrorJsonResponseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function errorProvider()
    {
        return [
            [
                new ErrorJsonResponse('unknown error.'),
                '{"message":"unknown error.","context":null}',
                ErrorJsonResponse::HTTP_INTERNAL_SERVER_ERROR,
            ],
            [
                new ErrorJsonResponse('resource not found.', ErrorJsonResponse::HTTP_NOT_FOUND, ['ctx1', 'ctx2']),
                '{"message":"resource not found.","context":["ctx1","ctx2"]}',
                ErrorJsonResponse::HTTP_NOT_FOUND,
            ],
        ];
    }

    /**
     * @dataProvider errorProvider
     *
     * @param ErrorJsonResponse $errorResponse
     * @param $response
     * @param int $statusCode
     */
    public function testIsCreatingJsonErrorResponse(ErrorJsonResponse $errorResponse, $response, $statusCode)
    {
        $this->assertEquals($errorResponse->getContent(), $response);
        $this->assertEquals($errorResponse->getStatusCode(), $statusCode);
    }
}
