<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use PHPUnit_Framework_TestCase;
use Symfony\Component\Translation\TranslatorInterface;

class MobileConfigurationTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public static function mobileConfigurationDataProvider(): array
    {
        $formName = 'registration';

        $form = [
            'name' => 'registration',
            'fields' => [
                [
                    'enabled' => true,
                    'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\EmailType',
                    'options' => [
                        'label' => 'Email',
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'Ej: carlos@mail.com',
                        ],
                    ],
                    'validation' => null,
                    'name' => 'email',
                ],
                [
                    'enabled' => true,
                    'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType',
                    'options' => [
                        'label' => 'Teléfono Celular (opcional)',
                        'required' => false,
                        'attr' => [
                            'mask' => '99 9999 9999',
                        ],
                    ],
                    'validation' => [
                        'Regex' => '/\d{2} \d{4} \d{4}/',
                    ],
                    'name' => 'sms_phone',
                ],
                [
                    'enabled' => true,
                    'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType',
                    'options' => [
                        'label' => 'Nombre',
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'Ejemplo: Carlos',
                        ],
                    ],
                    'validation' => null,
                    'name' => 'first_name',
                ],
                [
                    'enabled' => true,
                    'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType',
                    'options' => [
                        'label' => 'Apellidos',
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'Ejemplo: López Martínez',
                        ],
                    ],
                    'validation' => null,
                    'name' => 'last_name',
                ],
                [
                    'enabled' => true,
                    'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\RepeatedType',
                    'options' => [
                        'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\PasswordType',
                        'label' => '',
                        'required' => true,
                        'first_options' => [
                            'label' => 'Contraseña',
                            'attr' => [
                                'placeholder' => '●●●●●●●●',
                            ],
                        ],
                        'second_options' => [
                            'label' => 'Confirmar contraseña',
                            'attr' => [
                                'placeholder' => '●●●●●●●●',
                            ],
                        ],
                    ],
                    'validation' => null,
                    'name' => 'password',
                ],
                [
                    'enabled' => true,
                    'type' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\CheckboxType',
                    'options' => [
                        'label' => 'Me gustaría recibir comunicaciones promocionales',
                        'data' => true,
                        'required' => false,
                    ],
                    'name' => 'subscribedToNewsletter',
                    'validation' => null,
                ],
            ],
        ];

        $fixtures = include_once __DIR__ . '/../../fixtures/mobile/MobileConfigurationFixture.php';

        $parseData = $fixtures['fixtureParseData'];

        return [
            [$formName, $form, $parseData],
        ];
    }

    /**
     * @return array
     */
    public function itemReturnConfigurationDataProvider(): array
    {
        $parseData['actions'] = [
            'Otro producto igual',
            'Otro producto diferente',
            'Reembolso en crédito Linio (2 días hábiles)',
            'Reembolso a cuenta (2-5 días hábiles)',
            'Reverso a tarjeta (10-15 días hábiles)',
        ];
        $parseData['reasons'] = [
            'El producto no me queda',
            'El producto no cumple con mis expectativas',
            'Defectuoso/No funciona bien',
            'Empaque exterior dañado',
            'Diferente a la descripción de la página web',
            'No es el producto comprado',
            'Faltan partes o accesorios de este producto',
            'Se excedió la fecha de entrega estimada',
            'Disponible a mejor precio',
            'Pedido accidental',
        ];

        return [[$parseData]];
    }

    /**
     * @dataProvider mobileConfigurationDataProvider
     *
     * @param string $formName
     * @param array $transformedConfiguration
     * @param array $parseData
     */
    public function testIsReturningTransformedConfiguration($formName, array $transformedConfiguration, array $parseData)
    {
        $storeTransformer = new MobileConfiguration();
        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('customer.registration.email')->willReturn('Email');
        $translator->trans('customer.registration.mobile_phone.optional')->willReturn('Teléfono Celular (opcional)');
        $translator->trans('customer.registration.first_name')->willReturn('Nombre');
        $translator->trans('customer.registration.last_name')->willReturn('Apellidos');
        $translator->trans('customer.registration.password.password')->willReturn('Contraseña');
        $translator->trans('customer.registration.password.confirm')->willReturn('Confirmar contraseña');
        $translator->trans('')->willReturn('');
        $translator->trans('customer.registration.subscription.newsletter')->willReturn('Me gustaría recibir comunicaciones promocionales');
        $translator->trans('Ej: carlos@mail.com')->willReturn('Ej: carlos@mail.com');
        $translator->trans('Ejemplo: Carlos')->willReturn('Ejemplo: Carlos');
        $translator->trans('Ejemplo: López Martínez')->willReturn('Ejemplo: López Martínez');

        $storeTransformer->setTranslator($translator->reveal());

        $actual = $storeTransformer->fromDynamicFormConfiguration($parseData, $formName);

        $this->assertSame($transformedConfiguration, $actual);
    }

    /**
     * @dataProvider itemReturnConfigurationDataProvider
     *
     * @param array $parseData
     */
    public function testIsGettingItemReturnConfiguration(array $parseData)
    {
        $storeTransformer = new MobileConfiguration();
        $result = $storeTransformer->fromItemReturnConfiguration($parseData);

        $this->assertNotEmpty($result);
        $this->assertNotEmpty($result['actions']);
        $this->assertNotEmpty($result['reasons']);
        $this->assertNotEmpty($result);
        $this->assertNotEmpty($result['actions']);
        $this->assertNotEmpty($result['reasons']);
        $this->assertArrayHasKey('id', $result['actions'][0]);
        $this->assertArrayHasKey('label', $result['actions'][0]);
        $this->assertInternalType('integer', $result['actions'][0]['id']);
        $this->assertInternalType('string', $result['actions'][0]['label']);
        $this->assertArrayHasKey('id', $result['reasons'][0]);
        $this->assertArrayHasKey('label', $result['reasons'][0]);
        $this->assertInternalType('integer', $result['reasons'][0]['id']);
        $this->assertInternalType('string', $result['reasons'][0]['label']);
    }
}
