<?php

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Newsletter\NewsletterSubscription;
use Linio\Frontend\Newsletter\PromotionalNewsletterSubscription;

class DefaultNewsletterTest extends \PHPUnit_Framework_TestCase
{
    public function TestIsTransformingFromNewsletterSubscription()
    {
        $frequencies = [
            1 => 'daily mail',
            2 => 'two emails a week',
            3 => 'one email every weeks',
            4 => 'one email every two weeks',
            5 => 'one email every month',
        ];

        $newsletterSubscription = new NewsletterSubscription('test@test.com');

        $newsletterSubscription->setFrequency(3);
        $newsletterSubscription->setPhoneNumber('(555) 555-5555');
        $newsletterSubscription->setFrequencies($frequencies);

        $output = new MobileNewsletter();

        $newsletter = $output->fromNewsletterSubscription($newsletterSubscription);

        $this->assertNotEmpty($newsletter['email']);
        $this->assertEquals($newsletterSubscription->getPhoneNumber(), $newsletter['phoneNumber']);
        $this->assertNotEmpty($newsletter['frequencies']);
    }

    public function testIsTransformingFromPromotionalNewsletterSubscription()
    {
        $newsletterSubscription = new PromotionalNewsletterSubscription('test@test.com');

        $newsletterSubscription->setPhoneNumber('(555) 555-5555');
        $newsletterSubscription->setCoupon('TEST123');
        $newsletterSubscription->setSource('Desktop');
        $newsletterSubscription->subscribeToEmail();

        $output = new MobileNewsletter();

        $newsletter = $output->fromPromotionalNewsletterSubscription($newsletterSubscription);

        $this->assertEquals($newsletterSubscription->getCoupon(), $newsletter['coupon']);
        $this->assertEquals($newsletterSubscription->getSource(), $newsletter['source']);
        $this->assertNotEmpty($newsletter['email']);
        $this->assertTrue($newsletter['emailSubscribed']);
    }
}
