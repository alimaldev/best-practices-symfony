<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Bundle;
use Linio\Frontend\Entity\Bundle\Product as BundleProduct;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Entity\Seller;
use Linio\Type\Dictionary;
use Linio\Type\Money;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\TranslatorInterface;

class MobileProductTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function productProvider(): array
    {
        $productOutput = [
            'sku' => 'NU073TB53BKKLMX-953',
            'name' => 'Cepillo-Dental',
            'slug' => 'cepillo-dental',
            'brand' => [
                'id' => 4342,
                'name' => 'Ubisoft',
                'slug' => 'ubisoft',
            ],
            'seller' => [
                'id' => 1,
                'name' => 'Ubisoft',
                'slug' => 'ubisoft',
                'rating' => 4,
                'type' => 'merchant',
                'operationType' => 'bulk_import',
                'import' => true,
            ],
            'description' => 'Cepillo dental',
            'linioPlusLevel' => 1,
            'simples' => [
                [
                    'sku' => 'LE859TB52BKLLMX-954',
                    'stock' => 2,
                    'active' => true,
                    'attributes' => [
                        'short_description' => ['shortDescription'],
                    ],
                    'price' => 75.0,
                    'originalPrice' => 75.0,
                    'hasSpecialPrice' => false,
                    'percentageOff' => 0.0,
                    'savedAmount' => 0.0,
                    'linioPlusLevel' => 0,
                    'hasFreeShipping' => false,
                ],
            ],
            'marketplaceChildren' => [
                [
                    'sku' => 'FI562TB51BKMLMX-955',
                    'seller' => [
                        'id' => 1,
                        'name' => 'Ubisoft',
                        'slug' => 'ubisoft',
                        'rating' => 4,
                        'type' => 'merchant',
                        'operationType' => 'bulk_import',
                        'import' => true,
                    ],
                    'stock' => 2,
                    'active' => true,
                    'attributes' => [
                        'short_description' => ['shortDescription'],
                    ],
                    'price' => 90.0,
                    'originalPrice' => 100.0,
                    'hasSpecialPrice' => true,
                    'percentageOff' => 10.0,
                    'savedAmount' => 10.0,
                    'linioPlusLevel' => 2,
                    'hasFreeShipping' => true,
                    'fulfillmentLabel' => 'Enviado y Vendido por:',
                ],
            ],
            'quantity' => 2,
            'stock' => 4,
            'shippingTimeFrom' => 1,
            'shippingTimeTo' => 3,
            'attributes' => [
                'short_description' => ['shortDescription'],
            ],
            'rating' => [
                'averagePoint' => 5,
                'reviewsQuantity' => 1,
                'voteSummaryTotals' => 1,
                'reviews' => [
                    [
                        'title' => 'Buen producto.',
                        'comment' => 'Mis dientes ahora ya no tienen sarro.',
                        'customerName' => 'Artemio Fragoso',
                        'createdAt' => '2012-06-01T18:42:16-0500',
                        'experienceAverage' => 5,
                    ],
                ],
            ],
            'outOfStock' => false,
            'images' => [
                ['url' => 'https://cepillo-dental-product.jpg', 'isMain' => true],
            ],
            'category' => [
                'id' => 34234,
                'name' => 'Salud y bienestar',
                'slug' => 'salud-y-bienestar',
                'urlKey' => 'salud-y-bienestar',
                'isCurrent' => false,
                'count' => 100,
            ],
            'imported' => false,
            'returnAllowed' => false,
            'postPaymentAllowed' => false,
            'bundles' => [
                [
                    'name' => 'Kit de salud bucal',
                    'discount' => 10.0,
                    'startDate' => '2016-01-01T00:00:00-0600',
                    'endDate' => '2016-02-02T00:00:00-0600',
                    'products' => [
                        [
                            'name' => 'Pasta dental',
                            'originalPrice' => 50,
                            'price' => 50,
                            'selected' => true,
                            'sku' => 'AV637TB41BKWLMX-965',
                            'sprite' => 'sprite',
                        ],
                    ],
                    'savingPrice' => 20,
                    'type' => 'bundle',
                ],
            ],
            'marketplace' => true,
            'variationType' => 'variation',
            'fulfillmentType' => 'sold_and_fulfilled_by_linio',
            'marketplaceParentSku' => 'FI562TB5B8HMLMX-955',
            'unitPrice' => 70.0,
            'price' => 70.0,
            'originalPrice' => 70.0,
            'percentageOff' => 0.0,
            'savedAmount' => 0.0,
            'installmentPrice' => 30.0,
            'installments' => 3,
            'configId' => 'config-id',
            'attributeSet' => 'attributeSet',
            'campaignSlug' => 'salud',
            'deliveredByChristmas' => false,
            'deliveryTime' => 3,
            'deliveryTimeFromFirstSimple' => null,
            'attributeLabels' => ['short_description' => 'Descripción corta'],
            'disclaimersStatus' => false,
            'disclaimersMessages' => [],
            'groupedProducts' => null,
            'inWishLists' => false,
            'oversized' => null,
            'freeStorePickup' => false,
            'fulfillmentLabel' => 'Enviado y Vendido por Linio',
        ];

        $brand = new Brand();
        $brand->setId($productOutput['brand']['id']);
        $brand->setName($productOutput['brand']['name']);
        $brand->setSlug($productOutput['brand']['slug']);

        $seller = new Seller();
        $seller->setSellerId($productOutput['seller']['id']);
        $seller->setName($productOutput['seller']['name']);
        $seller->setSlug($productOutput['seller']['slug']);
        $seller->setRating($productOutput['seller']['rating']);
        $seller->setType($productOutput['seller']['type']);
        $seller->setOperationType($productOutput['seller']['operationType']);

        $simple = new Simple();
        $simple->setSku($productOutput['simples'][0]['sku']);
        $simple->setStock($productOutput['simples'][0]['stock']);
        $simple->setIsActive($productOutput['simples'][0]['active']);
        $simple->addAttribute('short_description', '<ul><li>shortDescription></li></ul>');
        $simple->setPrice(new Money($productOutput['simples'][0]['price']));
        $simple->setOriginalPrice(new Money($productOutput['simples'][0]['originalPrice']));
        $simple->setLinioPlusLevel($productOutput['simples'][0]['linioPlusLevel']);
        $simple->setHasFreeShipping($productOutput['simples'][0]['hasFreeShipping']);

        $marketPlaceChild = new MarketplaceChild();
        $marketPlaceChild->setSku($productOutput['marketplaceChildren'][0]['sku']);
        $marketPlaceChild->setSeller($seller);
        $marketPlaceChild->setStock($productOutput['marketplaceChildren'][0]['stock']);
        $marketPlaceChild->setIsActive($productOutput['marketplaceChildren'][0]['active']);
        $marketPlaceChild->addAttribute('short_description', 'shortDescription');
        $marketPlaceChild->setPrice(new Money($productOutput['marketplaceChildren'][0]['price']));
        $marketPlaceChild->setOriginalPrice(new Money($productOutput['marketplaceChildren'][0]['originalPrice']));
        $marketPlaceChild->setLinioPlusLevel($productOutput['marketplaceChildren'][0]['linioPlusLevel']);
        $marketPlaceChild->setHasFreeShipping($productOutput['marketplaceChildren'][0]['hasFreeShipping']);
        $marketPlaceChild->setFulfillmentType('sold_and_fulfilled');

        $review = new Review();
        $review->setTitle($productOutput['rating']['reviews'][0]['title']);
        $review->setComment($productOutput['rating']['reviews'][0]['comment']);
        $review->setCustomerName($productOutput['rating']['reviews'][0]['customerName']);
        $review->setCreatedAt(new \Datetime($productOutput['rating']['reviews'][0]['createdAt']));
        $review->setExperienceAverage($productOutput['rating']['reviews'][0]['experienceAverage']);
        $reviews = [$review];

        $rating = new Rating();
        $rating->setAveragePoint($productOutput['rating']['averagePoint']);
        $rating->setReviewsQuantity(count($reviews));
        $rating->setReviews($reviews);

        $image = new Image();
        $image->setMain(true);
        $image->setSlug('cepillo-dental');

        $category = new Category();
        $category->setId($productOutput['category']['id']);
        $category->setName($productOutput['category']['name']);
        $category->setSlug($productOutput['category']['slug']);
        $category->setUrlKey($productOutput['category']['urlKey']);
        $category->setIsCurrent($productOutput['category']['isCurrent']);
        $category->setCount($productOutput['category']['count']);

        $bundleProduct = new BundleProduct();
        $bundleProduct->setName($productOutput['bundles'][0]['products'][0]['name']);
        $bundleProduct->setOriginalPrice(new Money($productOutput['bundles'][0]['products'][0]['originalPrice']));
        $bundleProduct->setPrice(new Money($productOutput['bundles'][0]['products'][0]['price']));
        $bundleProduct->setSelected($productOutput['bundles'][0]['products'][0]['selected']);
        $bundleProduct->setSku($productOutput['bundles'][0]['products'][0]['sku']);
        $bundleProduct->setSprite($productOutput['bundles'][0]['products'][0]['sprite']);

        $bundle = new Bundle();
        $bundle->setName($productOutput['bundles'][0]['name']);
        $bundle->setDiscount($productOutput['bundles'][0]['discount']);
        $bundle->setStartDate(new \DateTime($productOutput['bundles'][0]['startDate']));
        $bundle->setEndDate(new \DateTime($productOutput['bundles'][0]['endDate']));
        $bundle->setProducts([$bundleProduct]);
        $bundle->setSavingPrice(new Money($productOutput['bundles'][0]['savingPrice']));
        $bundle->setType($productOutput['bundles'][0]['type']);

        $product = new Product();
        $product->setSku('NU073TB53BKKLMX-953');
        $product->setName($productOutput['name']);
        $product->setSlug('cepillo-dental');
        $product->setBrand($brand);
        $product->setSeller($seller);
        $product->setDescription('Cepillo dental');
        $product->setLinioPlusLevel(1);
        $product->setSimples([$simple]);
        $product->setMarketplaceChildren([$marketPlaceChild]);
        $product->setQuantity(2);
        $product->setStock(4);
        $product->setShippingTimeFrom(1);
        $product->setShippingTimeTo(3);
        $product->setAttributes(new Dictionary(['short_description' => 'shortDescription']));
        $product->setRating($rating);
        $product->setImages([$image]);
        $product->setCategory($category);
        $product->setImported($productOutput['imported']);
        $product->setReturnAllowed($productOutput['returnAllowed']);
        $product->setPostPaymentAllowed($productOutput['postPaymentAllowed']);
        $product->setBundles([$bundle]);
        $product->setVariationType($productOutput['variationType']);
        $product->setFulfillmentType($productOutput['fulfillmentType']);
        $product->setMarketplaceParentSku($productOutput['marketplaceParentSku']);
        $product->setUnitPrice(new Money($productOutput['unitPrice']));
        $product->setPrice(new Money($productOutput['price']));
        $product->setOriginalPrice(new Money($productOutput['originalPrice']));
        $product->setInstallmentPrice(new Money($productOutput['installmentPrice']));
        $product->setInstallments($productOutput['installments']);
        $product->setConfigId($productOutput['configId']);
        $product->setAttributeSet($productOutput['attributeSet']);
        $product->setCampaignSlug($productOutput['campaignSlug']);
        $product->setDeliveryTime($productOutput['deliveryTime']);

        $bundleLess = clone $product;
        $bundleLess->setBundles(null);

        $bundleLessOputput = $productOutput;
        $bundleLessOputput['bundles'] = [];

        return [
            [$product, $productOutput],
            [$bundleLess, $bundleLessOputput],
        ];
    }

    /**
     * @return array
     */
    public function marketplaceChildProvider(): array
    {
        $childOutput = [
            'sku' => 'NU073TB53BKKLMX-953',
            'seller' => [
                'id' => 1,
                'name' => 'Ubisoft',
                'slug' => 'ubisoft',
                'rating' => 4,
                'type' => 'merchant',
                'operationType' => 'bulk_import',
                'import' => true,
            ],
            'stock' => 3,
            'active' => true,
            'attributes' => [
                'short_description' => ['shortDescription'],
            ],
            'price' => 100.0,
            'originalPrice' => 100.0,
            'hasSpecialPrice' => false,
            'percentageOff' => 0.0,
            'savedAmount' => 0.0,
            'linioPlusLevel' => 2,
            'hasFreeShipping' => false,
        ];

        $seller = new Seller();
        $seller->setSellerId($childOutput['seller']['id']);
        $seller->setName($childOutput['seller']['name']);
        $seller->setSlug($childOutput['seller']['slug']);
        $seller->setRating($childOutput['seller']['rating']);
        $seller->setType($childOutput['seller']['type']);
        $seller->setOperationType($childOutput['seller']['operationType']);

        $child = new MarketplaceChild();
        $child->setSku($childOutput['sku']);
        $child->setSeller($seller);
        $child->setStock($childOutput['stock']);
        $child->setIsActive($childOutput['active']);
        $child->addAttribute('short_description', '<ul><li>shortDescription></li></ul>');
        $child->setPrice(new Money($childOutput['price']));
        $child->setOriginalPrice(new Money($childOutput['originalPrice']));
        $child->setLinioPlusLevel($childOutput['linioPlusLevel']);
        $child->setHasFreeShipping($childOutput['hasFreeShipping']);

        return [
            [$child, $childOutput],
        ];
    }

    /**
     * @return array
     */
    public function simpleProvider(): array
    {
        $simpleOutput = [
            'sku' => 'NU073TB53BKKLMX-953',
            'stock' => 4,
            'active' => false,
            'attributes' => [
                'short_description' => ['shortDescription'],
            ],
            'price' => 95.0,
            'originalPrice' => 100.0,
            'hasSpecialPrice' => true,
            'percentageOff' => 5.0,
            'savedAmount' => 5.0,
            'linioPlusLevel' => 0,
            'hasFreeShipping' => false,
        ];

        $simple = new Simple();
        $simple->setSku($simpleOutput['sku']);
        $simple->setStock($simpleOutput['stock']);
        $simple->setIsActive($simpleOutput['active']);
        $simple->addAttribute('short_description', '<ul><li>shortDescription></li></ul>');
        $simple->setPrice(new Money($simpleOutput['price']));
        $simple->setOriginalPrice(new Money($simpleOutput['originalPrice']));
        $simple->setLinioPlusLevel($simpleOutput['linioPlusLevel']);
        $simple->setHasFreeShipping($simpleOutput['hasFreeShipping']);

        return [
            [$simple, $simpleOutput],
        ];
    }

    /**
     * @dataProvider productProvider
     *
     * @param Product $product
     * @param array $productOutput
     */
    public function testIsOutputtingFromProduct(Product $product, array $productOutput)
    {
        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('https');

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $attributeCacheService = $this->prophesize(CacheService::class);
        $configurationCacheService = $this->prophesize(CacheService::class);
        $disclaimerCategories = [
            [
                'categories' => [
                    8308,
                    5928,
                ],
                'message' => 'Message Disclaimer',
            ],
            [
                'categories' => [
                    8308,
                    1424,
                ],
                'message' => 'Disclaimer 2',
            ],
        ];

        $configurationCacheService->get('disclaimer_categories')->willReturn($disclaimerCategories);

        $attributeCacheService->get('attributes')->willReturn(['short_description' => 'Descripción corta']);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('product_details.' . $product->getFulfillmentType())->shouldBeCalled()->willReturn('Enviado y Vendido por Linio');
        $translator->trans('product_details.sold_and_fulfilled')->shouldBeCalled()->willReturn('Enviado y Vendido por:');

        $output = new MobileProduct();
        $output->setAttributeCacheService($attributeCacheService->reveal());
        $output->setRequestStack($requestStack->reveal());
        $output->setFilteredProductDescriptionOutput(new FilteredProductDescription());
        $output->setSellerOutput(new MobileSeller());
        $output->setTranslator($translator->reveal());
        $output->setConfigurationCacheService($configurationCacheService->reveal());
        $actual = $output->fromProduct($product);

        $this->assertEquals($productOutput, $actual);
    }

    /**
     * @return array
     */
    public function productFulfillmentProvider(): array
    {
        $merchantSeller = new Seller();
        $merchantSeller->setType(Seller::SELLER_TYPE_MERCHANT);

        $supplierSeller = new Seller();
        $supplierSeller->setType(Seller::SELLER_TYPE_SUPPLIER);

        $productSoldAndShippedByLinio = new Product();
        $productSoldAndShippedByLinio->setFulfillmentType('sold_and_fulfilled_by_linio');
        $productSoldAndShippedByLinio->setSeller($merchantSeller);
        $productSoldAndShippedByLinio->setDescription('description');
        $productSoldAndShippedByLinio->setCategory(new Category());

        $productSoldAndShippedBySeller = new Product();
        $productSoldAndShippedBySeller->setFulfillmentType('sold_and_fulfilled');
        $productSoldAndShippedBySeller->setSeller($merchantSeller);
        $productSoldAndShippedBySeller->setDescription('description');
        $productSoldAndShippedBySeller->setCategory(new Category());

        $productSoldBySellerAndShippedByLinio = new Product();
        $productSoldBySellerAndShippedByLinio->setFulfillmentType('sold');
        $productSoldBySellerAndShippedByLinio->setSeller($merchantSeller);
        $productSoldBySellerAndShippedByLinio->setDescription('description');
        $productSoldBySellerAndShippedByLinio->setCategory(new Category());

        $productWithoutFulfillment = new Product();
        $productWithoutFulfillment->setFulfillmentType(null);
        $productWithoutFulfillment->setSeller($merchantSeller);
        $productWithoutFulfillment->setDescription('description');
        $productWithoutFulfillment->setCategory(new Category());

        $productWithSellerTypeSupplier = new Product();
        $productWithSellerTypeSupplier->setFulfillmentType(null);
        $productWithSellerTypeSupplier->setSeller($supplierSeller);
        $productWithSellerTypeSupplier->setDescription('description');
        $productWithSellerTypeSupplier->setCategory(new Category());

        return [
            [$productSoldAndShippedByLinio, 'product_details.' . $productSoldAndShippedByLinio->getFulfillmentType(), 'Enviado y Vendido por Linio'],
            [$productSoldAndShippedBySeller, 'product_details.' . $productSoldAndShippedBySeller->getFulfillmentType(), 'Enviado y Vendido por:'],
            [$productSoldBySellerAndShippedByLinio, 'product_details.' . $productSoldBySellerAndShippedByLinio->getFulfillmentType(), 'Enviado por Linio y Vendido por:'],
            [$productWithoutFulfillment, 'product_details.sold', 'Enviado por Linio y Vendido por:'],
            [$productWithSellerTypeSupplier, 'product_details.sold_and_fulfilled_by_linio', 'Enviado y Vendido por Linio'],
        ];
    }

    /**
     * @dataProvider productFulfillmentProvider
     *
     * @param Product $product
     * @param string $fulfillmentTranslationKey
     * @param string $fulfillmentLabel
     */
    public function testIsOutputtingProductFulfillment(Product $product, string $fulfillmentTranslationKey, string $fulfillmentLabel)
    {
        $sellerOutput = $this->prophesize(SellerOutput::class);
        $sellerOutput->fromSeller($product->getSeller())->shouldBeCalled()->willReturn(['name' => 'tecdepot']);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans($fulfillmentTranslationKey)->shouldBeCalled()->willReturn($fulfillmentLabel);

        $filteredProductDescriptionOutput = $this->prophesize(FilteredProductDescriptionOutput::class);
        $filteredProductDescriptionOutput->filteredProductDescription($product->getDescription())->shouldBeCalled()->willReturn($product->getDescription());

        $attributeCacheService = $this->prophesize(CacheService::class);
        $attributeCacheService->get('attributes')->willReturn(['attribute' => 'Atributo']);

        $configurationCacheService = $this->prophesize(CacheService::class);
        $disclaimerCategories = [
            [
                'categories' => [
                    8308,
                    5928,
                ],
                'message' => 'Message Disclaimer',
            ],
            [
                'categories' => [
                    8308,
                    1424,
                ],
                'message' => 'Disclaimer 2',
            ],
        ];

        $configurationCacheService->get('disclaimer_categories')->willReturn($disclaimerCategories);

        $output = new MobileProduct();
        $output->setSellerOutput($sellerOutput->reveal());
        $output->setTranslator($translator->reveal());
        $output->setFilteredProductDescriptionOutput($filteredProductDescriptionOutput->reveal());
        $output->setAttributeCacheService($attributeCacheService->reveal());
        $output->setConfigurationCacheService($configurationCacheService->reveal());

        $actual = $output->fromProduct($product);

        $this->assertEquals($fulfillmentLabel, $actual['fulfillmentLabel']);
    }

    /**
     * @dataProvider marketplaceChildProvider
     *
     * @param MarketplaceChild $child
     * @param array $childOutput
     */
    public function testIsOutputtingFromMarketplaceChild(MarketplaceChild $child, array $childOutput)
    {
        $output = new MobileProduct();
        $output->setFilteredProductDescriptionOutput(new FilteredProductDescription());
        $output->setSellerOutput(new MobileSeller());
        $actual = $output->fromFilteredMarketplaceChild($child);

        $this->assertEquals($actual, $childOutput);
    }

    /**
     * @dataProvider simpleProvider
     *
     * @param Simple $simple
     * @param array $simpleOutput
     */
    public function testIsOutputtingFromSimple(Simple $simple, array $simpleOutput)
    {
        $output = new MobileProduct();
        $output->setFilteredProductDescriptionOutput(new FilteredProductDescription());
        $output->setSellerOutput(new MobileSeller());
        $actual = $output->fromFilteredSimple($simple);

        $this->assertEquals($actual, $simpleOutput);
    }
}
