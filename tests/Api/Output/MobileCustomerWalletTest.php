<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Customer\Wallet\Points;
use Linio\Frontend\Entity\Customer\Wallet\Transaction;
use Linio\Frontend\Entity\Customer\Wallet\TransactionCollection;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;

class MobileCustomerWalletTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function walletProvider(): array
    {
        $walletOutput = [
            'account' => '20394837291',
            'accruedPoints' => 9.0,
            'usedPoints' => 1.0,
            'expiredPoints' => 0,
            'balance' => 1.0,
            'transactions' => [
                [
                    'createdAt' => '2016-10-25T22:10:10-0500',
                    'charge' => 0,
                    'payment' => 100,
                    'validUntil' => '2017-10-19T00:00:00-0500',
                    'status' => 'active',
                    'balance' => 900,
                ],
                [
                    'createdAt' => '2016-10-20T22:10:10-0500',
                    'charge' => 1000,
                    'payment' => 0,
                    'validUntil' => null,
                    'status' => 'active',
                    'balance' => 1000,
                ],
            ],
        ];

        $transaction1 = new Transaction(
            new DateTime($walletOutput['transactions'][0]['createdAt']),
            new Points($walletOutput['transactions'][0]['charge']),
            new Points($walletOutput['transactions'][0]['payment']),
            $walletOutput['transactions'][0]['status'],
            new Points($walletOutput['transactions'][0]['balance']),
            new DateTime($walletOutput['transactions'][0]['validUntil'])
        );

        $transaction2 = new Transaction(
            new DateTime($walletOutput['transactions'][1]['createdAt']),
            new Points($walletOutput['transactions'][1]['charge']),
            new Points($walletOutput['transactions'][1]['payment']),
            $walletOutput['transactions'][1]['status'],
            new Points($walletOutput['transactions'][1]['balance'])
        );

        $wallet = new Wallet();
        $wallet->setAccount($walletOutput['account']);
        $wallet->setAccruedPoints(new Points($walletOutput['accruedPoints']));
        $wallet->setUsedPoints(new Points($walletOutput['usedPoints']));
        $wallet->setExpiredPoints(new Points($walletOutput['expiredPoints']));
        $wallet->setBalance(new Points($walletOutput['balance']));
        $wallet->addTransaction($transaction1);
        $wallet->addTransaction($transaction2);

        $walletWithoutTransactionsOutput = $walletOutput;
        $walletWithoutTransactionsOutput['transactions'] = [];

        $walletWithoutTransactions = clone $wallet;
        $walletWithoutTransactions->setTransactions(new TransactionCollection());

        return [
            [$wallet, $walletOutput],
            [$walletWithoutTransactions, $walletWithoutTransactionsOutput],
        ];
    }

    /**
     * @dataProvider walletProvider
     *
     * @param Wallet $wallet
     * @param array $walletOutput
     */
    public function testIsOutputtingFromCustomerWallet(Wallet $wallet, array $walletOutput)
    {
        $output = new MobileCustomerWallet();
        $actual = $output->fromCustomerWallet($wallet);

        $this->assertEquals($walletOutput, $actual);
    }
}
