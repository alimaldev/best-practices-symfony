<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Test\WishListOutputFixture;

class MobileWishListTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromWishList()
    {
        $wishlistFixture = new WishListOutputFixture();

        $wishListProductOutput = $this->prophesize(WishListProductOutput::class);
        $wishListProductOutput->fromProduct(array_values($wishlistFixture->getWishList()->getProducts())[0])
            ->willReturn(array_values($wishlistFixture->getWishListArray()['products'])[0]);

        $output = new MobileWishList();
        $output->setWishListProductOutput($wishListProductOutput->reveal());
        $actual = $output->fromWishList($wishlistFixture->getWishList());

        $this->assertEquals($wishlistFixture->getWishListArray(), $actual);
    }

    public function testIsOutputtingFromWishListWishOutProducts()
    {
        $wishlistFixture = new WishListOutputFixture();

        $output = new MobileWishList();
        $actual = $output->fromWishList($wishlistFixture->getWishListWithoutProducts());

        $this->assertEquals($wishlistFixture->getWishListWithoutProductsArray(), $actual);
    }

    public function testIsOutputtingFromWishLists()
    {
        $wishlistFixture = new WishListOutputFixture();

        $wishListProductOutput = $this->prophesize(WishListProductOutput::class);
        $wishListProductOutput->fromProduct(array_values($wishlistFixture->getWishList()->getProducts())[0])
            ->willReturn(array_values($wishlistFixture->getWishListArray()['products'])[0]);

        $output = new MobileWishList();
        $output->setWishListProductOutput($wishListProductOutput->reveal());
        $actual = $output->fromWishLists($wishlistFixture->getWishLists());

        $this->assertEquals([$wishlistFixture->getWishListArray()], $actual);
    }
}
