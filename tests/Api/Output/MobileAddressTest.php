<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Customer\Order\Shipping\Address as ShippingAddress;
use Linio\Frontend\Location\Address;

class MobileAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromAddress()
    {
        $expected = [
            'id' => 390,
            'title' => 'Sr',
            'type' => 'type',
            'firstName' => 'Anka',
            'lastName' => 'Garcia',
            'prefix' => 'prefix',
            'line1' => 'el Tejar centro comercial hermano miguel pasillo 14c',
            'line2' => 'wre',
            'betweenStreet1' => 'betweenStreet1',
            'betweenStreet2' => 'betweenStreet2',
            'streetNumber' => 'wre',
            'apartment' => 'wre',
            'lot' => 'lot',
            'neighborhood' => 'neighborhood',
            'department' => 'department',
            'municipality' => 'MERA',
            'urbanization' => 'urbanization',
            'city' => 'city',
            'cityId' => 8,
            'cityName' => 'cityName',
            'region' => 'PASTAZA',
            'regionId' => 6,
            'regionCode' => 'regionCode',
            'regionName' => 'regionName',
            'postcode' => 'postcode',
            'additionalInformation' => 'additionalInformation',
            'phone' => 'phone',
            'mobilePhone' => 'mobilePhone',
            'countryId' => 63,
            'countryCode' => 'EC',
            'countryName' => 'Ecuador',
            'taxIdentificationNumber' => '123421PO432-2',
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => 'maternalName',
            'invoiceType' => 'invoiceType',
            'country' => 'Ecuador',
            'company' => 'company',
        ];

        $address = new Address();
        $address->setId($expected['id']);
        $address->setTitle($expected['title']);
        $address->setType($expected['type']);
        $address->setFirstName($expected['firstName']);
        $address->setLastName($expected['lastName']);
        $address->setPrefix($expected['prefix']);
        $address->setLine1($expected['line1']);
        $address->setLine2($expected['line2']);
        $address->setBetweenStreet1($expected['betweenStreet1']);
        $address->setBetweenStreet2($expected['betweenStreet2']);
        $address->setStreetNumber($expected['streetNumber']);
        $address->setApartment($expected['apartment']);
        $address->setLot($expected['lot']);
        $address->setNeighborhood($expected['neighborhood']);
        $address->setDepartment($expected['department']);
        $address->setMunicipality($expected['municipality']);
        $address->setUrbanization($expected['urbanization']);
        $address->setCity($expected['city']);
        $address->setCityId($expected['cityId']);
        $address->setCityName($expected['cityName']);
        $address->setRegion($expected['region']);
        $address->setRegionId($expected['regionId']);
        $address->setRegionCode($expected['regionCode']);
        $address->setRegionName($expected['regionName']);
        $address->setPostcode($expected['postcode']);
        $address->setAdditionalInformation($expected['additionalInformation']);
        $address->setPhone($expected['phone']);
        $address->setMobilePhone($expected['mobilePhone']);
        $address->setCountryId($expected['countryId']);
        $address->setCountryCode($expected['countryCode']);
        $address->setCountryName($expected['countryName']);
        $address->setTaxIdentificationNumber($expected['taxIdentificationNumber']);
        $address->setDefaultBilling($expected['defaultBilling']);
        $address->setDefaultShipping($expected['defaultShipping']);
        $address->setMaternalName($expected['maternalName']);
        $address->setInvoiceType($expected['invoiceType']);
        $address->setCompany($expected['company']);

        $output = new MobileAddress();
        $actual = $output->fromAddress($address);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider addressesProvider
     *
     * @param Address $address1
     * @param Address $address2
     */
    public function testIsOutputtingFromAddresses(Address $address1, Address $address2)
    {
        $output = new MobileAddress();

        $actual = $output->fromAddresses([$address1, $address2]);

        $this->assertArrayHasKey(1, $actual);
        $this->assertArrayHasKey(2, $actual);
    }

    /**
     * @dataProvider shippingAddressProvider
     *
     * @param ShippingAddress $shippingAddress
     * @param array $shippingAddressOutput
     */
    public function testIsOutputtingFromShippingAddress(ShippingAddress $shippingAddress, array $shippingAddressOutput)
    {
        $output = new MobileAddress();
        $actual = $output->fromShippingAddress($shippingAddress);
        $this->assertEquals($shippingAddressOutput, $actual);
    }

    /**
     * @return array
     */
    public function addressesProvider(): array
    {
        $address1 = new Address();
        $address1->setId(1);
        $address1->setFirstName('firstName1');
        $address1->setLastName('lastName1');
        $address1->setLine1('line11');
        $address1->setLine2('line21');
        $address1->setStreetNumber('streetNumber1');
        $address1->setCity('city1');
        $address1->setMunicipality('municipality1');
        $address1->setNeighborhood('neighborhood1');
        $address1->setRegion('region1');
        $address1->setRegionId(1);
        $address1->setCountryId(2);
        $address1->setPhone('phone1');
        $address1->setPrefix('prefix1');
        $address1->setMobilePhone('mobilePhone1');
        $address1->setPostcode('postcode1');
        $address1->setDefaultBilling(true);
        $address1->setDefaultShipping(false);
        $address1->setApartment('apartment1');
        $address1->setAdditionalInformation('additionalInformation1');
        $address1->setBetweenStreet1('betweenStreet11');
        $address1->setBetweenStreet2('betweenStreet21');
        $address1->setTaxIdentificationNumber('taxIdentificationNumber1');
        $address1->setInvoiceType('invoiceType1');
        $address1->setLot('lot1');
        $address1->setUrbanization('urbanization1');
        $address1->setType('directionType1');
        $address1->setDepartment('department1');

        $address2 = new Address();
        $address2->setId(2);
        $address2->setFirstName('firstName2');
        $address2->setLastName('lastName2');
        $address2->setLine1('line12');
        $address2->setLine2('line22');
        $address2->setStreetNumber('streetNumber2');
        $address2->setCity('city2');
        $address2->setMunicipality('municipality2');
        $address2->setNeighborhood('neighborhood2');
        $address2->setRegion('region2');
        $address2->setRegionId(1);
        $address2->setCountryId(2);
        $address2->setPhone('phone2');
        $address2->setPrefix('prefix2');
        $address2->setMobilePhone('mobilePhone2');
        $address2->setPostcode('postcode2');
        $address2->setDefaultBilling(false);
        $address2->setDefaultShipping(true);
        $address2->setApartment('apartment2');
        $address2->setAdditionalInformation('additionalInformation2');
        $address2->setBetweenStreet1('betweenStreet12');
        $address2->setBetweenStreet2('betweenStreet22');
        $address2->setTaxIdentificationNumber('taxIdentificationNumber2');
        $address2->setInvoiceType('invoiceType2');
        $address2->setLot('lot2');
        $address2->setUrbanization('urbanization2');
        $address2->setType('directionType2');
        $address2->setDepartment('department2');

        return [
            [$address1, $address2],
        ];
    }

    /**
     * @return array
     */
    public function shippingAddressProvider(): array
    {
        $shippingAddressOutput = [
            'id' => 1,
            'title' => 'title',
            'type' => 'directionType',
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'prefix' => 'prefix',
            'line1' => 'line1',
            'line2' => 'line2',
            'betweenStreet1' => 'betweenStreet1',
            'betweenStreet2' => 'betweenStreet2',
            'streetNumber' => 'streetNumber',
            'apartment' => 'apartment',
            'lot' => 'lot',
            'neighborhood' => 'neighborhood',
            'department' => 'department',
            'municipality' => 'municipality',
            'urbanization' => 'urbanization',
            'city' => 'city',
            'cityId' => 1,
            'cityName' => 'cityName',
            'region' => 'region',
            'regionId' => 1,
            'regionCode' => 'regionCode',
            'regionName' => 'regionName',
            'postcode' => 'postcode',
            'additionalInformation' => 'additionalInformation',
            'phone' => 'phone',
            'mobilePhone' => 'mobilePhone',
            'countryId' => 2,
            'countryCode' => 'countryCode',
            'countryName' => 'countryName',
            'taxIdentificationNumber' => 'taxIdentificationNumber',
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => 'maternalName',
            'invoiceType' => 'invoiceType',
            'country' => 'countryName',
            'alternativeRecipientName' => 'alternativeRecipientName',
            'alternativeRecipientId' => 'alternativeRecipientId',
            'directionPrefix' => 'directionPrefix',
            'nationalRegistrationNumber' => 'nationalRegistrationNumber',
            'pickupStore' => null,
            'company' => 'company',
        ];

        $shippingAddress = new ShippingAddress();
        $shippingAddress->setId($shippingAddressOutput['id']);
        $shippingAddress->setTitle($shippingAddressOutput['title']);
        $shippingAddress->setFirstName($shippingAddressOutput['firstName']);
        $shippingAddress->setLastName($shippingAddressOutput['lastName']);
        $shippingAddress->setLine1($shippingAddressOutput['line1']);
        $shippingAddress->setLine2($shippingAddressOutput['line2']);
        $shippingAddress->setStreetNumber($shippingAddressOutput['streetNumber']);
        $shippingAddress->setCity($shippingAddressOutput['city']);
        $shippingAddress->setCityId($shippingAddressOutput['cityId']);
        $shippingAddress->setCityName($shippingAddressOutput['cityName']);
        $shippingAddress->setMunicipality($shippingAddressOutput['municipality']);
        $shippingAddress->setNeighborhood($shippingAddressOutput['neighborhood']);
        $shippingAddress->setRegion($shippingAddressOutput['region']);
        $shippingAddress->setRegionId($shippingAddressOutput['regionId']);
        $shippingAddress->setRegionCode($shippingAddressOutput['regionCode']);
        $shippingAddress->setRegionName($shippingAddressOutput['regionName']);
        $shippingAddress->setCountryId($shippingAddressOutput['countryId']);
        $shippingAddress->setCountryName($shippingAddressOutput['countryName']);
        $shippingAddress->setPhone($shippingAddressOutput['phone']);
        $shippingAddress->setPrefix($shippingAddressOutput['prefix']);
        $shippingAddress->setMobilePhone($shippingAddressOutput['mobilePhone']);
        $shippingAddress->setPostcode($shippingAddressOutput['postcode']);
        $shippingAddress->setDefaultBilling($shippingAddressOutput['defaultBilling']);
        $shippingAddress->setDefaultShipping($shippingAddressOutput['defaultShipping']);
        $shippingAddress->setApartment($shippingAddressOutput['apartment']);
        $shippingAddress->setAdditionalInformation($shippingAddressOutput['additionalInformation']);
        $shippingAddress->setBetweenStreet1($shippingAddressOutput['betweenStreet1']);
        $shippingAddress->setBetweenStreet2($shippingAddressOutput['betweenStreet2']);
        $shippingAddress->setTaxIdentificationNumber($shippingAddressOutput['taxIdentificationNumber']);
        $shippingAddress->setInvoiceType($shippingAddressOutput['invoiceType']);
        $shippingAddress->setLot($shippingAddressOutput['lot']);
        $shippingAddress->setUrbanization($shippingAddressOutput['urbanization']);
        $shippingAddress->setType($shippingAddressOutput['type']);
        $shippingAddress->setDepartment($shippingAddressOutput['department']);
        $shippingAddress->setCountry($shippingAddressOutput['countryName']);
        $shippingAddress->setCountryCode($shippingAddressOutput['countryCode']);
        $shippingAddress->setAlternativeRecipientName($shippingAddressOutput['alternativeRecipientName']);
        $shippingAddress->setAlternativeRecipientId($shippingAddressOutput['alternativeRecipientId']);
        $shippingAddress->setDirectionPrefix($shippingAddressOutput['directionPrefix']);
        $shippingAddress->setMaternalName($shippingAddressOutput['maternalName']);
        $shippingAddress->setNationalRegistrationNumber($shippingAddressOutput['nationalRegistrationNumber']);
        $shippingAddress->setCompany($shippingAddressOutput['company']);

        return [
            [$shippingAddress, $shippingAddressOutput],
        ];
    }
}
