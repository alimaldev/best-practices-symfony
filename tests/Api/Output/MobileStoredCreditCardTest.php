<?php

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Customer\CreditCard;

class MobileStoredCreditCardTest extends \PHPUnit_Framework_TestCase
{
    public function testIsReturningTransformedCreditCardData()
    {
        $creditCard = new CreditCard();

        $creditCard->setId(25);
        $creditCard->setBrand(CreditCard::BRAND_MASTERCARD);
        $creditCard->setCardholderName('John Snow');
        $creditCard->setCardNumber('9999 9999 9999 9999');
        $creditCard->setExpirationDate(DateTime::createFromFormat('Y-m-d', '2020-05-01'));
        $creditCard->setPaymentMethod('CreditCard');

        $output = new MobileStoredCreditCard();

        $creditCardData = $output->fromCreditCard($creditCard);

        $this->assertEquals($creditCard->getId(), $creditCardData['id']);
        $this->assertEquals($creditCard->getBrand(), $creditCardData['brand']);
        $this->assertEquals($creditCard->getCardholderName(), $creditCardData['cardHolderName']);
        $this->assertEquals('John', $creditCardData['cardHolderFirstName']);
        $this->assertEquals('Snow', $creditCardData['cardHolderLastName']);
        $this->assertEquals($creditCard->getFirstDigits(), $creditCardData['firstDigits']);
        $this->assertEquals($creditCard->getLastDigits(), $creditCardData['lastDigits']);
        $this->assertEquals($creditCard->getExpirationDate()->format('Y-m-d'), $creditCardData['expirationDate']);
        $this->assertEquals($creditCard->getPaymentMethod(), $creditCardData['paymentMethod']);
    }

    public function testIsReturningTransformedCreditCardsData()
    {
        $creditCard1 = new CreditCard();

        $creditCard1->setId(25);
        $creditCard1->setBrand(CreditCard::BRAND_MASTERCARD);
        $creditCard1->setCardholderName('John Snow');
        $creditCard1->setCardNumber('9999 9999 9999 9999');
        $creditCard1->setExpirationDate(DateTime::createFromFormat('Y-m-d', '2020-05-01'));
        $creditCard1->setPaymentMethod('CreditCard');

        $creditCard2 = new CreditCard();

        $creditCard2->setId(26);
        $creditCard2->setBrand(CreditCard::BRAND_VISA);
        $creditCard2->setCardholderName('Ned Stark de la santa cruz');
        $creditCard2->setCardNumber('9999 8888 7777 6666');
        $creditCard2->setExpirationDate(DateTime::createFromFormat('Y-m-d', '2002-04-03'));
        $creditCard2->setPaymentMethod('CreditCard');

        $creditCards = [$creditCard1, $creditCard2];

        $output = new MobileStoredCreditCard();

        $creditCardsData = $output->fromCreditCards($creditCards);

        $this->assertEquals($creditCard1->getId(), $creditCardsData[0]['id']);
        $this->assertEquals($creditCard1->getBrand(), $creditCardsData[0]['brand']);
        $this->assertEquals($creditCard1->getCardholderName(), $creditCardsData[0]['cardHolderName']);
        $this->assertEquals('John', $creditCardsData[0]['cardHolderFirstName']);
        $this->assertEquals('Snow', $creditCardsData[0]['cardHolderLastName']);
        $this->assertEquals($creditCard1->getFirstDigits(), $creditCardsData[0]['firstDigits']);
        $this->assertEquals($creditCard1->getLastDigits(), $creditCardsData[0]['lastDigits']);
        $this->assertEquals($creditCard1->getExpirationDate()->format('Y-m-d'), $creditCardsData[0]['expirationDate']);
        $this->assertEquals($creditCard1->getPaymentMethod(), $creditCardsData[0]['paymentMethod']);

        $this->assertEquals($creditCard2->getId(), $creditCardsData[1]['id']);
        $this->assertEquals($creditCard2->getBrand(), $creditCardsData[1]['brand']);
        $this->assertEquals($creditCard2->getCardholderName(), $creditCardsData[1]['cardHolderName']);
        $this->assertEquals('Ned', $creditCardsData[1]['cardHolderFirstName']);
        $this->assertEquals('Stark de la santa cruz', $creditCardsData[1]['cardHolderLastName']);
        $this->assertEquals($creditCard2->getFirstDigits(), $creditCardsData[1]['firstDigits']);
        $this->assertEquals($creditCard2->getLastDigits(), $creditCardsData[1]['lastDigits']);
        $this->assertEquals($creditCard2->getExpirationDate()->format('Y-m-d'), $creditCardsData[1]['expirationDate']);
        $this->assertEquals($creditCard2->getPaymentMethod(), $creditCardsData[1]['paymentMethod']);
    }
}
