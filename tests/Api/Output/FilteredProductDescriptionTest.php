<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

// @TODO: Add more tests.
class FilteredProductDescriptionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @TODO: Add more samples to cover different cases.
     *
     * @return array
     */
    public function shortDescriptionProvider(): array
    {
        $shortDescription1 = '<ul>
<li>Tamaño de pantalla: 4 pulgadas.</li>
<li>Resolución de pantalla: 2592 х 1944.</li>
<li>Sistema operativo: Androidv4.4.2 KitKat.</li>
<li>Procesador y velocidad de procesamiento: Quad-core a 1.5 GHz Cortex-A53 y Quad-core a 2.1 GHz Cortex-A57.</li>
<li>Memoria de almacenamiento: 4 GB.</li>
<li>Memoria RAM: 512 MB.</li>
<li>Resolución cámara trasera: 5 MP.</li>
<li>Conectividad: Bluetooth, Wi-Fi, Micro USB.</li>
</ul>';

        $expected1 = [
            'Tamaño de pantalla: 4 pulgadas.',
            'Resolución de pantalla: 2592 х 1944.',
            'Sistema operativo: Androidv4.4.2 KitKat.',
            'Procesador y velocidad de procesamiento: Quad-core a 1.5 GHz Cortex-A53 y Quad-core a 2.1 GHz Cortex-A57.',
            'Memoria de almacenamiento: 4 GB.',
            'Memoria RAM: 512 MB.',
            'Resolución cámara trasera: 5 MP.',
            'Conectividad: Bluetooth, Wi-Fi, Micro USB.',
        ];

        $shortDescription2 = 'Tamaño de pantalla: 4 pulgadas.';

        $shortDescription3 = '<ul style="margin: 1em 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; list-style-position: inside; box-sizing: content-box; font-family: Arial, Helvetica, \'Nimbus Sans L\', sans-serif; font-size: 13px; line-height: normal;"><li style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; box-sizing: content-box;"><span><span lang="es">Cargador inalámbrico Para todo el equipo estándar Qi</span></span></li><li style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; box-sizing: content-box;"><span><span lang="es">Fácil la carga sin problemas, ni cables.&nbsp;</span></span></li><li style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; box-sizing: content-box;"><span><span lang="es">Mejore su vida móvil inteligente .</span></span></li><li style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; box-sizing: content-box;"><span><span lang="es">Área activa grande para facilitar la alineación .</span></span></li><li style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; box-sizing: content-box;"><span><span lang="es">Qi compatible.&nbsp;</span></span></li><li style="margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; box-sizing: content-box;"><span><span lang="es"><span>La luz indicadora LED</span></span></span></li></ul>';
        $expected3 = [
            'Cargador inalámbrico Para todo el equipo estándar Qi',
            'Fácil la carga sin problemas, ni cables.',
            'Mejore su vida móvil inteligente .',
            'Área activa grande para facilitar la alineación .',
            'Qi compatible.',
            'La luz indicadora LED',
        ];

        $shortDescription4 = '<b>Principales Características:</b><ul><li><span>- Acero Shark a series</span></li><li><span>- Diseñado por el renombrado diseñador japonés "Mr. HitoImachi "</span></li><li><span>- Movimiento DUAL con el último avance Tecnología cuarzo Japón</span></li><li><span>- Fecha LED, día , tiempo de visualización y función de alarma</span></li><li><span>- Resistente al agua hasta 3 ATM</span></li><li><span>- Garantía&nbsp;</span></li></ul><br /><b>Especificación (aproximado ) :</b><ul><li><span>- Diámetro: 5.0cm ( 1.96 pulgadas ) aprox .</span></li><li><span>- Grueso del caso : el 1.5cm ( 0.59 pulgadas ) aprox .</span></li><li><span>- Longitud de la banda : 19cm ( 7.4 pulgadas ) de aprox.</span></li><li><span>- Material de la banda : Cuero</span></li><li><span>- Banda ajustable: Sí</span></li></ul><br />';
        $expected4 = [
            'Principales Características:',
            '- Acero Shark a series',
            '- Diseñado por el renombrado diseñador japonés "Mr. HitoImachi "',
            '- Movimiento DUAL con el último avance Tecnología cuarzo Japón',
            '- Fecha LED, día , tiempo de visualización y función de alarma',
            '- Resistente al agua hasta 3 ATM',
            '- Garantía',
            'Especificación (aproximado ) :',
            '- Diámetro: 5.0cm ( 1.96 pulgadas ) aprox .',
            '- Grueso del caso : el 1.5cm ( 0.59 pulgadas ) aprox .',
            '- Longitud de la banda : 19cm ( 7.4 pulgadas ) de aprox.',
            '- Material de la banda : Cuero',
            '- Banda ajustable: Sí',
        ];

        return [
            ['<ul><li>shortDescription></li></ul>', ['shortDescription']],
            [$shortDescription1, $expected1],
            ['   ' . $shortDescription1, $expected1],
            [$shortDescription2, [$shortDescription2]],
            [$shortDescription3, $expected3],
            [$shortDescription4, $expected4],
        ];
    }

    /**
     * @dataProvider shortDescriptionProvider
     *
     * @param string $shortDescription
     * @param array $expected
     */
    public function testShortDescriptionIsTransformedToArray(string $shortDescription, array $expected)
    {
        $transformer = new FilteredProductDescription();

        $actual = $transformer->filteredShortDescription($shortDescription);

        $this->assertSame($expected, $actual);
    }
}
