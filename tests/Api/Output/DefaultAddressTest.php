<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Customer\Order\Shipping\Address as ShippingAddress;
use Linio\Frontend\Customer\Order\Shipping\PickupStore;
use Linio\Frontend\Location\Address;

class DefaultAddressTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromAddress()
    {
        $expected = [
            'id' => 1,
            'title' => 'title',
            'type' => 'type',
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'prefix' => 'prefix',
            'line1' => 'line1',
            'line2' => 'line2',
            'betweenStreet1' => 'betweenStreet1',
            'betweenStreet2' => 'betweenStreet2',
            'streetNumber' => 'streetNumber',
            'apartment' => 'apartment',
            'lot' => 'lot',
            'neighborhood' => 'neighborhood',
            'department' => 'department',
            'municipality' => 'municipality',
            'urbanization' => 'urbanization',
            'city' => 'city',
            'cityId' => 2,
            'cityName' => 'cityName',
            'region' => 'region',
            'regionId' => 3,
            'regionCode' => 'regionCode',
            'regionName' => 'regionName',
            'postcode' => 'postcode',
            'additionalInformation' => 'additionalInformation',
            'phone' => 'phone',
            'mobilePhone' => 'mobilePhone',
            'countryId' => 4,
            'countryCode' => 'countryCode',
            'countryName' => 'countryName',
            'taxIdentificationNumber' => 'taxIdentificationNumber',
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => 'maternalName',
            'invoiceType' => 'invoiceType',
            'company' => 'company',
        ];

        $address = new Address();
        $address->setId(1);
        $address->setTitle('title');
        $address->setType('type');
        $address->setFirstName('firstName');
        $address->setLastName('lastName');
        $address->setPrefix('prefix');
        $address->setLine1('line1');
        $address->setLine2('line2');
        $address->setBetweenStreet1('betweenStreet1');
        $address->setBetweenStreet2('betweenStreet2');
        $address->setStreetNumber('streetNumber');
        $address->setApartment('apartment');
        $address->setLot('lot');
        $address->setNeighborhood('neighborhood');
        $address->setDepartment('department');
        $address->setMunicipality('municipality');
        $address->setUrbanization('urbanization');
        $address->setCity('city');
        $address->setCityId(2);
        $address->setCityName('cityName');
        $address->setRegion('region');
        $address->setRegionId(3);
        $address->setRegionCode('regionCode');
        $address->setRegionName('regionName');
        $address->setPostcode('postcode');
        $address->setAdditionalInformation('additionalInformation');
        $address->setPhone('phone');
        $address->setMobilePhone('mobilePhone');
        $address->setCountryId(4);
        $address->setCountryCode('countryCode');
        $address->setCountryName('countryName');
        $address->setTaxIdentificationNumber('taxIdentificationNumber');
        $address->setDefaultBilling(true);
        $address->setDefaultShipping(true);
        $address->setMaternalName('maternalName');
        $address->setInvoiceType('invoiceType');
        $address->setCompany('company');

        $output = new DefaultAddress();
        $actual = $output->fromAddress($address);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider addressesProvider
     *
     * @param Address $address1
     * @param Address $address2
     */
    public function testIsOutputtingFromAddresses(Address $address1, Address $address2)
    {
        $output = new DefaultAddress();

        $actual = $output->fromAddresses([$address1, $address2]);

        $this->assertArrayHasKey(1, $actual);
        $this->assertArrayHasKey(2, $actual);
    }

    /**
     * @dataProvider shippingAddressProvider
     *
     * @param ShippingAddress $shippingAddress
     * @param array $shippingAddressOutput
     */
    public function testIsOutputtingFromShippingAddress(ShippingAddress $shippingAddress, array $shippingAddressOutput)
    {
        $output = new DefaultAddress();
        $actual = $output->fromShippingAddress($shippingAddress);
        $this->assertEquals($shippingAddressOutput, $actual);
    }

    /**
     * @dataProvider storePickupShippingAddressProvider
     *
     * @param ShippingAddress $shippingAddress
     * @param array $shippingAddressOutput
     */
    public function testIsOutputtingFromShippingAddressWithStorePickup(ShippingAddress $shippingAddress, array $shippingAddressOutput)
    {
        $output = new DefaultAddress();
        $actual = $output->fromShippingAddress($shippingAddress);
        $this->assertEquals($shippingAddressOutput, $actual);
    }

    /**
     * @return array
     */
    public function addressesProvider(): array
    {
        $address1 = new Address();
        $address1->setId(1);
        $address1->setFirstName('firstName1');
        $address1->setLastName('lastName1');
        $address1->setLine1('line11');
        $address1->setLine2('line21');
        $address1->setStreetNumber('streetNumber1');
        $address1->setCity('city1');
        $address1->setMunicipality('municipality1');
        $address1->setNeighborhood('neighborhood1');
        $address1->setRegion('region1');
        $address1->setRegionId(1);
        $address1->setCountryId(2);
        $address1->setPhone('phone1');
        $address1->setPrefix('prefix1');
        $address1->setMobilePhone('mobilePhone1');
        $address1->setPostcode('postcode1');
        $address1->setDefaultBilling(true);
        $address1->setDefaultShipping(false);
        $address1->setApartment('apartment1');
        $address1->setAdditionalInformation('additionalInformation1');
        $address1->setBetweenStreet1('betweenStreet11');
        $address1->setBetweenStreet2('betweenStreet21');
        $address1->setTaxIdentificationNumber('taxIdentificationNumber1');
        $address1->setInvoiceType('invoiceType1');
        $address1->setCompany('company1');
        $address1->setLot('lot1');
        $address1->setUrbanization('urbanization1');
        $address1->setType('directionType1');
        $address1->setDepartment('department1');

        $address2 = new Address();
        $address2->setId(2);
        $address2->setFirstName('firstName2');
        $address2->setLastName('lastName2');
        $address2->setLine1('line12');
        $address2->setLine2('line22');
        $address2->setStreetNumber('streetNumber2');
        $address2->setCity('city2');
        $address2->setMunicipality('municipality2');
        $address2->setNeighborhood('neighborhood2');
        $address2->setRegion('region2');
        $address2->setRegionId(1);
        $address2->setCountryId(2);
        $address2->setPhone('phone2');
        $address2->setPrefix('prefix2');
        $address2->setMobilePhone('mobilePhone2');
        $address2->setPostcode('postcode2');
        $address2->setDefaultBilling(false);
        $address2->setDefaultShipping(true);
        $address2->setApartment('apartment2');
        $address2->setAdditionalInformation('additionalInformation2');
        $address2->setBetweenStreet1('betweenStreet12');
        $address2->setBetweenStreet2('betweenStreet22');
        $address2->setTaxIdentificationNumber('taxIdentificationNumber2');
        $address2->setInvoiceType('invoiceType2');
        $address2->setCompany('company2');
        $address2->setLot('lot2');
        $address2->setUrbanization('urbanization2');
        $address2->setType('directionType2');
        $address2->setDepartment('department2');

        return [
            [$address1, $address2],
        ];
    }

    /**
     * @return array
     */
    public function shippingAddressProvider(): array
    {
        $shippingAddressOutput = [
            'id' => 1,
            'title' => 'title',
            'type' => 'directionType',
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'prefix' => 'prefix',
            'line1' => 'line1',
            'line2' => 'line2',
            'betweenStreet1' => 'betweenStreet1',
            'betweenStreet2' => 'betweenStreet2',
            'streetNumber' => 'streetNumber',
            'apartment' => 'apartment',
            'lot' => 'lot',
            'neighborhood' => 'neighborhood',
            'department' => 'department',
            'municipality' => 'municipality',
            'urbanization' => 'urbanization',
            'city' => 'city',
            'cityId' => 1,
            'cityName' => 'cityName',
            'region' => 'region',
            'regionId' => 1,
            'regionCode' => 'regionCode',
            'regionName' => 'regionName',
            'postcode' => 'postcode',
            'additionalInformation' => 'additionalInformation',
            'phone' => 'phone',
            'mobilePhone' => 'mobilePhone',
            'countryId' => 2,
            'countryCode' => 'countryCode',
            'countryName' => 'countryName',
            'taxIdentificationNumber' => 'taxIdentificationNumber',
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => 'maternalName',
            'invoiceType' => 'invoiceType',
            'company' => 'company',
            'country' => 'countryName',
            'alternativeRecipientName' => 'alternativeRecipientName',
            'alternativeRecipientId' => 'alternativeRecipientId',
            'directionPrefix' => 'directionPrefix',
            'nationalRegistrationNumber' => 'nationalRegistrationNumber',
            'pickupStore' => null,
        ];

        $shippingAddress = new ShippingAddress();
        $shippingAddress->setId($shippingAddressOutput['id']);
        $shippingAddress->setTitle($shippingAddressOutput['title']);
        $shippingAddress->setFirstName($shippingAddressOutput['firstName']);
        $shippingAddress->setLastName($shippingAddressOutput['lastName']);
        $shippingAddress->setLine1($shippingAddressOutput['line1']);
        $shippingAddress->setLine2($shippingAddressOutput['line2']);
        $shippingAddress->setStreetNumber($shippingAddressOutput['streetNumber']);
        $shippingAddress->setCity($shippingAddressOutput['city']);
        $shippingAddress->setCityId($shippingAddressOutput['cityId']);
        $shippingAddress->setCityName($shippingAddressOutput['cityName']);
        $shippingAddress->setMunicipality($shippingAddressOutput['municipality']);
        $shippingAddress->setNeighborhood($shippingAddressOutput['neighborhood']);
        $shippingAddress->setRegion($shippingAddressOutput['region']);
        $shippingAddress->setRegionId($shippingAddressOutput['regionId']);
        $shippingAddress->setRegionCode($shippingAddressOutput['regionCode']);
        $shippingAddress->setRegionName($shippingAddressOutput['regionName']);
        $shippingAddress->setCountryId($shippingAddressOutput['countryId']);
        $shippingAddress->setCountryName($shippingAddressOutput['countryName']);
        $shippingAddress->setPhone($shippingAddressOutput['phone']);
        $shippingAddress->setPrefix($shippingAddressOutput['prefix']);
        $shippingAddress->setMobilePhone($shippingAddressOutput['mobilePhone']);
        $shippingAddress->setPostcode($shippingAddressOutput['postcode']);
        $shippingAddress->setDefaultBilling($shippingAddressOutput['defaultBilling']);
        $shippingAddress->setDefaultShipping($shippingAddressOutput['defaultShipping']);
        $shippingAddress->setApartment($shippingAddressOutput['apartment']);
        $shippingAddress->setAdditionalInformation($shippingAddressOutput['additionalInformation']);
        $shippingAddress->setBetweenStreet1($shippingAddressOutput['betweenStreet1']);
        $shippingAddress->setBetweenStreet2($shippingAddressOutput['betweenStreet2']);
        $shippingAddress->setTaxIdentificationNumber($shippingAddressOutput['taxIdentificationNumber']);
        $shippingAddress->setInvoiceType($shippingAddressOutput['invoiceType']);
        $shippingAddress->setCompany($shippingAddressOutput['company']);
        $shippingAddress->setLot($shippingAddressOutput['lot']);
        $shippingAddress->setUrbanization($shippingAddressOutput['urbanization']);
        $shippingAddress->setType($shippingAddressOutput['type']);
        $shippingAddress->setDepartment($shippingAddressOutput['department']);
        $shippingAddress->setCountry($shippingAddressOutput['countryName']);
        $shippingAddress->setCountryCode($shippingAddressOutput['countryCode']);
        $shippingAddress->setAlternativeRecipientName($shippingAddressOutput['alternativeRecipientName']);
        $shippingAddress->setAlternativeRecipientId($shippingAddressOutput['alternativeRecipientId']);
        $shippingAddress->setDirectionPrefix($shippingAddressOutput['directionPrefix']);
        $shippingAddress->setMaternalName($shippingAddressOutput['maternalName']);
        $shippingAddress->setNationalRegistrationNumber($shippingAddressOutput['nationalRegistrationNumber']);

        return [
            [$shippingAddress, $shippingAddressOutput],
        ];
    }

    /**
     * @return array
     */
    public function storePickupShippingAddressProvider(): array
    {
        $shippingAddressOutput = [
            'id' => 1,
            'title' => 'title',
            'type' => 'directionType',
            'firstName' => 'firstName',
            'lastName' => 'lastName',
            'prefix' => 'prefix',
            'line1' => 'line1',
            'line2' => 'line2',
            'betweenStreet1' => 'betweenStreet1',
            'betweenStreet2' => 'betweenStreet2',
            'streetNumber' => 'streetNumber',
            'apartment' => 'apartment',
            'lot' => 'lot',
            'neighborhood' => 'neighborhood',
            'department' => 'department',
            'municipality' => 'municipality',
            'urbanization' => 'urbanization',
            'city' => 'city',
            'cityId' => 1,
            'cityName' => 'cityName',
            'region' => 'region',
            'regionId' => 1,
            'regionCode' => 'regionCode',
            'regionName' => 'regionName',
            'postcode' => 'postcode',
            'additionalInformation' => 'additionalInformation',
            'phone' => 'phone',
            'mobilePhone' => 'mobilePhone',
            'countryId' => 2,
            'countryCode' => 'countryCode',
            'countryName' => 'countryName',
            'taxIdentificationNumber' => 'taxIdentificationNumber',
            'defaultBilling' => true,
            'defaultShipping' => true,
            'maternalName' => 'maternalName',
            'invoiceType' => 'invoiceType',
            'company' => 'company',
            'country' => 'countryName',
            'alternativeRecipientName' => 'alternativeRecipientName',
            'alternativeRecipientId' => 'alternativeRecipientId',
            'directionPrefix' => 'directionPrefix',
            'nationalRegistrationNumber' => 'nationalRegistrationNumber',
            'pickupStore' => [
                'id' => 1,
                'name' => 'storeName',
                'description' => 'storeDescription',
                'geohash' => 'gbsuv',
                'itemCapacity' => 3,
                'weightCapacity' => 50,
            ],
        ];

        $pickupStore = new PickupStore($shippingAddressOutput['pickupStore']['id']);
        $pickupStore->setName($shippingAddressOutput['pickupStore']['name']);
        $pickupStore->setDescription($shippingAddressOutput['pickupStore']['description']);
        $pickupStore->setGeohash($shippingAddressOutput['pickupStore']['geohash']);
        $pickupStore->setItemCapacity($shippingAddressOutput['pickupStore']['itemCapacity']);
        $pickupStore->setWeightCapacity($shippingAddressOutput['pickupStore']['weightCapacity']);

        $shippingAddress = new ShippingAddress();
        $shippingAddress->setId($shippingAddressOutput['id']);
        $shippingAddress->setTitle($shippingAddressOutput['title']);
        $shippingAddress->setFirstName($shippingAddressOutput['firstName']);
        $shippingAddress->setLastName($shippingAddressOutput['lastName']);
        $shippingAddress->setLine1($shippingAddressOutput['line1']);
        $shippingAddress->setLine2($shippingAddressOutput['line2']);
        $shippingAddress->setStreetNumber($shippingAddressOutput['streetNumber']);
        $shippingAddress->setCity($shippingAddressOutput['city']);
        $shippingAddress->setCityId($shippingAddressOutput['cityId']);
        $shippingAddress->setCityName($shippingAddressOutput['cityName']);
        $shippingAddress->setMunicipality($shippingAddressOutput['municipality']);
        $shippingAddress->setNeighborhood($shippingAddressOutput['neighborhood']);
        $shippingAddress->setRegion($shippingAddressOutput['region']);
        $shippingAddress->setRegionId($shippingAddressOutput['regionId']);
        $shippingAddress->setRegionCode($shippingAddressOutput['regionCode']);
        $shippingAddress->setRegionName($shippingAddressOutput['regionName']);
        $shippingAddress->setCountryId($shippingAddressOutput['countryId']);
        $shippingAddress->setCountryName($shippingAddressOutput['countryName']);
        $shippingAddress->setPhone($shippingAddressOutput['phone']);
        $shippingAddress->setPrefix($shippingAddressOutput['prefix']);
        $shippingAddress->setMobilePhone($shippingAddressOutput['mobilePhone']);
        $shippingAddress->setPostcode($shippingAddressOutput['postcode']);
        $shippingAddress->setDefaultBilling($shippingAddressOutput['defaultBilling']);
        $shippingAddress->setDefaultShipping($shippingAddressOutput['defaultShipping']);
        $shippingAddress->setApartment($shippingAddressOutput['apartment']);
        $shippingAddress->setAdditionalInformation($shippingAddressOutput['additionalInformation']);
        $shippingAddress->setBetweenStreet1($shippingAddressOutput['betweenStreet1']);
        $shippingAddress->setBetweenStreet2($shippingAddressOutput['betweenStreet2']);
        $shippingAddress->setTaxIdentificationNumber($shippingAddressOutput['taxIdentificationNumber']);
        $shippingAddress->setInvoiceType($shippingAddressOutput['invoiceType']);
        $shippingAddress->setCompany($shippingAddressOutput['company']);
        $shippingAddress->setLot($shippingAddressOutput['lot']);
        $shippingAddress->setUrbanization($shippingAddressOutput['urbanization']);
        $shippingAddress->setType($shippingAddressOutput['type']);
        $shippingAddress->setDepartment($shippingAddressOutput['department']);
        $shippingAddress->setCountry($shippingAddressOutput['countryName']);
        $shippingAddress->setCountryCode($shippingAddressOutput['countryCode']);
        $shippingAddress->setAlternativeRecipientName($shippingAddressOutput['alternativeRecipientName']);
        $shippingAddress->setAlternativeRecipientId($shippingAddressOutput['alternativeRecipientId']);
        $shippingAddress->setDirectionPrefix($shippingAddressOutput['directionPrefix']);
        $shippingAddress->setMaternalName($shippingAddressOutput['maternalName']);
        $shippingAddress->setPickupStore($pickupStore);
        $shippingAddress->setNationalRegistrationNumber($shippingAddressOutput['nationalRegistrationNumber']);

        return [
            [$shippingAddress, $shippingAddressOutput],
        ];
    }
}
