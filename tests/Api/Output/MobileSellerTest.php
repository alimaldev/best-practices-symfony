<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Entity\Seller\PaginatedReviews;
use Linio\Frontend\Entity\Seller\Rating;
use Linio\Frontend\Entity\Seller\RatingCollection;
use Linio\Frontend\Entity\Seller\RatingSummary;
use Linio\Frontend\Entity\Seller\Review;
use Linio\Frontend\Entity\Seller\ReviewCollection;
use Linio\Frontend\Entity\Seller\Seller as ReviewSeller;

class MobileSellerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromSeller()
    {
        $sellerOutput = [
            'id' => 232,
            'name' => 'TECDEPOT',
            'slug' => 'tecdepot',
            'rating' => 4,
            'type' => 'merchant',
            'operationType' => 'bulk_import',
            'import' => true,
        ];

        $seller = new Seller();
        $seller->setSellerId($sellerOutput['id']);
        $seller->setName($sellerOutput['name']);
        $seller->setSlug($sellerOutput['slug']);
        $seller->setRating($sellerOutput['rating']);
        $seller->setType($sellerOutput['type']);
        $seller->setOperationType($sellerOutput['operationType']);

        $output = new MobileSeller();
        $actual = $output->fromSeller($seller);

        $this->assertEquals($sellerOutput, $actual);
    }

    public function testGetsReviewsFromPaginatedReviews()
    {
        $reviewsOutput = [
            'currentPage' => 1,
            'totalPages' => 1,
            'reviews' => [
                [
                'title' => 'Buen precio',
                'customerName' => 'Ned Stark',
                'comment' => 'Todo perfecto',
                'createdAt' => '2016-06-18T00:00:00+0000',
                'experienceAverage' => 5,
            ],
            [
                'title' => 'Llegó después',
                'customerName' => 'Arya Stark',
                'comment' => 'Pudo ser mejor',
                'createdAt' => '2016-06-14T00:00:00+0000',
                'experienceAverage' => 4,
            ],
                ],
        ];

        $review1 = new Review(
            'Buen precio',
            'Todo perfecto',
            5,
            'Ned Stark',
            new DateTime('2016-06-18')
        );

        $review2 = new Review(
            'Llegó después',
            'Pudo ser mejor',
            4,
            'Arya Stark',
            new DateTime('2016-06-14')
        );

        $reviewCollection = new ReviewCollection();
        $reviewCollection->add($review1);
        $reviewCollection->add($review2);

        $paginatedReview = new PaginatedReviews(1, 1, $reviewCollection);

        $output = new MobileSeller();
        $actual = $output->fromPaginatedReviews($paginatedReview);

        $this->assertEquals($reviewsOutput, $actual);
    }

    public function testIsOutputtingFromReviewSeller()
    {
        $expected = [
            'id' => 549,
            'name' => 'FUNDACION EL TRIANGULO',
            'slug' => 'fundacion-el-triangulo',
            'type' => 'merchant',
            'operationType' => null,
            'ratingSummary' => [
                'averagePoint' => 5,
                'ratings' => [
                    [
                        'stars' => 1,
                        'votes' => 0,
                    ],
                    [
                        'stars' => 3,
                        'votes' => 0,
                    ],
                ],
                'reviewsQuantity' => 2,
            ],
        ];

        $rating = new Rating(
            $expected['ratingSummary']['ratings'][0]['stars'],
            $expected['ratingSummary']['ratings'][0]['votes']
        );
        $rating2 = new Rating(
            $expected['ratingSummary']['ratings'][1]['stars'],
            $expected['ratingSummary']['ratings'][1]['votes']
        );

        $ratingCollection = new RatingCollection();
        $ratingCollection->add($rating);
        $ratingCollection->add($rating2);

        $ratingSummary = new RatingSummary(
            $expected['ratingSummary']['averagePoint'],
            $expected['ratingSummary']['reviewsQuantity'],
            $ratingCollection
        );

        $reviewSeller = new ReviewSeller(
            $expected['id'],
            $expected['name'],
            $expected['slug'],
            $expected['type'],
            $ratingSummary,
            null
        );

        $output = new MobileSeller();
        $actual = $output->fromReviewSeller($reviewSeller);

        $this->assertEquals($expected, $actual);
    }
}
