<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Partnership;
use Linio\Frontend\Order\Payment\InstallmentOption;
use Linio\Frontend\Order\Payment\InstallmentOptions;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\Payment\PaymentRedirect;
use Linio\Frontend\Order\RecalculatedOrder;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\Packages;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\PickupStores;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Linio\Frontend\Order\Shipping\ShippingQuotes;
use Linio\Frontend\Order\Wallet;
use Linio\Type\Money;
use Symfony\Component\Translation\TranslatorInterface;

class MobileOrderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromRecalculatedOrder()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'shippingAddress' => null,
                'items' => [
                    'ABC-123' => [
                        'sku' => 'ABC-123',
                        'name' => 'Foobar',
                        'description' => 'foo bar baz',
                        'slug' => null,
                        'variationType' => 'default',
                        'seller' => [
                            'id' => 1,
                            'name' => 'name',
                            'slug' => 'slug',
                            'rating' => 3,
                            'type' => 'merchant',
                            'operationType' => 'operationType',
                            'import' => false,
                        ],
                        'unitPrice' => 0.0,
                        'originalPrice' => 0.0,
                        'paidPrice' => 0.0,
                        'taxAmount' => 0.0,
                        'shippingAmount' => 0.0,
                        'subtotal' => 0,
                        'quantity' => 3,
                        'availableQuantity' => 0,
                        'maxItemsToSell' => 1,
                        'image' => '',
                        'linioPlusLevel' => 0,
                        'linioPlusEnabledQuantity' => 0,
                        'variation' => 'black',
                        'freeShipping' => false,
                        'freeStorePickup' => false,
                        'percentageOff' => 0.0,
                        'deliveredByChristmas' => false,
                        'minimumDeliveryDate' => 'January 1, 2015',
                        'imported' => false,
                        'oversized' => null,
                        'brand' => [
                            'id' => 1,
                            'name' => 'A brand',
                            'slug' => 'a-brand-slug',
                        ],
                        'category' => [
                            'id' => '10000',
                            'name' => 'Category name',
                            'slug' => 'categories/category-name',
                            'urlKey' => 'category-name',
                            'isCurrent' => false,
                            'count' => 0,
                        ],
                    ],
                ],
                'packages' => [
                    1 => [
                        'items' => ['ABC-123' => 3],
                        'shippingQuotes' => [
                            [
                                'shippingMethod' => 'jegue',
                                'fee' => 0.0,
                                'estimatedDeliveryDate' => 'January 1, 2015',
                                'deliveredByChristmas' => false,
                                'selected' => false,
                                'freeShipping' => true,
                                'name' => 'Super Jegue',
                            ],
                        ],
                    ],
                ],
                'undeliverables' => ['ABC-789'],
                'wallet' => [
                    'totalDiscount' => 0.0,
                    'totalPointsUsed' => 0.0,
                    'pointsBalance' => 0.0,
                    'shippingDiscount' => 0.0,
                    'pointsUsedForShipping' => 0.0,
                    'maxPointsForOrder' => 0.0,
                    'conversionRate' => 0.0,
                ],
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'pickupStores' => [
                    '2' => [
                        'id' => 2,
                        'name' => 'Pickup Store 2',
                        'description' => 'Blah blah blah',
                        'postcode' => '33020',
                        'geohash' => 'dhwupu4907e',
                        'region' => 'Region',
                        'municipality' => 'Municipality',
                        'city' => 'City',
                        'subLocality' => 'Sublocality',
                        'neighborhood' => 'Neighborhood',
                        'line1' => 'AddressLine1',
                        'line2' => 'AddressLine2',
                        'apartment' => 'Apartment',
                        'referencePoint' => 'ReferencePoint',
                        'network' => [
                            'id' => 1,
                            'name' => 'NetworkName',
                        ],
                        'selected' => false,
                    ],
                ],
                'geohash' => 'geohash',
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $simple = new Simple();
        $simple->setSku($expectedApiOutput['order']['items']['ABC-123']['sku']);
        $simple->addAttribute('variation', 'black');
        $simple->setHasFreeShipping(false);

        $product = new Product('ABC');
        $product->setName($expectedApiOutput['order']['items']['ABC-123']['name']);
        $product->setDescription($expectedApiOutput['order']['items']['ABC-123']['description']);
        $product->addSimple($simple);
        $product->setVariationType($expectedApiOutput['order']['items']['ABC-123']['variationType']);
        $product->setImported(false);

        $seller = new Seller();
        $seller->setSellerId($expectedApiOutput['order']['items']['ABC-123']['seller']['id']);
        $seller->setName($expectedApiOutput['order']['items']['ABC-123']['seller']['name']);
        $seller->setSlug($expectedApiOutput['order']['items']['ABC-123']['seller']['slug']);
        $seller->setRating($expectedApiOutput['order']['items']['ABC-123']['seller']['rating']);
        $seller->setType($expectedApiOutput['order']['items']['ABC-123']['seller']['type']);
        $seller->setOperationType($expectedApiOutput['order']['items']['ABC-123']['seller']['operationType']);

        $product->setSeller($seller);

        $brand = new Brand();
        $brand->setId(1);
        $brand->setName('A brand');
        $brand->setSlug('a-brand-slug');

        $product->setBrand($brand);

        $category = new Category();
        $category->setId($expectedApiOutput['order']['items']['ABC-123']['category']['id']);
        $category->setName($expectedApiOutput['order']['items']['ABC-123']['category']['name']);
        $category->setSlug($expectedApiOutput['order']['items']['ABC-123']['category']['slug']);
        $category->setUrlKey($expectedApiOutput['order']['items']['ABC-123']['category']['urlKey']);
        $category->setIsCurrent($expectedApiOutput['order']['items']['ABC-123']['category']['isCurrent']);
        $category->setCount($expectedApiOutput['order']['items']['ABC-123']['category']['count']);

        $product->setCategory($category);

        $orderItem = new Item('ABC-123', 3, $product);

        $dateTime = new DateTime('2015-01-01');
        $orderItem->setMinimumDeliveryDate($dateTime);
        $undeliverableOrderItem = new Item('ABC-789', 1, $product);

        $shippingQuote = new ShippingQuote('jegue', new \DateTime('2015-01-01'));
        $shippingQuotes = new ShippingQuotes();
        $shippingQuotes->add($shippingQuote);

        $package = new Package();
        $package->setId(1);
        $package->addItem($orderItem);
        $package->setShippingQuotes($shippingQuotes);

        $packages = new Packages();
        $packages->add($package);

        $pickupStore = new PickupStore($expectedApiOutput['order']['pickupStores']['2']['id']);
        $pickupStore->setName($expectedApiOutput['order']['pickupStores']['2']['name']);
        $pickupStore->setDescription($expectedApiOutput['order']['pickupStores']['2']['description']);
        $pickupStore->setPostcode($expectedApiOutput['order']['pickupStores']['2']['postcode']);
        $pickupStore->setGeohash($expectedApiOutput['order']['pickupStores']['2']['geohash']);
        $pickupStore->setRegion($expectedApiOutput['order']['pickupStores']['2']['region']);
        $pickupStore->setMunicipality($expectedApiOutput['order']['pickupStores']['2']['municipality']);
        $pickupStore->setCity($expectedApiOutput['order']['pickupStores']['2']['city']);
        $pickupStore->setSubLocality($expectedApiOutput['order']['pickupStores']['2']['subLocality']);
        $pickupStore->setNeighborhood($expectedApiOutput['order']['pickupStores']['2']['neighborhood']);
        $pickupStore->setLine1($expectedApiOutput['order']['pickupStores']['2']['line1']);
        $pickupStore->setLine2($expectedApiOutput['order']['pickupStores']['2']['line2']);
        $pickupStore->setApartment($expectedApiOutput['order']['pickupStores']['2']['apartment']);
        $pickupStore->setReferencePoint($expectedApiOutput['order']['pickupStores']['2']['referencePoint']);
        $pickupStore->setNetworkId($expectedApiOutput['order']['pickupStores']['2']['network']['id']);
        $pickupStore->setNetworkName($expectedApiOutput['order']['pickupStores']['2']['network']['name']);
        $pickupStores = new PickupStores();
        $pickupStores->add($pickupStore);

        $order = new Order('42');
        $order->addItem($orderItem);
        $order->setPackages($packages);
        $order->setCustomer(new Customer());
        $order->setPickupStores($pickupStores);
        $order->setGeohash('geohash');
        $order->setWallet(new Wallet());

        $recalculatedOrder = new RecalculatedOrder($order);
        $recalculatedOrder->addUndeliverable($undeliverableOrderItem);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('checkout.shipping_methods.jegue')->willReturn('Super Jegue');

        $mobileSeller = $this->prophesize(MobileSeller::class);
        $mobileSeller->fromSeller($seller)->willReturn($expectedApiOutput['order']['items']['ABC-123']['seller']);

        $filteredDescription = $this->prophesize(FilteredProductDescription::class);
        $filteredDescription->filteredProductDescription($expectedApiOutput['order']['items']['ABC-123']['description'])->willReturn($expectedApiOutput['order']['items']['ABC-123']['description']);

        $dateFormatter = $this->prophesize(DateFormatter::class);
        $dateFormatter->formatLocale(new \DateTime('2015-01-01'))->willReturn('January 1, 2015');

        $output = new MobileOrder();
        $output->setTranslator($translator->reveal());
        $output->setSellerOutput($mobileSeller->reveal());
        $output->setDateFormatter($dateFormatter->reveal());
        $output->setFilteredProductDescriptionOutput($filteredDescription->reveal());
        $apiOutput = $output->fromRecalculatedOrder($recalculatedOrder);
        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingFromEmptyRecalculatedOrder()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => null,
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => 0.0,
                'originalShippingAmount' => 0.0,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'pickupStores' => null,
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $output = new MobileOrder();
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder(new Order('foobar')));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithTranslatedMessages()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => 0.0,
                'originalShippingAmount' => 0.0,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [
                    [
                        'installments' => 1,
                        'interestFee' => 0.0,
                        'total' => 0.0,
                        'totalInterest' => 0.0,
                        'amount' => 0.0,
                        'selected' => false,
                        'paymentMethodName' => null,
                        'description' => 'Translated installment. Yay!',
                        'hasBankInterestFee' => false,
                    ],
                ],
                'highestInstallment' => 1,
                'lowestInstallment' => 1,
                'pickupStores' => null,
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [
                    'Translated error. Yay!',
                ],
                'warnings' => [
                    'Translated Warning #1. Yay!',
                    'Translated Warning #2. Yay!',
                ],
                'success' => 'Foobar!',
            ],
        ];

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->transChoice('checkout.installment_option', 1, ['%amount%' => 0, '%total%' => 0, '%interest%' => 'no interest', '%count%' => 1])->willReturn('Translated installment. Yay!');
        $translator->trans('error.key', ['%context%' => 'ABC-123'])->willReturn('Translated error. Yay!');
        $translator->trans('warning1.key', ['%context%' => 'FOO-123'])->willReturn('Translated Warning #1. Yay!');
        $translator->trans('warning2.key', ['%context%' => 'FOO-123'])->willReturn('Translated Warning #2. Yay!');
        $translator->trans('success.key')->willReturn('Foobar!');
        $translator->trans('checkout.without_interest')->willReturn('no interest');

        $moneyFormatter = $this->prophesize(MoneyFormatter::class);
        $moneyFormatter->format(new Money())->willReturn(0);
        $moneyFormatter->format(new Money())->willReturn(0);

        $installmentOption = new InstallmentOption();
        $installmentOptions = new InstallmentOptions();
        $installmentOptions->add($installmentOption);

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setInstallmentOptions($installmentOptions);

        $recalculatedOrder = new RecalculatedOrder($order);
        $recalculatedOrder->setErrors(['ABC-123' => 'error.key']);
        $recalculatedOrder->setWarnings(['FOO-123' => ['warning1.key', 'warning2.key']]);
        $recalculatedOrder->setSuccessMessage('success.key');

        $output = new MobileOrder();
        $output->setTranslator($translator->reveal());
        $output->setMoneyFormatter($moneyFormatter->reveal());
        $apiOutput = $output->fromRecalculatedOrder($recalculatedOrder);

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithShippingAddress()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'shippingAddress' => ['foo' => 'bar'],
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'pickupStores' => null,
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $address = new Address();
        $addressOutput = $this->prophesize(MobileAddress::class);
        $addressOutput->fromAddress($address)->willReturn(['foo' => 'bar']);

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setShippingAddress($address);

        $output = new MobileOrder();
        $output->setAddressOutput($addressOutput->reveal());
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithPaymentMethods()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [
                    'CashOnDelivery_Payment' => [
                        'allowed' => true,
                        'allowedByCoupon' => false,
                        'requireBillingAddress' => false,
                        'paymentMethodLabel' => 'Pago en efectivo al recibir',
                    ],
                    'Consignacion_Payment' => [
                        'allowed' => true,
                        'allowedByCoupon' => false,
                        'requireBillingAddress' => true,
                        'paymentMethodLabel' => 'Depósito bancario o transferencia electrónica',
                    ],
                    'PayClub_HostedPaymentPage' => [
                        'allowed' => true,
                        'allowedByCoupon' => true,
                        'requireBillingAddress' => false,
                        'paymentMethodLabel' => 'Tarjeta de crédito',
                    ],
                ],
                'paymentMethod' => 'Foobar',
                'creditCardBinNumber' => '123456',
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'pickupStores' => null,
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $normal = PaymentMethodFactory::fromName('CashOnDelivery_Payment');
        $normal->allow();

        $needsBilling = PaymentMethodFactory::fromName('Consignacion_Payment');
        $needsBilling->allow();
        $needsBilling->requireBillingAddress();

        $allowedByCoupon = PaymentMethodFactory::fromName('PayClub_HostedPaymentPage');
        $allowedByCoupon->allow();
        $allowedByCoupon->allowByCoupon();

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans(sprintf('payment_methods.%s.display_name', 'CashOnDelivery_Payment'))->willReturn('Pago en efectivo al recibir');
        $translator->trans(sprintf('payment_methods.%s.display_name', 'Consignacion_Payment'))->willReturn('Depósito bancario o transferencia electrónica');
        $translator->trans(sprintf('payment_methods.%s.display_name', 'PayClub_HostedPaymentPage'))->willReturn('Tarjeta de crédito');

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setAvailablePaymentMethods([$normal, $needsBilling, $allowedByCoupon]);

        $creditCard = new CreditCard($expectedApiOutput['order']['paymentMethod']);
        $creditCard->setBinNumber($expectedApiOutput['order']['creditCardBinNumber']);
        $order->setPaymentMethod($creditCard);

        $output = new MobileOrder();
        $output->setTranslator($translator->reveal());
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithPartnerships()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => 0.0,
                'originalShippingAmount' => 0.0,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [
                    [
                        'name' => 'Foobar!',
                        'description' => 'Foobar Foobar!',
                        'code' => 'foobar',
                        'accountNumber' => null,
                        'level' => null,
                        'appliedDiscount' => null,
                        'active' => false,
                    ],
                    [
                        'name' => 'Barfoo!',
                        'description' => 'Barfoo Barfoo!',
                        'code' => 'barfoo',
                        'accountNumber' => '123456',
                        'level' => null,
                        'appliedDiscount' => null,
                        'active' => true,
                    ],
                ],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'pickupStores' => null,
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('partnership.foobar.name')->willReturn($expectedApiOutput['order']['partnerships'][0]['name']);
        $translator->trans('partnership.foobar.description')->willReturn($expectedApiOutput['order']['partnerships'][0]['description']);
        $translator->trans('partnership.barfoo.name')->willReturn($expectedApiOutput['order']['partnerships'][1]['name']);
        $translator->trans('partnership.barfoo.description')->willReturn($expectedApiOutput['order']['partnerships'][1]['description']);

        $partnershipA = new Partnership($expectedApiOutput['order']['partnerships'][0]['code']);
        $partnershipB = new Partnership($expectedApiOutput['order']['partnerships'][1]['code'], $expectedApiOutput['order']['partnerships'][1]['accountNumber']);
        $partnershipB->enable();
        $partnerships = [$partnershipA, $partnershipB];

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setPartnerships($partnerships);

        $output = new MobileOrder();
        $output->setTranslator($translator->reveal());
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingFromCompletedOrder()
    {
        $expectedApiOutput = [
            'grandTotal' => 0.0,
            'orderNumber' => 'foobar',
            'paymentMethod' => 'MequetrequePay',
            'redirect' => [
                'method' => 'POST',
                'target' => '/foobar',
                'body' => [],
            ],
        ];

        $completedOrder = new CompletedOrder('123');
        $completedOrder->setOrderNumber($expectedApiOutput['orderNumber']);
        $completedOrder->setPaymentMethod(PaymentMethodFactory::fromName($expectedApiOutput['paymentMethod']));

        $redirect = new PaymentRedirect();
        $redirect->setMethod($expectedApiOutput['redirect']['method']);
        $redirect->setTarget($expectedApiOutput['redirect']['target']);
        $redirect->setBody([]);
        $completedOrder->setPaymentRedirect($redirect);

        $output = new MobileOrder();
        $apiOutput = $output->fromCompletedOrder($completedOrder);
        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingProductWithoutCategory()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'shippingAddress' => null,
                'items' => [
                    'DEF-456' => [
                        'sku' => 'DEF-456',
                        'name' => 'Foobar',
                        'description' => 'foo bar baz',
                        'slug' => null,
                        'variationType' => 'default',
                        'seller' => null,
                        'unitPrice' => 0.0,
                        'originalPrice' => 0.0,
                        'paidPrice' => 0.0,
                        'taxAmount' => 0.0,
                        'shippingAmount' => 0.0,
                        'subtotal' => 0.0,
                        'quantity' => 3,
                        'availableQuantity' => 0,
                        'maxItemsToSell' => 1,
                        'image' => '',
                        'linioPlusLevel' => 0,
                        'linioPlusEnabledQuantity' => 0,
                        'variation' => 'black',
                        'freeShipping' => false,
                        'freeStorePickup' => false,
                        'percentageOff' => 0.0,
                        'deliveredByChristmas' => false,
                        'minimumDeliveryDate' => null,
                        'imported' => false,
                        'oversized' => null,
                        'brand' => [
                            'id' => null,
                            'name' => null,
                            'slug' => null,
                        ],
                        'category' => null,
                    ],
                ],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'pickupStores' => null,
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $simple = new Simple();
        $simple->setSku($expectedApiOutput['order']['items']['DEF-456']['sku']);
        $simple->addAttribute('variation', 'black');
        $simple->setHasFreeShipping(false);

        $product = new Product('DEF');
        $product->setName($expectedApiOutput['order']['items']['DEF-456']['name']);
        $product->setDescription($expectedApiOutput['order']['items']['DEF-456']['description']);
        $product->addSimple($simple);
        $product->setVariationType($expectedApiOutput['order']['items']['DEF-456']['variationType']);
        $product->setImported(false);

        $orderItem = new Item('DEF-456', 3, $product);

        $order = new Order('foobar');
        $order->addItem($orderItem);
        $order->setCustomer(new Customer());

        $filteredDescription = $this->prophesize(FilteredProductDescription::class);
        $filteredDescription->filteredProductDescription($expectedApiOutput['order']['items']['DEF-456']['description'])->willReturn($expectedApiOutput['order']['items']['DEF-456']['description']);

        $dateFormatter = $this->prophesize(DateFormatter::class);
        $dateFormatter->formatLocale(new \DateTime('2016-08-01'))->willReturn('August 1, 2016');

        $output = new MobileOrder();
        $output->setDateFormatter($dateFormatter->reveal());
        $output->setFilteredProductDescriptionOutput($filteredDescription->reveal());
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }
}
