<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output\Mobile\V2;

use Linio\Frontend\Api\Output\MobileSeller;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Catalog\Facets\MultipleChoice;
use Linio\Frontend\Entity\Catalog\Facets\Range;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Catalog\SearchResults;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Seller;
use Linio\Type\Money;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class CatalogResultTest extends PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider searchResultDataProvider
     *
     * @param array $originalSearchResult
     */
    public function testIsTransformingSearchResultsWithDidYouMean(array $originalSearchResult)
    {
        $searchResult = $this->buildSearchResult($originalSearchResult);

        $searchResults = new SearchResults($searchResult);
        $searchResults->setDidYouMeanSearchResult($searchResult);

        $router = $this->prophesize(RouterInterface::class);

        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('https');
        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request);

        $output = new CatalogResult();
        $output->setRouter($router->reveal());
        $output->setRequestStack($requestStack->reveal());
        $output->setSellerOutput(new MobileSeller());

        $actual = $output->fromSearchResults($searchResults);

        $this->assertArrayHasKey('didYouMean', $actual);
        $this->assertArrayHasKey('original', $actual);
        $this->assertGreaterThan(0, $actual['original']['totalItemsFound']);
        $this->assertGreaterThan(0, $actual['didYouMean']['totalItemsFound']);
    }

    /**
     * @dataProvider searchResultDataProvider
     *
     * @param array $originalSearchResult
     */
    public function testIsTransformingSearchResultsWithoutDidYouMean(array $originalSearchResult)
    {
        $searchResult = $this->buildSearchResult($originalSearchResult);
        $searchResults = new SearchResults($searchResult);

        $router = $this->prophesize(RouterInterface::class);

        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('https');
        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request);

        $output = new CatalogResult();
        $output->setRouter($router->reveal());
        $output->setRequestStack($requestStack->reveal());
        $output->setSellerOutput(new MobileSeller());

        $actual = $output->fromSearchResults($searchResults);

        $this->assertArrayHasKey('didYouMean', $actual);
        $this->assertArrayHasKey('original', $actual);
        $this->assertGreaterThan(0, $actual['original']['totalItemsFound']);
        $this->assertNull($actual['didYouMean']);
    }

    /**
     * @return array
     */
    public function searchResultDataProvider(): array
    {
        $originalSearchResult = [
            'totalItemsFound' => 28,
            'didYouMean' => 'iPhone',
            'relatedQueries' => ['smart phone', 'samsung'],
            'currentPage' => 1,
            'pageCount' => 1,
            'guidedSearchTerms' => ['iphone 6', 'iphone 6Plus'],
            'filters' => [
                'brand' => [
                    'label' => 'Marca',
                    'activeValues' => null,
                    'values' => [],
                ],
                'price' => [
                    'label' => 'Precio',
                    'activeValues' => [],
                    'values' => [
                        'min' => 330.0,
                        'max' => 1260.0,
                    ],
                ],
            ],
            'products' => [
                [
                    'sku' => 'iph0n36',
                    'name' => 'Smartphone Iphone 6S 16GB-Dorado',
                    'previousPrice' => 0.0,
                    'actualPrice' => 1260.0,
                    'percentageOff' => 0.0,
                    'linioPlusLevel' => 0,
                    'hasFreeShipping' => false,
                    'isOutOfStock' => false,
                    'deliveryTime' => 5,
                    'path' => '/mapi/v1/p/smartphone-iphone-6s-16gb-dorado-ld6e56',
                    'image' => 'https://media.linio.com.ec/p/apple-6309-88377-1-product.jpg',
                    'inWishLists' => true,
                    'oversized' => null,
                    'freeStorePickup' => false,
                    'seller' => [
                        'id' => 25,
                        'name' => '',
                        'slug' => '',
                        'rating' => 4,
                        'type' => 'merchant',
                        'operationType' => 'bulk',
                        'import' => false,
                    ],
                ],
            ],
        ];

        return [[$originalSearchResult]];
    }

    /**
     * @param array $values
     *
     * @return SearchResult
     */
    protected function buildSearchResult(array $values): SearchResult
    {
        $searchResult = new SearchResult();
        $searchResult->setTotalItemsFound($values['totalItemsFound']);
        $searchResult->setDidYouMeanTerm($values['didYouMean']);
        $searchResult->setRelatedQueries($values['relatedQueries']);
        $searchResult->setCurrentPage($values['currentPage']);
        $searchResult->setPageCount($values['pageCount']);
        $searchResult->setGuidedSearchTerms($values['guidedSearchTerms']);

        $facetCollection = new FacetCollection();

        foreach ($values['filters'] as $name => $value) {
            switch ($name) {
                case 'price':
                    $facet = new Range();
                    $facet->setMinimum(new Money($value['values']['min']));
                    $facet->setMaximum(new Money($value['values']['max']));
                    break;
                case 'is_international':
                case 'operating_system':
                case 'display_size':
                case 'brand':
                default:
                    $facet = new MultipleChoice();
                    $facet->setActiveValues($value['activeValues']);
                    $facet->setValues($value['values']);
                    break;
            }

            $facet->setName($name);
            $facet->setLabel($value['label']);

            $facetCollection->add($facet);
        }

        $searchResult->setFacetCollection($facetCollection);

        $products = [];

        foreach ($values['products'] as $productData) {
            $product = new Product($productData['sku']);
            $product->setName($productData['name']);
            $product->setOriginalPrice(new Money($productData['previousPrice']));
            $product->setPrice(new Money($productData['actualPrice']));
            $product->setLinioPlusLevel($productData['linioPlusLevel']);
            $product->setHasFreeShipping($productData['hasFreeShipping']);
            $product->setStock($productData['isOutOfStock'] ? 0 : 1);
            $product->setDeliveryTime($productData['deliveryTime']);

            if ($productData['inWishLists']) {
                $product->addedToWishLists();
            }

            $pathParts = explode('/', $productData['path']);
            $slug = end($pathParts);

            $product->setSlug($slug);

            $image = new Image();
            $image->setSlug(rtrim($productData['image'], '-product.jpg'));
            $image->setMain(true);

            $product->setImages([$image]);

            $seller = new Seller();
            $seller->setSellerId($productData['seller']['id']);
            $seller->setName($productData['seller']['name']);
            $seller->setSlug($productData['seller']['slug']);
            $seller->setRating($productData['seller']['rating']);
            $seller->setType($productData['seller']['type']);
            $seller->setOperationType($productData['seller']['operationType']);

            $product->setSeller($seller);

            $products[] = $product;
        }

        $searchResult->setProducts($products);

        return $searchResult;
    }
}
