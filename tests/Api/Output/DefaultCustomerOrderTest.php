<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Customer\Order\Shipping\TrackingEvent;
use Linio\Frontend\Entity\Product;
use Linio\Type\Money;
use PHPUnit_Framework_TestCase;

class DefaultCustomerOrderTest extends PHPUnit_Framework_TestCase
{
    public function testIsMappingShipmentsForOutput()
    {
        $expected = [
            [
                'trackingCode' => '781846968074',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'FEDEX',
                'updatedAt' => '2015-12-07 21:52:22',
                'items' => [
                    [
                        'product' => 'SKU001-001',
                        'quantity' => 1,
                    ],
                    [
                        'product' => 'SKU001-002',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03 10:26:02'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03 17:58:02'],
                ],
            ],
            [
                'trackingCode' => 'Z7102304920394',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'UPS',
                'updatedAt' => '2015-12-07 21:52:22',
                'items' => [
                    [
                        'product' => 'SKU001-003',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03 10:26:02'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03 17:58:02'],
                ],
            ],
        ];

        $product1 = new Product();
        $product1->setSku('SKU001-001');

        $product2 = new Product();
        $product2->setSku('SKU001-002');

        $product3 = new Product();
        $product3->setSku('SKU001-003');

        $itemsShipping1 = [
            [
                'product' => $product1,
                'quantity' => 1,
            ],
            [
                'product' => $product2,
                'quantity' => 1,
            ],
        ];

        $itemsShipping2 = [
            [
                'product' => $product3,
                'quantity' => 1,
            ],
        ];

        $shipment1 = new Shipment($expected[0]['trackingCode']);
        $shipment1->setTrackingUrl($expected[0]['trackingUrl']);
        $shipment1->setCarrier($expected[0]['carrier']);
        $shipment1->setUpdatedAt(new DateTime($expected[0]['updatedAt']));
        $shipment1->setItems($itemsShipping1);
        $shipment1->setTrackingEvents([
            new TrackingEvent(
                $expected[0]['events'][0]['code'],
                $expected[0]['events'][0]['description'],
                new DateTime($expected[0]['events'][0]['createdAt'])
            ),
            new TrackingEvent(
                $expected[0]['events'][1]['code'],
                $expected[0]['events'][1]['description'],
                new DateTime($expected[0]['events'][1]['createdAt'])
            ),
        ]);

        $shipment2 = new Shipment($expected[1]['trackingCode']);
        $shipment2->setTrackingUrl($expected[1]['trackingUrl']);
        $shipment2->setCarrier($expected[1]['carrier']);
        $shipment2->setUpdatedAt(new DateTime($expected[1]['updatedAt']));
        $shipment2->setItems($itemsShipping2);
        $shipment2->setTrackingEvents([
            new TrackingEvent(
                $expected[1]['events'][0]['code'],
                $expected[1]['events'][0]['description'],
                new DateTime($expected[1]['events'][0]['createdAt'])
            ),
            new TrackingEvent(
                $expected[1]['events'][1]['code'],
                $expected[1]['events'][1]['description'],
                new DateTime($expected[1]['events'][1]['createdAt'])
            ),
        ]);

        $shipments = [$shipment1, $shipment2];

        $output = new DefaultCustomerOrder();

        $actual = $output->fromShipments($shipments);

        $this->assertSame($expected, $actual);
    }

    public function testIsOutputtingFromPendingOrders()
    {
        $expected = [
            [
                'id' => 12423,
                'orderNumber' => '348372938923',
                'purchaseDate' => '01 de Enero de 2016',
                'grandTotal' => 1300.0,
                'status' => ['status1'],
                'productCount' => 2,
            ],
        ];

        $item1 = new Item('ASKJDSASDDSDS-HJKD2');
        $item2 = new Item('SDADSLJSADJKL-LSOW2');

        $dateFormatter = $this->prophesize(DateFormatter::class);
        $dateFormatter->formatLocale(new DateTime('01/01/2016'))
            ->shouldBeCalled()
            ->willReturn('01 de Enero de 2016');

        $pendingOrder = new Order($expected[0]['id']);
        $pendingOrder->setOrderNumber($expected[0]['orderNumber']);
        $pendingOrder->setCreatedAt(new DateTime('01/01/2016'));
        $pendingOrder->setGrandTotal(new Money($expected[0]['grandTotal']));
        $pendingOrder->setStatusProgressConfiguration($expected[0]['status']);
        $pendingOrder->setItems([$item1, $item2]);

        $output = new DefaultCustomerOrder();
        $output->setDateFormatter($dateFormatter->reveal());

        $actual = $output->fromPendingOrders([$pendingOrder]);

        $this->assertSame($expected, $actual);
    }
}
