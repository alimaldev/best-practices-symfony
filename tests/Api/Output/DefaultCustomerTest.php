<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer;

class DefaultCustomerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsTransformingFromCustomerProfile()
    {
        $customer = new Customer();

        $customer->setId(1);
        $customer->setLinioId('x2qxu6');
        $customer->setFirstName('Anka');
        $customer->setLastName('Garcia');
        $customer->setGender('female');
        $customer->setBornDate(new DateTime('1991-10-10'));
        $customer->setEmail('customer-501@linio.com');
        $customer->setTaxIdentificationNumber(null);
        $customer->setNationalRegistrationNumber('171622007-2');

        $mileageProgram = new LoyaltyProgram();
        $mileageProgram->setId('13123334');
        $mileageProgram->setName('clubPremier');

        $customer->setLoyaltyProgram($mileageProgram);

        $output = new MobileCustomer();

        $customerProfile = $output->fromCustomer($customer);

        $this->assertEquals($customerProfile['id'], $customer->getId());
        $this->assertEquals($customerProfile['linio_id'], $customer->getLinioId());
        $this->assertEquals($customerProfile['first_name'], $customer->getFirstName());
        $this->assertEquals($customerProfile['last_name'], $customer->getLastName());
        $this->assertEquals($customerProfile['gender'], $customer->getGender());
        $this->assertEquals(new DateTime($customerProfile['bornDate']), $customer->getBornDate());
        $this->assertEquals($customerProfile['email'], $customer->getEmail());
        $this->assertEquals($customerProfile['loyalty_program'], $customer->getLoyaltyProgram()->getName());
        $this->assertEquals($customerProfile['loyalty_id'], $customer->getLoyaltyProgram()->getId());
    }
}
