<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\CreditCard as SavedCreditCard;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Partnership;
use Linio\Frontend\Order\Payment\InstallmentOption;
use Linio\Frontend\Order\Payment\InstallmentOptions;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\Payment\PaymentRedirect;
use Linio\Frontend\Order\RecalculatedOrder;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\Packages;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\PickupStores;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Linio\Frontend\Order\Shipping\ShippingQuotes;
use Linio\Frontend\Order\Wallet;
use Linio\Type\Money;
use Symfony\Component\Translation\TranslatorInterface;

class DefaultOrderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromRecalculatedOrder()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'savedCards' => [],
                'shippingAddress' => null,
                'items' => [
                    'ABC-123' => [
                        'sku' => 'ABC-123',
                        'name' => 'Foobar',
                        'description' => 'foo bar baz',
                        'slug' => null,
                        'variationType' => 'default',
                        'seller' => null,
                        'unitPrice' => 0.0,
                        'originalPrice' => 0.0,
                        'paidPrice' => 0.0,
                        'taxAmount' => 0.0,
                        'shippingAmount' => 0.0,
                        'subtotal' => 0,
                        'quantity' => 3,
                        'availableQuantity' => 0,
                        'maxItemsToSell' => 1,
                        'image' => '',
                        'linioPlusLevel' => 0,
                        'linioPlusEnabledQuantity' => 0,
                        'variation' => 'black',
                        'freeShipping' => false,
                        'freeStorePickup' => false,
                        'percentageOff' => 0.0,
                        'deliveredByChristmas' => false,
                        'minimumDeliveryDate' => null,
                        'imported' => false,
                        'oversized' => null,
                    ],
                ],
                'packages' => [
                    1 => [
                        'items' => ['ABC-123' => 3],
                        'shippingQuotes' => [
                            [
                                'shippingMethod' => 'jegue',
                                'fee' => 0.0,
                                'estimatedDeliveryDate' => 'January 1, 2015',
                                'deliveredByChristmas' => false,
                                'selected' => false,
                                'freeShipping' => true,
                                'name' => 'Super Jegue',
                            ],
                        ],
                    ],
                ],
                'undeliverables' => ['ABC-789'],
                'wallet' => [
                    'totalDiscount' => 0.0,
                    'totalPointsUsed' => 0.0,
                    'pointsBalance' => 0.0,
                    'shippingDiscount' => 0.0,
                    'pointsUsedForShipping' => 0.0,
                    'maxPointsForOrder' => 0.0,
                    'conversionRate' => 0.0,
                ],
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'storePickupEnabled' => true,
                'pickupStores' => [
                    '2' => [
                        'id' => 2,
                        'name' => 'Pickup Store 2',
                        'description' => 'Blah blah blah',
                        'postcode' => '33020',
                        'geohash' => 'dhwupu4907e',
                        'region' => 'Region',
                        'municipality' => 'Municipality',
                        'city' => 'City',
                        'subLocality' => 'Sublocality',
                        'neighborhood' => 'Neighborhood',
                        'line1' => 'AddressLine1',
                        'line2' => 'AddressLine2',
                        'streetNumber' => 2,
                        'apartment' => 'Apartment',
                        'referencePoint' => 'ReferencePoint',
                        'network' => [
                            'id' => 1,
                            'name' => 'NetworkName',
                        ],
                        'selected' => false,
                    ],
                ],
                'geohash' => 'geohash',
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $simple = new Simple();
        $simple->setSku('ABC-123');
        $simple->addAttribute('variation', 'black');
        $simple->setHasFreeShipping(false);

        $product = new Product('ABC');
        $product->setName('Foobar');
        $product->setDescription('foo bar baz');
        $product->addSimple($simple);
        $product->setVariationType('default');
        $product->setImported(false);

        $orderItem = new Item('ABC-123', 3, $product);
        $undeliverableOrderItem = new Item('ABC-789', 1, $product);

        $shippingQuote = new ShippingQuote('jegue', new DateTime('2015-01-01'));
        $shippingQuotes = new ShippingQuotes();
        $shippingQuotes->add($shippingQuote);

        $package = new Package();
        $package->setId(1);
        $package->addItem($orderItem);
        $package->setShippingQuotes($shippingQuotes);

        $packages = new Packages();
        $packages->add($package);

        $pickupStore = new PickupStore(2);
        $pickupStore->setName('Pickup Store 2');
        $pickupStore->setDescription('Blah blah blah');
        $pickupStore->setPostcode('33020');
        $pickupStore->setGeohash('dhwupu4907e');
        $pickupStore->setRegion('Region');
        $pickupStore->setMunicipality('Municipality');
        $pickupStore->setCity('City');
        $pickupStore->setSubLocality('Sublocality');
        $pickupStore->setNeighborhood('Neighborhood');
        $pickupStore->setLine1('AddressLine1');
        $pickupStore->setLine2('AddressLine2');
        $pickupStore->setStreetNumber('2');
        $pickupStore->setApartment('Apartment');
        $pickupStore->setReferencePoint('ReferencePoint');
        $pickupStore->setNetworkId(1);
        $pickupStore->setNetworkName('NetworkName');
        $pickupStores = new PickupStores();
        $pickupStores->add($pickupStore);

        $order = new Order('42');
        $order->addItem($orderItem);
        $order->setPackages($packages);
        $order->setCustomer(new Customer());
        $order->setPickupStores($pickupStores);
        $order->enableStorePickup();
        $order->setGeohash('geohash');
        $order->setWallet(new Wallet());

        $recalculatedOrder = new RecalculatedOrder($order);
        $recalculatedOrder->addUndeliverable($undeliverableOrderItem);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('checkout.shipping_methods.jegue')->willReturn('Super Jegue');

        $dateFormatter = $this->prophesize(DateFormatter::class);
        $dateFormatter->formatLocale(new DateTime('2015-01-01'))->willReturn('January 1, 2015');

        $output = new DefaultOrder();
        $output->setTranslator($translator->reveal());
        $output->setDateFormatter($dateFormatter->reveal());
        $apiOutput = $output->fromRecalculatedOrder($recalculatedOrder);
        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingFromEmptyRecalculatedOrder()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => null,
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => 0.0,
                'originalShippingAmount' => 0.0,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'savedCards' => [],
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'storePickupEnabled' => false,
                'pickupStores' => [],
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $output = new DefaultOrder();
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder(new Order('foobar')));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithTranslatedMessages()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => 0.0,
                'originalShippingAmount' => 0.0,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'savedCards' => [],
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [
                    [
                        'installments' => 1,
                        'interestFee' => 0.0,
                        'total' => 0.0,
                        'totalInterest' => 0.0,
                        'amount' => 0.0,
                        'selected' => false,
                        'paymentMethodName' => null,
                        'description' => 'Translated installment. Yay!',
                        'hasBankInterestFee' => false,
                    ],
                ],
                'highestInstallment' => 1,
                'lowestInstallment' => 1,
                'storePickupEnabled' => false,
                'pickupStores' => [],
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [
                    'Translated error. Yay!',
                ],
                'warnings' => [
                    'Translated Warning #1. Yay!',
                    'Translated Warning #2. Yay!',
                ],
                'success' => 'Foobar!',
            ],
        ];

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->transChoice('checkout.installment_option', 1, ['%amount%' => 0, '%total%' => 0, '%interest%' => 'no interest', '%count%' => 1])->willReturn('Translated installment. Yay!');
        $translator->trans('error.key', ['%context%' => 'ABC-123'])->willReturn('Translated error. Yay!');
        $translator->trans('warning1.key', ['%context%' => 'FOO-123'])->willReturn('Translated Warning #1. Yay!');
        $translator->trans('warning2.key', ['%context%' => 'FOO-123'])->willReturn('Translated Warning #2. Yay!');
        $translator->trans('success.key')->willReturn('Foobar!');
        $translator->trans('checkout.without_interest')->willReturn('no interest');

        $moneyFormatter = $this->prophesize(MoneyFormatter::class);
        $moneyFormatter->format(new Money())->willReturn(0);
        $moneyFormatter->format(new Money())->willReturn(0);

        $installmentOption = new InstallmentOption();
        $installmentOptions = new InstallmentOptions();
        $installmentOptions->add($installmentOption);

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setInstallmentOptions($installmentOptions);

        $recalculatedOrder = new RecalculatedOrder($order);
        $recalculatedOrder->setErrors(['ABC-123' => 'error.key']);
        $recalculatedOrder->setWarnings(['FOO-123' => ['warning1.key', 'warning2.key']]);
        $recalculatedOrder->setSuccessMessage('success.key');

        $output = new DefaultOrder();
        $output->setTranslator($translator->reveal());
        $output->setMoneyFormatter($moneyFormatter->reveal());
        $apiOutput = $output->fromRecalculatedOrder($recalculatedOrder);

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithShippingAddress()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'savedCards' => [],
                'shippingAddress' => ['foo' => 'bar'],
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'storePickupEnabled' => false,
                'pickupStores' => [],
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $address = new Address();
        $addressOutput = $this->prophesize(AddressOutput::class);
        $addressOutput->fromAddress($address)->willReturn(['foo' => 'bar']);

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setShippingAddress($address);

        $output = new DefaultOrder();
        $output->setAddressOutput($addressOutput->reveal());
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithPaymentMethods()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => null,
                'originalShippingAmount' => null,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [
                    'MequetrequePay' => [
                        'allowed' => true,
                        'allowedByCoupon' => false,
                        'requireBillingAddress' => false,
                    ],
                    'AdolfinhoPay' => [
                        'allowed' => true,
                        'allowedByCoupon' => false,
                        'requireBillingAddress' => true,
                    ],
                    'TrequePay' => [
                        'allowed' => true,
                        'allowedByCoupon' => true,
                        'requireBillingAddress' => false,
                    ],
                ],
                'paymentMethod' => 'Foobar',
                'creditCardBinNumber' => '123456',
                'savedCards' => [
                    [
                        'id' => 42,
                        'firstDigits' => '471672',
                        'lastDigits' => '8077',
                        'cardholderName' => 'Jaimito El Cartero',
                        'brand' => 'visa',
                        'paymentMethod' => null,
                    ],
                ],
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'storePickupEnabled' => false,
                'pickupStores' => [],
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $normal = PaymentMethodFactory::fromName('MequetrequePay');
        $normal->allow();

        $needsBilling = PaymentMethodFactory::fromName('AdolfinhoPay');
        $needsBilling->allow();
        $needsBilling->requireBillingAddress();

        $allowedByCoupon = PaymentMethodFactory::fromName('TrequePay');
        $allowedByCoupon->allow();
        $allowedByCoupon->allowByCoupon();

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setAvailablePaymentMethods([$normal, $needsBilling, $allowedByCoupon]);

        $savedCard = new SavedCreditCard();
        $savedCard->setId(42);
        $savedCard->setCardholderName('Jaimito El Cartero');
        $savedCard->setCardNumber('4716728873328077');
        $savedCard->setBrand('visa');
        $savedCard->setExpirationDate(new \DateTime('2019-12-22'));

        $creditCard = new CreditCard('Foobar');
        $creditCard->setBinNumber('123456');
        $creditCard->setSavedCards([$savedCard]);
        $order->setPaymentMethod($creditCard);

        $output = new DefaultOrder();
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingWithPartnerships()
    {
        $expectedApiOutput = [
            'order' => [
                'customer' => [
                    'loyaltyProgram' => null,
                    'loyaltyId' => null,
                    'nationalRegistrationNumber' => null,
                    'taxIdentificationNumber' => null,
                ],
                'subTotal' => 0.0,
                'grandTotal' => 0.0,
                'taxAmount' => 0.0,
                'shippingAmount' => 0.0,
                'originalShippingAmount' => 0.0,
                'shippingDiscountAmount' => 0.0,
                'coupon' => null,
                'totalDiscountAmount' => 0.0,
                'linioPlusSavedAmount' => 0.0,
                'loyaltyPointsAccrued' => 0,
                'availablePaymentMethods' => [],
                'paymentMethod' => null,
                'creditCardBinNumber' => null,
                'savedCards' => [],
                'shippingAddress' => null,
                'items' => [],
                'packages' => [],
                'undeliverables' => [],
                'wallet' => null,
                'partnerships' => [
                    [
                        'name' => 'Foobar!',
                        'description' => 'Foobar Foobar!',
                        'code' => 'foobar',
                        'accountNumber' => null,
                        'level' => null,
                        'appliedDiscount' => null,
                        'active' => false,
                    ],
                    [
                        'name' => 'Barfoo!',
                        'description' => 'Barfoo Barfoo!',
                        'code' => 'barfoo',
                        'accountNumber' => '123456',
                        'level' => null,
                        'appliedDiscount' => null,
                        'active' => true,
                    ],
                ],
                'partnershipDiscount' => 0.0,
                'installmentOptions' => [],
                'highestInstallment' => null,
                'lowestInstallment' => null,
                'storePickupEnabled' => false,
                'pickupStores' => [],
                'geohash' => null,
            ],
            'messages' => [
                'errors' => [],
                'warnings' => [],
                'success' => null,
            ],
        ];

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('partnership.foobar.name')->willReturn('Foobar!');
        $translator->trans('partnership.foobar.description')->willReturn('Foobar Foobar!');
        $translator->trans('partnership.barfoo.name')->willReturn('Barfoo!');
        $translator->trans('partnership.barfoo.description')->willReturn('Barfoo Barfoo!');

        $partnershipA = new Partnership('foobar');
        $partnershipB = new Partnership('barfoo', '123456');
        $partnershipB->enable();
        $partnerships = [$partnershipA, $partnershipB];

        $order = new Order('foobar');
        $order->setCustomer(new Customer());
        $order->setPartnerships($partnerships);

        $output = new DefaultOrder();
        $output->setTranslator($translator->reveal());
        $apiOutput = $output->fromRecalculatedOrder(new RecalculatedOrder($order));

        $this->assertEquals($expectedApiOutput, $apiOutput);
    }

    public function testIsOutputtingFromCompletedOrder()
    {
        $expectedApiOutput = [
            'grandTotal' => 0.0,
            'orderNumber' => 'foobar',
            'paymentMethod' => 'MequetrequePay',
            'redirect' => [
                'method' => 'POST',
                'target' => '/foobar',
                'body' => [],
            ],
        ];

        $completedOrder = new CompletedOrder('123');
        $completedOrder->setOrderNumber('foobar');
        $completedOrder->setPaymentMethod(PaymentMethodFactory::fromName('MequetrequePay'));

        $redirect = new PaymentRedirect();
        $redirect->setMethod('POST');
        $redirect->setTarget('/foobar');
        $redirect->setBody([]);
        $completedOrder->setPaymentRedirect($redirect);

        $output = new DefaultOrder();
        $apiOutput = $output->fromCompletedOrder($completedOrder);
        $this->assertEquals($expectedApiOutput, $apiOutput);
    }
}
