<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Test\WishListProductFixture;
use Linio\Frontend\WishList\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class MobileWishListProductTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function productProvider(): array
    {
        $productFixture = new WishListProductFixture();

        return [
            [$productFixture->getProductWithoutImages(), $productFixture->getProductWithoutImagesArray()],
            [$productFixture->getProduct(), $productFixture->getArray()],
            [$productFixture->getInactiveProduct(), $productFixture->getInactiveProductArray()],
        ];
    }

    /**
     * @dataProvider productProvider
     *
     * @param Product $product
     * @param array $productOutput
     */
    public function testIsOutputtingFromWishListProduct(Product $product, array $productOutput)
    {
        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('https');

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $router = $this->prophesize(RouterInterface::class);
        $router->generate('frontend.mobile.catalog.detail.v1', ['productSlug' => 'iphone'], 1)
            ->willReturn('/mapi/v1/p/iphone');

        $output = new MobileWishListProduct();
        $output->setRouter($router->reveal());
        $output->setProductDescriptionFilter(new FilteredProductDescription());
        $output->setRequestStack($requestStack->reveal());

        $actual = $output->fromProduct($product);

        $this->assertEquals($productOutput, $actual);
    }
}
