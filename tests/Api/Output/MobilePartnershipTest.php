<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Customer\Membership\Partnership;
use PHPUnit_Framework_TestCase;

class MobilePartnershipTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function partnershipProvider(): array
    {
        $partnershipDataPoints = [
            [
                'partnership' => [
                    'id' => 123,
                    'code' => 'ABC123',
                    'name' => 'foo',
                ],
                'identifier' => '123fddf',
                'level' => '123',
                'isActive' => true,
                'lastStatusCheck' => '2016-01-01T00:00:00+0000',
            ],
            [
                'partnership' => [
                    'id' => 124,
                    'code' => 'ABC124',
                    'name' => 'barr',
                ],
                'identifier' => '124fddf',
                'level' => 'platinum',
                'isActive' => false,
                'lastStatusCheck' => '2015-01-01T00:00:00+0000',
            ],
            [
                'partnership' => [
                    'id' => 1,
                    'code' => 'clarin',
                    'name' => 'Clarin',
                ],
                'identifier' => null,
                'level' => null,
                'isActive' => null,
                'lastStatusCheck' => null,
            ],
        ];

        $expected = [
            [
                'partnership' => [
                    'code' => $partnershipDataPoints[0]['partnership']['code'],
                    'name' => $partnershipDataPoints[0]['partnership']['name'],
                ],
                'accountNumber' => $partnershipDataPoints[0]['identifier'],
                'level' => $partnershipDataPoints[0]['level'],
                'active' => (bool) $partnershipDataPoints[0]['isActive'],
                'lastStatusCheck' => $partnershipDataPoints[0]['lastStatusCheck'],
            ],
            [
                'partnership' => [
                    'code' => $partnershipDataPoints[1]['partnership']['code'],
                    'name' => $partnershipDataPoints[1]['partnership']['name'],
                ],
                'accountNumber' => $partnershipDataPoints[1]['identifier'],
                'level' => $partnershipDataPoints[1]['level'],
                'active' => (bool) $partnershipDataPoints[1]['isActive'],
                'lastStatusCheck' => $partnershipDataPoints[1]['lastStatusCheck'],
            ],
            [
                'partnership' => [
                    'code' => $partnershipDataPoints[2]['partnership']['code'],
                    'name' => $partnershipDataPoints[2]['partnership']['name'],
                ],
                'accountNumber' => $partnershipDataPoints[2]['identifier'],
                'level' => $partnershipDataPoints[2]['level'],
                'active' => (bool) $partnershipDataPoints[2]['isActive'],
                'lastStatusCheck' => $partnershipDataPoints[2]['lastStatusCheck'],
            ],
        ];

        return [
            [[$this->buildPartnership($partnershipDataPoints[0])], [$expected[0]]],
            [
                [
                    $this->buildPartnership($partnershipDataPoints[0]),
                    $this->buildPartnership($partnershipDataPoints[1]),
                ],
                [$expected[0], $expected[1]],
            ],
            [[$this->buildPartnership($partnershipDataPoints[2])], [$expected[2]]],
            [
                [
                    $this->buildPartnership($partnershipDataPoints[0]),
                    $this->buildPartnership($partnershipDataPoints[2]),
                ],
                [$expected[0], $expected[2]],
            ],
        ];
    }

    /**
     * @dataProvider partnershipProvider
     *
     * @param array $partnerships
     * @param array $expected
     */
    public function testIsOutputtingFromPartnerships(array $partnerships, array $expected)
    {
        $output = new MobilePartnership();

        $actual = $output->fromPartnerships($partnerships);

        $this->assertSame($expected, $actual);
    }

    /**
     * @param array $data
     *
     * @return Partnership
     */
    protected function buildPartnership(array $data): Partnership
    {
        $partnership = new Partnership($data['partnership']['code'], $data['identifier']);
        $partnership->setName($data['partnership']['name']);

        if (isset($data['level'])) {
            $partnership->setLevel($data['level']);
        }

        if (isset($data['lastStatusCheck'])) {
            $partnership->setLastStatusCheck(new DateTime($data['lastStatusCheck']));
        }

        if ($data['isActive'] === true) {
            $partnership->enable();
        }

        return $partnership;
    }
}
