<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\MakeDeliveryStatusFriendly;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Customer\Order\Shipping\TrackingEvent;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Test\CustomerOrderFixture;
use Linio\Type\Date;
use Linio\Type\Money;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\TranslatorInterface;

class MobileCustomerOrderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingOrderObjectAsArray()
    {
        $orderFixtureFactory = new CustomerOrderFixture();
        $results = [];
        for ($o = 0; $o < 3; $o++) {
            $results[] = $orderFixtureFactory->createOrder();
        }

        $paginatedResult = new PaginatedResult();
        $paginatedResult->setCurrent(1);
        $paginatedResult->setPages(1);
        $paginatedResult->setSize(1);
        $paginatedResult->setItemCount(3);
        $paginatedResult->setResult($results);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans(Argument::type('string'))->shouldBeCalled()->willReturn('translated');

        $friendlyStatusTransformer = new MakeDeliveryStatusFriendly();
        $friendlyStatusTransformer->setTranslator($translator->reveal());
        $friendlyStatusTransformer->setTimezone('UTC');

        $dateFormatter = $this->prophesize(DateFormatter::class);
        $dateFormatter->formatLocale(Argument::type(Date::class))->shouldBeCalled()->willReturn('6 de Enero de 2016');

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySkus(Argument::any())->willReturn($orderFixtureFactory->createCacheProducts());

        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('http');
        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $transformer = new MobileCustomerOrder();
        $transformer->setAddressOutput(new MobileAddress());
        $transformer->setTranslator($translator->reveal());
        $transformer->setMakeDeliveryStatusFriendlyService($friendlyStatusTransformer);
        $transformer->setProductService($productService->reveal());
        $transformer->setRequestStack($requestStack->reveal());
        $transformer->setDateFormatter($dateFormatter->reveal());

        $actual = $transformer->fromPaginatedOrderList($paginatedResult);

        $this->assertSame($actual['page'], $paginatedResult->getCurrent());
        $this->assertSame($actual['pageCount'], $paginatedResult->getPages());
        $this->assertSame($actual['ordersPerPage'], $paginatedResult->getSize());
        $this->assertSame($actual['ordersFound'], $paginatedResult->getItemCount());
        $this->assertCount($paginatedResult->getItemCount(), $actual['orders']);

        $sample = rand(0, count($actual['orders']) - 1);
        /** @var Order $expectedOrder */
        $expectedOrder = $paginatedResult->getResult()[$sample];
        $actualOrder = $actual['orders'][$sample];

        $this->assertInternalType('int', $actualOrder['orderId']);
        $this->assertRegExp('/\d{9}/', $actualOrder['orderNumber']);
        $this->assertInternalType('float', $actualOrder['subTotal']);
        $this->assertInternalType('float', $actualOrder['grandTotal']);
        $this->assertInstanceOf(DateTime::class, DateTime::createFromFormat(DATE_ISO8601, $actualOrder['createdAt']));
        $this->assertInternalType('string', $actualOrder['customer']);

        $this->assertSame($expectedOrder->getShipping()->getAmount()->getMoneyAmount(), $actualOrder['shipping']['amount']);
        $this->assertSame($expectedOrder->getShipping()->getDiscount()->getMoneyAmount(), $actualOrder['shipping']['discount']);

        $this->assertSame($expectedOrder->getPayment()->getPaymentMethod()->getName(), $actualOrder['payment']['paymentMethod']['name']);
        $this->assertSame($expectedOrder->getPayment()->getInstallments()->getAmount()->getMoneyAmount(), $actualOrder['payment']['installments']['amount']);

        $this->assertNotEmpty($actualOrder['wallet']);

        $this->assertCount(count($expectedOrder->getPackages()), $actualOrder['packages']);

        $samplePackage = rand(0, count($actualOrder['packages']) - 1);
        $expectedPackage = $expectedOrder->getPackages()[$samplePackage];
        $actualPackage = $actualOrder['packages'][$samplePackage];

        $this->assertSame($expectedPackage->getNumber(), $actualPackage['number']);
        $this->assertSame($expectedPackage->getStatus(), $actualPackage['status']);
        $this->assertSame($expectedPackage->getProgressStep(), $actualPackage['progressStep']);
        $this->assertSame($expectedPackage->getExpectedDeliveryDate()->format(DATE_ISO8601), $actualPackage['expectedDeliveryDate']);
        $this->assertSame($expectedPackage->getUpdatedDeliveryDate()->format(DATE_ISO8601), $actualPackage['updatedDeliveryDate']);
        $this->assertSame($expectedPackage->getDeliveredAt()->format(DATE_ISO8601), $actualPackage['deliveredAt']);
        $this->assertSame($expectedPackage->isReturnAllowed(), $actualPackage['allowReturn']);
        $this->assertSame($expectedPackage->isTrackingAllowed(), $actualPackage['allowTracking']);
        $this->assertSame($expectedPackage->isSellerReviewAllowed(), $actualPackage['allowSellerReview']);

        foreach ($actualPackage['items'] as $status => $items) {
            $this->assertInternalType('string', $status);

            foreach ($items as $item) {
                $this->assertRegExp('/[A-Z0-9]{17}\-\d{5}/', $item['sku']);
                $this->assertInternalType('string', $item['name']);
                $this->assertInternalType('int', $item['quantity']);
                $this->assertInternalType('float', $item['unitPrice']);
                $this->assertSame('translated', $item['seller']);
                $this->assertInstanceOf(DateTime::class, DateTime::createFromFormat(DATE_ISO8601, $item['statusUpdatedAt']));
                $this->assertContains('.jpg', $item['image']);
                $this->assertInternalType('string', $item['expectedDeliveryDate']);
                $this->assertInternalType('string', $item['updatedDeliveryDate']);
            }
        }
    }

    public function testIsOutputtingFromShipments()
    {
        $expected = [
            [
                'trackingCode' => '781846968074',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'FEDEX',
                'updatedAt' => '2015-12-07T21:52:22+0000',
                'items' => [
                    [
                        'product' => 'SKU001-001',
                        'quantity' => 1,
                    ],
                    [
                        'product' => 'SKU001-002',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03T10:26:02+0000'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03T17:58:02+0000'],
                ],
            ],
            [
                'trackingCode' => 'Z7102304920394',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'UPS',
                'updatedAt' => '2015-12-07T21:52:22+0000',
                'items' => [
                    [
                        'product' => 'SKU001-003',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03T10:26:02+0000'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03T17:58:02+0000'],
                ],
            ],
        ];

        $product1 = new Product();
        $product1->setSku('SKU001-001');

        $product2 = new Product();
        $product2->setSku('SKU001-002');

        $product3 = new Product();
        $product3->setSku('SKU001-003');

        $itemsShipping1 = [
            [
                'product' => $product1,
                'quantity' => 1,
            ],
            [
                'product' => $product2,
                'quantity' => 1,
            ],
        ];

        $itemsShipping2 = [
            [
                'product' => $product3,
                'quantity' => 1,
            ],
        ];

        $shipment1 = new Shipment($expected[0]['trackingCode']);
        $shipment1->setTrackingUrl($expected[0]['trackingUrl']);
        $shipment1->setCarrier($expected[0]['carrier']);
        $shipment1->setUpdatedAt(new DateTime($expected[0]['updatedAt']));
        $shipment1->setItems($itemsShipping1);
        $shipment1->setTrackingEvents([
            new TrackingEvent(
                $expected[0]['events'][0]['code'],
                $expected[0]['events'][0]['description'],
                new DateTime($expected[0]['events'][0]['createdAt'])
            ),
            new TrackingEvent(
                $expected[0]['events'][1]['code'],
                $expected[0]['events'][1]['description'],
                new DateTime($expected[0]['events'][1]['createdAt'])
            ),
        ]);

        $shipment2 = new Shipment($expected[1]['trackingCode']);
        $shipment2->setTrackingUrl($expected[1]['trackingUrl']);
        $shipment2->setCarrier($expected[1]['carrier']);
        $shipment2->setUpdatedAt(new DateTime($expected[1]['updatedAt']));
        $shipment2->setItems($itemsShipping2);
        $shipment2->setTrackingEvents([
            new TrackingEvent(
                $expected[1]['events'][0]['code'],
                $expected[1]['events'][0]['description'],
                new DateTime($expected[1]['events'][0]['createdAt'])
            ),
            new TrackingEvent(
                $expected[1]['events'][1]['code'],
                $expected[1]['events'][1]['description'],
                new DateTime($expected[1]['events'][1]['createdAt'])
            ),
        ]);

        $shipments = [$shipment1, $shipment2];

        $output = new MobileCustomerOrder();

        $actual = $output->fromShipments($shipments);

        $this->assertSame($expected, $actual);
    }

    public function testIsOutputtingFromPendingOrders()
    {
        $expected = [
            [
                'id' => 12423,
                'orderNumber' => '348372938923',
                'purchaseDate' => '01 de Enero de 2016',
                'grandTotal' => 1300.0,
                'status' => ['status1'],
                'productCount' => 2,
            ],
        ];

        $item1 = new Item('ASKJDSASDDSDS-HJKD2');
        $item2 = new Item('SDADSLJSADJKL-LSOW2');

        $dateFormatter = $this->prophesize(DateFormatter::class);
        $dateFormatter->formatLocale(new DateTime('01/01/2016'))
            ->shouldBeCalled()
            ->willReturn('01 de Enero de 2016');

        $pendingOrder = new Order($expected[0]['id']);
        $pendingOrder->setOrderNumber($expected[0]['orderNumber']);
        $pendingOrder->setCreatedAt(new DateTime('01/01/2016'));
        $pendingOrder->setGrandTotal(new Money($expected[0]['grandTotal']));
        $pendingOrder->setStatusProgressConfiguration($expected[0]['status']);
        $pendingOrder->setItems([$item1, $item2]);

        $output = new MobileCustomerOrder();
        $output->setDateFormatter($dateFormatter->reveal());

        $actual = $output->fromPendingOrders([$pendingOrder]);

        $this->assertSame($expected, $actual);
    }
}
