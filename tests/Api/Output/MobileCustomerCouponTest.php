<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Customer\Coupon;
use Linio\Type\Money;

class MobileCustomerCouponTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function couponsDataProvider(): array
    {
        $expectedResultActive = [];
        $expectedResultInactive = [];

        $couponActive[] = new Coupon();
        $couponActive[0]->setCode('TEST1d3');
        $couponActive[0]->setActive(true);
        $couponActive[0]->setValidFrom(DateTime::createFromFormat('Y-m-d H:i:s', '2013-04-26 00:00:00'));
        $couponActive[0]->setValidUntil(DateTime::createFromFormat('Y-m-d H:i:s', '2016-04-26 00:00:00'));
        $couponActive[0]->setDiscountType(Coupon::DISCOUNT_TYPE_PERCENT);
        $couponActive[0]->setDiscountPercentage(25.0);

        $expectedResultActive['code'] = 'TEST1d3';
        $expectedResultActive['isActive'] = true;
        $expectedResultActive['validFrom'] = '2013-04-26T00:00:00+0000';
        $expectedResultActive['validUntil'] = '2016-04-26T00:00:00+0000';
        $expectedResultActive['discountType'] = Coupon::DISCOUNT_TYPE_PERCENT;
        $expectedResultActive['discountPercentage'] = 25.0;
        $expectedResultActive['amountAvailable'] = null;
        $expectedResultActive['amount'] = null;

        $couponInactive[] = new Coupon();
        $couponInactive[0]->setCode('OTHEr3');
        $couponInactive[0]->setActive(false);
        $couponInactive[0]->setValidFrom(DateTime::createFromFormat('Y-m-d H:i:s', '2015-01-15 00:00:00'));
        $couponInactive[0]->setValidUntil(DateTime::createFromFormat('Y-m-d H:i:s', '2017-08-24 00:00:00'));
        $couponInactive[0]->setDiscountType(Coupon::DISCOUNT_TYPE_PERCENT);
        $couponInactive[0]->setDiscountPercentage(15.0);

        $expectedResultInactive['code'] = 'OTHEr3';
        $expectedResultInactive['isActive'] = false;
        $expectedResultInactive['validFrom'] = '2015-01-15T00:00:00+0000';
        $expectedResultInactive['validUntil'] = '2017-08-24T00:00:00+0000';
        $expectedResultInactive['discountType'] = Coupon::DISCOUNT_TYPE_PERCENT;
        $expectedResultInactive['discountPercentage'] = 15.0;
        $expectedResultInactive['amountAvailable'] = null;
        $expectedResultInactive['amount'] = null;

        $couponMoney[] = new Coupon();
        $couponMoney[0]->setCode('M0N3Y');
        $couponMoney[0]->setActive(true);
        $couponMoney[0]->setValidFrom(DateTime::createFromFormat('Y-m-d H:i:s', '2015-01-15 00:00:00'));
        $couponMoney[0]->setValidUntil(DateTime::createFromFormat('Y-m-d H:i:s', '2017-08-24 00:00:00'));
        $couponMoney[0]->setDiscountType(Coupon::DISCOUNT_TYPE_MONEY);
        $couponMoney[0]->setBalance(new Money(0.5));
        $couponMoney[0]->setAmount(new Money(1.0));

        $expectedResultMoney['code'] = 'M0N3Y';
        $expectedResultMoney['isActive'] = true;
        $expectedResultMoney['validFrom'] = '2015-01-15T00:00:00+0000';
        $expectedResultMoney['validUntil'] = '2017-08-24T00:00:00+0000';
        $expectedResultMoney['discountType'] = Coupon::DISCOUNT_TYPE_MONEY;
        $expectedResultMoney['discountPercentage'] = null;
        $expectedResultMoney['amountAvailable'] = 0.5;
        $expectedResultMoney['amount'] = 1.0;

        return [
            [$couponActive, $expectedResultActive],
            [$couponInactive, $expectedResultInactive],
            [$couponMoney, $expectedResultMoney],
        ];
    }

    /**
     * @dataProvider couponsDataProvider
     *
     * @param array $coupon
     * @param array $expectedResult
     */
    public function testIsOutputtingCouponObjectAsArray(array $coupon, array $expectedResult)
    {
        $output = new MobileCustomerCoupon();
        $result = $output->fromCoupons($coupon);

        $this->assertEquals($expectedResult['code'], $result[0]['code']);
        $this->assertEquals($expectedResult['isActive'], $result[0]['isActive']);
        $this->assertEquals($expectedResult['validFrom'], $result[0]['validFrom']);
        $this->assertEquals($expectedResult['validUntil'], $result[0]['validUntil']);
        $this->assertEquals($expectedResult['discountType'], $result[0]['discountType']);
        $this->assertEquals($expectedResult['discountPercentage'], $result[0]['discountPercentage']);
        $this->assertEquals($expectedResult['amountAvailable'], $result[0]['amountAvailable']);
        $this->assertEquals($expectedResult['amount'], $result[0]['amount']);
    }
}
