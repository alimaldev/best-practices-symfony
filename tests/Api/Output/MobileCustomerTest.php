<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\LinioPlusMembership;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Seller;
use Linio\Type\Money;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class MobileCustomerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingFromCustomer()
    {
        $expected = [
            'id' => '19283',
            'linio_id' => 'l1n1oId',
            'first_name' => 'Zuriel',
            'last_name' => 'Alcántara',
            'gender' => 'male',
            'bornDate' => '1989-12-20 00:00:00',
            'email' => 'zuriel@linio.com',
            'tax_identification_number' => '328472983749',
            'national_registration_number' => '203402840924',
            'is_linio_plus' => true,
            'loyalty_program' => null,
            'loyalty_id' => null,
        ];

        $customer = new Customer();
        $customer->setId($expected['id']);
        $customer->setLinioId($expected['linio_id']);
        $customer->setFirstName($expected['first_name']);
        $customer->setLastName($expected['last_name']);
        $customer->setGender($expected['gender']);
        $customer->setBornDate(new DateTime($expected['bornDate']));
        $customer->setEmail($expected['email']);
        $customer->setTaxIdentificationNumber($expected['tax_identification_number']);
        $customer->setNationalRegistrationNumber($expected['national_registration_number']);
        $customer->setSubscribedToLinioPlus($expected['is_linio_plus']);

        $output = new MobileCustomer();
        $actual = $output->fromCustomer($customer);

        $this->assertEquals($expected, $actual);
    }

    public function testIsOutputtingFromCustomerSubscribedToLoyaltyProgram()
    {
        $expected = [
            'id' => '19283',
            'linio_id' => 'l1n1oId',
            'first_name' => 'Zuriel',
            'last_name' => 'Alcántara',
            'gender' => 'male',
            'bornDate' => '1989-12-20 00:00:00',
            'email' => 'zuriel@linio.com',
            'tax_identification_number' => '328472983749',
            'national_registration_number' => '203402840924',
            'loyalty_program' => 'lanpass',
            'loyalty_id' => '2340234029',
            'is_linio_plus' => true,
        ];

        $program = new LoyaltyProgram();
        $program->setId($expected['loyalty_id']);
        $program->setName($expected['loyalty_program']);

        $customer = new Customer();
        $customer->setId($expected['id']);
        $customer->setLinioId($expected['linio_id']);
        $customer->setFirstName($expected['first_name']);
        $customer->setLastName($expected['last_name']);
        $customer->setGender($expected['gender']);
        $customer->setBornDate(new DateTime($expected['bornDate']));
        $customer->setEmail($expected['email']);
        $customer->setTaxIdentificationNumber($expected['tax_identification_number']);
        $customer->setNationalRegistrationNumber($expected['national_registration_number']);
        $customer->setSubscribedToLinioPlus($expected['is_linio_plus']);
        $customer->setLoyaltyProgram($program);

        $output = new MobileCustomer();
        $actual = $output->fromCustomer($customer);

        $this->assertEquals($expected, $actual);
    }

    public function testIsOutputtingFromPlusMembership()
    {
        $expected = [
            'id' => 123,
            'reference' => 'ABC',
            'isActive' => true,
            'balance' => 131.23,
            'startDate' => '2016-01-01T00:00:00+0000',
            'endDate' => '2016-01-01T00:00:00+0000',
            'canceledAt' => '2016-01-01T00:00:00+0000',
            'walletSubscriptionId' => 1,
            'createdAt' => '2016-01-01T00:00:00+0000',
            'updatedAt' => '2016-01-01T00:00:00+0000',
        ];

        $customer = new Customer();
        $customer->setId(1);

        $linioPlusMembership = new LinioPlusMembership(
            $expected['id'],
            $customer->getId(),
            new DateTime($expected['startDate']),
            new DateTime($expected['endDate']),
            $expected['isActive']
        );
        $linioPlusMembership->setReference($expected['reference']);

        $money = new Money($expected['balance']);

        $linioPlusMembership->setBalance($money);
        $linioPlusMembership->setWalletSubscription($expected['walletSubscriptionId']);
        $linioPlusMembership->setLastUpdated(new DateTime($expected['updatedAt']));
        $linioPlusMembership->setCancellationDate(new DateTime($expected['canceledAt']));
        $linioPlusMembership->setCreateDate(new DateTime($expected['createdAt']));

        $output = new MobileCustomer();
        $actual = $output->fromPlusMembership($linioPlusMembership);

        $this->assertEquals($expected, $actual);
    }

    public function testIsOutputtingFromPaginatedCustomerReviews()
    {
        $customerReviews = [
            '1' => [
                'orderNumber' => '200400600',
                'datePurchase' => '2016-07-28T00:00:00+0000',
                'sellers' => [
                    '10' => [
                        'name' => 'Seller Name',
                        'slug' => 'seller-slug',
                        'products' => [
                            [
                                'sku' => 'SKUTEST-32HD',
                                'name' => 'Product Name',
                                'slug' => 'product-slug',
                                'imageUrl' => 'http://media.linio.com.mx/p/product-name-3200-1-product.jpg',
                            ],
                        ],
                        'review' => [
                            'title' => 'Review title',
                            'detail' => 'Details seller review',
                            'rating' => 5,
                        ],
                    ],
                ],
            ],
        ];

        $expected = [
            'currentPage' => 1,
            'totalPages' => 1,
            'customerReviews' => $customerReviews,
        ];

        $reviewsByOrder = [];

        $review = new SellerReview('Review title', 'Details seller review', 5);

        $seller = new Seller();
        $seller->setSellerId(10);
        $seller->setName('Seller Name');
        $seller->setSlug('seller-slug');

        $reviewsByOrder[1]['sellers'][10]['review'] = $review;
        $reviewsByOrder[1]['sellers'][10]['name'] = 'Seller Name';
        $reviewsByOrder[1]['sellers'][10]['seller'] = $seller;

        $image1 = new Image();
        $image1->setMain(true);
        $image1->setSlug('//media.linio.com.mx/p/product-name-3200-1');

        $image2 = new Image();
        $image2->setMain(false);
        $image2->setSlug('//media.linio.com.mx/p/product-name-3200-2');

        $product = new Product('SKUTEST-32HD');
        $product->setName('Product Name');
        $product->setSlug('product-slug');
        $product->setImages([$image1, $image2]);

        $reviewsByOrder[1]['sellers'][10]['products'][0] = $product;

        $reviewsByOrder[1]['orderNumber'] = '200400600';
        $reviewsByOrder[1]['orderDate'] = new DateTime('2016-07-28 00:00:00');

        $paginatedResult = new PaginatedResult();
        $paginatedResult->setCurrent(1);
        $paginatedResult->setItemCount(2);
        $paginatedResult->setPages(1);
        $paginatedResult->setSize(2);
        $paginatedResult->setResult($reviewsByOrder);

        $request = $this->prophesize(Request::class);
        $request->getScheme()->willReturn('http');

        $requestStack = $this->prophesize(RequestStack::class);
        $requestStack->getCurrentRequest()->willReturn($request->reveal());

        $output = new MobileCustomer();
        $output->setRequestStack($requestStack->reveal());
        $actual = $output->fromPaginatedCustomerReviews($paginatedResult);

        $this->assertEquals($expected, $actual);
    }
}
