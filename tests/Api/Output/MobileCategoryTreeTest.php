<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;

class MobileCategoryTreeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function categoryProvider(): array
    {
        $categoryOutput = [
            'id' => '343',
            'name' => 'Celulares y tablets',
            'slug' => 'celulares-y-tablets',
            'urlKey' => 'celulares-y-tablets',
            'isCurrent' => true,
            'count' => 15,
            'path' => [
                [
                    'id' => '5433',
                    'name' => 'Celulares y smartphones',
                    'slug' => 'celulares-y-smartphones',
                    'urlKey' => 'celulares-y-smartphones',
                    'isCurrent' => false,
                    'count' => 5,
                    'path' => [],
                    'children' => [],
                ],
            ],
            'children' => [
                '2343' => [
                    'id' => '2343',
                    'name' => 'Tablets',
                    'slug' => 'tablets',
                    'urlKey' => 'tablets',
                    'isCurrent' => false,
                    'count' => 10,
                    'path' => [],
                    'children' => [],
                ],
            ],
        ];

        $child = new Category();
        $child->setId(2343);
        $child->setName('Tablets');
        $child->setSlug('tablets');
        $child->setUrlKey('tablets');
        $child->setIsCurrent(false);
        $child->setCount(10);

        $pathItem = new Category();
        $pathItem->setId(5433);
        $pathItem->setName('Celulares y smartphones');
        $pathItem->setSlug('celulares-y-smartphones');
        $pathItem->setUrlKey('celulares-y-smartphones');
        $pathItem->setIsCurrent(false);
        $pathItem->setCount(5);

        $category = new Category();
        $category->setId(343);
        $category->setName('Celulares y tablets');
        $category->setSlug('celulares-y-tablets');
        $category->setUrlKey('celulares-y-tablets');
        $category->setIsCurrent(true);
        $category->setCount(15);
        $category->setPath([$pathItem]);
        $category->setChildren([$child]);

        return [
            [$category, $categoryOutput],
        ];
    }

    public function campaignProvider()
    {
        $campaignExpected = [
                'id' => '19',
                'name' => 'Outlet',
                'slug' => 'outlet',
                'urlKey' => null,
                'skus' => [
                    'PL327ME0HLDP4LAEC',
                    'XX380HL1M9ABCLAEC',
                ],
                'filters' => [
                    'SO008ME07PNOLAEC' => [
                        'SO008ME17KAWLAEC',
                    ],
                ],
                'isCurrent' => true,
                'count' => 2,
                'path' => [
                    [
                        'id' => '19',
                        'name' => 'Outlet',
                        'slug' => 'outlet',
                        'urlKey' => null,
                        'skus' => [
                            'PL327ME0HLDP4LAEC',
                            'XX380HL1M9ABCLAEC',
                        ],
                        'filters' => [
                            'SO008ME07PNOLAEC' => [
                                'SO008ME17KAWLAEC',
                            ],
                        ],
                        'isCurrent' => true,
                        'count' => 2,
                        'path' => [],
                        'children' => [],
                    ],
                ],
                'children' => [],
        ];

        $campaign = new Campaign();
        $campaign->setId(19);
        $campaign->setName('Outlet');
        $campaign->setSlug('outlet');
        $campaign->setCount(2);

        $campaign->setSkus([
            'PL327ME0HLDP4LAEC',
            'XX380HL1M9ABCLAEC',
        ]);

        $campaign->setFilters([
            'SO008ME07PNOLAEC' => [
                'SO008ME17KAWLAEC',
            ],
        ]);

        $campaign->setIsCurrent(true);
        $campaign->setPath([$campaign]);

        return [
            [$campaign, $campaignExpected],
        ];
    }

    /**
     * @dataProvider categoryProvider
     *
     * @param Category $category
     * @param array $categoryOutput
     */
    public function testIsOutputtingFromCategoryTree(Category $category, array $categoryOutput)
    {
        $output = new MobileCategoryTree();
        $actual = $output->fromCategory($category);

        $this->assertEquals($categoryOutput, $actual);
    }

    /**
     * @dataProvider campaignProvider
     *
     * @param Campaign $campaign
     * @param array $campaignOutput
     */
    public function testIsOutputtingFromCampaignTree(Campaign $campaign, array $campaignOutput)
    {
        $output = new MobileCategoryTree();
        $actual = $output->fromCampaign($campaign);

        $this->assertEquals($campaignOutput, $actual);
    }
}
