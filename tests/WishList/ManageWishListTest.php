<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList;

use DateTime;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product as CatalogProduct;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\WishList\Communication\WishListAdapter;
use Linio\Frontend\WishList\Product as ProductWishList;
use Linio\Type\Money;

class ManageWishListTest extends \PHPUnit_Framework_TestCase
{
    public function testIsCreatingAWishList()
    {
        $customer = new Customer();
        $customer->setId(1);

        $wishList = new WishList('Birthday', $customer);

        $expectedWishList = new WishList('Birthday', $customer);
        $expectedWishList->setId(10);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->createWishList($wishList)
            ->shouldBeCalled()
            ->willReturn($expectedWishList);

        $skuCache = $this->prophesize(CacheService::class);

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());
        $actual = $service->createWishList($wishList);

        $this->assertEquals($expectedWishList, $actual);
    }

    public function testIsEditingAWishList()
    {
        $cachedSimple = new Simple();
        $cachedSimple->setSku('iph0n3-3431');

        $cachedProduct = new CatalogProduct('iph0n3');
        $cachedProduct->setSlug('iphone');
        $cachedProduct->setPrice(Money::fromCents(10000));
        $cachedProduct->setName('iPhone');
        $cachedProduct->addSimple($cachedSimple);

        $simple = new Simple();
        $simple->setSku('iph0n3-3431');

        $product = new ProductWishList($simple);
        $product->setWishListId(1);
        $product->setSlug('iphone');
        $product->setSku('iph0n3');
        $product->setName('iPhone');
        $product->setPrice(Money::fromCents(10000));
        $product->setWishListPrice(Money::fromCents(10000));
        $product->setAddedOn(new DateTime('2016-04-24 12:00:00'));

        $wishListOwner = new Customer();
        $wishListOwner->setId(10);

        $wishList = new WishList('Birthday', $wishListOwner);
        $wishList->addProduct($product);

        $expectedWishList = clone $wishList;

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySkus(['iph0n3'])
            ->shouldBeCalled()
            ->willReturn([$cachedProduct]);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->editWishList($wishList)
            ->shouldBeCalled()
            ->willReturn($expectedWishList);

        $skuCache = $this->prophesize(CacheService::class);

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());
        $service->setProductService($productService->reveal());

        $actual = $service->editWishList($wishList);

        $this->assertEquals($expectedWishList, $actual);
    }

    public function testIsGettingWishLists()
    {
        $cachedSimple = new Simple();
        $cachedSimple->setSku('iph0n3-3431');

        $cachedProduct = new CatalogProduct('iph0n3');
        $cachedProduct->setSlug('iphone');
        $cachedProduct->setPrice(Money::fromCents(10000));
        $cachedProduct->setName('iPhone');
        $cachedProduct->addSimple($cachedSimple);

        $simple = new Simple();
        $simple->setSku('iph0n3-3431');

        $product = new ProductWishList($simple);
        $product->setWishListId(1);
        $product->setSlug('iphone');
        $product->setSku('iph0n3');
        $product->setName('iPhone');
        $product->setPrice(Money::fromCents(10000));
        $product->setWishListPrice(Money::fromCents(10000));
        $product->setAddedOn(new DateTime('2016-04-24 12:00:00'));

        $wishListOwner = new Customer();
        $wishListOwner->setId(10);

        $wishList = new WishList('Birthday', $wishListOwner);
        $wishList->addProduct($product);

        $expectedWishList = clone $wishList;

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySkus(['iph0n3'])
            ->shouldBeCalled()
            ->willReturn([$cachedProduct]);

        $adapter = $this->prophesize(WishListAdapter::class);

        $adapter->getWishLists($wishListOwner)
            ->shouldBeCalled()
            ->willReturn([$expectedWishList]);

        $skuCache = $this->prophesize(CacheService::class);

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());
        $service->setProductService($productService->reveal());

        $actual = $service->getWishLists($wishListOwner);

        $this->assertContainsOnlyInstancesOf(WishList::class, $actual);
    }

    public function testIsGettingWishList()
    {
        $wishListOwner = new Customer();
        $wishListOwner->setId(10);

        // Entity sent to service
        $inputWishList = new WishList('', $wishListOwner);
        $inputWishList->setId(10);

        // Entity returned from Adapter
        $adapterSimple1 = new Simple();
        $adapterSimple1->setSku('iph0n3-6plus');

        $adapterSimple2 = new Simple();
        $adapterSimple2->setSku('M4cBo0k-air');

        $adapterProduct1 = new Product($adapterSimple1);
        $adapterProduct1->setSku('iph0n3');
        $adapterProduct1->setName('iPhone');
        $adapterProduct1->setWishListId(1);
        $adapterProduct1->setWishListPrice(Money::fromCents(110000));
        $adapterProduct1->setAddedOn(new DateTime('2016-04-24 12:00:00'));

        $adapterProduct2 = new ProductWishList($adapterSimple2);
        $adapterProduct2->setWishListId(2);
        $adapterProduct2->setSku('M4cBo0k');
        $adapterProduct2->setName('MacBookPro');
        $adapterProduct2->setWishListPrice(Money::fromCents(22000));
        $adapterProduct2->setAddedOn(new DateTime('2016-04-25 12:00:00'));

        $adapterWishList = new WishList('Birthday', $wishListOwner);
        $adapterWishList->setId(10);
        $adapterWishList->setVisibility(WishList::VISIBILITY_PRIVATE);
        $adapterWishList->setCreatedAt(new DateTime('2016-04-24 11:00:00'));
        $adapterWishList->setDescription('My birthday greed.');

        $adapterWishList->addProduct($adapterProduct1);
        $adapterWishList->addProduct($adapterProduct2);

        // Entities returned from cache
        $catalogSimple = new Simple();
        $catalogSimple->setSku('iph0n3-6plus');

        $catalogProduct = new CatalogProduct('iph0n3');
        $catalogProduct->setSlug('iphone');
        $catalogProduct->setPrice(Money::fromCents(100000));
        $catalogProduct->setOriginalPrice(Money::fromCents(110000));
        $catalogProduct->setName('iPhone');

        // Set up test doubles
        $productService = $this->prophesize(ProductService::class);

        $productService->getBySkus(['iph0n3', 'M4cBo0k'])
            ->shouldBeCalled()
            ->willReturn([$catalogProduct]);

        $adapter = $this->prophesize(WishListAdapter::class);

        $adapter->getWishList($inputWishList)
            ->shouldBeCalled()
            ->willReturn($adapterWishList);

        $skuCache = $this->prophesize(CacheService::class);

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());
        $service->setProductService($productService->reveal());

        $actual = $service->getWishList($inputWishList);

        // Expected entities
        $expectedWishList = clone $adapterWishList;
        $expectedWishList->setProducts([]);

        $expectedIPhone = clone $adapterProduct1;
        $expectedIPhone->activate();
        $expectedIPhone->setSlug($catalogProduct->getSlug());
        $expectedIPhone->setOriginalPrice($catalogProduct->getOriginalPrice());
        $expectedIPhone->setPrice($catalogProduct->getPrice());
        $expectedIPhone->setName($catalogProduct->getName());

        $expectedWishList->addProduct($adapterProduct2);
        $expectedWishList->addProduct($expectedIPhone);

        $this->assertEquals($expectedWishList, $actual);
    }

    public function testCallsSetDefaultWishList()
    {
        $wishListOwner = new Customer();
        $wishList = new WishList('Birthday', $wishListOwner);

        $adapter = $this->prophesize(WishListAdapter::class);

        $adapter->setDefaultWishList($wishList)
            ->shouldBeCalled();

        $skuCache = $this->prophesize(CacheService::class);

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());

        $service->setDefaultWishList($wishList);
    }

    public function testCallsDeleteWishList()
    {
        $customer = new Customer();
        $customer->setId(1);

        $wishList = new WishList('Default', $customer);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->deleteWishList($wishList)->shouldBeCalled();

        $skuCache = $this->prophesize(CacheService::class);
        $skuCache->delete((string) $customer->getId())->shouldBeCalled();

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());

        $service->deleteWishList($wishList);
    }

    public function testIsAddingProductToWishList()
    {
        $simpleWishList = new Simple();

        $product = new ProductWishList($simpleWishList);
        $product->setWishListId(1);
        $product->setSlug('iphone');
        $product->setSku('iph0n3');
        $product->setName('iPhone');
        $product->setPrice(Money::fromCents(10000));
        $product->setWishListPrice(Money::fromCents(10000));
        $product->setAddedOn(new DateTime('2016-04-24 12:00:00'));

        $customer = new Customer();
        $customer->setId(1);

        $wishList = new WishList('NameWishList', $customer);
        $wishList->addProduct($product);

        $expectedWishList = clone $wishList;

        $product2 = new ProductWishList($simpleWishList);
        $product2->setWishListId(1);
        $product2->setSlug('macBook');
        $product2->setSku('macBook');
        $product2->setName('macBook');
        $product2->setPrice(Money::fromCents(100000));
        $product2->setWishListPrice(Money::fromCents(100000));
        $product2->setAddedOn(new DateTime('2016-04-24 12:00:00'));

        $expectedWishList->addProduct($product2);

        $productService = $this->prophesize(ProductService::class);

        $catalogProduct = new CatalogProduct('iph0n3');
        $catalogProduct->setSlug('iphone');
        $catalogProduct->setPrice(Money::fromCents(10000));
        $catalogProduct->setName('iPhone');

        $productService->getBySkus(['iph0n3', 'macBook'])
            ->shouldBeCalled()
            ->willReturn([$catalogProduct]);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->addProductToWishList($wishList, $product2)
            ->shouldBeCalled()
            ->willReturn($expectedWishList);

        $skuCache = $this->prophesize(CacheService::class);
        $skuCache->delete((string) $customer->getId())->shouldBeCalled();

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());
        $service->setProductService($productService->reveal());
        $actual = $service->addProductToWishList($wishList, $product2);

        $this->assertEquals($expectedWishList, $actual);
    }

    public function testCallsRemoveProductFromWishList()
    {
        $simpleWishList = new Simple();

        $product = new ProductWishList($simpleWishList);

        $customer = new Customer();
        $customer->setId(1);

        $wishList = new WishList('NameWishList', $customer);
        $wishList->addProduct($product);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->removeProductFromWishList($wishList, $product)
            ->shouldBeCalled();

        $skuCache = $this->prophesize(CacheService::class);
        $skuCache->delete((string) $customer->getId())->shouldBeCalled();

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());

        $service->removeProductFromWishList($wishList, $product);
    }

    public function testItCallsGetSkusInWishListsWhenCacheIsEmpty()
    {
        $customer = new Customer();
        $customer->setId(1);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->getAllSkusInWishLists($customer)->shouldBeCalled()->willReturn(['SKU1234']);

        $skuCache = $this->prophesize(CacheService::class);
        $skuCache->get((string) $customer->getId())->shouldBeCalled()->willReturn(null);
        $skuCache->set((string) $customer->getId(), ['SKU1234'])->shouldBeCalled();

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());

        $actual = $service->getAllSkusInWishLists($customer);

        $this->assertSame(['SKU1234'], $actual);
    }

    public function testItGetsAllSkusInWishListsFromCache()
    {
        $customer = new Customer();
        $customer->setId(1);

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->getAllSkusInWishLists($customer)->shouldNotBeCalled();

        $skuCache = $this->prophesize(CacheService::class);
        $skuCache->get((string) $customer->getId())->shouldBeCalled()->willReturn(['SKU1234']);
        $skuCache->set((string) $customer->getId(), ['SKU1234'])->shouldNotBeCalled();

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());

        $actual = $service->getAllSkusInWishLists($customer);

        $this->assertSame(['SKU1234'], $actual);
    }

    public function testItGetsNoSkusInWishListsWhenCustomerIsAGuest()
    {
        $customer = new GuestUser();

        $adapter = $this->prophesize(WishListAdapter::class);
        $adapter->getAllSkusInWishLists($customer)->shouldNotBeCalled();

        $skuCache = $this->prophesize(CacheService::class);
        $skuCache->get((string) $customer->getId())->shouldNotBeCalled();
        $skuCache->set((string) $customer->getId(), ['SKU1234'])->shouldNotBeCalled();

        $service = new ManageWishList($adapter->reveal(), $skuCache->reveal());

        $actual = $service->getAllSkusInWishLists($customer);

        $this->assertEmpty($actual);
    }
}
