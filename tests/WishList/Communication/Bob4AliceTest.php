<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication;

use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Linio\Component\Util\Json;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\Test\CommunicationTestCase;
use Linio\Frontend\WishList\Communication\Input\Bob4Alice as Input;
use Linio\Frontend\WishList\Communication\Input\WishListInput;
use Linio\Frontend\WishList\Communication\Output\Bob4Alice as Output;
use Linio\Frontend\WishList\Communication\Output\WishListOutput;
use Linio\Frontend\WishList\Exception\InvalidWishListException;
use Linio\Frontend\WishList\Exception\WishListException;
use Linio\Frontend\WishList\Exception\WishListNotFoundException;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;
use Linio\Type\Money;

class Bob4AliceTest extends CommunicationTestCase
{
    public function testIsCreatingWishList()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
        ];

        $responseBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '4c3ss',
            'wishlistId' => 23,
            'createdAt' => '2007-05-05 13:24:15',
            'isDefault' => false,
        ];

        $owner = new Customer();
        $owner->setId($requestBody['customerId']);
        $owner->setFirstName('Otis');
        $owner->setLastName('Redding');
        $owner->setEmail('otis_redding@linio.com');

        $wishList = new WishList($requestBody['name'], $owner);
        $wishList->setVisibility($requestBody['visibility']);
        $wishList->setDescription($requestBody['description']);
        $wishList->setAccessHash($requestBody['accessHash']);

        $response = new Response(200, [], Json::encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/wishlist/create', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice(
            $client->reveal(),
            new Input(),
            new Output($platformInformation)
        );
        $actual = $adapter->createWishList($wishList);

        $this->assertInstanceOf(WishList::class, $actual);
        $this->assertEquals($owner, $actual->getOwner());
        $this->assertEquals($responseBody['name'], $actual->getName());
        $this->assertEquals($responseBody['wishlistId'], $actual->getId());
        $this->assertEquals($responseBody['visibility'], $actual->getVisibility());
        $this->assertEquals($responseBody['description'], $actual->getDescription());
        $this->assertEquals(new DateTime($responseBody['createdAt']), $actual->getCreatedAt());
    }

    public function testIsThrowingExceptionOnClientErrorWhenCreatingAWishList()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
        ];

        $wishList = new WishList('Great wish list.', new Customer());

        $input = $this->prophesize(WishListInput::class);
        $input->fromWishListDetail($wishList)
            ->shouldNotBeCalled();

        $output = $this->prophesize(WishListOutput::class);
        $output->toCreateWishList($wishList)
            ->shouldBeCalled()
            ->willReturn($requestBody);

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            Json::encode($responseBody),
            '/wishlist/create'
        );

        $adapter = new Bob4Alice(
            $client,
            $input->reveal(),
            $output->reveal()
        );

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->createWishList($wishList);
    }

    public function testIsThrowingExceptionOnServerErrorWhenCreatingAWishList()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
        ];

        $wishList = new WishList('Great wish list.', new Customer());

        $input = $this->prophesize(WishListInput::class);
        $input->fromWishListDetail($wishList)
            ->shouldNotBeCalled();

        $output = $this->prophesize(WishListOutput::class);
        $output->toCreateWishList($wishList)
            ->shouldBeCalled()
            ->willReturn($requestBody);

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            Json::encode($responseBody),
            '/wishlist/create'
        );

        $adapter = new Bob4Alice(
            $client,
            $input->reveal(),
            $output->reveal()
        );

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->createWishList($wishList);
    }

    public function testIsEditingAWishList()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
            'wishlistId' => 23,
        ];

        $responseBody = $requestBody;
        $responseBody['accessHash'] = '4c3ss';
        $responseBody['isDefault'] = false;

        $owner = new Customer();
        $owner->setId($requestBody['customerId']);
        $owner->setFirstName('Otis');
        $owner->setLastName('Redding');
        $owner->setEmail($requestBody['ownerEmail']);

        $wishList = new WishList($requestBody['name'], $owner);
        $wishList->setId($requestBody['wishlistId']);
        $wishList->setVisibility($requestBody['visibility']);
        $wishList->setDescription($requestBody['description']);
        $wishList->setAccessHash($requestBody['accessHash']);

        $response = new Response(200, [], Json::encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/wishlist/edit', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice(
            $client->reveal(),
            new Input(),
            new Output($platformInformation)
        );
        $actual = $adapter->editWishList($wishList);

        $this->assertInstanceOf(WishList::class, $actual);
        $this->assertEquals($owner, $actual->getOwner());
        $this->assertEquals($responseBody['name'], $actual->getName());
        $this->assertEquals($responseBody['wishlistId'], $actual->getId());
        $this->assertEquals($responseBody['visibility'], $actual->getVisibility());
        $this->assertEquals($responseBody['description'], $actual->getDescription());
    }

    public function testIsReturningErrorWhenWishListEditCallResponseIsInvalidRequest()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
            'wishlistId' => 23,
        ];

        $wishList = new WishList('Great wish list.', new Customer());

        $input = $this->prophesize(WishListInput::class);
        $input->fromWishListDetail($wishList)
            ->shouldNotBeCalled();

        $output = $this->prophesize(WishListOutput::class);
        $output->toEdit($wishList)
            ->shouldBeCalled()
            ->willReturn($requestBody);

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            Json::encode($responseBody),
            '/wishlist/edit'
        );

        $adapter = new Bob4Alice(
            $client,
            $input->reveal(),
            $output->reveal()
        );

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->editWishList($wishList);
    }

    public function testIsReturningWishListNotFoundErrorWhenEditingAWishList()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
            'wishlistId' => 0,
        ];

        $wishList = new WishList('Great wish list.', new Customer());

        $input = $this->prophesize(WishListInput::class);
        $input->fromWishListDetail($wishList)
            ->shouldNotBeCalled();

        $output = $this->prophesize(WishListOutput::class);
        $output->toEdit($wishList)
            ->shouldBeCalled()
            ->willReturn($requestBody);

        $responseBody = [
            'code' => 'ENTITY_NOT_FOUND',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            Json::encode($responseBody),
            '/wishlist/edit'
        );

        $adapter = new Bob4Alice(
            $client,
            $input->reveal(),
            $output->reveal()
        );

        $this->expectException(WishListNotFoundException::class);
        $this->expectExceptionMessage('ENTITY_NOT_FOUND');

        $adapter->editWishList($wishList);
    }

    public function testIsThrowingExceptionOnServerErrorWhenEditingAWishList()
    {
        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'visibility' => 'private',
            'name' => 'Great wish list.',
            'description' => 'A wish list for my friends.',
            'ownerName' => 'Otis Redding',
            'ownerEmail' => 'otis_redding@linio.com',
            'accessHash' => '',
            'wishlistId' => 23,
        ];

        $wishList = new WishList('Great wish list.', new Customer());

        $input = $this->prophesize(WishListInput::class);
        $input->fromWishListDetail($wishList)
            ->shouldNotBeCalled();

        $output = $this->prophesize(WishListOutput::class);
        $output->toEdit($wishList)
            ->shouldBeCalled()
            ->willReturn($requestBody);

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            Json::encode($responseBody),
            '/wishlist/edit'
        );

        $adapter = new Bob4Alice(
            $client,
            $input->reveal(),
            $output->reveal()
        );

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->editWishList($wishList);
    }

    public function testIsDeletingWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/wishlist/delete', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $adapter->deleteWishList($wishList);
    }

    public function testIsThrowingWishListExceptionOnClientErrorWhenDeletingWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            Json::encode($responseBody),
            '/wishlist/delete'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->deleteWishList($wishList);
    }

    public function testIsThrowingWishListExceptionOnServerErrorWhenDeletingWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            Json::encode($responseBody),
            '/wishlist/delete'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->deleteWishList($wishList);
    }

    public function testIsThrowingWishListNotFoundExceptionOnClientErrorWhenDeletingWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $responseBody = [
            'code' => 'ENTITY_NOT_FOUND',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            Json::encode($responseBody),
            '/wishlist/delete'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListNotFoundException::class);
        $this->expectExceptionMessage('ENTITY_NOT_FOUND');

        $adapter->deleteWishList($wishList);
    }

    public function testIsSettingDefaultWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/wishlist/default', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $adapter->setDefaultWishList($wishList);
    }

    public function testIsThrowingWishListExceptionOnClientErrorWhenSettingDefaultWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(Json::encode($responseBody), '/wishlist/default');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->setDefaultWishList($wishList);
    }

    public function testIsThrowingWishListExceptionOnServerErrorWhenSettingDefaultWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(Json::encode($responseBody), '/wishlist/default');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->setDefaultWishList($wishList);
    }

    public function testIsThrowingWishListNotFoundExceptionOnClientErrorWhenSettingDefaultWishList()
    {
        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];

        $responseBody = [
            'code' => 'ENTITY_NOT_FOUND',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(Json::encode($responseBody), '/wishlist/default');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListNotFoundException::class);
        $this->expectExceptionMessage('ENTITY_NOT_FOUND');

        $adapter->setDefaultWishList($wishList);
    }

    public function testCallsWishListDetailWithSuccessfulResponse()
    {
        $bob4AliceExpectedResponse = Json::encode(
            [
                'wishlistId' => 10,
                'visibility' => 'private',
                'isDefault' => false,
                'name' => 'christmas',
                'description' => 'My Christmas list',
                'ownerName' => 'Anka Gonzales',
                'ownerEmail' => 'customer-501@linio.com',
                'accessHash' => '',
                'createdAt' => '2016-04-24 00:00:00',
                'products' => [
                    [
                        'wishlistItemId' => 2,
                        'simpleSku' => 'ABC234-AB24',
                        'configSku' => 'ABC234',
                        'name' => 'iPhone',
                        'price' => '4000',
                        'createdAt' => '2016-04-25 00:00:00',
                    ],
                ],
            ]
        );

        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
            'wishlistId' => 65,
        ];

        $wishList = new WishList('', $customer);
        $wishList->setId(65);

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/wishlist/detail', ['json' => $wishListData])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], $bob4AliceExpectedResponse));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $wishListResult = $adapter->getWishList($wishList, $customer);

        $this->assertInstanceOf(WishList::class, $wishListResult);
    }

    public function testCallsWishListDetailWithEntityNotFoundError()
    {
        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
            'wishlistId' => 65,
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'ENTITY_NOT_FOUND',
                'message' => 'Wishlist not found. [wishlistId] = [' . $wishListData['wishlistId'] . '], [customerId] = [' . $wishListData['customerId'] . '], [storeId] = [' . $wishListData['storeId'] . ']',
                'errors' => [],
            ]
        );

        $wishList = new WishList('', $customer);
        $wishList->setId(65);

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            $bob4AliceExpectedResponse,
            '/wishlist/detail'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListNotFoundException::class);
        $this->expectExceptionMessage('ENTITY_NOT_FOUND');

        $adapter->getWishList($wishList, $customer);
    }

    public function testCallsWishListDetailWithInvalidRequestError()
    {
        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
            'wishlistId' => 65,
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'INVALID_REQUEST',
                'message' => 'ERROR',
                'errors' => [],
            ]
        );

        $wishList = new WishList('', $customer);
        $wishList->setId(65);

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            $bob4AliceExpectedResponse,
            '/wishlist/detail'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->getWishList($wishList, $customer);
    }

    public function testCallsWishListDetailWithServerError()
    {
        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
            'wishlistId' => 65,
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'AN_ERROR_HAS_OCCURRED',
                'message' => '',
                'errors' => [],
            ]
        );

        $wishList = new WishList('', $customer);
        $wishList->setId(65);

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            $bob4AliceExpectedResponse,
            '/wishlist/detail'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->getWishList($wishList, $customer);
    }

    public function testCallsWishListListWithSuccessfulResponse()
    {
        $bob4AliceExpectedResponse = Json::encode(
            [
                [
                    'wishlistId' => 65,
                    'visibility' => 'private',
                    'isDefault' => true,
                    'name' => 'Quiero esto',
                    'description' => null,
                    'ownerName' => 'Cristopher Peña',
                    'ownerEmail' => 'cisaacpr@hotmail.com',
                    'accessHash' => '',
                    'createdAt' => '2015-06-23 19:31:59',
                ],
            ]
        );

        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/wishlist/list', ['json' => $wishListData])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], $bob4AliceExpectedResponse));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $wishListResult = $adapter->getWishLists($customer);

        $this->assertInstanceOf(WishList::class, $wishListResult[0]);
    }

    public function testCallsWishListListWithInvalidRequestError()
    {
        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'INVALID_REQUEST',
                'message' => 'ERROR',
                'errors' => [],
            ]
        );

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            $bob4AliceExpectedResponse,
            '/wishlist/list'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->getWishLists($customer);
    }

    public function testCallsWishListListWithServerError()
    {
        $customer = new Customer();
        $customer->setId(2244);

        $wishListData = [
            'storeId' => 1,
            'customerId' => 2244,
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'AN_ERROR_HAS_OCCURRED',
                'message' => '',
                'errors' => [],
            ]
        );

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            $bob4AliceExpectedResponse,
            '/wishlist/list'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListData['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->getWishLists($customer);
    }

    public function testIsRemovingProductOnWishList()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-1234567');
        $product = new Product($simple);

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/wishlist/remove-product', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $adapter->removeProductFromWishList($wishList, $product);
    }

    public function testIsThrowingWishListExceptionOnClientErrorWhenRemovingProductWishList()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-1234567');
        $product = new Product($simple);

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            Json::encode($responseBody),
            '/wishlist/remove-product'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->removeProductFromWishList($wishList, $product);
    }

    public function testIsThrowingWishListExceptionOnServerErrorWhenRemovingProductWishList()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-1234567');
        $product = new Product($simple);

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            Json::encode($responseBody),
            '/wishlist/remove-product'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->removeProductFromWishList($wishList, $product);
    }

    public function testCallsRemoveProductFromWishListWithEntityNotFoundError()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-1234567');
        $product = new Product($simple);

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'ENTITY_NOT_FOUND',
                'message' => sprintf('Error when adding a product to a wishlist. [wishlistId] = [%s], [simpleSku] = [%s]', $requestBody['wishlistId'], $requestBody['simpleSku']),
                'errors' => [],
            ]
        );

        $wishList = new WishList('', $customer);
        $wishList->setId(65);

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            $bob4AliceExpectedResponse,
            '/wishlist/remove-product'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListNotFoundException::class);
        $this->expectExceptionMessage('ENTITY_NOT_FOUND');

        $adapter->removeProductFromWishList($wishList, $product);
    }

    public function testIsAddingWishListProduct()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-1234567');

        $product = new Product($simple);
        $product->setName('iPhone 5');
        $product->setWishListPrice(Money::fromCents(10000));

        $secondSimple = new Simple();
        $secondSimple->setSku('ABCDFGHIJKL-7654321');
        $secondProduct = new Product($secondSimple);
        $secondProduct->setName('Android');
        $secondProduct->setWishListPrice(Money::fromCents(5000));

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);
        $wishList->addProduct($product);

        $expectedWishList = clone $wishList;
        $expectedWishList->addProduct($secondProduct);

        $bob4AliceExpectedResponse = Json::encode(['wishlistItemId' => 872]);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $secondProduct->getSelectedSimple()->getSku(),
            'configSku' => $secondProduct->getSelectedSimple()->getConfigSku(),
            'name' => $secondProduct->getName(),
            'price' => $secondProduct->getWishListPrice()->getAmount(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/wishlist/add-product', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], $bob4AliceExpectedResponse));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $actual = $adapter->addProductToWishList($wishList, $secondProduct);

        $this->assertEquals($expectedWishList, $actual);
    }

    public function testCallsAddProductToWishListWithEntityNotFoundError()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-7654321');
        $product = new Product($simple);
        $product->setName('Android');
        $product->setWishListPrice(Money::fromCents(5000));

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
        ];

        $bob4AliceExpectedResponse = Json::encode(
            [
                'code' => 'ENTITY_NOT_FOUND',
                'message' => 'Wishlist not found. [wishlistId] = [' . $wishList->getId() . ']',
                'errors' => [],
            ]
        );

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            $bob4AliceExpectedResponse,
            '/wishlist/add-product'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListNotFoundException::class);
        $this->expectExceptionMessage('ENTITY_NOT_FOUND');
        $adapter->addProductToWishList($wishList, $product);
    }

    public function testCallsAddProductToWishListWithWishListInvalidError()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-7654321');
        $product = new Product($simple);
        $product->setName('Android');
        $product->setWishListPrice(Money::fromCents(5000));

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
        ];

        $responseBody = Json::encode([
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ]);

        $client = $this->createMockBob4AliceClientThatThrowsClientException(
            $responseBody,
            '/wishlist/add-product'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');
        $adapter->addProductToWishList($wishList, $product);
    }

    public function testCallsAddProductToWishListWithServerError()
    {
        $simple = new Simple();
        $simple->setSku('ABCDFGHIJKL-7654321');
        $product = new Product($simple);
        $product->setName('Android');
        $product->setWishListPrice(Money::fromCents(5000));

        $customer = new Customer();
        $customer->setId(2);

        $wishList = new WishList('wishList', $customer);
        $wishList->setId(5);

        $requestBody = [
            'storeId' => 1,
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
            'configSku' => $product->getSelectedSimple()->getConfigSku(),
            'name' => $product->getName(),
            'price' => $product->getWishListPrice()->getAmount(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(
            Json::encode($responseBody),
            '/wishlist/add-product'
        );

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');
        $adapter->addProductToWishList($wishList, $product);
    }

    public function testIsGettingAllSkusInWishLists()
    {
        $customer = new Customer();
        $customer->setId(1);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'SKU1234',
            'SKU12345',
        ];

        $response = new Response(200, [], json_encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/wishlist/skus', ['json' => $requestBody])->shouldBeCalled()->willReturn($response);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client->reveal(), new Input(), new Output($platformInformation));

        $actual = $adapter->getAllSkusInWishLists($customer);

        $this->assertSame($responseBody, $actual);
    }

    public function testIsHandlingAClientErrorWhileGettingAllSkusInWishLists()
    {
        $customer = new Customer();
        $customer->setId(1);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(json_encode($responseBody), '/wishlist/skus');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(InvalidWishListException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->getAllSkusInWishLists($customer);
    }

    public function testIsHandlingAServerErrorWhileGettingAllSkusInWishLists()
    {
        $customer = new Customer();
        $customer->setId(1);

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(json_encode($responseBody), '/wishlist/skus');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice($client, new Input(), new Output($platformInformation));

        $this->expectException(WishListException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->getAllSkusInWishLists($customer);
    }
}
