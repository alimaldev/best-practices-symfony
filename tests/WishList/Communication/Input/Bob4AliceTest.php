<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication\Input;

use DateTime;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

class Bob4AliceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingWishListFromWishListData()
    {
        $wishListData = [
            'wishlistId' => 20,
            'visibility' => 'public',
            'isDefault' => true,
            'name' => 'Birthday',
            'description' => 'My Birthday list',
            'ownerName' => 'Anka Gonzales',
            'ownerEmail' => 'customer-501@linio.com',
            'accessHash' => '',
            'createdAt' => '2015-03-20 00:00:00',
            'products' => [
                [
                    'wishlistItemId' => 2,
                    'simpleSku' => 'MACBOOK-AB24',
                    'configSku' => 'MACBOOK',
                    'name' => 'MacBook',
                    'price' => '450.00',
                    'createdAt' => '2015-03-20 00:00:00',
                ],
            ],
        ];

        $customer = new Customer();
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzales');
        $customer->setEmail('customer-501@linio.com');

        $input = new Bob4Alice();
        $wishList = $input->fromWishListDetail($wishListData, $customer);

        /** @var Product $product */
        $product = array_values($wishList->getProducts())[0];

        $this->assertContains($wishList->getVisibility(), ['public', 'private']);
        $this->assertSame($wishListData['description'], $wishList->getDescription());
        $this->assertContains($wishList->getOwner()->getFirstName(), $wishListData['ownerName']);
        $this->assertContains($wishList->getOwner()->getLastName(), $wishListData['ownerName']);
        $this->assertSame($wishListData['ownerEmail'], $wishList->getOwner()->getEmail());
        $this->assertSame($wishListData['createdAt'], $wishList->getCreatedAt()->format('Y-m-d H:i:s'));
        $this->assertInternalType('string', $wishList->getAccessHash());
        $this->assertTrue($wishList->isDefault());
        $this->assertCount(1, $wishList->getProducts());
        $this->assertArrayHasKey($wishListData['products'][0]['configSku'], $wishList->getProducts());
        $this->assertEquals($product->getWishListId(), $wishListData['products'][0]['wishlistItemId']);
        $this->assertEquals($product->getSelectedSimple()->getSku(), $wishListData['products'][0]['simpleSku']);
        $this->assertEquals($product->getSku(), $wishListData['products'][0]['configSku']);
        $this->assertEquals($product->getName(), $wishListData['products'][0]['name']);
        $this->assertEquals($product->getWishListPrice()->getMoneyAmount(), $wishListData['products'][0]['price']);
        $this->assertEquals($product->getAddedOn(), new DateTime($wishListData['products'][0]['createdAt']));
    }

    public function testGettingWishListFromWishListsArray()
    {
        $wishListsData[] = [
            'wishlistId' => 10,
            'visibility' => 'private',
            'isDefault' => false,
            'name' => 'christmas',
            'description' => 'My Christmas list',
            'ownerName' => 'Anka Gonzales',
            'ownerEmail' => 'customer-501@linio.com',
            'accessHash' => '',
            'createdAt' => '2016-04-24 00:00:00',
            'products' => [
                [
                    'wishlistItemId' => 2,
                    'simpleSku' => 'ABC234-AB24',
                    'configSku' => 'ABC234',
                    'name' => 'iPhone',
                    'price' => '4000',
                    'createdAt' => '2016-04-25 00:00:00',
                ],
            ],
        ];
        $wishListsData[] = [
            'wishlistId' => 20,
            'visibility' => 'public',
            'isDefault' => true,
            'name' => 'Birthday',
            'description' => 'My Birthday list',
            'ownerName' => 'Anka Gonzales',
            'ownerEmail' => 'customer-501@linio.com',
            'accessHash' => '',
            'createdAt' => '2015-03-20 00:00:00',
            'products' => [
                [
                    'wishlistItemId' => 2,
                    'simpleSku' => 'MACBOOK-AB24',
                    'configSku' => 'MACBOOK',
                    'name' => 'MacBook',
                    'price' => '45000',
                    'createdAt' => '2015-03-20 00:00:00',
                ],
            ],
        ];

        $customer = new Customer();
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzales');
        $customer->setEmail('customer-501@linio.com');

        $input = new Bob4Alice();
        $wishLists = $input->fromWishLists($wishListsData, $customer);

        foreach ($wishLists as $wishList) {
            $this->assertInstanceOf(WishList::class, $wishList);
        }
    }

    public function testIsAddingWishListProductFromWishListData()
    {
        $customer = new Customer();
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzales');
        $customer->setEmail('customer-501@linio.com');

        $wishList = new WishList('christmas', $customer);
        $wishList->setVisibility('private');
        $wishList->setDefault(false);
        $wishList->setDescription('My Christmas list');
        $wishList->setAccessHash('');
        $wishList->setCreatedAt(new DateTime('2016-04-24 00:00:00'));
        $countProducts = count($wishList->getProducts());

        $wishListData['wishlistItemId'] = 4;

        $simple = new Simple();
        $product = new Product($simple);

        $input = new Bob4Alice();
        $input->fromAddWishListProduct($wishListData, $wishList, $product);

        $this->assertGreaterThan($countProducts, $wishList->getProducts());
    }

    public function testGettingWishListWithEmptyProductsFromAddWishListProductArray()
    {
        $customer = new Customer();
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzales');
        $customer->setEmail('customer-501@linio.com');

        $wishList = new WishList('christmas', $customer);
        $wishList->setVisibility('private');
        $wishList->setDefault(false);
        $wishList->setDescription('My Christmas list');
        $wishList->setAccessHash('');
        $wishList->setCreatedAt(new DateTime('2016-04-24 00:00:00'));

        $simple = new Simple();
        $product = new Product($simple);

        $input = new Bob4Alice();
        $input->fromAddWishListProduct([], $wishList, $product);

        $this->assertInstanceOf(WishList::class, $wishList);
        $this->assertEmpty($wishList->getProducts());
    }

    public function wishListWithoutProductsDataProvider(): array
    {
        $wishListEmptyProductsData = [
            'wishlistId' => 10,
            'visibility' => 'private',
            'isDefault' => false,
            'name' => 'christmas',
            'description' => 'My Christmas list',
            'ownerName' => 'Anka Gonzales',
            'ownerEmail' => 'customer-501@linio.com',
            'accessHash' => '',
            'createdAt' => '2016-04-24 00:00:00',
            'products' => [],
        ];

        $customer = new Customer();
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzales');
        $customer->setEmail('customer-501@linio.com');

        $wishListNoProductsKeyData = $wishListEmptyProductsData;
        unset($wishListNoProductsKeyData['products']);

        return [
            [$wishListEmptyProductsData, $customer],
            [$wishListNoProductsKeyData, $customer],
        ];
    }

    /**
     * @dataProvider wishListWithoutProductsDataProvider
     *
     * @param array $wishListData
     * @param Customer $customer
     */
    public function testIsTransformingWishListDataWithEmptyProductsToWishListWithEmptyProducts(array $wishListData, Customer $customer)
    {
        $input = new Bob4Alice();
        $wishList = $input->fromWishListDetail($wishListData, $customer);

        $this->assertInstanceOf(WishList::class, $wishList);
        $this->assertEmpty($wishList->getProducts());
    }
}
