<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication\Output;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;
use Linio\Type\Money;

class Bob4AliceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOutputtingToCreateWishList()
    {
        $wishListOutput = [
            'storeId' => 1,
            'customerId' => 502,
            'visibility' => 'private',
            'name' => 'christmas',
            'description' => 'My Christmas wish list',
            'ownerName' => 'Anka Gonzalez',
            'ownerEmail' => 'customer-502@linio.com',
            'accessHash' => '',
        ];

        $customer = new Customer();
        $customer->setId($wishListOutput['customerId']);
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzalez');
        $customer->setEmail($wishListOutput['ownerEmail']);

        $wishList = new WishList($wishListOutput['name'], $customer);
        $wishList->setVisibility($wishListOutput['visibility']);
        $wishList->setDescription($wishListOutput['description']);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListOutput['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toCreateWishList($wishList);

        $this->assertEquals($wishListOutput, $actual);
    }

    public function testIsOutputtingToEditWishList()
    {
        $wishListOutput = [
            'storeId' => 1,
            'customerId' => 502,
            'visibility' => 'private',
            'name' => 'christmas',
            'description' => 'My Christmas wish list',
            'ownerName' => 'Anka Gonzalez',
            'ownerEmail' => 'customer-502@linio.com',
            'accessHash' => '',
            'wishlistId' => 24,
        ];

        $customer = new Customer();
        $customer->setId($wishListOutput['customerId']);
        $customer->setFirstName('Anka');
        $customer->setLastName('Gonzalez');
        $customer->setEmail($wishListOutput['ownerEmail']);

        $wishList = new WishList($wishListOutput['name'], $customer);
        $wishList->setId($wishListOutput['wishlistId']);
        $wishList->setVisibility($wishListOutput['visibility']);
        $wishList->setDescription($wishListOutput['description']);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListOutput['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toEdit($wishList);

        $this->assertEquals($wishListOutput, $actual);
    }

    public function testIsOutputtingToWishListIdentifier()
    {
        $wishListIdentifierOutput = [
            'storeId' => 1,
            'customerId' => 502,
            'wishlistId' => 3,
        ];

        $customer = new Customer();
        $customer->setId($wishListIdentifierOutput['customerId']);

        $wishList = new WishList('christmas', $customer);
        $wishList->setId($wishListIdentifierOutput['wishlistId']);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListIdentifierOutput['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toWishListIdentifier($wishList);

        $this->assertEquals($wishListIdentifierOutput, $actual);
    }

    public function testIsOutputtingToWishListCollection()
    {
        $wishListsCollectionOutput = [
            'storeId' => 1,
            'customerId' => 502,
        ];

        $customer = new Customer();
        $customer->setId($wishListsCollectionOutput['customerId']);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($wishListsCollectionOutput['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toWishListCollection($customer);

        $this->assertEquals($wishListsCollectionOutput, $actual);
    }

    public function testIsOutputtingToAddWishListProduct()
    {
        $addWishListProductOutput = [
            'storeId' => 1,
            'customerId' => 1,
            'wishlistId' => 2,
            'simpleSku' => 'abc1234-bd42',
            'configSku' => 'abc1234',
            'name' => 'iPhone',
            'price' => 40000,
        ];

        $simple = new Simple();
        $simple->setSku($addWishListProductOutput['simpleSku']);

        $wishListProduct = new Product($simple);
        $wishListProduct->setName($addWishListProductOutput['name']);
        $wishListProduct->setSelectedSimple($simple);
        $wishListProduct->setWishListPrice(Money::fromCents($addWishListProductOutput['price']));

        $customer = new Customer();
        $customer->setId(1);

        $wishList = new WishList('christmas', $customer);
        $wishList->setId($addWishListProductOutput['wishlistId']);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($addWishListProductOutput['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toAddWishListProduct($wishList, $wishListProduct);

        $this->assertEquals($addWishListProductOutput, $actual);
    }

    public function testIsOutputtingToRemoveWishListProduct()
    {
        $removeWishListProductOutput = [
            'storeId' => 1,
            'wishlistId' => 2,
            'simpleSku' => 'abc1234-bd42',
        ];

        $simple = new Simple();
        $simple->setSku($removeWishListProductOutput['simpleSku']);

        $wishListProduct = new Product($simple);

        $wishList = new WishList('christmas', new Customer());
        $wishList->setId($removeWishListProductOutput['wishlistId']);

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($removeWishListProductOutput['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toRemoveWishListProduct($wishList, $wishListProduct);

        $this->assertEquals($removeWishListProductOutput, $actual);
    }

    public function testIsOutputtingToGetAllSkusInWishLists()
    {
        $customer = new Customer();
        $customer->setId(1);

        $expected = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($expected['storeId']);

        $output = new Bob4Alice($platformInformation);
        $actual = $output->toGetAllSkusInWishLists($customer);

        $this->assertSame($expected, $actual);
    }
}
