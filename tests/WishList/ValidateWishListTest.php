<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList;

use Prophecy\Argument;
use Symfony\Component\Form\FormBuilderInterface;

class ValidateWishListTest extends \PHPUnit_Framework_TestCase
{
    public function testIsFormBuild()
    {
        $builderFormMock = $this->prophesize(FormBuilderInterface::class);
        $builderFormMock->add('name', Argument::any(), Argument::any())->willReturn($builderFormMock);
        $builderFormMock->add('description', Argument::any(), Argument::any())->willReturn($builderFormMock);
        $builderFormMock->add('default', Argument::any(), Argument::any())->willReturn($builderFormMock);

        $wishlistForm = new ValidateWishList();

        $wishlistForm->buildForm($builderFormMock->reveal(), []);
    }
}
