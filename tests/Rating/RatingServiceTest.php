<?php

namespace Linio\Frontend\Rating;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Product\Review\Vote;
use Linio\Frontend\Entity\Product\Review\VoteSummary;

class RatingServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGetBySkuReturningFalseWithProductWithoutRating()
    {
        $skuFake = 'FAKESKU2469';

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with(RatingService::CACHE_PREFIX . $skuFake)
            ->willReturn(false);

        $ratingService = new RatingService();
        $ratingService->setCacheService($cacheServiceMock);

        $response = $ratingService->getBySku($skuFake);
        $this->assertFalse($response);
    }

    /**
     * @return array
     */
    public function providerProductRatingData()
    {
        $sku = 'FAKESKU2469';
        $data = [
            'title' => 'title of review',
            'detail' => 'text of detail',
            'nickname' => 'nickname',
            'created_at' => date('Y-m-d h:i:s'),
            'options' => [
                [
                    'type_code' => 'option1',
                    'type_title' => 'option title 1',
                    'option_value' => rand(1, 5),
                ],
                [
                    'type_code' => 'option2',
                    'type_title' => 'option title 2',
                    'option_value' => rand(1, 5),
                ],
                [
                    'type_code' => 'option3',
                    'type_title' => 'option title 3',
                    'option_value' => rand(1, 5),
                ],
            ],
        ];
        $data = [$data];

        $review = new Review();
        $review->setTitle($data[0]['title']);
        $review->setComment($data[0]['detail']);
        $review->setCustomerName($data[0]['nickname']);
        $review->setCreatedAt($data[0]['created_at']);

        $vote = new Vote();
        $vote->setType('option1');
        $vote->setTitle('vote title');
        $vote->setValue(rand(1, 5));

        $review->setVotes([$vote]);

        $rating = new Rating();
        $rating->setAveragePoint(1);
        $rating->setReviewsQuantity(1);
        $rating->setReviews([$review]);

        $voteSummary = new VoteSummary();
        $voteSummary->setStars(rand(1, 5));
        $voteSummary->setVotes(rand(10, 20));
        $rating->setVoteSummary([$voteSummary]);

        return [
            [$sku, $data, $rating],
        ];
    }

    /**
     * @dataProvider providerProductRatingData
     *
     * @param string $sku
     * @param array  $data
     * @param Rating $rating
     */
    public function testIsGetBySkuReturningRating($sku, $data, $rating)
    {
        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with(RatingService::CACHE_PREFIX . $sku)
            ->willReturn($data);

        $ratingMapperMock = $this->createMock(RatingMapper::class);
        $ratingMapperMock->expects($this->once())
            ->method('map')
            ->with($data)
            ->willReturn($rating);

        $ratingService = new RatingService();
        $ratingService->setCacheService($cacheServiceMock);
        $ratingService->setRatingMapper($ratingMapperMock);

        $response = $ratingService->getBySku($sku);

        $this->assertInstanceOf(Rating::class, $response);
    }
}
