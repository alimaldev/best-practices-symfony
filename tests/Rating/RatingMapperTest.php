<?php

declare(strict_types=1);

namespace Linio\Frontend\Rating;

use DateTime;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Product\Review\Vote;
use Linio\Frontend\Entity\Product\Review\VoteSummary;

class RatingMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function providerProductRatingData()
    {
        $data = [
            'average_point' => 4,
            'vote_summary' => [
                [
                    'stars' => 5,
                    'votes' => 1,
                ],
                [
                    'stars' => 4,
                    'votes' => 2,
                ],
            ],
            'reviews' => [
                [
                    'title' => 'title of review',
                    'detail' => 'text of detail',
                    'nickname' => 'nickname',
                    'created_at' => date('Y-m-d h:i:s'),
                    'experience_average' => 4,
                    'confirmed_purchase' => 0,
                    'is_customer' => true,
                    'votes' => [
                        [
                            'type_code' => 'option1',
                            'type_title' => 'option title 1',
                            'option_value' => 3,
                        ],
                    ],
                ],
            ],
        ];

        $review = new Review();
        $review->setTitle($data['reviews'][0]['title']);
        $review->setComment($data['reviews'][0]['detail']);
        $review->setCustomerName($data['reviews'][0]['nickname']);
        $review->setCreatedAt(new DateTime($data['reviews'][0]['created_at']));
        $review->setExperienceAverage(4);
        $review->setConfirmedPurchase((bool) $data['reviews'][0]['confirmed_purchase']);
        $review->setCustomerVerified(true);

        $vote = new Vote();
        $vote->setType($data['reviews'][0]['votes'][0]['type_code']);
        $vote->setTitle($data['reviews'][0]['votes'][0]['type_title']);
        $vote->setValue($data['reviews'][0]['votes'][0]['option_value']);

        $review->setVotes([$vote]);

        $rating = new Rating();
        $rating->setAveragePoint(4.5);
        $rating->setReviewsQuantity(1);
        $rating->setReviews([$review]);

        $voteSummary5 = new VoteSummary();
        $voteSummary5->setStars(5);
        $voteSummary5->setVotes(1);
        $voteSummary4 = new VoteSummary();
        $voteSummary4->setStars(4);
        $voteSummary4->setVotes(1);
        $voteSummary3 = new VoteSummary();
        $voteSummary3->setStars(3);
        $voteSummary3->setVotes(0);
        $voteSummary2 = new VoteSummary();
        $voteSummary2->setStars(2);
        $voteSummary2->setVotes(0);
        $voteSummary1 = new VoteSummary();
        $voteSummary1->setStars(1);
        $voteSummary1->setVotes(0);
        $rating->setVoteSummary([$voteSummary1, $voteSummary2, $voteSummary3, $voteSummary4, $voteSummary5]);

        return [
            [$data, $rating],
        ];
    }

    /**
     * @dataProvider providerProductRatingData
     *
     * @param array  $data
     * @param Rating $rating
     */
    public function testIsMapperWorking(array $data, Rating $rating)
    {
        $ratingMapper = new RatingMapper();
        $response = $ratingMapper->map($data);

        $this->assertInstanceOf(Rating::class, $response);
        $this->assertEquals($rating, $response);
    }
}
