<?php

namespace tests\Product;

use DateInterval;
use DateTime;
use Linio\Frontend\Product\ProductHelper;
use Linio\Type\Money;

class ProductHelperTest extends \PHPUnit_Framework_TestCase
{
    public function priceData()
    {
        $today = new DateTime();

        $oneDayAgo = clone $today;
        $oneDayAgo->sub(new DateInterval('P1D'));

        $twoDaysAgo = clone $today;
        $twoDaysAgo->sub(new DateInterval('P2D'));

        $oneDayAhead = clone $today;
        $oneDayAhead->add(new DateInterval('P2D'));

        $twoDaysAhead = clone $today;
        $twoDaysAhead->add(new DateInterval('P2D'));

        return [
            [new Money(1), new Money(1), new Money(2), $twoDaysAgo, $oneDayAgo],
            [new Money(2), new Money(1), new Money(2), $twoDaysAgo, $today],
            [new Money(2), new Money(1), new Money(2), $today, $today],
            [new Money(2), new Money(1), new Money(2), $today, $oneDayAhead],
            [new Money(1), new Money(1), new Money(2), $oneDayAhead, $twoDaysAhead],
            [new Money(1), new Money(1), null, null, null],
            [new Money(1), new Money(1), null, $today, null],
            [new Money(2), new Money(1), new Money(2), $twoDaysAgo, null],
            [new Money(2), new Money(1), new Money(2), $oneDayAgo, null],
            [new Money(2), new Money(1), new Money(2), $today, null],
            [new Money(1), new Money(1), new Money(2), $oneDayAhead, null],
            [new Money(1), new Money(1), new Money(2), $twoDaysAhead, null],
            [new Money(1), new Money(1), new Money(2), null, $today],
        ];
    }

    /**
     * @dataProvider priceData
     *
     * @param Money $expected
     * @param Money $price
     * @param Money|null
     * @param $specialFromDate
     * @param $specialToDate
     */
    public function testIsReturningTheRightPrice(Money $expected, Money $price, $specialPrice, $specialFromDate, $specialToDate)
    {
        $this->assertEquals($expected, ProductHelper::getPrice($price, $specialPrice, $specialFromDate, $specialToDate));
    }
}
