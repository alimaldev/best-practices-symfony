<?php

namespace Linio\Frontend\Product;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Rating\RatingService;
use Linio\Frontend\Security\User;
use Linio\Frontend\WishList\ManageWishList;
use Linio\Type\Dictionary;
use Linio\Type\Money;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ProductMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function providerProductData()
    {
        $simpleSku1 = 'AD029SP63RLSLMX-416900';
        $simpleSku2 = 'AD029SP63RLSLMX-416901';

        $data = [
            'sku' => 'SKU12345',
            'name' => 'Test Product',
            'slug' => 'test-product',
            'free_shipping' => true,
            'linio_plus_level' => 1,
            'config_id' => '1677099',
            'delivery_time' => 4,
            'variation_type' => 'default',
            'fulfillment_type' => 'sold',
            'stock' => 1,
            'brand' => [
                'id' => 123,
                'name' => 'Test Brand',
                'slug' => 'test-brand',
            ],
            'is_international' => true,
            'categories' => [
                [
                    [
                        'id' => '1',
                        'name' => 'Root Category',
                        'slug' => 'root-category',
                    ],
                    [
                        'id' => '266',
                        'name' => 'Salud y Cuidado personal',
                        'slug' => 'salud-y-cuidado-personal',
                    ],
                    [
                        'id' => '299',
                        'name' => 'Perfumes',
                        'slug' => 'perfumes',
                    ],
                    [
                        'id' => '492',
                        'name' => 'Perfumes para Mujer',
                        'slug' => 'perfumes-para-mujer',
                    ],
                ],
            ],
            'description' => 'Description',
            'ean_code' => 'EANCODETEST',
            'is_oversized' => false,
            'express_invoice_enabled' => true,
            'seller' => [
                'id' => 123,
                'name' => 'Foo Bar Baz S.A. de C.V.',
                'slug' => 'foo-bar-baz-sa-de-cv',
                'type' => 'supplier',
                'rating' => 3,
                'operation_type' => null,
            ],
            'images' => [
                [
                    'position' => 1,
                    'main' => true,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-1',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-1-sprite.jpg',
                ],
                [
                    'position' => 2,
                    'main' => false,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-2',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-2-sprite.jpg',
                ],
            ],
            'attribute_set' => 'attributeSetExample',
            'attributes' => [
                'free_pickup' => '',
                'megapixels' => '5',
                'number_cpus' => '2',
                'supplier_warranty_months' => '5',
            ],
            'bundles' => [
                [
                    'name' => 'Megabundle',
                    'type' => 'fixed',
                    'discount' => null,
                    'saving_price' => 1000,
                    'start_date' => '',
                    'end_date' => '',
                    'products' => [
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 1',
                            'price' => 100,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 2',
                            'price' => 100,
                            'special_price' => 50,
                            'special_from_date' => '2015-01-01 10:00:00',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 3',
                            'price' => 100,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                    ],
                ],
                [
                    'name' => 'Megabundle 2',
                    'type' => 'percentage',
                    'discount' => 80,
                    'saving_price' => 2000,
                    'start_date' => '2015-01-01 10:00:00',
                    'end_date' => '',
                    'products' => [
                        [
                            'sku' => 'SKU12345',
                            'name' => 'Test Product',
                            'price' => 540.00,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'ABC1234',
                            'name' => 'Iphone 6 - 128Gb',
                            'price' => 660.00,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                    ],
                ],
            ],
            'grouped_products' => [],
            'marketplace_products' => [],
            'simples' => [
                [
                    'sku' => $simpleSku1,
                    'active' => true,
                    'created_at' => '2014-04-03 10:22:32',
                    'price' => 540,
                    'original_price' => 540,
                    'special_price' => 449,
                    'special_to_date' => '2015-05-10',
                    'special_from_date' => '2015-05-05',
                    'linio_plus_level' => 1,
                    'free_shipping' => false,
                    'stock' => 4,
                    'attributes' => [
                        'variation' => 'negro',
                        'max_delivery_time' => 6,
                        'min_delivery_time' => 2,
                        'promise_delivery_badge' => '',
                    ],
                ],
                [
                    'sku' => $simpleSku2,
                    'active' => false,
                    'created_at' => '2014-04-03 10:22:32',
                    'price' => 540,
                    'linio_plus_level' => 1,
                    'free_shipping' => false,
                    'stock' => 2,
                    'original_price' => 540,
                    'special_price' => 449,
                    'special_to_date' => '',
                    'special_from_date' => '',
                    'attributes' => [
                        'variation' => 'blanco',
                        'max_delivery_time' => 6,
                        'min_delivery_time' => 2,
                        'promise_delivery_badge' => 1,
                    ],
                ],
            ],
        ];

        return [
            [$data, $simpleSku1, $simpleSku2],
        ];
    }

    /**
     * @return array
     */
    public function providerMarketplaceProductData()
    {
        $simpleSku1 = 'AD029SP63RLSLMX-416900';
        $simpleSku2 = 'AD029SP63RLSLMX-416901';

        $data = [
            'sku' => 'SKU12345',
            'name' => 'Test Product',
            'slug' => 'test-product',
            'free_shipping' => true,
            'linio_plus_level' => 1,
            'config_id' => '1677099',
            'delivery_time' => 4,
            'variation_type' => 'default',
            'fulfillment_type' => 'sold',
            'stock' => 1,
            'brand' => [
                'id' => 123,
                'name' => 'Test Brand',
                'slug' => 'test-brand',
            ],
            'is_international' => true,
            'categories' => [
                [
                    [
                        'id' => '1',
                        'name' => 'Root Category',
                        'slug' => 'root-category',
                    ],
                    [
                        'id' => '266',
                        'name' => 'Salud y Cuidado personal',
                        'slug' => 'salud-y-cuidado-personal',
                    ],
                    [
                        'id' => '299',
                        'name' => 'Perfumes',
                        'slug' => 'perfumes',
                    ],
                    [
                        'id' => '492',
                        'name' => 'Perfumes para Mujer',
                        'slug' => 'perfumes-para-mujer',
                    ],
                ],
            ],
            'description' => 'Description',
            'ean_code' => 'EANCODETEST',
            'is_oversized' => false,
            'express_invoice_enabled' => true,
            'seller' => [
                'id' => 123,
                'name' => 'Foo Bar Baz S.A. de C.V.',
                'slug' => 'foo-bar-baz-sa-de-cv',
                'type' => 'supplier',
                'rating' => 3,
                'operation_type' => null,
            ],
            'images' => [
                [
                    'position' => 1,
                    'main' => true,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-1',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-1-sprite.jpg',
                ],
                [
                    'position' => 2,
                    'main' => false,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-2',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-2-sprite.jpg',
                ],
            ],
            'attribute_set' => 'attributeSetExample',
            'attributes' => [
                'free_pickup' => '',
                'megapixels' => '5',
                'number_cpus' => '2',
            ],
            'bundles' => [
                [
                    'name' => 'Megabundle',
                    'type' => 'fixed',
                    'discount' => null,
                    'saving_price' => 1000,
                    'start_date' => '',
                    'end_date' => '',
                    'products' => [
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 1',
                            'price' => 100,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 2',
                            'price' => 100,
                            'special_price' => 50,
                            'special_from_date' => '2015-01-01 10:00:00',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 3',
                            'price' => 100,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                    ],
                ],
                [
                    'name' => 'Megabundle 2',
                    'type' => 'percentage',
                    'discount' => 80,
                    'saving_price' => 2000,
                    'start_date' => '2015-01-01 10:00:00',
                    'end_date' => '',
                    'products' => [
                        [
                            'sku' => 'SKU12345',
                            'name' => 'Test Product',
                            'price' => 540.00,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'ABC1234',
                            'name' => 'Iphone 6 - 128Gb',
                            'price' => 660.00,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                    ],
                ],
            ],
            'grouped_products' => [],
            'marketplace_products' => [
                [
                    'seller' => [
                        'id' => 123,
                        'name' => 'Foo Bar Baz S.A. de C.V.',
                        'slug' => 'foo-bar-baz-sa-de-cv',
                        'type' => 'supplier',
                        'rating' => 3,
                        'operation_type' => null,
                    ],
                    'sku' => 'FOO123',
                    'stock' => 2,
                    'delivery_time' => 1,
                    'price' => '660.00',
                    'special_price' => '680.00',
                    'fulfillment_type' => 'sold_and_fulfilled',
                ],
            ],
            'simples' => [],
        ];

        return [
            [$data, $simpleSku1, $simpleSku2],
        ];
    }

    /**
     * @return array
     */
    public function providerProductDataWithoutSupplierWarranty()
    {
        $data = [
            'sku' => 'SKU12345',
            'name' => 'Test Product',
            'slug' => 'test-product',
            'free_shipping' => true,
            'linio_plus_level' => 1,
            'config_id' => '1677099',
            'delivery_time' => 4,
            'variation_type' => 'default',
            'fulfillment_type' => 'sold',
            'stock' => 1,
            'brand' => [
                'id' => 123,
                'name' => 'Test Brand',
                'slug' => 'test-brand',
            ],
            'is_international' => true,
            'categories' => [
                [
                    [
                        'id' => '1',
                        'name' => 'Root Category',
                        'slug' => 'root-category',
                    ],
                    [
                        'id' => '266',
                        'name' => 'Salud y Cuidado personal',
                        'slug' => 'salud-y-cuidado-personal',
                    ],
                    [
                        'id' => '299',
                        'name' => 'Perfumes',
                        'slug' => 'perfumes',
                    ],
                    [
                        'id' => '492',
                        'name' => 'Perfumes para Mujer',
                        'slug' => 'perfumes-para-mujer',
                    ],
                ],
            ],
            'description' => 'Description',
            'ean_code' => 'EANCODETEST',
            'is_oversized' => false,
            'express_invoice_enabled' => true,
            'seller' => [
                'id' => 123,
                'name' => 'Foo Bar Baz S.A. de C.V.',
                'slug' => 'foo-bar-baz-sa-de-cv',
                'type' => 'supplier',
                'rating' => 3,
                'operation_type' => null,
            ],
            'images' => [
                [
                    'position' => 1,
                    'main' => true,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-1',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-1-sprite.jpg',
                ],
                [
                    'position' => 2,
                    'main' => false,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-2',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-2-sprite.jpg',
                ],
            ],
            'attribute_set' => 'attributeSetExample',
            'attributes' => [
                'free_pickup' => '',
                'megapixels' => '5',
                'number_cpus' => '2',
                'supplier_warranty_months' => '0',
            ],
            'bundles' => [
                [
                    'name' => 'Megabundle',
                    'type' => 'fixed',
                    'discount' => null,
                    'saving_price' => 1000,
                    'start_date' => '',
                    'end_date' => '',
                    'products' => [
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 1',
                            'price' => 100,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 2',
                            'price' => 100,
                            'special_price' => 50,
                            'special_from_date' => '2015-01-01 10:00:00',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'AR855HB58WSZLAAR',
                            'name' => 'Título del producto 3',
                            'price' => 100,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                    ],
                ],
                [
                    'name' => 'Megabundle 2',
                    'type' => 'percentage',
                    'discount' => 80,
                    'saving_price' => 2000,
                    'start_date' => '2015-01-01 10:00:00',
                    'end_date' => '',
                    'products' => [
                        [
                            'sku' => 'SKU12345',
                            'name' => 'Test Product',
                            'price' => 540.00,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                        [
                            'sku' => 'ABC1234',
                            'name' => 'Iphone 6 - 128Gb',
                            'price' => 660.00,
                            'special_price' => 0,
                            'special_from_date' => '',
                            'special_to_date' => '',
                        ],
                    ],
                ],
            ],
            'grouped_products' => [],
            'marketplace_products' => [],
            'simples' => [
                [
                    'sku' => 'SKU1',
                    'active' => true,
                    'created_at' => '2014-04-03 10:22:32',
                    'price' => 540,
                    'original_price' => 540,
                    'special_price' => 449,
                    'special_to_date' => '2015-05-10',
                    'special_from_date' => '2015-05-05',
                    'linio_plus_level' => 1,
                    'free_shipping' => false,
                    'stock' => 4,
                    'attributes' => [
                        'variation' => 'negro',
                        'max_delivery_time' => 6,
                        'min_delivery_time' => 2,
                        'promise_delivery_badge' => 0,
                    ],
                ],
                [
                    'sku' => 'SKU2',
                    'active' => false,
                    'created_at' => '2014-04-03 10:22:32',
                    'price' => 540,
                    'linio_plus_level' => 1,
                    'free_shipping' => false,
                    'stock' => 2,
                    'original_price' => 540,
                    'special_price' => 449,
                    'special_to_date' => '',
                    'special_from_date' => '',
                    'attributes' => [
                        'variation' => 'blanco',
                        'max_delivery_time' => 6,
                        'min_delivery_time' => 2,
                        'promise_delivery_badge' => '',
                    ],
                ],
            ],
        ];

        return [
            [$data],
        ];
    }

    /**
     * @dataProvider providerMarketplaceProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenThereAreMarketplaceProducts(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(new Rating());

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 1]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);
        $brand = $result->getBrand();
        $item = $result->getMarketplaceChild('FOO123');
        $seller = $item->getSeller();
        $bundle = $result->getBundles()[0];
        $productAttributes = $result->getAttributes();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['delivery_time'], $result->getDeliveryTime());
        $this->assertEquals($data['config_id'], $result->getConfigId());
        $this->assertEquals($data['variation_type'], $result->getVariationType());
        $this->assertEquals($data['fulfillment_type'], $result->getFulfillmentType());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertEquals($data['ean_code'], $result->getEanCode());

        $this->assertEquals(new Money(680), $item->getPrice());
        $this->assertEquals('sold_and_fulfilled', $item->getFulfillmentType());

        $this->assertTrue($result->hasFreeShipping());
        $this->assertFalse($result->hasFreeStorePickup());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());

        $this->assertInstanceOf(Seller::class, $seller);
        $this->assertEquals($data['seller']['id'], $seller->getSellerId());
        $this->assertEquals($data['seller']['name'], $seller->getName());
        $this->assertEquals($data['seller']['slug'], $seller->getSlug());
        $this->assertEquals($data['seller']['type'], $seller->getType());
        $this->assertInstanceOf('Linio\Type\Dictionary', $productAttributes);
        $this->assertCount(3, $productAttributes);

        $this->assertEquals($data['bundles'][0]['name'], $bundle->getName());
        $this->assertEquals($data['bundles'][0]['type'], $bundle->getType());
        $this->assertEquals($data['bundles'][0]['products'][0]['price'], $bundle->getProducts()[0]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][1]['special_price'], $bundle->getProducts()[1]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][2]['price'], $bundle->getProducts()[2]->getPrice()->getMoneyAmount());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenProductHaveRating(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(new Rating());

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 1]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);
        $brand = $result->getBrand();
        $seller = $result->getSeller();
        $bundle = $result->getBundles()[0];
        $productAttributes = $result->getAttributes();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['delivery_time'], $result->getDeliveryTime());
        $this->assertEquals($data['config_id'], $result->getConfigId());
        $this->assertEquals($data['variation_type'], $result->getVariationType());
        $this->assertEquals($data['fulfillment_type'], $result->getFulfillmentType());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertEquals($data['ean_code'], $result->getEanCode());

        $this->assertTrue($result->hasFreeShipping());
        $this->assertFalse($result->hasFreeStorePickup());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());
        $this->assertEquals($data['simples'][0]['sku'], $result->getSimple($simpleSku1)->getSku());
        $this->assertEquals($data['simples'][1]['sku'], $result->getSimple($simpleSku2)->getSku());
        $this->assertInstanceOf(Seller::class, $seller);
        $this->assertEquals($data['seller']['id'], $seller->getSellerId());
        $this->assertEquals($data['seller']['name'], $seller->getName());
        $this->assertEquals($data['seller']['slug'], $seller->getSlug());
        $this->assertEquals($data['seller']['type'], $seller->getType());
        $this->assertInstanceOf('Linio\Type\Dictionary', $productAttributes);
        $this->assertCount(4, $productAttributes);

        $this->assertEquals($data['bundles'][0]['name'], $bundle->getName());
        $this->assertEquals($data['bundles'][0]['type'], $bundle->getType());
        $this->assertEquals($data['bundles'][0]['products'][0]['price'], $bundle->getProducts()[0]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][1]['special_price'], $bundle->getProducts()[1]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][2]['price'], $bundle->getProducts()[2]->getPrice()->getMoneyAmount());
        $this->assertEquals(6, $result->getFirstSimple()->getMinimumDeliveryDays());
        $this->assertEquals(7, $result->getFirstSimple()->getMaximumDeliveryDays());
        $this->assertTrue($result->getSimple($data['simples'][0]['sku'])->isActive());
        $this->assertFalse($result->getSimple($data['simples'][1]['sku'])->isActive());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenProductDoesntHaveRating(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(false);

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 0]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);
        $brand = $result->getBrand();
        $seller = $result->getSeller();
        $productAttributes = $result->getAttributes();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['slug'], $result->getSlug());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());
        $this->assertEquals($data['simples'][0]['sku'], $result->getSimple($simpleSku1)->getSku());
        $this->assertEquals($data['simples'][1]['sku'], $result->getSimple($simpleSku2)->getSku());
        $this->assertInstanceOf(Seller::class, $seller);
        $this->assertEquals($data['seller']['id'], $seller->getSellerId());
        $this->assertEquals($data['seller']['name'], $seller->getName());
        $this->assertEquals($data['seller']['slug'], $seller->getSlug());
        $this->assertEquals($data['seller']['type'], $seller->getType());
        $this->assertInstanceOf('Linio\Type\Dictionary', $productAttributes);
        $this->assertCount(4, $productAttributes);
        $this->assertEquals(2, $result->getFirstSimple()->getMinimumDeliveryDays());
        $this->assertEquals(6, $result->getFirstSimple()->getMaximumDeliveryDays());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWithSpecialPricesDates(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(new Rating());

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 0]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);
        $brand = $result->getBrand();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());
        $this->assertEquals($data['simples'][0]['sku'], $result->getSimple($simpleSku1)->getSku());
        $this->assertEquals($data['simples'][1]['sku'], $result->getSimple($simpleSku2)->getSku());
        $this->assertEquals(2, $result->getFirstSimple()->getMinimumDeliveryDays());
        $this->assertEquals(6, $result->getFirstSimple()->getMaximumDeliveryDays());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenProductHasFreeStorePickup(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(new Rating());

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 1]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $data['attributes']['free_pickup'] = '1';

        $result = $mapper->map($data);
        $brand = $result->getBrand();
        $seller = $result->getSeller();
        $bundle = $result->getBundles()[0];
        $productAttributes = $result->getAttributes();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['delivery_time'], $result->getDeliveryTime());
        $this->assertEquals($data['config_id'], $result->getConfigId());
        $this->assertEquals($data['variation_type'], $result->getVariationType());
        $this->assertEquals($data['fulfillment_type'], $result->getFulfillmentType());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertEquals($data['ean_code'], $result->getEanCode());

        $this->assertTrue($result->hasFreeShipping());
        $this->assertTrue($result->hasFreeStorePickup());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());
        $this->assertEquals($data['simples'][0]['sku'], $result->getSimple($simpleSku1)->getSku());
        $this->assertEquals($data['simples'][1]['sku'], $result->getSimple($simpleSku2)->getSku());
        $this->assertInstanceOf(Seller::class, $seller);
        $this->assertEquals($data['seller']['id'], $seller->getSellerId());
        $this->assertEquals($data['seller']['name'], $seller->getName());
        $this->assertEquals($data['seller']['slug'], $seller->getSlug());
        $this->assertEquals($data['seller']['type'], $seller->getType());
        $this->assertInstanceOf('Linio\Type\Dictionary', $productAttributes);
        $this->assertCount(4, $productAttributes);

        $this->assertEquals($data['bundles'][0]['name'], $bundle->getName());
        $this->assertEquals($data['bundles'][0]['type'], $bundle->getType());
        $this->assertEquals($data['bundles'][0]['products'][0]['price'], $bundle->getProducts()[0]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][1]['special_price'], $bundle->getProducts()[1]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][2]['price'], $bundle->getProducts()[2]->getPrice()->getMoneyAmount());
        $this->assertEquals(6, $result->getFirstSimple()->getMinimumDeliveryDays());
        $this->assertEquals(7, $result->getFirstSimple()->getMaximumDeliveryDays());
        $this->assertTrue($result->getSimple($data['simples'][0]['sku'])->isActive());
        $this->assertFalse($result->getSimple($data['simples'][1]['sku'])->isActive());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenSkuIsInWishLists(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(new Rating());

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 1]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn(['SKU12345']);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);
        $brand = $result->getBrand();
        $seller = $result->getSeller();
        $bundle = $result->getBundles()[0];
        $productAttributes = $result->getAttributes();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['delivery_time'], $result->getDeliveryTime());
        $this->assertEquals($data['config_id'], $result->getConfigId());
        $this->assertEquals($data['variation_type'], $result->getVariationType());
        $this->assertEquals($data['fulfillment_type'], $result->getFulfillmentType());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertEquals($data['ean_code'], $result->getEanCode());

        $this->assertTrue($result->hasFreeShipping());
        $this->assertFalse($result->hasFreeStorePickup());
        $this->assertTrue($result->isInWishLists());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());
        $this->assertEquals($data['simples'][0]['sku'], $result->getSimple($simpleSku1)->getSku());
        $this->assertEquals($data['simples'][1]['sku'], $result->getSimple($simpleSku2)->getSku());
        $this->assertInstanceOf(Seller::class, $seller);
        $this->assertEquals($data['seller']['id'], $seller->getSellerId());
        $this->assertEquals($data['seller']['name'], $seller->getName());
        $this->assertEquals($data['seller']['slug'], $seller->getSlug());
        $this->assertEquals($data['seller']['type'], $seller->getType());
        $this->assertInstanceOf('Linio\Type\Dictionary', $productAttributes);
        $this->assertCount(4, $productAttributes);

        $this->assertEquals($data['bundles'][0]['name'], $bundle->getName());
        $this->assertEquals($data['bundles'][0]['type'], $bundle->getType());
        $this->assertEquals($data['bundles'][0]['products'][0]['price'], $bundle->getProducts()[0]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][1]['special_price'], $bundle->getProducts()[1]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][2]['price'], $bundle->getProducts()[2]->getPrice()->getMoneyAmount());
        $this->assertEquals(6, $result->getFirstSimple()->getMinimumDeliveryDays());
        $this->assertEquals(7, $result->getFirstSimple()->getMaximumDeliveryDays());
        $this->assertTrue($result->getSimple($data['simples'][0]['sku'])->isActive());
        $this->assertFalse($result->getSimple($data['simples'][1]['sku'])->isActive());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenProductHasVariation(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $categoryServiceMock->expects($this->once())
            ->method('getCategory')
            ->with($data['categories'][0][3]['id'])
            ->willReturn(new Category());

        $ratingServiceMock = $this->createMock(RatingService::class);
        $ratingServiceMock->expects($this->once())
            ->method('getBySku')
            ->with($data['sku'])
            ->willReturn(new Rating());

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('fulfillment')
            ->willReturn(['shipment_routing_grace_window' => 1]);

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);
        $brand = $result->getBrand();
        $seller = $result->getSeller();
        $bundle = $result->getBundles()[0];
        $productAttributes = $result->getAttributes();

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['sku'], $result->getSku());
        $this->assertEquals($data['name'], $result->getName());
        $this->assertEquals($data['delivery_time'], $result->getDeliveryTime());
        $this->assertEquals($data['config_id'], $result->getConfigId());
        $this->assertEquals($data['variation_type'], $result->getVariationType());
        $this->assertEquals($data['fulfillment_type'], $result->getFulfillmentType());
        $this->assertEquals($data['attribute_set'], $result->getAttributeSet());
        $this->assertEquals($data['ean_code'], $result->getEanCode());

        $this->assertTrue($result->hasFreeShipping());
        $this->assertFalse($result->hasFreeStorePickup());
        $this->assertTrue($result->hasVariation());
        $this->assertInstanceOf(Brand::class, $brand);
        $this->assertEquals($data['brand']['id'], $brand->getId());
        $this->assertEquals($data['brand']['name'], $brand->getName());
        $this->assertEquals($data['brand']['slug'], $brand->getSlug());
        $this->assertEquals($data['description'], $result->getDescription());
        $this->assertEquals($data['simples'][0]['sku'], $result->getSimple($simpleSku1)->getSku());
        $this->assertEquals($data['simples'][1]['sku'], $result->getSimple($simpleSku2)->getSku());
        $this->assertInstanceOf(Seller::class, $seller);
        $this->assertEquals($data['seller']['id'], $seller->getSellerId());
        $this->assertEquals($data['seller']['name'], $seller->getName());
        $this->assertEquals($data['seller']['slug'], $seller->getSlug());
        $this->assertEquals($data['seller']['type'], $seller->getType());
        $this->assertInstanceOf(Dictionary::class, $productAttributes);
        $this->assertCount(4, $productAttributes);

        $this->assertEquals($data['bundles'][0]['name'], $bundle->getName());
        $this->assertEquals($data['bundles'][0]['type'], $bundle->getType());
        $this->assertEquals($data['bundles'][0]['products'][0]['price'], $bundle->getProducts()[0]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][1]['special_price'], $bundle->getProducts()[1]->getPrice()->getMoneyAmount());
        $this->assertEquals($data['bundles'][0]['products'][2]['price'], $bundle->getProducts()[2]->getPrice()->getMoneyAmount());
        $this->assertEquals(6, $result->getFirstSimple()->getMinimumDeliveryDays());
        $this->assertEquals(7, $result->getFirstSimple()->getMaximumDeliveryDays());
        $this->assertTrue($result->getSimple($data['simples'][0]['sku'])->isActive());
        $this->assertFalse($result->getSimple($data['simples'][1]['sku'])->isActive());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenProductHasWarranty(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $ratingServiceMock = $this->createMock(RatingService::class);

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['attributes']['supplier_warranty_months'], $result->getSupplierWarrantyMonths());
        $this->assertTrue($result->hasSupplierWarranty());
    }

    /**
     * @dataProvider providerProductDataWithoutSupplierWarranty
     *
     * @param array  $data
     */
    public function testIsMappingProductWithoutWarranty(array $data)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $ratingServiceMock = $this->createMock(RatingService::class);

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);

        $this->assertInstanceOf(Product::class, $result);
        $this->assertEquals($data['attributes']['supplier_warranty_months'], $result->getSupplierWarrantyMonths());
        $this->assertfalse($result->hasSupplierWarranty());
    }

    /**
     * @dataProvider providerProductData
     *
     * @param array  $data
     * @param string $simpleSku1
     * @param string $simpleSku2
     */
    public function testIsMappingWhenProductHasPromiseDelivery(array $data, $simpleSku1, $simpleSku2)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $ratingServiceMock = $this->createMock(RatingService::class);

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);

        $this->assertTrue($result->hasPromiseDelivery());
    }

    /**
     * @dataProvider providerProductDataWithoutSupplierWarranty
     *
     * @param array  $data
     */
    public function testIsMappingWhenProductDoesntHavePromiseDelivery(array $data)
    {
        $categoryServiceMock = $this->getMockBuilder(CategoryService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $ratingServiceMock = $this->createMock(RatingService::class);

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $customer = new User();
        $customer->setId(1);

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token);

        $mapper = new ProductMapper();
        $mapper->setCategoryService($categoryServiceMock);
        $mapper->setRatingService($ratingServiceMock);
        $mapper->setCacheService($cacheServiceMock);
        $mapper->setManageWishList($manageWishList->reveal());
        $mapper->setTokenStorage($tokenStorage->reveal());

        $result = $mapper->map($data);

        $this->assertFalse($result->hasPromiseDelivery());
    }
}
