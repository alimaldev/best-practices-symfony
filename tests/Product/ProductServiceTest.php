<?php

namespace Linio\Frontend\Product;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Seller;

class ProductServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingProductBySku()
    {
        $sku = 'SKU12345';
        $product = [
            'sku' => $sku,
            'name' => 'Test Product',
            'brand' => [
                'id' => 123,
                'name' => 'Test Brand',
                'slug' => 'test-brand',
            ],
            'categories' => [
                1,
                2,
                3,
            ],
            'description' => 'Description',
            'seller' => [
                'id' => 123,
                'name' => 'Foo Bar Baz S.A. de C.V.',
                'slug' => 'foo-bar-baz-sa-de-cv',
                'type' => 'supplier',
            ],
            'images' => [
                [
                    'image' => 1,
                    'main' => true,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-1',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-1-sprite.jpg',
                ],
                [
                    'image' => 2,
                    'main' => false,
                    'slug' => 'http://shop-front.dev/p/adidas-6868-130992-2',
                    'sprite' => 'http://shop-front.dev/p/adidas-6868-130992-2-sprite.jpg',
                ],
            ],
            'attributes' => [
                'megapixels' => '5',
                'number_cpus' => '2',
            ],
            'simples' => [
                'AD029SP63RLSLMX-416900' => [
                    'created_at' => '2014-04-03 10:22:32',
                    'original_price' => 540,
                    'price' => 540,
                    'sku' => 'AD029SP63RLSLMX-416900',
                    'special_from_date' => '',
                    'special_price' => 449,
                    'special_to_date' => '',
                    'attributes' => [
                        'variation' => 'negro',
                    ],
                ],
                'AD029SP63RLSLMX-416901' => [
                    'created_at' => '2014-04-03 10:22:32',
                    'original_price' => 540,
                    'price' => 540,
                    'sku' => 'AD029SP63RLSLMX-416901',
                    'special_from_date' => '',
                    'special_price' => 450,
                    'special_to_date' => '',
                    'attributes' => [
                        'variation' => 'blanco',
                    ],
                ],
            ],
        ];

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productMapperMock = $this->getMockBuilder(ProductMapper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productService = new ProductService();
        $productService->setCacheService($cacheServiceMock);
        $productService->setProductMapper($productMapperMock);

        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('product:' . $sku)
            ->willReturn($product);

        $productMapperMock->expects($this->once())
            ->method('map')
            ->with($product)
            ->willReturn(new Product());

        $result = $productService->getBySku($sku);

        $this->assertInstanceOf(Product::class, $result);
    }

    /**
     * @expectedException \Linio\Frontend\Product\Exception\ProductNotFoundException
     */
    public function testIsThrowingExceptionWhenGetProductWithNotFoundSku()
    {
        $sku = 'SKUNOTFOUND';

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productService = new ProductService();
        $productService->setCacheService($cacheServiceMock);

        $cacheServiceMock->expects($this->once())
            ->method('get')
            ->with('product:' . $sku)
            ->willReturn(null);

        $productService->getBySku($sku);
    }

    /**
     * @return array
     */
    public function providerSku()
    {
        return [
            ['MO154EL21AOOLAPA'],
            ['AC428EL22MGJLAPA'],
            ['AC446EL08LFZLAPA'],
        ];
    }

    /**
     * @return array
     */
    public function providerSkuWithSimple()
    {
        return [
            ['MO154EL21AOOLAPA-378', 'MO154EL21AOOLAPA'],
            ['AC428EL22MGJLAPA-8268', 'AC428EL22MGJLAPA'],
            ['AC446EL08LFZLAPA-7582', 'AC446EL08LFZLAPA'],
        ];
    }

    /**
     * @dataProvider providerSkuWithSimple
     *
     * @param string $simpleSku
     * @param string $sku
     */
    public function testIsSkuExtractedWithSimple($simpleSku, $sku)
    {
        $productService = new ProductService();
        $this->assertEquals($sku, $productService->getSkuFromSimple($simpleSku));
    }

    /**
     * @dataProvider providerSku
     *
     * @param string $sku
     */
    public function testIsSkuExtractedWithoutSimple($sku)
    {
        $productService = new ProductService();
        $this->assertEquals($sku, $productService->getSkuFromSimple($sku));
    }

    public function testIsReturningSameProductAsSellerPlaced()
    {
        $seller = new Seller();
        $seller->setSlug('montgomery-sales-inc');

        $product = new Product();
        $product->setSeller($seller);

        $service = new ProductService();

        $actual = $service->getPlacedBySeller($product, $seller->getSlug());

        $this->assertEquals($product, $actual);
    }

    public function testIsReturningMasterProductAsPlacedBySeller()
    {
        $masterData = ['sku' => 'AP068EL065XLKLMX'];

        $masterSeller = new Seller();
        $masterSeller->setSlug('adismaelectronics');

        $masterProduct = new Product();
        $masterProduct->setSku($masterData['sku']);
        $masterProduct->setSeller($masterSeller);

        $seller = new Seller();
        $seller->setSlug('montgomery-sales-inc');

        $product = new Product();
        $product->setMarketplaceParentSku($masterData['sku']);
        $product->setSeller($seller);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('product:AP068EL065XLKLMX')
            ->shouldBeCalled()
            ->willReturn($masterData);

        $productMapper = $this->prophesize(ProductMapper::class);
        $productMapper->map($masterData)
            ->shouldBeCalled()
            ->willReturn($masterProduct);

        $service = new ProductService();
        $service->setCacheService($cache->reveal());
        $service->setProductMapper($productMapper->reveal());

        $actual = $service->getPlacedBySeller($product, $masterSeller->getSlug());

        $this->assertEquals($masterProduct, $actual);
    }

    public function testIsReturningMarketplaceChildAsPlacedBySeller()
    {
        $childData = ['sku' => 'AP068EL065XLKLMX'];
        $masterData = ['sku' => 'AP068EL0JF5JCLMX'];

        $childProduct = new Product();
        $childProduct->setSku($childData['sku']);

        $marketPlaceChildSeller = new Seller();
        $marketPlaceChildSeller->setSlug('tecdepot');

        $marketPlaceChild = new MarketplaceChild();
        $marketPlaceChild->setSku($childData['sku']);
        $marketPlaceChild->setSeller($marketPlaceChildSeller);

        $masterSeller = new Seller();
        $masterSeller->setSlug('adismaelectronics');

        $masterProduct = new Product();
        $masterProduct->setSku($masterData['sku']);
        $masterProduct->setSeller($masterSeller);
        $masterProduct->addMarketplaceChild($marketPlaceChild);

        $seller = new Seller();
        $seller->setSlug('montgomery-sales-inc');

        $product = new Product();
        $product->setMarketplaceParentSku($masterData['sku']);
        $product->setSeller($seller);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('product:' . $masterData['sku'])
            ->shouldBeCalled()
            ->willReturn($masterData);
        $cache->get('product:' . $childData['sku'])
            ->shouldBeCalled()
            ->willReturn($childData);

        $productMapper = $this->prophesize(ProductMapper::class);
        $productMapper->map($masterData)
            ->shouldBeCalled()
            ->willReturn($masterProduct);
        $productMapper->map($childData)
            ->shouldBeCalled()
            ->willReturn($childProduct);

        $service = new ProductService();
        $service->setCacheService($cache->reveal());
        $service->setProductMapper($productMapper->reveal());

        $actual = $service->getPlacedBySeller($product, $marketPlaceChildSeller->getSlug());

        $this->assertEquals($childProduct, $actual);
    }

    public function testIsReturningSameProductWhenProductPlacedBySellerNotFound()
    {
        $childData = ['sku' => 'AP068EL065XLKLMX'];
        $masterData = ['sku' => 'AP068EL0JF5JCLMX'];

        $childProduct = new Product();
        $childProduct->setSku($childData['sku']);

        $marketPlaceChildSeller = new Seller();
        $marketPlaceChildSeller->setSlug('tecdepot');

        $marketPlaceChild = new MarketplaceChild();
        $marketPlaceChild->setSku($childData['sku']);
        $marketPlaceChild->setSeller($marketPlaceChildSeller);

        $masterSeller = new Seller();
        $masterSeller->setSlug('adismaelectronics');

        $masterProduct = new Product();
        $masterProduct->setSku($masterData['sku']);
        $masterProduct->setSeller($masterSeller);
        $masterProduct->addMarketplaceChild($marketPlaceChild);

        $seller = new Seller();
        $seller->setSlug('montgomery-sales-inc');

        $product = new Product();
        $product->setMarketplaceParentSku($masterData['sku']);
        $product->setSeller($seller);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('product:' . $masterData['sku'])
            ->shouldBeCalled()
            ->willReturn($masterData);

        $productMapper = $this->prophesize(ProductMapper::class);
        $productMapper->map($masterData)
            ->shouldBeCalled()
            ->willReturn($masterProduct);

        $service = new ProductService();
        $service->setCacheService($cache->reveal());
        $service->setProductMapper($productMapper->reveal());

        $actual = $service->getPlacedBySeller($product, 'seller');

        $this->assertEquals($product, $actual);
    }

    public function testIsReturningSameProductAsSellerPlacedWhenProductDoesNotHaveSeller()
    {
        $product = new Product();

        $service = new ProductService();

        $actual = $service->getPlacedBySeller($product, 'tecdepot');

        $this->assertEquals($product, $actual);
    }
}
