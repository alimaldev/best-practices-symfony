<?php

namespace Linio\Frontend\Transformer\EventListener;

use Linio\Frontend\EventListener\JsonRequestTransformerListener;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class JsonRequestTransformerListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsOnKernelRequestReplacingDecodedContent()
    {
        $responseEventMock = $this->prophesize(GetResponseEvent::class);
        $requestMock = $this->prophesize(Request::class);
        $requestAttributeMock = $this->prophesize(ParameterBag::class);

        $content = '{"customerAddress":{"firstName":"FirstName", "lastName":"LastName"}}';
        $decodedContent = [
            'customerAddress' => [
                'firstName' => 'FirstName',
                'lastName' => 'LastName',
            ],
        ];

        $requestMock->getContentType()
            ->shouldBeCalled()
            ->willReturn('json');

        $requestMock->getContent()
            ->shouldBeCalled()
            ->willReturn($content);

        $requestAttributeMock->replace($decodedContent)
            ->shouldBeCalled();

        $requestMock->request = $requestAttributeMock;

        $responseEventMock->getRequest()
            ->shouldBeCalled()
            ->willReturn($requestMock);

        $jsonRequestTransformerListener = new JsonRequestTransformerListener();
        $jsonRequestTransformerListener->onKernelRequest($responseEventMock->reveal());
    }

    public function testIsHandlingEmptyContentWithValidContentType()
    {
        $responseEventMock = $this->prophesize(GetResponseEvent::class);
        $requestMock = $this->prophesize(Request::class);
        $requestAttributeMock = $this->prophesize(ParameterBag::class);

        $requestMock->getContentType()
            ->shouldBeCalled()
            ->willReturn('json');

        $requestMock->getContent()
            ->shouldBeCalled()
            ->willReturn('');

        $requestAttributeMock->replace([])
            ->shouldBeCalled();

        $requestMock->request = $requestAttributeMock;

        $responseEventMock->getRequest()
            ->shouldBeCalled()
            ->willReturn($requestMock);

        $jsonRequestTransformerListener = new JsonRequestTransformerListener();
        $jsonRequestTransformerListener->onKernelRequest($responseEventMock->reveal());
    }

    public function testIsOnKernelRequestSettingABadResponse()
    {
        $responseEventMock = $this->prophesize(GetResponseEvent::class);
        $requestMock = $this->prophesize(Request::class);

        $content = '{"customerAddress":"firstName":"FirstName", "lastName":"LastName"}}';

        $requestMock->getContentType()
            ->shouldBeCalled()
            ->willReturn('json');

        $requestMock->getContent()
            ->shouldBeCalled()
            ->willReturn($content);

        $responseEventMock->getRequest()
            ->shouldBeCalled()
            ->willReturn($requestMock);

        $responseEventMock->setResponse(Argument::type(Response::class))
            ->shouldBeCalled();

        $jsonRequestTransformerListener = new JsonRequestTransformerListener();
        $jsonRequestTransformerListener->onKernelRequest($responseEventMock->reveal());
    }

    public function testIsOnKernelRequestJustReturning()
    {
        $responseEventMock = $this->prophesize(GetResponseEvent::class);
        $requestMock = $this->prophesize(Request::class);

        $requestMock->getContentType()
            ->shouldBeCalled()
            ->willReturn('html');

        $responseEventMock->getRequest()
            ->shouldBeCalled()
            ->willReturn($requestMock);

        $jsonRequestTransformerListener = new JsonRequestTransformerListener();
        $jsonRequestTransformerListener->onKernelRequest($responseEventMock->reveal());
    }
}
