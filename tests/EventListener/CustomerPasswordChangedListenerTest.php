<?php

namespace EventListener;

use Linio\Frontend\Event\CustomerPasswordChangedEvent;
use Linio\Frontend\EventListener\CustomerPasswordChangedListener;
use Linio\Frontend\Security\Jwt\TokenManager;
use Linio\Frontend\Security\User;

class CustomerPasswordChangedListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsFlushingCustomerTokens()
    {
        $user = new User();
        $event = new CustomerPasswordChangedEvent($user);

        $tokenManager = $this->prophesize(TokenManager::class);
        $tokenManager->flushTokens($user)->shouldBeCalled();

        $listener = new CustomerPasswordChangedListener($tokenManager->reveal());
        $listener->onCustomerPasswordChanged($event);
    }
}
