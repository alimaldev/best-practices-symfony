<?php

declare(strict_types=1);

namespace EventListener;

use Linio\Frontend\EventListener\PlatformInformationListener;
use Linio\Frontend\Request\PlatformInformation;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class PlatformInformationListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function requestProvider(): array
    {
        $desktopRequest = new Request();
        $desktopRequest->headers = new HeaderBag(['User-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36']);

        $iosRequest = new Request();
        $iosRequest->headers = new HeaderBag(['User-agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17 linio_ios']);

        $androidRequest = new Request();
        $androidRequest->headers = new HeaderBag(['User-agent' => 'Mozilla/5.0 (Linux; Android 5.0.2; XT1225 Build/LXG22.33-12.16) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.81 Mobile Safari/537.36 linio_android']);

        return [
            [$desktopRequest, 1, 'desktop'],
            [$iosRequest, '2', 'ios'],
            [$androidRequest, '3', 'android'],
        ];
    }

    /**
     * @dataProvider requestProvider
     *
     * @param Request $request
     * @param int $storeId
     * @param string $platformId
     */
    public function testIsSettingUpPlatformInformationBasedOnRequest(Request $request, int $storeId, string $platformId)
    {
        $platformInformation = new PlatformInformation();

        $storeIdMap = [
            'desktop' => 1,
            'ios' => 2,
            'android' => 3,
        ];

        $event = $this->prophesize(GetResponseEvent::class);
        $event->getRequest()->shouldBeCalled()->willReturn($request);

        $listener = new PlatformInformationListener($storeIdMap, $platformInformation);
        $actual = $listener->onKernelRequest($event->reveal());

        $this->assertEquals($storeId, $platformInformation->getStoreId());
        $this->assertEquals($platformId, $platformInformation->getPlatform());
        $this->assertNull($actual);
    }
}
