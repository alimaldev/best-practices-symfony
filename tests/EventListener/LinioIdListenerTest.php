<?php

declare(strict_types=1);

namespace EventListener;

use Linio\Frontend\EventListener\GuestUserAuthenticationListener;
use Linio\Frontend\EventListener\LinioIdListener;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\Security\User;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class LinioIdListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testNoTokenSetsGuestUserIdInCartId()
    {
        $listener = new LinioIdListener();
        $listener->setTokenStorage(new TokenStorage());

        $request = new Request();
        $request->setSession(new Session(new MockArraySessionStorage()));

        $response = new Response();

        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);

        $listener->onKernelResponse($event);

        $cookies = $event->getResponse()->headers->getCookies();
        $linioId = $cookies[0]->getValue();

        $this->assertRegExp('/_[a-zA-Z0-9]{8,16}\.[a-zA-Z0-9]{5,8}/', $linioId);
    }

    public function testAnonymousTokenSetsGuestUserIdInCartId()
    {
        $listener = new LinioIdListener();

        $expectedLinioId = 'abc1234';
        $guestUser = new GuestUser($expectedLinioId);
        $tokenStorage = new TokenStorage();
        $tokenStorage->setToken(new AnonymousToken('abc', $guestUser, []));
        $listener->setTokenStorage($tokenStorage);

        $request = new Request();
        $request->setSession(new Session(new MockArraySessionStorage()));

        $response = new Response();

        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);

        $listener->onKernelResponse($event);

        $cookies = $event->getResponse()->headers->getCookies();
        $linioId = $cookies[0]->getValue();

        $this->assertEquals($expectedLinioId, $linioId);
    }

    public function testLoggedInUserTokenSetsLinioId()
    {
        $listener = new LinioIdListener();

        $expectedLinioId = 'abc1234';
        $loggedInUser = new User();
        $loggedInUser->setLinioId($expectedLinioId);
        $tokenStorage = new TokenStorage();
        $tokenStorage->setToken(new AnonymousToken('abc', $loggedInUser, []));
        $listener->setTokenStorage($tokenStorage);

        $request = new Request();
        $request->setSession(new Session(new MockArraySessionStorage()));

        $response = new Response();

        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);

        $listener->onKernelResponse($event);

        $cookies = $event->getResponse()->headers->getCookies();
        $linioId = $cookies[0]->getValue();

        $this->assertEquals($expectedLinioId, $linioId);
    }

    public function testNoTokenSetsGuestUserIdInHeader()
    {
        $listener = new LinioIdListener();
        $listener->setTokenStorage(new TokenStorage());

        $request = new Request();
        $request->setSession(new Session(new MockArraySessionStorage()));

        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, new Response());

        $listener->onKernelResponse($event);

        $linioId = $event->getResponse()->headers->get(GuestUserAuthenticationListener::LINIO_ID_HEADER, '');

        $this->assertRegExp('/_[a-zA-Z0-9]{8,16}\.[a-zA-Z0-9]{5,8}/', $linioId);
    }

    public function testAnonymousTokenSetsGuestUserIdInHeader()
    {
        $listener = new LinioIdListener();

        $expectedLinioId = 'abc1234';
        $guestUser = new GuestUser($expectedLinioId);
        $tokenStorage = new TokenStorage();
        $tokenStorage->setToken(new AnonymousToken('abc', $guestUser, []));
        $listener->setTokenStorage($tokenStorage);

        $request = new Request();
        $request->setSession(new Session(new MockArraySessionStorage()));

        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, new Response());

        $listener->onKernelResponse($event);

        $linioId = $event->getResponse()->headers->get(GuestUserAuthenticationListener::LINIO_ID_HEADER, '');

        $this->assertEquals($expectedLinioId, $linioId);
    }

    public function testLoggedInUserTokenSetsLinioIdHeader()
    {
        $listener = new LinioIdListener();

        $expectedLinioId = 'abc1234';
        $loggedInUser = new User();
        $loggedInUser->setLinioId($expectedLinioId);
        $tokenStorage = new TokenStorage();
        $tokenStorage->setToken(new AnonymousToken('abc', $loggedInUser, []));
        $listener->setTokenStorage($tokenStorage);

        $request = new Request();
        $request->setSession(new Session(new MockArraySessionStorage()));

        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, new Response());

        $listener->onKernelResponse($event);

        $linioId = $event->getResponse()->headers->get(GuestUserAuthenticationListener::LINIO_ID_HEADER, '');

        $this->assertEquals($expectedLinioId, $linioId);
    }
}
