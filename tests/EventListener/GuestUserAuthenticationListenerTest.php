<?php

namespace EventListener;

use Linio\Frontend\EventListener\GuestUserAuthenticationListener;
use Linio\Frontend\Security\GuestUser;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class GuestUserAuthenticationListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testNewAnonymousUserIsCreated()
    {
        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new GetResponseEvent($kernel, new Request(), HttpKernelInterface::MASTER_REQUEST);

        $tokenStorage = new TokenStorage();
        $listener = new GuestUserAuthenticationListener($tokenStorage, 'abc');

        $listener->handle($event);

        $this->assertInstanceOf(AnonymousToken::class, $tokenStorage->getToken());
        $this->assertInstanceOf(GuestUser::class, $tokenStorage->getToken()->getUser());
        $this->assertRegExp('/_[a-zA-Z0-9]{8,16}\.[a-zA-Z0-9]{5,8}/', $tokenStorage->getToken()->getUser()->getLinioId());
    }

    public function testNewRequestsGenerateDifferentGuestUsers()
    {
        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new GetResponseEvent($kernel, new Request(), HttpKernelInterface::MASTER_REQUEST);

        $tokenStorage1 = new TokenStorage();
        $listener1 = new GuestUserAuthenticationListener($tokenStorage1, 'abc');

        $listener1->handle($event);

        $user1 = $tokenStorage1->getToken()->getUser();

        $tokenStorage2 = new TokenStorage();
        $listener2 = new GuestUserAuthenticationListener($tokenStorage2, 'abc');

        $listener2->handle($event);

        $user2 = $tokenStorage2->getToken()->getUser();

        $this->assertNotEquals($user1->getLinioId(), $user2->getLinioId());
    }

    public function testRequestHeaderSetsGuestUserId()
    {
        $expectedLinioId = 'abc1234';
        $request = new Request();
        $request->headers->set(GuestUserAuthenticationListener::LINIO_ID_HEADER, $expectedLinioId);
        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $event = new GetResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST);

        $tokenStorage = new TokenStorage();
        $listener = new GuestUserAuthenticationListener($tokenStorage, 'abc');

        $listener->handle($event);

        $this->assertEquals($expectedLinioId, $tokenStorage->getToken()->getUser()->getLinioId());
    }
}
