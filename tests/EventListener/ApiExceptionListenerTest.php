<?php

declare(strict_types=1);

namespace Linio\Frontend\Transformer\EventListener;

use Linio\Exception\NotFoundHttpException as LinioNotFoundException;
use Linio\Frontend\Api\Response\ErrorJsonResponse;
use Linio\Frontend\EventListener\ApiExceptionListener;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use Prophecy\Argument;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as SymfonyNotFoundException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Translation\TranslatorInterface;

class ApiExceptionListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsCatchingDomainExceptionsAndConvertingToJsonError()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new DomainException('foobar', ['foo' => 'bar']);
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::ERROR, $exception->getMessage(), ['exception' => $exception])->shouldBeCalled();

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('foobar')->willReturn('foobar translated');

        $listener = new ApiExceptionListener();
        $listener->setTranslator($translator->reveal());
        $listener->setLogger($logger->reveal());
        $listener->onKernelException($event);

        $this->assertEquals('{"message":"foobar translated","context":{"foo":"bar"}}', $event->getResponse()->getContent());
        $this->assertEquals(500, $event->getResponse()->getStatusCode());
    }

    public function testIsCatchingDomainExceptionsAndUsingAFallbackTranslation()
    {
        $translationString = '¡Ay! Algo salió mal por aquí';

        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new DomainException('foobar', ['foo' => 'bar']);
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::ERROR, $exception->getMessage(), ['exception' => $exception])->shouldBeCalled();

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('foobar')->willReturn('foobar');
        $translator->trans('error_500_page.something_is_wrong')->willReturn($translationString);

        $listener = new ApiExceptionListener();
        $listener->setTranslator($translator->reveal());
        $listener->setLogger($logger->reveal());
        $listener->onKernelException($event);

        $this->assertJsonStringEqualsJsonString(
            '{"message":"' . $translationString . '", "context":{"foo":"bar"}}',
            $event->getResponse()->getContent()
        );
    }

    public function testIsCatchingInputExceptionsAndConvertingToJsonError()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new InputException('foobar', ['foo' => 'bar']);
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::INFO, $exception->getMessage(), ['exception' => $exception])->shouldBeCalled();

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('foobar')->willReturn('foobar translated');

        $listener = new ApiExceptionListener();
        $listener->setTranslator($translator->reveal());
        $listener->setLogger($logger->reveal());
        $listener->onKernelException($event);

        $this->assertEquals('{"message":"foobar translated","context":{"foo":"bar"}}', $event->getResponse()->getContent());
        $this->assertEquals(400, $event->getResponse()->getStatusCode());
    }

    public function testIsCatchingLinioNotFoundExceptionAndConvertingToJsonError()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new LinioNotFoundException('foobar');
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::WARNING, $exception->getMessage(), Argument::that(function (array $parameter) {
            return !empty($parameter['exception']) && $parameter['exception'] instanceof DomainException;
        }))->shouldBeCalled();

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('foobar')->willReturn('foobar translated');

        $listener = new ApiExceptionListener();
        $listener->setTranslator($translator->reveal());
        $listener->setLogger($logger->reveal());
        $listener->onKernelException($event);

        $this->assertEquals('{"message":"foobar translated","context":null}', $event->getResponse()->getContent());
        $this->assertEquals(404, $event->getResponse()->getStatusCode());
    }

    public function testIsCatchingSymfonyNotFoundExceptionAndConvertingToJsonError()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new SymfonyNotFoundException('foobar');
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::WARNING, $exception->getMessage(), Argument::that(function (array $parameter) {
            return !empty($parameter['exception']) && $parameter['exception'] instanceof DomainException;
        }))->shouldBeCalled();

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('foobar')->willReturn('foobar translated');

        $listener = new ApiExceptionListener();
        $listener->setTranslator($translator->reveal());
        $listener->setLogger($logger->reveal());
        $listener->onKernelException($event);

        $this->assertEquals('{"message":"foobar translated","context":null}', $event->getResponse()->getContent());
        $this->assertEquals(404, $event->getResponse()->getStatusCode());
    }

    public function testIsIgnoringNonDomainExceptions()
    {
        $translationString = '¡Ay! Algo salió mal por aquí';

        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new \Exception('foobar');
        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::CRITICAL, $exception->getMessage(), ['exception' => $exception])->shouldBeCalled();

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('error_500_page.something_is_wrong')->willReturn($translationString);

        $listener = new ApiExceptionListener();
        $listener->setTranslator($translator->reveal());
        $listener->setLogger($logger->reveal());
        $listener->onKernelException($event);

        $this->assertJsonStringEqualsJsonString(
            '{"message":"' . $translationString . '", "context": null}',
            $event->getResponse()->getContent()
        );
        $this->assertEquals(500, $event->getResponse()->getStatusCode());
    }

    public function testIsIgnoringNonJsonRequests()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new \Exception('foobar');
        $request = new Request();

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $listener = new ApiExceptionListener();
        $listener->onKernelException($event);

        $this->assertNotInstanceOf(ErrorJsonResponse::class, $event->getResponse());
    }
}
