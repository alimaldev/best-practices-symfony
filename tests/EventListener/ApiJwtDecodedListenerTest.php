<?php

namespace Linio\Frontend\Transformer\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Linio\Frontend\EventListener\ApiJwtDecodedListener;
use Linio\Frontend\Security\Jwt\TokenManager;
use Symfony\Component\HttpFoundation\Request;

class ApiJwtDecodedListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsDetectingValidToken()
    {
        $request = new Request();
        $token = 'cs8df9sd8f.a0s8d79a8s7d98s.a9sd07as86f5a8da9sf';

        $tokenManager = $this->prophesize(TokenManager::class);
        $tokenManager->getTokenFromRequest($request)->willReturn($token);
        $tokenManager->isPayloadValid($token)->willReturn(true);
        $tokenManager->tokenExists($token)->willReturn(true);

        $jwtDecodedEvent = $this->prophesize(JWTDecodedEvent::class);
        $jwtDecodedEvent->getRequest()->willReturn($request);

        $listener = new ApiJwtDecodedListener($tokenManager->reveal());
        $listener->onJWTDecoded($jwtDecodedEvent->reveal());
    }

    public function testIsDetectingInvalidToken()
    {
        $request = new Request();
        $token = 'cs8df9sd8f.a0s8d79a8s7d98s.a9sd07as86f5a8da9sf';

        $tokenManager = $this->prophesize(TokenManager::class);
        $tokenManager->getTokenFromRequest($request)->willReturn($token);
        $tokenManager->isPayloadValid($token)->willReturn(false);
        $tokenManager->tokenExists($token)->willReturn(false);

        $jwtDecodedEvent = $this->prophesize(JWTDecodedEvent::class);
        $jwtDecodedEvent->getRequest()->willReturn($request);
        $jwtDecodedEvent->markAsInvalid()->shouldBeCalled();

        $listener = new ApiJwtDecodedListener($tokenManager->reveal());
        $listener->onJWTDecoded($jwtDecodedEvent->reveal());
    }
}
