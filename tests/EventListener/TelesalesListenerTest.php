<?php

namespace Linio\Frontend\Transformer\EventListener;

use Linio\Frontend\EventListener\TelesalesListener;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class TelesalesListenerTest extends PHPUnit_Framework_TestCase
{
    public function testIsSettingTelesalesUtmDataInSession()
    {
        $utm = [
            'source' => 'source',
            'medium' => 'medium',
            'campaign' => 'campaign',
        ];

        $query = $this->prophesize(ParameterBag::class);
        $query->get('utm_source', '')->willReturn('source');
        $query->get('utm_medium', '')->willReturn('medium');
        $query->get('utm_campaign', '')->willReturn('campaign');

        $request = $this->prophesize(Request::class);
        $request->query = $query->reveal();
        $request->get('_route')->willReturn('frontend.security.telesales.login');

        $session = $this->prophesize(SessionInterface::class);
        $session->set('telesales/utm', $utm);

        $event = $this->prophesize(GetResponseEvent::class);
        $event->getRequest()->willReturn($request);

        $listener = new TelesalesListener($session->reveal());
        $listener->onKernelRequest($event->reveal());
    }
}
