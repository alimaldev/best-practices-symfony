<?php

declare(strict_types=1);

namespace Linio\Frontend\Transformer\tests\EventListener;

use Linio\Frontend\Api\Response\ErrorJsonResponse;
use Linio\Frontend\EventListener\ApiAuthExceptionListener;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Translation\TranslatorInterface;

class ApiAuthExceptionListenerTest extends \PHPUnit_Framework_TestCase
{
    public function exceptionDataProvider()
    {
        return [
            [new UnauthorizedHttpException('foobar')],
            [new AccessDeniedHttpException('foobar')],
            [new AuthenticationException('foobar')],
            [new AccessDeniedException('foobar')],
        ];
    }

    /**
     * @dataProvider exceptionDataProvider
     */
    public function testIsCatchingExceptionAndConvertingToJsonError($exception)
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $logger = $this->prophesize(LoggerInterface::class);
        $logger->log(LogLevel::DEBUG, $exception->getMessage(), ['exception' => $exception])->shouldBeCalled();

        $translator = $this->prophesize(TranslatorInterface::class);
        $translator->trans('UNAUTHORIZED')
            ->shouldBeCalled()
            ->willReturn('El usuario o contraseña no son correctos, favor de verificarlos.');

        $listener = new ApiAuthExceptionListener();
        $listener->setLogger($logger->reveal());
        $listener->setTranslator($translator->reveal());
        $listener->onKernelException($event);

        $expected = [
            'message' => 'El usuario o contraseña no son correctos, favor de verificarlos.',
            'context' => null,
        ];

        $this->assertEquals(json_encode($expected), $event->getResponse()->getContent());
        $this->assertEquals(401, $event->getResponse()->getStatusCode());
    }

    public function testIsIgnoringNonAuthHttpExceptions()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new HttpException('foobar');

        $request = new Request();
        $request->headers->set('Accept', 'application/json');

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);

        $listener = new ApiAuthExceptionListener();
        $listener->onKernelException($event);
        $listener->setTranslator($translator->reveal());

        $this->assertNull($event->getResponse());
    }

    public function testIsIgnoringNonJsonRequests()
    {
        $kernel = $this->prophesize(HttpKernelInterface::class)->reveal();
        $exception = new \Exception('foobar');
        $request = new Request();

        $event = new GetResponseForExceptionEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $exception);

        $translator = $this->prophesize(TranslatorInterface::class);

        $listener = new ApiAuthExceptionListener();
        $listener->onKernelException($event);
        $listener->setTranslator($translator->reveal());

        $this->assertNotInstanceOf(ErrorJsonResponse::class, $event->getResponse());
    }
}
