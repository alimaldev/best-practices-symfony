<?php

namespace Linio\Frontend\Transformer\EventListener;

use Exception;
use Linio\Exception\NotFoundHttpException as LinioNotFoundException;
use Linio\Frontend\Entity\Catalog\Redirect;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\EventListener\ResourceNotFoundListener;
use Linio\Frontend\Helper\HashIds;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\SlugResolver\Exception\SlugNotFoundException;
use Linio\Frontend\SlugResolver\SlugResolverService;
use PHPUnit_Framework_TestCase;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class ResourceNotFoundListenerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function exceptionProvider()
    {
        return [
            [new SlugNotFoundException('foo')],
            [new LinioNotFoundException('foo')],
        ];
    }

    public function testIsRedirectingToProductDetail()
    {
        $expected = new RedirectResponse('/p/bored-product', 301);

        $router = $this->prophesize(RouterInterface::class);
        $router->match('bored-product-46484.html')
            ->willReturn([]);
        $router->generate(
            'frontend.catalog.detail',
            ['productSlug' => 'bored-product'])
            ->willReturn('/p/bored-product');

        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->willReturn('bored-product-46484.html');
        $request->getQueryString()->willReturn(null);
        $request->query = new ParameterBag();

        $hashIds = $this->prophesize(HashIds::class);
        $hashIds->getHashId('46484')->willReturn('SKU-CONFIG');

        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getException()->willReturn(new NotFoundHttpException());
        $event->getRequest()->willReturn($request->reveal());
        $event->setResponse($expected)->shouldBeCalled();

        $product = new Product();
        $product->setSlug('bored-product');

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolve('product', 'SKU-CONFIG')->willReturn($product);

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $router->reveal(),
            $hashIds->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }

    /**
     * @dataProvider exceptionProvider
     *
     * @param Exception $exception
     */
    public function testIsRedirectingUsingCachedRoute(Exception $exception)
    {
        $expected = new RedirectResponse('/s/comtex', 301);

        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->shouldBeCalledTimes(2)->willReturn('comtex');
        $request->getQueryString()->willReturn(null);

        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getException()->willReturn($exception);
        $event->getRequest()->willReturn($request->reveal());
        $event->setResponse($expected)->shouldBeCalled();

        $redirect = new Redirect();
        $redirect->setUrl('/s/comtex');

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolveLegacy('comtex')->willReturn($redirect);

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $this->prophesize(RouterInterface::class)->reveal(),
            $this->prophesize(HashIds::class)->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }

    public function testIsRedirectingProductUsingCacheRoute()
    {
        $expected = new RedirectResponse('/c/computacion/pc-portatil?q=Laptop%20Dell%20latitude', 301);

        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->shouldBeCalledTimes(3)->willReturn('Laptop-Dell-Latitude-E6400-2250903.html');
        $request->getQueryString()->willReturn(null);

        $hashIds = $this->prophesize(HashIds::class);
        $hashIds->getHashId('2250903')->willReturn('HASH');

        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getException()->willReturn(new NotFoundHttpException());
        $event->getRequest()->willReturn($request->reveal());
        $event->setResponse($expected)->shouldBeCalled();

        $redirect = new Redirect();
        $redirect->setUrl('/c/computacion/pc-portatil?q=Laptop%20Dell%20latitude');

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolve('product', 'HASH')->willThrow(SlugNotFoundException::class);
        $slugResolverService->resolveLegacy('Laptop-Dell-Latitude-E6400-2250903.html')->willReturn($redirect);

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $this->prophesize(RouterInterface::class)->reveal(),
            $hashIds->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }

    public function testIsIgnoringRedirectWhenExceptionIsNotAnExpectedOne()
    {
        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getRequest()->willReturn(new Request());
        $event->getException()->willReturn(new ProductNotFoundException('foo'));
        $event->setResponse(Argument::any())->shouldNotBeCalled();

        $hashIds = $this->prophesize(HashIds::class);
        $hashIds->getHashId(Argument::any())->shouldNotBeCalled();

        $router = $this->prophesize(RouterInterface::class);
        $router->match(Argument::any())->shouldNotBeCalled();
        $router->generate(Argument::any(), Argument::any(), Argument::any())
            ->shouldNotBeCalled();

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolveLegacy(Argument::any())->shouldNotBeCalled();

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $router->reveal(),
            $hashIds->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }

    public function testIsIgnoringRedirectWhenARouteIsNotFoundForTheRedirectUrl()
    {
        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->willReturn('/b/rocket/kicker');
        $request->getQueryString()->willReturn(null);

        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getRequest()->willReturn($request);
        $event->getException()->willReturn(new SlugNotFoundException('/b/rocket/kicker not found'));
        $event->setResponse(Argument::any())->shouldNotBeCalled();
        $event->setException(Argument::type(NotFoundHttpException::class))->shouldBeCalled();

        $redirect = new Redirect();
        $redirect->setUrl('/b/rocket/b/kicker');

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolveLegacy('/b/rocket/kicker')->shouldBeCalled()->willReturn($redirect);

        $hashIds = $this->prophesize(HashIds::class);
        $hashIds->getHashId(Argument::any())->shouldNotBeCalled();

        $router = $this->prophesize(RouterInterface::class);
        $router->match('/b/rocket/b/kicker')->shouldBeCalled()->willThrow(new ResourceNotFoundException());

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $router->reveal(),
            $hashIds->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }

    public function testIsChangingExceptionToNotFoundIfUnhandled()
    {
        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getRequest()->willReturn(new Request());
        $event->getException()->willReturn(new NotFoundHttpException('foo'));
        $event->setResponse(Argument::any())->shouldNotBeCalled();
        $event->setException(Argument::type(NotFoundHttpException::class))->shouldBeCalled();

        $hashIds = $this->prophesize(HashIds::class);
        $hashIds->getHashId(Argument::any())->shouldNotBeCalled();

        $router = $this->prophesize(RouterInterface::class);
        $router->match(Argument::any())->shouldNotBeCalled();
        $router->generate(Argument::any(), Argument::any(), Argument::any())
            ->shouldNotBeCalled();

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolveLegacy(Argument::any())->willThrow(SlugNotFoundException::class);

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $router->reveal(),
            $hashIds->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }

    /**
     * @dataProvider exceptionProvider
     *
     * @param Exception $exception
     */
    public function testIsPreventingRedirectLoopsWhenRouteFoundWithCombinedLegacyData(Exception $exception)
    {
        $request = $this->prophesize(Request::class);
        $request->getPathInfo()->shouldBeCalledTimes(2)->willReturn('/s/comtex');
        $request->getQueryString()->willReturn(null);

        $event = $this->prophesize(GetResponseForExceptionEvent::class);
        $event->getException()->willReturn($exception);
        $event->getRequest()->willReturn($request->reveal());
        $event->setResponse(Argument::any())->shouldNotBeCalled();
        $event->setException(Argument::type(NotFoundHttpException::class))->shouldBeCalled();

        $redirect = new Redirect();
        $redirect->setUrl('/s/comtex');

        $slugResolverService = $this->prophesize(SlugResolverService::class);
        $slugResolverService->resolveLegacy('/s/comtex')->willReturn($redirect);

        $resourceNotFoundListener = new ResourceNotFoundListener(
            $this->prophesize(RouterInterface::class)->reveal(),
            $this->prophesize(HashIds::class)->reveal(),
            $slugResolverService->reveal()
        );

        $resourceNotFoundListener->onKernelException($event->reveal());
    }
}
