<?php

declare(strict_types=1);

namespace Linio\Frontend\Transformer\EventListener;

use Linio\Frontend\EventListener\DataLayerListener;
use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class DataLayerListenerTest extends PHPUnit_Framework_TestCase
{
    public function testIsSettingNlsInSession()
    {
        $query = $this->prophesize(ParameterBag::class);
        $query->get('nls')->willReturn(300000);

        $request = $this->prophesize(Request::class);
        $request->query = $query->reveal();

        $session = $this->prophesize(SessionInterface::class);
        $session->set('data_layer/nls', 300000)->shouldBeCalled();

        $event = $this->prophesize(GetResponseEvent::class);
        $event->getRequest()->willReturn($request);

        $listener = new DataLayerListener($session->reveal());
        $listener->onKernelRequest($event->reveal());
    }
}
