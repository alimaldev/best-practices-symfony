<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use PHPUnit_Framework_TestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class RequestIdListenerTest extends PHPUnit_Framework_TestCase
{
    public function testItGeneratesANewRequestId()
    {
        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $request = new Request();
        $event = new GetResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST);

        $listener = new RequestIdListener();
        $listener->onKernelRequest($event);

        $this->assertNotEmpty($event->getRequest()->headers->get('X-Request-ID'));
    }

    public function testItUsesTheRequestIdFromTheOriginalRequest()
    {
        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $request = new Request();
        $request->headers->add(['X-Request-ID' => 'test']);
        $event = new GetResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST);

        $listener = new RequestIdListener();
        $listener->onKernelRequest($event);

        $this->assertEquals('test', $event->getRequest()->headers->get('X-Request-ID'));
    }

    public function testItAddsTheRequestIdToTheResponse()
    {
        $kernel = new HttpKernel(new EventDispatcher(), new ControllerResolver());
        $request = new Request();
        $request->headers->add(['X-Request-ID' => 'test']);
        $response = new Response();
        $event = new FilterResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);

        $listener = new RequestIdListener();
        $listener->onKernelResponse($event);

        $this->assertNotEmpty($event->getResponse()->headers->get('X-Request-ID'));
    }
}
