<?php

namespace Linio\Frontend\Search\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Linio\Frontend\Entity\Catalog\Aggregation\CategoryAggregation;
use Linio\Frontend\Entity\Catalog\Aggregation\Item;
use Linio\Frontend\Entity\Catalog\AggregationCollection;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Rating\RatingService;
use Linio\Frontend\Search\ResultSetMapper\AggregationMapper;
use Linio\Frontend\WishList\ManageWishList;
use Linio\Test\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class HawkAdapterTest extends UnitTestCase
{
    protected $didYouMeanThreshold = 3;

    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../../fixtures/search/results.yml');
    }

    protected function getSimilarResponseContents()
    {
        return '[
  {
    "title": "Reloj Touch Aviador F50 Watch",
    "brand": {
        "id": 22,
        "name": "Smart",
        "slug": "smart"
    },
    "id": "SM791FA16OTBLMX-1436588",
    "group_id": "SM791FA16OTBLMX",
    "price": 478,
    "special_price": 239,
    "discount": 0.5,
    "url": "reloj-touch-Aviador-f50-watch",
    "image": "http://media.linio.com.mx/p/smart-6398-3899471-1-zoom.jpg",
    "color": "Negro",
    "categories": [
      "relojes caballero",
      "relojes deportivos caballero"
    ],
    "main_category": "relojes deportivos caballero",
    "sku": "SM791FA16OTBLMX-1436588",
    "skuConfig": "SM791FA16OTBLMX"
  },
  {
    "title": "Reloj Bunker Watch Binario",
    "brand": {
        "id": 22,
        "name": "Smart",
        "slug": "smart"
    },
    "id": "SM791FA52OCHLMX-1436169",
    "group_id": "SM791FA52OCHLMX",
    "price": 478,
    "special_price": 239,
    "discount": 0.5,
    "url": "reloj-bunker-watch-binario",
    "url_referrer": "",
    "image": "http://media.linio.com.mx/p/smart-2952-7459471-1-zoom.jpg",
    "color": "Gris",
    "categories": [
      "relojes caballero",
      "relojes de moda caballero"
    ],
    "main_category": "relojes de moda caballero",
    "sku": "SM791FA52OCHLMX-1436169",
    "skuConfig": "SM791FA52OCHLMX"
  }
]';
    }

    protected function getSearchResponseContents()
    {
        return '{
    "count": 1,
    "items": [
        {
            "attributes": {
                "color": [
                    "Rosado"
                ],
                "max_item_to_sell": [
                    "0"
                ],
                "model": [
                    "100 ml"
                ],
                "package_height": [
                    "12.50"
                ],
                "package_length": [
                    "12.50"
                ],
                "package_weight": [
                    "0.50"
                ],
                "package_width": [
                    "12.50"
                ],
                "price_comparison": [
                    "1"
                ],
                "product_measures": [
                    "10 x 10 x 10"
                ],
                "product_warranty": [
                    "No aplica"
                ],
                "product_weight": [
                    "0.4"
                ],
                "short_description": [
                    "<ul><li>Fragancia de: Ralph Lauren.</li>\n   <li>Volumen: 100ml.</li>\n   <li>Para: Mujer.</li>\n   <li>Floral amaderado, Tierno, sutil, Romántico.</li>\n\n  \n   </ul>"
                ],
                "supplier_lead_time": [
                    "0"
                ],
                "url_price_comparison": [
                    "0"
                ],
                "visible_in_shop": [
                    "Linio"
                ],
                "free_pickup": [
                    "1"        
                ]
            },
            "brand": {
                "id": "1869",
                "name": "Ralph Lauren",
                "slug": "ralph-lauren"
            },
            "seller": {
                "id": 123,
                "name": "Foo Bar Baz S.A. de C.V.",
                "slug": "foo-bar-baz-sa-de-cv",
                "type": "supplier",
                "rating": 3,
                "operation_type": null,
                "international": true
            },
            "categories": [
                [
                    {
                        "id": "1",
                        "name": "Root Category",
                        "slug": "root-category"
                    },
                    {
                        "id": "266",
                        "name": "Salud y Cuidado personal",
                        "slug": "salud-y-cuidado-personal"
                    },
                    {
                        "id": "299",
                        "name": "Perfumes",
                        "slug": "perfumes"
                    },
                    {
                        "id": "492",
                        "name": "Perfumes para Mujer",
                        "slug": "perfumes-para-mujer"
                    }
                ]
            ],
            "id": "ar:A869HB29TNMLAAR",
            "price": {
                "currency": "usd",
                "current": 1400,
                "previous": 1500,
                "special_from_date": "2015-01-01",
                "special_to_date": "2015-02-01"
            },
            "skus": [
                "RA869HB29TNMLAAR-11882",
                "RA869HB29TNMLAAR"
            ],
            "stock_count": 1,
            "title": "Ralph Lauren Romance Eau de Parfum 100 ml para Mujer",
            "average_rating": 2,
            "payload": {
                "sku": "RA869HB29TNMLAAR",
                "slug": "ralph-lauren-romance-eau-de-parfum-100-ml-para-mujer",
                "free_shipping": true,
                "linio_plus_level": null,
                "delivery_time": 1,
                "is_international": true,
                "seller": {
                    "id": 123,
                    "name": "Foo Bar Baz S.A. de C.V.",
                    "slug": "foo-bar-baz-sa-de-cv",
                    "type": "supplier",
                    "rating": 3,
                    "operation_type": null
                },
                "images": [
                    {
                        "main": true,
                        "position": 1,
                        "slug": "/p/ralph-lauren-0937-07703-1"
                    }
                ]
            }
        }
    ]
}';
    }

    public function testIsSearching()
    {
        $expected = $this->fixtures['search_result_standard'];

        $facetCollection = new FacetCollection();

        $rating = new Rating();
        $rating->setAveragePoint(2);

        $responseMock = new Response(200, [], $this->getSearchResponseContents());

        $ratingService = $this->prophesize(RatingService::class);
        $ratingService->getBySku(Argument::type('string'))->willReturn($rating);

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => null,
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'info',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->add(['category' => 10]);
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setRatingService($ratingService->reveal());
        $adapter->setImageHost('foo-image-host');
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());
        $adapter->setManageWishList($manageWishList->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSendingPersonalisationCustomerDataWhenSearches()
    {
        $expected = $this->fixtures['search_result_standard'];

        $facetCollection = new FacetCollection();

        $rating = new Rating();
        $rating->setAveragePoint(2);

        $responseMock = new Response(200, [], $this->getSearchResponseContents());

        $ratingService = $this->prophesize(RatingService::class);
        $ratingService->getBySku(Argument::type('string'))->willReturn($rating);

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => null,
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'info',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->add(['category' => 10]);
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn([]);

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setRatingService($ratingService->reveal());
        $adapter->setImageHost('foo-image-host');
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());
        $adapter->setManageWishList($manageWishList->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }

    public function testIsHandlingEmptyResults()
    {
        $expected = $this->fixtures['search_result_empty'];

        $facetCollection = new FacetCollection();

        $responseMock = new Response(200, [], '{"count": 0,"items": []}');

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => null,
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'info',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->add(['category' => 10]);
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSearchinghWithDidYouMean()
    {
        $expected = $this->fixtures['search_result_did_you_mean'];
        $facetCollection = new FacetCollection();

        $responseMock = new Response(200, [],
            '{"count":0,"items":[],"dum":{"executed":"0","old_count":"0","q":"foo","sq":"foo_did_you_mean"}}');

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => 'foo',
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'replace',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->set('category', 10);
        $parameterBag->set('q', 'foo');
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection, true);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSearchReturningGuidedSearchTerms()
    {
        $expected = $this->fixtures['search_result_guided_search'];
        $facetCollection = new FacetCollection();

        $responseMock = new Response(200, [], '{"count":0,"items":[],"guided_search":["foo_guided_search"]}');

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => 'foo',
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'info',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->set('category', 10);
        $parameterBag->set('q', 'foo');
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSearchReturningRelatedQueries()
    {
        $expected = $this->fixtures['search_result_related_queries'];
        $facetCollection = new FacetCollection();

        $responseMock = new Response(200, [], '{"count":0,"items":[],"related_queries":["foo_related_query"]}');

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => 'foo',
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'info',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->set('category', 10);
        $parameterBag->set('q', 'foo');
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSearchReturningAggregations()
    {
        $expected = $this->fixtures['search_result_category_aggregation'];
        $facetCollection = new FacetCollection();

        $responseMock = new Response(200, [],
            '{"count":0,"items":[],"facets":{"category":[{"id":"1","text":"Root Category","count":18832,"deep":1,"items":[{"id":"9","text":"Hogar","count":97,"deep":2,"items":[{"id":"12","text":"Muebles","count":40,"deep":2}]}]}]}}');

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                        'q' => 'foo',
                        'size' => 60,
                        'page' => 1,
                        'gs' => 10,
                        'rq' => 8,
                        'dum' => 'info',
                        'dum_count' => $this->didYouMeanThreshold,
                        'category_dept' => 4,
                        'sort' => 'relevance',
                        'filters' => ['category' => ['10']],
                        'facets' => [],
                        'facet_count' => 100,
                        'facet_include_filtered' => true,
                        'identifiers' => [
                            [
                                'type' => 'user_id',
                                'value' => 'fd8sdfx',
                            ],
                            [
                                'type' => 'web_id',
                                'value' => 'fd8sdfx',
                            ],
                        ],
                    ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $item1 = new Item();
        $item1->setId('1');
        $item1->setCount(18832);
        $item2 = new Item();
        $item2->setId('9');
        $item2->setCount(97);
        $item3 = new Item();
        $item3->setId('12');
        $item3->setCount(40);

        $categoryAggregation = new CategoryAggregation();
        $categoryAggregation->addItem($item1);
        $categoryAggregation->addItem($item2);
        $categoryAggregation->addItem($item3);

        $aggregationCollection = new AggregationCollection();
        $aggregationCollection->set($categoryAggregation->getCode(), $categoryAggregation);

        $aggregationMapperMock = $this->prophesize(AggregationMapper::class);
        $aggregationMapperMock->map([
            'category' => [
                [
                    'key' => '1',
                    'count' => 18832,
                ],
                [
                    'key' => '9',
                    'count' => 97,
                ],
                [
                    'key' => '12',
                    'count' => 40,
                ],
            ],
        ])
            ->shouldBeCalled()
            ->willReturn($aggregationCollection);

        $expected->setAggregations($aggregationCollection);

        $parameterBag = new ParameterBag();
        $parameterBag->set('category', 10);
        $parameterBag->set('q', 'foo');
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setAggregationMapper($aggregationMapperMock->reveal());
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindingSimilar()
    {
        $expected = $this->fixtures['search_result_find_similar'];

        $responseMock = new Response(200, [], $this->getSimilarResponseContents());

        $clientMock = $this->prophesize(Client::class);
        $clientMock->getConfig('query')
            ->shouldBeCalled()
            ->willReturn(['key' => 'foo']);
        $clientMock->get('moreLikeThis',
            [
                'query' => [
                    'key' => 'foo',
                    'method' => 'default',
                    'sku' => 'MYSKU1234',
                ],
            ])
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $actual = $adapter->findSimilar('MYSKU1234');

        $this->assertEquals($expected, $actual);
    }

    public function testIsMappingActiveFilters()
    {
        $filters = [];
        $request = new ParameterBag([
            'category' => 1,
            'seller.id' => 42,
        ]);

        $facets = new FacetCollection();

        $adapter = new HawkAdapter();
        $adapter->setCountryCode('rs');
        $filters = $adapter->mapActiveFilters($filters, $request, $facets);

        $this->assertEquals([
            'category' => [1],
            'seller.id' => [42],
        ], $filters);
    }

    public function testIsSearchingWithSkusInWishLists()
    {
        $expected = $this->fixtures['search_result_wishlist_skus'];

        $facetCollection = new FacetCollection();

        $rating = new Rating();
        $rating->setAveragePoint(2);

        $responseMock = new Response(200, [], $this->getSearchResponseContents());

        $ratingService = $this->prophesize(RatingService::class);
        $ratingService->getBySku(Argument::type('string'))->willReturn($rating);

        $clientMock = $this->prophesize(Client::class);
        $clientMock->request(
            'POST',
            'search/?key=',
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => [
                    'q' => null,
                    'size' => 60,
                    'page' => 1,
                    'gs' => 10,
                    'rq' => 8,
                    'dum' => 'info',
                    'dum_count' => $this->didYouMeanThreshold,
                    'category_dept' => 4,
                    'sort' => 'relevance',
                    'filters' => ['category' => ['10']],
                    'facets' => [],
                    'facet_count' => 100,
                    'facet_include_filtered' => true,
                    'identifiers' => [
                        [
                            'type' => 'user_id',
                            'value' => 'fd8sdfx',
                        ],
                        [
                            'type' => 'web_id',
                            'value' => 'fd8sdfx',
                        ],
                    ],
                ],
            ]
        )
            ->shouldBeCalled()
            ->willReturn($responseMock);

        $parameterBag = new ParameterBag();
        $parameterBag->add(['category' => 10]);
        $category = new Category();
        $category->setId(10);

        $customer = new Customer();
        $customer->setLinioId('fd8sdfx');

        $token = $this->prophesize(TokenInterface::class);
        $token->getUser()->willReturn($customer);
        $tokenStorage = $this->prophesize(TokenStorageInterface::class);
        $tokenStorage->getToken()->willReturn($token->reveal());

        $manageWishList = $this->prophesize(ManageWishList::class);
        $manageWishList->getAllSkusInWishLists($customer)->willReturn(['RA869HB29TNMLAAR']);

        $adapter = new HawkAdapter();
        $adapter->setClient($clientMock->reveal());
        $adapter->setRatingService($ratingService->reveal());
        $adapter->setImageHost('foo-image-host');
        $adapter->setDidYouMeanThreshold($this->didYouMeanThreshold);
        $adapter->setTokenStorage($tokenStorage->reveal());
        $adapter->setManageWishList($manageWishList->reveal());

        $actual = $adapter->searchProducts($parameterBag, $category, $facetCollection);

        $this->assertEquals($expected, $actual);
    }
}
