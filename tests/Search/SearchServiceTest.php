<?php

namespace Linio\Frontend\Search;

use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Catalog\SearchResults;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\ParameterBag;

class SearchServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testIsSearchingProducts()
    {
        $query = new ParameterBag();
        $category = new Category();
        $adapterMock = $this->createMock(SearchAdapterInterface::class);
        $searchResult = new SearchResult();
        $expected = new SearchResults($searchResult);

        $adapterMock->expects($this->once())
            ->method('searchProducts')
            ->with($query, $category, new FacetCollection())
            ->willReturn($searchResult);

        $filterQueryMock = $this->prophesize(FilterQueryMapper::class);
        $filterQueryMock->map([], Argument::any())
            ->willReturn(new FacetCollection());

        $searchService = new SearchService();
        $searchService->setAdapter($adapterMock);
        $searchService->setFilterQueryMapper($filterQueryMock->reveal());

        $actual = $searchService->searchProducts($query, $category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsSearchingProductsWithDidYouMean()
    {
        $query = new ParameterBag();
        $query->add(['q' => 'tst']);

        $category = new Category();

        $originalSearchResult = new SearchResult();
        $originalSearchResult->setOriginalQuery('tst');
        $originalSearchResult->setDidYouMeanTerm('test');

        $didYouMeanSearchResult = new SearchResult();

        $expected = new SearchResults($originalSearchResult);
        $expected->setDidYouMeanSearchResult($didYouMeanSearchResult);

        $adapter = $this->prophesize(SearchAdapterInterface::class);
        $adapter->searchProducts($query, $category, new FacetCollection(), false)
            ->shouldBeCalled()
            ->willReturn($originalSearchResult);

        $adapter->searchProducts($query, $category, new FacetCollection(), true)
            ->shouldBeCalled()
            ->willReturn($didYouMeanSearchResult);

        $filterQueryMapper = $this->prophesize(FilterQueryMapper::class);
        $filterQueryMapper->map(['q' => 'tst'], Argument::any())
            ->shouldBeCalled()
            ->willReturn(new FacetCollection());

        $searchService = new SearchService();
        $searchService->setAdapter($adapter->reveal());
        $searchService->setFilterQueryMapper($filterQueryMapper->reveal());

        $actual = $searchService->searchProducts($query, $category);

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindingSimilarProducts()
    {
        $adapterMock = $this->createMock(SearchAdapterInterface::class);
        $expected = new SearchResult();

        $adapterMock->expects($this->once())
            ->method('findSimilar')
            ->with('MYSKU1234')
            ->willReturn($expected);

        $searchService = new SearchService();
        $searchService->setAdapter($adapterMock);

        $actual = $searchService->findSimilar('MYSKU1234');

        $this->assertSame($expected, $actual);
    }
}
