<?php

namespace Linio\Frontend\Search;

class SearchAwareTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGettingSearchService()
    {
        $service = $this->createMock(SearchService::class);

        $searchAware = $this->getObjectForTrait(SearchAware::class);
        $searchAware->setSearchService($service);

        $this->assertEquals($service, $searchAware->getSearchService());
    }
}
