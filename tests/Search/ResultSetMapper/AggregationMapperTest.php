<?php

namespace Linio\Frontend\Search\ResultSetMapper;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Catalog\Aggregation\BrandAggregation;
use Linio\Frontend\Entity\Catalog\Aggregation\Item;
use Linio\Frontend\Entity\Catalog\AggregationCollection;

class AggregationMapperTest extends \PHPUnit_Framework_TestCase
{
    public function testIsAddingCacheService()
    {
        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mapper = new AggregationMapper();
        $mapper->addCacheService('test', $cacheServiceMock);

        $result = $mapper->getCacheServices();

        $this->assertInternalType('array', $result);
        $this->assertCount(1, $result);
        $this->assertArrayHasKey('test', $result);
        $this->assertEquals($cacheServiceMock, $result['test']);
    }

    public function testIsMapping()
    {
        $resultSet = [
            'brand' => [
                ['key' => 'Item 1', 'count' => 10],
                ['key' => 'Item 2', 'count' => 20],
                ['key' => 'Item 3', 'count' => 30],
            ],
        ];

        $item1 = new Item();
        $item1->setId('Item 1');
        $item1->setName('Item 1');
        $item1->setCount(10);

        $item2 = new Item();
        $item2->setId('Item 2');
        $item2->setName('Item 2');
        $item2->setCount(20);

        $item3 = new Item();
        $item3->setId('Item 3');
        $item3->setName('Item 3');
        $item3->setCount(30);

        $brandAggregation = new BrandAggregation();
        $brandAggregation->addItem($item1);
        $brandAggregation->addItem($item2);
        $brandAggregation->addItem($item3);

        $mapper = new AggregationMapper();
        $actual = $mapper->map($resultSet);

        $this->assertInstanceOf(AggregationCollection::class, $actual);
        $this->assertCount(1, $actual);
        $this->assertEquals($actual->get(BrandAggregation::CODE), $brandAggregation);
    }

    public function testIsSkippingNotFoundAggregation()
    {
        $resultSet = [
            'aggnotfound' => [
                ['key' => 'Item 1', 'count' => 10],
                ['key' => 'Item 2', 'count' => 20],
                ['key' => 'Item 3', 'count' => 30],
            ],
        ];

        $mapper = new AggregationMapper();

        $result = $mapper->map($resultSet);

        $this->assertInstanceOf(AggregationCollection::class, $result);
        $this->assertTrue($result->isEmpty());
    }

    public function testIsGettingDataFromCache()
    {
        $item1 = new Item();
        $item1->setId(1000);
        $item1->setName('Brand 1');
        $item1->setCount(10);

        $item2 = new Item();
        $item2->setId(2000);
        $item2->setName('Brand 2');
        $item2->setCount(20);

        $item3 = new Item();
        $item3->setId(3000);
        $item3->setName('Brand 3');
        $item3->setCount(30);

        $brandAggregation = new BrandAggregation();
        $brandAggregation->addItem($item1);
        $brandAggregation->addItem($item2);
        $brandAggregation->addItem($item3);

        $resultSet = [
            'brand' => [
                ['key' => 1000, 'count' => 10],
                ['key' => 2000, 'count' => 20],
                ['key' => 3000, 'count' => 30],
            ],
        ];

        $cacheServiceMock = $this->getMockBuilder(CacheService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cacheServiceMock->expects($this->once())
            ->method('getMulti')
            ->with(['brand:1000', 'brand:2000', 'brand:3000'])
            ->willReturn([
                'mx:brand:1000' => ['id' => 1000, 'name' => 'Brand 1'],
                'mx:brand:2000' => ['id' => 2000, 'name' => 'Brand 2'],
                'mx:brand:3000' => ['id' => 3000, 'name' => 'Brand 3'],
            ]);

        $mapper = new AggregationMapper();
        $mapper->addCacheService('brand', $cacheServiceMock);

        $actual = $mapper->map($resultSet);

        $this->assertInstanceOf(AggregationCollection::class, $actual);
        $this->assertCount(1, $actual);
        $this->assertEquals($actual->get(BrandAggregation::CODE), $brandAggregation);
    }
}
