<?php

declare(strict_types=1);

namespace Linio\Frontend\Logging;

use PHPUnit_Framework_TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestIdMonologProcessorTest extends PHPUnit_Framework_TestCase
{
    public function testIsAddingRequestIdIfRequestExists()
    {
        $request = new Request();
        $request->headers->set('X-Request-ID', 'test');

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $processor = new RequestIdMonologProcessor();
        $processor->setRequestStack($requestStack);

        $expected = ['context' => ['requestId' => 'test', 'countryCode' => null]];
        $this->assertEquals($expected, $processor([]));
    }

    public function testIsSettingRequestIdToEmptyIfRequestDoesNotExist()
    {
        $requestStack = new RequestStack();
        $processor = new RequestIdMonologProcessor();
        $processor->setRequestStack($requestStack);

        $expected = ['context' => ['requestId' => '']];
        $this->assertEquals($expected, $processor([]));
    }

    public function testIsAddingCountryCode()
    {
        $request = new Request();
        $request->headers->set('X-Request-ID', 'test');

        $requestStack = new RequestStack();
        $requestStack->push($request);

        $processor = new RequestIdMonologProcessor();
        $processor->setRequestStack($requestStack);

        $processor = new RequestIdMonologProcessor();
        $processor->setRequestStack($requestStack);
        $processor->setCountryCode('MX');

        $expected = ['context' => ['requestId' => 'test', 'countryCode' => 'MX']];
        $this->assertEquals($expected, $processor([]));
    }
}
