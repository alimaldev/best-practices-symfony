<?php

namespace Linio\Frontend\Category;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Test\UnitTestCase;

class CategoryServiceTest extends UnitTestCase
{
    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../fixtures/category/tree.yml');
    }
    public function getRawRootCategory()
    {
        return json_decode('{"children":[{"id":"5","name":"TV, Audio y Video","slug":"tv-audio-y-video","url_key":"tv-audio-y-video"}],"id":"1","name":"Root Category","path":[{"id":"1","name":"Root Category","slug":"root-category","url_key":"root-category"}],"slug":"root-category","url_key":"root-category"}', true);
    }

    public function getRawLevel2Category()
    {
        return json_decode('{"children":[{"id":"3","name":"Tv y Video","slug":"tv-y-video","url_key":"tv-y-video"}],"id":"2","name":"TV, Audio y Video","path":[{"id":"1","name":"Root Category","slug":"root-category","url_key":"root-category"},{"id":"2","name":"TV, Audio y Video","slug":"tv-audio-y-video","url_key":"tv-audio-y-video"}],"slug":"tv-audio-y-video","url_key":"tv-audio-y-video"}', true);
    }

    public function getRawLevel3Category()
    {
        return json_decode('{"children":[{"id":"4","name":"Televisores","slug":"televisores","url_key":"televisores"}],"id":"3","name":"Tv y Video","path":[{"id":"1","name":"Root Category","slug":"root-category","url_key":"root-category"},{"id":"2","name":"TV, Audio y Video","slug":"tv-audio-y-video","url_key":"tv-audio-y-video"},{"id":"3","name":"Tv y Video","slug":"tv-y-video","url_key":"tv-y-video"}],"slug":"tv-y-video","url_key":"tv-y-video"}', true);
    }

    public function getRawLevel4Category()
    {
        return json_decode('{"children":[{"id":"5","name":"4K TV","slug":"4k-tv","url_key":"4k-tv"}],"id":"4","name":"Televisores","path":[{"id":"1","name":"Root Category","slug":"root-category","url_key":"root-category"},{"id":"2","name":"TV, Audio y Video","slug":"tv-audio-y-video","url_key":"tv-audio-y-video"},{"id":"3","name":"Tv y Video","slug":"tv-y-video","url_key":"tv-y-video"},{"id":"4","name":"Televisores","slug":"televisores","url_key":"televisores"}],"slug":"televisores","url_key":"televisores"}', true);
    }

    public function getRawLevel5Category()
    {
        return json_decode('{"id":"5","name":"4K TV","path":[{"id":"1","name":"Root Category","slug":"root-category","url_key":"root-category"},{"id":"2","name":"TV, Audio y Video","slug":"tv-audio-y-video","url_key":"tv-audio-y-video"},{"id":"3","name":"Tv y Video","slug":"tv-y-video","url_key":"tv-y-video"},{"id":"4","name":"Televisores","slug":"televisores","url_key":"televisores"},{"id":"5","name":"4K TV","slug":"4k-tv","url_key":"4k-tv"}],"slug":"4k-tv","url_key":"4k-tv"}', true);
    }

    public function testIsGettingFirstParentWhenItsTheRootCategory()
    {
        /** @var Category $expected */
        $expected = $this->fixtures['category_root'];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get(CategoryService::ROOT_CATEGORY)
            ->shouldBeCalled()
            ->willReturn($this->getRawRootCategory());

        $categoryService = new CategoryService();
        $categoryService->setCacheService($cacheServiceMock->reveal());

        $actual = $categoryService->getFirstParent($this->fixtures['category_root']);

        $this->assertEquals($actual->getId(), $expected->getId());
    }

    public function testIsGettingFirstParentWhenItsASecondLevelCategory()
    {
        /** @var Category $expected */
        $expected = $this->fixtures['category_level_2'];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get(CategoryService::ROOT_CATEGORY)
            ->shouldBeCalled()
            ->willReturn($this->getRawRootCategory());
        $cacheServiceMock->get(2)
            ->shouldBeCalled()
            ->willReturn($this->getRawLevel2Category());

        $categoryService = new CategoryService();
        $categoryService->setCacheService($cacheServiceMock->reveal());

        $actual = $categoryService->getFirstParent($this->fixtures['category_level_2']);

        $this->assertEquals($actual->getId(), $expected->getId());
    }

    public function testIsGettingFirstParentWhenItsAThirdLevelCategory()
    {
        /** @var Category $expected */
        $expected = $this->fixtures['category_level_2'];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get(CategoryService::ROOT_CATEGORY)
            ->shouldBeCalled()
            ->willReturn($this->getRawRootCategory());
        $cacheServiceMock->get(2)
            ->shouldBeCalled()
            ->willReturn($this->getRawLevel2Category());

        $categoryService = new CategoryService();
        $categoryService->setCacheService($cacheServiceMock->reveal());

        $actual = $categoryService->getFirstParent($this->fixtures['category_level_3']);

        $this->assertEquals($actual->getId(), $expected->getId());
    }

    public function testIsBuildingCategoryTree()
    {
        $categoryLevel2 = $this->fixtures['category_level_2'];

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get(CategoryService::ROOT_CATEGORY)
            ->shouldBeCalled()
            ->willReturn($this->getRawRootCategory());
        $cacheServiceMock->get(2)
            ->shouldBeCalled()
            ->willReturn($this->getRawLevel2Category());
        $cacheServiceMock->get(3)
            ->shouldBeCalled()
            ->willReturn($this->getRawLevel3Category());
        $cacheServiceMock->get(4)
            ->shouldBeCalled()
            ->willReturn($this->getRawLevel4Category());
        $cacheServiceMock->get(5)
            ->shouldBeCalled()
            ->willReturn($this->getRawLevel5Category());

        $categoryService = new CategoryService();
        $categoryService->setCacheService($cacheServiceMock->reveal());

        $categoryTree = $categoryService->buildTree(5);

        $actualCurrentCategory = $categoryTree->getChildren()[2]->getChildren()[3]->getChildren()[4]->getChildren()[5];

        $this->assertEquals($categoryLevel2->getId(), $categoryTree->getId());
        $this->assertEquals(2, $categoryTree->getChildren()[2]->getId());
        $this->assertEquals(3, $categoryTree->getChildren()[2]->getChildren()[3]->getId());
        $this->assertTrue($actualCurrentCategory->isCurrent());
    }

    public function testIsThrowingExceptionWhenCategoryNotFound()
    {
        $this->setExpectedException(CategoryNotFoundException::class);

        $cacheServiceMock = $this->prophesize(CacheService::class);
        $cacheServiceMock->get(123)
            ->shouldBeCalled()
            ->willReturn(null);

        $categoryService = new CategoryService();
        $categoryService->setCacheService($cacheServiceMock->reveal());

        $categoryService->getCategory(123);
    }
}
