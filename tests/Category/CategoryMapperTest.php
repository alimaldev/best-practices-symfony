<?php

namespace Linio\Frontend\Category;

use Linio\Frontend\Entity\Catalog\Category;

class CategoryMapperTest extends \PHPUnit_Framework_TestCase
{
    public function testIsMappingOneCategory()
    {
        $resultSet = [
            'id' => 1,
            'name' => 'Root Category',
            'slug' => 'root',
            'url_key' => 'root',
            'path' => [
                [
                    'id' => 2,
                    'name' => 'Path 1',
                    'slug' => 'path-1',
                    'url_key' => 'path-1',
                ],
                [
                    'id' => 3,
                    'name' => 'Path 2',
                    'slug' => 'path-2',
                    'url_key' => 'path-2',
                ],
            ],
            'children' => [
                [
                    'id' => 1000,
                    'name' => 'Category 1',
                    'slug' => 'category-1',
                    'url_key' => 'category-1',
                ],
                [
                    'id' => 2000,
                    'name' => 'Category 2',
                    'slug' => 'category-2',
                    'url_key' => 'category-2',
                ],
            ],
        ];

        $expected = new Category();
        $expected->setId(1);
        $expected->setName('Root Category');
        $expected->setSlug('root');
        $expected->setUrlKey('root');

        $path1 = new Category();
        $path1->setId(2);
        $path1->setName('Path 1');
        $path1->setSlug('path-1');
        $path1->setUrlKey('path-1');

        $path2 = new Category();
        $path2->setId(3);
        $path2->setName('Path 2');
        $path2->setSlug('path-2');
        $path2->setUrlKey('path-2');

        $category1 = new Category();
        $category1->setId(1000);
        $category1->setName('Category 1');
        $category1->setSlug('category-1');
        $category1->setUrlKey('category-1');
        $category1->setParent($expected);

        $category2 = new Category();
        $category2->setId(2000);
        $category2->setName('Category 2');
        $category2->setSlug('category-2');
        $category2->setUrlKey('category-2');
        $category2->setParent($expected);

        $expected->addPath($path1);
        $expected->addPath($path2);
        $expected->addChild($category1);
        $expected->addChild($category2);

        $mapper = new CategoryMapper();

        $actual = $mapper->map($resultSet);

        $this->assertEquals($expected, $actual);
    }

    public function testIsMappingMultipleCategories()
    {
        $resultSet = [
            [
                'id' => 1,
                'name' => 'Root Category',
                'slug' => 'root',
                'url_key' => 'root',
                'path' => [
                    [
                        'id' => 2,
                        'name' => 'Path 1',
                        'slug' => 'path-1',
                        'url_key' => 'path-1',
                    ],
                    [
                        'id' => 3,
                        'name' => 'Path 2',
                        'slug' => 'path-2',
                        'url_key' => 'path-2',
                    ],
                ],
                'children' => [
                    [
                        'id' => 1000,
                        'name' => 'Category 1',
                        'slug' => 'category-1',
                        'url_key' => 'category-1',
                    ],
                    [
                        'id' => 2000,
                        'name' => 'Category 2',
                        'slug' => 'category-2',
                        'url_key' => 'category-2',
                    ],
                ],
            ],
            [
                'id' => 4,
                'name' => 'Parent Category',
                'slug' => 'parent',
                'url_key' => 'parent',
                'path' => [
                    [
                        'id' => 5,
                        'name' => 'Path 5',
                        'slug' => 'path-5',
                        'url_key' => 'path-5',
                    ],
                    [
                        'id' => 6,
                        'name' => 'Path 6',
                        'slug' => 'path-6',
                        'url_key' => 'path-6',
                    ],
                ],
                'children' => [
                    [
                        'id' => 3000,
                        'name' => 'Category 3',
                        'slug' => 'category-3',
                        'url_key' => 'category-3',
                    ],
                    [
                        'id' => 4000,
                        'name' => 'Category 4',
                        'slug' => 'category-4',
                        'url_key' => 'category-4',
                    ],
                ],
            ],
        ];

        $rootCategory = new Category();
        $rootCategory->setId(1);
        $rootCategory->setName('Root Category');
        $rootCategory->setSlug('root');
        $rootCategory->setUrlKey('root');

        $path1 = new Category();
        $path1->setId(2);
        $path1->setName('Path 1');
        $path1->setSlug('path-1');
        $path1->setUrlKey('path-1');

        $path2 = new Category();
        $path2->setId(3);
        $path2->setName('Path 2');
        $path2->setSlug('path-2');
        $path2->setUrlKey('path-2');

        $category1 = new Category();
        $category1->setId(1000);
        $category1->setName('Category 1');
        $category1->setSlug('category-1');
        $category1->setUrlKey('category-1');
        $category1->setParent($rootCategory);

        $category2 = new Category();
        $category2->setId(2000);
        $category2->setName('Category 2');
        $category2->setSlug('category-2');
        $category2->setUrlKey('category-2');
        $category2->setParent($rootCategory);

        $parentCategory = new Category();
        $parentCategory->setId(4);
        $parentCategory->setName('Parent Category');
        $parentCategory->setSlug('parent');
        $parentCategory->setUrlKey('parent');

        $path3 = new Category();
        $path3->setId(5);
        $path3->setName('Path 5');
        $path3->setSlug('path-5');
        $path3->setUrlKey('path-5');

        $path6 = new Category();
        $path6->setId(6);
        $path6->setName('Path 6');
        $path6->setSlug('path-6');
        $path6->setUrlKey('path-6');

        $category3 = new Category();
        $category3->setId(3000);
        $category3->setName('Category 3');
        $category3->setSlug('category-3');
        $category3->setUrlKey('category-3');
        $category3->setParent($parentCategory);

        $category4 = new Category();
        $category4->setId(4000);
        $category4->setName('Category 4');
        $category4->setSlug('category-4');
        $category4->setUrlKey('category-4');
        $category4->setParent($parentCategory);

        $rootCategory->addPath($path1);
        $rootCategory->addPath($path2);
        $rootCategory->addChild($category1);
        $rootCategory->addChild($category2);

        $parentCategory->addPath($path3);
        $parentCategory->addPath($path6);
        $parentCategory->addChild($category3);
        $parentCategory->addChild($category4);

        $expected = [
            $rootCategory,
            $parentCategory,
        ];

        $mapper = new CategoryMapper();

        $actual = $mapper->map($resultSet);

        $this->assertEquals($expected, $actual);
    }
}
