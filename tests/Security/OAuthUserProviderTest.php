<?php

declare(strict_types=1);

namespace Linio\Frontend\Security;

use HWI\Bundle\OAuthBundle\OAuth\ResourceOwnerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use Linio\Frontend\Communication\Customer\CustomerService;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Security\OAuth\OauthToken;
use Prophecy\Argument;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class OAuthUserProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsLoadingUserFromOAuthResponseCredentials()
    {
        $user = new User();
        $customer = new Customer();

        $resourceOwner = $this->prophesize(ResourceOwnerInterface::class);
        $resourceOwner->getName()->willReturn('facebook');

        $userResponse = $this->prophesize(UserResponseInterface::class);
        $userResponse->getEmail()->willReturn('Jimi');
        $userResponse->getResourceOwner()->willReturn($resourceOwner->reveal());
        $userResponse->getUsername()->willReturn('jimi_hendrix');
        $userResponse->getFirstName()->willReturn('Jimi');
        $userResponse->getLastName()->willReturn('Hendrix');

        $customerService = $this->prophesize(CustomerService::class);
        $customerService->loginOAuth(Argument::type(OAuthToken::class))
            ->willReturn($customer);

        $oauthProvider = new OAuthUserProvider();
        $oauthProvider->setCustomerService($customerService->reveal());

        $actual = $oauthProvider->loadUserByOAuthUserResponse($userResponse->reveal());

        $this->assertEquals($user, $actual);
    }

    public function testIsThrowingAnExceptionWhenProvidedBadOAuthCredentials()
    {
        $resourceOwner = $this->prophesize(ResourceOwnerInterface::class);
        $resourceOwner->getName()->willReturn('facebook');

        $userResponse = $this->prophesize(UserResponseInterface::class);
        $userResponse->getEmail()->willReturn('Jimi');
        $userResponse->getResourceOwner()->willReturn($resourceOwner->reveal());
        $userResponse->getUsername()->willReturn('jimi_hendrix');
        $userResponse->getFirstName()->willReturn('Jimi');
        $userResponse->getLastName()->willReturn('Hendrix');

        $customerService = $this->prophesize(CustomerService::class);
        $customerService->loginOAuth(Argument::type(OAuthToken::class))
            ->willThrow(new InvalidLoginCredentialsException('invalid credentials'));

        $oauthProvider = new OAuthUserProvider();
        $oauthProvider->setCustomerService($customerService->reveal());

        $this->expectException(UsernameNotFoundException::class);
        $this->expectExceptionMessage('invalid credentials');

        $oauthProvider->loadUserByOAuthUserResponse($userResponse->reveal());
    }
}
