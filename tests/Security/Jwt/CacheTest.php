<?php

namespace Linio\Frontend\Security\JWT;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Security\Jwt\Cache;
use Linio\Frontend\Security\User;

class CacheTest extends \PHPUnit_Framework_TestCase
{
    public function testIsStoringToken()
    {
        $token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NTk0NTA5NjAsInVzZXJuYW1lIjoidXNlciIsImlwIjoiOjoxIiwiaWF0IjoiMTQ1OTM2NDU2MCJ9.
        aUtWWM9Mkhfl-HlryuO_YOekjUeCjvwU8CxjsMzHPQ6G_nD0o_rwfgvf-hCDJPUHpTzFTxp5lv1Uxs5XDqwi-PT61
        KBzn5uEqh7Skwp_weW8nxyibRvu-R3oQk5q-lUw6bQPFbPr0RwgORdwBWnu5Osqpv4vGcafKJ3cdSDA_XrHtQ-pbo
        R5_zx0O37psxkwhmBd37wt1pL8EWdiXrknEu8Zk6vPtnxxBpwehPWMuY2u7psGOtLLK4AoAZa0Hkr52_HCmebNP0A
        qr3QNS0gogbVzHNyZaY-fsmBsACToF7ow5iK3udRxJ85z8H5LOgJ8RLi4W4VYh9kS74nDx13esuT7eYNMrgC8S4bs
        X64or-6sVqnIfZjwefufcw3sAFgPqXQs6tg9Wp62T4SQzIYge9lj62szLGEUZn-GJ1-URjMJ7IfzBKGb-Iq62ntqH
        _KCVDBIlNodQSo2syxR4cWi-D0-_4RQWgtjJxQ6sJDNOOVWijtbCuipwA4TFuc8bsr4A6lud8YGcZgZR6v-mGyFAH
        9hxgoCLd-WhIv5LtgwoJ4JMad1EaKZSWM098VL7LhQwjm3aP6Ulu43z9InmEt1MGKRvejt-kj4sDYrcA1s7_4blqj
        QegGA7k_YXpx6G1dHsGwGU5k_Hha12K_MkLIN3HygJDrknpgTaY7S1HEuQ4';

        $user = new User();
        $user->setId(201289);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('201289')->willReturn(['03b6d95b527032683a959d61f7848e88']);
        $cacheService->set('201289', ['03b6d95b527032683a959d61f7848e88', 'c5bf1fb8b26fc11deaa504cee736cd52'])->willReturn(true);

        $jwtCache = new Cache($cacheService->reveal());
        $jwtCache->add($token, $user);
    }

    public function testIsRemovingToken()
    {
        $token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NTk0NTA5NjAsInVzZXJuYW1lIjoidXNlciIsImlwIjoiOjoxIiwiaWF0IjoiMTQ1OTM2NDU2MCJ9.
        aUtWWM9Mkhfl-HlryuO_YOekjUeCjvwU8CxjsMzHPQ6G_nD0o_rwfgvf-hCDJPUHpTzFTxp5lv1Uxs5XDqwi-PT61
        KBzn5uEqh7Skwp_weW8nxyibRvu-R3oQk5q-lUw6bQPFbPr0RwgORdwBWnu5Osqpv4vGcafKJ3cdSDA_XrHtQ-pbo
        R5_zx0O37psxkwhmBd37wt1pL8EWdiXrknEu8Zk6vPtnxxBpwehPWMuY2u7psGOtLLK4AoAZa0Hkr52_HCmebNP0A
        qr3QNS0gogbVzHNyZaY-fsmBsACToF7ow5iK3udRxJ85z8H5LOgJ8RLi4W4VYh9kS74nDx13esuT7eYNMrgC8S4bs
        X64or-6sVqnIfZjwefufcw3sAFgPqXQs6tg9Wp62T4SQzIYge9lj62szLGEUZn-GJ1-URjMJ7IfzBKGb-Iq62ntqH
        _KCVDBIlNodQSo2syxR4cWi-D0-_4RQWgtjJxQ6sJDNOOVWijtbCuipwA4TFuc8bsr4A6lud8YGcZgZR6v-mGyFAH
        9hxgoCLd-WhIv5LtgwoJ4JMad1EaKZSWM098VL7LhQwjm3aP6Ulu43z9InmEt1MGKRvejt-kj4sDYrcA1s7_4blqj
        QegGA7k_YXpx6G1dHsGwGU5k_Hha12K_MkLIN3HygJDrknpgTaY7S1HEuQ4';

        $user = new User();
        $user->setId(201289);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('201289')->willReturn(['03b6d95b527032683a959d61f7848e88', 'c5bf1fb8b26fc11deaa504cee736cd52']);
        $cacheService->set('201289', ['03b6d95b527032683a959d61f7848e88'])->willReturn(true);

        $jwtCache = new Cache($cacheService->reveal());
        $jwtCache->remove($token, $user);
    }

    public function testIsFlushingTokens()
    {
        $user = new User();
        $user->setId(201289);

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->set('201289', [])->willReturn(true);

        $jwtCache = new Cache($cacheService->reveal());
        $jwtCache->flushTokens($user);
    }

    public function testIsFindingToken()
    {
        $user = new User();
        $user->setId(201289);

        $jwt = '18162a.212b60a25d.e3de782eabdc6421';

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('201289')->willReturn([md5($jwt)]);

        $jwtCache = new Cache($cacheService->reveal());

        $actual = $jwtCache->tokenExists($jwt, $user);

        $this->assertTrue($actual);
    }

    public function testIsDetectingTokenNotFound()
    {
        $user = new User();
        $user->setId(201289);

        $jwt = ['18162a212b60a25de3de782eabdc6421'];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get('201289')->willReturn([$jwt]);

        $jwtCache = new Cache($cacheService->reveal());

        $actual = $jwtCache->tokenExists('token', $user);

        $this->assertFalse($actual);
    }
}
