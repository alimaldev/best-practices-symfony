<?php

namespace Linio\Frontend\Security\JWT;

use Linio\Frontend\Security\Jwt\TokenExtractor;
use Linio\Frontend\Security\Jwt\TokenManager;
use Linio\Frontend\Security\Jwt\TokenStorage;
use Linio\Frontend\Security\User;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;

class TokenManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testIsAddingToken()
    {
        $user = new User();

        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NTk0NTA5NjAsInVzZXJuYW1lIjoidXNlciIsImlwIjoiOjoxIiwiaWF0IjoiMTQ1OTM2NDU2MCJ9.
        aUtWWM9Mkhfl-HlryuO_YOekjUeCjvwU8CxjsMzHPQ6G_nD0o_rwfgvf-hCDJPUHpTzFTxp5lv1Uxs5XDqwi-PT61
        KBzn5uEqh7Skwp_weW8nxyibRvu-R3oQk5q-lUw6bQPFbPr0RwgORdwBWnu5Osqpv4vGcafKJ3cdSDA_XrHtQ-pbo
        R5_zx0O37psxkwhmBd37wt1pL8EWdiXrknEu8Zk6vPtnxxBpwehPWMuY2u7psGOtLLK4AoAZa0Hkr52_HCmebNP0A
        qr3QNS0gogbVzHNyZaY-fsmBsACToF7ow5iK3udRxJ85z8H5LOgJ8RLi4W4VYh9kS74nDx13esuT7eYNMrgC8S4bs
        X64or-6sVqnIfZjwefufcw3sAFgPqXQs6tg9Wp62T4SQzIYge9lj62szLGEUZn-GJ1-URjMJ7IfzBKGb-Iq62ntqH
        _KCVDBIlNodQSo2syxR4cWi-D0-_4RQWgtjJxQ6sJDNOOVWijtbCuipwA4TFuc8bsr4A6lud8YGcZgZR6v-mGyFAH
        9hxgoCLd-WhIv5LtgwoJ4JMad1EaKZSWM098VL7LhQwjm3aP6Ulu43z9InmEt1MGKRvejt-kj4sDYrcA1s7_4blqj
        QegGA7k_YXpx6G1dHsGwGU5k_Hha12K_MkLIN3HygJDrknpgTaY7S1HEuQ4';

        $jwtStorage = $this->prophesize(TokenStorage::class);
        $jwtStorage->add($jwt, $user)->shouldBeCalled();

        $manager = new TokenManager($jwtStorage->reveal());
        $manager->add($jwt, $user);
    }

    public function testIsRemovingToken()
    {
        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NjAwNjUxMDUsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsInVzZXJfaWQiOjEyNjk1LCJpYXQiOiIxNDU5OTc4NzA1In0
        .J9Gmo-rEtxjR6Xi4-4P0pqHm31ppaXmPusUoGbk6lrjKS9TipRuwSFinkLxUIoIyc4ZkQjstcnGIjZ7sNCYl2csSB5mVZbhu_LkTz-NGme
        6MUvL7mEjlEB0-5WcgUI1oBqVgJ8RE4XjwpCQuEYgpr2JoV_XK9c1qyx_-BvC9QZmcw6MNNuFryurFvtnJF-KUfkzbUB1-UJsOFo4HjlaI5
        lNtpadQdqo9vdH6-XNc-xz-BXsKixI5Wl0hUaTN5Pj9rU6bre5P3-4IgHqwpJD_njj8OjBZLofD6msd9KqifrSxRR20Rw4UBm1Wzuq86Mby
        tLl7q4d52Yky6S5yW6b2k2W9DrvJ8ZE3-jjzJD6Q1mcZlhDdoLQVfmKqq4z_5D90Qj8JMJuR_33qxrjkvba3SHj46NKXBoY9b9zwMiUCPTp
        h0TTZgAL6MjA7-B4Sb78vFtqwzcucmy5tqNj61YSfutgE7_bkV0OEBFMwU3irXRub3XVSEN3mJrXkqx3OHdmP1u-0Nb8o1Scsq_wjkOZtT_
        3BBJyIn-xtK-wGNpqqScZX-H92PVoCRDEy2OFL8LaNS6Dy3h3zg5ZSjbQLiQIkOxZ1YZ2R6kflpaWYhAqhJZ3OZAgc8Mi5BQY_91u5dYBJi
        GGYSDQeClY1SmZPrcGr3UiQTHQkCVT-vfxqv7HnB-s';

        $jwtStorage = $this->prophesize(TokenStorage::class);
        $jwtStorage->remove($jwt, Argument::type(User::class))->shouldBeCalled();

        $manager = new TokenManager($jwtStorage->reveal());
        $manager->remove($jwt);
    }

    public function testIsRemovingTokenOfUserThatComesFromJwtPayload()
    {
        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NjAwNjUxMDUsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsInVzZXJfaWQiOjEyNjk1LCJpYXQiOiIxNDU5OTc4NzA1In0.
        J9Gmo-rEtxjR6Xi4-4P0pqHm31ppaXmPusUoGbk6lrjKS9TipRuwSFinkLxUIoIyc4ZkQjstcnGIjZ7sNCYl2csSB5mVZbhu_LkTz-NGme6M
        UvL7mEjlEB0-5WcgUI1oBqVgJ8RE4XjwpCQuEYgpr2JoV_XK9c1qyx_-BvC9QZmcw6MNNuFryurFvtnJF-KUfkzbUB1-UJsOFo4HjlaI5lNt
        padQdqo9vdH6-XNc-xz-BXsKixI5Wl0hUaTN5Pj9rU6bre5P3-4IgHqwpJD_njj8OjBZLofD6msd9KqifrSxRR20Rw4UBm1Wzuq86MbytLl7
        q4d52Yky6S5yW6b2k2W9DrvJ8ZE3-jjzJD6Q1mcZlhDdoLQVfmKqq4z_5D90Qj8JMJuR_33qxrjkvba3SHj46NKXBoY9b9zwMiUCPTph0TTZ
        gAL6MjA7-B4Sb78vFtqwzcucmy5tqNj61YSfutgE7_bkV0OEBFMwU3irXRub3XVSEN3mJrXkqx3OHdmP1u-0Nb8o1Scsq_wjkOZtT_3BBJyI
        n-xtK-wGNpqqScZX-H92PVoCRDEy2OFL8LaNS6Dy3h3zg5ZSjbQLiQIkOxZ1YZ2R6kflpaWYhAqhJZ3OZAgc8Mi5BQY_91u5dYBJiGGYSDQe
        ClY1SmZPrcGr3UiQTHQkCVT-vfxqv7HnB-s';

        $jwtStorage = $this->prophesize(TokenStorage::class);
        $jwtStorage->remove($jwt, Argument::type(User::class))->shouldBeCalled();

        $manager = new TokenManager($jwtStorage->reveal());
        $manager->remove($jwt);
    }

    public function testIsFlushingTokens()
    {
        $user = new User();

        $jwtStorage = $this->prophesize(TokenStorage::class);
        $jwtStorage->flushTokens($user)->shouldBeCalled();

        $manager = new TokenManager($jwtStorage->reveal());
        $manager->flushTokens($user);
    }

    public function testIsRemovingTokenFromRequest()
    {
        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NjAwNjUxMDUsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsInVzZXJfaWQiOjEyNjk1LCJpYXQiOiIxNDU5OTc4NzA1In0
        .J9Gmo-rEtxjR6Xi4-4P0pqHm31ppaXmPusUoGbk6lrjKS9TipRuwSFinkLxUIoIyc4ZkQjstcnGIjZ7sNCYl2csSB5mVZbhu_LkTz-NGme
        6MUvL7mEjlEB0-5WcgUI1oBqVgJ8RE4XjwpCQuEYgpr2JoV_XK9c1qyx_-BvC9QZmcw6MNNuFryurFvtnJF-KUfkzbUB1-UJsOFo4HjlaI5
        lNtpadQdqo9vdH6-XNc-xz-BXsKixI5Wl0hUaTN5Pj9rU6bre5P3-4IgHqwpJD_njj8OjBZLofD6msd9KqifrSxRR20Rw4UBm1Wzuq86Mby
        tLl7q4d52Yky6S5yW6b2k2W9DrvJ8ZE3-jjzJD6Q1mcZlhDdoLQVfmKqq4z_5D90Qj8JMJuR_33qxrjkvba3SHj46NKXBoY9b9zwMiUCPTp
        h0TTZgAL6MjA7-B4Sb78vFtqwzcucmy5tqNj61YSfutgE7_bkV0OEBFMwU3irXRub3XVSEN3mJrXkqx3OHdmP1u-0Nb8o1Scsq_wjkOZtT_
        3BBJyIn-xtK-wGNpqqScZX-H92PVoCRDEy2OFL8LaNS6Dy3h3zg5ZSjbQLiQIkOxZ1YZ2R6kflpaWYhAqhJZ3OZAgc8Mi5BQY_91u5dYBJi
        GGYSDQeClY1SmZPrcGr3UiQTHQkCVT-vfxqv7HnB-s';

        $request = $this->prophesize(Request::class);

        $tokenExtractor = $this->prophesize(TokenExtractor::class);
        $tokenExtractor->extract($request->reveal())->willReturn($jwt);

        $jwtStorage = $this->prophesize(TokenStorage::class);
        $jwtStorage->remove($jwt, Argument::type(User::class))->shouldBeCalled();

        $manager = new TokenManager($jwtStorage->reveal());
        $manager->setTokenExtractor($tokenExtractor->reveal());
        $manager->removeTokenExtractedFromRequest($request->reveal());
    }

    public function testIsExtractingTokenFromRequest()
    {
        $request = new Request();
        $jwt = 'eyJhbGc.iOiJSUz.I1NiIsInR5cCI6IkpXUyJ';

        $tokenExtractor = $this->prophesize(TokenExtractor::class);
        $tokenExtractor->extract($request)->willReturn($jwt);

        $tokenManager = new TokenManager($this->prophesize(TokenStorage::class)->reveal());
        $tokenManager->setTokenExtractor($tokenExtractor->reveal());

        $actual = $tokenManager->getTokenFromRequest($request);

        $this->assertEquals($actual, $jwt);
    }

    public function testIsValidatingIfTokenExists()
    {
        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NjAwNjUxMDUsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsInVzZXJfaWQiOjEyNjk1LCJpYXQiOiIxNDU5OTc4NzA1In0
        .J9Gmo-rEtxjR6Xi4-4P0pqHm31ppaXmPusUoGbk6lrjKS9TipRuwSFinkLxUIoIyc4ZkQjstcnGIjZ7sNCYl2csSB5mVZbhu_LkTz-NGme
        6MUvL7mEjlEB0-5WcgUI1oBqVgJ8RE4XjwpCQuEYgpr2JoV_XK9c1qyx_-BvC9QZmcw6MNNuFryurFvtnJF-KUfkzbUB1-UJsOFo4HjlaI5
        lNtpadQdqo9vdH6-XNc-xz-BXsKixI5Wl0hUaTN5Pj9rU6bre5P3-4IgHqwpJD_njj8OjBZLofD6msd9KqifrSxRR20Rw4UBm1Wzuq86Mby
        tLl7q4d52Yky6S5yW6b2k2W9DrvJ8ZE3-jjzJD6Q1mcZlhDdoLQVfmKqq4z_5D90Qj8JMJuR_33qxrjkvba3SHj46NKXBoY9b9zwMiUCPTp
        h0TTZgAL6MjA7-B4Sb78vFtqwzcucmy5tqNj61YSfutgE7_bkV0OEBFMwU3irXRub3XVSEN3mJrXkqx3OHdmP1u-0Nb8o1Scsq_wjkOZtT_
        3BBJyIn-xtK-wGNpqqScZX-H92PVoCRDEy2OFL8LaNS6Dy3h3zg5ZSjbQLiQIkOxZ1YZ2R6kflpaWYhAqhJZ3OZAgc8Mi5BQY_91u5dYBJi
        GGYSDQeClY1SmZPrcGr3UiQTHQkCVT-vfxqv7HnB-s';

        $jwtStorage = $this->prophesize(TokenStorage::class);
        $jwtStorage->tokenExists($jwt, Argument::type(User::class))->willReturn(true);

        $tokenManager = new TokenManager($jwtStorage->reveal());

        $this->assertTrue($tokenManager->tokenExists($jwt));
    }

    public function testIsDetectingValidPayload()
    {
        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NjAwNjUxMDUsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsInVzZXJfaWQiOjEyNjk1LCJpYXQiOiIxNDU5OTc4NzA1In0
        .J9Gmo-rEtxjR6Xi4-4P0pqHm31ppaXmPusUoGbk6lrjKS9TipRuwSFinkLxUIoIyc4ZkQjstcnGIjZ7sNCYl2csSB5mVZbhu_LkTz-NGme
        6MUvL7mEjlEB0-5WcgUI1oBqVgJ8RE4XjwpCQuEYgpr2JoV_XK9c1qyx_-BvC9QZmcw6MNNuFryurFvtnJF-KUfkzbUB1-UJsOFo4HjlaI5
        lNtpadQdqo9vdH6-XNc-xz-BXsKixI5Wl0hUaTN5Pj9rU6bre5P3-4IgHqwpJD_njj8OjBZLofD6msd9KqifrSxRR20Rw4UBm1Wzuq86Mby
        tLl7q4d52Yky6S5yW6b2k2W9DrvJ8ZE3-jjzJD6Q1mcZlhDdoLQVfmKqq4z_5D90Qj8JMJuR_33qxrjkvba3SHj46NKXBoY9b9zwMiUCPTp
        h0TTZgAL6MjA7-B4Sb78vFtqwzcucmy5tqNj61YSfutgE7_bkV0OEBFMwU3irXRub3XVSEN3mJrXkqx3OHdmP1u-0Nb8o1Scsq_wjkOZtT_
        3BBJyIn-xtK-wGNpqqScZX-H92PVoCRDEy2OFL8LaNS6Dy3h3zg5ZSjbQLiQIkOxZ1YZ2R6kflpaWYhAqhJZ3OZAgc8Mi5BQY_91u5dYBJi
        GGYSDQeClY1SmZPrcGr3UiQTHQkCVT-vfxqv7HnB-s';

        $tokenManager = new TokenManager($this->prophesize(TokenStorage::class)->reveal());

        $this->assertTrue($tokenManager->isPayloadValid($jwt));
    }

    public function testIsDetectingInvalidPayload()
    {
        $jwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjE0NTk4MzkzOTAsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsImlhdCI6IjE0NTk3NTI5OTAifQ.
        TgCFZzNqXuX7h6E7B_lGS5ohcnwxT__hcv0pmktTSY4iO36nqPyGMwdDLdA6wlHn7U46EtZaYYrHnnjXy4WUMD1
        D6oSfpGyY79lQeMkJAdrfgCe7geh84n1QKI8p2GVBcSMoMdxnJCvVOgyMBwX3DN0dZrejRPsFKloIWNTbAN58nZ
        PbQNHkjbsmqCxZHg93Bgfaujwq90SDF3hj9ICMnVHfFSTVgacTq0UzFlv7-ujiFVsuG0QSNRIM1iZArwKNulXHs
        O_psf7eVV-f4beEkE-xYiZJ8wkgklvf9N9CjnCKyvYvL9B-LzJvCwV4TsJXI9Fis-EMvdYeSSGU5KoWt6fRsu2R
        RjgaUqayr7U-T7eFC_ili9Z71mICDSM4tbULkeGxkQp9wVHZAmY8PfIJ9sxecMN6dKx-FgHh_HZOBFRo23G-l2-
        OxNaBTtjGSfYOq-lvtYGvtmNdq_Jruh5swQzSKcoT4DxhEpbAqBWIrge4QOd4M71U7EIQKDu2h5FpwoeT7zYcnf
        5_0AriSnsVoIxRF22eliRhJjNiDzEkvG-qN-2kpTg_osr-WB1ssayvlAyXtQ7DjeTRzedRUnYVf3DHTS8Lum31K
        YveECAXpjt-N7S_SdZ_h_ImjtBJRcWIc6eaFESSvm3E4bTScXZCE4SjeYAKhW37SgeYfRMxzCY';

        $tokenManager = new TokenManager($this->prophesize(TokenStorage::class)->reveal());

        $this->assertFalse($tokenManager->isPayloadValid($jwt));
    }
}
