<?php

namespace Linio\Frontend\Security\JWT;

use Linio\Frontend\Security\Jwt\TokenExtractor;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;

class TokenExtractorTest extends \PHPUnit_Framework_TestCase
{
    public function testIsExtractingJwtFromRequest()
    {
        $jwt = 's9fs98f.sdf9ds87f98sd7.d7fds8g5af94gfx96df5xads79';

        $jwtAuthorizationHeaderName = 'X-Auth';
        $authorizationHeader = 'Bearer ' . $jwt;

        $headers = $this->prophesize(HeaderBag::class);
        $headers->get($jwtAuthorizationHeaderName, '')->willReturn($authorizationHeader);

        $request = $this->prophesize(Request::class);
        $request->headers = $headers->reveal();

        $jwtExtractor = new TokenExtractor($jwtAuthorizationHeaderName);
        $actual = $jwtExtractor->extract($request->reveal());

        $this->assertEquals($actual, $jwt);
    }

    /**
     * @expectedException \Linio\Frontend\Security\Jwt\Exception\JwtExtractionFailedException
     * @expectedExceptionMessage JWT_NOT_FOUND
     */
    public function testIsDetectingTokenNotFound()
    {
        $jwt = 's9fs98f.sdf9ds87f98sd7.d7fds8g5af94gfx96df5xads79';

        $jwtAuthorizationHeaderName = 'X-Auth';
        $authorizationHeader = $jwt;

        $headers = $this->prophesize(HeaderBag::class);
        $headers->get($jwtAuthorizationHeaderName, '')->willReturn($authorizationHeader);

        $request = $this->prophesize(Request::class);
        $request->headers = $headers->reveal();

        $jwtExtractor = new TokenExtractor($jwtAuthorizationHeaderName);
        $jwtExtractor->extract($request->reveal());
    }
}
