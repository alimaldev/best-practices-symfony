<?php

namespace Linio\Frontend\Security;

use Linio\Frontend\Communication\Customer\CustomerService;
use Symfony\Component\Security\Core\User\User as SecurityUser;

class UserProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsLoadingByUsername()
    {
        $customerService = $this->prophesize(CustomerService::class);

        $user = new User();
        $user->setEmail('foo');

        $customerService->getUser('foo')->willReturn($user);

        $userProvider = new UserProvider();
        $userProvider->setCustomerService($customerService->reveal());

        $actual = $userProvider->loadUserByUsername('foo');

        $this->assertEquals($user, $actual);
    }

    public function testIsRefreshingUser()
    {
        $customerService = $this->prophesize(CustomerService::class);

        $user = new User();
        $user->setEmail('foo');

        $customerService->getUser('foo')->willReturn($user);

        $userProvider = new UserProvider();
        $userProvider->setCustomerService($customerService->reveal());

        $actual = $userProvider->refreshUser($user);

        $this->assertSame($user, $actual);
    }

    public function testIsSupportingClass()
    {
        $userProvider = new UserProvider();

        $actualTrue = $userProvider->supportsClass(User::class);
        $actualFalse = $userProvider->supportsClass(SecurityUser::class);

        $this->assertTrue($actualTrue);
        $this->assertFalse($actualFalse);
    }
}
