<?php

namespace Linio\Frontend\Security;

use Linio\Frontend\Communication\Customer\CustomerService;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Entity\Customer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Role\Role;

class AuthenticationProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsAuthenticatingToken()
    {
        $authenticationToken = new AuthenticationToken(new User(), 'foo', 'test');

        $customer = new Customer();
        $customerServiceMock = $this->prophesize(CustomerService::class);
        $customerServiceMock->login($authenticationToken)
            ->shouldBeCalled()
            ->willReturn($customer);

        $userProvider = new UserProvider();

        $authenticationProvider = new AuthenticationProvider();
        $authenticationProvider->setCustomerService($customerServiceMock->reveal());
        $actual = $authenticationProvider->authenticateToken($authenticationToken, $userProvider, 'test');

        $this->assertInstanceOf(User::class, $actual->getUser());
    }

    /**
     * @expectedException \Symfony\Component\Security\Core\Exception\BadCredentialsException
     */
    public function testIsHandlingInvalidCredentials()
    {
        $authenticationToken = new AuthenticationToken(new User(), 'foo', 'test');

        $customer = new Customer();
        $customerServiceMock = $this->prophesize(CustomerService::class);
        $customerServiceMock->login($authenticationToken)
            ->shouldBeCalled()
            ->willThrow(new InvalidLoginCredentialsException(InvalidLoginCredentialsException::CUSTOMER_LOGIN_FAILURE));

        $userProvider = new UserProvider();

        $authenticationProvider = new AuthenticationProvider();
        $authenticationProvider->setCustomerService($customerServiceMock->reveal());
        $authenticationProvider->authenticateToken($authenticationToken, $userProvider, 'test');
    }

    public function testIsSupportingToken()
    {
        $token = new AuthenticationToken(new User(), 'foo', 'test');

        $authenticationProvider = new AuthenticationProvider();
        $actual = $authenticationProvider->supportsToken($token, 'test');

        $this->assertTrue($actual);
    }

    public function testIsNotSupportingToken()
    {
        $token = new AuthenticationToken(new User(), 'foo', 'test');

        $authenticationProvider = new AuthenticationProvider();
        $actual = $authenticationProvider->supportsToken($token, 'foo');

        $this->assertFalse($actual);
    }

    public function testIsCreatingToken()
    {
        $authenticationProvider = new AuthenticationProvider();
        $actual = $authenticationProvider->createToken(new Request(), 'foo', 'bar', 'test');

        $this->assertInstanceOf(AuthenticationToken::class, $actual);
        $this->assertEquals('foo', $actual->getUsername());
        $this->assertEquals('bar', $actual->getCredentials());
        $this->assertEquals('test', $actual->getProviderKey());
        $this->assertEquals([new Role('ROLE_USER')], $actual->getRoles());
    }
}
