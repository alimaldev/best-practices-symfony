<?php

declare(strict_types=1);

namespace Linio\Frontend\Security;

use Linio\Frontend\Security\Jwt\TokenManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MobileLogoutSuccessHandlerTest extends \PHPUnit_Framework_TestCase
{
    public function testTokenIsRemovedOnLogout()
    {
        $request = new Request();

        $tokenManager = $this->prophesize(TokenManager::class);
        $tokenManager->removeTokenExtractedFromRequest($request)->shouldBeCalled();

        $logout = new MobileLogoutSuccessHandler();
        $logout->setTokenManager($tokenManager->reveal());
        $actual = $logout->onLogoutSuccess($request);

        $this->assertInstanceOf(JsonResponse::class, $actual);
    }
}
