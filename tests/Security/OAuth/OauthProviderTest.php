<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\OAuth;

use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Linio\Frontend\Communication\Customer\CustomerService;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Security\User;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class OauthProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testIsAuthenticatingOAuthToken()
    {
        $customer = new Customer();
        $token = new OauthToken(
            'john.bonham@linio.com',
            'facebook',
            'john.bonham',
            'John',
            'Bonham'
        );

        $customerService = $this->prophesize(CustomerService::class);
        $customerService->loginOAuth($token)->willReturn($customer);

        $oAuthProvider = new OAuthProvider();
        $oAuthProvider->setCustomerService($customerService->reveal());

        $actual = $oAuthProvider->authenticate($token);
        $this->assertInstanceOf(User::class, $actual->getUser());
    }

    public function testIsReturningErrorWhenOAuthTokenIsInvalid()
    {
        $token = $this->prophesize(OAuthToken::class);

        $customerService = $this->prophesize(CustomerService::class);
        $customerService->loginOAuth($token->reveal())
            ->willThrow(new AuthenticationException('Invalid credentials'));

        $oAuthProvider = new OAuthProvider();
        $oAuthProvider->setCustomerService($customerService->reveal());

        $this->expectException(AuthenticationException::class);
        $this->expectExceptionMessage('Invalid credentials');

        $oAuthProvider->authenticate($token->reveal());
    }

    public function testIsSupportingOAuthToken()
    {
        $token = new OauthToken(
            'ozzy_osborn@linio.com',
            'facebook',
            'ozzy',
            'Ozzy',
            'Osborn'
        );

        $oAuthProvider = new OAuthProvider();

        $this->assertTrue($oAuthProvider->supports($token));
    }

    public function testDoesNotSupportNonOAuthTokens()
    {
        $token = new JWTUserToken();

        $oAuthProvider = new OAuthProvider();

        $this->assertFalse($oAuthProvider->supports($token));
    }
}
