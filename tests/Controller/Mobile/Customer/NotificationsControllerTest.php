<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Test\MobileApiTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group mapi
 */
class NotificationsControllerTest extends MobileApiTestCase
{
    public function testGetNotificationSuccessful()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/customer/notifications',
            [],
            [],
            ['HTTP_X_AUTH' => 'Bearer ' . self::JWT]
        );

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath($client->getContainer(), 'notifications.json');

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    public function testUnsubscribeSuccessful()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/customer/notifications/unsubscribe',
            [],
            [],
            ['HTTP_X_AUTH' => 'Bearer ' . self::JWT]
        );

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath($client->getContainer(),
            'unsubscribe_newsletter.json');

        $this->assertSame(201, $response->getStatusCode(), $response->getContent());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    public function testUpdateSubscriptionSuccessful()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/customer/notifications/update-subscriptions',
            [],
            [],
            ['HTTP_X_AUTH' => 'Bearer ' . self::JWT],
            '{
              "frequency": 3,
              "preferences": [ 1, 2, 4, 7],
              "phoneNumber": "(555) 555-5555",
              "subscribedToSms": "true"
            }'
        );

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath(
            $client->getContainer(),
            'update_subscription.json'
        );

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    /**
     * @param ContainerInterface $container
     * @param string $jsonFile
     *
     * @return string
     */
    protected function getExpectedResponseFilePath(ContainerInterface $container, string $jsonFile): string
    {
        $countryCode = $container->getParameter('country_code');
        $appBase = $container->getParameter('kernel.root_dir');
        $expectedJsonFilePath = realpath(
            sprintf('%s%s%s%s%s', dirname($appBase), '/tests/fixtures/customer/', $countryCode, '/', $jsonFile)
        );

        return $expectedJsonFilePath;
    }
}
