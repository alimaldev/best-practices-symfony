<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Test\MobileApiTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group mapi
 */
class NewsletterControllerTest extends MobileApiTestCase
{
    public function testSubscriptionSuccessful()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/subscribe-email-newsletter',
            [],
            [],
            ['REMOTE_ADDR' => '192.168.99.1'],
            '{
               "email" : "jjw@linio.com",
               "source": "mobapi"
            }'
        );

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath($client->getContainer(),
            'subscribe_newsletter.json');

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    /**
     * @param ContainerInterface $container
     * @param string $jsonFile
     *
     * @return string
     */
    protected function getExpectedResponseFilePath(ContainerInterface $container, string $jsonFile): string
    {
        $countryCode = $container->getParameter('country_code');
        $appBase = $container->getParameter('kernel.root_dir');
        $expectedJsonFilePath = realpath(
            sprintf('%s%s%s%s%s', dirname($appBase), '/tests/fixtures/customer/', $countryCode, '/', $jsonFile)
        );

        return $expectedJsonFilePath;
    }
}
