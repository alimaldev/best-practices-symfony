<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoder;
use Linio\Frontend\Security\User;
use Linio\Frontend\Test\MobileApiTestCase;

/**
 * @group mapi
 */
class SecurityControllerTest extends MobileApiTestCase
{
    public function testSuccessfulLogin()
    {
        $client = self::createClient();

        $username = 'customer-1@linio.com';

        $client->request(
            'POST',
            '/mapi/v1/auth/login',
            [],
            [],
            [],
            json_encode(['username' => $username, 'password' => 'rock4me'])
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode());

        $content = json_decode((string) $response->getContent(), true);

        $this->assertSame($username, $content['username']);

        /** @var JWTEncoder $jwtEncoder */
        $jwtEncoder = $client->getContainer()->get('lexik_jwt_authentication.jwt_encoder');

        $tokenPayload = $jwtEncoder->decode($content['token']);

        $this->assertSame($username, $tokenPayload['username']);
    }

    public function testReturnsAnErrorWithInvalidCredentials()
    {
        $client = self::createClient();

        $client->request(
            'POST',
            '/mapi/v1/auth/login',
            [],
            [],
            [],
            json_encode(['username' => 'fake.user1@linio.com', 'password' => 'rock4me'])
        );

        $response = $client->getResponse();

        $this->assertSame(401, $response->getStatusCode());

        $content = json_decode((string) $response->getContent(), true);

        $this->assertSame(
            [
                'code' => 401,
                'message' => 'Bad credentials',
            ],
            $content
        );
    }

    public function testDetectsInvalidToken()
    {
        $client = self::createClient([], [], true);

        $jwt = str_replace(' ', '', 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.
        eyJleHAiOjEzNzQ2MTc0MTYsInVzZXJuYW1lIjoiZGFmQGxpbmlvLmNvbSIsInVzZXJfaWQiOjEzNDI5LCJpYXQiOiIxNDYxMDE3NDE2In0.
        Kol61lWh1mDNlr6LmyKHg9nJ7Jr8G_NCNAz8NyrJsOcZWkpkNWfLro1-HQ6wJpxbuDOsN8r9Zx4PElCy9n6ul5rmpQbmA_xkL-9TK4X69KtS
        wuRoS7XhaUFwPWm2kKLFC7R9LWLoIhqdnTk6mT9YPYlDb7TqjSHPfYmFARsjwYNkIWODvS9jiiCyAjlWobwi55CZo18u-bYd99DwBYfkkYXI
        r2i6AngAOuCOTaBJocqAhh_1DycOpXO5KlccuKRfpVLr3PA2vm6ur4C1v85MMYkPcInlkOU03arRaEcXp9sPJ_-Ix_x52Ey2PGdzLHDSQ96A
        DIt95GFTDopsnVZovDLyqZC071CbbOcGuyuo6nkcEK-gKpgKPWBTHnbXQwzr246QWbE93GEdNar6O4pksbSiU2nj7KYMzUslIMRfUfRUGTq3
        2xnPn8Rklp35PM3oBiowe_v2bBnl3BxoduxgSI6Od-T7PBRrgZ4-NK5lkEdkdxWdemgG2SljEIPM4qAEOCDyx_9daYgz545uFBWPFxYus8Z4
        wlxPgh3c8HEUZosq7BAFjx97Bg8rCv7gUK6Um6zJKCcv3nmPIiLqBd8bBKkrgw8-QUTc5LCr6eS98IpQbxnKB0G1C5ivl2bvB7SMsRb9S177
        3fvmBntJFoFS1u7X-HqleNUxWHzlRh9TOC4');

        $tokenManager = $client->getContainer()->get('security.auth.token_manager');

        $user = new User();
        $user->setId(13429);

        $tokenManager->add($jwt, $user);

        $this->assertCount(1, $tokenManager->getTokens($user));

        $client->request(
            'POST',
            '/mapi/v1/search',
            [],
            [],
            [
                'HTTP_X-Auth' => 'Bearer ' . $jwt,
            ],
            json_encode(['q' => 'linio'])
        );

        $response = $client->getResponse();

        $this->assertSame(401, $response->getStatusCode());

        $content = json_decode((string) $response->getContent(), true);

        $this->assertEmpty($tokenManager->getTokens($user));

        $this->assertSame(
            [
                'message' => 'El token es inválido, inicia sesión usando usuario y contraseña',
                'context' => null,
            ],
            $content
        );
    }
}
