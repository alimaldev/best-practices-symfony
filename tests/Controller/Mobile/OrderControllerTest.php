<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Test\MobileApiTestCase;

/**
 * @group mapi
 */
class OrderControllerTest extends MobileApiTestCase
{
    public function testGetNewOrderInProgressSuccessfully()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_5708129e3cc519.08544268',
            ]
        );

        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertEmpty($payload['order']['items']);
        $this->assertEmpty($payload['order']['availablePaymentMethods']);
    }

    public function testAddProduct()
    {
        $parameters = [
            'sku' => 'CO752FA0DJ51CLAEC-83271',
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f3fbca4263f2.71735122',
            ]
        );

        $order = json_decode($client->getResponse()->getContent(), true);

        $this->assertEmpty($order['order']['items']);

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order/add-item',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f3fbca4263f2.71735122',
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode());
    }

    public function testRemoveProduct()
    {
        $parameters = [
            'sku' => 'CO752FA0DJ51CLAEC-83271',
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f3fbca4263f2.71738822',
            ]
        );

        $order = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(1, $order['order']['items']);

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order/remove-item',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f3fbca4263f2.71738822',
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertEmpty($payload['order']['items']);
    }

    public function testUpdateQuantity()
    {
        $parameters = [
            'sku' => 'KI017EL79POQLAEC-13765',
            'quantity' => 2,
        ];

        $client = static::createClient();

        $client->request(
            'POST',
            '/mapi/v1/order/update-quantity',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f3fbca4263f2.61548488',
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertNotEmpty($payload['order']['items']);
        $this->assertEquals($parameters['quantity'], $payload['order']['items'][$parameters['sku']]['quantity']);
    }

    public function testAddCouponInOrderUnsuccessfully()
    {
        $parameters = [
            'coupon' => 'NOTVALID',
        ];

        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/apply-coupon',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_570e6e27792678.46405345',
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();
        $this->assertSame(500, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);

        $this->assertEmpty($payload['context']);
        $this->assertEquals('Cupón no válido', $payload['message']);
    }

    public function testAddCouponInOrderSuccessfully()
    {
        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/apply-coupon',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_570e6e27792678.46405345',
            ],
            json_encode(['coupon' => 'TEST1d13'])
        );

        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertNotEmpty($payload['order']['coupon']);
    }

    public function testRemoveCouponInOrderSuccessfully()
    {
        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/apply-coupon',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_570e6e27792678.46405345',
            ],
            json_encode(['coupon' => 'TEST1d13'])
        );

        $orderResponse = $client->getResponse();
        $arrayOrderResponse = json_decode($orderResponse->getContent(), true);

        $this->assertNotEmpty($arrayOrderResponse['order']['coupon']);

        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/remove-coupon',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_570e6e27792678.46405345',
            ]
        );

        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);

        $this->assertEmpty($payload['messages']['errors']);
        $this->assertEmpty($payload['messages']['warnings']);
        $this->assertEmpty($payload['order']['coupon']);
    }

    public function testSetShippingQuoteAddressSuccessfully()
    {
        $parameters = [
            'region' => 'EL ORO',
            'municipality' => 'BARBONES',
        ];

        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/shipping-quote-address',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_570e6e27792678.46405346',
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);

        $this->assertNotEmpty($payload['order']['items']);
        $this->assertNotEmpty($payload['order']['packages']);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertEmpty($payload['messages']['warnings']);
    }

    public function testUpdatePaymentMethodSuccessfully()
    {
        $parameters = [
            'paymentMethod' => 'PayClub_HostedPaymentPage',
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order/update-payment-method',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertNotEmpty($payload['order']['items']);
        $this->assertSame($parameters['paymentMethod'], $payload['order']['paymentMethod']);
    }

    public function testUpdatePaymentMethodUnsuccessfully()
    {
        $parameters = [
            'paymentMethod' => 'CashOnDelivery_Payment',
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ]
        );

        $order = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotSame($parameters['paymentMethod'], $order['order']['paymentMethod']);

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order/update-payment-method',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertNotEmpty($payload['order']['items']);
        $this->assertNotEmpty($payload['messages']['errors']);
        $this->assertEquals(['Este método de pago no es válido para tu orden'], $payload['messages']['errors']);
    }

    public function testUpdateShippingMethodSuccessfully()
    {
        $parameters = [
            'packageId' => 1,
            'shippingMethod' => 'express',
        ];

        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/update-shipping-method',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();
        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);

        $this->assertNotEmpty($payload['order']['items']);
        $this->assertNotEmpty($payload['order']['packages']);
        $this->assertNotEmpty($payload['order']['packages'][1]['shippingQuotes']);
        $this->assertEmpty($payload['messages']['errors']);
        $this->assertEmpty($payload['messages']['warnings']);
        $this->assertTrue($payload['order']['packages'][1]['shippingQuotes'][1]['selected']);
    }

    public function testSetCustomerShippingAddressSuccessfully()
    {
        $parameters = [
            'addressId' => 10563,
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ]
        );

        $order = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEquals($order['order']['shippingAddress']['id'], $parameters['addressId']);

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/order/customer-shipping-address',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $payload = json_decode($response->getContent(), true);
        $this->assertNotEmpty($payload['order']['items']);
        $this->assertEquals($payload['order']['shippingAddress']['id'], $parameters['addressId']);
        $this->assertEmpty($payload['messages']['errors']);
    }

    public function testPlaceOrderSuccessfully()
    {
        $parameters = [
            'grandTotal' => 62.99,
            'coupon' => null,
            'installments' => 1,
            'packages' => [
                '1' => [
                    'id' => 0,
                    'shippingMethod' => 'regular',
                ],
            ],
            'items' => [
                'CO752FA0DJ51CLAEC-83271' => 1,
            ],
            'shippingAddress' => [
                'id' => 390,
                'municipality' => 'QUITO',
                'city' => 'QUITO',
                'region' => 'PICHINCHA',
                'postcode' => '118015',
            ],
            'billingAddress' => [
                'id' => 390,
                'municipality' => 'QUITO',
                'city' => 'QUITO',
                'region' => 'PICHINCHA',
                'postcode' => '118015',
            ],
            'paymentMethod' => 'PayClub_HostedPaymentPage',
            'paymentData' => null,
            'extraFields' => [
                'nationalRegistrationNumber' => '171622007-2',
            ],
            'walletPoints' => null,
        ];

        $client = static::createClient();

        $client->request('POST',
            '/mapi/v1/order/place',
            [],
            [],
            [
                'REMOTE_ADDR' => '172.17.0.1',
                'HTTP_X_LINIO_ID' => 'x2qxu6',
                'HTTP_X_AUTH' => 'Bearer ' . self::JWT,
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();
        $actual = json_decode($response->getContent(), true);

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $this->assertSame('200026696', $actual['orderNumber']);
        $this->assertSame($parameters['paymentMethod'], $actual['paymentMethod']);
        $this->assertNotEmpty($actual['redirect']);
    }
}
