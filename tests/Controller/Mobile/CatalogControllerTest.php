<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Test\MobileApiTestCase;

/**
 * @group mapi
 */
class CatalogControllerTest extends MobileApiTestCase
{
    public function testSuccessfulSellerAction()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/mapi/v1/s/fundacion-el-triangulo',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f9ac5477dc45.30994455',
            ]
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('searchResult', $content);
        $this->assertArrayHasKey('categoryTree', $content);
        $this->assertArrayHasKey('catalogTitle', $content);
        $this->assertArrayHasKey('seller', $content);
    }

    public function testSuccessfulBrandAction()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/mapi/v1/b/apple',
            [],
            [],
            [
                'HTTP_X_LINIO_ID' => '_56f9ac5477dc45.30994455',
            ],
            '{"searchResults": true}'
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $content = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('searchResult', $content);
        $this->assertSame(67, $content['searchResult']['totalItemsFound']);
        $this->assertArrayHasKey('categoryTree', $content);
        $this->assertArrayHasKey('catalogTitle', $content);
    }

    public function testSuccessfulAddProductReview()
    {
        $review = '{"ratingScore": 3,"title": "It\'s wonderful","comment": "It\'s such a wonderful piece of hardware that when I threw at a tree, the trunk split in half."}';

        $client = static::createClient();

        $client->request(
            'POST',
            '/mapi/v1/add-review/convertidor-smartv-divermax-speedcast-vazv5i',
            [],
            [],
            [],
            $review
        );

        $response = $client->getResponse();

        $this->assertSame(201, $response->getStatusCode(), $response->getContent());
        $this->assertSame('{}', $response->getContent());
    }
}
