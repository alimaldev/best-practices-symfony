<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Test\MobileApiTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group mapi
 */
class CmsControllerTest extends MobileApiTestCase
{
    public function testGetCategoryMenuSuccess()
    {
        $client = static::createClient();
        $client->request('POST', '/mapi/v1/cms/category-menu');

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath($client->getContainer(), 'category_menu.json');

        $this->assertSame(200, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    public function testGetTopMenuSuccess()
    {
        $client = static::createClient();
        $client->request('POST', '/mapi/v1/cms/top-menu');

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath($client->getContainer(), 'top_menu.json');

        $this->assertSame(200, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    public function testGetPageContentSuccess()
    {
        $client = static::createClient();
        $client->request(
                'POST',
                '/mapi/v1/cms/page-content',
                [],
                [],
                [],
                '{"slug":"asdads"}'
        );

        $response = $client->getResponse();

        $expectedResponseFilePath = $this->getExpectedResponseFilePath($client->getContainer(), 'page_content.json');

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());
        $this->assertJsonStringEqualsJsonFile($expectedResponseFilePath, $response->getContent());
    }

    /**
     * @param ContainerInterface $container
     * @param string $jsonFile
     *
     * @return string
     */
    protected function getExpectedResponseFilePath(ContainerInterface $container, string $jsonFile): string
    {
        $countryCode = $container->getParameter('country_code');
        $appBase = $container->getParameter('kernel.root_dir');
        $expectedJsonFilePath = realpath(
            sprintf('%s%s%s%s%s', dirname($appBase), '/tests/fixtures/cms/', $countryCode, '/', $jsonFile)
        );

        return $expectedJsonFilePath;
    }
}
