<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Test\MobileApiTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @group mapi
 */
class CustomerControllerTest extends MobileApiTestCase
{
    public function testCreateCustomerSuccess()
    {
        $email = 'functional.test4@linio.com';

        $parameters = [
            'registration' => [
                'email' => $email,
                'first_name' => 'Functional',
                'last_name' => 'Test',
                'password' => 'abcd1234',
                'national_registration_number' => '123456789-0',
                'sms_phone' => '(555) 555-5555',
                'subscribedToNewsletter' => true,
                'acceptTerms' => true,
            ],
        ];

        $client = static::createClient();
        $client->request(
            'POST',
            '/mapi/v1/customer',
            [],
            [],
            [
                'X_AUTH_STORE' => $client->getContainer()->getParameter('country_code'),
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($parameters)
        );

        $response = $client->getResponse();

        $this->assertSame(200, $response->getStatusCode(), $response->getContent());

        $body = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('token', $body);
        $this->assertSame($email, $body['username']);
    }

    /**
     * @param ContainerInterface $container
     * @param string $jsonFile
     *
     * @return string
     */
    protected function getExpectedResponseFilePath(ContainerInterface $container, string $jsonFile): string
    {
        $countryCode = $container->getParameter('country_code');
        $appBase = $container->getParameter('kernel.root_dir');
        $expectedJsonFilePath = realpath(
            sprintf('%s%s%s%s%s', dirname($appBase), '/tests/fixtures/customer/', $countryCode, '/', $jsonFile)
        );

        return $expectedJsonFilePath;
    }
}
