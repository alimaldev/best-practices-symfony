<?php

namespace Linio\Frontend\Form;

use Linio\Frontend\Form\Type\EmailType;
use Prophecy\Argument;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

class LoginFormTest extends \PHPUnit_Framework_TestCase
{
    public function testIsBuildingLoginForm()
    {
        $customerLoginUrl = '/account/login';
        $optionsMock = [];
        $routerMock = $this->prophesize(RouterInterface::class);
        $builderFormMock = $this->prophesize(FormBuilderInterface::class);

        $routerMock->generate(Argument::type('string'), Argument::type('array'), Argument::type('int'))
            ->shouldBeCalled()
            ->willReturn($customerLoginUrl);

        $builderFormMock->setAction(Argument::type('string'))
            ->willReturn($builderFormMock);
        $builderFormMock->add('email', EmailType::class, [
            'attr' => ['class' => 'login-form-input form-style'],
            'label_attr' => ['class' => 'login-form-label'],
            'label' => 'login.form.email.label',
        ])
            ->willReturn($builderFormMock);
        $builderFormMock->add('password', PasswordType::class, [
            'attr' => ['class' => 'login-form-input form-style', 'placeholder' => 'login.form.password.placeholder'],
            'label_attr' => ['class' => 'login-form-label'],
            'label' => 'login.form.password.label',
        ])
            ->willReturn($builderFormMock);
        $builderFormMock->add('login', SubmitType::class, [
            'attr' => ['class' => 'btn btn-action btn-m btn-block font-size-tall margin-mini-top'],
            'label' => 'login.form.login',
        ])
            ->willReturn($builderFormMock);

        $loginForm = new LoginForm();
        $loginForm->setRouter($routerMock->reveal());

        $loginForm->buildForm($builderFormMock->reveal(), $optionsMock);
    }
}
