<?php

namespace Linio\Frontend\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpirationMonthTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGeneratingMonthRange()
    {
        $optionsResolver = new OptionsResolver();
        $type = new ExpirationMonthType();
        $type->configureOptions($optionsResolver);
        $options = $optionsResolver->resolve([]);

        $expectedRange = range(1, 12);
        $this->assertEquals(array_combine($expectedRange, $expectedRange), $options['choices']);
    }
}
