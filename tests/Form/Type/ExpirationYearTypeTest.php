<?php

namespace Linio\Frontend\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpirationYearTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGeneratingYearRange()
    {
        $optionsResolver = new OptionsResolver();
        $type = new ExpirationYearType();
        $type->configureOptions($optionsResolver);
        $options = $optionsResolver->resolve([]);

        $this->assertEquals('Y', $options['year_format']);
        $expectedYearRange = range(date('Y'), (date('Y') + 20));
        $this->assertEquals(array_combine($expectedYearRange, $expectedYearRange), $options['choices']);
    }

    public function testIsGeneratingYearRangeWithCustomRange()
    {
        $optionsResolver = new OptionsResolver();
        $type = new ExpirationYearType();
        $type->configureOptions($optionsResolver);
        $options = $optionsResolver->resolve(['year_range' => 30]);

        $this->assertEquals('Y', $options['year_format']);
        $expectedYearRange = range(date('Y'), (date('Y') + 30));
        $this->assertEquals(array_combine($expectedYearRange, $expectedYearRange), $options['choices']);
    }
}
