<?php

namespace Linio\Frontend\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;

class InstallmentTypeTest extends \PHPUnit_Framework_TestCase
{
    public function testIsGeneratingInstallmentsRange()
    {
        $optionsResolver = new OptionsResolver();
        $type = new InstallmentType();
        $type->configureOptions($optionsResolver);
        $options = $optionsResolver->resolve([]);

        $expectedRange = range(1, 36);
        $this->assertEquals(array_combine($expectedRange, $expectedRange), $options['choices']);
    }
}
