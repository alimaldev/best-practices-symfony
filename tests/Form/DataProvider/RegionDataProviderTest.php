<?php

namespace tests\Form\DataProvider;

use Linio\Frontend\Form\DataProvider\RegionDataProvider;
use Linio\Frontend\Location\ResolveLocation;
use Linio\Test\UnitTestCase;

class RegionDataProviderTest extends UnitTestCase
{
    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../../fixtures/address/legacy.yml');
    }

    public function testIsParsingRegions()
    {
        $expected = [
            'Region 1-1' => 'Region 1-1',
            'Region 1-2' => 'Region 1-2',
        ];

        $addressService = $this->prophesize(ResolveLocation::class);
        $addressService->getRegions()
            ->shouldBeCalled()
            ->willReturn([$this->fixtures['region_1_1'], $this->fixtures['region_1_2']]);

        $region = new RegionDataProvider();
        $region->setResolveLocation($addressService->reveal());

        $actual = $region->getData();

        $this->assertEquals($expected, $actual);
    }
}
