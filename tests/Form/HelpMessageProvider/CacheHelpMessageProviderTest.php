<?php

declare(strict_types=1);

namespace tests\Form\DataProvider;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Form\HelpMessageProvider\CacheHelpMessageProvider;
use Linio\Test\UnitTestCase;

class CacheHelpMessageProviderTest extends UnitTestCase
{
    public function testIsGettingHelpMessage()
    {
        $key = 'desktop:markdown_text:global:message';
        $cms = [
            'revision_number' => 1,
            'text' => 'Help Messsage.',
        ];

        $cacheService = $this->prophesize(CacheService::class);
        $cacheService->get($key)
            ->shouldBeCalled()
            ->willReturn($cms);

        $helpMessageProvider = new CacheHelpMessageProvider();
        $helpMessageProvider->setCacheService($cacheService->reveal());

        $actual = $helpMessageProvider->getHelpMessage($key);

        $this->assertEquals($cms['text'], $actual);
    }
}
