<?php

namespace Linio\Frontend\Seller;

use DateTime;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Seller\PaginatedReviews;
use Linio\Frontend\Entity\Seller\Rating;
use Linio\Frontend\Entity\Seller\RatingCollection;
use Linio\Frontend\Entity\Seller\RatingSummary;
use Linio\Frontend\Entity\Seller\Review;
use Linio\Frontend\Entity\Seller\ReviewCollection;
use Linio\Frontend\Entity\Seller\Seller;
use Linio\Frontend\Seller\Communication\SellerAdapter;
use PHPUnit_Framework_TestCase;

class SellerServiceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function sellerProvider()
    {
        $collection = new RatingCollection();

        $ratings = [
            '1' => 1,
            '2' => 3,
            '3' => 1,
            '4' => 38,
            '5' => 46,
        ];

        foreach ($ratings as $star => $votes) {
            $collection->add(new Rating($star, $votes));
        }

        $summary = new RatingSummary(4.5, 89, $collection);

        $sellerData = [
            'name' => 'KELWIN',
            'slug' => 'kelwin',
            'type' => 'merchant',
            'operation_type' => 'national',
            'rating_average' => 4.5,
            'rating_summary' => [
                '1' => 1,
                '2' => 3,
                '3' => 1,
                '4' => 38,
                '5' => 46,
            ],
        ];

        return [
            [$sellerData, new Seller(468, 'KELWIN', 'kelwin', 'merchant', $summary, 'national')],
        ];
    }

    /**
     * @return array
     */
    public function sellerReviewsProvider()
    {
        $reviewsData = [
            [
                'title' => 'Rapida compra 1.',
                'detail' => 'The product is good.',
                'rating' => 1,
                'reviewer' => 'Roy',
                'reviewed_at' => '2014-07-01',
            ],
            [
                'title' => 'Excelente vendedor 2.',
                'detail' => 'Amazing.',
                'rating' => 2,
                'reviewer' => 'Jose Luis',
                'reviewed_at' => '2015-07-01',
            ],
        ];

        $collection = new ReviewCollection();

        foreach ($reviewsData as $review) {
            $collection->add(new Review($review['title'], $review['detail'], $review['rating'], $review['reviewer'], new DateTime($review['reviewed_at'])));
        }

        return [
            [$reviewsData, new PaginatedReviews(1, 1, $collection)],
        ];
    }

    /**
     * @dataProvider sellerProvider
     *
     * @param array $sellerData
     * @param Seller $seller
     */
    public function testIsGettingSellerById(array $sellerData, Seller $seller)
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->get('data:468')->willReturn($sellerData);

        $adapter = $this->prophesize(SellerAdapter::class);

        $service = new SellerService($cache->reveal(), $adapter->reveal());
        $actual = $service->get(468);

        $this->assertEquals($seller, $actual);
    }

    /**
     * @expectedException \Linio\Exception\NotFoundHttpException
     * @expectedExceptionMessage Seller with id 468 not found
     */
    public function testIsDetectingSellerNotFound()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->get('data:468')->willReturn(null);

        $adapter = $this->prophesize(SellerAdapter::class);

        $service = new SellerService($cache->reveal(), $adapter->reveal());
        $service->get(468);
    }

    /**
     * @param array $reviewsData
     * @param PaginatedReviews $paginatedReviews
     *
     * @dataProvider sellerReviewsProvider
     */
    public function testIsGettingSellerReviews(array $reviewsData, PaginatedReviews $paginatedReviews)
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->get('review:468')->willReturn($reviewsData);

        $adapter = $this->prophesize(SellerAdapter::class);

        $service = new SellerService($cache->reveal(), $adapter->reveal());
        $actual = $service->getReviews(468, 1, 2);

        $this->assertEquals($paginatedReviews, $actual);
    }

    /**
     * @expectedException \Linio\Exception\NotFoundHttpException
     * @expectedExceptionMessage Seller with id 468 not found
     */
    public function testIsDetectingSellerNotFoundWhenGetsReviews()
    {
        $cache = $this->prophesize(CacheService::class);
        $cache->get('review:468')->willReturn(null);

        $adapter = $this->prophesize(SellerAdapter::class);

        $service = new SellerService($cache->reveal(), $adapter->reveal());
        $service->getReviews(468, 1, 2);
    }

    public function testIsFindingCustomerReviews()
    {
        $customer = new Customer();
        $customer->setId(1);

        $page = 1;
        $pageSize = 10;

        $result = [
            '123456' => [
                'sellers' => [
                    1 => [
                        'name' => 'Seller 1 (new)',
                        'review' => new SellerReview('Title', 'Detail', 5),
                    ],
                    2 => [],
                ],
            ],
        ];

        $seller1Data = [
            'name' => 'Seller 1',
            'slug' => 'slug-1',
            'type' => 'type',
            'operation_type' => 'operationType',
            'rating_summary' => [],
            'rating_average' => 1,
        ];
        $seller1 = new Seller(
            1,
            'Seller 1 (new)',
            'slug-1',
            'type',
            new RatingSummary(1.0, 0, new RatingCollection()),
            'operationType'
        );

        $seller2Data = [
            'name' => 'Seller 2',
            'slug' => 'slug-2',
            'type' => 'type',
            'operation_type' => 'operationType',
            'rating_summary' => [],
            'rating_average' => 1,
        ];
        $seller2 = new Seller(
            2,
            'Seller 2',
            'slug-2',
            'type',
            new RatingSummary(1.0, 0, new RatingCollection()),
            'operationType'
        );

        $paginatedResult = new PaginatedResult();
        $paginatedResult->setResult($result);

        $cache = $this->prophesize(CacheService::class);
        $cache->get('data:1')->shouldBeCalled()->willReturn($seller1Data);
        $cache->get('data:2')->shouldBeCalled()->willReturn($seller2Data);

        $adapter = $this->prophesize(SellerAdapter::class);
        $adapter->findCustomerReviews($customer, $page, $pageSize)->shouldBeCalled()->willReturn($paginatedResult);

        $service = new SellerService($cache->reveal(), $adapter->reveal());

        $actual = $service->findCustomerReviews($customer, $page, $pageSize);

        $this->assertEquals($seller1, $actual->getResult()['123456']['sellers'][1]['review']->getSeller());
        $this->assertEquals($seller1, $actual->getResult()['123456']['sellers'][1]['seller']);

        $this->assertArrayNotHasKey('review', $actual->getResult()['123456']['sellers'][2]);
        $this->assertEquals($seller2, $actual->getResult()['123456']['sellers'][2]['seller']);
    }
}
