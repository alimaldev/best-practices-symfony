<?php

declare(strict_types=1);

namespace Linio\Frontend\Seller\Communication\Input;

use DateTime;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\ProductService;
use PHPUnit_Framework_TestCase;

class Bob4AliceTest extends PHPUnit_Framework_TestCase
{
    public function testItMapsToSellerReviews()
    {
        $responseBody = [
            'pagination' => [
                'current' => 1,
                'size' => 2,
                'total' => 1,
                'itemCount' => 2,
            ],
            'result' => [
                [
                    'customerId' => 1,
                    'orderId' => 1,
                    'orderNumber' => '123456',
                    'orderDate' => '2016-06-06 00:00:00',
                    'sellerId' => 1,
                    'sellerName' => 'Seller 1',
                    'products' => [
                        [
                            'simpleSku' => 'SKU001-001',
                            'name' => 'Product 1',
                        ],
                    ],
                    'review' => [
                        'id' => 1,
                        'rating' => 5,
                        'reviewTitle' => 'Review 1 Title',
                        'reviewText' => 'Review 1 Text',
                        'nickname' => 'Review 1 Nickname',
                    ],
                ],
                [
                    'customerId' => 1,
                    'orderId' => 2,
                    'orderNumber' => '654321',
                    'orderDate' => '2016-06-07 00:00:00',
                    'sellerId' => 2,
                    'sellerName' => 'Seller 2',
                    'products' => [
                        [
                            'simpleSku' => 'SKU002-001',
                            'name' => 'Product 2',
                        ],
                        [
                            'simpleSku' => 'SKU003-001',
                            'name' => 'Product 3',
                        ],
                    ],
                    'review' => [
                        'id' => 2,
                        'rating' => 5,
                        'reviewTitle' => 'Review 2 Title',
                        'reviewText' => 'Review 2 Text',
                        'nickname' => 'Review 2 Nickname',
                    ],
                ],
                [
                    'customerId' => 1,
                    'orderId' => 2,
                    'orderNumber' => '654321',
                    'orderDate' => '2016-06-07 00:00:00',
                    'sellerId' => 3,
                    'sellerName' => 'Seller 3',
                    'products' => [
                        [
                            'simpleSku' => 'SKU004-001',
                            'name' => 'Product 4',
                        ],
                    ],
                ],
            ],
        ];

        $reviewsByOrder = [];

        // Review 1
        $reviewData1 = $responseBody['result'][0];

        $review1 = new SellerReview(
            $reviewData1['review']['reviewTitle'],
            $reviewData1['review']['reviewText'],
            $reviewData1['review']['rating']
        );
        $reviewsByOrder[$reviewData1['orderId']]['sellers'][$reviewData1['sellerId']]['review'] = $review1;
        $reviewsByOrder[$reviewData1['orderId']]['sellers'][$reviewData1['sellerId']]['name'] = 'Seller 1';

        $product1 = new Product($reviewData1['products'][0]['simpleSku']);
        $reviewsByOrder[$reviewData1['orderId']]['sellers'][$reviewData1['sellerId']]['products'][] = $product1;

        $reviewsByOrder[$reviewData1['orderId']]['orderNumber'] = $reviewData1['orderNumber'];
        $reviewsByOrder[$reviewData1['orderId']]['orderDate'] = new DateTime($reviewData1['orderDate']);

        // Review 2
        $reviewData2 = $responseBody['result'][1];

        $review2 = new SellerReview(
            $reviewData2['review']['reviewTitle'],
            $reviewData2['review']['reviewText'],
            $reviewData2['review']['rating']
        );
        $reviewsByOrder[$reviewData2['orderId']]['sellers'][$reviewData2['sellerId']]['review'] = $review2;
        $reviewsByOrder[$reviewData2['orderId']]['sellers'][$reviewData2['sellerId']]['name'] = 'Seller 2';

        $product2 = new Product($reviewData2['products'][0]['simpleSku']);
        $product3 = new Product($reviewData2['products'][1]['simpleSku']);
        $reviewsByOrder[$reviewData2['orderId']]['sellers'][$reviewData2['sellerId']]['products'][] = $product2;
        $reviewsByOrder[$reviewData2['orderId']]['sellers'][$reviewData2['sellerId']]['products'][] = $product3;

        $reviewsByOrder[$reviewData2['orderId']]['orderNumber'] = $reviewData2['orderNumber'];
        $reviewsByOrder[$reviewData2['orderId']]['orderDate'] = new DateTime($reviewData2['orderDate']);

        // Review 3
        $reviewData3 = $responseBody['result'][2];

        $reviewsByOrder[$reviewData3['orderId']]['sellers'][$reviewData3['sellerId']]['review'] = null;
        $reviewsByOrder[$reviewData3['orderId']]['sellers'][$reviewData3['sellerId']]['name'] = 'Seller 3';

        $product4 = new Product($reviewData3['products'][0]['simpleSku']);
        $reviewsByOrder[$reviewData3['orderId']]['sellers'][$reviewData3['sellerId']]['products'][] = $product4;

        $reviewsByOrder[$reviewData3['orderId']]['orderNumber'] = $reviewData3['orderNumber'];
        $reviewsByOrder[$reviewData3['orderId']]['orderDate'] = new DateTime($reviewData3['orderDate']);

        // PaginatedResult
        $expected = new PaginatedResult();
        $expected->setCurrent($responseBody['pagination']['current']);
        $expected->setItemCount($responseBody['pagination']['itemCount']);
        $expected->setPages($responseBody['pagination']['total']);
        $expected->setSize($responseBody['pagination']['size']);
        $expected->setResult($reviewsByOrder);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku($reviewData1['products'][0]['simpleSku'])->shouldBeCalled()->willReturn($product1);
        $productService->getBySku($reviewData2['products'][0]['simpleSku'])->shouldBeCalled()->willReturn($product2);
        $productService->getBySku($reviewData2['products'][1]['simpleSku'])->shouldBeCalled()->willReturn($product3);
        $productService->getBySku($reviewData3['products'][0]['simpleSku'])->shouldBeCalled()->willReturn($product4);

        $input = new Bob4Alice();
        $input->setProductService($productService->reveal());

        $this->assertEquals($expected, $input->toSellerReviews($responseBody));
    }
}
