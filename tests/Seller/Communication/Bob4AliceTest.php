<?php

declare(strict_types=1);

namespace Linio\Frontend\Seller\Communication;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Seller\Communication\Input\Bob4Alice as Bob4AliceInput;
use PHPUnit_Framework_TestCase;
use Prophecy\Argument;

class Bob4AliceTest extends PHPUnit_Framework_TestCase
{
    public function testFindCustomerReviewsIsCallingInput()
    {
        $customer = new Customer();
        $customer->setId(1);

        $page = 1;
        $pageSize = 10;

        $requestBody = [
            'customerId' => $customer->getId(),
            'page' => $page,
            'pageSize' => $pageSize,
        ];

        $responseBody = [
            'test' => 'body',
        ];

        $response = new Response(200, [], json_encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/order/get-seller-reviews', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $input = $this->prophesize(Bob4AliceInput::class);
        $input->toSellerReviews($responseBody)->shouldBeCalled();

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());
        $adapter->setInput($input->reveal());

        $adapter->findCustomerReviews($customer, $page, $pageSize);
    }

    public function testFindingCustomerReviewsFailsWithAClientError()
    {
        $customer = new Customer();
        $customer->setId(0);

        $page = 1;
        $pageSize = 10;

        $requestBody = [
            'customerId' => $customer->getId(),
            'page' => $page,
            'pageSize' => $pageSize,
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [],
        ];

        $response = new Response(400, [], json_encode($responseBody));

        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/order/get-seller-reviews', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $input = $this->prophesize(Bob4AliceInput::class);
        $input->toSellerReviews(Argument::any())->shouldNotBeCalled();

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());
        $adapter->setInput($input->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->findCustomerReviews($customer, $page, $pageSize);
    }

    public function testFindingCustomerReviewsFailsWithAServerError()
    {
        $customer = new Customer();
        $customer->setId(1);

        $page = 1;
        $pageSize = 10;

        $requestBody = [
            'customerId' => $customer->getId(),
            'page' => $page,
            'pageSize' => $pageSize,
        ];

        $responseBody = [
            'code' => 'AN_ERROR_OCCURRED',
            'message' => '',
            'errors' => [],
        ];

        $response = new Response(400, [], json_encode($responseBody));

        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn($response);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/order/get-seller-reviews', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $input = $this->prophesize(Bob4AliceInput::class);
        $input->toSellerReviews(Argument::any())->shouldNotBeCalled();

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());
        $adapter->setInput($input->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('AN_ERROR_OCCURRED');

        $adapter->findCustomerReviews($customer, $page, $pageSize);
    }
}
