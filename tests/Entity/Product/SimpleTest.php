<?php

namespace Linio\Frontend\Entity\Product;

use Linio\Type\Money;

class SimpleTest extends \PHPUnit_Framework_TestCase
{
    public function testIsDetectingSpecialPrice()
    {
        $simple = new Simple();
        $simple->setOriginalPrice(new Money(250));
        $simple->setPrice(new Money(100));
        $this->assertTrue($simple->hasSpecialPrice());

        $simple = new Simple();
        $simple->setPrice(new Money(100));
        $this->assertFalse($simple->hasSpecialPrice());
    }

    public function testIsGettingPercentageOff()
    {
        $simple = new Simple();
        $simple->setOriginalPrice(new Money(250));
        $simple->setPrice(new Money(100));
        $this->assertEquals(60, $simple->getPercentageOff());
    }

    public function testIsGettingPercentageOffWithoutOriginalPrice()
    {
        $simple = new Simple();
        $simple->setPrice(new Money(100));
        $this->assertEquals(0, $simple->getPercentageOff());
    }

    public function testIsGettingSavedAmount()
    {
        $simple = new Simple();
        $simple->setOriginalPrice(new Money(250));
        $simple->setPrice(new Money(100));
        $this->assertEquals(new Money(150), $simple->getSavedAmount());
    }

    public function testIsGettingSavedAmountWithoutOriginalPrice()
    {
        $simple = new Simple();
        $simple->setPrice(new Money(100));
        $this->assertEquals(new Money(0), $simple->getSavedAmount());
    }
}
