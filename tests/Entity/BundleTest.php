<?php

namespace Linio\Frontend\Entity;

use DateTime;
use Linio\Frontend\Entity\Bundle\Product as BundleProduct;
use Linio\Type\Money;

class BundleTest extends \PHPUnit_Framework_TestCase
{
    public function testIsDetectingValidBundle()
    {
        $bundle = new Bundle();
        $bundle->setStartDate(new DateTime('yesterday'));
        $bundle->setEndDate(new DateTime('tomorrow'));
        $this->assertTrue($bundle->isValid());
    }

    public function testIsDetectingInvalidBundle()
    {
        $bundle = new Bundle();
        $bundle->setStartDate(new DateTime('-10 days'));
        $bundle->setEndDate(new DateTime('yesterday'));
        $this->assertFalse($bundle->isValid());
    }

    public function testIsValidWithoutEndDate()
    {
        $bundle = new Bundle();
        $bundle->setEndDate(new DateTime('yesterday'));
        $this->assertFalse($bundle->isValid());
    }

    public function testIsValidWithoutStartDate()
    {
        $bundle = new Bundle();
        $bundle->setStartDate(new DateTime('yesterday'));
        $this->assertTrue($bundle->isValid());
    }

    public function testIsCalculatingRegularPrice()
    {
        $bundleProduct1 = new BundleProduct();
        $bundleProduct1->setPrice(new Money(100));

        $bundleProduct2 = new BundleProduct();
        $bundleProduct2->setPrice(new Money(100));

        $bundle = new Bundle();
        $bundle->addProduct($bundleProduct1);
        $bundle->addProduct($bundleProduct1);

        $this->assertEquals(200, $bundle->getRegularPrice()->getMoneyAmount());
    }

    public function testIsCalculatingSavingTotal()
    {
        $bundleProduct1 = new BundleProduct();
        $bundleProduct1->setPrice(new Money(100));

        $bundleProduct2 = new BundleProduct();
        $bundleProduct2->setPrice(new Money(100));

        $bundle = new Bundle();
        $bundle->setSavingPrice(new Money(100));
        $bundle->addProduct($bundleProduct1);
        $bundle->addProduct($bundleProduct1);

        $this->assertEquals(100, $bundle->getSavingTotal()->getMoneyAmount());
    }
}
