<?php

namespace Linio\Frontend\Entity;

use Carbon\Carbon;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Simple;

class ProductTest extends \PHPUnit_Framework_TestCase
{
    public function testIsDetectingImported()
    {
        $product = new Product();
        $product->setImported(true);
        $this->assertTrue($product->isImported());
    }

    public function testIsDetectingOutOfStock()
    {
        $product = new Product();
        $product->setStock(1);
        $this->assertFalse($product->isOutOfStock());
        $product->setStock(0);
        $this->assertTrue($product->isOutOfStock());
    }

    public function testIsGettingFirstValidSimple()
    {
        $simpleA = new Simple();
        $simpleA->setSku('A');

        $simpleB = new Simple();
        $simpleB->setSku('B');
        $simpleB->setStock(1);

        $simpleC = new Simple();
        $simpleC->setSku('C');

        $product = new Product();
        $product->addSimple($simpleA);
        $product->addSimple($simpleB);
        $product->addSimple($simpleC);
        $this->assertEquals($simpleB, $product->getFirstSimple());
    }

    public function testIsGettingFirstSimple()
    {
        $simpleA = new Simple();
        $simpleA->setSku('A');
        $simpleB = new Simple();
        $simpleB->setSku('B');
        $simpleC = new Simple();
        $simpleC->setSku('C');

        $product = new Product();
        $product->addSimple($simpleA);
        $product->addSimple($simpleB);
        $product->addSimple($simpleC);
        $this->assertEquals($simpleA, $product->getFirstSimple());
    }

    public function testIsGettingNullWithoutFirstSimple()
    {
        $product = new Product();
        $this->assertNull($product->getFirstSimple());
    }

    public function testIsGettingFirstMarketplaceChild()
    {
        $simpleA = new Simple();
        $simpleA->setSku('Simple A');
        $marketplaceChildA = new MarketplaceChild();
        $marketplaceChildA->setSku('A');
        $marketplaceChildB = new MarketplaceChild();
        $marketplaceChildB->setSku('B');

        $product = new Product();
        $product->addSimple($simpleA);
        $product->addMarketplaceChild($marketplaceChildA);
        $product->addMarketplaceChild($marketplaceChildB);
        $this->assertEquals($marketplaceChildA, $product->getFirstSimple());
    }

    public function testIsCheckingDeliveryByDate()
    {
        Carbon::setTestNow(Carbon::create(2015, 12, 1));

        $product = new Product();
        $product->setDeliveryTime(5);

        $this->assertFalse($product->isDeliveredBy(Carbon::create(2015, 12, 5)));
        $this->assertFalse($product->isDeliveredBy(Carbon::create(2015, 12, 6)));
        $this->assertFalse($product->isDeliveredBy(Carbon::create(2015, 12, 7)));
        $this->assertTrue($product->isDeliveredBy(Carbon::create(2015, 12, 8)));
        $this->assertTrue($product->isDeliveredBy(Carbon::create(2015, 12, 9)));
    }

    public function testIsCheckingDeliveryByChristmas()
    {
        Carbon::setTestNow(Carbon::create(null, 12, 10));

        $product = new Product();
        $product->setDeliveryTime(15);
        $this->assertFalse($product->isDeliveredByChristmas());

        $product->setDeliveryTime(5);
        $this->assertTrue($product->isDeliveredByChristmas());
    }

    public function testIsReturningTheMarketplaceChildWhenGettingSimple()
    {
        $marketplaceChild = new MarketplaceChild();
        $marketplaceChild->setSku('SKU001');

        $product = new Product();
        $product->addMarketplaceChild($marketplaceChild);

        $this->assertSame($marketplaceChild, $product->getSimple($marketplaceChild->getSku()));
    }

    public function testIsReturningTheSimpleWhenGettingSimple()
    {
        $simple = new Simple();
        $simple->setSku('SKU001');

        $product = new Product();
        $product->addSimple($simple);

        $this->assertSame($simple, $product->getSimple($simple->getSku()));
    }

    public function testIsReturningNothingWhenGettingSimpleAndNeitherAMarketplaceChildOrSimpleExists()
    {
        $product = new Product();

        $this->assertNull($product->getSimple('INVALID_SKU'));
    }

    public function testIsHandlingNullFiltersOnCampaignFilter()
    {
        $campaign = new Campaign();

        $simpleA = new Simple();
        $simpleA->setSku('Simple A');
        $marketplaceChildA = new MarketplaceChild();
        $marketplaceChildA->setSku('A');
        $marketplaceChildB = new MarketplaceChild();
        $marketplaceChildB->setSku('B');

        $product = new Product();
        $product->addSimple($simpleA);
        $product->addMarketplaceChild($marketplaceChildA);
        $product->addMarketplaceChild($marketplaceChildB);

        $this->assertNull($product->filterCampaignProducts($campaign));
    }
}
