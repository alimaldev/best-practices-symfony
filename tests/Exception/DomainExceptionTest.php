<?php

declare(strict_types=1);

namespace Linio\Frontend\Exception;

class DomainExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function testSetContextChangesTheInitialContext()
    {
        $context = [
            'error1' => 'some error',
        ];

        $expected = [
            'error2' => 'some other error',
        ];

        $exception = new DomainException('This is an exception', $context);
        $exception->setContext($expected);

        $this->assertNotSame($context, $exception->getContext());
        $this->assertSame($expected, $exception->getContext());
    }

    public function testAddContextAddsNewContextKey()
    {
        $context = [
            'error1' => 'some error',
        ];

        $additional = [
            'error2' => 'some other error',
        ];

        $exception = new DomainException('This is an exception', $context);
        $exception->addContext($additional);

        $this->assertArrayHasKey('error2', $exception->getContext());
    }

    public function testAddContextOverridesExistingContext()
    {
        $context = [
            'error1' => 'some error',
        ];

        $expected = [
            'error1' => 'some other error',
        ];

        $exception = new DomainException('This is an exception', $context);
        $exception->addContext($expected);

        $this->assertSame($expected, $exception->getContext());
    }
}
