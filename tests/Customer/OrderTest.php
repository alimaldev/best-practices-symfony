<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

use Linio\Frontend\Customer\Order\Package;
use PHPUnit_Framework_TestCase;

class OrderTest extends PHPUnit_Framework_TestCase
{
    public function testItHasPackagesAvailableForReturn()
    {
        $package1 = new Package(1);
        $package1->setAllowReturn(false);

        $package2 = new Package(2);
        $package2->setAllowReturn(true);

        $order = new Order(1234);
        $order->addPackage($package1);
        $order->addPackage($package2);

        $this->assertTrue($order->hasPackagesAvailableForReturn());
    }

    public function testItDoesNotHavePackagesAvailableForReturn()
    {
        $package = new Package(1);
        $package->setAllowReturn(false);

        $order = new Order(1234);
        $order->addPackage($package);

        $this->assertFalse($order->hasPackagesAvailableForReturn());
    }

    public function testItHasPackagesAvailableForReview()
    {
        $package1 = new Package(1);
        $package1->setAllowSellerReview(false);

        $package2 = new Package(2);
        $package2->setAllowSellerReview(true);

        $order = new Order(1234);
        $order->addPackage($package1);
        $order->addPackage($package2);

        $this->assertTrue($order->hasPackagesAvailableForReview());
    }

    public function testItDoesNotHavePackagesAvailableForReview()
    {
        $package = new Package(1);
        $package->setAllowSellerReview(false);

        $order = new Order(1234);
        $order->addPackage($package);

        $this->assertFalse($order->hasPackagesAvailableForReview());
    }
}
