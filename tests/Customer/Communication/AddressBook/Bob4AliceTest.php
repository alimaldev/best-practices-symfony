<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\AddressBook;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Communication\Output\Bob4Alice\Address as AddressOutput;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Location\Exception\AddressException;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\Test\CommunicationTestCase;

class Bob4AliceTest extends CommunicationTestCase
{
    public function setUp()
    {
        $this->loadFixtures(__DIR__ . '/../../../fixtures/customer/address.yml');
    }

    /**
     * @dataProvider addressListResponseProvider
     *
     * @param string $addresses
     */
    public function testIsGettingAddresses(string $addresses)
    {
        $customer = new Customer();
        $customer->setId(2);

        /** @var Address $address1 */
        $address1 = $this->fixtures['address1'];

        /** @var Address $address2 */
        $address2 = $this->fixtures['address2'];

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/list', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], $addresses));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());
        $adapter->setAddressOutput(new AddressOutput());

        $actual = $adapter->getAddresses($customer, $address1->getId());

        $this->assertEquals([$address1, $address2], $actual);
    }

    /**
     * @dataProvider addressListResponseProvider
     *
     * @param string $addresses
     */
    public function testIsGettingAddress(string $addresses)
    {
        $customer = new Customer();
        $customer->setId(2);

        /** @var Address $address1 */
        $address1 = $this->fixtures['address1'];

        $requestBody = [
            'storeId' => 1,
            'customerId' => $customer->getId(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/list', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], $addresses));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());
        $adapter->setAddressOutput(new AddressOutput());

        $actual = $adapter->getAddress($customer, $address1->getId());

        $this->assertEquals($address1, $actual);
    }

    public function testIsCreatingAddress()
    {
        $customer = new Customer();
        $customer->setId(2);

        /** @var Address $address */
        $address = $this->fixtures['address1'];

        $addressToCreate = clone $address;
        $addressToCreate->setId(0);
        $addressToCreate->setCreatedAt(new \DateTime());
        $addressToCreate->setUpdatedAt(new \DateTime());

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'id' => $addressToCreate->getId(),
            'title' => $addressToCreate->getTitle(),
            'type' => $addressToCreate->getType(),
            'firstName' => $addressToCreate->getFirstName(),
            'lastName' => $addressToCreate->getLastName(),
            'prefix' => $addressToCreate->getPrefix(),
            'line1' => $addressToCreate->getLine1(),
            'line2' => $addressToCreate->getLine2(),
            'betweenStreet1' => $addressToCreate->getBetweenStreet1(),
            'betweenStreet2' => $addressToCreate->getBetweenStreet2(),
            'streetNumber' => $addressToCreate->getStreetNumber(),
            'apartment' => $addressToCreate->getApartment(),
            'lot' => $addressToCreate->getLot(),
            'neighborhood' => $addressToCreate->getNeighborhood(),
            'department' => $addressToCreate->getDepartment(),
            'municipality' => $addressToCreate->getMunicipality(),
            'urbanization' => $addressToCreate->getUrbanization(),
            'city' => $addressToCreate->getCity(),
            'cityId' => $addressToCreate->getCityId(),
            'cityName' => $addressToCreate->getCityName(),
            'region' => $addressToCreate->getRegion(),
            'regionId' => $addressToCreate->getRegionId(),
            'regionCode' => $addressToCreate->getRegionCode(),
            'regionName' => $addressToCreate->getRegionName(),
            'postcode' => $addressToCreate->getPostcode(),
            'additionalInformation' => $addressToCreate->getAdditionalInformation(),
            'phone' => $addressToCreate->getPhone(),
            'mobilePhone' => $addressToCreate->getMobilePhone(),
            'countryId' => $addressToCreate->getCountryId(),
            'countryCode' => $addressToCreate->getCountryCode(),
            'countryName' => $addressToCreate->getCountryName(),
            'taxIdentificationNumber' => $addressToCreate->getTaxIdentificationNumber(),
            'isDefaultBillingAddress' => $addressToCreate->isDefaultBilling(),
            'isDefaultShippingAddress' => $addressToCreate->isDefaultShipping(),
            'maternalName' => $addressToCreate->getMaternalName(),
            'invoiceType' => $addressToCreate->getInvoiceType(),
        ];

        $responseBody = [
            'id' => $address->getId(),
            'createdAt' => $address->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => $address->getUpdatedAt()->format('Y-m-d H:i:s'),
            'isDefaultShippingAddress' => $address->isDefaultShipping(),
            'isDefaultBillingAddress' => $address->isDefaultBilling(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/create', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], Json::encode($responseBody)));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());

        $adapter->createAddress($customer, $addressToCreate);

        $this->assertEquals($address->getId(), $addressToCreate->getId());
        $this->assertEquals($address->getCreatedAt()->format('Y-m-d H:i:s'), $addressToCreate->getCreatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals($address->getUpdatedAt()->format('Y-m-d H:i:s'), $addressToCreate->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals($address->isDefaultShipping(), $addressToCreate->isDefaultShipping());
        $this->assertEquals($address->isDefaultBilling(), $addressToCreate->isDefaultShipping());
    }

    public function testIsCreatingAddressAndFallingBackToDefaultCountryId()
    {
        $customer = new Customer();
        $customer->setId(2);

        /** @var Address $address */
        $address = $this->fixtures['address_without_country'];

        $addressToCreate = clone $address;
        $addressToCreate->setId(0);
        $addressToCreate->setCreatedAt(new \DateTime());
        $addressToCreate->setUpdatedAt(new \DateTime());

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'id' => $addressToCreate->getId(),
            'title' => $addressToCreate->getTitle(),
            'type' => $addressToCreate->getType(),
            'firstName' => $addressToCreate->getFirstName(),
            'lastName' => $addressToCreate->getLastName(),
            'prefix' => $addressToCreate->getPrefix(),
            'line1' => $addressToCreate->getLine1(),
            'line2' => $addressToCreate->getLine2(),
            'betweenStreet1' => $addressToCreate->getBetweenStreet1(),
            'betweenStreet2' => $addressToCreate->getBetweenStreet2(),
            'streetNumber' => $addressToCreate->getStreetNumber(),
            'apartment' => $addressToCreate->getApartment(),
            'lot' => $addressToCreate->getLot(),
            'neighborhood' => $addressToCreate->getNeighborhood(),
            'department' => $addressToCreate->getDepartment(),
            'municipality' => $addressToCreate->getMunicipality(),
            'urbanization' => $addressToCreate->getUrbanization(),
            'city' => $addressToCreate->getCity(),
            'cityId' => $addressToCreate->getCityId(),
            'cityName' => $addressToCreate->getCityName(),
            'region' => $addressToCreate->getRegion(),
            'regionId' => $addressToCreate->getRegionId(),
            'regionCode' => $addressToCreate->getRegionCode(),
            'regionName' => $addressToCreate->getRegionName(),
            'postcode' => $addressToCreate->getPostcode(),
            'additionalInformation' => $addressToCreate->getAdditionalInformation(),
            'phone' => $addressToCreate->getPhone(),
            'mobilePhone' => $addressToCreate->getMobilePhone(),
            'countryId' => 42,
            'countryCode' => $addressToCreate->getCountryCode(),
            'countryName' => $addressToCreate->getCountryName(),
            'taxIdentificationNumber' => $addressToCreate->getTaxIdentificationNumber(),
            'isDefaultBillingAddress' => $addressToCreate->isDefaultBilling(),
            'isDefaultShippingAddress' => $addressToCreate->isDefaultShipping(),
            'maternalName' => $addressToCreate->getMaternalName(),
            'invoiceType' => $addressToCreate->getInvoiceType(),
        ];

        $responseBody = [
            'id' => $address->getId(),
            'createdAt' => $address->getCreatedAt()->format('Y-m-d H:i:s'),
            'updatedAt' => $address->getUpdatedAt()->format('Y-m-d H:i:s'),
            'isDefaultShippingAddress' => $address->isDefaultShipping(),
            'isDefaultBillingAddress' => $address->isDefaultBilling(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/create', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(200, [], Json::encode($responseBody)));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setCountryId(42);
        $adapter->setClient($client->reveal());

        $adapter->createAddress($customer, $addressToCreate);

        $this->assertEquals($address->getId(), $addressToCreate->getId());
        $this->assertEquals($address->getCreatedAt()->format('Y-m-d H:i:s'), $addressToCreate->getCreatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals($address->getUpdatedAt()->format('Y-m-d H:i:s'), $addressToCreate->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertEquals($address->isDefaultShipping(), $addressToCreate->isDefaultShipping());
        $this->assertEquals($address->isDefaultBilling(), $addressToCreate->isDefaultShipping());
    }

    public function testIsUpdatingAddress()
    {
        $customer = new Customer();
        $customer->setId(2);

        /** @var Address $address */
        $address = $this->fixtures['address1'];

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'id' => $address->getId(),
            'title' => $address->getTitle(),
            'type' => $address->getType(),
            'firstName' => $address->getFirstName(),
            'lastName' => $address->getLastName(),
            'prefix' => $address->getPrefix(),
            'line1' => $address->getLine1(),
            'line2' => $address->getLine2(),
            'betweenStreet1' => $address->getBetweenStreet1(),
            'betweenStreet2' => $address->getBetweenStreet2(),
            'streetNumber' => $address->getStreetNumber(),
            'apartment' => $address->getApartment(),
            'lot' => $address->getLot(),
            'neighborhood' => $address->getNeighborhood(),
            'department' => $address->getDepartment(),
            'municipality' => $address->getMunicipality(),
            'urbanization' => $address->getUrbanization(),
            'city' => $address->getCity(),
            'cityId' => $address->getCityId(),
            'cityName' => $address->getCityName(),
            'region' => $address->getRegion(),
            'regionId' => $address->getRegionId(),
            'regionCode' => $address->getRegionCode(),
            'regionName' => $address->getRegionName(),
            'postcode' => $address->getPostcode(),
            'additionalInformation' => $address->getAdditionalInformation(),
            'phone' => $address->getPhone(),
            'mobilePhone' => $address->getMobilePhone(),
            'countryId' => $address->getCountryId(),
            'countryCode' => $address->getCountryCode(),
            'countryName' => $address->getCountryName(),
            'taxIdentificationNumber' => $address->getTaxIdentificationNumber(),
            'isDefaultBillingAddress' => $address->isDefaultBilling(),
            'isDefaultShippingAddress' => $address->isDefaultShipping(),
            'maternalName' => $address->getMaternalName(),
            'invoiceType' => $address->getInvoiceType(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());

        $adapter->updateAddress($customer, $address);
    }

    public function testIsUpdatingAddressAndFallingBackToDefaultCountryId()
    {
        $customer = new Customer();
        $customer->setId(2);

        /** @var Address $address */
        $address = $this->fixtures['address_without_country'];

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'id' => $address->getId(),
            'title' => $address->getTitle(),
            'type' => $address->getType(),
            'firstName' => $address->getFirstName(),
            'lastName' => $address->getLastName(),
            'prefix' => $address->getPrefix(),
            'line1' => $address->getLine1(),
            'line2' => $address->getLine2(),
            'betweenStreet1' => $address->getBetweenStreet1(),
            'betweenStreet2' => $address->getBetweenStreet2(),
            'streetNumber' => $address->getStreetNumber(),
            'apartment' => $address->getApartment(),
            'lot' => $address->getLot(),
            'neighborhood' => $address->getNeighborhood(),
            'department' => $address->getDepartment(),
            'municipality' => $address->getMunicipality(),
            'urbanization' => $address->getUrbanization(),
            'city' => $address->getCity(),
            'cityId' => $address->getCityId(),
            'cityName' => $address->getCityName(),
            'region' => $address->getRegion(),
            'regionId' => $address->getRegionId(),
            'regionCode' => $address->getRegionCode(),
            'regionName' => $address->getRegionName(),
            'postcode' => $address->getPostcode(),
            'additionalInformation' => $address->getAdditionalInformation(),
            'phone' => $address->getPhone(),
            'mobilePhone' => $address->getMobilePhone(),
            'countryId' => 42,
            'countryCode' => $address->getCountryCode(),
            'countryName' => $address->getCountryName(),
            'taxIdentificationNumber' => $address->getTaxIdentificationNumber(),
            'isDefaultBillingAddress' => $address->isDefaultBilling(),
            'isDefaultShippingAddress' => $address->isDefaultShipping(),
            'maternalName' => $address->getMaternalName(),
            'invoiceType' => $address->getInvoiceType(),
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setCountryId(42);
        $adapter->setClient($client->reveal());

        $adapter->updateAddress($customer, $address);
    }

    public function testIsRemovingAddress()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'addressId' => 3,
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/delete', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());
        $adapter->setAddressOutput(new AddressOutput());

        $adapter->removeAddress($customer, $address);
    }

    public function testIsSettingDefaultShippingAddress()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'addressId' => 3,
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/default-shipping', ['json' => $requestBody])
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());

        $adapter->setDefaultShipping($customer, $address);
    }

    public function testIsThrowingInputExceptionOnClientErrorWhenSettingDefaultShipping()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $requestBody = [
            'code' => 'exception',
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(json_encode($requestBody), '/address/default-shipping');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('exception');

        $adapter->setDefaultShipping($customer, $address);
    }

    public function testIsThrowingAddressExceptionOnServerErrorWhenSettingDefaultShipping()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $requestBody = [
            'code' => 'exception',
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(json_encode($requestBody), '/address/default-shipping');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(AddressException::class);
        $this->expectExceptionMessage('exception');

        $adapter->setDefaultShipping($customer, $address);
    }

    public function testIsSettingDefaultBillingAddress()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $requestBody = [
            'storeId' => 1,
            'customerId' => 2,
            'addressId' => 3,
        ];

        $client = $this->prophesize(Client::class);
        $client->request('POST', '/address/default-billing', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId($requestBody['storeId']);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client->reveal());

        $adapter->setDefaultBilling($customer, $address);
    }

    public function testIsThrowingInputExceptionOnClientErrorWhenSettingDefaultBilling()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $responseBody = [
            'code' => 'exception',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsClientException(json_encode($responseBody), '/address/default-billing');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('exception');

        $adapter->setDefaultBilling($customer, $address);
    }

    public function testIsThrowingAddressExceptionOnServerErrorWhenSettingDefaultBilling()
    {
        $customer = new Customer();
        $customer->setId(2);

        $address = new Address();
        $address->setId(3);

        $responseBody = [
            'code' => 'exception',
            'message' => '',
            'errors' => [],
        ];

        $client = $this->createMockBob4AliceClientThatThrowsServerException(json_encode($responseBody), '/address/default-billing');

        $platformInformation = new PlatformInformation();
        $platformInformation->setStoreId(1);

        $adapter = new Bob4Alice();
        $adapter->setPlatformInformation($platformInformation);
        $adapter->setClient($client);

        $this->expectException(AddressException::class);
        $this->expectExceptionMessage('exception');

        $adapter->setDefaultBilling($customer, $address);
    }

    /**
     * @return array
     */
    public function addressListResponseProvider(): array
    {
        $this->loadFixtures(__DIR__ . '/../../../fixtures/customer/address.yml');

        /** @var Address $address1 */
        $address1 = $this->fixtures['address1'];

        /** @var Address $address2 */
        $address2 = $this->fixtures['address2'];

        return [
            [
                Json::encode([
                    [
                        'storeId' => 1,
                        'customerId' => 1,
                        'id' => $address1->getId(),
                        'title' => $address1->getTitle(),
                        'type' => $address1->getType(),
                        'firstName' => $address1->getFirstName(),
                        'lastName' => $address1->getLastName(),
                        'prefix' => $address1->getPrefix(),
                        'line1' => $address1->getLine1(),
                        'line2' => $address1->getLine2(),
                        'betweenStreet1' => $address1->getBetweenStreet1(),
                        'betweenStreet2' => $address1->getBetweenStreet2(),
                        'streetNumber' => $address1->getStreetNumber(),
                        'apartment' => $address1->getApartment(),
                        'lot' => $address1->getLot(),
                        'neighborhood' => $address1->getNeighborhood(),
                        'department' => $address1->getDepartment(),
                        'municipality' => $address1->getMunicipality(),
                        'urbanization' => $address1->getUrbanization(),
                        'city' => $address1->getCity(),
                        'cityId' => $address1->getCityId(),
                        'cityName' => $address1->getCityName(),
                        'region' => $address1->getRegion(),
                        'regionId' => $address1->getRegionId(),
                        'regionCode' => $address1->getRegionCode(),
                        'regionName' => $address1->getRegionName(),
                        'postcode' => $address1->getPostcode(),
                        'additionalInformation' => $address1->getAdditionalInformation(),
                        'phone' => $address1->getPhone(),
                        'mobilePhone' => $address1->getMobilePhone(),
                        'countryId' => $address1->getCountryId(),
                        'countryCode' => $address1->getCountryCode(),
                        'countryName' => $address1->getCountryName(),
                        'taxIdentificationNumber' => $address1->getTaxIdentificationNumber(),
                        'isDefaultBillingAddress' => $address1->isDefaultBilling(),
                        'isDefaultShippingAddress' => $address1->isDefaultShipping(),
                        'maternalName' => $address1->getMaternalName(),
                        'invoiceType' => $address1->getInvoiceType(),
                        'createdAt' => $address1->getCreatedAt()->format('Y-m-d H:i:s'),
                        'updatedAt' => $address1->getUpdatedAt()->format('Y-m-d H:i:s'),
                    ],
                    [
                        'storeId' => 1,
                        'customerId' => 1,
                        'id' => $address2->getId(),
                        'title' => $address2->getTitle(),
                        'type' => $address2->getType(),
                        'firstName' => $address2->getFirstName(),
                        'lastName' => $address2->getLastName(),
                        'prefix' => $address2->getPrefix(),
                        'line1' => $address2->getLine1(),
                        'line2' => $address2->getLine2(),
                        'betweenStreet1' => $address2->getBetweenStreet1(),
                        'betweenStreet2' => $address2->getBetweenStreet2(),
                        'streetNumber' => $address2->getStreetNumber(),
                        'apartment' => $address2->getApartment(),
                        'lot' => $address2->getLot(),
                        'neighborhood' => $address2->getNeighborhood(),
                        'department' => $address2->getDepartment(),
                        'municipality' => $address2->getMunicipality(),
                        'urbanization' => $address2->getUrbanization(),
                        'city' => $address2->getCity(),
                        'cityId' => $address2->getCityId(),
                        'cityName' => $address2->getCityName(),
                        'region' => $address2->getRegion(),
                        'regionId' => $address2->getRegionId(),
                        'regionCode' => $address2->getRegionCode(),
                        'regionName' => $address2->getRegionName(),
                        'postcode' => $address2->getPostcode(),
                        'additionalInformation' => $address2->getAdditionalInformation(),
                        'phone' => $address2->getPhone(),
                        'mobilePhone' => $address2->getMobilePhone(),
                        'countryId' => $address2->getCountryId(),
                        'countryCode' => $address2->getCountryCode(),
                        'countryName' => $address2->getCountryName(),
                        'taxIdentificationNumber' => $address2->getTaxIdentificationNumber(),
                        'isDefaultBillingAddress' => $address2->isDefaultBilling(),
                        'isDefaultShippingAddress' => $address2->isDefaultShipping(),
                        'maternalName' => $address2->getMaternalName(),
                        'invoiceType' => $address2->getInvoiceType(),
                        'createdAt' => $address2->getCreatedAt()->format('Y-m-d H:i:s'),
                        'updatedAt' => $address2->getUpdatedAt()->format('Y-m-d H:i:s'),
                    ],
                ]),
            ],
        ];
    }
}
