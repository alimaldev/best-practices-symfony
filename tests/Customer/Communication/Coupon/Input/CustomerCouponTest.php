<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Coupon\Input;

use Linio\Frontend\Entity\Customer\Coupon;
use Linio\Type\Money;

class CustomerCouponTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function couponProvider(): array
    {
        $percentCoupon = [
            'code' => 'P3rc3nTc0d3',
            'validFrom' => '2016-01-01',
            'validUntil' => '2017-01-01',
            'isActive' => 1,
            'discountType' => 'percent',
            'discountPercentage' => 1000,
            'amountAvailable' => null,
            'amount' => null,
        ];

        $outputPercentCoupon = new Coupon();
        $outputPercentCoupon->setCode($percentCoupon['code']);
        $outputPercentCoupon->setValidFrom(new \DateTime($percentCoupon['validFrom']));
        $outputPercentCoupon->setValidUntil(new \DateTime($percentCoupon['validUntil']));
        $outputPercentCoupon->setActive(true);
        $outputPercentCoupon->setDiscountType(Coupon::DISCOUNT_TYPE_PERCENT);
        $outputPercentCoupon->setDiscountPercentage($percentCoupon['discountPercentage'] / 100);

        $moneyCoupon = [
            'code' => 'M0n3yc0d3',
            'validFrom' => '2015-01-01',
            'validUntil' => '2015-06-06',
            'isActive' => 0,
            'discountType' => 'money',
            'discountPercentage' => null,
            'amountAvailable' => 10050,
            'amount' => 20000,
        ];

        $outputMoneyCoupon = new Coupon();
        $outputMoneyCoupon->setCode($moneyCoupon['code']);
        $outputMoneyCoupon->setValidFrom(new \DateTime($moneyCoupon['validFrom']));
        $outputMoneyCoupon->setValidUntil(new \DateTime($moneyCoupon['validUntil']));
        $outputMoneyCoupon->setActive(false);
        $outputMoneyCoupon->setDiscountType(Coupon::DISCOUNT_TYPE_MONEY);
        $outputMoneyCoupon->setBalance(Money::fromCents($moneyCoupon['amountAvailable']));
        $outputMoneyCoupon->setAmount(Money::fromCents($moneyCoupon['amount']));

        return [
            [$percentCoupon, $outputPercentCoupon],
            [$moneyCoupon, $outputMoneyCoupon],
        ];
    }

    /**
     * @dataProvider couponProvider
     *
     * @param array $coupon
     * @param Coupon $outputCoupon
     */
    public function testIsTransformingDataToCustomerCoupon(array $coupon, Coupon $outputCoupon)
    {
        $input = new Bob4Alice();
        $actual = $input->toCoupon($coupon);

        $this->assertEquals($outputCoupon, $actual);
    }

    /**
     * @dataProvider couponProvider
     *
     * @param array $coupon
     * @param Coupon $outputCoupon
     */
    public function testIsTransformingDataToCustomerCouponCollection(array $coupon, Coupon $outputCoupon)
    {
        $coupons = [$outputCoupon];

        $input = new Bob4Alice();
        $actual = $input->toCoupons([$coupon]);

        $this->assertEquals($coupons, $actual);
    }
}
