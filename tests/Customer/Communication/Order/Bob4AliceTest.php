<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Order;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Communication\Output\Bob4Alice\Order;
use Linio\Frontend\Customer\Communication\Output\Bob4Alice\Order as OrderOutput;
use Linio\Frontend\Customer\Order as OrderEntity;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Package;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Test\UnitTestCase;
use Prophecy\Argument;

class Bob4AliceTest extends UnitTestCase
{
    public function testIsFindingOrders()
    {
        $customer = new Customer();
        $customer->setId(1);

        $search = new Search();
        $search->setPage(1);
        $search->setPageSize(10);

        $expectedRequestBody = [
            'customerId' => $customer->getId(),
            'pageSize' => $search->getPageSize(),
            'page' => $search->getPage(),
            'fromDate' => $search->getFromDate()->format('Y-m-d'),
            'toDate' => $search->getToDate()->format('Y-m-d'),
        ];

        $responseContents = file_get_contents(__DIR__ . '/../../../fixtures/customer/resultSetMapper/bob4alice_response.json');
        $response = new Response(200, [], $responseContents);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/order/list', ['json' => $expectedRequestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $orderOutput = $this->prophesize(Order::class);
        $orderOutput->toPaginatedOrders(json_decode($responseContents, true))->shouldBeCalled()->willReturn(new PaginatedResult());

        $adapter = new Bob4Alice();
        $adapter->setClient($client->reveal());
        $adapter->setOrderOutput($orderOutput->reveal());

        $actual = $adapter->findOrders($customer, $search);

        $this->assertEquals(new PaginatedResult(), $actual);
    }

    public function testIsFindingOrderTrackingHistory()
    {
        $customer = new Customer();
        $customer->setId(1);

        $shipments = [new Shipment('123456')];

        $orderOutput = $this->prophesize(OrderOutput::class);
        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice();

        $requestBody = [
            'customerId' => $customer->getId(),
            'orderNumber' => '123456',
        ];

        $responseBody = [
            [
                'trackingCode' => '781846968074',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'FEDEX',
                'updatedAt' => '2015-12-07 21:52:22',
                'skus' => [
                    [
                        'sku' => 'SKU001-001',
                        'quantity' => 1,
                    ],
                    [
                        'sku' => 'SKU002-002',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03 10:26:02'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03 17:58:02'],
                ],
            ],
        ];

        $response = new Response(200, [], json_encode($responseBody));

        $client->request('POST', '/customer/order/tracking', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $orderOutput->toOrderShipments($responseBody)->shouldBeCalled()->willReturn($shipments);

        $adapter->setClient($client->reveal());
        $adapter->setOrderOutput($orderOutput->reveal());

        $actual = $adapter->findOrderTrackingHistory($customer, $requestBody['orderNumber']);

        $this->assertCount(1, $actual);
        $this->assertContainsOnly(Shipment::class, $actual);
        $this->assertSame($shipments, $actual);
    }

    public function testItFailsToFindTheOrderTrackingHistoryDueToInvalidParameters()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $clientException = $this->prophesize(ClientException::class);
        $adapter = new Bob4Alice();

        $requestBody = [
            'customerId' => $customer->getId(),
            'orderNumber' => '1234568901892',
        ];

        $responseBody = [
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [
                'customerId' => [
                    'INVALID_CUSTOMER_ID',
                ],
            ],
        ];

        $response = new Response(400, [], json_encode($responseBody));

        $clientException->getResponse()->willReturn($response);

        $client->request('POST', '/customer/order/tracking', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($clientException->reveal());

        $adapter->setClient($client->reveal());
        $adapter->setOrderOutput(new OrderOutput());

        $this->expectException(OrderException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->findOrderTrackingHistory($customer, $requestBody['orderNumber']);
    }

    public function testItFailsToFindTheOrderTrackingHistoryDueToAServerError()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $serverException = $this->prophesize(ServerException::class);
        $adapter = new Bob4Alice();

        $requestBody = [
            'customerId' => $customer->getId(),
            'orderNumber' => '1234568901892',
        ];

        $responseBody = [
            'code' => 'AN_ERROR_HAS_OCCURRED',
            'message' => 'A unexpected error has occurred. Please check the logs for more information.',
            'errors' => [],
        ];

        $response = new Response(500, [], json_encode($responseBody));

        $serverException->getResponse()->willReturn($response);

        $client->request('POST', '/customer/order/tracking', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($serverException->reveal());

        $adapter->setClient($client->reveal());
        $adapter->setOrderOutput(new OrderOutput());

        $this->expectException(OrderException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->findOrderTrackingHistory($customer, $requestBody['orderNumber']);
    }

    public function testItFindsAnOrderWithOnlyReturnableItems()
    {
        $customer = new Customer();
        $customer->setId(1);

        $orderNumber = '123456789012';

        $responseContents = Json::encode(Json::decode(file_get_contents(__DIR__ . '/../../../fixtures/customer/resultSetMapper/bob4alice_response.json'))['result'][0]);
        $response = new Response(200, [], $responseContents);

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/order/get', Argument::type('array'))
            ->shouldBeCalled()
            ->willReturn($response);

        $item1 = new Item('SO008EL06SIGWLAEC-80806');
        $item1->setAllowReturn(false);

        $item2 = new Item('SA007EL10CYOOLAEC-17400');
        $item2->setAllowReturn(true);

        $order = new OrderEntity(1);
        $order->addPackage(new Package(1));
        $order->addItem($item1);
        $order->addItem($item2);

        $orderOutput = $this->prophesize(OrderOutput::class);
        $orderOutput->toOrder(json_decode($responseContents, true))->shouldBeCalled()->willReturn($order);

        $adapter = new Bob4Alice();
        $adapter->setOrderOutput($orderOutput->reveal());
        $adapter->setClient($client->reveal());

        $actual = $adapter->findOrderWithReturnableItems($customer, $orderNumber);

        $this->assertCount(1, $actual->getItems());
        $this->assertTrue($actual->getItems()[0]->isReturnAllowed());
        $this->assertEmpty($actual->getPackages());
    }

    public function testItFindsOrdersPendingBankConfirmation()
    {
        $customer = new Customer();
        $customer->setId(1);

        $requestBody = [
            'customerId' => 1,
            'page' => 1,
            'pageSize' => 100,
        ];

        $responseBody = [
            [
                'id' => 1,
                'orderNumber' => '123456',
            ],
        ];

        $response = new Response(200, [], json_encode($responseBody));

        $client = $this->prophesize(ClientInterface::class);
        $client->request('POST', '/customer/order/pending-bank-transfer', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $orderOutput = $this->prophesize(OrderOutput::class);
        $orderOutput->toOrders($responseBody)->shouldBeCalled();

        $adapter = new Bob4Alice();
        $adapter->setOrderOutput($orderOutput->reveal());
        $adapter->setClient($client->reveal());

        $adapter->findOrdersPendingBankConfirmation($customer);
    }
}
