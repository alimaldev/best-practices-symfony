<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Output\Bob4Alice;

use DateTime;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Communication\Output\Bob4Alice\Order as OrderOutput;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Customer\Order\Shipping\TrackingEvent;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductService;
use Linio\Test\UnitTestCase;

class OrderTest extends UnitTestCase
{
    public function orderDataProvider()
    {
        $this->loadFixtures(__DIR__ . '/../../../../fixtures/customer/resultSetMapper/bob4alice_mapped_order.yml');

        return [
            [
                [$this->fixtures['mapped_order1']],
                Json::decode(
                    file_get_contents(
                        __DIR__ . '/../../../../fixtures/customer/resultSetMapper/bob4alice_response.json'
                    )
                ),
            ],
            [
                [$this->fixtures['mapped_order2']],
                Json::decode(
                    file_get_contents(
                        __DIR__ . '/../../../../fixtures/customer/resultSetMapper/bob4alice_response_store_pickup.json'
                    )
                ),
            ],
        ];
    }

    /**
     * @dataProvider orderDataProvider
     *
     * @param Order[] $orders
     * @param array $data
     */
    public function testIsMappingPaginatedOrders(array $orders, array $data)
    {
        $expected = new PaginatedResult();
        $expected->setCurrent(1);
        $expected->setPages(2);
        $expected->setItemCount(15);
        $expected->setSize(10);
        $expected->setResult($orders);

        $simple1 = new Simple();
        $simple1->setSku('SO008EL06SIGWLAEC-80806');
        $simple1->addAttribute('variation', 'variation-1');

        $product1 = new Product();
        $product1->setSku('SO008EL06SIGWLAEC');
        $product1->setSlug('slug-1');
        $product1->setHasVariation(true);
        $product1->setVariationType('size');
        $product1->addSimple($simple1);

        $simple2 = new Simple();
        $simple2->setSku('SA007EL10CYOOLAEC-17400');
        $simple2->addAttribute('variation', 'variation-2');

        $product2 = new Product();
        $product2->setSku('SA007EL10CYOOLAEC');
        $product2->setSlug('slug-2');
        $product2->setHasVariation(true);
        $product2->setVariationType('size');
        $product2->addSimple($simple2);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('SO008EL06SIGWLAEC-80806')->shouldBeCalled()->willReturn($product1);
        $productService->getBySku('SA007EL10CYOOLAEC-17400')->shouldBeCalled()->willReturn($product2);

        $output = new OrderOutput();
        $output->setProductService($productService->reveal());
        $output->setImageHost('imagehost.local');

        $actual = $output->toPaginatedOrders($data);

        $this->assertEquals($expected, $actual);
    }

    public function testIsMappingOrders()
    {
        $this->loadFixtures(__DIR__ . '/../../../../fixtures/customer/resultSetMapper/bob4alice_mapped_order.yml');

        $expected = [$this->fixtures['mapped_order1']];
        $data = Json::decode(
            file_get_contents(
                __DIR__ . '/../../../../fixtures/customer/resultSetMapper/bob4alice_response.json'
            )
        )['result'];

        $simple1 = new Simple();
        $simple1->setSku('SO008EL06SIGWLAEC-80806');
        $simple1->addAttribute('variation', 'variation-1');

        $product1 = new Product();
        $product1->setSku('SO008EL06SIGWLAEC');
        $product1->setSlug('slug-1');
        $product1->setHasVariation(true);
        $product1->setVariationType('size');
        $product1->addSimple($simple1);

        $simple2 = new Simple();
        $simple2->setSku('SA007EL10CYOOLAEC-17400');
        $simple2->addAttribute('variation', 'variation-2');

        $product2 = new Product();
        $product2->setSku('SA007EL10CYOOLAEC');
        $product2->setSlug('slug-2');
        $product2->setHasVariation(true);
        $product2->setVariationType('size');
        $product2->addSimple($simple2);

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku('SO008EL06SIGWLAEC-80806')->shouldBeCalled()->willReturn($product1);
        $productService->getBySku('SA007EL10CYOOLAEC-17400')->shouldBeCalled()->willReturn($product2);

        $output = new OrderOutput();
        $output->setProductService($productService->reveal());
        $output->setImageHost('imagehost.local');

        $actual = $output->toOrders($data);

        $this->assertEquals($expected, $actual);
    }

    public function testIsMappingOrderShipments()
    {
        $orderTrackingData = [
            [
                'trackingCode' => '781846968074',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'FEDEX',
                'updatedAt' => '2015-12-07 21:52:22',
                'skus' => [
                    [
                        'sku' => 'SKU001-001',
                        'name' => 'Product name',
                        'quantity' => 1,
                    ],
                    [
                        'sku' => 'SKU002-002',
                        'name' => 'Product name',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03 10:26:02'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03 17:58:02'],
                ],
            ],
            [
                'trackingCode' => 'Z7102304920394',
                'trackingUrl' => 'http://tracking_url_1',
                'carrier' => 'UPS',
                'updatedAt' => '2015-12-07 21:52:22',
                'skus' => [
                    [
                        'sku' => 'SKU002-003',
                        'name' => 'Product name',
                        'quantity' => 1,
                    ],
                ],
                'events' => [
                    ['code' => 'OC', 'description' => 'Orden creada', 'createdAt' => '2015-12-03 10:26:02'],
                    ['code' => 'PU', 'description' => 'Recolectado', 'createdAt' => '2015-12-03 17:58:02'],
                ],
            ],
        ];

        $product1 = new Product();
        $product2 = new Product();
        $product2->setName($orderTrackingData[0]['skus'][1]['name']);
        $product3 = new Product();

        $productService = $this->prophesize(ProductService::class);
        $productService->getBySku($orderTrackingData[0]['skus'][0]['sku'])->willReturn($product1);
        $productService->getBySku($orderTrackingData[0]['skus'][1]['sku'])->willThrow(new ProductNotFoundException('Product not found'));
        $productService->getBySku($orderTrackingData[1]['skus'][0]['sku'])->willReturn($product3);

        $shipment1 = new Shipment($orderTrackingData[0]['trackingCode']);
        $shipment1->setTrackingUrl($orderTrackingData[0]['trackingUrl']);
        $shipment1->setCarrier($orderTrackingData[0]['carrier']);
        $shipment1->setUpdatedAt(new DateTime($orderTrackingData[0]['updatedAt']));
        $shipment1->setItems([
            [
                'product' => $product1,
                'quantity' => 1,
            ],
            [
                'product' => $product2,
                'quantity' => 1,
            ],
        ]);
        $shipment1->setTrackingEvents([
            new TrackingEvent(
                $orderTrackingData[0]['events'][0]['code'],
                $orderTrackingData[0]['events'][0]['description'],
                new DateTime($orderTrackingData[0]['events'][0]['createdAt'])
            ),
            new TrackingEvent(
                $orderTrackingData[0]['events'][1]['code'],
                $orderTrackingData[0]['events'][1]['description'],
                new DateTime($orderTrackingData[0]['events'][1]['createdAt'])
            ),
        ]);

        $shipment2 = new Shipment($orderTrackingData[1]['trackingCode']);
        $shipment2->setTrackingUrl($orderTrackingData[1]['trackingUrl']);
        $shipment2->setCarrier($orderTrackingData[1]['carrier']);
        $shipment2->setUpdatedAt(new DateTime($orderTrackingData[1]['updatedAt']));
        $shipment2->setItems([
            [
                'product' => $product3,
                'quantity' => 1,
            ],
        ]);
        $shipment2->setTrackingEvents([
            new TrackingEvent(
                $orderTrackingData[1]['events'][0]['code'],
                $orderTrackingData[1]['events'][0]['description'],
                new DateTime($orderTrackingData[1]['events'][0]['createdAt'])
            ),
            new TrackingEvent(
                $orderTrackingData[1]['events'][1]['code'],
                $orderTrackingData[1]['events'][1]['description'],
                new DateTime($orderTrackingData[1]['events'][1]['createdAt'])
            ),
        ]);

        $expected = [$shipment1, $shipment2];

        $output = new OrderOutput();
        $output->setProductService($productService->reveal());

        $actual = $output->toOrderShipments($orderTrackingData);

        $this->assertEquals($expected, $actual);
    }
}
