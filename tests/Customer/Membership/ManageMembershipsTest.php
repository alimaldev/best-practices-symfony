<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership;

use Linio\Frontend\Customer\Membership\Communication\LoyaltyProgram\LoyaltyProgramAdapter;
use Linio\Frontend\Customer\Membership\Communication\Partnership\PartnershipAdapter;
use Linio\Frontend\Entity\Customer;
use PHPUnit_Framework_TestCase;

class ManageMembershipsTest extends PHPUnit_Framework_TestCase
{
    public function testItSignsTheCustomerUpForAPartnership()
    {
        $adapter = $this->prophesize(PartnershipAdapter::class);
        $manageMemberships = new ManageMemberships();
        $manageMemberships->setPartnershipAdapter($adapter->reveal());

        $customer = new Customer();
        $partnership = new Partnership('PROGRAM', '123456');

        $adapter->signup($customer, $partnership)->shouldBeCalled();

        $manageMemberships->signupForPartnership($customer, $partnership);
    }

    public function testItUpdatesAPartnership()
    {
        $adapter = $this->prophesize(PartnershipAdapter::class);
        $manageMemberships = new ManageMemberships();
        $manageMemberships->setPartnershipAdapter($adapter->reveal());

        $customer = new Customer();
        $partnership = new Partnership('PROGRAM', '123456');

        $adapter->update($customer, $partnership)->shouldBeCalled();

        $manageMemberships->updatePartnership($customer, $partnership);
    }

    public function testItGetsTheLoyaltyProgramForACustomer()
    {
        $adapter = $this->prophesize(LoyaltyProgramAdapter::class);
        $manageMemberships = new ManageMemberships();
        $manageMemberships->setLoyaltyProgramAdapter($adapter->reveal());

        $customer = new Customer();

        $adapter->getProgram($customer)->shouldBeCalled();

        $manageMemberships->getLoyaltyProgram($customer);
    }
}
