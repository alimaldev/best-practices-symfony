<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership\Communication\LoyaltyProgram;

use DateTime;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use PHPUnit_Framework_TestCase;

class Bob4AliceTest extends PHPUnit_Framework_TestCase
{
    public function testIsSigningCustomerOnLoyaltyProgram()
    {
        $customer = new Customer();
        $customer->setId(1);

        $loyaltyProgram = new LoyaltyProgram('HervalPlus', '4242');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        $client->request('POST', '/loyalty/set-registration-number', ['json' => $request])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $adapter->signup($customer, $loyaltyProgram);
    }

    public function testIsNotSigningCustomerOnLoyaltyProgramWithInvalidRequest()
    {
        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn(new Response(400, [], '{"code": "foobar"}'));

        $customer = new Customer();
        $customer->setId(1);

        $loyaltyProgram = new LoyaltyProgram('HervalPlus', '4242');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        $client->request('POST', '/loyalty/set-registration-number', ['json' => $request])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('foobar');

        $adapter->signup($customer, $loyaltyProgram);
    }

    public function testIsNotSigningCustomerOnLoyaltyProgramWithServerError()
    {
        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn(new Response(500, [], '{"code": "foobar"}'));

        $customer = new Customer();
        $customer->setId(1);

        $loyaltyProgram = new LoyaltyProgram('HervalPlus', '4242');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        $client->request('POST', '/loyalty/set-registration-number', ['json' => $request])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('foobar');

        $adapter->signup($customer, $loyaltyProgram);
    }

    public function testIsUpdatingCustomerOnLoyaltyProgram()
    {
        $customer = new Customer();
        $customer->setId(1);

        $loyaltyProgram = new LoyaltyProgram('HervalPlus', '4242');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        $client->request('POST', '/loyalty/set-registration-number', ['json' => $request])
            ->shouldBeCalled()
            ->willReturn(new Response(204));

        $adapter->update($customer, $loyaltyProgram);
    }

    public function testIsNotUpdatingCustomerOnLoyaltyProgramWithInvalidRequest()
    {
        $exception = $this->prophesize(ClientException::class);
        $exception->getResponse()->willReturn(new Response(400, [], '{"code": "foobar"}'));

        $customer = new Customer();
        $customer->setId(1);

        $loyaltyProgram = new LoyaltyProgram('HervalPlus', '4242');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        $client->request('POST', '/loyalty/set-registration-number', ['json' => $request])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('foobar');

        $adapter->update($customer, $loyaltyProgram);
    }

    public function testIsNotUpdatingCustomerOnLoyaltyProgramWithServerError()
    {
        $exception = $this->prophesize(ServerException::class);
        $exception->getResponse()->willReturn(new Response(500, [], '{"code": "foobar"}'));

        $customer = new Customer();
        $customer->setId(1);

        $loyaltyProgram = new LoyaltyProgram('HervalPlus', '4242');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        $client->request('POST', '/loyalty/set-registration-number', ['json' => $request])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('foobar');

        $adapter->update($customer, $loyaltyProgram);
    }

    public function testIsGettingTheLoyaltyProgram()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
        ];

        $response = new Response(200, [], Json::encode([
            'programId' => 1,
            'programName' => 'HervalPlus',
            'registrationNumber' => '1234567',
            'registrationDate' => '2016-01-01 00:00:00',
        ]));

        $client->request('POST', '/loyalty/get-account', ['json' => $request])
            ->shouldBeCalled()
            ->willReturn($response);

        $loyaltyProgram = $adapter->getProgram($customer);

        $this->assertSame('HervalPlus', $loyaltyProgram->getName());
        $this->assertSame('1234567', $loyaltyProgram->getId());
        $this->assertEquals(new DateTime('2016-01-01 00:00:00'), $loyaltyProgram->getRegistrationDate());
    }

    public function testIsFailingWhileGettingTheLoyaltyProgramWithAnInvalidRequest()
    {
        $customer = new Customer();
        $customer->setId(1);

        $exception = $this->prophesize(ClientException::class);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => 1,
        ];

        $response = new Response(400, [], Json::encode([
            'code' => ExceptionMessage::INVALID_REQUEST,
            'message' => null,
            'errors' => [],
        ]));

        $exception->getResponse()->willReturn($response);

        $client->request('POST', '/loyalty/get-account', ['json' => $request])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage(ExceptionMessage::INVALID_REQUEST);

        $adapter->getProgram($customer);
    }

    public function testIsFailingWhenAServerErrorOccursWhileGettingTheLoyaltyProgram()
    {
        $customer = new Customer();
        $customer->setId(1);

        $exception = $this->prophesize(ServerException::class);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $request = [
            'customerId' => $customer->getId(),
        ];

        $response = new Response(400, [], Json::encode([
            'code' => 'AN_ERROR_HAS_OCCURRED',
            'message' => null,
            'errors' => [],
        ]));

        $exception->getResponse()->willReturn($response);

        $client->request('POST', '/loyalty/get-account', ['json' => $request])
            ->shouldBeCalled()
            ->willThrow($exception->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->getProgram($customer);
    }
}
