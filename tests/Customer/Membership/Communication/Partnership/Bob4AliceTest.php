<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership\Communication\Partnership;

use DateTime;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Linio\Frontend\Customer\Membership\Partnership;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use PHPUnit_Framework_TestCase;

class Bob4AliceTest extends PHPUnit_Framework_TestCase
{
    public function testItSignsTheCustomerUpForThePartnership()
    {
        $customer = new Customer();
        $customer->setId(1);

        $partnership = new Partnership('PROGRAM', '123456');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        $response = new Response(204);

        $client->request('POST', '/partnership/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $this->assertEmpty($adapter->signup($customer, $partnership));
    }

    public function testItFailsToSignTheCustomerUpForThePartnershipDueToInvalidParameters()
    {
        $customer = new Customer();
        $customer->setId(0);

        $partnership = new Partnership('PROGRAM', '123456');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $clientException = $this->prophesize(ClientException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        $response = new Response(400, [], json_encode([
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [
                'customerId' => [
                    'INVALID_CUSTOMER_ID',
                ],
            ],
        ]));

        $clientException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($clientException->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->signup($customer, $partnership);
    }

    public function testItFailsToSignTheCustomerUpForThePartnershipDueAServerError()
    {
        $customer = new Customer();
        $customer->setId(1);

        $partnership = new Partnership('PROGRAM', '123456');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $serverException = $this->prophesize(ServerException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        $response = new Response(500, [], json_encode([
            'code' => 'AN_ERROR_HAS_OCCURRED',
            'message' => 'A unexpected error has occurred. Please check the logs for more information.',
            'errors' => [],
        ]));

        $serverException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($serverException->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->signup($customer, $partnership);
    }

    public function testItUpdatesThePartnership()
    {
        $customer = new Customer();
        $customer->setId(1);

        $partnership = new Partnership('PROGRAM', '123456');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        $response = new Response(204);

        $client->request('POST', '/partnership/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $this->assertEmpty($adapter->update($customer, $partnership));
    }

    public function testItFailsToUpdateThePartnershipDueToInvalidParameters()
    {
        $customer = new Customer();
        $customer->setId(0);

        $partnership = new Partnership('PROGRAM', '123456');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $clientException = $this->prophesize(ClientException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        $response = new Response(400, [], json_encode([
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [
                'customerId' => [
                    'INVALID_CUSTOMER_ID',
                ],
            ],
        ]));

        $clientException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($clientException->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->update($customer, $partnership);
    }

    public function testItFailsToUpdateThePartnershipDueAServerError()
    {
        $customer = new Customer();
        $customer->setId(1);

        $partnership = new Partnership('PROGRAM', '123456');

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $serverException = $this->prophesize(ServerException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        $response = new Response(500, [], json_encode([
            'code' => 'AN_ERROR_HAS_OCCURRED',
            'message' => 'A unexpected error has occurred. Please check the logs for more information.',
            'errors' => [],
        ]));

        $serverException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/update', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($serverException->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->update($customer, $partnership);
    }

    public function testItGetsThePartnershipsForACustomer()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $requestBody = [
            'customerId' => $customer->getId(),
        ];

        $response = new Response(200, [], json_encode([
            [
                'partnership' => [
                    'id' => 1,
                    'code' => 'TEST',
                    'name' => 'Test Partnership',
                ],
                'identifier' => '123456',
                'level' => 'zero',
                'isActive' => true,
                'lastStatusCheck' => '2016-06-03',
            ],
        ]));

        $client->request('POST', '/partnership/list', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $partnerships = $adapter->getPartnerships($customer);

        $partnership = $partnerships[0];

        $this->assertEquals('TEST', $partnership->getCode());
        $this->assertEquals('Test Partnership', $partnership->getName());
        $this->assertEquals('123456', $partnership->getAccountNumber());
        $this->assertEquals('zero', $partnership->getLevel());
        $this->assertTrue($partnership->isActive());
        $this->assertEquals(new DateTime('2016-06-03'), $partnership->getLastStatusCheck());
    }

    public function testItGetsTheAvailablePartnershipsForACustomer()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $requestBody = [
            'customerId' => $customer->getId(),
        ];

        $response = new Response(200, [], json_encode([
            [
                'partnership' => [
                    'id' => 1,
                    'code' => 'TEST',
                    'name' => 'Test Partnership',
                ],
                'identifier' => null,
                'level' => null,
                'isActive' => false,
                'lastStatusCheck' => null,
            ],
        ]));

        $client->request('POST', '/partnership/list', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $partnerships = $adapter->getPartnerships($customer);

        $partnership = $partnerships[0];

        $this->assertEquals('TEST', $partnership->getCode());
        $this->assertEquals('Test Partnership', $partnership->getName());
        $this->assertNull($partnership->getAccountNumber());
        $this->assertNull($partnership->getLevel());
        $this->assertFalse($partnership->isActive());
        $this->assertNull($partnership->getLastStatusCheck());
    }

    public function testItFailsToGetThePartnershipsDueToInvalidParameters()
    {
        $customer = new Customer();
        $customer->setId(0);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $clientException = $this->prophesize(ClientException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
        ];

        $response = new Response(400, [], json_encode([
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [
                'customerId' => [
                    'INVALID_CUSTOMER_ID',
                ],
            ],
        ]));

        $clientException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/list', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($clientException->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->getPartnerships($customer);
    }

    public function testItFailsToGetThePartnershipsDueAServerError()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $serverException = $this->prophesize(ServerException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
        ];

        $response = new Response(500, [], json_encode([
            'code' => 'AN_ERROR_HAS_OCCURRED',
            'message' => 'A unexpected error has occurred. Please check the logs for more information.',
            'errors' => [],
        ]));

        $serverException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/list', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($serverException->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->getPartnerships($customer);
    }

    public function testItRemovesThePartnershipsForACustomer()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => 'test',
        ];

        $response = new Response(204);

        $client->request('POST', '/partnership/delete', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willReturn($response);

        $this->assertEmpty($adapter->remove($customer, 'test'));
    }

    public function testItFailsToRemoveThePartnershipsDueToInvalidParameters()
    {
        $customer = new Customer();
        $customer->setId(0);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $clientException = $this->prophesize(ClientException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => 'test',
        ];

        $response = new Response(400, [], json_encode([
            'code' => 'INVALID_REQUEST',
            'message' => '',
            'errors' => [
                'customerId' => [
                    'INVALID_CUSTOMER_ID',
                ],
            ],
        ]));

        $clientException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/delete', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($clientException->reveal());

        $this->expectException(InputException::class);
        $this->expectExceptionMessage('INVALID_REQUEST');

        $adapter->remove($customer, 'test');
    }

    public function testItFailsToRemoveThePartnershipsDueAServerError()
    {
        $customer = new Customer();
        $customer->setId(1);

        $client = $this->prophesize(ClientInterface::class);
        $adapter = new Bob4Alice($client->reveal());
        $serverException = $this->prophesize(ServerException::class);

        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => 'test',
        ];

        $response = new Response(500, [], json_encode([
            'code' => 'AN_ERROR_HAS_OCCURRED',
            'message' => 'A unexpected error has occurred. Please check the logs for more information.',
            'errors' => [],
        ]));

        $serverException->getResponse()->willReturn($response);

        $client->request('POST', '/partnership/delete', ['json' => $requestBody])
            ->shouldBeCalled()
            ->willThrow($serverException->reveal());

        $this->expectException(DomainException::class);
        $this->expectExceptionMessage('AN_ERROR_HAS_OCCURRED');

        $adapter->remove($customer, 'test');
    }
}
