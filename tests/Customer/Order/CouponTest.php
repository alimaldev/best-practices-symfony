<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use InvalidArgumentException;
use PHPUnit_Framework_TestCase;

class CouponTest extends PHPUnit_Framework_TestCase
{
    public function testItOnlyAcceptsFixedFixedOrPercentTypes()
    {
        $this->expectException(InvalidArgumentException::class);

        new Coupon('code', 'invalid');
    }
}
