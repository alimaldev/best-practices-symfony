<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Carbon\Carbon;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Test\UnitTestCase;
use Linio\Type\Date;
use Prophecy\Argument;
use Symfony\Component\Translation\TranslatorInterface;

class MakeDeliveryStatusFriendlyTest extends UnitTestCase
{
    /**
     * @return array
     */
    public function statusProvider() : array
    {
        $today = new Date('2016-06-24'); // Friday
        $yesterday = (clone $today)->modify('-1 day');
        $twoDaysAgo = (clone $today)->modify('-2 day');
        $past = (clone $today)->modify('-4 day');
        $tomorrow = (clone $today)->modify('+1 day');
        $twoDaysAhead = new Date('2016-06-26'); // Sunday
        $future = new Date('2099-01-30');

        // Expected delivery, updated delivery, deliveredAt, status, translation, translation key
        return [
            [$today, $today, $twoDaysAhead, 'closed', 'Closed', 'customer.order.closed'],
            [$today, null, null, 'canceled', 'Canceled', 'customer.order.canceled'],
            [$today, null, null, 'refundNeeded', 'Refund Needed', 'customer.order.refund_needed'],
            [$today, $today, null, 'refunded', 'Refunded', 'customer.order.refunded'],
            [$today, null, null, 'pendingConfirmation', 'Pending Confirmation', 'customer.order.pending_confirmation'],
            [$today, null, null, 'confirmed', 'Confirmed', 'customer.order.confirmed'],
            [$today, null, null, 'invalid', 'Invalid', 'customer.order.invalid'],
            [$today, $today, null, 'shipped', 'Delivering today', 'customer.order.delivering_today'],
            [$tomorrow, $tomorrow, null, 'shipped', 'Delivering tomorrow', 'customer.order.delivering_tomorrow'],
            [$twoDaysAhead, $twoDaysAhead, null, 'shipped', 'Delivering on DayOfWeek', 'customer.order.delivering_on_weekday'],
            [$future, $future, null, 'shipped', 'Delivering on 2099-01-30', 'customer.order.delivering_on_date'],
            [$today, $today, null, 'delivered', 'Delivered', 'customer.order.delivered'],
            [$today, $today, $today, 'delivered', 'Delivered Today', 'customer.order.delivered_today'],
            [$today, $yesterday, $yesterday, 'delivered', 'Delivered Yesterday', 'customer.order.delivered_yesterday'],
            [$today, $twoDaysAgo, $twoDaysAgo, 'delivered', 'Delivered on Tuesday', 'customer.order.delivered_on_weekday'],
            [$today, $past, $past, 'delivered', 'Delivered on Date', 'customer.order.delivered_on_date'],
            [null, null, null, 'anything else', 'Shipping Soon', 'customer.order.shipping_soon'],
        ];
    }

    /**
     * @dataProvider statusProvider
     *
     * @param $expectedDeliveryDate
     * @param $updatedDeliveryDate
     * @param $deliveredAt
     * @param $status
     * @param $expectedTranslation
     * @param $translationKey
     */
    public function testIsGettingFriendlyStatusForPackage(
        $expectedDeliveryDate,
        $updatedDeliveryDate,
        $deliveredAt,
        $status,
        $expectedTranslation,
        $translationKey
    ) {
        Carbon::setTestNow(new Carbon('2016-06-24'));

        $item = new Item('CO752FA0DJ51CLAEC-83271');
        $item->setStatus('canceled');

        $package = new Package(1);
        $package->addItem($item);
        $package->setStatus($status);

        if (isset($expectedDeliveryDate)) {
            $package->setExpectedDeliveryDate($expectedDeliveryDate);
        }

        if (isset($updatedDeliveryDate)) {
            $package->setUpdatedDeliveryDate($updatedDeliveryDate);
        }

        if (isset($deliveredAt)) {
            $package->setDeliveredAt($deliveredAt);
        }

        $translator = $this->prophesize(TranslatorInterface::class);

        $translator->trans($translationKey, Argument::any())->willReturn($expectedTranslation)->shouldBeCalled();
        $translator->trans(Argument::containingString('global.weekdays.'))->willReturn('DayOfWeek');
        $translator->trans(Argument::containingString('global.months.'))->willReturn('Month');

        $makeDeliveryStatusFriendlyService = new MakeDeliveryStatusFriendly();
        $makeDeliveryStatusFriendlyService->setTranslator($translator->reveal());
        $makeDeliveryStatusFriendlyService->setTimezone('UTC');

        $friendlyStatus = $makeDeliveryStatusFriendlyService->getPackageFriendlyStatus($package);

        $this->assertEquals($expectedTranslation, $friendlyStatus);
    }

    /**
     * @dataProvider statusProvider
     *
     * @param $expectedDeliveryDate
     * @param $updatedDeliveryDate
     * @param $deliveredAt
     * @param $status
     * @param $expectedTranslation
     * @param $translationKey
     */
    public function testIsGettingFriendlyStatusForItem(
        $expectedDeliveryDate,
        $updatedDeliveryDate,
        $deliveredAt,
        $status,
        $expectedTranslation,
        $translationKey
    ) {
        Carbon::setTestNow(new Carbon('2016-06-24'));

        $item = new Item('CO752FA0DJ51CLAEC-83271');
        $item->setStatus($status);

        if (isset($expectedDeliveryDate)) {
            $item->setExpectedDeliveryDate($expectedDeliveryDate);
        }

        if (isset($updatedDeliveryDate)) {
            $item->setUpdatedDeliveryDate($updatedDeliveryDate);
        }

        if (isset($deliveredAt)) {
            $item->setDeliveredAt($deliveredAt);
        }

        $translator = $this->prophesize(TranslatorInterface::class);

        $translator->trans($translationKey, Argument::any())->willReturn($expectedTranslation)->shouldBeCalled();
        $translator->trans(Argument::containingString('global.weekdays.'))->willReturn('DayOfWeek');
        $translator->trans(Argument::containingString('global.months.'))->willReturn('Month');

        $makeDeliveryStatusFriendlyService = new MakeDeliveryStatusFriendly();
        $makeDeliveryStatusFriendlyService->setTranslator($translator->reveal());
        $makeDeliveryStatusFriendlyService->setTimezone('UTC');

        $friendlyStatus = $makeDeliveryStatusFriendlyService->getItemFriendlyStatus($item);

        $this->assertEquals($expectedTranslation, $friendlyStatus);
    }
}
