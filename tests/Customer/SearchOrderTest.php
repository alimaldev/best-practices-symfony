<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

use Linio\Frontend\Customer\Communication\Order\OrderAdapter;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Test\UnitTestCase;
use Linio\Type\Date;

class SearchOrderTest extends UnitTestCase
{
    public function testIsFindOrdersCallingTheAdapter()
    {
        $expected = new PaginatedResult();

        $customer = new Customer();
        $search = new Search(new Date());

        $adapter = $this->prophesize(OrderAdapter::class);
        $adapter->findOrders($customer, $search)
            ->shouldBeCalled()
            ->willReturn($expected);

        $searchOrder = new SearchOrder();
        $searchOrder->setAdapter($adapter->reveal());

        $actual = $searchOrder->findOrders($customer, $search);

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindOrderCallingTheAdapter()
    {
        $customer = new Customer();
        $expected = new Order(1);

        $adapter = $this->prophesize(OrderAdapter::class);
        $adapter->findOrder($customer, '1234567')
            ->shouldBeCalled()
            ->willReturn($expected);

        $searchOrder = new SearchOrder();
        $searchOrder->setAdapter($adapter->reveal());

        $actual = $searchOrder->findOrder($customer, '1234567');

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindLoyaltyOrdersCallingTheAdapter()
    {
        $customer = new Customer();

        $adapter = $this->prophesize(OrderAdapter::class);
        $adapter->findLoyaltyOrders($customer)
            ->shouldBeCalled()
            ->willReturn([]);

        $searchOrder = new SearchOrder();
        $searchOrder->setAdapter($adapter->reveal());

        $actual = $searchOrder->findLoyaltyOrders($customer);

        $this->assertEquals([], $actual);
    }

    public function testIsFindPendingBankConfirmationOrdersCallingTheAdapter()
    {
        $customer = new Customer();

        $adapter = $this->prophesize(OrderAdapter::class);
        $adapter->findOrdersPendingBankConfirmation($customer)
            ->shouldBeCalled()
            ->willReturn([]);

        $searchOrder = new SearchOrder();
        $searchOrder->setAdapter($adapter->reveal());

        $actual = $searchOrder->findOrdersPendingBankConfirmation($customer);

        $this->assertEquals([], $actual);
    }

    public function testIsFindOrderTrackingHistoryCallingTheAdapter()
    {
        $customer = new Customer();
        $expected = [new Shipment('123456789')];

        $adapter = $this->prophesize(OrderAdapter::class);
        $adapter->findOrderTrackingHistory($customer, '1234567')
            ->shouldBeCalled()
            ->willReturn($expected);

        $searchOrder = new SearchOrder();
        $searchOrder->setAdapter($adapter->reveal());

        $actual = $searchOrder->findOrderTrackingHistory($customer, '1234567');

        $this->assertEquals($expected, $actual);
    }

    public function testIsFindOrderWithReturnableItemsCallingTheAdapter()
    {
        $customer = new Customer();

        $item = new Item('SKU1');
        $item->setAllowReturn(true);

        $order = new Order(1234567);
        $order->addItem($item);

        $adapter = $this->prophesize(OrderAdapter::class);
        $adapter->findOrderWithReturnableItems($customer, '1234567')
            ->shouldBeCalled()
            ->willReturn($order);

        $searchOrder = new SearchOrder();
        $searchOrder->setAdapter($adapter->reveal());

        $actual = $searchOrder->findOrderWithReturnableItems($customer, '1234567');

        $this->assertEquals($order, $actual);
    }
}
