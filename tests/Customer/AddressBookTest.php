<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

use Linio\Frontend\Customer\Communication\AddressBook\AddressBookAdapter;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Location\Address;
use Linio\Test\UnitTestCase;

class AddressBookTest extends UnitTestCase
{
    public function testIsGetAddressesCallingTheAdapter()
    {
        $customer = new Customer();

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->getAddresses($customer)
            ->shouldBeCalled()
            ->willReturn([]);

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $actual = $addressBook->getAddresses($customer);

        $this->assertEquals([], $actual);
    }

    public function testIsGetAddressCallingTheAdapter()
    {
        $address = new Address();
        $address->setId(5);

        $customer = new Customer();

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->getAddress($customer, $address->getId())
            ->shouldBeCalled()
            ->willReturn($address);

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $actual = $addressBook->getAddress($customer, $address->getId());

        $this->assertInstanceOf(Address::class, $actual);
        $this->assertEquals($address->getId(), $actual->getId());
    }

    public function testIsCreateAddressCallingTheAdapter()
    {
        $address = new Address();
        $address->setId(5);

        $customer = new Customer();

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->createAddress($customer, $address)
            ->shouldBeCalled();

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $addressBook->createAddress($customer, $address);
    }

    public function testIsUpdateAddressCallingTheAdapter()
    {
        $address = new Address();
        $address->setId(5);

        $customer = new Customer();

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->updateAddress($customer, $address)
            ->shouldBeCalled();

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $addressBook->updateAddress($customer, $address);
    }

    public function testIsRemoveAddressCallingTheAdapter()
    {
        $address = new Address();
        $address->setId(5);

        $customer = new Customer();

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->removeAddress($customer, $address)
            ->shouldBeCalled();

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $addressBook->removeAddress($customer, $address);
    }

    public function testIsSettingDefaultShippingCallingTheAdapter()
    {
        $address = new Address();
        $address->setId(5);

        $customer = new Customer();
        $customer->setId(1);

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->setDefaultShipping($customer, $address)
            ->shouldBeCalled();

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $addressBook->setDefaultShipping($customer, $address);
    }

    public function testIsSettingDefaultBillingCallingTheAdapter()
    {
        $address = new Address();
        $address->setId(5);

        $customer = new Customer();
        $customer->setId(1);

        $adapter = $this->prophesize(AddressBookAdapter::class);
        $adapter->setDefaultBilling($customer, $address)
            ->shouldBeCalled();

        $addressBook = new AddressBook();
        $addressBook->setAdapter($adapter->reveal());

        $addressBook->setDefaultBilling($customer, $address);
    }
}
