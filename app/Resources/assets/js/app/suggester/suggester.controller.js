'use strict';

/**
 * SuggestionController Constructor
 * @param {Scope} $scope
 * @param {CartService} cartService
 */
angular.module('linioApp').controller('SuggesterController', [
  '$rootScope', '$scope', '$timeout', 'suggesterService',
  function ($rootScope, $scope, $timeout, suggesterService) {
    var initialLoad = true;
    var limitSuggestions = 4;
    $scope.q = '';
    $scope.focusSuggesterId = 0;
    $scope.suggestions = [];

    $scope.tabSuggestion = function (key) {
      if (key.which === 13) {
        if ($scope.focusSuggesterId > 0) {
          key.preventDefault();
          var selectedSuggestion =  $scope.suggestions[$scope.focusSuggesterId - 1];
          parent.window.location.href = selectedSuggestion.link;
        }
      } else {
        if (key.which === 38) {
          if ($scope.focusSuggesterId >= 1) {
            $scope.focusSuggesterId--;
          } else {
            $scope.focusSuggesterId = $scope.counterSuggesterItems;
          }

          key.preventDefault();
        } else if (key.which === 40) {
          if ($scope.focusSuggesterId < $scope.counterSuggesterItems) {
            $scope.focusSuggesterId++;
          } else {
            $scope.focusSuggesterId = 0;
          }

          key.preventDefault();
        }

      }
    };

    $scope.unsetInitialLoad = function () {
      initialLoad = false;
    };

    $scope.activeSuggest = function () {
      if (!initialLoad) {
        $rootScope.suggestActive = true;

        if ($scope.query != '') {
          $scope.getSuggestion($scope.query);
        }
      }
    };

    $scope.getSuggestion = function (query) {
      if (initialLoad) {
        $scope.unsetInitialLoad();
        $scope.activeSuggest();
      }

      suggesterService.getSuggestion(query)
        .then(
          function (response) {
            $scope.suggestionBrands = [];
            $scope.suggestionCategories = [];
            $scope.suggestionPopularSearch = [];
            $scope.suggestionQuerySearch = [];
            $rootScope.matchedString = query;
            $scope.suggestions = [];
            $scope.focusSuggesterId = 0;
            $scope.counterSuggesterItems = 0;

            angular.forEach(response.data, function (suggestion) {
              switch (suggestion.ty) {
                case 0:
                  if (suggestion.hasOwnProperty('br')) {
                    $scope.suggestionBrands.push(mapBrand(suggestion));
                  } else {
                    $scope.suggestionCategories.push(mapCategory(suggestion));
                  }

                  break;

                case 1:
                  $scope.suggestionPopularSearch.push(mapProduct(suggestion));
                  break;

                case 2:
                  $scope.suggestionQuerySearch.push(mapQuery(suggestion));
                  break;

              }
            });

            if ($scope.suggestionBrands == '' &&
              $scope.suggestionCategories == '' &&
              $scope.suggestionPopularSearch == ''
            ) {
              $rootScope.suggestNoResults = true;
            } else {
              $rootScope.suggestNoResults = false;
              $scope.orderSuggestions($scope.suggestionQuerySearch, 0, '/search?q=');
              $scope.orderSuggestions($scope.suggestionCategories, 0, '//');
              $scope.orderSuggestions($scope.suggestionBrands, limitSuggestions, '//');
              $scope.orderSuggestions($scope.suggestionPopularSearch, limitSuggestions, '//');
            }
          }

        );
    };

    $scope.orderSuggestions = function (categories, limitSuggestions, pathURL) {
      if (limitSuggestions > 0) {
        categories.splice(limitSuggestions);
      }

      angular.forEach(categories, function (category) {
        $scope.counterSuggesterItems++;
        category.suggesterId = $scope.counterSuggesterItems;

        $scope.suggestions.push({
          id: $scope.counterSuggesterItems,
          link: pathURL + category.link,
        });
      });
    };

    var mapBrand = function (suggestion) {
      var brand = {};

      brand.name = suggestion.br;
      brand.category = suggestion.ca;
      brand.title = suggestion.ti;
      brand.link = suggestion.li;

      return brand;
    };

    var mapCategory = function (suggestion) {
      var category = {};

      category.name = suggestion.ca;
      category.title = suggestion.ti;
      category.link = suggestion.li;

      return category;
    };

    var mapProduct = function (suggestion) {
      var product = {};

      product.name = suggestion.br;
      product.category = suggestion.ca;
      product.image = suggestion.th;
      product.title = suggestion.ti;
      product.link = suggestion.li;
      product.sku = suggestion.sku;

      if (suggestion.hasOwnProperty('co')) {
        product.color = suggestion.co;
      } else {
        product.color = '';
      }

      return product;
    };

    var mapQuery = function (suggestion) {
      var query = {};
      var splitedTitle = suggestion.ti.split(' ');
      var link = '';

      splitedTitle.forEach(function (i) {
        link += (link === '') ? i : '+' + i;
      });

      query.title = suggestion.ti;
      query.link = 'search?q=' + link;

      return query;
    };
  },

]);
