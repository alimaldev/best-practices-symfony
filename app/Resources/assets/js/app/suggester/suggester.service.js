'use strict';

/**
 * suggesterService
 */
angular.module('linioApp').factory('suggesterService', ['$http', function ($http) {
  return {
    getSuggestion: function (value) {
      return $http.get(
        lsfConfig.suggesterUrl + value + '?key=' + lsfConfig.suggesterKey, {
          withCredentials: true,
          spinner: false,
        }
      );
    },
  };
}, ]);
