'use strict';

angular.module('linioApp').factory('mileageService', ['$http', function ($http) {
  return {
    updateMileageId: function (mileageId) {
      return $http.post('/api/customer/mileage', {
        mileageId: {
          first: mileageId,
          second: mileageId,
        },
      });
    },

    getMileage: function (total) {
      return $http.post('/api/checkout/mileage', {
        total: total,
      });
    },
  };
}, ]);
