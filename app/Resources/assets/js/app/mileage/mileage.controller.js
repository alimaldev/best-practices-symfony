angular.module('linioApp').controller('MileageController', [
  '$rootScope', '$scope', '$q', '$log', 'mileageService',
  function ($rootScope, $scope, $q, $log, mileageService) {

    var totalField = 'grand_total';
    $scope.mileage = {
      id: null,
      points: 0,
    };
    $scope.activeMileageForm = false;
    $scope.mileageFormErrors = [];

    $scope.updateMileagePoints = function (amount) {
      if (!$scope.mileage.id) {
        $scope.mileage.points = 0;
        return $q.reject();
      }

      return mileageService.getMileage(amount)
        .then(function (response) {
          if (response.hasOwnProperty('data') && response.data.hasOwnProperty('mileage')) {
            $scope.mileage.points = response.data.mileage;
          }
        });
    };

    $scope.updateMileageId = function (form, newProgramId) {
      $scope.mileageFormErrors = [];
      if ((form.$invalid && form.$dirty) || !newProgramId) {
        return;
      }

      $rootScope.overlayActive = true;

      mileageService.updateMileageId(newProgramId)
        .then(function () {
            $scope.showMileageForm = false;
            $scope.mileage.id = newProgramId;
          },

          function (response) {
            if (response.hasOwnProperty('data') && angular.isObject(response.data)) {
              for (var mileageField in response.data) {
                if (response.data.hasOwnProperty(mileageField)) {
                  // jscs:disable maximumLineLength
                  $scope.mileageFormErrors = $scope.mileageFormErrors.concat(response.data[mileageField]);

                  // jscs:enable maximumLineLength
                }
              }
            } else {
              $log.debug(response.data);
            }
          })
        .then(function () {
          return $scope.updateMileagePoints($scope[totalField]);
        })
        .finally(function () {
          $rootScope.overlayActive = false;
        });
    };

    $rootScope.$on('checkout:cart:update', function updatePointsFromEvent(e, amount) {
      $scope.updateMileagePoints(amount);
    });

    $rootScope.$on('checkout:installments:update', function updatePointsFromEvent(e, amount) {
      $scope.updateMileagePoints(amount);
    });

    $scope.updateMileagePoints($scope[totalField]);
  },

]);
