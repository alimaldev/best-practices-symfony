'use strict';

/**
 * Home Page Controller
 */
angular.module('linioApp').controller('SubMenuController', [
  '$scope', '$rootScope',
  function ($scope, $rootScope) {
    $scope.activateMenu = function () {
      $rootScope.overlayEnabled = !$rootScope.overlayEnabled;
    };
  },

]);
