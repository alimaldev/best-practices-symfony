'use strict';

/**
 * Home Page Controller
 */
angular.module('linioApp').controller('menuController', ['$scope', function ($scope) {
  var imagesLoaded = false;

  // jscs:disable maximumLineLength
  var menuAsyncNodes = document.querySelectorAll('.fly-menu-content > .secondary-menu-item [data-aload]');

  // jscs:enable maximumLineLength

  $scope.renderMenuItem = function (id) {
    $scope.menuItemSelected = id;

    if (!imagesLoaded) {
      aload(menuAsyncNodes);
      imagesLoaded = true;
    }
  };

}, ]);
