/* jshint maxlen: 150 */
'use strict';

/**
 * Home Page Controller
 */
angular.module('linioApp')
  .controller('HeaderController', ['$scope', '$location', '$anchorScroll', '$rootScope', 'ngDialog', 'localStorageService',
    function ($scope, $location, $anchorScroll, $rootScope, ngDialog, localStorageService) {

      $scope.activeTabs = [];

      $scope.modalLogin = function () {
        ngDialog.open({
          template: 'loginModal',
          className: 'ngdialog-login ngdialog-login-overlay',
        });
      };

      $scope.scrollTo = function (position) {
        $location.hash(position);
        $anchorScroll();
      };

      $scope.logout = function () {
        localStorageService.remove('cart_count');
      };

      $scope.cartItems = function () {
        return localStorageService.get('cart_count') || 0;
      };

      $scope.activateMenu = function () {
        $rootScope.overlayEnabled = !$rootScope.overlayEnabled;
        $rootScope.suggestActive = false;
      };

      $scope.newsLetterModal = function () {
        ngDialog.open({
          template: 'newsletterTemplate',
          controller: 'NewsletterModalController',
          className: 'ngdialog-theme-default newsletter-dialog',
        });
      };
    },

  ]);

window.onload = function () {
  var firstMenuItemAsyncNodes = document.querySelectorAll('.fly-menu-content-item-1 [data-aload]');
  var remainingAsyncNodes = document.querySelectorAll('div:not(.fly-banner) > a > img[data-aload]');

  aload(firstMenuItemAsyncNodes);
  aload(remainingAsyncNodes);

  echo.init({
    offset: 500,
    throttle: 10,
    debounce: false,
    unload: false,
    callback: function (element, op) {
      element.onerror = function () {
        var ele = element;
        var notFoundClass = 'image-404';
        if (ele.className.indexOf(notFoundClass) == -1) {
          ele.src = 'data:image/png;base64,';
          ele.className = ele.className + ' ' + notFoundClass;
        }
      };
    },
  });

};
