'use strict';

/**
 * Newsletter Homepage Controller
 */
angular.module('linioApp')
    .controller('newsletterHomeController', ['$scope', 'ngDialog', 'NewsletterService',
      function ($scope, ngDialog, NewsletterService) {

        $scope.newsLetterModal = function () {
          ngDialog.open({
            template: 'newsletterTemplate',
            className: 'ngdialog-theme-default newsletter-dialog',
            scope: $scope,
          });
        };

        if (typeof newsletterStatus !== 'undefined' && newsletterStatus.status == 'success') {
          $scope.newsletterStep = 'coupon';
          $scope.newsletter = { coupon: newsletterStatus.coupon };
          $scope.newsLetterModal();
        }

        $scope.isSaving = false;
        $scope.error = '';

        var setFormError = function (error) {
          $scope.isSaving = false;
          $scope.error = error;
          $scope.newsletterStep = 'form';
        };

        $scope.subscribe = function () {
          if ($scope.newsletterSubscription.$valid) {
            ngDialog.closeAll();
            $scope.error = '';
            $scope.isSaving = true;

            NewsletterService.subscribe($scope.newsletter.email)
                .then(
                function (response) {
                  if (response.data.status == 'success') {
                    $scope.newsletter.email = '';
                    $scope.newsletter.coupon = response.data.coupon;
                    $scope.newsletterStep = 'coupon';
                  } else if (response.data.status == 'already_subscribed') {
                    setFormError('Oops!, Ya te habías suscrito antes.');
                  }

                  $scope.newsLetterModal();
                }

            );
          }
        };

      }, ]);
