// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

angular.module('linioApp').factory('jetloreFeedService', [
  '$http', '$q', 'jetloreFeedMapper', 'lazyLoadService',
  function ($http, $q, jetloreFeedMapper, lazyLoadService) {
    return {
      getLayout: function (type) {

        var deferred = $q.defer();
        var rankerConfig = {};
        var layoutConfig = {
          layout: 'homepage',
          response: 'full'
        };
        var userId = '';
        var layout;
        var SDKsrc = 'https://assets.jetlore.com/js/jlranker.js';

        if (typeof dataLayer !== 'undefined') {
          if (dataLayer[0].mail_hash !== 'n/a') {
            userId = dataLayer[0].mail_hash;
          }
        }

        rankerConfig = {
          cid: lsfConfig.jetloreSDKToken,
          id: userId
        };

        lazyLoadService.loadScript(SDKsrc)
          .then(function () {
            JL_RANKER.init(rankerConfig);
            layout = JL_RANKER.get_layout(
              layoutConfig,
              function (layout) {
                deferred.resolve(jetloreFeedMapper.mapLayout(layout.sections));
              }
            );
          });

        return deferred.promise;
      }
    };
  }

]);
