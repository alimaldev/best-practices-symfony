'use strict';

/**
 * Product gallery zoom directive
 */
/* istanbul ignore next */
angular.module('linioApp').directive('zoom', function () {

  function link(scope, element, attrs) {
    var $ = angular.element;
    var rect;
    var original = element;
    var originalImg = original.find('img');
    var zoomed = angular.element(document.querySelector('#zoomed'));
    var zoomedImg = zoomed.find('img');
    var originalContainer = document.querySelector('.original');
    var mainProductImage = document.querySelector('.mainProductImage');
    var startZoomPosition = document.querySelector('#startZoom');
    var endZoomContainer = document.querySelector('#endZoom');
    var widthOriginalImg;
    var heightOriginalImg;
    var markWidth;
    var markHeight;

    // jscs:disable requireDollarBeforejQueryAssignment, maximumLineLength
    var mark = $('<div class="magnifying-glass hidden-sm" style="position: absolute; z-index: 10"></div>');

    // jscs:enable requireDollarBeforejQueryAssignment, maximumLineLength

    var limitWidth;
    var limitHeight;
    var percentWithDisplacement;
    var percentHeightDisplacement;

    original.append(mark);

    element
      .on('mouseenter', function (evt) {
        var offset = calculateOffset(evt, 'enter');;
        scope.imagebig = scope.srcslug + '-zoom.jpg';
        calculateZoomContainer();
        mark.removeClass('hide');
        zoomed.removeClass('hide');
        moveMark(offset.X, offset.Y);
      })
      .on('mouseleave', function () {
        mark.addClass('hide');
        zoomed.addClass('hide');
      })
      .on('mousemove', function (evt) {
        var offset = calculateOffset(evt, 'move');
        moveMark(offset.X, offset.Y);
      });

    scope.$on('mark:moved', function (event, data) {
      updateZoomed.apply(this, data);
    });

    function calculateZoomContainer() {
      var rectOriginalContainer = originalContainer.getBoundingClientRect();
      var marginTopOriginalImg = rectOriginalContainer.top;
      var marginLeftOriginalImg = mainProductImage.getBoundingClientRect().left;
      var marginLeftZoomContainer = startZoomPosition.getBoundingClientRect().left;
      var marginTopZoomContainer = startZoomPosition.getBoundingClientRect().top;
      var marginLeftEndZoom = endZoomContainer.getBoundingClientRect().left;
      var widthZoomContainer = marginLeftEndZoom - marginLeftZoomContainer;
      var widthImgZoom = 850;
      var percentWidth = widthZoomContainer / widthImgZoom;
      var heigthImgZoom = 850;
      var heigthZoomContainer = 442;
      var percentHeigth = heigthZoomContainer / heigthImgZoom;
      var displacementZoomWidth = widthImgZoom - widthZoomContainer;
      var displacementZoomHeight = heigthImgZoom - heigthZoomContainer;

      widthOriginalImg = rectOriginalContainer.right - rectOriginalContainer.left;
      heightOriginalImg = rectOriginalContainer.bottom - rectOriginalContainer.top;
      markWidth = widthOriginalImg * percentWidth;
      markHeight = heightOriginalImg * percentHeigth;
      limitWidth = widthOriginalImg - markWidth;
      limitHeight = heightOriginalImg - markHeight;
      percentWithDisplacement = displacementZoomWidth / limitWidth;
      percentHeightDisplacement = displacementZoomHeight / limitHeight;
      mark
        .css('height', markHeight + 'px')
        .css('width', markWidth + 'px');

      scope.$apply(function () {
        zoomed
          .css('height', '420px')
          .css('width', widthZoomContainer + 'px')
          .css('margin-left', marginLeftZoomContainer - marginLeftOriginalImg + 'px')
          .css('margin-top', marginTopZoomContainer - marginTopOriginalImg + 'px');
      });
    }

    function moveMark(offsetX, offsetY) {
      var x = offsetX - markWidth / 2;
      var y = offsetY - markHeight / 2;

      if (x < 0) {
        x = 0;
      } else if (x > limitWidth) {
        x = limitWidth;
      }

      if (y < 0) {
        y = 0;
      } else if (y > limitHeight) {
        y = limitHeight;
      }

      mark
        .css('left', x + 'px')
        .css('top', y + 'px');

      scope.$broadcast('mark:moved', [
        x, y, markWidth, markHeight, originalImg[0].height, originalImg[0].width,
      ]);
    }

    function updateZoomed(originalX, originalY) {
      scope.$apply(function () {
        zoomedImg
          .attr('src', scope.imagebig)
          .css('left', (percentWithDisplacement * originalX * -1) + 'px')
          .css('top', (percentHeightDisplacement * originalY * -1) + 'px');
      });
    }

    function calculateOffset(mouseEvent, type) {
      var offsetX;
      var offsetY;

      if (type === 'enter') {
        rect = mouseEvent.target.getBoundingClientRect();
      } else {
        rect = rect;
      }

      rect = rect || mouseEvent.target.getBoundingClientRect();
      offsetX = mouseEvent.clientX - rect.left;
      offsetY = mouseEvent.clientY - rect.top;

      return {
        X: offsetX,
        Y: offsetY,
      };

    }

  }

  return {
    restrict: 'EA',
    scope: {
      srcthumb: '@srcthumb',
      srcslug: '@srcslug',
      productname: '@productname',
    },

    /*jshint multistr: true */

    // jscs:disable maximumLineLength
    template: '<img id="" ng-src="{[srcthumb]}" alt="{[productname]}" class="product-slider" draggable="false"/>',

    // jscs:enable maximumLineLength
    link: link,
  };
});
