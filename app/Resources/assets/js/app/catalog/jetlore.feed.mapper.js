// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

angular.module('linioApp').factory('jetloreFeedMapper', function () {
  return {
    mapLayout: function(sections) {
      var mappedSections = [];
      var popularCategoriesSection = {};

      popularCategoriesSection.title = 'Categorías populares';
      popularCategoriesSection.type = 'categories';
      popularCategoriesSection.items = [];

      angular.forEach(sections, function (section) {
        var mappedSection = {};
        var items = [];
        section = section.hasOwnProperty('promos') ? section.promos[0] : section;

        if (section.img) {
          popularCategoriesSection.items.push(mapCategory(section));
        } else {
          mappedSection.title = section.title;
          mappedSection.url = section.url;
          mappedSection.type = 'products';

          if (section.hasOwnProperty('featured_products')) {
            items = section.featured_products;
          } else if (section.hasOwnProperty('products')) {
            items = section.products;
          }

          mappedSection.items = mapProducts(items);
          mappedSections.push(mappedSection);
        }
      });

      mappedSections.push(popularCategoriesSection);

      return mappedSections;
    }
  };

  function mapProducts(products) {
    var mappedProducts = [];

    angular.forEach(products, function (product) {
      var mappedProduct = {};
      var current_price = product.current_price / 100;
      var original_price = product.original_price / 100;

      mappedProduct.title = product.title;
      mappedProduct.image = product.img.replace('http:', 'https:');
      mappedProduct.url = product.url;

      if (original_price != current_price) {
        mappedProduct.percentageOff = 100 - parseInt(current_price * 100 / original_price);
        mappedProduct.originalPrice = original_price;
        mappedProduct.price = current_price;
      } else {
        mappedProduct.price = original_price;
      }

      mappedProducts.push(mappedProduct);
    });

    return mappedProducts;
  }

  function mapCategory(category) {
    var mappedCategory = { images: [] };

    mappedCategory.title = '';
    mappedCategory.url = category.url;
    mappedCategory.images.push(category.img.replace('http:', 'https:'));

    return mappedCategory;
  }
});
