/*jslint maxlen: 120 */
'use strict';

/**
 * CatalogViewController Constructor
 * @param {Scope} $scope
 */
angular.module('linioApp').controller('CatalogViewController', [
  '$scope', 'ngDialog', '$location', '$httpParamSerializer', '$window',
  function ($scope, ngDialog, $location, $httpParamSerializer, $window) {

    $scope.uri = $location.search();
    $scope.activeFilters = [];
    $scope.gridView = $window.localStorage.getItem('gridView');
    $scope.localStoreEnabled = true;

    if (!$scope.gridView) {
      try {
        $window.localStorage.setItem('gridView', 'view-small');
      } catch (exception) {
        $scope.localStoreEnabled = false;
      }

      $scope.gridView = 'view-small';
    }

    var buildUri = function () {
      var uriString = '';

      delete $scope.uri['page'];

      for (var key in $scope.uri) {
        var filterKey = key;

        if ($scope.uri[key] instanceof Array) {
          for (var index in $scope.uri[key]) {
            if (uriString != '') {
              uriString = uriString + '&';
            }

            uriString = uriString + key + '=' + $scope.uri[key][index];
          }
        } else {
          if (uriString != '') {
            uriString = uriString + '&';
          }

          uriString = uriString + key + '=' + $scope.uri[key];
        }
      }

      return uriString;
    };

    var refreshFilters = function () {
      var fullPath = $location.path() + '?' + buildUri();

      $window.location.href = fullPath;
    };

    $scope.isFilterBoxOpen = function (key) {
      return $scope.activeFilters.indexOf(key) > -1;
    };

    $scope.openFilterBox = function (key) {
      if ($scope.isFilterBoxOpen(key)) {
        $scope.activeFilters.splice($scope.activeFilters.indexOf(key), 1);
      } else {
        $scope.activeFilters.push(key);
      }
    };

    $scope.openModal = function (key) {
      ngDialog.open({
        template: 'modal_' + key,
        controller: 'FilterModalController',
      });
    };

    $scope.clearFilters = function () {
      var itemsToKeep = ['q'];

      for (key in $scope.uri) {
        if (itemsToKeep.indexOf(key) > -1) {
          continue;
        }

        delete $scope.uri[key];
      }

      refreshFilters();
    };

    $scope.clearFilter = function (key) {
      if ($scope.uri[key] === undefined) {
        return;
      }

      delete $scope.uri[key];

      refreshFilters();
    };

    $scope.changeMultipleChoiceFilter = function (key, value) {
      if ($scope.uri[key] instanceof Array) {
        var index = $scope.uri[key].indexOf(value);

        if (index === -1) {
          $scope.uri[key].push(value);
        } else {
          $scope.uri[key].splice(index, 1);
        }
      } else if (typeof $scope.uri[key] == 'string') {
        if ($scope.uri[key] == value) {
          delete $scope.uri[key];
        } else {
          $scope.uri[key] = [$scope.uri[key], value];
        }
      } else {
        $scope.uri[key] = [value];
      }

      refreshFilters();
    };

    $scope.changeCheckboxFilter = function (key, value) {
      var currentFilterValue = value ? value : 0;

      if (currentFilterValue == 1) {
        delete $scope.uri[key];
      } else {
        $scope.uri[key] = 1;
      }

      refreshFilters();
    };

    $scope.changeRangeFilter = function (key) {
      if ($scope.ranges[key] === undefined) {
        return;
      }

      var activeMinimum = $scope.ranges[key].minimum;
      var activeMaximum = $scope.ranges[key].maximum;

      if ($scope.ranges[key].activeMinimum > $scope.ranges[key].minimum) {
        activeMinimum = $scope.ranges[key].activeMinimum;
      }

      if ($scope.ranges[key].activeMaximum > $scope.ranges[key].maximum) {
        activeMaximum = $scope.ranges[key].activeMaximum;
      }

      var value = (activeMinimum || 0) + '-' + (activeMaximum || 0);

      if (value == '0-0') {
        return;
      }

      $scope.uri[key] = value;

      refreshFilters();
    };

    $scope.changeGrid = function (viewType) {
      if ($scope.localStoreEnabled) {
        $window.localStorage.setItem('gridView', viewType);
      }

      $scope.gridView = viewType;
    };
  },

]);
