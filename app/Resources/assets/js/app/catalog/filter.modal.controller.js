'use strict';

/**
 * Filter Modal Controller
 */

angular.module('linioApp').controller('FilterModalController', function ($scope, ngDialog) {
  $scope.applyFilters = function () {
    ngDialog.close();
  };
});
