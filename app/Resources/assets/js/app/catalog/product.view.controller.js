// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

/**
 * ProductViewController Constructor
 * @param {Scope} $scope
 */

angular.module('linioApp').controller('ProductViewController', [
  '$sce', '$scope', '$timeout', '$window', 'feedService',
  '$location', '$anchorScroll', '$rootScope',
  function (
    $sce, $scope, $timeout, $window, feedService, $location,
    $anchorScroll, $rootScope
  ) {
    var selectedThumbIndex = 0;
    var selectionByThumb = false;
    $scope.productJSONString = {};
    $scope.product = '';
    $scope.currentDate = Math.floor(Date.now() / 1000);
    $scope.productImageIndex = 0;

    var initialLoad = $scope.$watch(
      function () {
        return $window.product.data;
      },

      function (data) {
        $scope.product = Object.keys(data).length ? data : null;
        $scope.baseSimple = (
          $scope.activeSimpleExists()
        ) ? $scope.activeSimple : $scope.product.firstSimple;

        if ($scope.numberOfActiveSimples($scope.product) == 1) {
          $scope.setActiveSimple($scope.product.firstSimple);
        }

        if ($scope.product.images) {
          $scope.initGallery();
        }

        if ($scope.product.firstSimple) {
          feedService.getSimilarProducts($scope.product.firstSimple.sku)
            .then(function (response) {
              $scope.similar_products = response;
              $scope.gettingSimilarProducts = false;
            });
        }

        initialLoad();

      });

    $scope.$watch('activeSimple', function () {
      $scope.baseSimple = (
        $scope.activeSimpleExists()) ? $scope.activeSimple : $scope.product.firstSimple;
    }, true);

    $scope.setActiveSimple = function (simple) {
      if (simple.stock < 1) {
        return;
      }

      $scope.activeSimple = simple;
    };

    $scope.mouseOverThumb = function (thumbImage, sourceSlide) {
      var id = thumbImage.id;

      if (sourceSlide === 'slide') {
        id = id - 1;
      }

      $scope.productImageIndex = id;
      selectedThumbIndex = id;
      selectionByThumb = true;
    };

    $scope.initGallery = function () {
      var galleryImage = $scope.product.images[1];

      angular.forEach($scope.product.images, function (item) {
        /* jshint unused:false */
        if (item.main) {
          galleryImage = item;
        }
      });

      $scope.mouseOverThumb(galleryImage, 'firstimage');
      $scope.slidesProduct = [];
      addSlides($scope.slidesProduct, $scope.product.images);
      $scope.slidesThumbs = [];
      addSlides($scope.slidesThumbs, $scope.product.images);
      rebuildSlideThumbs(4);

      $scope.slidesSeeAlso = [];
      addSlides($scope.slidesSeeAlso, $scope.seimages);
      rebuildSlideSeeAlso(5);
    };

    $scope.fbCommentsVisible = false;

    $scope.productQuestions = function () {
      $scope.fbCommentsVisible = !$scope.fbCommentsVisible;
    };

    $scope.specialPriceActive = function (currentDate, startDate, endDate) {
      return currentDate >= startDate && currentDate <= endDate;
    };

    $scope.activeSimpleExists = function () {
      return typeof $scope.activeSimple.sku !== 'undefined' && $scope.activeSimple.stock >= 1;
    };

    $scope.numberOfSimples = function (product) {
      return Object.keys(product).length;
    };

    $scope.numberOfActiveSimples = function (product) {
      var total = 0;

      angular.forEach(product.simples, function (simple) {
        if (simple.active) {
          total++;
        }
      });

      return total;
    };

    /* Mock of recommendation sections */
    $scope.recommendations = [{
      slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
      name: 'Samsung Galaxy S5 Telcel',
      price: '$7,495',
      originalprice: '$140,000',
      discount: '-50%',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
      name: 'APPLE iPhone 5S 64GB Telcel ',
      price: '$5,879',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
      name: 'Sony Xperia Z1 Desbloqueado Negro',
      price: '$3,290',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-1246-8856141-1',
      name: 'HTC ONE Mini Desbloqueado Gris',
      price: '$7,495',
      originalprice: '$40,020',
      discount: '-10%',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6348-6272821-1',
      name: 'Samsung',
      price: '$4,990',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
      name: 'Samsung Galaxy S5 Telcel',
      price: '$7,495',
      originalprice: '$140,000',
      discount: '-50%',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
      name: 'Sony Xperia Z1 Desbloqueado Negro',
      price: '$3,290',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-1246-8856141-1',
      name: 'HTC ONE Mini Desbloqueado Gris',
      price: '$7,495',
      originalprice: '$40,020',
      discount: '-10%',
    }, {
      slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
      name: 'Samsung Galaxy S5 Telcel',
      price: '$7,495',
      originalprice: '$140,000',
      discount: '-50%',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
      name: 'APPLE iPhone 5S 64GB Telcel ',
      price: '$5,879',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
      name: 'Sony Xperia Z1 Desbloqueado Negro',
      price: '$3,290',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
      name: 'Samsung Galaxy S5 Telcel',
      price: '$7,495',
      originalprice: '$140,000',
      discount: '-50%',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
      name: 'Sony Xperia Z1 Desbloqueado Negro',
      price: '$3,290',
      originalprice: '',
      discount: '0',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-1246-8856141-1',
      name: 'HTC ONE Mini Desbloqueado Gris',
      price: '$7,495',
      originalprice: '$40,020',
      discount: '-10%',
    }, {
      slug: 'http://media.linio.com.mx/p/apple-6348-6272821-1',
      name: 'Samsung',
      price: '$4,990',
      originalprice: '',
      discount: '0',
    }, ];

    function getSlide(target, item) {
      var i = target.length;

      return {
        id: (i + 1),
        label: 'slide #' + (i + 1),
        img: item.slug + '-product.jpg',
        slug: item.slug,
        name: item.name,
        price: item.price,
        originalprice: item.originalprice,
        discount: item.discount,
      };
    }

    function addSlide(target, item) {
      target.push(getSlide(target, item));
    }

    function addSlides(target, srcimages) {
      angular.forEach(srcimages, function (item) {
        addSlide(target, item);
      });
    }

    function rebuildSlideThumbs(n) {
      var imageCollectionThumbs = [];
      var slide = [];
      var index;

      for (index = 0; index < $scope.slidesThumbs.length; index++) {
        if (slide.length === n) {
          imageCollectionThumbs.push(slide);
          slide = [];
        }

        slide.push($scope.slidesThumbs[index]);
      }

      imageCollectionThumbs.push(slide);

      $scope.imageCollectionThumbs = imageCollectionThumbs;
    }

    function rebuildSlideSeeAlso(n) {
      var imageCollectionSeeAlso = [];
      var slide = [];
      var index;

      for (index = 0; index < $scope.slidesSeeAlso.length; index++) {
        if (slide.length === n) {
          imageCollectionSeeAlso.push(slide);
          slide = [];
        }

        slide.push($scope.slidesSeeAlso[index]);
      }

      imageCollectionSeeAlso.push(slide);
      $scope.imageCollectionSeeAlso = imageCollectionSeeAlso;
    }

    $scope.$watch('productImageIndex', function (index) {
      var diffIndexSelected;

      if (index) {
        $scope.thumbIndex = Math.floor(index / 4);
      }

      if (selectionByThumb) {
        diffIndexSelected = Math.abs(selectedThumbIndex - index);
        if (diffIndexSelected >= 1) {
          $timeout(function () {
            $scope.productImageIndex = selectedThumbIndex;
            selectionByThumb = false;
          }, 0);
        }
      }
    });

    $scope.disableThumbSelection = function () {
      selectionByThumb = false;
    };

    $scope.goToAnchor = function (target) {
      $anchorScroll.yOffset = $rootScope.initSticky ? 30 : 140;
      var oldHash = $location.hash();
      $location.hash(target);
      $anchorScroll();
      $location.hash(oldHash);
    };

    $window.addEventListener('load', function () {
      var target = $location.hash();

      if (target == 'reviewForm') {
        $scope.goToAnchor(target);
      }
    });
  },

]);
