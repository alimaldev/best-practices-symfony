// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

angular.module('linioApp').factory('lazyLoadService', [
  '$q', '$window',
  function ($q, $window) {
    return {
      loadScript: function (src) {
        var deferred = $q.defer();

        $window.scriptLoaded = function () {
          deferred.resolve();
        }

        function loadScript() {
          var script = document.createElement('script');
          script.src = src;
          script.onload = scriptLoaded;
          document.body.appendChild(script);
        }

        loadScript();

        return deferred.promise;
      }
    }
  }
]);
