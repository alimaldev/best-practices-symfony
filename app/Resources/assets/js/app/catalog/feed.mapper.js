// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

/**
 * feedMapper
 */
angular.module('linioApp').factory('feedMapper', function () {
  return {
    mapProducts: function (products) {
      var mappedProducts = [];

      angular.forEach(products, function (product) {
        var mappedProduct = {};

        mappedProduct.title = product.title;
        mappedProduct.brand = product.brand.name;
        mappedProduct.image = product.images[0];
        mappedProduct.url = '//' + product.url;

        var price = product.price;

        if (product.price.previous) {
          // TODO: Ask for product.price.discount.

          mappedProduct.percentageOff = 100 - parseInt(price.current * 100 / price.previous);
          mappedProduct.originalPrice = price.previous;
          mappedProduct.price = price.current;
        } else {
          mappedProduct.price = price.current;
        }

        mappedProducts.push(mappedProduct);
      });

      return mappedProducts;
    },
  };
});
