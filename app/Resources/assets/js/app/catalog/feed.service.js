'use strict';

/**
 * feedService
 */
angular.module('linioApp').factory('feedService', [
  '$http', 'feedMapper',
  function ($http, feedMapper) {
    return {
      getSimilarProducts: function (sku) {
        // jscs:disable maximumLineLength
        var url = lsfConfig.feedUrl + lsfConfig.similarProductsPath + '?key=' + lsfConfig.feedKey + '&sku=' + sku;

        // jscs:enable maximumLineLength

        var blacklist = '';

        if (lsfConfig.catalogFeedBlacklistedCategories) {
          lsfConfig.catalogFeedBlacklistedCategories.forEach(function (blacklistedItem) {
            blacklist += '&bl=' + blacklistedItem;
          });

          url += blacklist;
        }

        return $http.get(url, {
            withCredentials: true,
          })
          .then(
            function (response) {
              return feedMapper.mapProducts(response.data.items);
            },

            function () {
              return [];
            }

          );
      },
    };
  },

]);
