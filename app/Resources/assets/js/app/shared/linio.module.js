'use strict';

angular.module('linioApp', [
  'ui.router', 'ngAnimate', 'angular-carousel', 'ngDialog',
  'ngCookies', 'ngMask', 'facebook', 'googleplus', 'ngOrderObjectBy',
  'ngClipboard', 'linio.checkout',
]);
