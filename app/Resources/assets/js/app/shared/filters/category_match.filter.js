'use strict';

angular.module('linioApp')
  .filter('categoryMatch', function ($sce) {
    return function (value, queryToMatch) {
      var searchPattern = new RegExp('(' + queryToMatch + ')', 'ig');

      // jscs:disable maximumLineLength
      return $sce.trustAsHtml(value.replace(searchPattern, '<span class="suggester-match">$1</span>'));

      // jscs:disable maximumLineLength
    };
  });
