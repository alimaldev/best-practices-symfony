'use strict';

angular.module('linioApp')
  .filter('percent', ['$filter', function ($filter) {
    return function (value) {
      return $filter('number')(Math.round(value)) + '%';
    };
  }, ]);
