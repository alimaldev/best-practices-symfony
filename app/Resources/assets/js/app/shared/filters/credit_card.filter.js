'use strict';

angular.module('linioApp')
  .filter('getMethodFromCard', function () {
    return function (cardNumber) {
      if (/^(34)|^(37)/.test(cardNumber)) {
        return 'amex';
      }

      return 'banorte_payworks';
    };
  });
