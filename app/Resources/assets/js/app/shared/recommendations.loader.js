'use strict';

if (lsfConfig.jetloreFeedEnabled) {
  var loadRecommendations = function (type) {
    var $rootScope = angular.element(document.body).injector().get('$rootScope');
    $rootScope.$broadcast('load-recommendations', type);
  };
}
