angular.module('linioApp').directive('bannerSlide', function ($window) {
  return {
    restrict: 'E',
    scope: {
      banners: '=',
    },

    // jscs:disable maximumLineLength
    template: '<div class="is-center"> <div class="placeholders-wrapper" ng-swipe-left="swiping=true;" ng-swipe-right="swiping=true;"> <ul rn-carousel rn-carousel-controls rn-carousel-index="recommendationIndex" class="rn-carousel image"> <li class="banner-item" ng-repeat="images in itemsSlides"> <a href="{[image.url]}" ng-repeat="image in images"> <span ng-if="image != null"> <img class="banner-img" src="{[image.img]}"> </span> </a> </li> </ul> </div> </div>',

    // jscs:enable maximumLineLength

    link: function (scope, element, attrs) {
      var itemsCollection = [];
      var itemsSlides = [];
      var currentSize = 0;

      function getItem(target, item) {
        if (item.contentType == 'dfa') {
          return {

            //jscs:disable requireCamelCaseOrUpperCaseIdentifiers
            img: item.image_url,
            url: item.target_url,

            //jscs:enable requireCamelCaseOrUpperCaseIdentifiers
          };
        } else if (item.contentType == 'banner') {
          return {
            img: item.image.src,
            url: item.href,
          };
        } else if (item.contentType == 'simple_image') {
          return {
            img: item.src,
          };
        } else {
          return null;
        }
      }

      function addItems(target, srcimages) {
        angular.forEach(srcimages, function (item) {

          //jscs:disable requireCamelCaseOrUpperCaseIdentifiers
          type = item.content_type;

          //jscs:enable requireCamelCaseOrUpperCaseIdentifiers
          item.data.contentType = type;
          target.push(getItem(target, item.data));
        });
      }

      function rebuildSlideSeeAlso(n) {
        var slide = [];
        var index;
        itemsSlides.length = 0;
        for (index = 0; index < itemsCollection.length; index++) {
          if (slide.length === n) {
            itemsSlides.push(slide);
            slide = [];
          }

          slide.push(itemsCollection[index]);
        }

        itemsSlides.push(slide);
        return true;
      }

      function changeSize(size) {
        if (size != currentSize) {
          if (rebuildSlideSeeAlso(size)) {
            scope.recommendationIndex = 0;
          }
        }

        currentSize = size;
      }

      scope.$watch(function ($scope) {
          return $window.innerWidth;
        },

        function (value) {
          var imageWidth = 134;
          var rest = 138;
          var fullscreen = 1180;
          var sevenImages = fullscreen - 188;
          var sixImages = sevenImages - rest;
          var fiveImages = sixImages - rest;

          if (value > fullscreen) {
            changeSize(8);
          } else if ((value <= fullscreen) && (value > sevenImages)) {
            changeSize(7);
          } else if ((value <= sevenImages) && (value > sixImages)) {
            changeSize(6);
          } else if ((value <= sixImages) && (value > fiveImages)) {
            changeSize(5);
          } else {
            changeSize(4);
          }
        });

      itemsCollection = [];
      itemsSlides = [];
      addItems(itemsCollection, scope.banners);
      scope.itemsSlides = itemsSlides;

      scope.clickRecommendation = function (path) {
        if (!scope.swiping) {
          $window.location = path;
        }

        scope.swiping = false;
      };
    },
  };
});
