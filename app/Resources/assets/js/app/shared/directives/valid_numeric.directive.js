'use strict';

/**
 * Limits model input to numeric only characters,
 * with a max length defined by the value of validNumeric attribute (if defined).
 */
angular.module('linioApp').directive('validNumeric', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {

      modelCtrl.$parsers.push(function (inputValue) {

        var maxLength;
        var transformedInput;

        if (!isNaN(attrs.validNumeric)) {
          maxLength = parseInt(attrs.validNumeric);
        } else {
          maxLength = 0;
        }

        if (maxLength) {
          transformedInput = inputValue
                              .substring(0, maxLength)
                              .replace(/\D/g, '')
                              .replace(/ /g, '');
        } else {
          transformedInput = inputValue
                              .replace(/\D/g, '')
                              .replace(/ /g, '');
        }

        if (transformedInput !== inputValue) {
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }

        return transformedInput;
      });
    },
  };
});
