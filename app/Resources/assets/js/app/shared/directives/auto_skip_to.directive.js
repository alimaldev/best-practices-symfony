/*jslint maxlen: 110 */
'use strict';

/**
 * Sets focus on input determined by auto-skip-to attribute when model is valid.
 */
angular.module('linioApp').directive('autoSkipTo', [
  '$timeout', '$document',
  function ($timeout, $document) {
    return {
      restrict: 'A',
      require: ['ngModel'],
      link: function (scope, element, attrs, ctrls) {
        var model = ctrls[0];
        var nextElementName;
        var nextElement;

        if (typeof attrs.autoSkipTo !== 'undefined') {
          nextElementName = attrs.autoSkipTo;
        } else {
          nextElementName = null;
        }

        if (nextElementName) {
          nextElement = $document[0].getElementsByName(nextElementName)[0];
        } else {
          nextElement = null;
        }

        scope.next = function () {
          return model.$valid;
        };

        scope.$watch(scope.next, function (newValue, oldValue) {
          /* jshint unused:false */
          if (newValue && model.$dirty && nextElement) {
            $timeout(function () {
              nextElement.focus();
            }, 0, false);
          }
        });
      },
    };
  },

]);
