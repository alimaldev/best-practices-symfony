'use strict';

/**
 * Limits model input to non-numeric only characters,
 * with a max length defined by the value of validNonNumeric attribute (if defined).
 */
angular.module('linioApp').directive('validNonNumeric', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {

      modelCtrl.$parsers.push(function (inputValue) {

        var maxLength;
        var transformedInput;

        if (!isNaN(attrs.validNonNumeric)) {
          maxLength = parseInt(attrs.validNonNumeric);
        } else {
          maxLength = 0;
        }

        if (maxLength) {
          transformedInput = inputValue
                              .substring(0, maxLength)
                              .replace(/[0-9\.]+/g, '');
        } else {
          transformedInput = inputValue
                              .replace(/[0-9\.]+/g, '');
        }

        if (transformedInput !== inputValue) {
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }

        return transformedInput;
      });
    },
  };
});
