/*jslint maxlen: 110 */
'use strict';

/**
 * If present, require that the recaptcha field be successful before submitting the form
 */
angular.module('linioApp').directive('requireRecaptcha', ['$window', function ($window) {

  var errorMsg = '<div class="alert alert-error">' +
      'El valor del captcha no es válido.' +
      '</div>';

  var hasError = false;

  return {
    link: function modifyElement(scope, element) {
      element.on('submit', function (event) {
        if (hasError) {
          var fields = element.children();

          for (var f in fields) {
            if (fields.hasOwnProperty(f)) {
              var field = angular.element(fields[f]);
              if (field.hasClass('alert-error')) {
                field.remove();
                break;
              }
            }
          }

          hasError = false;
        }

        if ($window.hasOwnProperty('grecaptcha')) {
          var captcha = $window.grecaptcha.getResponse();
          if (captcha.length == 0) {
            element.prepend(errorMsg);
            $window.grecaptcha.reset();
            hasError = true;
            event.preventDefault();
            return false;
          }
        }

        return false;
      });
    },
  };
}, ]);
