'use strict';

/**
 * Sets the ng-focused or ng-blurred class on an input based on its focus state.
 */
angular.module('linioApp').directive('ngFocus', function () {
  var focusClass = 'ng-focused';
  var blurClass = 'ng-blurred';

  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ctrl) {
      element.removeClass(focusClass);
      element.addClass(blurClass);
      ctrl.$focused = false;
      element.bind('focus', function () {
        element.addClass(focusClass);
        element.removeClass(blurClass);
        scope.$apply(function () {ctrl.$focused = true;});
      }).bind('blur', function () {
        element.removeClass(focusClass);
        element.addClass(blurClass);
        scope.$apply(function () {ctrl.$focused = false;});
      });
    },
  };
});
