'use strict';

/* TODO: Remove this controller used as a quick fix. */
angular.module('linioApp').controller('recommendationsController', function(){});

angular.module('linioApp').directive('carousel', function ($window) {

  return {
    restrict: 'E',
    scope: {
      collection: '=',
    },
    templateUrl: 'carousel',
    link: function (scope, element, attrs) {
      var itemsCollection = [];
      var itemsSlides = [];
      var currentSize = 0;

      function getItem(target, item) {
        var i = target.length;
        return {
          id: (i + 1),
          label: 'slide #' + (i + 1),
          img: item.image,
          slug: item.image,
          name: item.title,
          url: item.url,
          price: item.price,
          originalprice: item.originalPrice,
          discount: item.percentageOff,
        };
      }

      function addItems(target, srcimages) {
        angular.forEach(srcimages, function (item) {
          target.push(getItem(target, item));
        });
      }

      function rebuildSlideSeeAlso(n) {
        var slide = [];
        var index;
        itemsSlides.length = 0;
        for (index = 0; index < itemsCollection.length; index++) {
          if (slide.length === n) {
            itemsSlides.push(slide);
            slide = [];
          }

          slide.push(itemsCollection[index]);
        }

        itemsSlides.push(slide);

        return true;
      }

      function changeSize(size) {
        if (size != currentSize) {
          if (rebuildSlideSeeAlso(size)) {
            scope.recommendationIndex = 0;
          }
        }

        currentSize = size;
      }

      scope.$watch(function ($scope) {
        return $window.innerWidth;
      }, function (value) {

        var imageWidth = 220;
        var initBreakpoint = 704;
        var threeImagesWidth = initBreakpoint;
        var fourImagesWidth = initBreakpoint + imageWidth;
        var fiveImagesWidth = initBreakpoint + imageWidth * 2;

        if (value > fiveImagesWidth) {
          changeSize(5);
        } else if (value > fourImagesWidth) {
          changeSize(4);
        } else if (value > threeImagesWidth) {
          changeSize(3);
        } else {
          changeSize(2);
        }
      });

      itemsCollection = [];
      itemsSlides = [];
      addItems(itemsCollection, scope.collection);
      scope.itemsSlides = itemsSlides;

      scope.clickRecommendation = function (path) {
        if (!scope.swiping) {
          $window.location = path;
        }

        scope.swiping = false;
      };

    },
  };
});
