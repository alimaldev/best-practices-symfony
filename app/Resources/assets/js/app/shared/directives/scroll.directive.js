'use strict';

/**
 * Detect if windows is scrolled.
 */
angular.module('linioApp').directive('scroll', function ($window) {
  return function (scope, element, attrs) {
    angular.element($window).bind('scroll', function () {
      if (this.pageYOffset >= 30) {
        scope.initSticky = true;
      } else {
        scope.initSticky = false;
      };

      scope.$apply();
    });
  };
});
