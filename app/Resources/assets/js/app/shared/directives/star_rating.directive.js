// jslint maxlen: 110
// jscs:disable

'use strict';

/**
 * Functionality for star based rating input.
 */
angular.module('linioApp').directive('starRating', [function() {

  var ratingTemplate =
    '<span class="star-rating massive reviews-stars-input">\
      <i aria-hidden="true" data-icon="š" class="star" ng-click="setRating($event)" ng-mouseover="setActiveStars($event)" ng-mouseleave="clearTempActive()"></i>\
      <i aria-hidden="true" data-icon="š" class="star" ng-click="setRating($event)" ng-mouseover="setActiveStars($event)" ng-mouseleave="clearTempActive()"></i>\
      <i aria-hidden="true" data-icon="š" class="star" ng-click="setRating($event)" ng-mouseover="setActiveStars($event)" ng-mouseleave="clearTempActive()"></i>\
      <i aria-hidden="true" data-icon="š" class="star" ng-click="setRating($event)" ng-mouseover="setActiveStars($event)" ng-mouseleave="clearTempActive()"></i>\
      <i aria-hidden="true" data-icon="š" class="star" ng-click="setRating($event)" ng-mouseover="setActiveStars($event)" ng-mouseleave="clearTempActive()"></i>\
    </span>';

  /**
   * Sets stars to active recursively
   *
   * @param star
   */
  function setActiveRecursive(star) {
    if (star.previousSibling) {
      setActiveRecursive(star.previousSibling);
    }

    var thisStar = angular.element(star);

    if (!thisStar.hasClass('active')) {
      thisStar.addClass('active');
    }
  }

  /**
   * Generates a list of stars already active
   *
   * @param stars
   * @returns {{}}
   */
  function getActiveStars(stars) {
    var activeStars = {};
    for (var s in stars) {
      if (stars.hasOwnProperty(s) && s != 'length') {
        activeStars[s] = angular.element(stars[s]).hasClass('active');
      }
    }

    return activeStars;
  }

  /**
   * Get count of active stars
   *
   * @param activeStars
   * @returns {number}
   */
  function getActiveStarCount(activeStars) {
    var count = 0;

    for (var a in activeStars) {
      if (activeStars.hasOwnProperty(a) && activeStars[a]) {
        count += 1;
      }
    }

    return count;
  }

  /**
   * Set all active to false
   *
   * @param stars
   * @param activeStars
   * @returns {*}
   */
  function clearStars(stars, activeStars) {
    for (var s in activeStars) {
      if (activeStars.hasOwnProperty(s)) {
        activeStars[s] = false;
      }

      if (stars.hasOwnProperty(s)) {
        angular.element(stars[s]).removeClass('active');
      }
    }

    return activeStars;
  }

  /**
   * Remove active class from temporarily active stars
   *
   * @param stars
   * @param activeStars
   */
  function clearNonActiveStars(stars, activeStars) {
    for (var s in stars) {
      if (stars.hasOwnProperty(s) && s != 'length' && !activeStars[s]) {
        var star = angular.element(stars[s]);
        star.removeClass('active');
      }
    }

  }

  /**
   * Set the scope methods & elment properties
   *
   * @param scope
   * @param formInput
   */
  function linker(scope, formInput, attrs, ngModel) {

    var stars = formInput.next().children();
    var activeStars = getActiveStars(stars);

    scope.setRating = function setRating(event) {
      if (event.target) {
        clearStars(stars, activeStars);
        setActiveRecursive(event.target);
        activeStars = getActiveStars(stars);
        formInput.val(getActiveStarCount(activeStars));

        if (ngModel) {
          ngModel.$setViewValue(getActiveStarCount(activeStars));
          ngModel.$render();
        }
      }
    };

    scope.setActiveStars = function setActiveStars(event) {
      clearNonActiveStars(stars, activeStars);
      if (event.target) {
        setActiveRecursive(event.target);
      }
    };

    scope.clearTempActive = function clearTempActiveStars() {
      clearNonActiveStars(stars, activeStars);
    };
  }

  /**
   * Compile the template to include the star rating & original input element
   *
   * @param element
   * @returns {linker}
   */
  function generateStarRating(element) {
    element.after(ratingTemplate);
    return linker;
  }

  return {
    require: '?ngModel',
    restrict: 'A',
    compile: generateStarRating
  };
}]);
