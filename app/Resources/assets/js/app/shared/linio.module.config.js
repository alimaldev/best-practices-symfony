/*jslint maxlen: 150 */
'use strict';

function configLinioApp($locationProvider, $interpolateProvider, ngDialogProvider, FacebookProvider, GooglePlusProvider, ngClipProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false,
    rewriteLinks: false,
  });

  $interpolateProvider.startSymbol('{[').endSymbol(']}');

  ngDialogProvider.setDefaults({
    className: 'ngdialog-container',
    plain: false,
    showClose: true,
    closeByDocument: true,
    closeByEscape: true,
    appendTo: false,
  });

  FacebookProvider.init(lsfConfig.fbId);
  GooglePlusProvider.init({
    clientId: lsfConfig.gplusId,
    apiKey: lsfConfig.gplusApiKey,
  });

  ngClipProvider.setPath('//cdnjs.cloudflare.com/ajax/libs/zeroclipboard/2.1.6/ZeroClipboard.swf');

}

/**
 * linioApp module config function
 */
angular.module('linioApp')
    .config(['$locationProvider', '$interpolateProvider', 'ngDialogProvider', 'FacebookProvider', 'GooglePlusProvider', 'ngClipProvider', configLinioApp]);
