'use strict';

function ApiErrorLogger($window, $q, $log) {
  return {
    responseError: function (response) {

      var logRecord = '';

      logRecord += response.status + ' ' + response.config.method + ' ' + response.config.url;
      if (response.config.paramSerializer()) {
        logRecord += '?' + response.config.paramSerializer();
      }

      logRecord += '\n';

      // @TODO: scrub sensitive data like passwords.
      // @TODO: scrub credit card numbers that may exist in the request
      switch (true) {
        case response.data.hasOwnProperty('error'):
          logRecord += JSON.stringify(response.data.error) + '\n';
          break;
        case response.data.hasOwnProperty('errors'):
          logRecord += JSON.stringify(response.data.errors) + '\n';
          break;
        default:
          logRecord += JSON.stringify(response.data) + '\n';
          break;
      }

      if (response.config.hasOwnProperty('data')) {
        // jscs:disable maximumLineLength
        logRecord += response.config.method + ' parameters: \n' + JSON.stringify(response.config.data);

        // jscs:enable maximumLineLength
      }

      if (
        $window.hasOwnProperty('config') &&
        $window.lsfConfig.hasOwnProperty('debug') &&
        $window.lsfConfig.debug
      ) {
        $log.debug(logRecord);
      }

      return $q.reject(response);
    },
  };
}

angular.module('linioApp').factory('LinioApiErrorInterceptor', [
  '$window', '$q', '$log', ApiErrorLogger,
]);
