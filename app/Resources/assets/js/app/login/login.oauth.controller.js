'use strict';

/**
 * LoginOAuthController
 */
angular.module('linioApp').controller('LoginOAuthController', [
  '$scope', 'Facebook', 'GooglePlus',
  function ($scope, Facebook, GooglePlus) {
    $scope.fbRedirect = '';
    $scope.gplusRedirect = '';
    $scope.isFacebookReady = false;

    $scope.fbLogin = function () {
      Facebook.getLoginStatus(function (response) {
        if (response.status === 'connected') {
          document.location = $scope.fbRedirect;
        } else {
          Facebook.login(function (response) {
            if (response.authResponse) {
              document.location = $scope.fbRedirect;
            }
          });
        }
      });
    };

    $scope.gplusLogin = function () {
      GooglePlus.login().then(function (response) {
        document.location = $scope.gplusRedirect;
      });
    };

    $scope.$watch(
      function () {
        return Facebook.isReady();
      },

      function (isReady) {
        $scope.isFacebookReady = isReady;
      }

    );
  },

]);
