/* jshint maxlen: 150 */
'use strict';

angular.module('linioApp').controller('successController', ['$scope', function ($scope) {
  $scope.recommendationGeneral = [{
    slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
    name: '1Samsung Galaxy S5 Telcel',
    price: '$7,495',
    originalprice: '$140,000',
    discount: '-50%',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
    name: '2APPLE iPhone 5S 64GB Telcel ',
    price: '$5,879',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
    name: '3Sony Xperia Z1 Desbloqueado Negro',
    price: '$3,290',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-1246-8856141-1',
    name: '4HTC ONE Mini Desbloqueado Gris',
    price: '$7,495',
    originalprice: '$40,020',
    discount: '-10%',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6348-6272821-1',
    name: '5Samsung',
    price: '$4,990',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
    name: '6Samsung Galaxy S5 Telcel',
    price: '$7,495',
    originalprice: '$140,000',
    discount: '-50%',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
    name: '7Sony Xperia Z1 Desbloqueado Negro',
    price: '$3,290',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-1246-8856141-1',
    name: '8HTC ONE Mini Desbloqueado Gris',
    price: '$7,495',
    originalprice: '$40,020',
    discount: '-10%',
  }, {
    slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
    name: '9Samsung Galaxy S5 Telcel',
    price: '$7,495',
    originalprice: '$140,000',
    discount: '-50%',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
    name: '10APPLE iPhone 5S 64GB Telcel ',
    price: '$5,879',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
    name: '11Sony Xperia Z1 Desbloqueado Negro',
    price: '$3,290',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/invicta-2162-020094-1',
    name: '12Samsung Galaxy S5 Telcel',
    price: '$7,495',
    originalprice: '$140,000',
    discount: '-50%',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6274-1013551-1',
    name: '13Sony Xperia Z1 Desbloqueado Negro',
    price: '$3,290',
    originalprice: '',
    discount: '0',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-1246-8856141-1',
    name: '14HTC ONE Mini Desbloqueado Gris',
    price: '$7,495',
    originalprice: '$40,020',
    discount: '-10%',
  }, {
    slug: 'http://media.linio.com.mx/p/apple-6348-6272821-1',
    name: '15Samsung',
    price: '$4,990',
    originalprice: '',
    discount: '0',
  }, ];
}, ]);
