'use strict';

angular.module('linioApp').controller('ReviewController', [
  '$scope', 'reviewService',
  function ($scope, reviewService) {
    $scope.setSellerReview = function (sellerSlug, review) {
      $scope.submittingForm = true;
      reviewService.setSellerReview(sellerSlug, review)
        .then(
          function (response) {
            $scope.reviewSubmitted = true;
            $scope.submittingForm = false;
          },

          function () {
            $scope.submittingForm = false;
          }

        );
    };
  },

]);
