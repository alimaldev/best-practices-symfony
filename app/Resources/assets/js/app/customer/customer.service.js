'use strict';

(function (angular) {

  function customerService($http) {
    return {
      setTaxID: function (taxId, taxIdField) {
        var params = {};
        params[taxIdField] = taxId;
        return $http.post('/api/customer/tax-id', params);
      },
    };

  }

  angular.module('linioApp').factory('customerService', ['$http', customerService]);

})(angular);
