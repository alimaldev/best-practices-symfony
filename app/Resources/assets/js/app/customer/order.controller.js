'use strict';

angular.module('linioApp').controller('orderController', [
  '$rootScope', '$scope', '$compile', '$http', 'CustomerAddressService', 'ngDialog', '$filter', 'invoiceService',
  function ($rootScope, $scope, $compile, $http, customerAddressService, ngDialog, $filter, invoiceService
  ) {
    $scope.invoiceStatus = 'initial';

    $scope.generateInvoice = function () {
      $scope.invoiceStatus = 'addressFormView';

      ngDialog.open({
        scope: $scope,
        template: 'invoiceModal',
      });
    };

    $scope.activateChooseAddressTab = function () {
      $scope.chooseAddressTabVisible = true;
      $scope.addAddressTabVisible = false;
    };

    $scope.activateAddAddressTab = function () {
      $scope.chooseAddressTabVisible = false;
      $scope.addAddressTabVisible = true;
    };

    $rootScope.$on('ngDialog.closed', function (e, $dialog) {
      $scope.invoiceStatus = 'initial';
    });

    $scope.saveAndRequestInvoice = function (form, billingAddress, orderNumber) {
      form.$submitted = true;

      if (form.$valid) {
        $scope.invoiceStatus = 'generatingInvoice';

        invoiceService.getInvoice(billingAddress, orderNumber)
          .then(
            function (response) {
              $scope.invoiceStatus = 'showInvoice';
              if (response.data.length) {
                $scope.invoices = response.data;
              } else {
                $scope.invoiceStatus = 'invoiceNotFound';
              }
            },

            function () {
              $scope.invoiceStatus = 'invoiceNotFound';
            }

          );

      }
    };
  },

]);
