'use strict';

angular.module('linioApp').controller('CustomerAddressFormController', [
  '$scope', 'CustomerAddressService',
  function ($scope, $window, addressService) {
    var regions = [];
    var municipalities = [];
    var cities = [];

    // Allow parent scope access to this scope.
    $scope.$parent.childScope = $scope;

    $scope.addressData = {};

    $scope.loadRegions = function () {
      addressService.getRegions($scope.addressData.country)
        .then(
          function (response) {
            $scope.regions = response.data;
          }

        );
    };

    $scope.loadMunicipalities = function () {
      return addressService.getMunicipalities($scope.addressData.region)
        .then(
          function (response) {
            $scope.municipalities = response.data;
            if ($window.hasOwnProperty('addressData')) {
              $scope.addressData.municipality = $window.addressData.municipality;
            }
          }

        );
    };

    $scope.loadCities = function () {
      addressService.getCities($scope.addressData.municipality)
        .then(
          function (response) {
            $scope.cities = response.data;
          }

        );
    };
  },

]);
