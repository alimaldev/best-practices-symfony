'use strict';

/**
 *  Newsletter Modal Controller
 */

// jscs:disable maximumLineLength
angular.module('linioApp').controller('NewsletterModalController', ['$scope', 'NewsletterService', function ($scope, newsletterService) {

  // jscs:disable maximumLineLength

  $scope.isSaving = false;
  $scope.error = '';
  $scope.newsletterStep = 'form';

  var setFormError = function (error) {
    $scope.isSaving = false;
    $scope.error = error;
  };

  $scope.subscribe = function () {
    if ($scope.newsletterSubscription.$valid) {
      $scope.error = '';
      $scope.isSaving = true;

      newsletterService.subscribe($scope.newsletter.email)
        .then(
          function (response) {
            if (response.data.status == 'success') {
              $scope.newsletter.email = '';
              $scope.newsletter.coupon = response.data.coupon;
              $scope.newsletterStep = 'coupon';
            } else if (response.data.status == 'already_subscribed') {
              setFormError('Oops!, Ya te habías suscrito antes.');
            }
          },

          function () {
            setFormError('Ocurrió un error, por favor revisa tu información.');
          }

        );
    }
  };
}, ]);
