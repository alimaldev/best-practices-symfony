'use strict';

angular.module('linioApp').factory('reviewService', ['$http', function ($http) {
  return {
    setSellerReview: function (sellerSlug, review) {
      return $http.post('/api/customer/seller/' + sellerSlug + '/review', review);
    },
  };
}, ]);
