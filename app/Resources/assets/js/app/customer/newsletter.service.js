'use strict';

/**
 * Newsletter service
 */
angular.module('linioApp').factory('NewsletterService', ['$http', function ($http) {

  return {
    subscribe: function (email) {
      return $http.post('/api/newsletter/subscribe', {
        email: email,
      });
    },
  };
}, ]);
