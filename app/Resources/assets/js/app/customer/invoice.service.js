'use strict';

/**
 * invoiceService
 */
angular.module('linioApp').factory('invoiceService', ['$http', function ($http) {

  return {
    getInvoice: function (billingAddress, orderNumber) {

      var customerAddress = {};

      angular.forEach(billingAddress, function (value, key) {

        if (angular.isObject(value) && value.hasOwnProperty('value')) {
          this[key] = value.value;
        } else if (angular.isObject(value) && value.hasOwnProperty('id')) {
          this[key] = value.id;
        } else {
          this[key] = value;
        }

      }, customerAddress);

      return $http.post('/api/customer/request-invoice', { customerAddress: customerAddress, orderId: orderNumber });
    },
  };
}, ]);
