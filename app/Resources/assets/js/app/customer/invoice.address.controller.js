'use strict';

angular.module('linioApp').controller('InvoiceAddressController', [
  '$rootScope', '$scope', '$window', 'CustomerAddressService',
  function ($rootScope, $scope, $window, addressService) {
    var regions = [];
    var municipalities = [];
    var cities = [];
    var neighborhoods = [];

    // Allow parent scope access to this scope.
    $scope.$parent.childScope = $scope;

    $scope.billingAddress = {};

    $scope.loadRegions = function () {
      $rootScope.overlayActive = true;

      addressService.getRegions($scope.billingAddress)
        .then(
          function (response) {
            $scope.regions = response.data;
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.loadMunicipalities = function () {
      //jscs:disable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength
      if (!$scope.billingAddress.region && !$scope.billingAddress.regionId && !$scope.billingAddress.city) {
        //jscs:enable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength
        return;
      }

      $rootScope.overlayActive = true;

      return addressService.getMunicipalities($scope.billingAddress)
        .then(
          function (response) {
            $scope.municipalities = response.data;
            if ($window.hasOwnProperty('billingAddress')) {
              $scope.billingAddress.municipality = $window.billingAddress.municipality;
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.loadCities = function () {
      //jscs:disable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength
      if (!$scope.billingAddress.municipality && !$scope.billingAddress.region && !$scope.billingAddress.regionId) {
        //jscs:enable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength
        return;
      }

      $rootScope.overlayActive = true;

      addressService.getCities($scope.billingAddress)
        .then(
          function (response) {
            $scope.cities = response.data;
            if ($window.hasOwnProperty('billingAddress')) {
              $scope.billingAddress.city = $window.billingAddress.city;
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.loadNeighborhoods = function () {
      if (!$scope.billingAddress.city) {
        return;
      }

      $rootScope.overlayActive = true;

      addressService.getNeighborhoods($scope.billingAddress)
        .then(
          function (response) {
            $scope.neighborhoods = response.data;
            if ($window.hasOwnProperty('billingAddress')) {
              $scope.billingAddress.neighborhood = $window.billingAddress.neighborhood;
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.resolvePostcode = function () {
      // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      if (!$scope.billingAddress['billingAddress[postcode]'].$valid) {
        return;
      }

      // jscs:enable requireCamelCaseOrUpperCaseIdentifiers

      $rootScope.overlayActive = true;

      $scope.regions = [];
      $scope.municipalities = [];
      $scope.cities = [];
      $scope.neighborhoods = [];

      addressService.resolvePostcode($scope.billingAddress.postcode)
        .then(
          function (response) {
            $scope.regions.push(response.data.region);
            $scope.municipalities.push(response.data.municipality);
            $scope.cities.push(response.data.city);
            $scope.neighborhoods = response.data.neighborhoods;

            //jscs:disable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength
            if ($scope.billingAddress.hasOwnProperty('billingAddress[regionId]')) {
              $scope.billingAddress.regionId = response.data.region.id;
            }

            if ($scope.billingAddress.hasOwnProperty('billingAddress[region]')) {
              $scope.billingAddress.region = response.data.region;
            }

            //jscs:enable requireCamelCaseOrUpperCaseIdentifiers, maximumLineLength
            $scope.billingAddress.city = response.data.city;
            $scope.billingAddress.municipality = response.data.municipality;

            if (response.data.neighborhoods.length > 0) {
              $scope.billingAddress.neighborhood = response.data.neighborhoods[0];
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    if ($window.hasOwnProperty('billingAddress')) {
      for (var prop in $window.billingAddress) {
        if (prop != 'municipality' || prop != 'city' || prop != 'neighborhood') {
          $scope.billingAddress[prop] = $window.billingAddress[prop];
        }
      }

      //jscs:disable requireCamelCaseOrUpperCaseIdentifiers
      if ($scope.billingAddress.region || $scope.billingAddress.regionId) {
        //jscs:enable requireCamelCaseOrUpperCaseIdentifiers
        $scope.loadMunicipalities();
      }

      if ($scope.billingAddress.municipality) {
        $scope.loadCities();
      }

      if ($scope.billingAddress.city) {
        $scope.loadNeighborhoods();
      }
    }
  },

]);
