'use strict';

/**
 * Address form service for dynamic data
 */
angular.module('linioApp').factory('CustomerAddressService', ['$http', function ($http) {

  var prepareData = function (addressData) {
    //jscs:disable
    var data = {
      country: '',
      region: '',
      fk_customer_address_region: '',
      city: '',
      municipality: '',
      neighborhood: '',
    };

    //jscs:enable

    angular.forEach(data, function (value, key) {
      this[key] = addressData[key];
    }, data);

    return data;
  };

  return {
    resolvePostcode: function (postcode) {
      return $http({
        method: 'GET',
        url: '/api/address/postcode',
        params: {
          postcode: postcode,
        },
      });
    },

    getRegions: function (addressData) {
      return $http({
        method: 'GET',
        url: '/api/address/regions',
        params: prepareData(addressData),
      });
    },

    getMunicipalities: function (addressData) {
      return $http({
        method: 'GET',
        url: '/api/address/municipalities',
        params: prepareData(addressData),
      });
    },

    getCities: function (addressData) {
      return $http({
        method: 'GET',
        url: '/api/address/cities',
        params: prepareData(addressData),
      });
    },

    getNeighborhoods: function (addressData) {
      return $http({
        method: 'GET',
        url: '/api/address/neighborhoods',
        params: prepareData(addressData),
      });
    },

    saveAddress: function (type, rawData) {

      var data = {};

      angular.forEach(rawData, function mapDataToFlat(value, key) {

        if (angular.isObject(value) && value.hasOwnProperty('value')) {
          this[key] = value.value;
        } else if (angular.isObject(value) && value.hasOwnProperty('id')) {
          this[key] = value.id;
        } else {
          this[key] = value;
        }

      }, data);

      return $http.post('/api/customer/address/' + type + '/create', data);
    },

    getAddresses: function () {
      return $http.get('/api/customer/addresses');
    },

    getAddressById: function (addressId) {
      var url = '/api/address/' + addressId;
      var config = { withCredential: true };
      return $http.get(url, config).then(function (response) {
        return response.data;
      });
    },

    getShippingAddress: function () {
      return $http.get('/api/customer/addresses/shipping');
    },

    getBillingAddress: function () {
      return $http.get('/api/customer/addresses/billing');
    },

    setDefaultAddress: function (type, addressId) {
      return $http.patch('/api/customer/address/set-default/' + type + '/' + addressId);
    },

    typeShipping: function () {
      return 'shipping';
    },

    typeBilling: function () {
      return 'billing';
    },
  };
}, ]);
