'use strict';

angular.module('linioApp').controller('CustomerAddressController', [
  '$rootScope', '$scope', '$window', 'CustomerAddressService',
  function ($rootScope, $scope, $window, addressService) {
    var regions = [];
    var municipalities = [];
    var cities = [];
    var neighborhoods = [];

    // Allow parent scope access to this scope.
    $scope.$parent.childScope = $scope;

    $scope.addressData = {};

    $scope.loadRegions = function () {
      $rootScope.overlayActive = true;

      addressService.getRegions($scope.addressData)
        .then(
          function (response) {
            $scope.regions = response.data;
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.loadMunicipalities = function () {
      //jscs:disable
      if (!$scope.addressData.region && !$scope.addressData.fk_customer_address_region && !$scope.addressData.city) {
        //jscs:enable
        return;
      }

      $rootScope.overlayActive = true;

      return addressService.getMunicipalities($scope.addressData)
        .then(
          function (response) {
            $scope.municipalities = response.data;
            if ($window.hasOwnProperty('addressData')) {
              $scope.addressData.municipality = $window.addressData.municipality;
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.loadCities = function () {
      //jscs:disable
      if (!$scope.addressData.municipality && !$scope.addressData.region && !$scope.addressData.fk_customer_address_region) {
        //jscs:enabled
        return;
      }

      $rootScope.overlayActive = true;

      addressService.getCities($scope.addressData)
        .then(
          function (response) {
            $scope.cities = response.data;
            if ($window.hasOwnProperty('addressData')) {
              $scope.addressData.city = $window.addressData.city;
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.loadNeighborhoods = function () {
      if (!$scope.addressData.city) {
        return;
      }

      $rootScope.overlayActive = true;

      addressService.getNeighborhoods($scope.addressData)
        .then(
          function (response) {
            $scope.neighborhoods = response.data;
            if ($window.hasOwnProperty('addressData')) {
              $scope.addressData.neighborhood = $window.addressData.neighborhood;
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    $scope.resolvePostcode = function () {
      if (!$scope.address['address[postcode]'].$valid) {
        return;
      }

      $rootScope.overlayActive = true;

      $scope.regions = [];
      $scope.municipalities = [];
      $scope.cities = [];
      $scope.neighborhoods = [];

      addressService.resolvePostcode($scope.addressData.postcode)
        .then(
          function (response) {
            $scope.regions.push(response.data.region);
            $scope.municipalities.push(response.data.municipality);
            $scope.cities.push(response.data.city);
            $scope.neighborhoods = response.data.neighborhoods;

            //jscs:disable
            if ($scope.address.hasOwnProperty('address[fk_customer_address_region]')) {
              $scope.addressData.fk_customer_address_region = response.data.region.id;
            }

            //jscs:enable

            if ($scope.address.hasOwnProperty('address[region]')) {
              $scope.addressData.region = response.data.region;
            }

            $scope.addressData.city = response.data.city;
            $scope.addressData.municipality = response.data.municipality;

            if (response.data.neighborhoods.length > 0) {
              $scope.addressData.neighborhood = response.data.neighborhoods[0];
            }
          }

        )
        .finally(function () {
            $rootScope.overlayActive = false;
          }

        );
    };

    if ($window.hasOwnProperty('addressData')) {
      for (var prop in $window.addressData) {
        if (prop != 'municipality' || prop != 'city' || prop != 'neighborhood') {
          $scope.addressData[prop] = $window.addressData[prop];
        }
      }

      //jscs:disable
      if ($scope.addressData.region || $scope.addressData.fk_customer_address_region) {
        //jscs:enable
        $scope.loadMunicipalities();
      }

      if ($scope.addressData.municipality) {
        $scope.loadCities();
      }

      if ($scope.addressData.city) {
        $scope.loadNeighborhoods();
      }
    }
  },

]);
