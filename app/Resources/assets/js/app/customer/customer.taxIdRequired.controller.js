'use strict';

(function (angular) {
  function taxIdRequiredFormController($rootScope, $scope, customerService) {
    $scope.submittingForm = false;
    $scope.registerTaxId = function (form) {
      if (form.$valid) {
        $scope.submittingForm = true;
        $rootScope.overlayActive = true;
        customerService.setTaxID($scope.customer.taxIdentificationNumber, $scope.taxIdField)
          .then(
            function () {
              $scope.closeThisDialog(true);
              $rootScope.overlayActive = false;
            },

            function () {
              $scope.submittingForm = false;
              $rootScope.overlayActive = false;
            }

          );
      }
    };
  }

  angular.module('linioApp').controller('taxIdRequiredFormController', [
    '$rootScope',
    '$scope',
    'customerService',
    taxIdRequiredFormController,
  ]);
})(angular);
