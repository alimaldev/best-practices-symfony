(function () {
  'use strict';

  angular
    .module('linio.account')
    .component('sellerReviewForm', {
      controller: SellerReviewFormController,
      controllerAs: 'sellerReview',
      templateUrl: 'sellerReviewForm',
      bindings: {
        orderNumber: '@',
        sellerId: '@',
      },
    });

  SellerReviewFormController.$inject = ['customer'];
  function SellerReviewFormController(customer) {
    var viewModel = this;
    viewModel.title = '';
    viewModel.detail = '';
    viewModel.rating = 0;
    viewModel.showForm = true;
    viewModel.hasError = false;
    viewModel.hasSuccess = false;
    viewModel.canSubmit = canSubmit;
    viewModel.submit = submit;

    function canSubmit() {
      if (viewModel.rating == 0) {
        return false;
      }

      if (!viewModel.title) {
        return false;
      }

      if (!viewModel.detail) {
        return false;
      }

      return true;
    }

    function submit() {
      var review = {
        rating: viewModel.rating,
        title: viewModel.title,
        detail: viewModel.detail,
      };

      customer.reviewSeller(viewModel.orderNumber, viewModel.sellerId, review).then(function () {
        viewModel.showForm = false;
        viewModel.hasSuccess = true;
        viewModel.hasError = false;
      }).catch(function () {
        viewModel.hasError = true;
      });
    }
  }
})();
