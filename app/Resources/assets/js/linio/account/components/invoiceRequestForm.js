(function () {
  'use strict';

  angular
    .module('linio.account')
    .component('invoiceRequestForm', {
      controller: InvoiceRequestForm,
      controllerAs: 'invoiceRequestForm',
      bindings: {
        input: '<',
        orderNumber: '<',
      },
      templateUrl: '/ng/invoice-request-form',
    });

  InvoiceRequestForm.$inject = ['event', 'customer'];
  function InvoiceRequestForm(event, customer) {
    var viewModel = this;
    viewModel.hasError = false;
    viewModel.showForm = true;
    viewModel.invoices = [];
    viewModel.submit = submit;

    function submit() {
      viewModel.showForm = false;

      customer.requestInvoice(viewModel.orderNumber, viewModel.input).then(function (invoices) {
        viewModel.invoices = invoices;
        viewModel.showForm = false;
      }).catch(function () {
        viewModel.hasError = true;
        viewModel.showForm = false;
      });
    }
  }
})();
