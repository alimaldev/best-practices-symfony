(function () {
  'use strict';

  angular
    .module('linio.account')
    .component('invoiceRequest', {
      controller: InvoiceRequestController,
      controllerAs: 'invoiceRequest',
      template: '<a href="" ng-click="invoiceRequest.show()" class="btn btn-primary btn-md is-weight-bold"><i data-icon="P" class="invoice-icon"></i> {[ invoiceRequest.title ]}</a>',
      bindings: {
        title: '@',
        orderNumber: '@',
      },
    });

  InvoiceRequestController.$inject = ['$scope', 'event', 'ngDialog'];
  function InvoiceRequestController($scope, event, ngDialog) {
    var viewModel = this;
    viewModel.addressBook = null;
    viewModel.selectedAddress = null;
    viewModel.listeners = [];
    viewModel.$onInit = onInit;
    viewModel.$onDestroy = onDestroy;
    viewModel.show = show;

    function onInit() {
      viewModel.listeners.push(event.listen('address.selected_address', onSelectedAddress));
    }

    function onDestroy() {
      for (var listener in viewModel.listeners) {
        viewModel.listeners[listener]();
      }
    }

    function show() {
      viewModel.addressBook = ngDialog.open({
        template: '<address-book selectable="true" type="shipping" />',
        plain: true,
      });
    }

    function onSelectedAddress(event, selectedAddress) {
      viewModel.selectedAddress = selectedAddress.address;

      if (viewModel.addressBook) {
        viewModel.addressBook.close();
        ngDialog.open({
          template: '<invoice-request-form input="invoiceRequest.selectedAddress" order-number="invoiceRequest.orderNumber" />',
          plain: true,
          scope: $scope,
        });
      }
    }
  }
})();
