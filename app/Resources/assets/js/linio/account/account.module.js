(function () {
  'use strict';

  angular.module('linio.account', [
    'linio.core',
    'linio.customer',
    'linio.address',
  ]);
})();
