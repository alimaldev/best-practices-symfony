(function () {
  'use strict';

  angular
    .module('linio.account')
    .controller('OrderReturnController', OrderReturnController);

  OrderReturnController.$inject = ['$q', 'customer', 'navigate'];
  function OrderReturnController($q, customer, navigate) {
    var viewModel = this;
    viewModel.items = {};
    viewModel.hasSuccess = false;
    viewModel.canSubmit = canSubmit;
    viewModel.submit = submit;

    function canSubmit() {
      for (var sku in viewModel.items) {
        if (viewModel.items[sku].selected) {
          return true;
        }
      }

      return false;
    }

    function submit(orderNumber) {
      var promises = [];

      for (var sku in viewModel.items) {
        if (!viewModel.items[sku].selected) {
          continue;
        }

        viewModel.items[sku].sku = sku;
        promises.push(customer.returnOrderItem(orderNumber, viewModel.items[sku]).then(function () {
          viewModel.items[sku].selected = false;
        }));
      }

      $q.all(promises).then(function () {
        viewModel.hasSuccess = true;
        navigate.to('/account/order/' + orderNumber + '/return/confirmation');
      });
    }
  }
})();
