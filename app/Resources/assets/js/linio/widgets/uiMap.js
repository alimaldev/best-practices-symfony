(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .directive('uiMap', uiMap);

  uiMap.$inject = ['event'];
  function uiMap(event) {
    var markerIcon = '/assets/images/icons/location.png';
    var selectedMarkerIcon = '/assets/images/icons/location-picked.png';
    var lastMarker;

    return {
      link: link,
      restrict: 'E',
      replace: true,
      template: '<div></div>',
      scope: {
        center: '<',
        markers: '<',
        zoom: '<?',
        changeRadius: '<?',
      },
    };

    function link(scope, element, attrs) {
      if (!scope.zoom) {
        scope.zoom = 14;
      }

      if (!scope.changeRadius) {
        scope.changeRadius = 3000;
      }

      var map = new google.maps.Map(element[0], { center: scope.center, zoom: scope.zoom });
      var initialBounds;

      map.addListener('dragend', function () {
        if (!initialBounds) {
          return;
        }

        if (initialBounds.contains(map.getCenter())) {
          return;
        }

        var currentCenter = map.getCenter();
        event.dispatch('map.past_radius', { lat: currentCenter.lat(), lng: currentCenter.lng() });
        initialBounds = createBounds(currentCenter, scope.changeRadius);
      });

      scope.$watch('center', function (center) {
        map.setCenter(center);
        initialBounds = createBounds(map.getCenter(), scope.changeRadius);
      });

      scope.$watch('markers', function (markers) {
        updateMarkers(scope, map, markers);
      }, true);
    }

    function createBounds(center, radius) {
      var range = new google.maps.Circle({ center: center, radius: radius });

      return range.getBounds();
    }

    function updateMarkers(scope, map, markers) {
      for (var markerId in markers) {
        var markerData = markers[markerId];
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(markerData.lat, markerData.lng),
          map: map,
          title: markerData.name,
          id: markerData.id,
          icon: markerIcon,
        });

        marker.addListener('click', function () {
          event.dispatch('map.selected_store', this);
          map.setCenter(this.getPosition());
          this.setIcon(selectedMarkerIcon);

          if (scope.lastMarker) {
            scope.lastMarker.setIcon(markerIcon);
          }

          scope.lastMarker = this;
        });

        marker.setMap(map);
      }
    }
  }
})();
