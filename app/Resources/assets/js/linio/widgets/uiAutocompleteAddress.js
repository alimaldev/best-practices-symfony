(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .directive('uiAutocompleteAddress', uiAutocompleteAddress);

  uiAutocompleteAddress.$inject = ['event'];
  function uiAutocompleteAddress(event) {
    return {
      link: link,
      restrict: 'A',
    };

    function link(scope, element, attrs) {
      var autocomplete = new google.maps.places.Autocomplete(element[0]);

      autocomplete.addListener('place_changed', function () {
        event.dispatch('address.selected_place', autocomplete.getPlace());
      });
    }
  }
})();
