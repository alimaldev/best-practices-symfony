(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .directive('uiAccordion', uiAccordion);

  function uiAccordion() {
    return {
      restrict: 'AE',
      templateUrl: 'accordion',
      transclude: {
        accordiontitle: '?uiAccordionTitle',
      },
      scope: {
        title: '@?',
        logo: '@?',
        className: '@?',
      },
      controller: UiAccordionController,
      controllerAs: 'accordion',
    };

    function UiAccordionController($scope) {
      var viewModel = this;
      viewModel.className = $scope.className ? $scope.className : '';
      viewModel.expanded = false;
      viewModel.loadContent = loadContent;
      viewModel.logo = $scope.logo ? $scope.logo : '';
      viewModel.switchPane = switchPane;
      viewModel.title = $scope.title ? $scope.title : '';

      function loadContent(contentController) {
        viewModel.title = contentController.title;
        viewModel.logo = contentController.logo;
        viewModel.content = contentController;
      };

      function switchPane() {
        viewModel.expanded = !viewModel.expanded;
      };
    }
  }
})();
