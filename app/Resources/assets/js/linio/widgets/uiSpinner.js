(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .directive('uiSpinner', uiSpinner);

  uiSpinner.$inject = ['$http', 'event'];
  function uiSpinner($http, event) {
    return {
      restrict: 'E',
      link: link,
    };

    function link(scope, element, attrs) {
      scope.spinningEvent = false;
      scope.isLoading = function () {
        for (var request in $http.pendingRequests) {
          if ($http.pendingRequests[request].spinner === false) {
            return false;
          }
        }

        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, function (isLoading) {
        if (scope.spinningEvent) {
          return;
        }

        isLoading ? element.removeClass('ng-hide') : element.addClass('ng-hide');
      });

      event.listen('spinner.spin', function () {
        scope.spinningEvent = true;
        element.removeClass('ng-hide');
      });

      event.listen('spinner.stop', function () {
        scope.spinningEvent = false;
        element.addClass('ng-hide');
      });

      event.listen('spinner.customize', function (event, options) {
        element.addClass(options.class);
      });
    }
  }
})();
