(function () {
  'use strict';

  angular.module('ui.widgets', [
    'linio.core',
  ]);
})();
