(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .component('starRatings', {
      controller: StarRatingsController,
      controllerAs: 'star',
      template: '<span class="star-rating massive reviews-stars-input">' +
          '<i ng-repeat="i in star.starsObject" aria-hidden="true" data-icon="š" class="star" ng-class="i==true?\'active\':\'\'" ng-click="star.setRating($index)" ng-mouseover="star.setActiveStars($index)" ng-mouseleave="star.clearTempActiveStars()"></i>' +
          '</span>',
      bindings: {
        stars: '=',
      },
      require: {
        ngModel: 'ngModel',
      },
    });

  function StarRatingsController() {
    var viewModel = this;
    viewModel.$onInit = onInit;
    viewModel.setRating = setRating;
    viewModel.setActiveStars = setActiveStars;
    viewModel.clearTempActiveStars = clearTempActiveStars;
    viewModel.starsObject = {};
    viewModel.selectedStar;

    function onInit() {
      clearStars();
    }

    function setRating(starPosition) {
      clearStars();
      setActiveRecursive(starPosition);

      viewModel.selectedStar = starPosition;

      if (viewModel) {
        viewModel.ngModel.$setViewValue(starPosition + 1);
      }
    }

    function clearStars() {
      for (var i = 0; i < viewModel.stars; i++) {
        viewModel.starsObject[i] = false;
      }
    }

    function setActiveRecursive(starPosition) {
      angular.forEach(viewModel.starsObject, function (value, key) {
        if (key <= starPosition) {
          viewModel.starsObject[key] = true;
        }
      });
    }

    function setActiveStars(starPosition) {
      if (typeof viewModel.selectedStar === 'undefined') {
        clearStars();
        setActiveRecursive(starPosition);
      }
    }

    function clearTempActiveStars() {
      if (typeof viewModel.selectedStar === 'undefined') {
        clearStars();
        angular.forEach(viewModel.starsObject, function (value, key) {
          if (typeof viewModel.selectedStar != 'undefined') {
            if (key <= viewModel.selectedStar) {
              viewModel.starsObject[key] = true;
            }
          }
        });

      }
    }
  }
})();
