angular
  .module('ui.widgets')
  .controller('OuiBounceModalController', OuiBounceModalController);

OuiBounceModalController.$inject = ['newsletter', '$scope', 'ngDialog'];
function OuiBounceModalController(newsletter, $scope, ngDialog) {

  var viewModel = this;

  viewModel.subscribe = subscribe;

  function setFormError(error) {
    viewModel.isSaving = false;
    viewModel.error = error;
  }

  function subscribe() {
    viewModel.error = '';
    viewModel.isSaving = true;

    newsletter.subscribe(viewModel.subscriber)
      .then(
        function (response) {
          if (response.status == 'success') {
            ngDialog.closeAll();
          } else if (response.status == 'already_subscribed') {
            setFormError('Oops!, Ya te habías suscrito antes.');
          }
        }
      );
  }
}
