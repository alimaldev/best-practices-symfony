(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .component('ouiBounce', {
      controller: OuiBounceController,
      controllerAs: 'ouiBounce',
      template: '',
      bindings: {
        aggressive: '@',
        sensitivity: '@',
        cookieExpire: '@',
        timer: '@',
        delay: '@',
      },
    });

  OuiBounceController.$inject = ['ngDialog'];
  function OuiBounceController(ngDialog) {

    var viewModel = this;

    viewModel.bounceExchangeModal = null;
    viewModel.show = show;

    ouibounce(false, {
      aggressive: viewModel.aggressive,
      cookieExpire: viewModel.cookieExpire,
      timer: viewModel.timer,
      delay: viewModel.delay,
      sensitivity: viewModel.sensitivity,
      callback: show,
    });

    function show() {
      viewModel.bounceExchangeModal = ngDialog.open({
        template: 'ouiBounceModal',
        className: 'modal-ouibounce-container',
        controller: 'OuiBounceModalController',
        controllerAs: 'ouiBounceModal',
      });
    }
  }

})();
