(function () {
  'use strict';

  angular
    .module('ui.widgets')
    .directive('uiMessenger', uiMessenger);

  uiMessenger.$inject = ['messenger'];
  function uiMessenger(messenger) {
    return {
      link: link,
      restrict: 'E',
      template: '<div ng-repeat="message in messages"><div class="alert alert-{{ message.type }}">{{ message.content }}</div></div>',
    };

    function link(scope, element, attrs) {
      scope.checkMessages = function () {
        return messenger.queue;
      };

      scope.$watch(scope.checkMessages, function (messages) {
        scope.messages = messages;
      });
    }
  }
})();
