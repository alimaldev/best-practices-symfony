(function () {
  'use strict';

  angular.module('linio.customer', [
    'linio.core',
    'ui.widgets',
  ]);
})();
