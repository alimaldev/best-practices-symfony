(function () {
  'use strict';

  angular
    .module('linio.customer')
    .factory('customer', customer);

  customer.$inject = ['$http', '$q', 'logger', 'messenger'];
  function customer($http, $q, logger, messenger) {
    var customer = {
      addresses: null,
      loadAddresses: loadAddresses,
      getAddresses: getAddresses,
      getAddressById: getAddressById,
      getDefaultShippingAddress: getDefaultShippingAddress,
      getDefaultBillingAddress: getDefaultBillingAddress,
      createAddress: createAddress,
      updateAddress: updateAddress,
      removeAddressById: removeAddressById,
      signupPartnership: signupPartnership,
      removePartnership: removePartnership,
      signupLoyalty: signupLoyalty,
      registerWallet: registerWallet,
      getCards: getCards,
      reviewSeller: reviewSeller,
      returnOrderItem: returnOrderItem,
      requestInvoice: requestInvoice,
    };

    return customer;

    function loadAddresses() {
      return $http
        .get('/api/customer/addresses')
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function getAddresses() {
      if (customer.addresses) {
        return $q.when(customer.addresses);
      }

      return loadAddresses().then(function (addresses) {
        customer.addresses = addresses;

        return customer.addresses;
      });
    }

    function getAddressById(id) {
      var address = null;

      return customer.getAddresses().then(function (addresses) {
        if (addresses[id] == undefined) {
          return $q.reject('Invalid address id: ' + id);
        }

        address = addresses[id];

        return $q.when(address);
      });
    }

    function getDefaultShippingAddress() {
      var address = null;

      return customer.getAddresses().then(function (addresses) {
        for (var addressId in addresses) {
          if (addresses[addressId].defaultShipping) {
            address = addresses[addressId];
            break;
          }
        }

        return $q.when(address);
      });
    }

    function getDefaultBillingAddress() {
      var address = null;

      return customer.getAddresses().then(function (addresses) {
        for (var addressId in addresses) {
          if (addresses[addressId].defaultBilling) {
            address = addresses[addressId];
            break;
          }
        }

        return $q.when(address);
      });
    }

    function createAddress(addressForm) {
      return $http
        .post('/api/customer/address/create', addressForm)
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        var savedAddress = response.data;

        if (!customer.addresses || customer.addresses.constructor === Array) {
          customer.addresses = {};
        }

        customer.addresses[savedAddress.id] = savedAddress;

        return savedAddress;
      }
    }

    function updateAddress(addressForm) {
      return $http
        .post('/api/customer/address/update', addressForm)
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        var updatedAddress = response.data;
        customer.addresses[updatedAddress.id] = updatedAddress;

        return updatedAddress;
      }
    }

    function removeAddressById(id) {
      return $http
          .post('/api/customer/address/remove', { id: id })
          .then(onRequestSuccess)
          .catch(onRequestFailure);

      function onRequestSuccess(response) {
        var removedAddress = response.data;
        delete customer.addresses[removedAddress.id];

        return removedAddress;
      }
    }

    function signupPartnership(code, accountNumber) {
      return $http
        .post('/api/customer/partnership/signup', { code: code, accountNumber: accountNumber })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function removePartnership(code) {
      return $http
        .post('/api/customer/partnership/' + code + '/remove')
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function signupLoyalty(loyaltyId) {
      return $http
        .post('/api/customer/loyalty/signup', { loyaltyId: loyaltyId })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function registerWallet(account, holderName, holderLastname) {
      return $http
        .post('/api/customer/wallet/register', { account: account, holderName: holderName, holderLastname: holderLastname })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function getCards() {
      return $http
        .get('/api/customer/cards')
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function reviewSeller(orderNumber, sellerId, review) {
      return $http
        .post('/api/customer/review-seller', {
          orderNumber: orderNumber,
          sellerId: sellerId,
          rating: review.rating,
          title: review.title,
          detail: review.detail,
        })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function returnOrderItem(orderNumber, item) {
      return $http
        .post('/api/customer/order/return-item', {
          orderNumber: orderNumber,
          sku: item.sku,
          quantity: item.quantity,
          reason: item.reason,
          action: item.action,
          comment: item.comment,
        })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function requestInvoice(orderNumber, address) {
      return $http
        .post('/api/customer/request-invoice', { orderId: orderNumber, customerAddress: address })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function onRequestFailure(error) {
      logger.error(error.data.message, error);
      messenger.clear();
      messenger.error(error.data.message);

      return $q.reject(error);
    }
  }
})();
