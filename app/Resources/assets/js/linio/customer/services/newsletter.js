(function () {
  'use strict';

  angular
    .module('linio.customer')
    .factory('newsletter', newsletter);

  newsletter.$inject = ['$http', 'logger', 'messenger'];
  function newsletter($http, logger, messenger) {

    var newsletter = {
      subscribe: subscribe,
    };

    return newsletter;

    function subscribe(subscriber) {
      return $http
        .post('/api/newsletter/subscribe', subscriber)
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function onRequestFailure(error) {
      logger.error(error.data.message, error);
      messenger.clear();
      messenger.error(error.data.message);

      return $q.reject(error.data.message);
    }

  }
})();
