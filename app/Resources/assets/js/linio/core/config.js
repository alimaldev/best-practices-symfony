(function () {
  'use strict';

  var core = angular.module('linio.core');
  core.constant('config', (typeof lsfConfig != 'undefined') ? lsfConfig : {});
  core.config(configure);

  configure.$inject = ['config', '$interpolateProvider', '$httpProvider', '$compileProvider'];
  function configure(config, $interpolateProvider, $httpProvider, $compileProvider) {
    $interpolateProvider.startSymbol('{[').endSymbol(']}');
    $httpProvider.interceptors.push('loginInterceptor');
    $httpProvider.interceptors.push('requestIdInterceptor');
    $compileProvider.debugInfoEnabled(config.debug);
  }
})();
