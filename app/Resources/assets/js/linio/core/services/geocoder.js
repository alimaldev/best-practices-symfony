(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('geocoder', geocoder);

  geocoder.$inject = ['$q'];
  function geocoder($q) {
    return {
      getCoordinates: getCoordinates,
    };

    function getCoordinates(address) {
      var deferred = $q.defer();

      if (!google.maps.Geocoder) {
        deferred.reject('Google Maps library not loaded.');

        return deferred.promise;
      }

      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ address: address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          deferred.resolve({
            lat: results[0].geometry.location.lat(),
            lng: results[0].geometry.location.lng(),
          });
        } else {
          deferred.reject(status);
        }
      });

      return deferred.promise;
    }
  }
}());
