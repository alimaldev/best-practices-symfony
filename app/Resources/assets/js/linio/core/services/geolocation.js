(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('geolocation', geolocation);

  geolocation.$inject = ['$window', '$q'];
  function geolocation($window, $q) {
    return {
      getCurrentCoordinates: getCurrentCoordinates,
    };

    function getCurrentCoordinates() {
      var deferred = $q.defer();

      if ($window.navigator.geolocation) {
        $window.navigator.geolocation.getCurrentPosition(function (position) {

          deferred.resolve({ lat: position.coords.latitude, lng: position.coords.longitude });
        }, function (error) {

          deferred.reject(error);
        });
      } else {
        deferred.reject('Missing geolocation support.');
      }

      return deferred.promise;
    }
  }
}());
