(function () {
  'use strict';

  angular
    .module('linio.core')
    .provider('router', routerProvider);

  routerProvider.$inject = ['$locationProvider', '$stateProvider'];
  function routerProvider($locationProvider, $stateProvider) {
    var config = {
      docTitle: undefined,
      resolveAlways: {},
    };

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false,
      rewriteLinks: false,
    });

    this.configure = function (cfg) {
      angular.extend(config, cfg);
    };

    this.$get = Router;
    Router.$inject = ['$location', '$rootScope', '$state', 'logger'];
    function Router($location, $rootScope, $state, logger) {
      var currentlyHandlingStateError = false;
      var service = {
        configureStates: configureStates,
        getStates: getStates,
      };

      init();

      return service;

      function configureStates(states) {
        states.forEach(function (state) {
          state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
          $stateProvider.state(state.state, state.config);
        });
      }

      function handleRoutingErrors() {
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
          if (currentlyHandlingStateError) {
            return;
          }

          currentlyHandlingStateError = true;
          logger.warning('Unable to change state: ' + error.data, [toState]);
          $location.path('/');
        });
      }

      function init() {
        handleRoutingErrors();
      }

      function getStates() {
        return $state.get();
      }
    }
  }
})();
