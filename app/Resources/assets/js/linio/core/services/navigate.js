(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('navigate', navigate);

  navigate.$inject = ['$window', 'event'];
  function navigate($window, event) {
    return {
      to: to,
      toCart: toCart,
      toCheckoutSuccess: toCheckoutSuccess,
      toLogin: toLogin,
    };

    function to(uri) {
      $window.location.href = uri;
      event.dispatch('spinner.spin');
    }

    function toCart() {
      $window.location.href = '/cart';
      event.dispatch('spinner.spin');
    }

    function toCheckoutSuccess() {
      $window.location.href = '/checkout/success';
      event.dispatch('spinner.spin');
    }

    function toLogin() {
      $window.location.href = '/account/login';
      event.dispatch('spinner.spin');
    }
  }
}());
