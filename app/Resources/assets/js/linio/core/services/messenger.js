(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('messenger', messenger);

  function messenger() {
    var messenger = {
      queue: [],
      report: report,
      clear: clear,
      error: error,
      info: info,
      warning: warning,
      success: success,
    };

    return messenger;

    function report() {
      return messenger.queue.shift();
    }

    function clear() {
      messenger.queue = [];
    }

    function error(message) {
      messenger.queue.push({ content: message, type: 'error' });
    }

    function info(message) {
      messenger.queue.push({ content: message, type: 'info' });
    }

    function success(message) {
      messenger.queue.push({ content: message, type: 'success' });
    }

    function warning(message) {
      messenger.queue.push({ content: message, type: 'warning' });
    }
  }
}());
