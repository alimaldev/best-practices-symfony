(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('logger', logger);

  logger.$inject = ['$log'];
  function logger($log) {
    return {
      log: $log.log,
      info: info,
      warning: warning,
      error: error,
      debug: debug,
    };

    function info(message, data) {
      $log.info('Info: ' + message, data);
    }

    function warning(message, data) {
      $log.warn('Warning: ' + message, data);
    }

    function error(message, data) {
      $log.error('Error: ' + message, data);
    }

    function debug(message, data) {
      $log.debug('Debug: ' + message, data);
    }
  }
}());
