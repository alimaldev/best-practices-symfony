(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('event', event);

  event.$inject = ['$rootScope'];
  function event($rootScope) {
    return {
      dispatch: dispatch,
      listen: listen,
    };

    function dispatch(event, data) {
      return $rootScope.$broadcast(event, data);
    }

    function listen(event, listener) {
      return $rootScope.$on(event, listener);
    }
  }
}());
