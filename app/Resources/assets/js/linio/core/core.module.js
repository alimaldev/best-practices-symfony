(function () {
  'use strict';

  angular.module('linio.core', [
    'ngAnimate',
    'ngMask',
    'ngDialog',
    'ui.router',
    'credit-cards',
  ]);
})();
