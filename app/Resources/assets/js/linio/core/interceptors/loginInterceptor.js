(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('loginInterceptor', loginInterceptor);

  loginInterceptor.$inject = ['$q', 'navigate'];
  function loginInterceptor($q, navigate) {
    return {
      responseError: onResponseError,
    };

    function onResponseError(response) {
      if (response.status == 401 || response.status == 403) {
        navigate.toLogin();
      }

      return $q.reject(response);
    }
  }
}());
