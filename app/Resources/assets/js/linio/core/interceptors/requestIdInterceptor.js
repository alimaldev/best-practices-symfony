(function () {
  'use strict';

  angular
    .module('linio.core')
    .factory('requestIdInterceptor', requestIdInterceptor);

  function requestIdInterceptor() {
    return {
      request: onRequest,
    };

    function onRequest(config) {
      // Without this the preflight is always triggered
      if (config.withCredentials == true) {
        return config;
      }

      config.headers['X-Request-ID'] = uuid();

      return config;
    }

    function uuid() {
      var uuid = '';

      for (var i = 0; i < 8; i++) {
        uuid += (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      }

      return uuid;
    }

  }
}());
