(function () {
  'use strict';

  angular
    .module('linio.core')
    .directive('broadcastValidation', broadcastValidation);

  broadcastValidation.$inject = ['event'];
  function broadcastValidation(event) {
    return {
      restrict: 'A',
      require: '^form',
      link: link,
    };

    function link(scope, element, attrs, ctrl) {
      scope.$watch(ctrl.$name + '.$valid', function (isValid, wasValid) {
        if (isValid == undefined) {
          return;
        }

        event.dispatch('form.' + ctrl.$name + '.validation', {
          isValid: isValid,
          error: ctrl.$error,
        });
      });
    }
  }
}());
