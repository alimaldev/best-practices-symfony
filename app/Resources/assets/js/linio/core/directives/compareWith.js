(function () {
  'use strict';

  angular
    .module('linio.core')
    .directive('compareWith', compareWith);

  compareWith.$inject = ['event'];
  function compareWith(event) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: link,
      scope: {
        valueToCompare: '<compareWith',
      },
    };

    function link(scope, element, attrs, ctrl) {
      ctrl.$validators.compareWith = function (modelValue) {
        return modelValue == scope.valueToCompare;
      };

      scope.$watch('valueToCompare', function () {
        ctrl.$validate();
      });
    }
  }
}());
