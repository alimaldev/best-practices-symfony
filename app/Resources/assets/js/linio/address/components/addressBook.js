(function () {
  'use strict';

  angular
    .module('linio.address')
    .component('addressBook', {
      controller: AddressBookController,
      controllerAs: 'addressBook',
      bindings: {
        type: '@',
        selectable: '@',
        removable: '@',
      },
      templateUrl: 'addressBook',
    });

  AddressBookController.$inject = ['customer', 'event'];
  function AddressBookController(customer, event) {
    var viewModel = this;
    viewModel.currentTab = null;
    viewModel.addressToEdit = null;
    viewModel.addresses = {};
    viewModel.$onInit = onInit;
    viewModel.$onDestroy = onDestroy;
    viewModel.listeners = [];
    viewModel.hasAddresses = hasAddresses;
    viewModel.select = select;
    viewModel.edit = edit;
    viewModel.remove = remove;
    viewModel.switchTab = switchTab;

    function onInit() {
      customer.getAddresses().then(function (addresses) {
        viewModel.addresses = addresses;
        viewModel.currentTab = hasAddresses() ? 'list' : 'form';
      });

      viewModel.listeners.push(event.listen('address.new_address_created', onNewAddressCreated));
      viewModel.listeners.push(event.listen('address.address_updated', onAddressUpdated));
    }

    function onDestroy() {
      for (var listener in viewModel.listeners) {
        viewModel.listeners[listener]();
      }
    }

    function hasAddresses() {
      return Object.keys(viewModel.addresses).length > 0;
    }

    function select(id) {
      customer.getAddressById(id).then(function (address) {
        event.dispatch('address.selected_address', {
          type: viewModel.type,
          address: address,
        });
      });
    }

    function edit(id) {
      customer.getAddressById(id).then(function (address) {
        viewModel.currentTab = 'edit';
        viewModel.addressToEdit = address;

        if (address.regionId) {
          viewModel.addressToEdit.region = address.regionId.toString();
        }
      });
    }

    function remove(id) {
      customer.removeAddressById(id).then(function (removedAddress) {
        updateAddressList();
        event.dispatch('address.removed_address', {
          type: viewModel.type,
          address: removedAddress,
        });
      });
    }

    function switchTab(name) {
      viewModel.currentTab = name;
      viewModel.addressToEdit = null;
    }

    function updateAddressList() {
      customer.getAddresses().then(function (addresses) {
        viewModel.addresses = addresses;
      });
    }

    function onNewAddressCreated(e, savedAddress) {
      updateAddressList();

      if (viewModel.selectable) {
        event.dispatch('address.selected_address', {
          type: viewModel.type,
          address: savedAddress,
        });
      }

      viewModel.currentTab = 'list';
    }

    function onAddressUpdated() {
      switchTab('list');
    }
  }
})();
