(function () {
  'use strict';

  angular
    .module('linio.address')
    .component('addressForm', {
      controller: AddressFormController,
      controllerAs: 'addressForm',
      bindings: {
        input: '<',
        edit: '@',
      },
      templateUrl: '/ng/address-form',
    });

  AddressFormController.$inject = ['event', 'customer', 'resolveLocation'];
  function AddressFormController(event, customer, resolveLocation) {
    var viewModel = this;
    viewModel.input = null;
    viewModel.regions = [];
    viewModel.municipalities = [];
    viewModel.cities = [];
    viewModel.neighborhoods = [];
    viewModel.$onChanges = onChanges;
    viewModel.resolvePostcode = resolvePostcode;
    viewModel.loadRegions = loadRegions;
    viewModel.loadMunicipalities = loadMunicipalities;
    viewModel.loadCities = loadCities;
    viewModel.loadNeighborhoods = loadNeighborhoods;
    viewModel.save = save;

    function onChanges() {
      if (!viewModel.input) {
        return;
      }

      if (viewModel.input.city) {
        loadCities();
      }

      if (viewModel.input.municipality) {
        loadMunicipalities();
      }

      if (viewModel.input.neighborhood) {
        loadNeighborhoods();
      }
    }

    function resolvePostcode() {
      resolveLocation.resolvePostcode(viewModel.input.postcode).then(function (postcodeData) {
        viewModel.input.country = postcodeData.country;
        viewModel.input.region = postcodeData.region;

        loadCities().then(function () {
          viewModel.input.city = postcodeData.city;

          loadMunicipalities().then(function () {
            viewModel.input.municipality = postcodeData.municipality;

            loadNeighborhoods().then(function () {
              if (postcodeData.neighborhoods.length == 1) {
                viewModel.input.neighborhood = postcodeData.neighborhoods[0];
              }
            });
          });
        });
      });
    }

    function loadRegions() {
      return resolveLocation.getRegions(viewModel.input).then(function (regionData) {
        viewModel.regions = regionData;
      });
    }

    function loadMunicipalities() {
      return resolveLocation.getMunicipalities(viewModel.input).then(function (municipalityData) {
        viewModel.municipalities = municipalityData;
      });
    }

    function loadCities() {
      return resolveLocation.getCities(viewModel.input).then(function (cityData) {
        viewModel.cities = cityData;
      });
    }

    function loadNeighborhoods() {
      return resolveLocation.getNeighborhoods(viewModel.input).then(function (neighborhoodData) {
        viewModel.neighborhoods = neighborhoodData;
      });
    }

    function save() {
      if (viewModel.input.id) {
        customer.updateAddress(viewModel.input).then(function (updatedAddress) {
          event.dispatch('address.address_updated', updatedAddress);
        });
      } else {
        customer.createAddress(viewModel.input).then(function (savedAddress) {
          event.dispatch('address.new_address_created', savedAddress);
        });
      }
    }
  }
})();
