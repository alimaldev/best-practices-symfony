(function () {
  'use strict';

  angular
    .module('linio.address')
    .factory('resolveLocation', resolveLocation);

  resolveLocation.$inject = ['$http', '$q', 'logger', 'messenger'];
  function resolveLocation($http, $q, logger, messenger) {
    return {
      resolvePostcode: resolvePostcode,
      getRegions: getRegions,
      getMunicipalities: getMunicipalities,
      getCities: getCities,
      getNeighborhoods: getNeighborhoods,
    };

    function resolvePostcode(postcode) {
      return $http
        .get('/api/address/postcode', { params: { postcode: postcode } })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function getRegions(params) {
      return $http
        .get('/api/address/regions', { params: params })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function getMunicipalities(params) {
      return $http
        .get('/api/address/municipalities', { params: params })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function getCities(params) {
      return $http
        .get('/api/address/cities', { params: params })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function getNeighborhoods(params) {
      return $http
        .get('/api/address/neighborhoods', { params: params })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        return response.data;
      }
    }

    function onRequestFailure(error) {
      logger.error(error.data.message, error);
      messenger.clear();
      messenger.error(error.data.message);

      return $q.reject(error);
    }
  }
})();
