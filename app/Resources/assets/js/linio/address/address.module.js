(function () {
  'use strict';

  angular.module('linio.address', [
    'linio.core',
    'linio.customer',
  ]);
})();
