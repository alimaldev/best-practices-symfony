(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .directive('luhnExceptions', luhnExceptions);

  function luhnExceptions() {
    return {
      priority: 100,
      restrict: 'A',
      require: 'ngModel',
      link: link,
      scope: {
        exceptions: '<luhnExceptions',
      },
    };

    function link(scope, element, attrs, ctrl) {
      var parentNumberValidator = ctrl.$validators.ccNumber;
      ctrl.$validators.ccNumber = function (number) {
        if (isException(number)) {
          return true;
        }

        return parentNumberValidator(number);
      };

      var parentTypeValidator = ctrl.$validators.ccNumberType;
      ctrl.$validators.ccNumberType = function (number) {
        if (isException(number)) {
          return true;
        }

        return parentTypeValidator(number);
      };

      function isException(number) {
        if (ctrl.$isEmpty(ctrl.$viewValue)) {
          return true;
        }

        number = number.replace(/\s/g, '');

        if (scope.exceptions.indexOf(number.substring(0, 6)) !== -1) {
          return true;
        }

        return false;
      }
    }
  }
}());
