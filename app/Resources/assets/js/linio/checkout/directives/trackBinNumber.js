(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .directive('trackBinNumber', trackBinNumber);

  trackBinNumber.$inject = ['order', 'buildOrder'];
  function trackBinNumber(order, buildOrder) {
    return {
      restrict: 'A',
      link: link,
    };

    function link(scope, element, attrs) {
      scope.$watch(attrs.ngModel, function (number) {
        if (!number) {
          return;
        }

        number = number.replace(/\s/g, '');

        if (number.length < 12) {
          return;
        }

        buildOrder.setBinNumber(order, number.substring(0, 6));
      });
    }
  }
}());
