(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .directive('prefill', prefill);

  function prefill() {
    return {
      priority: 100,
      restrict: 'A',
      require: 'ngModel',
      link: link,
      scope: {
        prefill: '=',
      },
    };

    function link(scope, element, attrs, ctrl) {
      scope.$watch('prefill', function (value) {
        if (ctrl.$viewValue || !value) {
          return;
        }

        ctrl.$setViewValue(value);
        ctrl.$render();
      });
    }
  }
}());
