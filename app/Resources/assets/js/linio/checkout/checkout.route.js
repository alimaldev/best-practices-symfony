(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .run(boot);

  boot.$inject = ['$rootScope', '$state', 'router'];
  function boot($rootScope, $state, router) {
    router.configureStates(getStates());
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {
      if (fromState.name === '' && toState.name != 'address') {
        event.preventDefault();
        $state.go('address');
      }
    });
  }

  function getStates() {
    return [
      {
        state: 'address',
        config: {
          url: '{prefix:[^]*}/checkout/shipping-address',
          templateUrl: '/checkout/state/shipping-address',
          controller: 'AddressController',
          controllerAs: 'shippingAddress',
        },
      },
      {
        state: 'options',
        config: {
          url: '{prefix:[^]*}/checkout/shipping-options',
          templateUrl: '/checkout/state/shipping-options',
          controller: 'ShippingOptionsController',
          controllerAs: 'shippingOptions',
        },
      },
      {
        state: 'payment',
        config: {
          url: '{prefix:[^]*}/checkout/payment',
          templateUrl: '/checkout/state/payment',
          controller: 'PaymentController',
          controllerAs: 'checkout',
        },
      },
    ];
  }
})();
