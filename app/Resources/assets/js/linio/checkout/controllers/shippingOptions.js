(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .controller('ShippingOptionsController', ShippingOptionsController);

  ShippingOptionsController.$inject = ['$state', '$scope', 'order', 'geolocation', 'buildOrder', 'ngDialog', 'navigate', 'geohash'];
  function ShippingOptionsController($state, $scope, order, geolocation, buildOrder, ngDialog, navigate, geohash) {
    var viewModel = this;
    viewModel.isReady = false;
    viewModel.currentTab = 'homeDelivery';
    viewModel.order = order;
    viewModel.address = null;
    viewModel.center = null;
    viewModel.addressBook = null;
    viewModel.markers = {};
    viewModel.selectedStore = null;
    viewModel.showAddressBook = showAddressBook;
    viewModel.selectShippingMethod = selectShippingMethod;
    viewModel.selectStore = selectStore;
    viewModel.hasStores = hasStores;
    viewModel.updateMap = updateMap;
    viewModel.switchTab = switchTab;
    viewModel.nextStep = nextStep;
    viewModel.canContinue = canContinue;
    viewModel.removeItem = removeItem;

    $scope.$on('address.selected_address', onSelectedAddress);
    $scope.$on('address.selected_place', onSelectedPlace);
    $scope.$on('address.address_updated', onAddressUpdated);
    $scope.$on('map.selected_store', onSelectedStoreFromMap);
    $scope.$on('map.past_radius', onPastRadius);

    activate();

    function activate() {
      if (order.selectedStore) {
        switchTab('storePickup');
        viewModel.isReady = true;
      } else {
        switchTab('homeDelivery');
      }
    }

    function showAddressBook() {
      viewModel.addressBook = ngDialog.open({
        template: '<address-book selectable="true" type="shipping"></address-book>',
        plain: true,
      });
    }

    function selectShippingMethod(packageId, shippingMethod) {
      buildOrder.setShippingMethod(order, packageId, shippingMethod);
    }

    function selectStore(store) {
      viewModel.center = { lat: store.lat, lng: store.lng };
      buildOrder.setPickupStore(order, store.id);
    }

    function hasStores() {
      return Object.keys(viewModel.markers).length > 0;
    }

    function switchTab(name) {
      viewModel.currentTab = name;
      order.selectedStore = null;

      if (name == 'storePickup') {
        geolocation.getCurrentCoordinates().then(updateMap);
      }

      if (name == 'homeDelivery') {
        order.clearSelectedShippingQuotes();
        buildOrder.getOrderInProgress(order).then(onUpdatedOrder);
      }
    }

    function nextStep() {
      $state.go('payment');
    }

    function canContinue() {
      var isDeliverable = (viewModel.currentTab == 'homeDelivery') ? order.isHomeDeliverable : order.isStoreDeliverable;
      var canContinue = true;

      if (viewModel.currentTab == 'storePickup' && !order.selectedStore) {
        return false;
      }

      for (var packageId in order.packages) {
        canContinue = canContinue && isDeliverable(order.packages[packageId]);
      }

      return canContinue;
    }

    function removeItem(sku) {
      buildOrder.removeItem(order, sku).then(function () {
        if (order.countItems() == 0) {
          navigate.toCart();
        }
      });
    }

    function updateMap(coordinates) {
      var currentGeohash = geohash.encode(coordinates.lat, coordinates.lng);

      buildOrder.setGeohash(order, currentGeohash).then(function () {
        viewModel.center = coordinates;

        for (var storeId in order.pickupStores) {
          var storeData = order.pickupStores[storeId];
          var decodedGeohash = geohash.decode(storeData.geohash);

          storeData.lat = decodedGeohash.latitude;
          storeData.lng = decodedGeohash.longitude;

          viewModel.markers[storeData.id] = storeData;
        }

        onUpdatedOrder();
      });
    }

    function onSelectedAddress(event, selectedAddress) {
      buildOrder.setShippingAddressById(order, selectedAddress.address.id);

      if (viewModel.addressBook) {
        viewModel.addressBook.close();
      }
    }

    function onAddressUpdated(event, selectedAddress) {
      if (selectedAddress.id == viewModel.order.shippingAddress.id) {
        buildOrder.setShippingAddressById(order, selectedAddress.id);
      }
    }

    function onSelectedPlace(event, selectedPlace) {
      updateMap({ lat: selectedPlace.geometry.location.lat(), lng: selectedPlace.geometry.location.lng() });
    }

    function onSelectedStoreFromMap(event, store) {
      buildOrder.setPickupStore(order, store.id);
      viewModel.selectedStore = viewModel.markers[store.id];
    }

    function onPastRadius(event, center) {
      updateMap(center);
    }

    function onUpdatedOrder() {
      if (order.countItems() == 0) {
        return navigate.toCart();
      }

      viewModel.order = order;
      viewModel.isReady = true;
    }
  }
})();
