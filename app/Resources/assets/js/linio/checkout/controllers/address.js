(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .controller('AddressController', AddressController);

  AddressController.$inject = ['$state', '$scope', 'config', 'customer', 'buildOrder', 'order'];
  function AddressController($state, $scope, config, customer, buildOrder, order) {
    var viewModel = this;
    viewModel.isReady = false;

    activate();
    $scope.$on('address.new_address_created', onNewAddressCreated);

    function activate() {
      customer.getAddresses().then(function (addresses) {
        if (Object.keys(addresses).length > 0) {
          nextStep();

          return;
        }

        viewModel.isReady = true;
      });
    }

    function nextStep() {
      if (config.shippingOptionsEnabled) {
        $state.go('options');
      } else {
        $state.go('payment');
      }
    }

    function onNewAddressCreated(event, savedAddress) {
      buildOrder.setShippingAddressById(order, savedAddress.id).then(function () {
        nextStep();
      });
    }
  }
})();
