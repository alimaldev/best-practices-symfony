(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .controller('PaymentController', PaymentController);

  PaymentController.$inject = ['$state', 'navigate', 'ngDialog', 'event', 'customer', 'order', 'buildOrder', 'placeOrder', 'hostedPayment'];
  function PaymentController($state, navigate, ngDialog, event, customer, order, buildOrder, placeOrder, hostedPayment) {
    var viewModel = this;
    viewModel.isReady = false;
    viewModel.isPendingPaymentInfo = false;
    viewModel.isPendingExtraInfo = false;
    viewModel.isPendingInfo = isPendingInfo;
    viewModel.order = order;
    viewModel.addressBook = null;
    viewModel.paymentData = {};
    viewModel.extraFields = {};
    viewModel.removeItem = removeItem;
    viewModel.updateQuantity = updateQuantity;
    viewModel.setPaymentMethod = setPaymentMethod;
    viewModel.selectInstallment = selectInstallment;
    viewModel.canPlaceOrder = canPlaceOrder;
    viewModel.canShowPaymentMethods = canShowPaymentMethods;
    viewModel.canShowCardForm = true;
    viewModel.finish = finish;
    viewModel.showShippingOptions = showShippingOptions;
    viewModel.showAddressBook = showAddressBook;
    viewModel.showCardForm = showCardForm;
    viewModel.useCard = useCard;
    viewModel.refreshPaymentFormListener = null;

    event.listen('address.selected_address', onSelectedAddress);
    event.listen('form.extra_checkout_fields.validation', onExtraCheckoutValidation);
    activate();

    function activate() {
      buildOrder.getOrderInProgress(order).then(function () {
        if (order.countItems() == 0) {
          return navigate.toCart();
        }

        if (!order.shippingAddress) {
          showAddressBook('shipping');
        }

        if (!order.billingAddress) {
          customer.getDefaultBillingAddress().then(function (billingAddress) {
            buildOrder.setBillingAddress(order, billingAddress);
          });
        }

        if (order.paymentMethod) {
          onSelectedPaymentMethod(order.paymentMethod);
        }

        viewModel.isReady = true;
      });
    }

    function isPendingInfo() {
      if (viewModel.isPendingPaymentInfo) {
        return true;
      }

      if (viewModel.isPendingExtraInfo) {
        return true;
      }

      return false;
    }

    function removeItem(sku) {
      buildOrder.removeItem(order, sku).then(function () {
        if (order.countItems() == 0) {
          navigate.toCart();
        }
      });
    }

    function updateQuantity(sku, quantity) {
      buildOrder.updateQuantity(order, sku, quantity);
    }

    function setPaymentMethod(paymentMethod) {
      delete viewModel.paymentData.installments;

      buildOrder.setPaymentMethod(order, paymentMethod).then(function () {
        onSelectedPaymentMethod(paymentMethod);
      });
    }

    function selectInstallment(installmentQuantity) {
      if (!installmentQuantity) {
        return;
      }

      buildOrder.selectInstallmentQuantity(order, installmentQuantity);
    }

    function canPlaceOrder() {
      if (viewModel.isPendingInfo() && canShowPaymentMethods()) {
        return false;
      }

      if (!order.isValid()) {
        return false;
      }

      return true;
    }

    function canShowPaymentMethods() {
      if (order.paymentMethod == 'Zero_Payment') {
        return false;
      }

      if (order.undeliverables.length > 0) {
        return false;
      }

      if (order.wallet && order.wallet.totalPointsUsed > 0 && order.grandTotal == 0) {
        return false;
      }

      return true;
    }

    function finish() {
      if (order.paymentMethod == 'TransbankWebpay_Payment') {
        event.dispatch('spinner.customize', { class: 'loading-overlay-webpay-icon' });
      }

      viewModel.isPendingPaymentInfo = true;
      placeOrder.place(order, viewModel.paymentData, viewModel.extraFields).then(function (completedOrder) {
        if (completedOrder.redirect == null || completedOrder.redirect == undefined) {
          return navigate.toCheckoutSuccess();
        }

        hostedPayment.redirect(completedOrder.redirect.method, completedOrder.redirect.target, completedOrder.redirect.body);
      }).catch(function () {
        viewModel.isPendingPaymentInfo = false;
      });
    }

    function showShippingOptions() {
      $state.go('options');
    }

    function showAddressBook(addressType) {
      viewModel.addressBook = ngDialog.open({
        template: '<address-book selectable="true" type="' + addressType + '"></address-book>',
        plain: true,
      });
    }

    function showCardForm() {
      delete viewModel.paymentData.tokenId;
      order.creditCardBinNumber = null;
      viewModel.canShowCardForm = true;
    }

    function useCard(tokenId, bin) {
      viewModel.paymentData.tokenId = tokenId;
      viewModel.canShowCardForm = false;
      buildOrder.setBinNumber(order, bin);
    }

    function onSelectedPaymentMethod(paymentMethod) {
      if (viewModel.refreshPaymentFormListener) {
        viewModel.isPendingPaymentInfo = false;
        viewModel.refreshPaymentFormListener();
      }

      viewModel.refreshPaymentFormListener = event.listen('form.' + paymentMethod + '.validation', onPaymentFormValidation);

      if (paymentMethod == 'CreditCard' && viewModel.order.savedCards.length > 0) {
        viewModel.canShowCardForm = false;
        viewModel.useCard(viewModel.order.savedCards[0].id, viewModel.order.savedCards[0].firstDigits);
      }
    }

    function onSelectedAddress(event, selectedAddress) {
      if (selectedAddress.type == 'shipping') {
        buildOrder.setShippingAddressById(order, selectedAddress.address.id);
      }

      if (selectedAddress.type == 'billing') {
        buildOrder.setBillingAddress(order, selectedAddress.address);
      }

      if (viewModel.addressBook) {
        viewModel.addressBook.close();
      }
    }

    function onPaymentFormValidation(event, form) {
      viewModel.isPendingPaymentInfo = !form.isValid;
    }

    function onExtraCheckoutValidation(event, form) {
      viewModel.isPendingExtraInfo = !form.isValid;
    }
  }
})();
