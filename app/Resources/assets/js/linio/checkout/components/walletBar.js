(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .component('walletBar', {
      controller: WalletBarController,
      controllerAs: 'wallet',
      templateUrl: 'walletBar',
    });

  WalletBarController.$inject = ['order', 'buildOrder', 'customer'];
  function WalletBarController(order, buildOrder, customer) {
    var viewModel = this;
    viewModel.number = '';
    viewModel.holderName = '';
    viewModel.holderLastname = '';
    viewModel.order = order;
    viewModel.showForm = false;
    viewModel.hasError = false;
    viewModel.activate = activate;
    viewModel.use = use;
    viewModel.remove = remove;

    function activate() {
      if (!viewModel.number) {
        viewModel.hasError = true;
        return;
      }

      customer.registerWallet(viewModel.number, viewModel.holderName, viewModel.holderLastname).then(function () {
        buildOrder.getOrderInProgress(order);
      }).catch(function (error) {
        viewModel.number = '';
        viewModel.holderName = '';
        viewModel.holderLastname = '';
        viewModel.hasError = true;
      });
    }

    function use(points) {
      buildOrder.useWalletPoints(order, points);
    }

    function remove() {
      buildOrder.useWalletPoints(order, 0);
    }
  }
})();
