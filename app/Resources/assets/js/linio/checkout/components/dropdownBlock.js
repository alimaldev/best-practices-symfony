(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .component('dropdownBlock', {
      transclude: true,
      controller: DropdownBlockController,
      controllerAs: 'dropdownBlock',
      templateUrl: 'dropdownBlock',
    });

  function DropdownBlockController() {
    var viewModel = this;

    viewModel.content = '';
    viewModel.loadContent = loadContent;

    function loadContent(contentController) {
      viewModel.content = contentController;
    };
  }

})();
