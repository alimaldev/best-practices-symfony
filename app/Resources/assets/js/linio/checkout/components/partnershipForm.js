(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .component('partnershipForm', {
      require: {
        accordion: '?^uiAccordion',
        dropdownBlock: '?^^dropdownBlock',
      },
      controller: PartnershipFormController,
      controllerAs: 'form',
      templateUrl: 'partnershipForm',
      bindings: {
        partnership: '<',
        title: '@',
      },
    });

  PartnershipFormController.$inject = ['customer', 'order', 'buildOrder'];
  function PartnershipFormController(customer, order, buildOrder) {
    var viewModel = this;
    viewModel.accountNumber = '';
    viewModel.hasError = false;
    viewModel.isActive = false;
    viewModel.logo = '';
    viewModel.type = 'partnership';
    viewModel.remove = remove;
    viewModel.showForm = false;
    viewModel.signup = signup;
    viewModel.$onInit = onInit;

    loadPartnership();

    function loadPartnership() {
      if (viewModel.partnership) {
        viewModel.logo = 'icons-' + viewModel.partnership.code;

        if (viewModel.partnership.accountNumber && viewModel.partnership.accountNumber != '') {
          viewModel.isActive = true;
          viewModel.accountNumber = viewModel.partnership.accountNumber;
        }
      }
    }

    function onInit() {
      if (viewModel.accordion) {
        viewModel.accordion.loadContent(viewModel);
      }

      if (viewModel.dropdownBlock) {
        viewModel.dropdownBlock.loadContent(viewModel);
      }
    };

    function signup() {
      if (!viewModel.accountNumber) {
        viewModel.hasError = true;
        return;
      }

      customer.signupPartnership(viewModel.partnership.code, viewModel.accountNumber).then(function () {
        buildOrder.getOrderInProgress(order).then(function () {
          viewModel.showForm = false;
          viewModel.hasError = false;
          viewModel.accountNumber = viewModel.accountNumber;
          viewModel.isActive = true;
        });
      }).catch(function () {
        viewModel.hasError = true;
        viewModel.accountNumber = '';
      });
    }

    function remove() {
      customer.removePartnership(viewModel.partnership.code).then(function () {
        viewModel.accountNumber = '';
        viewModel.isActive = false;
        buildOrder.getOrderInProgress(order);
      });
    }
  }
})();
