(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .component('couponForm', {
      controller: CouponFormController,
      controllerAs: 'coupon',
      templateUrl: 'couponForm',
    });

  CouponFormController.$inject = ['order', 'buildOrder'];
  function CouponFormController(order, buildOrder) {
    var viewModel = this;
    viewModel.code = '';
    viewModel.order = order;
    viewModel.showForm = false;
    viewModel.hasError = false;
    viewModel.apply = apply;
    viewModel.remove = remove;

    function apply() {
      if (!viewModel.code) {
        viewModel.hasError = true;
        return;
      }

      buildOrder.applyCoupon(order, viewModel.code).then(function () {
        viewModel.showForm = false;
        viewModel.hasError = false;
        viewModel.code = viewModel.code;
      }).catch(function () {
        viewModel.hasError = true;
        viewModel.code = '';
      });
    }

    function remove() {
      buildOrder.removeCoupon(order).then(function () {
        viewModel.code = '';
      });
    }
  }
})();
