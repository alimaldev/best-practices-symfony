(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .component('loyaltyForm', {
      controller: LoyaltyFormController,
      controllerAs: 'loyalty',
      templateUrl: 'loyaltyForm',
    });

  LoyaltyFormController.$inject = ['customer', 'order', 'buildOrder'];
  function LoyaltyFormController(customer, order, buildOrder) {
    var viewModel = this;
    viewModel.id = '';
    viewModel.order = order;
    viewModel.error = false;
    viewModel.showForm = false;
    viewModel.signup = signup;

    function signup() {
      if (!viewModel.id) {
        return;
      }

      customer.signupLoyalty(viewModel.id).then(function () {
        buildOrder.getOrderInProgress(order).then(function () {
          viewModel.showForm = false;
          viewModel.error = false;
          viewModel.id = viewModel.id;
        });
      }).catch(function (error) {
        viewModel.error = error.data.message;
        viewModel.id = '';
      });
    }
  }
})();
