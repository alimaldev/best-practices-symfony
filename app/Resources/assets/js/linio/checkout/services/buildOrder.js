(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .factory('buildOrder', buildOrder);

  buildOrder.$inject = ['$http', '$q', 'logger', 'messenger', 'event', 'localStorageService'];
  function buildOrder($http, $q, logger, messenger, event, localStorageService) {
    return {
      getOrderInProgress: getOrderInProgress,
      removeItem: removeItem,
      updateQuantity: updateQuantity,
      applyCoupon: applyCoupon,
      removeCoupon: removeCoupon,
      setShippingAddress: setShippingAddress,
      setShippingAddressById: setShippingAddressById,
      setBillingAddress: setBillingAddress,
      setShippingMethod: setShippingMethod,
      setPaymentMethod: setPaymentMethod,
      setPickupStore: setPickupStore,
      setBinNumber: setBinNumber,
      setGeohash: setGeohash,
      selectInstallmentQuantity: selectInstallmentQuantity,
      useWalletPoints: useWalletPoints,
      getOrderState: getOrderState,
      onRequestFailure: onRequestFailure,
      parseMessages: parseMessages,
    };

    function getOrderInProgress(order) {
      return $http
        .post('/api/order', { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        localStorageService.set('cart_count', order.countItems());
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function removeItem(order, sku) {
      return $http
        .delete('/api/order/item/' + sku, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        localStorageService.set('cart_count', order.countItems());
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function updateQuantity(order, sku, quantity) {
      return $http
        .put('/api/order/item/' + sku, { quantity: quantity, order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        localStorageService.set('cart_count', order.countItems());
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function applyCoupon(order, code) {
      return $http
        .put('/api/order/coupon/' + code, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function removeCoupon(order) {
      return $http
        .delete('/api/order/coupon', { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setShippingAddress(order, shippingAddress) {
      return $http
        .put('/api/order/shipping-address', { address: shippingAddress, order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setShippingAddressById(order, shippingAddressId) {
      return $http
        .put('/api/order/shipping-address/' + shippingAddressId, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setBillingAddress(order, billingAddress) {
      order.billingAddress = billingAddress;

      return $q.when(billingAddress);
    }

    function setShippingMethod(order, packageId, shippingMethod) {
      return $http
        .put('/api/order/package/' + packageId, { shippingMethod: shippingMethod, order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setPaymentMethod(order, paymentMethod) {
      order.installmentOption = null;

      return $http
        .put('/api/order/payment-method/' + paymentMethod, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setPickupStore(order, storeId) {
      return $http
        .put('/api/order/pickup-store/' + storeId, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setBinNumber(order, binNumber) {
      return $http
        .put('/api/order/bin/' + binNumber, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function setGeohash(order, hash) {
      order.paymentMethod = null;

      return $http
        .put('/api/order/geohash/' + hash, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        localStorageService.set('pickup_stores', response.data.order.pickupStores);
        event.dispatch('order.update');
      }
    }

    function selectInstallmentQuantity(order, number) {
      return $http
        .put('/api/order/installment/' + number, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function useWalletPoints(order, points) {
      return $http
        .put('/api/order/wallet/' + points, { order: getOrderState(order) })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        order.update(response.data.order);
        parseMessages(response.data.messages);
        event.dispatch('order.update');
      }
    }

    function getOrderState(order) {
      var selectedShippingQuotes = order.getSelectedShippingQuotes();
      var selectedShippingOptions = {};

      for (var packageId in selectedShippingQuotes) {
        selectedShippingOptions[packageId] = selectedShippingQuotes[packageId].shippingMethod;
      }

      return {
        pickupStoreId: order.selectedStore ? order.selectedStore.id : null,
        shippingAddressId: order.shippingAddress ? order.shippingAddress.id : null,
        paymentMethod: order.paymentMethod,
        creditCardBinNumber: order.creditCardBinNumber,
        selectedShippingOptions: selectedShippingOptions,
        installments: order.installmentOption ? order.installmentOption.installments : null,
        walletPoints: order.isUsingWallet() ? order.wallet.maxPointsForOrder : null,
      };
    }

    function onRequestFailure(error) {
      logger.error(error.data.message, error);
      messenger.clear();
      messenger.error(error.data.message);
      event.dispatch('order.error', error.data);

      return $q.reject(error);
    }

    function parseMessages(messages) {
      if (messages == undefined) {
        return;
      }

      messenger.clear();

      if (messages.success) {
        messenger.success(messages.success);
      }

      for (var i = 0; i < messages.errors.length; i++) {
        messenger.error(messages.errors[i]);
        event.dispatch('order.error', messages.errors[i]);
      }

      for (var i = 0; i < messages.warnings.length; i++) {
        messenger.warning(messages.warnings[i]);
        event.dispatch('order.error', messages.warnings[i]);
      }
    }
  }
})();
