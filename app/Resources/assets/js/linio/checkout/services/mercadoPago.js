(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .factory('mercadoPago', mercadoPago);

  mercadoPago.$inject = ['$q', '$window'];
  function mercadoPago($q, $window) {
    return {
      tokenize: tokenize,
    };

    function tokenize() {
      var deferred = $q.defer();
      var form = $window.document.getElementsByName('CreditCard')[0];

      Mercadopago.createToken(form, function (status, tokenizeResponse) {
        if (status != 200 && status != 201) {
          deferred.reject(tokenizeResponse.message);

          return;
        }

        Mercadopago.getInstallments({ bin: tokenizeResponse.first_six_digits }, function (status, installmentsResponse) {
          if (status != 200 && status != 201) {
            deferred.reject(installmentsResponse.message);

            return;
          }

          var tokenizedCard = {
            cardToken: tokenizeResponse.id,
            paymentMethodId: installmentsResponse[0].payment_method_id,
            issuerId: null,
          };

          if (installmentsResponse[0].issuer) {
            tokenizedCard.issuerId = installmentsResponse[0].issuer.id;
          }

          deferred.resolve(tokenizedCard);
        });
      });

      return deferred.promise;
    }
  }
})();
