(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .factory('hostedPayment', hostedPayment);

  hostedPayment.$inject = ['$rootScope', '$compile', '$window'];
  function hostedPayment($rootScope, $compile, $window) {
    return {
      redirect: redirect,
      createForm: createForm,
    };

    function redirect(method, target, body) {
      if (method == 'GET') {
        $window.location.href = target;
      } else {
        post(method, target, body);
      }
    }

    function post(method, target, body) {
      var formDirective = angular.element(createForm(method, target, body));
      angular.element(document).find('body').eq(0).append(formDirective);
      $compile(formDirective)($rootScope);
      formDirective[0].submit();
    }

    function createForm(method, target, body) {
      var postForm = '<form method="' + method + '" action="' + target + '">';

      for (var key in body) {
        if (body.hasOwnProperty(key)) {
          postForm += '<input type="hidden" name="' + key + '" value="' + body[key] + '"></input>';
        }
      }

      postForm += '</form>';

      return postForm;
    }
  }
})();
