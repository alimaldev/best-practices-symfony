(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .factory('order', Order);

  function Order() {
    var order = {
      subTotal: 0,
      grandTotal: 0,
      taxAmount: 0,
      shippingAmount: 0,
      shippingDiscountAmount: 0,
      storePickupFee: 0,
      homeDeliveryFee: 0,
      coupon: null,
      geohash: null,
      totalDiscountAmount: 0,
      linioPlusSavedAmount: 0,
      loyaltyPointsAccrued: 0,
      availablePaymentMethods: [],
      paymentMethod: null,
      creditCardBinNumber: null,
      billingAddress: null,
      shippingAddress: null,
      items: [],
      packages: [],
      undeliverables: [],
      pickupStores: [],
      savedCards: [],
      storePickupEnabled: false,
      selectedStore: null,
      wallet: null,
      installmentOption: null,
      installmentOptions: [],
      partnerships: [],
      partnershipDiscount: 0,
      update: update,
      countItems: countItems,
      isEmpty: isEmpty,
      isUndeliverable: isUndeliverable,
      isHomeDeliverable: isHomeDeliverable,
      isStoreDeliverable: isStoreDeliverable,
      isPaymentMethodAllowed: isPaymentMethodAllowed,
      isPaymentMethodAllowedByCoupon: isPaymentMethodAllowedByCoupon,
      isValid: isValid,
      isBillingAddressRequired: isBillingAddressRequired,
      isUsingWallet: isUsingWallet,
      getSelectedStore: getSelectedStore,
      getSelectedPartnership: getSelectedPartnership,
      getSelectedShippingQuote: getSelectedShippingQuote,
      getSelectedShippingQuotes: getSelectedShippingQuotes,
      clearSelectedShippingQuotes: clearSelectedShippingQuotes,
      getSelectedInstallmentOption: getSelectedInstallmentOption,
      getSelectedItems: getSelectedItems,
      isFreeShipping: isFreeShipping,
    };

    return order;

    function update(data) {
      for (var attribute in data) {
        if (data.hasOwnProperty(attribute)) {
          order[attribute] = data[attribute];
        }
      }

      for (var packageId in order.packages) {
        order.packages[packageId].selectedShippingQuote = getSelectedShippingQuote(packageId);

        for (var itemSku in order.packages[packageId].items) {
          var itemQuantityInPackage = order.packages[packageId].items[itemSku];
          order.packages[packageId].items[itemSku] = angular.copy(order.items[itemSku]);
          order.packages[packageId].items[itemSku].quantity = itemQuantityInPackage;
        }
      }

      if (order.highestInstallment) {
        order.highestInstallment = getInstallment(order.highestInstallment);
      }

      if (order.lowestInstallment) {
        order.lowestInstallment = getInstallment(order.lowestInstallment);
      }

      order.installmentOption = getSelectedInstallmentOption() || order.lowestInstallment;
      order.partnership = getSelectedPartnership();
      order.selectedStore = getSelectedStore();
      setDeliveryFees();
    }

    function setDeliveryFees() {
      for (var packageId in order.packages) {
        for (var quoteIndex = 0; quoteIndex < order.packages[packageId].shippingQuotes.length; quoteIndex++) {
          var quote = order.packages[packageId].shippingQuotes[quoteIndex];
          order.storePickupFee = 0;

          if (quote.shippingMethod == 'store') {
            order.storePickupFee += quote.fee;
          }
        }
      }

      if (order.shippingAmount && order.pickupStores.length == 0) {
        order.homeDeliveryFee = order.shippingAmount;
      }
    }

    function isEmpty() {
      return order.countItems() == 0;
    }

    function isUndeliverable(sku) {
      return order.undeliverables.indexOf(sku) !== -1;
    }

    function isHomeDeliverable(shippingPackage) {
      for (var index in shippingPackage.shippingQuotes) {
        if (['regular', 'express', 'electronic'].indexOf(shippingPackage.shippingQuotes[index].shippingMethod) > -1) {
          return true;
        }
      }

      return false;
    }

    function isStoreDeliverable(shippingPackage) {
      for (var index in shippingPackage.shippingQuotes) {
        if (shippingPackage.shippingQuotes[index].shippingMethod == 'store') {
          return true;
        }
      }

      return false;
    }

    function isPaymentMethodAllowed(paymentMethod) {
      if (!order.availablePaymentMethods[paymentMethod]) {
        return false;
      }

      return order.availablePaymentMethods[paymentMethod].allowed;
    }

    function isPaymentMethodAllowedByCoupon(paymentMethod) {
      if (!order.availablePaymentMethods[paymentMethod]) {
        return false;
      }

      return order.availablePaymentMethods[paymentMethod].allowedByCoupon;
    }

    function isValid() {
      if (!order.shippingAddress) {
        return false;
      }

      if (!order.paymentMethod) {
        return false;
      }

      if (order.isBillingAddressRequired() && !order.billingAddress) {
        return false;
      }

      if (!order.packages || Object.keys(order.packages).length == 0) {
        return false;
      }

      if (order.undeliverables.length > 0) {
        return false;
      }

      return true;
    }

    function isBillingAddressRequired() {
      if (!order.availablePaymentMethods[order.paymentMethod]) {
        return false;
      }

      return order.availablePaymentMethods[order.paymentMethod].requireBillingAddress;
    }

    function isUsingWallet() {
      if (!order.wallet) {
        return false;
      }

      if (order.wallet.totalPointsUsed == 0) {
        return false;
      }

      return true;
    }

    function countItems() {
      var total = 0;

      for (var sku in order.items) {
        total += order.items[sku].quantity;
      }

      return total;
    }

    function getInstallment(installmentQuantity) {
      for (var i = 0; i < order.installmentOptions.length; i++) {
        var option = order.installmentOptions[i];

        if (option.installments == installmentQuantity) {
          return option;
        }
      }
    }

    function getSelectedStore() {
      for (var storeId in order.pickupStores) {
        if (order.pickupStores[storeId].selected) {
          return order.pickupStores[storeId];
        }
      }
    }

    function getSelectedPartnership() {
      for (var i = 0; i < order.partnerships.length; i++) {
        var partnership = order.partnerships[i];

        if (partnership.accountNumber && partnership.active) {
          return partnership;
        }
      }
    }

    function getSelectedShippingQuote(packageId) {
      if (!order.packages[packageId]) {
        return;
      }

      for (var quoteIndex = 0; quoteIndex < order.packages[packageId].shippingQuotes.length; quoteIndex++) {
        var quote = order.packages[packageId].shippingQuotes[quoteIndex];

        if (quote.selected) {
          return quote;
        }
      }
    }

    function getSelectedShippingQuotes() {
      var shippingQuotes = {};

      for (var packageId in order.packages) {
        var selectedQuote = getSelectedShippingQuote(packageId);

        if (selectedQuote) {
          shippingQuotes[packageId] = selectedQuote;
        }
      }

      return shippingQuotes;
    }

    function clearSelectedShippingQuotes() {
      for (var packageId in order.packages) {
        var selectedQuote = getSelectedShippingQuote(packageId);

        if (selectedQuote) {
          selectedQuote.selected = false;
        }
      }
    }

    function getSelectedInstallmentOption() {
      for (var i = 0; i < order.installmentOptions.length; i++) {
        var option = order.installmentOptions[i];

        if (option.selected) {
          return option;
        }
      }
    }

    function getSelectedItems() {
      var selectedItems = {};

      for (var sku in order.items) {
        selectedItems[sku] = order.items[sku].quantity;
      }

      return selectedItems;
    }

    function isFreeShipping(sku) {
      var isFreeShipping = false;

      for (var packageId in order.packages) {
        for (var itemSku in order.packages[packageId].items) {
          if (sku != itemSku) {
            continue;
          }

          var selectedQuote = getSelectedShippingQuote(packageId);

          if (!selectedQuote) {
            continue;
          }

          if (selectedQuote.freeShipping) {
            isFreeShipping = true;
          }

          if (isFreeShipping && !selectedQuote.freeShipping) {
            return false;
          }
        }
      }

      return isFreeShipping;
    }
  }
})();
