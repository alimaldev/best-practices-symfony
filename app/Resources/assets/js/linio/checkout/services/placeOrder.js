(function () {
  'use strict';

  angular
    .module('linio.checkout')
    .factory('placeOrder', placeOrder);

  placeOrder.$inject = ['$http', '$q', 'logger', 'messenger', 'localStorageService', 'mercadoPago'];
  function placeOrder($http, $q, logger, messenger, localStorageService, mercadoPago) {
    return {
      place: place,
      isMercadoPago: isMercadoPago,
    };

    function place(order, paymentData, extraFields) {
      if (isMercadoPago(order)) {
        return mercadoPago.tokenize().then(function (tokenizedCard) {
          return sendOrderRequest(order, tokenizedCard, extraFields);
        });
      }

      return sendOrderRequest(order, paymentData, extraFields);
    }

    function sendOrderRequest(order, paymentData, extraFields) {
      if (order.billingAddress) {
        paymentData.billingFirstName = order.billingAddress.firstName;
        paymentData.billingLastName = order.billingAddress.lastName;
      }

      var orderRequest = {
        grandTotal: order.grandTotal,
        shippingAddress: order.shippingAddress,
        billingAddress: order.billingAddress,
        installments: order.getSelectedInstallmentOption() ? order.getSelectedInstallmentOption().installments : null,
        packages: order.getSelectedShippingQuotes(),
        items: order.getSelectedItems(),
        coupon: order.coupon ? order.coupon.code : null,
        walletPoints: order.isUsingWallet() ? order.wallet.maxPointsForOrder : null,
        pickupStoreId: order.selectedStore ? order.selectedStore.id : null,
        paymentMethod: order.paymentMethod,
        dependentPaymentMethod: order.getSelectedInstallmentOption() ? order.getSelectedInstallmentOption().paymentMethodName : null,
        creditCardBinNumber: order.creditCardBinNumber,
        paymentData: paymentData,
        extraFields: extraFields,
      };

      return $http
        .post('/api/order/place', orderRequest)
        .then(onRequestSuccess)
        .catch(onRequestFailure);
    }

    function isMercadoPago(order) {
      if (order.paymentMethod != 'CreditCard') {
        return false;
      }

      var selectedInstallment = order.getSelectedInstallmentOption();

      if (!selectedInstallment) {
        return false;
      }

      if (selectedInstallment && selectedInstallment.paymentMethodName != 'MercadoPago_CreditCard_Api') {
        return false;
      }

      return true;
    }

    function onRequestSuccess(response) {
      localStorageService.remove('cart_count');

      return response.data;
    }

    function onRequestFailure(error) {
      var message = (error.data.context) ? error.data.context.message : error.data.message;
      logger.error(message, error);
      messenger.clear();
      messenger.error(message);

      return $q.reject(error);
    }
  }
})();
