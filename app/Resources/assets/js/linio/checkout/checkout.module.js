(function () {
  'use strict';

  angular.module('linio.checkout', [
    'linio.core',
    'linio.customer',
    'ui.widgets',
    'ngDialog',
    'LocalStorageModule',
    'angular-geohash',
  ]);
})();
