(function () {
  'use strict';

  angular.module('linio.cart', [
    'linio.core',
    'linio.checkout',
    'linio.address',
    'ui.widgets',
  ]);
})();
