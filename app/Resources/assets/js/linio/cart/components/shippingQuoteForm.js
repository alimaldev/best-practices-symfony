(function () {
  'use strict';

  angular
    .module('linio.cart')
    .component('shippingQuoteForm', {
      controller: ShippingQuoteFormController,
      controllerAs: 'addressForm',
      templateUrl: '/ng/shipping-quote-form',
    });

  ShippingQuoteFormController.$inject = ['resolveLocation', 'order', 'buildOrder', 'event'];
  function ShippingQuoteFormController(resolveLocation, order, buildOrder, event) {
    var viewModel = this;
    viewModel.input = null;
    viewModel.regions = [];
    viewModel.municipalities = [];
    viewModel.cities = [];
    viewModel.loadRegions = loadRegions;
    viewModel.loadMunicipalities = loadMunicipalities;
    viewModel.loadCities = loadCities;
    viewModel.quote = quote;

    function loadRegions() {
      resolveLocation.getRegions(viewModel.input).then(function (regionData) {
        viewModel.regions = regionData;
      });
    }

    function loadMunicipalities() {
      resolveLocation.getMunicipalities(viewModel.input).then(function (municipalityData) {
        viewModel.municipalities = municipalityData;
      });
    }

    function loadCities() {
      resolveLocation.getCities(viewModel.input).then(function (cityData) {
        viewModel.cities = cityData;
      });
    }

    function quote() {
      buildOrder.setShippingAddress(order, viewModel.input).then(function () {
        event.dispatch('address.quote_set');
      });
    }
  }
})();
