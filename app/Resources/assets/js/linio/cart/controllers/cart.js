(function () {
  'use strict';

  angular
    .module('linio.cart')
    .controller('CartController', CartController);

  CartController.$inject = ['ngDialog', 'order', 'buildOrder', 'event'];
  function CartController(ngDialog, order, buildOrder, event) {
    var viewModel = this;
    viewModel.isReady = false;
    viewModel.showShippingQuoteForm = false;
    viewModel.data = order;
    viewModel.removeItem = removeItem;
    viewModel.updateQuantity = updateQuantity;
    viewModel.toggleShippingQuoteForm = toggleShippingQuoteForm;
    viewModel.showLoginModal = showLoginModal;

    event.listen('address.quote_set', onShippingQuoteSet);
    activate();

    function activate() {
      buildOrder.getOrderInProgress(order).then(function () {
        viewModel.isReady = true;
      });
    }

    function removeItem(sku) {
      buildOrder.removeItem(order, sku);
    }

    function updateQuantity(sku, quantity) {
      buildOrder.updateQuantity(order, sku, quantity);
    }

    function toggleShippingQuoteForm() {
      viewModel.showShippingQuoteForm = !viewModel.showShippingQuoteForm;
    }

    function showLoginModal() {
      ngDialog.open({
        template: 'loginModal',
        className: 'ngdialog-login ngdialog-login-overlay',
      });
    }

    function onShippingQuoteSet() {
      viewModel.showShippingQuoteForm = false;
    }
  }
})();
