(function () {
  'use strict';

  angular
    .module('linio.filters')
    .filter('range', range);

  function range() {
    return function (range, start, end) {
      start = parseInt(start);
      end = parseInt(end);

      for (var number = start; number <= end; number++) {
        range.push(number);
      }

      return range;
    };
  }
}());
