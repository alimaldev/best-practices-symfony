(function () {
  'use strict';

  angular.module('linio.filters', [
    'linio.core',
  ]);
})();
