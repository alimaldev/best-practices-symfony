(function () {
  'use strict';

  angular
    .module('linio.catalog')
    .component('recommendations', {
      controller: RecommendationsController,
      controllerAs: 'recommendations',
      templateUrl: 'recommendations',
      bindings: {
        section: '@',
        sku: '@?',
      },
    });

  RecommendationsController.$inject = ['$scope', 'search', 'config', 'jetloreFeedService'];
  function RecommendationsController($scope, search, config, jetloreFeedService) {
    var viewModel = this;
    viewModel.sections = {};
    viewModel.$onInit = onInit;

    $scope.$on('load-recommendations', function (e, type) {
      if (type == 'jetlore') {
        jetloreFeedService
          .getLayout(viewModel.section)
          .then(function (response) {
            viewModel.sections = response;
          });
      } else {
        search.getFeedList('homepage').then(setRecommendationSections);
      }
    });

    function onInit() {
      if (!config.jetloreFeedEnabled) {
        search.getFeedList(viewModel.section, viewModel.sku).then(setRecommendationSections);
      }
    };

    function setRecommendationSections(sections) {
      viewModel.sections = sections;

      getRecommendations();
    }

    function getRecommendations() {
      angular.forEach(viewModel.sections, function (section) {
        search.getRecommendations(section)
          .then(function (items) {
            section.items = items;
          });
      });
    }
  }
})();
