(function () {
  angular
    .module('linio.catalog')
    .factory('search', search);

  search.$inject = ['$http', '$q', '$cookies', 'config', 'logger'];
  function search($http, $q, $cookies, config, logger) {
    return {
      getFeedList: getFeedList,
      getRecommendations: getRecommendations,
    };

    function getFeedList(section, sku) {
      var userIdCookie = 'linio_id';
      var userId = $cookies.get(userIdCookie);
      var url = config.recommendationsUrl + section + '?key=' + config.feedKey;

      if (sku) {
        url += '&sku=' + sku;
      }

      if (userId) {
        url += '&uuid=' + userId;
      }

      return $http
        .get(url, { withCredentials: true })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        var sections = (response.data.status == 'ok' && response.data.feeds) ? response.data.feeds : [];

        return sections;
      }
    }

    function getRecommendations(section) {
      return $http
        .get(section.url, { withCredentials: true })
        .then(onRequestSuccess)
        .catch(onRequestFailure);

      function onRequestSuccess(response) {
        var items = parseRecommendationItems(response.data.items, section.type);

        return items;
      }

      function parseRecommendationItems(items, type) {
        switch (type) {
          case 'products':
            return parseProducts(items);
          case 'categories':
            return parseCategories(items);
        }
      }

      function parseProducts(products) {
        var parsedProducts = [];

        angular.forEach(products, function (product) {
          var parsedProduct = {};

          parsedProduct.title = product.title;
          parsedProduct.brand = product.brand.name;
          parsedProduct.image = cleanUrl(product.images[0]);
          parsedProduct.url = '//' + product.url;

          var price = product.price;

          if (product.price.previous) {
            parsedProduct.percentageOff = 100 - parseInt(price.current * 100 / price.previous);
            parsedProduct.originalPrice = price.previous;
            parsedProduct.price = price.current;
          } else {
            parsedProduct.price = price.current;
          }

          parsedProducts.push(parsedProduct);
        });

        return parsedProducts;
      }

      function parseCategories(categories) {
        var parsedCategories = [];

        angular.forEach(categories, function (category) {
          var parsedCategory = {};

          parsedCategory.title = category.title;
          parsedCategory.url = '//' + category.url;
          parsedCategory.image = cleanUrl(category.images[0]);

          parsedCategories.push(parsedCategory);
        });

        return parsedCategories;
      }

      function cleanUrl(url) {
        return url.replace('http:', 'https:');
      }
    }

    function onRequestFailure(error) {
      logger.error(error.data.message, error);

      return $q.reject(error);
    }
  }
})();
