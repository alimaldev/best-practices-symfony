(function () {
  'use strict';

  angular.module('linio.catalog', [
    'linio.core',
    'ngCookies',
  ]);
})();
