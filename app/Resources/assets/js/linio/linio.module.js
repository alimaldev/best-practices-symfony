(function () {
  'use strict';

  angular.module('linio', [
    'linio.core',
    'linio.address',
    'linio.catalog',
    'linio.cart',
    'linio.checkout',
    'linio.customer',
    'linio.filters',
    'linio.account',
    'ui.widgets',
    'linioApp',
  ]);
})();
