var google = {
  maps: {
    places: {
      Autocomplete: function () {},
    },

    Circle: function () {},

    Map: function () {},

    LatLngBounds: function () {},

    Marker: function () {},

    LatLng: function () {},

    Geocoder: function () {},

    GeocoderStatus: {
      OK: 'OK',
    },
  },
};

var Mercadopago = {
  createToken: function () {},

  getInstallments: function () {},
};

var stubs = (function () {
  return {
    getOrder: getOrder,
    getValidOrder: getValidOrder,
    getOrderRequest: getOrderRequest,
    getOrderState: getOrderState,
    getAddresses: getAddresses,
    getProductRecommendations: getProductRecommendations,
    getCategoryRecommendations: getCategoryRecommendations,
    getCards: getCards,
    subscribe: subscribe,
    alreadySubscribed: alreadySubscribed,
  };

  function getOrder() {
    return {
      order: {
        customer: {
          loyaltyProgram: null,
          loyaltyId: null,
          nationalRegistrationNumber: '123',
          taxIdentificationNumber: '456',
        },
        subTotal: 177.52,
        grandTotal: 377.52,
        taxAmount: 28.34,
        shippingAmount: 200,
        shippingDiscountAmount: 0,
        coupon: {
          code: 'VALID_1',
          discount: 20,
        },
        totalDiscountAmount: 0,
        linioPlusSavedAmount: 0,
        loyaltyPointsAccrued: 0,
        availablePaymentMethods: {
          CashOnDelivery: {
            allowed: true,
            allowedByCoupon: false,
            requireBillingAddress: false,
          },
          DebitCard: {
            allowed: true,
            allowedByCoupon: true,
            requireBillingAddress: true,
          },
        },
        paymentMethod: null,
        billingAddress: null,
        shippingAddress: null,
        items: {
          'SKU0001-001': {
            sku: 'SKU0001-001',
            slug: null,
            name: 'Config_1',
            description: null,
            variation: null,
            variationType: null,
            seller: null,
            percentageOff: 0,
            unitPrice: 98.76,
            originalPrice: 123.45,
            paidPrice: 177.52,
            taxAmount: 28.34,
            shippingAmount: 200,
            quantity: 2,
            availableQuantity: 5,
            maxItemsToSell: 5,
            image: '',
            linioPlusLevel: 0,
            linioPlusEnabledQuantity: 0,
            deliveredByChristmas: false,
            minimumDeliveryDate: null,
            freeShipping: false,
            imported: null,
          },
        },
        packages: {
          1: {
            items: {
              'SKU0001-001': 2,
            },
            shippingQuotes: [
              {
                id: 0,
                shippingMethod: 'regular',
                fee: 100,
                estimatedDeliveryDate: '2016-03-16 00:00:00',
                selected: false,
              },
              {
                id: 1,
                shippingMethod: 'express',
                fee: 200,
                estimatedDeliveryDate: '2016-03-17 00:00:00',
                selected: true,
              },
              {
                id: 2,
                shippingMethod: 'store',
                fee: 50,
                estimatedDeliveryDate: '2016-03-18 00:00:00',
                selected: false,
              },
            ],
          },
        },
        undeliverables: ['SKU0001-002'],
        wallet: {
          totalDiscount: 0,
          totalPointsUsed: 0,
          pointsBalance: 1000,
          maxPointsForOrder: 0,
          shippingDiscount: 0,
          pointsUsedForShipping: 0,
          conversionRate: 0,
        },
        installmentOptions: [
          {
            installments: 1,
            interestFee: 0,
            total: 377.52,
            totalInterest: 377.52,
            amount: 377.52,
            selected: true,
            paymentMethodName: null,
          },
        ],
        highestInstallment: 1,
        lowestInstallment: 1,
        partnershipDiscount: 0,
        partnerships: [
          {
            accountNumber: '123456',
            appliedDiscount: true,
            code: 'foobar',
            description: 'Obtén un 15% en todas las compras que realices en Linio.',
            level: null,
            name: 'Foobar!',
            active: true,
          },
        ],
        pickupStores: {
          2: {
            id: 2,
            name: 'Pickup Store 2',
            description: 'Blah blah blah',
            postcode: '33020',
            geohash: 'dhwupu4907e',
            region: 'Region',
            municipality: 'Municipality',
            city: 'City',
            subLocality: 'Sublocality',
            neighborhood: 'Neighborhood',
            line1: 'AddressLine1',
            line2: 'AddressLine2',
            apartment: 'Apartment',
            referencePoint: 'ReferencePoint',
            network: {
              id: 1,
              name: 'NetworkName',
            },
            selected: true,
          },
        },
      },
      messages: {
        errors: [],
        warnings: [],
        success: null,
      },
    };
  }

  function getValidOrder() {
    var order = getOrder();
    order.order.paymentMethod = 'CashOnDelivery';
    order.order.undeliverables = [];

    return order;
  }

  function getOrderRequest() {
    return {
      grandTotal: 377.52,
      shippingAddress: null,
      billingAddress: null,
      installments: 1,
      packages: {
        1: {
          id: 1,
          shippingMethod: 'express',
          fee: 200,
          estimatedDeliveryDate: '2016-03-17 00:00:00',
          selected: true,
        },
      },
      items: {
        'SKU0001-001': 2,
      },
      coupon: 'VALID_1',
      pickupStoreId: 2,
      walletPoints: null,
      paymentMethod: null,
      dependentPaymentMethod: null,
      creditCardBinNumber: null,
      paymentData: null,
      extraFields: null,
    };
  }

  function getOrderState() {
    return {
      pickupStoreId: null,
      shippingAddressId: null,
      paymentMethod: null,
      creditCardBinNumber: null,
      selectedShippingOptions: {},
      installments: null,
      walletPoints: null,
    };
  }

  function getAddresses() {
    return {
      1: {
        firstName: 'Adolfinho',
        lastName: 'Mequetreque',
        address1: 'test',
        streetNumber: 'test',
        address2: null,
        city: 'CALUMA',
        municipality: 'CALUMA',
        neighborhood: null,
        region: 'BOLIVAR',
        fkCountry: 63,
        phone: null,
        prefix: null,
        mobilePhone: 823898932983,
        postCode: 100025,
        fkCustomerAddressRegion: null,
        defaultBilling: false,
        defaultShipping: true,
        apartment: 'test',
        additionalInformation: null,
        company: null,
        betweenStreet1: null,
        betweenStreet2: null,
        taxIdentificationNumber: null,
        regime: null,
        idSalesOrderAddress: null,
        lote: null,
        urba: null,
        dirType: null,
        depto: null,
      },
      2: {
        firstName: 'Barfoo',
        lastName: 'Foobar',
        address1: 'test',
        streetNumber: 'test',
        address2: null,
        city: 'CALUMA',
        municipality: 'CALUMA',
        neighborhood: null,
        region: 'BOLIVAR',
        fkCountry: 63,
        phone: null,
        prefix: null,
        mobilePhone: 823898932983,
        postCode: '',
        fkCustomerAddressRegion: null,
        defaultBilling: true,
        defaultShipping: false,
        apartment: 'test',
        additionalInformation: null,
        company: null,
        betweenStreet1: null,
        betweenStreet2: null,
        taxIdentificationNumber: null,
        regime: null,
        idSalesOrderAddress: null,
        lote: null,
        urba: null,
        dirType: null,
        depto: null,
      },
    };
  }

  function getProductRecommendations() {
    return [
      {
        id: 'LA140SP83NMQLAEC-12185',
        title: 'Bicicleta Lattizero',
        brand: {
          name: 'Lattizero',
        },
        attributes: {},
        price: {
          current: 209.93,
          previous: 246.98,
        },
        stock_count: 1,
        group_id: 'LA140SP83NMQLAEC',
        skus: [
          'LA140SP83NMQLAEC',
          'LA140SP83NMQLAEC-12185',
        ],
        categories: [
          [
            {
              name: 'deportes',
            },
            {
              name: 'ciclismo',
            },
            {
              name: 'bicicletas',
            },
          ],
        ],
        url: 'www.linio.com.ec/p/bicicleta-lattizero-mtb-rj-rim-26-talla-17-unisex-roja-osgkum',
        images: [
          'http://media.linio.com.ec/p/lattizero-2480-6119-1-product.jpg',
        ],
        version: '1463755616869769284',
      },
      {
        id: 'SA007EL66PLHLAEC-13982',
        title: 'Galaxy J1 Ace Duos Sm',
        brand: {
          name: 'Samsung',
        },
        attributes: {},
        price: {
          current: 139.99,
        },
        stock_count: 6,
        group_id: 'SA007EL66PLHLAEC',
        skus: [
          'SA007EL66PLHLAEC-13982',
          'SA007EL66PLHLAEC',
        ],
        categories: [
          [
            {
              name: 'celulares y tablets',
            },
            {
              name: 'celulares y smartphones',
            },
            {
              name: 'desbloqueados',
            },
          ],
        ],
        url: 'www.linio.com.ec/p/samsung-galaxy-j1-ace-duos-sm-j110m-4g-lte-8gb-tpvqbs',
        images: [
          'http://media.linio.com.ec/p/samsung-7378-33401-1-product.jpg',
        ],
        version: '1463755616869769284',
      },
    ];
  }

  function getCategoryRecommendations() {
    return [
      {
        id: 'desbloqueados',
        title: 'desbloqueados',
        url: 'www.linio.com.ec/c/celulares-y-smartphones/desbloqueados',
        images: [
          'http://media.linio.com.ec/p/samsung-7384-09848-1-product.jpg',
        ],
      },
      {
        id: 'celulares y smartphones',
        title: 'celulares y smartphones',
        url: 'www.linio.com.ec/c/celulares-y-tablets/celulares-y-smartphones',
        images: [
          'http://media.linio.com.ec/p/samsung-5335-46428-1-product.jpg',
        ],
      },
    ];
  }

  function getCards() {
    return [
      {
        id: 155942,
        brand: 'AMEX',
        cardholderName: 'l*** j**** p**** m*******',
        lastDigits: '1008',
        firstDigits: '376678',
        paymentMethod: null,
      },
    ];
  }

  function subscribe() {
    return {
      status: 'success',
      email: 'email@domain.com',
      coupon: 'NLgYXQy5',
    };
  }

  function alreadySubscribed() {
    return {
      status: 'already_subscribed',
    };
  }
})();
