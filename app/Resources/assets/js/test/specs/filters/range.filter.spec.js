describe('range filter', function () {
  beforeEach(function () {
    bard.appModule('linio.filters');
    bard.inject('$filter');
  });

  it('should be created successfully', function () {
    expect($filter('range')).to.exist;
  });

  it('should create range of numbers', function () {
    expect($filter('range')([], 1, 6)).to.be.deep.equals([1, 2, 3, 4, 5, 6]);
  });

  it('should create range of numbers with strings', function () {
    expect($filter('range')([], '1', '6')).to.be.deep.equals([1, 2, 3, 4, 5, 6]);
  });
});
