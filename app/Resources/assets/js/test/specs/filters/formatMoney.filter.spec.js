describe('formatMoney filter', function () {
  beforeEach(function () {
    bard.appModule('linio.filters');
    bard.inject('$filter', 'config');
  });

  it('should be created successfully', function () {
    expect($filter('formatMoney')).to.exist;
  });

  it('should format money amount with defaults', function () {
    expect($filter('formatMoney')(54654)).to.be.equal('$54,654.00');
    expect($filter('formatMoney')(42456.63)).to.be.equal('$42,456.63');
  });

  it('should format negative money amount', function () {
    expect($filter('formatMoney')(-1)).to.be.equal('$-1.00');
    expect($filter('formatMoney')(-42456.613)).to.be.equal('$-42,456.61');
  });

  it('should not format NaN', function () {
    expect($filter('formatMoney')({})).to.be.equal('');
  });

  it('should format money amount with custom configuration', function () {
    config.decimalPrecision = 2;
    config.currencySymbol = '@';
    config.thousandsSeparator = '.';
    config.decimalPoint = ',';
    expect($filter('formatMoney')(54654)).to.be.equal('@54.654,00');
  });

  it('should format money amount without decimal precision', function () {
    config.decimalPrecision = null;
    config.currencySymbol = '@';
    config.thousandsSeparator = '.';
    config.decimalPoint = ',';
    expect($filter('formatMoney')(54654)).to.be.equal('@54.654');
  });
});
