describe('uiMessenger', function () {
  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$rootScope', '$compile', 'messenger');

    scope = $rootScope;
    element = $compile('<ui-messenger></ui-messenger>')(scope);
    scope.$digest();
  });

  it('renders message when messenger reports', function () {
    messenger.error('foobar');
    element.scope().$digest();

    var messageDiv = element.find('div').find('div');
    expect(messageDiv).to.exist;
    expect(messageDiv.hasClass('alert-error')).to.be.true;
    expect(messageDiv.text()).to.be.equal('foobar');
  });

  it('renders nothing when there are no messenger reports', function () {
    element.scope().$digest();

    var messageDiv = element.find('div');
    expect(messageDiv).to.be.empty;
  });
});
