describe('uiAccordion', function () {
  var element;

  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$rootScope', '$compile', 'event');

    var mockTemplate = '<script type="text/ng-template" id="accordion"></script>';
    element = '<ui-accordion title="Welcome" logo="icons-logo" class-name="new-class"> <p>Lorem ipsum</p></ui-accordion>';
    scope = $rootScope;
    $compile(mockTemplate)(scope);
    element = $compile(element)(scope);
    scope.$digest();
    controller = element.controller('uiAccordion');
  });

  it('should set title and logo', function () {
    expect(controller.title).to.be.equal('Welcome');
    expect(controller.logo).to.be.equal('icons-logo');
    expect(controller.className).to.be.equal('new-class');
    expect(controller.expanded).to.be.false;
  });

  it('should replace title and logo', function () {
    var tempScope = { title:'New Title', logo: 'New Logo' };
    controller.loadContent(tempScope);
    expect(controller.title).to.be.equal('New Title');
    expect(controller.logo).to.be.equal('New Logo');
    expect(controller.content).to.exists;
  });

  it('should show content pane', function () {
    controller.switchPane();
    expect(controller.expanded).to.be.true;
  });

  it('should show and hide content pane', function () {
    controller.switchPane();
    expect(controller.expanded).to.be.true;
    controller.switchPane();
    expect(controller.expanded).to.be.false;
  });
});
