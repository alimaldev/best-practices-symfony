describe('uiSpinner', function () {
  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$http', '$httpBackend', '$rootScope', '$compile');

    scope = $rootScope;
    element = $compile('<ui-spinner></ui-spinner>')(scope);
    scope.$digest();
  });

  it('shows spinner when HTTP requests are sent', function () {
    $httpBackend.when('GET', '/foobar').respond(200);
    $http.get('/foobar');
    scope.$digest();

    expect(element).to.have.length(1);
    expect(element.hasClass('ng-hide')).to.be.false;

    $httpBackend.flush();
  });

  it('does not show spinner when when requests is configured', function () {
    $httpBackend.when('GET', '/prefix/not-me', { spinner: false }).respond(200);
    $http.get('/prefix/not-me', { spinner: false });
    scope.$digest();

    expect(element).to.have.length(1);
    expect(element.hasClass('ng-hide')).to.be.true;

    $httpBackend.flush();
  });

  it('does not show spinner when there are no HTTP requests', function () {
    expect(element).to.have.length(1);
    expect(element.hasClass('ng-hide')).to.be.true;
  });

  it('shows spinner when asked', function () {
    $rootScope.$broadcast('spinner.spin');
    scope.$digest();

    expect(element).to.have.length(1);
    expect(element.hasClass('ng-hide')).to.be.false;
    expect(scope.spinningEvent).to.be.true;
  });

  it('stops spinner when asked', function () {
    $rootScope.$broadcast('spinner.stop');
    scope.$digest();

    expect(element).to.have.length(1);
    expect(element.hasClass('ng-hide')).to.be.true;
    expect(scope.spinningEvent).to.be.false;
  });

  it('does not spin if there is a spinning event active', function () {
    $httpBackend.when('GET', '/foobar').respond(200);
    element.scope().spinningEvent = true;
    $http.get('/foobar');
    scope.$digest();

    expect(element).to.have.length(1);
    expect(element.hasClass('ng-hide')).to.be.true;

    $httpBackend.flush();
  });

  it('customizes spinner', function () {
    $rootScope.$broadcast('spinner.customize', { class: 'foobar' });
    scope.$digest();

    expect(element).to.have.length(1);
    expect(element.hasClass('foobar')).to.be.true;
    expect(scope.spinningEvent).to.be.false;
  });
});
