describe('Ouibounce component', function () {
  var component;
  var scope;
  var ngDialogMock;

  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$rootScope', '$componentController', 'event', '$compile', 'ngDialog');
    scope = $rootScope.$new();
  });

  it('should receive correct parameters', function () {
    component = $componentController('ouiBounce', { $scope: scope });

    var componentParameters = {
      aggressive: false,
      sensitivity: 0,
      cookieExpire: 2,
      timer: 10000,
      delay: 500,
    };

    component.aggressive = componentParameters.aggressive;
    component.sensitivity = componentParameters.sensitivity;
    component.cookieExpire = componentParameters.cookieExpire;
    component.timer = componentParameters.timer;
    component.delay = componentParameters.delay;

    expect(component.aggressive).to.equal(componentParameters.aggressive);
    expect(component.sensitivity).to.be.equal(componentParameters.sensitivity);
    expect(component.cookieExpire).to.be.equal(componentParameters.cookieExpire);
    expect(component.timer).to.be.equal(componentParameters.timer);
    expect(component.delay).to.be.equal(componentParameters.delay);
  });

  it('should display newsletter', function () {
    component = $componentController('ouiBounce', { $scope: scope });

    ngDialogMock = sinon.mock(ngDialog);
    ngDialogMock.expects('open').once().withArgs({
      template: 'ouiBounceModal',
      className: 'modal-ouibounce-container',
      controller: 'OuiBounceModalController',
      controllerAs: 'ouiBounceModal',
    }).returns('foo');

    expect(component.bounceExchangeModal).to.be.null;
    component.show();
    $rootScope.$apply();
    expect(component.bounceExchangeModal).to.be.equal('foo');
  });
});
