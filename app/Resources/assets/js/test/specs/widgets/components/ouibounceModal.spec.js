describe('OuibounceModal controller', function () {
  var controller;

  beforeEach(function () {
    bard.appModule('linio.customer');
    bard.inject('$controller', '$q', '$rootScope', 'newsletter', 'ngDialog');
  });

  bard.verifyNoOutstandingHttpRequests();

  it('should handle subscribe event success', function () {
    ngDialogMock = sinon.mock(ngDialog);
    sinon.stub(newsletter, 'subscribe').returns($q.when(stubs.subscribe()));
    controller = $controller('OuiBounceModalController', { $scope: $rootScope });
    controller.subscribe();
    ngDialogMock.expects('closeAll').once();
  });

  it('should handle subscribe event already-subscribed', function () {
    ngDialogMock = sinon.mock(ngDialog);
    sinon.stub(newsletter, 'subscribe').returns($q.when(stubs.alreadySubscribed()));
    controller = $controller('OuiBounceModalController', { $scope: $rootScope });
    controller.subscribe();
    $rootScope.$apply();
    ngDialogMock.expects('closeAll').never();
    expect(controller.error).to.not.equal('');
  });
});
