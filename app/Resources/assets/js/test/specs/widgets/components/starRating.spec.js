describe('starRaiting component', function () {
  var component;
  var scope;
  var ngModel;

  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$rootScope', '$componentController', 'event', '$compile');
    scope = $rootScope.$new();
    ngModel = {
      $setViewValue: function () {},
    };
  });

  it('should assign the name bindings to the stars object', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.stars = 5;
    expect(component.stars).to.be.equal(5);
  });

  it('should be attached to the scope', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.stars = 5;
    expect(scope.star).to.be.equal(component);
  });

  it('should add n star elements', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.stars = 5;
    component.$onInit();
    expect(component.starsObject).to.deep.equal({ 0: false, 1: false, 2: false, 3: false, 4: false });
  });

  it('should set true the selected element', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.stars = 5;
    component.setActiveStars(2);
    expect(component.starsObject[2]).to.equal(true);
  });

  it('should break setActiveStars if selectedStar is defined', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.stars = 5;

    component.selectedStar = 2;
    component.setActiveStars(2);
    expect(component.starsObject[2]).to.not.exist;
  });

  it('should set star rating', function () {
    var ngModelMock = sinon.mock(ngModel);
    ngModelMock.expects('$setViewValue').once().withArgs(3);

    component = $componentController('starRatings', { $scope: scope });
    component.ngModel = ngModel;
    component.stars = 5;
    component.setRating(2);

    ngModelMock.verify();
  });

  it('should clear temporary stars', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.ngModel = ngModel;
    component.stars = 5;
    component.setRating(2);
    component.clearTempActiveStars();
  });

  it('should break clearTempActiveStars if selectedStar is defined', function () {
    component = $componentController('starRatings', { $scope: scope });
    component.stars = 5;
    component.selectedStar = 2;
    component.clearTempActiveStars();
    expect(component.starsObject[2]).to.not.exist;
  });
});
