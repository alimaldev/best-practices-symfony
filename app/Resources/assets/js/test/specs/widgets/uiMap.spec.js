describe('uiMap', function () {
  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$rootScope', '$compile', 'event');
  });

  it('should render map with defaults', function () {
    var mapSetCenter = sinon.spy();
    var map = { setCenter: mapSetCenter, getCenter: sinon.spy(), addListener: sinon.spy() };
    var maps = sinon.mock(google.maps);
    maps
      .expects('Map')
      .once()
      .returns(map);
    maps
      .expects('Circle')
      .once()
      .returns({ getBounds: sinon.spy() });

    scope = $rootScope;
    element = $compile('<ui-map center="{lat: 0, lng: 0}" markers="[]"></ui-map>')(scope);
    scope.$digest();

    maps.verify();
    expect(element.isolateScope().markers).to.be.deep.equal([]);
    expect(element.isolateScope().zoom).to.be.equal(14);
    expect(element.isolateScope().changeRadius).to.be.equal(3000);
    assert(mapSetCenter.withArgs({ lat: 0, lng: 0 }).calledOnce);
  });

  it('should render map with provided markers', function () {
    var mapSetCenter = sinon.spy();
    var map = { setCenter: mapSetCenter, getCenter: sinon.spy(), addListener: sinon.spy() };

    var markerSetMap = sinon.spy();
    var markerAddListener = sinon.stub();
    var markerGetPosition = sinon.stub().returns('recenter');
    var markerSetIcon = sinon.spy();
    var latlng = { lat: 42, lng: 42 };
    var marker = { setMap: markerSetMap, position: latlng, addListener: markerAddListener, getPosition: markerGetPosition, setIcon: markerSetIcon };
    markerAddListener.callsArgOnWith(1, marker, 'foobar');

    var maps = sinon.mock(google.maps);
    maps
      .expects('Map')
      .once()
      .returns(map);
    maps
      .expects('LatLng')
      .once()
      .withArgs(42, 42)
      .returns(latlng);
    maps
      .expects('Circle')
      .once()
      .returns({ getBounds: sinon.spy() });
    maps
      .expects('Marker')
      .once()
      .withArgs({ position: latlng, map: map, title: 2, id: 42, icon: '/assets/images/icons/location.png' })
      .returns(marker);

    var eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('map.selected_store');

    scope = $rootScope;
    element = $compile('<ui-map center="{lat: 0, lng: 0}" markers="{42: {lat: 42, lng: 42, name: 2, id: 42}}"></ui-map>')(scope);
    scope.$digest();

    maps.verify();
    expect(element.isolateScope().markers).to.be.deep.equal({ 42: { lat: 42, lng: 42, name: 2, id: 42 } });
    expect(element.isolateScope().zoom).to.be.equal(14);
    assert(mapSetCenter.withArgs({ lat: 0, lng: 0 }).calledOnce);
    assert(mapSetCenter.withArgs('recenter').calledOnce);
    assert(markerSetMap.withArgs(map).calledOnce);
    assert(markerGetPosition.calledOnce);
  });

  it('should handle click on map markers', function () {
    var mapSetCenter = sinon.spy();
    var map = { setCenter: mapSetCenter, getCenter: sinon.spy(), addListener: sinon.spy() };

    var markerSetMap = sinon.spy();
    var markerAddListener = sinon.stub();
    var markerGetPosition = sinon.stub().returns('recenter');
    var markerSetIcon = sinon.spy();
    var latlng = { lat: 42, lng: 42 };
    var marker = { setMap: markerSetMap, position: latlng, addListener: markerAddListener, getPosition: markerGetPosition, setIcon: markerSetIcon };
    markerAddListener.callsArgOnWith(1, marker, 'foobar');

    var maps = sinon.mock(google.maps);
    maps
      .expects('Map')
      .once()
      .returns(map);
    maps
      .expects('LatLng')
      .once()
      .withArgs(42, 42)
      .returns(latlng);
    maps
      .expects('Circle')
      .once()
      .returns({ getBounds: sinon.spy() });
    maps
      .expects('Marker')
      .once()
      .withArgs({ position: latlng, map: map, title: 2, id: 42, icon: '/assets/images/icons/location.png' })
      .returns(marker);

    var eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('map.selected_store');

    scope = $rootScope;
    element = $compile('<ui-map center="{lat: 0, lng: 0}" markers="{42: {lat: 42, lng: 42, name: 2, id: 42}}"></ui-map>')(scope);
    element.isolateScope().lastMarker = marker;
    scope.$digest();

    maps.verify();
    expect(element.isolateScope().markers).to.be.deep.equal({ 42: { lat: 42, lng: 42, name: 2, id: 42 } });
    expect(element.isolateScope().zoom).to.be.equal(14);
    assert(mapSetCenter.withArgs({ lat: 0, lng: 0 }).calledOnce);
    assert(mapSetCenter.withArgs('recenter').calledOnce);
    assert(markerSetMap.withArgs(map).calledOnce);
    assert(markerGetPosition.calledOnce);
    assert(markerSetIcon.withArgs('/assets/images/icons/location-picked.png').calledOnce);
    markerAddListener.callsArgOnWith(1, marker, 'barfoo');
  });

  it('should detect movement past configured radius', function () {
    var addListener = sinon.spy();
    var setCenter = sinon.spy();
    var getCenter = sinon.stub();
    getCenter.returns({ lat: sinon.stub().returns(42), lng: sinon.stub().returns(42) });

    var contains = sinon.stub();
    contains.returns(false);
    var getBounds = sinon.stub();
    getBounds.returns({ contains: contains });

    var map = { addListener: addListener, setCenter: setCenter, getCenter: getCenter };
    var maps = sinon.mock(google.maps);
    maps
      .expects('Map')
      .once()
      .returns(map);
    maps
      .expects('Circle')
      .twice()
      .returns({ getBounds: getBounds });

    var eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('map.past_radius', { lat: 42, lng: 42 });

    scope = $rootScope;
    element = $compile('<ui-map center="{lat: 0, lng: 0}" markers="[]"></ui-map>')(scope);
    scope.$digest();

    // call dragend listener callback
    addListener.args[0][1]();

    maps.verify();
    eventMock.verify();
    expect(element.isolateScope().markers).to.be.deep.equal([]);
    expect(element.isolateScope().zoom).to.be.equal(14);
    expect(element.isolateScope().changeRadius).to.be.equal(3000);
    assert(addListener.withArgs('dragend').calledOnce);
    assert(setCenter.withArgs({ lat: 0, lng: 0 }).calledOnce);
  });

  it('should ignore movement within configured radius', function () {
    var addListener = sinon.spy();
    var setCenter = sinon.spy();
    var getCenter = sinon.stub();
    getCenter.returns({ lat: 0, lng: 0 });

    var contains = sinon.stub();
    contains.returns(true);
    var getBounds = sinon.stub();
    getBounds.returns({ contains: contains });

    var map = { addListener: addListener, setCenter: setCenter, getCenter: getCenter };
    var maps = sinon.mock(google.maps);
    maps
      .expects('Map')
      .once()
      .returns(map);
    maps
      .expects('Circle')
      .once()
      .withArgs({ center: { lat: 0, lng: 0 }, radius: 3000 })
      .returns({ getBounds: getBounds });

    var eventMock = sinon.mock(event);
    eventMock.expects('dispatch').never();

    scope = $rootScope;
    element = $compile('<ui-map center="{lat: 0, lng: 0}" markers="[]"></ui-map>')(scope);
    scope.$digest();

    // call dragend listener callback
    addListener.args[0][1]();

    maps.verify();
    eventMock.verify();
    expect(element.isolateScope().markers).to.be.deep.equal([]);
    expect(element.isolateScope().zoom).to.be.equal(14);
    expect(element.isolateScope().changeRadius).to.be.equal(3000);
    assert(addListener.withArgs('dragend').calledOnce);
    assert(setCenter.withArgs({ lat: 0, lng: 0 }).calledOnce);
  });

  it('should ignore movement without bounds', function () {
    var addListener = sinon.spy();
    var setCenter = sinon.spy();
    var getCenter = sinon.stub();
    getCenter.returns({ lat: 0, lng: 0 });

    var getBounds = sinon.stub();
    getBounds.returns(null);

    var map = { addListener: addListener, setCenter: setCenter, getCenter: getCenter };
    var maps = sinon.mock(google.maps);
    maps
      .expects('Map')
      .once()
      .returns(map);
    maps
      .expects('Circle')
      .once()
      .withArgs({ center: { lat: 0, lng: 0 }, radius: 3000 })
      .returns({ getBounds: getBounds });

    var eventMock = sinon.mock(event);
    eventMock.expects('dispatch').never();

    scope = $rootScope;
    element = $compile('<ui-map center="{lat: 0, lng: 0}" markers="[]"></ui-map>')(scope);
    scope.$digest();

    // call dragend listener callback
    addListener.args[0][1]();

    maps.verify();
    eventMock.verify();
    expect(element.isolateScope().markers).to.be.deep.equal([]);
    expect(element.isolateScope().zoom).to.be.equal(14);
    expect(element.isolateScope().changeRadius).to.be.equal(3000);
    assert(addListener.withArgs('dragend').calledOnce);
    assert(setCenter.withArgs({ lat: 0, lng: 0 }).calledOnce);
  });
});
