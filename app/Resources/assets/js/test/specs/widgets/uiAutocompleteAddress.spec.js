describe('uiAutocompleteAddress', function () {
  beforeEach(function () {
    bard.appModule('ui.widgets');
    bard.inject('$rootScope', '$compile', 'event');
  });

  it('renders message when messenger reports', function () {
    var addListener = sinon.stub();
    addListener.callsArgWith(1, 'foobar');

    var getPlace = sinon.spy();
    var autocomplete = { addListener: addListener, getPlace: getPlace };

    var places = sinon.mock(google.maps.places);
    places.expects('Autocomplete').once().returns(autocomplete);

    var eventMock = sinon.mock(event);
    eventMock.expects('dispatch').withArgs('address.selected_place');

    scope = $rootScope;
    element = $compile('<input ui-autocomplete-address />')(scope);
    scope.$digest();

    expect(getPlace.called).to.be.true;
    eventMock.verify();
    places.verify();
  });
});
