var assertAddressListIsCorrect = function (addresses) {
  expect(addresses).to.exist;
  expect(addresses[1]).to.exist;
  expect(addresses[2]).to.exist;
  expect(addresses[1].firstName).to.be.equals('Adolfinho');
  expect(addresses[1].lastName).to.be.equals('Mequetreque');
};

describe('customer service', function () {
  beforeEach(function () {
    bard.appModule('linio.customer');
    bard.inject('$httpBackend', '$q', '$rootScope', '$log', 'customer', 'messenger');
  });

  it('should be created successfully', function () {
    expect(customer).to.exist;
  });

  it('should load addresses', function () {
    $httpBackend.when('GET', '/api/customer/addresses').respond(200, stubs.getAddresses());

    customer.loadAddresses().then(function (data) {
      assertAddressListIsCorrect(data);
    });

    $httpBackend.flush();
  });

  it('should handle errors when loading addresses', function () {
    $httpBackend.when('GET', '/api/customer/addresses').respond(400, { message: 'INVALID_CUSTOMER' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_CUSTOMER');

    customer.loadAddresses().catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_CUSTOMER');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should get addresses and memoize', function () {
    $httpBackend.when('GET', '/api/customer/addresses').respond(200, stubs.getAddresses());
    expect(customer.addresses).to.not.exist;

    customer.getAddresses().then(function (data) {
      assertAddressListIsCorrect(data);
      assertAddressListIsCorrect(customer.addresses);

      customer.getAddresses().then(function (data) {
        $httpBackend.verifyNoOutstandingRequest();
        assertAddressListIsCorrect(data);
      });
    });

    $httpBackend.flush();
  });

  it('should get addresses via memoization', function () {
    expect(customer.addresses).to.not.exist;
    customer.addresses = stubs.getAddresses();

    customer.getAddresses().then(function (data) {
      assertAddressListIsCorrect(data);
      assertAddressListIsCorrect(customer.addresses);

      customer.getAddresses().then(function (data) {
        assertAddressListIsCorrect(data);
      });
    });

    $rootScope.$apply();
  });

  it('should get address by id', function () {
    customer.addresses = stubs.getAddresses();

    customer.getAddressById(1).then(function (data) {
      expect(data).to.exist;
      expect(data.firstName).to.be.equals('Adolfinho');
      expect(data.lastName).to.be.equals('Mequetreque');
    });

    $rootScope.$apply();
  });

  it('should not get address by invalid id', function () {
    customer.addresses = stubs.getAddresses();

    customer.getAddressById(42).catch(function (error) {
      expect(error).to.be.equals('Invalid address id: 42');
    });

    $rootScope.$apply();
  });

  it('should get default shipping address', function () {
    customer.addresses = stubs.getAddresses();

    customer.getDefaultShippingAddress().then(function (data) {
      expect(data).to.exist;
      expect(data.firstName).to.be.equals('Adolfinho');
      expect(data.lastName).to.be.equals('Mequetreque');
    });

    $rootScope.$apply();
  });

  it('should not get default shipping address when empty', function () {
    customer.addresses = {};

    customer.getDefaultShippingAddress().then(function (data) {
      expect(data).to.be.null;
    });

    $rootScope.$apply();
  });

  it('should get default billing address', function () {
    customer.addresses = stubs.getAddresses();

    customer.getDefaultBillingAddress().then(function (data) {
      expect(data).to.exist;
      expect(data.firstName).to.be.equals('Barfoo');
      expect(data.lastName).to.be.equals('Foobar');
    });

    $rootScope.$apply();
  });

  it('should not get default billing address when empty', function () {
    customer.addresses = {};

    customer.getDefaultBillingAddress().then(function (data) {
      expect(data).to.be.null;
    });

    $rootScope.$apply();
  });

  it('should create address', function () {
    $httpBackend.when('POST', '/api/customer/address/create', { id: 42, foo: 'bar' }).respond(200, { id: 42, foo: 'bar' });

    customer.createAddress({ id: 42, foo: 'bar' }).then(function (savedAddress) {
      customer.getAddresses().then(function (addresses) {
        expect(addresses[42]).to.exist;
        expect(addresses[42].foo).to.be.equals('bar');
      });
    });

    $httpBackend.flush();
  });

  it('should create address on existing list', function () {
    $httpBackend.when('POST', '/api/customer/address/create', { id: 42, foo: 'bar' }).respond(200, { id: 42, foo: 'bar' });

    customer.addresses = [];
    customer.createAddress({ id: 42, foo: 'bar' }).then(function (savedAddress) {
      customer.getAddresses().then(function (addresses) {
        expect(addresses[42]).to.exist;
        expect(addresses[42].foo).to.be.equals('bar');
      });
    });

    $httpBackend.flush();
  });

  it('should handle errors when creating addresses', function () {
    $httpBackend.when('POST', '/api/customer/address/create', { id: 42, foo: 'bar' }).respond(400, { message: 'INVALID_ADDRESS' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ADDRESS');

    customer.createAddress({ id: 42, foo: 'bar' }).catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ADDRESS');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should update address', function () {
    $httpBackend.when('POST', '/api/customer/address/update', { id: 42, foo: 'bar' }).respond(200, { id: 42, foo: 'bar' });

    customer.addresses = { 42: { foo: 'baz' } };
    customer.updateAddress({ id: 42, foo: 'bar' }).then(function (updatedAddress) {
      expect(customer.addresses[42].foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when updating addresses', function () {
    $httpBackend.when('POST', '/api/customer/address/update', { id: 42, foo: 'bar' }).respond(400, { message: 'INVALID_ADDRESS' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ADDRESS');

    customer.updateAddress({ id: 42, foo: 'bar' }).catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ADDRESS');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should remove address by id', function () {
    $httpBackend.when('POST', '/api/customer/address/remove', { id: 42 }).respond(200, { id: 42 });

    customer.addresses = { 42: { foo: 'baz' } };
    customer.removeAddressById(42).then(function (removedAddress) {
      expect(customer.addresses).to.be.deep.equals({});
    });

    $httpBackend.flush();
  });

  it('should handle errors when removing addresses', function () {
    $httpBackend.when('POST', '/api/customer/address/remove', { id: 42 }).respond(400, { message: 'INVALID_ADDRESS' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ADDRESS');

    customer.removeAddressById(42).catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ADDRESS');
    });

    $httpBackend.flush();
  });

  it('should signup for partnership', function () {
    $httpBackend.when('POST', '/api/customer/partnership/signup', { code: 'foobar', accountNumber: '42' }).respond(200, { foo: 'bar' });

    customer.signupPartnership('foobar', '42').then(function (partnershipResponse) {
      expect(partnershipResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when signing up for partnership', function () {
    $httpBackend.when('POST', '/api/customer/partnership/signup', { code: 'foobar', accountNumber: '42' }).respond(400, { message: 'INVALID_PARTNERSHIP' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_PARTNERSHIP');

    customer.signupPartnership('foobar', '42').catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_PARTNERSHIP');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should remove existing partnership', function () {
    $httpBackend.when('POST', '/api/customer/partnership/foobar/remove').respond(200, { foo: 'bar' });

    customer.removePartnership('foobar').then(function (partnershipResponse) {
      expect(partnershipResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when removing existing partnership', function () {
    $httpBackend.when('POST', '/api/customer/partnership/foobar/remove').respond(400, { message: 'INVALID_PARTNERSHIP' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_PARTNERSHIP');

    customer.removePartnership('foobar').catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_PARTNERSHIP');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should signup for loyalty', function () {
    $httpBackend.when('POST', '/api/customer/loyalty/signup', { loyaltyId: '42' }).respond(200, { foo: 'bar' });

    customer.signupLoyalty('42').then(function (loyaltyResponse) {
      expect(loyaltyResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when signing up for loyalty', function () {
    $httpBackend.when('POST', '/api/customer/loyalty/signup', { loyaltyId: '42' }).respond(400, { message: 'INVALID_LOYALTY' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_LOYALTY');

    customer.signupLoyalty('42').catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_LOYALTY');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should register wallet', function () {
    $httpBackend.when('POST', '/api/customer/wallet/register', { account: '42', holderName: 'foo', holderLastname: 'bar' }).respond(200, { foo: 'bar' });

    customer.registerWallet('42', 'foo', 'bar').then(function (walletResponse) {
      expect(walletResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when signing up for wallet', function () {
    $httpBackend.when('POST', '/api/customer/wallet/register', { account: '42', holderName: 'foo', holderLastname: 'bar' }).respond(400, { message: 'INVALID_ACCOUNT' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ACCOUNT');

    customer.registerWallet('42', 'foo', 'bar').catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ACCOUNT');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should get cards', function () {
    $httpBackend.when('GET', '/api/customer/cards').respond(200, stubs.getAddresses());

    customer.getCards().then(function (data) {
      assertAddressListIsCorrect(data);
    });

    $httpBackend.flush();
  });

  it('should handle errors when getting cards', function () {
    $httpBackend.when('GET', '/api/customer/cards').respond(400, { message: 'INVALID_CUSTOMER' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_CUSTOMER');

    customer.getCards().catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_CUSTOMER');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should submit seller review', function () {
    $httpBackend.when('POST', '/api/customer/review-seller', { orderNumber: '123', sellerId: 1, rating: 5, title: 'foo', detail: 'bar' }).respond(200, { foo: 'bar' });

    var review = {
      rating: 5,
      title: 'foo',
      detail: 'bar',
    };
    customer.reviewSeller('123', 1, review).then(function (loyaltyResponse) {
      expect(loyaltyResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when submitting seller review', function () {
    $httpBackend.when('POST', '/api/customer/review-seller', { orderNumber: '123', sellerId: 1, rating: 5, title: 'foo', detail: 'bar' }).respond(400, { message: 'INVALID_ACCOUNT' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ACCOUNT');

    var review = {
      rating: 5,
      title: 'foo',
      detail: 'bar',
    };
    customer.reviewSeller('123', 1, review).catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ACCOUNT');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should issue item return', function () {
    $httpBackend.when('POST', '/api/customer/order/return-item', { orderNumber: '123', sku: 'ABC-123', quantity: 2, reason: 1, action: 1, comment: 'foobar' }).respond(200, { foo: 'bar' });
    var item = {
      sku: 'ABC-123',
      quantity: 2,
      reason: 1,
      action: 1,
      comment: 'foobar',
    };

    customer.returnOrderItem('123', item).then(function (orderReturnResponse) {
      expect(orderReturnResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when issuing item return', function () {
    $httpBackend.when('POST', '/api/customer/order/return-item', { orderNumber: '123', sku: 'ABC-123', quantity: 2, reason: 1, action: 1, comment: 'foobar' }).respond(400, { message: 'INVALID_ORDER' });
    var item = {
      sku: 'ABC-123',
      quantity: 2,
      reason: 1,
      action: 1,
      comment: 'foobar',
    };

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ORDER');

    customer.returnOrderItem('123', item).catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ORDER');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should request invoice', function () {
    $httpBackend.when('POST', '/api/customer/request-invoice', { customerAddress: 'foo', orderId: '123' }).respond(200, { foo: 'bar' });

    customer.requestInvoice('123', 'foo').then(function (requestInvoiceResponse) {
      expect(requestInvoiceResponse.foo).to.be.equals('bar');
    });

    $httpBackend.flush();
  });

  it('should handle errors when requesting invoice', function () {
    $httpBackend.when('POST', '/api/customer/request-invoice', { customerAddress: 'foo', orderId: '123' }).respond(400, { message: 'INVALID_ORDER' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_ORDER');

    customer.requestInvoice('123', 'foo').catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_ORDER');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });
});
