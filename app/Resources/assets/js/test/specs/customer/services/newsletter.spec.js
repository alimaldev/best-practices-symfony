var assertSubscription = function (subscription) {
  expect(subscription).to.exist;
  expect(subscription.status).to.be.equals('success');
  expect(subscription.coupon).to.exist;
};

describe('newsletter service', function () {
  beforeEach(function () {
    bard.appModule('linio.customer');
    bard.inject('$httpBackend', '$q', '$rootScope', '$log', 'newsletter', 'messenger');
  });

  it('should be created successfully', function () {
    expect(newsletter).to.exist;
  });

  it('should subscribe successfully', function () {
    $httpBackend.when('POST', '/api/newsletter/subscribe').respond(200, stubs.subscribe());

    newsletter.subscribe().then(function (data) {
      assertSubscription(data);
    });

    $httpBackend.flush();
  });

  it('should handle errors when subscribing', function () {
    $httpBackend.when('POST', '/api/newsletter/subscribe').respond(400, { message: 'INVALID_SUBSCRIBER' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_SUBSCRIBER');

    newsletter.subscribe().catch(function () {
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_SUBSCRIBER');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });
});
