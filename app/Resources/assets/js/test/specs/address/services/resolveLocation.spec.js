describe('resolveLocation service', function () {
  beforeEach(function () {
    bard.appModule('linio.address');
    bard.inject('$httpBackend', '$q', '$rootScope', '$log', 'event', 'resolveLocation', 'messenger');
  });

  it('should be created successfully', function () {
    expect(resolveLocation).to.exist;
  });

  it('should resolve postcode', function () {
    $httpBackend.when('GET', '/api/address/postcode?postcode=123456').respond(200, { foo: 'bar' });

    resolveLocation.resolvePostcode('123456').then(function (data) {
      expect(data).to.be.deep.equal({ foo: 'bar' });
    });

    $httpBackend.flush();
  });

  it('should handle error when trying to resolve postcode', function () {
    $httpBackend.when('GET', '/api/address/postcode?postcode=123456').respond(400, { message: 'INVALID_POSTCODE' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_POSTCODE');

    resolveLocation.resolvePostcode('123456').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_POSTCODE');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should get regions', function () {
    $httpBackend.when('GET', '/api/address/regions?country=1').respond(200, { foo: 'bar' });

    resolveLocation.getRegions({ country: 1 }).then(function (data) {
      expect(data).to.be.deep.equal({ foo: 'bar' });
    });

    $httpBackend.flush();
  });

  it('should handle error when getting regions', function () {
    $httpBackend.when('GET', '/api/address/regions?country=1').respond(400, { message: 'NOT_FOUND' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('NOT_FOUND');

    resolveLocation.getRegions({ country: 1 }).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: NOT_FOUND');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should get municipalities', function () {
    $httpBackend.when('GET', '/api/address/municipalities?region=1').respond(200, { foo: 'bar' });

    resolveLocation.getMunicipalities({ region: 1 }).then(function (data) {
      expect(data).to.be.deep.equal({ foo: 'bar' });
    });

    $httpBackend.flush();
  });

  it('should handle error when getting municipalities', function () {
    $httpBackend.when('GET', '/api/address/municipalities?region=1').respond(400, { message: 'NOT_FOUND' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('NOT_FOUND');

    resolveLocation.getMunicipalities({ region: 1 }).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: NOT_FOUND');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should get cities', function () {
    $httpBackend.when('GET', '/api/address/cities?country=1').respond(200, { foo: 'bar' });

    resolveLocation.getCities({ country: 1 }).then(function (data) {
      expect(data).to.be.deep.equal({ foo: 'bar' });
    });

    $httpBackend.flush();
  });

  it('should handle error when getting cities', function () {
    $httpBackend.when('GET', '/api/address/cities?country=1').respond(400, { message: 'NOT_FOUND' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('NOT_FOUND');

    resolveLocation.getCities({ country: 1 }).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: NOT_FOUND');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should get neighborhoods', function () {
    $httpBackend.when('GET', '/api/address/neighborhoods?city=1').respond(200, { foo: 'bar' });

    resolveLocation.getNeighborhoods({ city: 1 }).then(function (data) {
      expect(data).to.be.deep.equal({ foo: 'bar' });
    });

    $httpBackend.flush();
  });

  it('should handle error when getting neighborhoods', function () {
    $httpBackend.when('GET', '/api/address/neighborhoods?city=1').respond(400, { message: 'NOT_FOUND' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('NOT_FOUND');

    resolveLocation.getNeighborhoods({ city: 1 }).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: NOT_FOUND');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });
});
