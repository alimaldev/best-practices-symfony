describe('addressBook component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.address');
    bard.inject('$rootScope', '$componentController', '$q', 'customer', 'event');

    scope = $rootScope.$new();
    component = $componentController('addressBook', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.addressBook).to.be.equal(component);
    expect(component.addresses).to.be.deep.equal({});
    expect(component.listeners).to.be.empty;
    expect(component.addressToEdit).to.be.null;
    expect(component.currentTab).to.be.null;
  });

  it('should be initialized with addresses', function () {
    sinon.stub(customer, 'getAddresses').returns($q.when(stubs.getAddresses()));
    component.$onInit();
    $rootScope.$apply();

    expect(component.addresses).to.not.be.empty;
    expect(component.listeners).to.not.be.empty;
    expect(component.addressToEdit).to.be.null;
    expect(component.currentTab).to.be.equal('list');
  });

  it('should be initialized without addresses', function () {
    sinon.stub(customer, 'getAddresses').returns($q.when({}));
    component.$onInit();
    $rootScope.$apply();

    expect(component.addresses).to.be.deep.equal({});
    expect(component.listeners).to.not.be.empty;
    expect(component.addressToEdit).to.be.null;
    expect(component.currentTab).to.be.equal('form');
  });

  it('should be clearing listeners on destruct', function () {
    var spyListener = sinon.spy();
    component.listeners = [spyListener];
    component.$onDestroy();

    expect(spyListener.called).to.be.true;
  });

  it('should be checking if has addresses', function () {
    expect(component.hasAddresses()).to.be.false;
    component.addresses = { 1: { id: 1 } };
    expect(component.hasAddresses()).to.be.true;
  });

  it('should select address by id', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('getAddressById').once().withArgs(42).returns($q.when({ id: 42 }));

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('address.selected_address', {
      type: 'shipping',
      address: {
        id: 42,
      },
    });

    component.type = 'shipping';
    component.select(42);
    $rootScope.$apply();

    customerMock.verify();
    eventMock.verify();
  });

  it('should edit address by id', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('getAddressById').once().withArgs(42).returns($q.when({ id: 42 }));

    component.edit(42);
    $rootScope.$apply();

    expect(component.currentTab).to.be.equal('edit');
    expect(component.addressToEdit).to.be.deep.equal({ id: 42 });

    customerMock.verify();
  });

  it('should edit address by id and use regionId as region if regionId is set', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('getAddressById').once().withArgs(42).returns($q.when({ id: 42, region: 'Something', regionId: 25 }));

    component.edit(42);
    $rootScope.$apply();

    expect(component.currentTab).to.be.equal('edit');
    expect(component.addressToEdit).to.be.deep.equal({ id: 42, region: '25', regionId: 25 });

    customerMock.verify();
  });

  it('should remove address by id', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('removeAddressById').once().withArgs(42).returns($q.when({ id: 42 }));
    customerMock.expects('getAddresses').once().returns($q.when({ 1: { id: 1 } }));

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('address.removed_address', {
      type: 'shipping',
      address: {
        id: 42,
      },
    });

    component.type = 'shipping';
    component.remove(42);
    $rootScope.$apply();

    expect(component.addresses[1]).to.exist;

    customerMock.verify();
    eventMock.verify();
  });

  it('should switch tabs', function () {
    expect(component.currentTab).to.be.null;
    component.addressToEdit = 'foo';
    component.switchTab('stuff');
    expect(component.currentTab).to.be.equal('stuff');
    expect(component.addressToEdit).to.be.null;
  });

  it('should handle new addresses being created', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('getAddresses').twice().returns($q.when({ 1: { id: 1 } }));

    component.type = 'shipping';
    component.$onInit();
    $rootScope.$broadcast('address.new_address_created', { id: 42 });
    $rootScope.$apply();

    customerMock.verify();
    expect(component.addresses[1]).to.exist;
    expect(component.currentTab).to.be.equal('list');
  });

  it('should handle new addresses being created within selectable mode', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('getAddresses').twice().returns($q.when({ 1: { id: 1 } }));

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('address.selected_address', {
      type: 'shipping',
      address: {
        id: 42,
      },
    });

    component.selectable = true;
    component.type = 'shipping';
    component.$onInit();
    $rootScope.$broadcast('address.new_address_created', { id: 42 });
    $rootScope.$apply();

    customerMock.verify();
    eventMock.verify();
    expect(component.addresses[1]).to.exist;
    expect(component.currentTab).to.be.equal('list');
  });

  it('should handle addresses being updated', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('getAddresses').once().returns($q.when({ 1: { id: 1 } }));

    component.addressToEdit = 'foo';
    component.$onInit();
    $rootScope.$broadcast('address.address_updated');
    $rootScope.$apply();

    customerMock.verify();
    expect(component.currentTab).to.be.equal('list');
    expect(component.addressToEdit).to.be.null;
  });
});
