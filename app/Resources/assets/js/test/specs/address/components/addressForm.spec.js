describe('addressForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.address');
    bard.inject('$rootScope', '$componentController', '$q', 'resolveLocation', 'customer', 'event');

    scope = $rootScope.$new();
    component = $componentController('addressForm', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.addressForm).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.input).to.be.null;
    expect(component.regions).to.be.empty;
    expect(component.municipalities).to.be.empty;
    expect(component.cities).to.be.empty;
    expect(component.neighborhoods).to.be.empty;
  });

  it('should resolvePostcode address', function () {
    resolveLocationMock = sinon.mock(resolveLocation);
    resolveLocationMock.expects('resolvePostcode').once().withArgs('123').returns($q.when({
      country: 'RS',
      region: 'RS',
      municipality: 'Herval',
      city: 'Herval',
      neighborhoods: ['Passo do Mingote'],
    }));
    resolveLocationMock.expects('getCities').once().withArgs({
      country: 'RS', postcode: '123', region: 'RS',
    }).returns($q.when([
      {
        id: 'Herval',
        name: 'Herval',
      },
    ]));
    resolveLocationMock.expects('getMunicipalities').once().withArgs({
      city: 'Herval', country: 'RS', postcode: '123', region: 'RS',
    }).returns($q.when([
      {
        id: 'Herval',
        name: 'Herval',
      },
    ]));
    resolveLocationMock.expects('getNeighborhoods').once().withArgs({
      city: 'Herval', country: 'RS', municipality: 'Herval', postcode: '123', region: 'RS',
    }).returns($q.when([
      {
        id: 'Passo do Mingote',
        name: 'Passo do Mingote',
      },
    ]));

    component.input = { postcode: '123' };
    component.resolvePostcode();
    $rootScope.$apply();
    resolveLocationMock.verify();

    expect(component.input.country).to.be.equal('RS');
    expect(component.input.region).to.be.equal('RS');
    expect(component.input.municipality).to.be.equal('Herval');
    expect(component.input.city).to.be.equal('Herval');
    expect(component.input.neighborhood).to.be.equal('Passo do Mingote');
  });

  it('should load regions', function () {
    sinon.stub(resolveLocation, 'getRegions').returns($q.when({ foo: 'bar' }));
    component.loadRegions().then(function () {
      expect(component.regions).to.deep.equal({ foo: 'bar' });
    });

    $rootScope.$apply();
  });

  it('should load municipalities', function () {
    sinon.stub(resolveLocation, 'getMunicipalities').returns($q.when({ foo: 'bar' }));
    component.loadMunicipalities().then(function () {
      expect(component.municipalities).to.deep.equal({ foo: 'bar' });
    });

    $rootScope.$apply();
  });

  it('should load cities', function () {
    sinon.stub(resolveLocation, 'getCities').returns($q.when({ foo: 'bar' }));
    component.loadCities().then(function () {
      expect(component.cities).to.deep.equal({ foo: 'bar' });
    });

    $rootScope.$apply();
  });

  it('should load neighborhoods', function () {
    sinon.stub(resolveLocation, 'getNeighborhoods').returns($q.when({ foo: 'bar' }));
    component.loadNeighborhoods().then(function () {
      expect(component.neighborhoods).to.deep.equal({ foo: 'bar' });
    });

    $rootScope.$apply();
  });

  it('should create new address', function () {
    var newAddress = { foo: 'bar' };

    customerMock = sinon.mock(customer);
    customerMock.expects('createAddress').once().withArgs(newAddress).returns($q.when(newAddress));

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('address.new_address_created', newAddress);

    component.input = newAddress;
    component.save();
    $rootScope.$apply();

    customerMock.verify();
    eventMock.verify();
  });

  it('should update existing address', function () {
    var addressToUpdate = { id: 1, foo: 'bar' };

    customerMock = sinon.mock(customer);
    customerMock.expects('updateAddress').once().withArgs(addressToUpdate).returns($q.when(addressToUpdate));

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('address.address_updated', addressToUpdate);

    component.input = addressToUpdate;
    component.save();
    $rootScope.$apply();

    customerMock.verify();
    eventMock.verify();
  });

  it('should not preload data if provided input contains invalid data', function () {
    resolveLocationMock = sinon.mock(resolveLocation);
    resolveLocationMock.expects('getCities').never();
    resolveLocationMock.expects('getMunicipalities').never();
    resolveLocationMock.expects('getNeighborhoods').never();

    component.$onChanges();
    resolveLocationMock.verify();
  });

  it('should preload data if provided input contains valid data', function () {
    resolveLocationMock = sinon.mock(resolveLocation);
    resolveLocationMock.expects('getCities').once().returns($q.reject());
    resolveLocationMock.expects('getMunicipalities').once().returns($q.reject());
    resolveLocationMock.expects('getNeighborhoods').once().returns($q.reject());

    component.input = {
      city: 'foo',
      municipality: 'foo',
      neighborhood: 'foo',
    };

    component.$onChanges();
    resolveLocationMock.verify();
  });
});
