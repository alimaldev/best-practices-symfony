var assertOrderWasUpdated = function (order) {
  expect(order).to.exist;
  expect(order.subTotal).to.equal(177.52);
  expect(order.grandTotal).to.equal(377.52);
  expect(order.taxAmount).to.equal(28.34);
};

describe('buildOrder service', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$httpBackend', '$q', '$rootScope', '$log', 'event', 'buildOrder', 'order', 'messenger', 'localStorageService');
  });

  it('should be created successfully', function () {
    expect(buildOrder).to.exist;
  });

  it('should get order in progress', function () {
    $httpBackend.when('POST', '/api/order', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('set').once().withArgs('cart_count', 2);
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.getOrderInProgress(order).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when trying to get order in progress', function () {
    $httpBackend.when('POST', '/api/order', { order: stubs.getOrderState() }).respond(400, { message: 'INVALID_SKU' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_SKU');

    buildOrder.getOrderInProgress(order).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_SKU');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should remove item', function () {
    $httpBackend.when('DELETE', '/api/order/item/ABC-123', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('set').once().withArgs('cart_count', 2);
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.removeItem(order, 'ABC-123').then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when removing item', function () {
    $httpBackend.when('DELETE', '/api/order/item/INV-123', { order: stubs.getOrderState() }).respond(400, { message: 'INVALID_SKU' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_SKU');

    buildOrder.removeItem(order, 'INV-123').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_SKU');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should update item quantity', function () {
    $httpBackend.when('PUT', '/api/order/item/ABC-123', { quantity: 2, order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('set').once().withArgs('cart_count', 2);
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.updateQuantity(order, 'ABC-123', 2).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when updating item quantity', function () {
    $httpBackend.when('PUT', '/api/order/item/ABC-123', { quantity: 2, order: stubs.getOrderState() }).respond(400, { message: 'INVALID_QUANTITY' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('INVALID_QUANTITY');

    buildOrder.updateQuantity(order, 'ABC-123', 2).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: INVALID_QUANTITY');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should apply coupon', function () {
    $httpBackend.when('PUT', '/api/order/coupon/FREE-STUFF', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.applyCoupon(order, 'FREE-STUFF').then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when applying coupon', function () {
    $httpBackend.when('PUT', '/api/order/coupon/FREE-STUFF', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.applyCoupon(order, 'FREE-STUFF').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should remove coupon', function () {
    $httpBackend.when('DELETE', '/api/order/coupon', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.removeCoupon(order).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when removing coupon', function () {
    $httpBackend.when('DELETE', '/api/order/coupon', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.removeCoupon(order).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set shipping address on order', function () {
    $httpBackend.when('PUT', '/api/order/shipping-address', { address: { region: 'foobar' }, order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setShippingAddress(order, { region: 'foobar' }).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting shipping address', function () {
    $httpBackend.when('PUT', '/api/order/shipping-address', { address: { region: 'foobar' }, order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setShippingAddress(order, { region: 'foobar' }).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set shipping address on order by id', function () {
    $httpBackend.when('PUT', '/api/order/shipping-address/1', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setShippingAddressById(order, 1).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting shipping address by id', function () {
    $httpBackend.when('PUT', '/api/order/shipping-address/1', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setShippingAddressById(order, 1).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set billing address on order', function () {
    buildOrder.setBillingAddress(order, { foo: 'bar' }).then(function (data) {
      expect(order.billingAddress.foo).to.be.equal('bar');
    });

    $rootScope.$apply();
  });

  it('should set shipping method for package', function () {
    $httpBackend.when('PUT', '/api/order/package/1', { shippingMethod: 'regular', order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setShippingMethod(order, 1, 'regular').then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting shipping method', function () {
    $httpBackend.when('PUT', '/api/order/package/1', { shippingMethod: 'regular', order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setShippingMethod(order, 1, 'regular').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set payment method', function () {
    order.installmentOption = stubs.getOrder().order.installmentOptions[0];

    var orderState = stubs.getOrderState();
    orderState.installments = null;

    $httpBackend.when('PUT', '/api/order/payment-method/cod', { order: orderState }).respond(200, stubs.getOrder());

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setPaymentMethod(order, 'cod').then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting payment method', function () {
    $httpBackend.when('PUT', '/api/order/payment-method/cod', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setPaymentMethod(order, 'cod').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set payment method', function () {
    $httpBackend.when('PUT', '/api/order/pickup-store/1', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setPickupStore(order, 1).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting payment method', function () {
    $httpBackend.when('PUT', '/api/order/pickup-store/1', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setPickupStore(order, 1).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set bin number', function () {
    $httpBackend.when('PUT', '/api/order/bin/123456', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setBinNumber(order, 123456).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting bin number', function () {
    $httpBackend.when('PUT', '/api/order/bin/123456', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setBinNumber(order, 123456).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should set geohash', function () {
    $httpBackend.when('PUT', '/api/order/geohash/123456', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.setGeohash(order, '123456').then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when setting geohash', function () {
    $httpBackend.when('PUT', '/api/order/geohash/123456', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.setGeohash(order, '123456').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should select installment quantity', function () {
    $httpBackend.when('PUT', '/api/order/installment/12', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.selectInstallmentQuantity(order, 12).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when selecting installment quantity', function () {
    $httpBackend.when('PUT', '/api/order/installment/12', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.selectInstallmentQuantity(order, 12).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should use wallet points', function () {
    $httpBackend.when('PUT', '/api/order/wallet/42', { order: stubs.getOrderState() }).respond(200, stubs.getOrder());
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.update');

    buildOrder.useWalletPoints(order, 42).then(function (data) {
      assertOrderWasUpdated(order);
      eventMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when using wallet points', function () {
    $httpBackend.when('PUT', '/api/order/wallet/42', { order: stubs.getOrderState() }).respond(400, { message: 'FOOBAR' });
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error');

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    buildOrder.useWalletPoints(order, 42).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      eventMock.verify();
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should not parse empty messages', function () {
    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').never();
    buildOrder.parseMessages();
    messengerMock.verify();
  });

  it('should parse success messages correctly', function () {
    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('success').once().withArgs('foobar');

    buildOrder.parseMessages({ success: 'foobar', errors: [], warnings: [] });
    messengerMock.verify();
  });

  it('should parse error messages correctly', function () {
    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('foobar');

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error', 'foobar');

    buildOrder.parseMessages({ success: null, errors: ['foobar'], warnings: [] });
    messengerMock.verify();
    eventMock.verify();
  });

  it('should parse warning messages correctly', function () {
    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('warning').once().withArgs('foobar');

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('order.error', 'foobar');

    buildOrder.parseMessages({ success: null, errors: [], warnings: ['foobar'] });
    messengerMock.verify();
    eventMock.verify();
  });

  it('should get current order state', function () {
    var orderState = buildOrder.getOrderState(order);

    expect(orderState).to.exist;
    expect(orderState.shippingAddressId).to.be.null;
    expect(orderState.paymentMethod).to.be.null;
    expect(orderState.creditCardBinNumber).to.be.null;
    expect(orderState.selectedShippingOptions).to.be.empty;
    expect(orderState.walletPoints).to.be.null;
  });

  it('should get current order state with data', function () {
    var validOrder = stubs.getOrder().order;
    validOrder.shippingAddress = { id: 42 },
    validOrder.paymentMethod = 'Foobar',
    validOrder.creditCardBinNumber = '123456',
    validOrder.wallet.totalPointsUsed = 42;
    validOrder.wallet.maxPointsForOrder = 42;
    order.update(validOrder);

    var orderState = buildOrder.getOrderState(order);

    expect(orderState).to.exist;
    expect(orderState.shippingAddressId).to.be.equal(42);
    expect(orderState.paymentMethod).to.be.equal('Foobar');
    expect(orderState.creditCardBinNumber).to.be.equal('123456');
    expect(orderState.selectedShippingOptions).to.be.deep.equal({ 1: 'express' });
    expect(orderState.walletPoints).to.be.equal(42);
  });

  it('should get current order state with partial data', function () {
    var validOrder = stubs.getOrder().order;
    validOrder.shippingAddress = { id: 42 },
    validOrder.paymentMethod = 'Foobar',
    validOrder.creditCardBinNumber = '123456',
    validOrder.packages[1].shippingQuotes = [];
    order.update(validOrder);

    var orderState = buildOrder.getOrderState(order);

    expect(orderState).to.exist;
    expect(orderState.shippingAddressId).to.be.equal(42);
    expect(orderState.paymentMethod).to.be.equal('Foobar');
    expect(orderState.creditCardBinNumber).to.be.equal('123456');
    expect(orderState.selectedShippingOptions).to.be.empty;
    expect(orderState.walletPoints).to.be.null;
  });
});
