describe('mercadoPago service', function () {
  beforeEach(function () {
    Mercadopago.createToken = function () {};

    Mercadopago.getInstallments = function () {};
  });

  it('should tokenize card based on form data', function () {
    var getElementsByName = sinon.stub();
    getElementsByName.returns(['foobar']);

    var createToken = sinon.stub(Mercadopago, 'createToken');
    createToken
      .withArgs('foobar')
      .callsArgWith(1, 200, { id: 42, first_six_digits: 123456 });

    var getInstallments = sinon.stub(Mercadopago, 'getInstallments');
    getInstallments
      .withArgs({ bin: 123456 })
      .callsArgWith(1, 200, [{ payment_method_id: 1, issuer: { id: 1 } }]);

    bard.appModule('linio.checkout', function ($provide) {
      $provide.value('$window', { document: { getElementsByName: getElementsByName } });
    });

    bard.inject('$rootScope', 'mercadoPago');

    mercadoPago.tokenize().then(function (tokenizedCard) {
      expect(tokenizedCard).to.be.deep.equal({
        cardToken: 42,
        paymentMethodId: 1,
        issuerId: 1,
      });
    });

    $rootScope.$apply();
    assert(getElementsByName.withArgs('CreditCard').calledOnce);
  });

  it('should detect error on getting installments for extra card info', function () {
    var getElementsByName = sinon.stub();
    getElementsByName.returns(['foobar']);

    var createToken = sinon.stub(Mercadopago, 'createToken');
    createToken
      .withArgs('foobar')
      .callsArgWith(1, 200, { id: 42, first_six_digits: 123456 });

    var getInstallments = sinon.stub(Mercadopago, 'getInstallments');
    getInstallments
      .withArgs({ bin: 123456 })
      .callsArgWith(1, 500, { message: 'Invalid card!' });

    bard.appModule('linio.checkout', function ($provide) {
      $provide.value('$window', { document: { getElementsByName: getElementsByName } });
    });

    bard.inject('$rootScope', 'mercadoPago');

    mercadoPago.tokenize().catch(function (error) {
      expect(error).to.be.equal('Invalid card!');
    });

    $rootScope.$apply();
    assert(getElementsByName.withArgs('CreditCard').calledOnce);
  });

  it('should detect error during tokenization', function () {
    var getElementsByName = sinon.stub();
    getElementsByName.returns(['foobar']);

    var createToken = sinon.stub(Mercadopago, 'createToken');
    createToken
      .withArgs('foobar')
      .callsArgWith(1, 400, { message: 'Oops!' });

    bard.appModule('linio.checkout', function ($provide) {
      $provide.value('$window', { document: { getElementsByName: getElementsByName } });
    });

    bard.inject('$rootScope', 'mercadoPago');

    mercadoPago.tokenize().catch(function (error) {
      expect(error).to.be.equal('Oops!');
    });

    $rootScope.$apply();
    assert(getElementsByName.withArgs('CreditCard').calledOnce);
  });
});
