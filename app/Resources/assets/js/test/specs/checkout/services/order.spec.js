describe('Order class', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$q', 'order');
  });

  it('should be created successfully', function () {
    expect(order).to.exist;
  });

  it('should update itself based on external data', function () {
    order.update({ foo: 'bar' });
    expect(order.foo).to.be.equal('bar');
  });

  it('should normalize order and link package items', function () {
    order.update(stubs.getOrder().order);

    expect(order.packages[1].selectedShippingQuote).to.exist;
    expect(order.packages[1].selectedShippingQuote.fee).to.be.equal(200);
    expect(order.packages[1].items['SKU0001-001']).to.exist;
    expect(order.packages[1].items['SKU0001-001'].name).to.be.equal('Config_1');

    expect(order.highestInstallment).to.exist;
    expect(order.highestInstallment.installments).to.be.equal(1);

    expect(order.lowestInstallment).to.exist;
    expect(order.lowestInstallment.installments).to.be.equal(1);

    expect(order.installmentOption).to.exist;
    expect(order.installmentOption.installments).to.be.equal(1);

    expect(order.partnership).to.exist;
    expect(order.partnership.name).to.be.equal('Foobar!');

    expect(order.selectedStore).to.exist;
    expect(order.selectedStore.name).to.be.equal('Pickup Store 2');
  });

  it('should normalize order and select lowest installment if there is none selected', function () {
    var validOrder = stubs.getOrder().order;
    validOrder.highestInstallment = 2;
    validOrder.installmentOptions = [
      {
        installments: 1,
        interestFee: 0,
        total: 377.52,
        totalInterest: 377.52,
        amount: 377.52,
        selected: false,
      },
      {
        installments: 2,
        interestFee: 0,
        total: 377.52,
        totalInterest: 377.52,
        amount: 377.52,
        selected: false,
      },
    ];
    order.update(validOrder);

    expect(order.highestInstallment).to.exist;
    expect(order.highestInstallment.installments).to.be.equal(2);

    expect(order.lowestInstallment).to.exist;
    expect(order.lowestInstallment.installments).to.be.equal(1);

    expect(order.installmentOption).to.exist;
    expect(order.installmentOption.installments).to.be.equal(1);
  });

  it('should set delivery fees', function () {
    var homeOrder = stubs.getOrder().order;
    homeOrder.pickupStores = [];
    order.update(homeOrder);

    expect(order.storePickupFee).to.be.equal(50);
    expect(order.homeDeliveryFee).to.be.equal(200);
  });

  it('should get the total number of items', function () {
    order.update(stubs.getOrder().order);

    expect(order.countItems()).to.be.equal(2);
  });

  it('should detect if order is empty', function () {
    expect(order.isEmpty()).to.be.true;
    order.update(stubs.getOrder().order);
    expect(order.isEmpty()).to.be.false;
  });

  it('should detect if undeliverable', function () {
    order.update(stubs.getOrder().order);

    expect(order.isUndeliverable('SKU0001-001')).to.be.false;
    expect(order.isUndeliverable('SKU0001-002')).to.be.true;
  });

  it('should detect if the package is home deliverable when shippingMethod is regular', function () {
    var package = {
      shippingQuotes: [
        {
          shippingMethod: 'regular',
        },
      ],
    };

    expect(order.isHomeDeliverable(package)).to.be.true;
  });

  it('should detect if the package is home deliverable when shippingMethod is express', function () {
    var package = {
      shippingQuotes: [
        {
          shippingMethod: 'express',
        },
      ],
    };

    expect(order.isHomeDeliverable(package)).to.be.true;
  });

  it('should detect if the package is home deliverable when shippingMethod is electronic', function () {
    var package = {
      shippingQuotes: [
        {
          shippingMethod: 'electronic',
        },
      ],
    };

    expect(order.isHomeDeliverable(package)).to.be.true;
  });

  it('should detect if the package is not home deliverable', function () {
    var package = {
      shippingQuotes: [
        {
          shippingMethod: 'store',
        },
      ],
    };

    expect(order.isHomeDeliverable(package)).to.be.false;
  });

  it('should detect if the package is not deliverable on empty shippingQuotes', function () {
    var package = {
      shippingQuotes: [],
    };

    expect(order.isHomeDeliverable(package)).to.be.false;
  });

  it('should detect if the package is store deliverable when shippingMethod is store', function () {
    var package = {
      shippingQuotes: [
        {
          shippingMethod: 'regular',
        },
        {
          shippingMethod: 'store',
        },
      ],
    };

    expect(order.isStoreDeliverable(package)).to.be.true;
  });

  it('should detect if the package is not store deliverable', function () {
    var package = {
      shippingQuotes: [
        {
          shippingMethod: 'regular',
        },
        {
          shippingMethod: 'express',
        },
      ],
    };

    expect(order.isStoreDeliverable(package)).to.be.false;
  });

  it('should detect if the package is not store deliverable on empty shippingQuotes', function () {
    var package = {
      shippingQuotes: [],
    };

    expect(order.isStoreDeliverable(package)).to.be.false;
  });

  it('should detect if payment method is allowed', function () {
    order.update(stubs.getValidOrder().order);

    expect(order.isPaymentMethodAllowed('CreditCard')).to.be.false;
    expect(order.isPaymentMethodAllowed('CashOnDelivery')).to.be.true;
  });

  it('should detect if payment method is allowed by coupon', function () {
    order.update(stubs.getValidOrder().order);

    expect(order.isPaymentMethodAllowedByCoupon('CreditCard')).to.be.false;
    expect(order.isPaymentMethodAllowedByCoupon('CashOnDelivery')).to.be.false;
    expect(order.isPaymentMethodAllowedByCoupon('DebitCard')).to.be.true;
  });

  it('should be invalid if there is no shipping address', function () {
    order.shippingAddress = null;
    expect(order.isValid()).to.be.false;
  });

  it('should be invalid if there is no payment method', function () {
    order.shippingAddress = {};
    order.paymentMethod = null;
    expect(order.isValid()).to.be.false;
  });

  it('should be invalid if billing address is required and was not provided', function () {
    order.shippingAddress = {};
    order.paymentMethod = 'CashOnDelivery';
    order.isBillingAddressRequired = function () {
      return true;
    };

    expect(order.isValid()).to.be.false;
  });

  it('should be invalid if there are no packages', function () {
    order.shippingAddress = {};
    order.paymentMethod = {};
    order.packages = null;
    expect(order.isValid()).to.be.false;

    order.packages = {};
    expect(order.isValid()).to.be.false;
  });

  it('should be invalid if there are undeliverables', function () {
    order.shippingAddress = {};
    order.paymentMethod = {};
    order.packages = { 1: {} };
    order.undeliverables = ['foo'];
    expect(order.isValid()).to.be.false;
  });

  it('should be valid with all constraints met', function () {
    order.shippingAddress = {};
    order.paymentMethod = {};
    order.packages = { 1: {} };
    order.undeliverables = [];
    expect(order.isValid()).to.be.true;
  });

  it('should detect if billing address is required', function () {
    order.update(stubs.getValidOrder().order);

    order.paymentMethod = 'CreditCard';
    expect(order.isBillingAddressRequired()).to.be.false;

    order.paymentMethod = 'CashOnDelivery';
    expect(order.isBillingAddressRequired()).to.be.false;

    order.paymentMethod = 'DebitCard';
    expect(order.isBillingAddressRequired()).to.be.true;
  });

  it('should detect if wallet is being used', function () {
    expect(order.isUsingWallet()).to.be.false;

    order.update(stubs.getValidOrder().order);
    order.wallet.totalPointsUsed = 0;
    expect(order.isUsingWallet()).to.be.false;

    order.wallet.totalPointsUsed = 100;
    expect(order.isUsingWallet()).to.be.true;
  });

  it('should get the selected store', function () {
    order.update(stubs.getOrder().order);

    var store = order.getSelectedStore();
    expect(store).to.exist;
    expect(store.name).to.be.equal('Pickup Store 2');
  });

  it('should get nothing when there is no selected store', function () {
    var store = order.getSelectedStore();
    expect(store).to.not.exist;
  });

  it('should get the selected partnership', function () {
    order.update(stubs.getOrder().order);

    var partnership = order.getSelectedPartnership();
    expect(partnership).to.exist;
    expect(partnership.name).to.be.equal('Foobar!');
  });

  it('should get nothing when there is no selected partnership', function () {
    var partnership = order.getSelectedPartnership();
    expect(partnership).to.not.exist;
  });

  it('should get the selected shipping quote', function () {
    order.update(stubs.getOrder().order);

    var quote = order.getSelectedShippingQuote(1);
    expect(quote).to.exist;
    expect(quote.shippingMethod).to.be.equal('express');
  });

  it('should get nothing when there is no selected shipping quote', function () {
    var quote = order.getSelectedShippingQuote(123);
    expect(quote).to.not.exist;
  });

  it('should get the selected shipping quotes', function () {
    order.update(stubs.getOrder().order);

    var quotes = order.getSelectedShippingQuotes();
    expect(quotes).to.exist;
    expect(quotes[1].shippingMethod).to.be.equal('express');
  });

  it('should get nothing when there is no selected shipping quote', function () {
    var clearedOrder = stubs.getOrder().order;
    clearedOrder.packages[1].shippingQuotes[1].selected = false;
    order.update(clearedOrder);

    var quotes = order.getSelectedShippingQuotes();
    expect(quotes).to.be.empty;
  });

  it('should clear selected shipping quotes', function () {
    order.update(stubs.getOrder().order);

    order.clearSelectedShippingQuotes();
    var quotes = order.getSelectedShippingQuotes();
    expect(quotes).to.be.empty;
  });

  it('should get nothing when there is no selected installment option', function () {
    var installmentOption = order.getSelectedInstallmentOption();
    expect(installmentOption).to.not.exist;
  });

  it('should get the selected items', function () {
    order.update(stubs.getOrder().order);

    var items = order.getSelectedItems();
    expect(items).to.exist;
    expect(items['SKU0001-001']).to.exist;
    expect(items['SKU0001-001']).to.be.equal(2);
  });

  it('should detect if item is free shipping without quotes', function () {
    var multiplePackageOrder = stubs.getOrder().order;
    multiplePackageOrder.packages[1].shippingQuotes = [];
    order.update(multiplePackageOrder);

    expect(order.isFreeShipping('SKU0001-001')).to.be.false;
  });

  it('should detect if item is free shipping if item is not available', function () {
    order.update(stubs.getOrder().order);

    expect(order.isFreeShipping('SKU0002')).to.be.false;
  });

  it('should detect if item is free shipping based on selected quote', function () {
    var multiplePackageOrder = stubs.getOrder().order;
    multiplePackageOrder.packages = {
      1: {
        items: {
          'SKU0001-001': 1,
        },
        shippingQuotes: [
          {
            id: 0,
            shippingMethod: 'regular',
            fee: 0,
            estimatedDeliveryDate: '2016-03-16 00:00:00',
            selected: true,
            freeShipping: true,
          },
          {
            id: 1,
            shippingMethod: 'express',
            fee: 200,
            estimatedDeliveryDate: '2016-03-17 00:00:00',
            selected: false,
            freeShipping: false,
          },
          {
            id: 2,
            shippingMethod: 'store',
            fee: 50,
            estimatedDeliveryDate: '2016-03-18 00:00:00',
            selected: false,
            freeShipping: false,
          },
        ],
      },
      2: {
        items: {
          'SKU0001-001': 1,
        },
        shippingQuotes: [
          {
            id: 0,
            shippingMethod: 'regular',
            fee: 100,
            estimatedDeliveryDate: '2016-03-16 00:00:00',
            selected: true,
            freeShipping: false,
          },
          {
            id: 1,
            shippingMethod: 'express',
            fee: 200,
            estimatedDeliveryDate: '2016-03-17 00:00:00',
            selected: false,
            freeShipping: false,
          },
          {
            id: 2,
            shippingMethod: 'store',
            fee: 50,
            estimatedDeliveryDate: '2016-03-18 00:00:00',
            selected: false,
            freeShipping: false,
          },
        ],
      },
    };

    order.update(multiplePackageOrder);
    expect(order.isFreeShipping('SKU0001-001')).to.be.false;

    multiplePackageOrder.packages[2].shippingQuotes[0].fee = 0;
    multiplePackageOrder.packages[2].shippingQuotes[0].freeShipping = true;
    order.update(multiplePackageOrder);

    expect(order.isFreeShipping('SKU0001-001')).to.be.true;
  });
});
