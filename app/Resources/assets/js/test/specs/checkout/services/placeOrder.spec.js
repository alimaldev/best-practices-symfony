var assertOrderWasUpdated = function (order) {
  expect(order).to.exist;
  expect(order.subTotal).to.equal(177.52);
  expect(order.grandTotal).to.equal(377.52);
  expect(order.taxAmount).to.equal(28.34);
};

describe('placeOrder service', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$httpBackend', '$q', '$rootScope', '$log', 'placeOrder', 'order', 'localStorageService', 'messenger', 'mercadoPago');
  });

  it('should be created successfully', function () {
    expect(placeOrder).to.exist;
  });

  it('should not detect MercadoPago', function () {
    order.paymentMethod = 'CashOnDelivery';
    expect(placeOrder.isMercadoPago(order)).to.be.false;
  });

  it('should not detect MercadoPago if there are no installments selected', function () {
    order.paymentMethod = 'CreditCard';
    expect(placeOrder.isMercadoPago(order)).to.be.false;
  });

  it('should not detect MercadoPago if there is an installment selected, but with different payment method', function () {
    order.paymentMethod = 'CreditCard';
    order.installmentOptions = [
      {
        paymentMethodName: 'AmexGateway',
        installments: 1,
        interestFee: 0,
        total: 377.52,
        totalInterest: 377.52,
        amount: 377.52,
        selected: true,
      },
    ];

    expect(placeOrder.isMercadoPago(order)).to.be.false;
  });

  it('should detect MercadoPago', function () {
    order.paymentMethod = 'CreditCard';
    order.installmentOptions = [
      {
        paymentMethodName: 'MercadoPago_CreditCard_Api',
        installments: 1,
        interestFee: 0,
        total: 377.52,
        totalInterest: 377.52,
        amount: 377.52,
        selected: true,
      },
    ];

    expect(placeOrder.isMercadoPago(order)).to.be.true;
  });

  it('should place order', function () {
    order.update(stubs.getOrder().order);
    $httpBackend.when('POST', '/api/order/place', stubs.getOrderRequest()).respond(200, stubs.getOrder());

    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('remove').once().withArgs('cart_count');

    placeOrder.place(order, null, null).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
    });

    $httpBackend.flush();
  });

  it('should place order without installment option selected', function () {
    order.update(stubs.getOrder().order);
    order.installmentOptions = [];

    var orderRequest = stubs.getOrderRequest();
    orderRequest.installments = null;

    $httpBackend.when('POST', '/api/order/place', orderRequest).respond(200, stubs.getOrder());

    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('remove').once().withArgs('cart_count');

    placeOrder.place(order, null, null).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
    });

    $httpBackend.flush();
  });

  it('should place order without coupon code', function () {
    var orderRequest = stubs.getOrderRequest();
    orderRequest.coupon = null;

    order.update(stubs.getOrder().order);
    order.coupon = null;

    $httpBackend.when('POST', '/api/order/place', orderRequest).respond(200, stubs.getOrder());

    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('remove').once().withArgs('cart_count');

    placeOrder.place(order, null, null).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
    });

    $httpBackend.flush();
  });

  it('should place order with billing address data', function () {
    var orderRequest = stubs.getOrderRequest();
    orderRequest.billingAddress = {
      id: 42,
      firstName: 'Adolfinho',
      lastName: 'Mequetreque',
    };
    orderRequest.paymentData = {
      billingFirstName: 'Adolfinho',
      billingLastName: 'Mequetreque',
    };

    order.update(stubs.getOrder().order);
    order.billingAddress = orderRequest.billingAddress;

    $httpBackend.when('POST', '/api/order/place', orderRequest).respond(200, stubs.getOrder());

    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('remove').once().withArgs('cart_count');

    placeOrder.place(order, {}, null).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle error when placing order', function () {
    order.update(stubs.getOrder().order);
    $httpBackend.when('POST', '/api/order/place', stubs.getOrderRequest()).respond(400, { context: { message: 'FOOBAR' } });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    placeOrder.place(order, null, null).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should handle fallback error message when placing order', function () {
    order.update(stubs.getOrder().order);
    $httpBackend.when('POST', '/api/order/place', stubs.getOrderRequest()).respond(400, { message: 'FOOBAR' });

    messengerMock = sinon.mock(messenger);
    messengerMock.expects('clear').once();
    messengerMock.expects('error').once().withArgs('FOOBAR');

    placeOrder.place(order, null, null).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: FOOBAR');
      messengerMock.verify();
    });

    $httpBackend.flush();
  });

  it('should place order with MercadoPago', function () {
    order.update(stubs.getOrder().order);
    order.paymentMethod = 'CreditCard';
    order.installmentOptions = [
      {
        paymentMethodName: 'MercadoPago_CreditCard_Api',
        installments: 1,
        interestFee: 0,
        total: 377.52,
        totalInterest: 377.52,
        amount: 377.52,
        selected: true,
      },
    ];

    var orderRequest = stubs.getOrderRequest();
    orderRequest.paymentMethod = 'CreditCard';
    orderRequest.paymentData = 'foobar';
    orderRequest.dependentPaymentMethod = 'MercadoPago_CreditCard_Api';

    $httpBackend.when('POST', '/api/order/place', orderRequest).respond(200, stubs.getOrder());

    localStorageServiceMock = sinon.mock(localStorageService);
    localStorageServiceMock.expects('remove').once().withArgs('cart_count');

    mercadoPagoMock = sinon.mock(mercadoPago);
    mercadoPagoMock.expects('tokenize').once().returns($q.when('foobar'));

    placeOrder.place(order, null, null).then(function (data) {
      assertOrderWasUpdated(order);
      localStorageServiceMock.verify();
      mercadoPagoMock.verify();
    });

    $httpBackend.flush();
  });
});
