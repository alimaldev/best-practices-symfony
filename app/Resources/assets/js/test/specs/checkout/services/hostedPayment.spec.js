describe('hostedPayment service', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout', function ($provide) {
      $provide.value('$window', { location: { href: '' } });
    });

    bard.inject('$rootScope', '$compile', '$window', 'hostedPayment');
  });

  it('should be created successfully', function () {
    expect(hostedPayment).to.exist;
  });

  it('should redirect via window for GET', function () {
    hostedPayment.redirect('GET', '/foobar', []);
    expect($window.location.href).to.be.equal('/foobar');
  });

  it('should create redirection form properly', function () {
    var createdForm = hostedPayment.createForm('POST', '/foobar', { foo: 'bar' });
    expect(createdForm).to.be.equal('<form method="POST" action="/foobar"><input type="hidden" name="foo" value="bar"></input></form>');
  });

  it('should add form to DOM and submit', sinon.test(function () {
    var submit = sinon.spy();
    var append = sinon.spy();
    var eq = sinon.stub();
    eq.returns({ append: append });
    var find = sinon.stub();
    find.withArgs('body').returns({ eq: eq });

    angularMock = this.mock(angular);
    angularMock.expects('element').withArgs('<form method="POST" action="/foobar"></form>').returns([{ submit: submit }]);
    angularMock.expects('element').withArgs(document).returns({ find: find });

    hostedPayment.redirect('POST', '/foobar');

    angularMock.verify();
    assert(find.withArgs('body').calledOnce);
    assert(eq.withArgs(0).calledOnce);
    assert(append.calledOnce);
    assert(submit.calledOnce);
  }));
});
