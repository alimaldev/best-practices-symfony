describe('walletBar component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$componentController', '$q', 'order', 'buildOrder', 'customer');

    scope = $rootScope.$new();
    component = $componentController('walletBar', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.wallet).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.number).to.be.equal('');
    expect(component.holderName).to.be.equal('');
    expect(component.holderLastname).to.be.equal('');
    expect(component.hasError).to.be.false;
  });

  it('should activate wallet', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('registerWallet').once().withArgs('123456', 'foo', 'bar').returns($q.when({}));

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().returns($q.when({}));

    component.number = '123456';
    component.holderName = 'foo';
    component.holderLastname = 'bar';
    component.activate();
    $rootScope.$apply();

    customerMock.verify();
    buildOrderMock.verify();

    expect(component.hasError).to.be.false;
    expect(component.number).to.be.equal('123456');
  });

  it('should not activate wallet without number', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('registerWallet').never();

    component.activate();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.hasError).to.be.true;
    expect(component.number).to.be.equal('');
  });

  it('should detect errors when activating wallet', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('registerWallet').once().withArgs('123456', 'foo', 'bar').returns($q.reject({}));

    component.number = '123456';
    component.holderName = 'foo';
    component.holderLastname = 'bar';
    component.activate();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.hasError).to.be.true;
    expect(component.number).to.be.equal('');
  });

  it('should use wallet points', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('useWalletPoints').once().withArgs(order, 20).returns($q.when({}));

    component.use(20);
    $rootScope.$apply();
    buildOrderMock.verify();
  });

  it('should remove wallet points', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('useWalletPoints').once().withArgs(order, 0).returns($q.when({}));

    component.remove();
    $rootScope.$apply();
    buildOrderMock.verify();
  });
});
