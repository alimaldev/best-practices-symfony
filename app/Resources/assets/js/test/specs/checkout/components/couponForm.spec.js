describe('couponForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$componentController', '$q', 'order', 'buildOrder');

    scope = $rootScope.$new();
    component = $componentController('couponForm', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.coupon).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.code).to.be.equal('');
    expect(component.order).to.be.equal(order);
    expect(component.showForm).to.be.false;
    expect(component.hasError).to.be.false;
  });

  it('should apply coupon', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('applyCoupon').once().withArgs(order, 'FREE').returns($q.when({}));

    component.code = 'FREE';
    component.apply();
    $rootScope.$apply();
    buildOrderMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.hasError).to.be.false;
    expect(component.code).to.be.equal('FREE');
  });

  it('should not apply coupon without code', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('applyCoupon').never();

    component.apply();
    $rootScope.$apply();
    buildOrderMock.verify();

    expect(component.hasError).to.be.true;
    expect(component.code).to.be.equal('');
  });

  it('should detect errors when applying coupon', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('applyCoupon').once().withArgs(order, 'FREE').returns($q.reject({}));

    component.code = 'FREE';
    component.apply();
    $rootScope.$apply();
    buildOrderMock.verify();

    expect(component.hasError).to.be.true;
    expect(component.code).to.be.equal('');
  });

  it('should remove coupon', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('removeCoupon').once().returns($q.when({}));

    component.code = 'FREE';
    component.remove();
    $rootScope.$apply();
    buildOrderMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.code).to.be.equal('');
  });
});
