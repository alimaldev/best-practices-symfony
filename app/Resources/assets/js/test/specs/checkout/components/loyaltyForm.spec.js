describe('loyaltyForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$componentController', '$q', 'customer', 'order', 'buildOrder');

    scope = $rootScope.$new();
    component = $componentController('loyaltyForm', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.loyalty).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.id).to.be.equal('');
    expect(component.order).to.be.equal(order);
    expect(component.showForm).to.be.false;
    expect(component.error).to.be.false;
  });

  it('should signup for partnership', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('signupLoyalty').once().withArgs('4242').returns($q.when({}));

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().returns($q.when({}));

    component.id = '4242';
    component.signup();

    $rootScope.$apply();
    customerMock.verify();
    buildOrderMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.error).to.be.false;
    expect(component.id).to.be.equal('4242');
  });

  it('should not signup without code', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('signupLoyalty').never();

    component.signup();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.id).to.be.equal('');
  });

  it('should detect errors when signing up', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('signupLoyalty').once().withArgs('4242').returns($q.reject({ data: { message: 'foobar' } }));

    component.id = '4242';
    component.signup();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.error).to.be.equal('foobar');
    expect(component.id).to.be.equal('');
  });
});
