describe('dropdownBlock component', function () {
  var component;

  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$componentController');

    scope = $rootScope.$new();
    component = $componentController('dropdownBlock', { $scope: scope });
  });

  it('should init with an empty content', function () {
    expect(component.content).to.be.empty;
  });

  it('should set parameter into content', function () {
    var contentMock = { accountNumber:'123654' };
    component.loadContent(contentMock);
    expect(component.content).to.not.equal('');
  });

});
