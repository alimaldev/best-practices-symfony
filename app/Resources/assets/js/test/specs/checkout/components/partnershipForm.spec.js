describe('partnershipForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$compile', '$componentController', '$q', 'customer', 'buildOrder');

    scope = $rootScope.$new();
    component = $componentController('partnershipForm', { $scope: scope });
  });

  it('should pass this controller to accordion controller if exists', function () {
    var templateMock = '<script type="text/ng-template" id="accordion"></script>';
    var elementMock = '<ui-accordion><partnership-form></partnership-form></ui-accordion>';
    var accordion = $compile(elementMock)(scope);
    $compile(templateMock)(scope);
    scope.$digest();

    uiAccordionController = accordion.controller('uiAccordion');
    component = $componentController('partnershipForm',
      { $scope: scope },
      { accordion: uiAccordionController }
    );

    component.$onInit();
    expect(uiAccordionController.content).to.be.equal(component);
  });

  it('should pass this controller to dropdown controller if exists', function () {
    var templateMock = '<script type="text/ng-template" id="dropdownBlock"></script>';
    var elementMock = '<dropdown-block><partnership-form></partnership-form></dropdown-block>';
    var dropdown = $compile(elementMock)(scope);
    $compile(templateMock)(scope);
    scope.$digest();

    dropdownBlockController = dropdown.controller('dropdownBlock');
    component = $componentController('partnershipForm',
      { $scope: scope },
      { dropdownBlock: dropdownBlockController }
    );
    component.$onInit();
    expect(dropdownBlockController.content).to.be.equal(component);
  });

  it('should be attached to the scope', function () {
    expect(scope.form).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.accountNumber).to.be.equal('');
    expect(component.showForm).to.be.false;
    expect(component.hasError).to.be.false;
  });

  it('should signup for partnership', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('signupPartnership').once().withArgs('foobar', '123456').returns($q.when({}));

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().returns($q.when({}));

    component.partnership = { code: 'foobar' };
    component.accountNumber = '123456';
    component.signup();
    $rootScope.$apply();

    customerMock.verify();
    buildOrderMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.hasError).to.be.false;
    expect(component.isActive).to.be.true;
    expect(component.accountNumber).to.be.equal('123456');
  });

  it('should not signup for partnership without account number', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('signupPartnership').never();

    component.signup();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.hasError).to.be.true;
    expect(component.accountNumber).to.be.equal('');
  });

  it('should detect errors when signing up for partnership', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('signupPartnership').once().withArgs('foobar', '123456').returns($q.reject({}));

    component.partnership = { code: 'foobar' };
    component.accountNumber = '123456';
    component.signup();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.hasError).to.be.true;
    expect(component.accountNumber).to.be.equal('');
  });

  it('should remove partnership', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('removePartnership').once().withArgs('foobar').returns($q.when({}));

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once();

    component.partnership = { code: 'foobar' };
    component.accountNumber = '123456';
    component.remove();
    $rootScope.$apply();
    customerMock.verify();
    buildOrderMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.isActive).to.be.false;
    expect(component.accountNumber).to.be.equal('');
  });

  it('should set active account', function () {
    var templateMock = '<script type="text/ng-template" id="partnershipForm"></script>';
    var elementMock = '<partnership-form partnership="partnership"></partnership-form>';
    scope.partnership = { accountNumber: '1234' };

    $compile(templateMock)(scope);
    elementMock = $compile(elementMock)(scope);
    scope.$digest();
    var controller = elementMock.controller('partnershipForm');

    expect(controller.isActive).to.be.true;
    expect(controller.accountNumber).to.be.equal('1234');
  });

  it('should set inactive account', function () {
    var templateMock = '<script type="text/ng-template" id="partnershipForm"></script>';
    var elementMock = '<partnership-form partnership="partnership"></partnership-form>';
    scope.partnership = { accountNumber: null };

    $compile(templateMock)(scope);
    elementMock = $compile(elementMock)(scope);
    scope.$digest();
    var controller = elementMock.controller('partnershipForm');

    expect(controller.isActive).to.be.false;
    expect(controller.accountNumber).to.be.equal('');
  });

});
