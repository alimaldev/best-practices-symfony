describe('ShippingOptions controller', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$controller', '$q', '$rootScope', '$state', 'order', 'geolocation', 'geohash', 'buildOrder', 'ngDialog', 'navigate');
    $state.go = function () { return; };
  });

  it('should update order data on activation', function () {
    sinon.stub(buildOrder, 'getOrderInProgress', function (order) {
      return $q.when(order.update(stubs.getOrder().order));
    });

    controller = $controller('ShippingOptionsController', { $scope: $rootScope });
    $rootScope.$apply();

    expect(controller.order).to.not.be.empty;
    expect(controller.isReady).to.be.true;
  });

  it('should redirect to cart if order has no items', function () {
    sinon.stub(buildOrder, 'getOrderInProgress').returns($q.when({}));

    navigateMock = sinon.mock(navigate);
    navigateMock.expects('toCart').once();

    controller = $controller('ShippingOptionsController', { $scope: $rootScope });
    $rootScope.$apply();

    expect(controller.order).to.not.be.empty;
    expect(controller.isReady).to.be.false;
    navigateMock.verify();
  });

  it('should open store pickup tab if order has a selected store', function () {
    order.update(stubs.getOrder().order);

    geolocationMock = sinon.mock(geolocation);
    geolocationMock.expects('getCurrentCoordinates').once().returns($q.reject());

    controller = $controller('ShippingOptionsController', { $scope: $rootScope });
    $rootScope.$apply();

    expect(controller.order).to.not.be.empty;
    expect(controller.isReady).to.be.true;
    expect(controller.currentTab).to.be.equal('storePickup');
    geolocationMock.verify();
  });

  it('should switch tabs', function () {
    sinon.stub(buildOrder, 'getOrderInProgress').returns($q.reject({}));

    controller = $controller('ShippingOptionsController', { $scope: $rootScope });
    $rootScope.$apply();

    expect(controller.currentTab).to.be.equal('homeDelivery');
    controller.switchTab('stuff');
    expect(controller.currentTab).to.be.equal('stuff');
  });

  it('should setup home delivery tab on switch', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').twice().returns($q.reject());

    controller = $controller('ShippingOptionsController', { $scope: $rootScope });
    $rootScope.$apply();

    expect(controller.currentTab).to.be.equal('homeDelivery');
    order.selectedStore = 'foo';
    controller.switchTab('homeDelivery');
    expect(controller.currentTab).to.be.equal('homeDelivery');
    expect(order.selectedStore).to.be.null;
    buildOrderMock.verify();
  });

  it('should setup store pickup tab on switch', function () {
    sinon.stub(buildOrder, 'getOrderInProgress').returns($q.reject({}));

    controller = $controller('ShippingOptionsController', { $scope: $rootScope });
    $rootScope.$apply();

    geolocationMock = sinon.mock(geolocation);
    geolocationMock.expects('getCurrentCoordinates').once().returns($q.reject());

    expect(controller.currentTab).to.be.equal('homeDelivery');
    controller.switchTab('storePickup');
    expect(controller.currentTab).to.be.equal('storePickup');
    geolocationMock.verify();
  });

  describe('after activate', function () {
    var controller;

    beforeEach(function () {
      sinon.stub(buildOrder, 'getOrderInProgress').returns($q.reject());

      controller = $controller('ShippingOptionsController', { $scope: $rootScope });
      $rootScope.$apply();
    });

    it('should register listeners', function () {
      scopeMock = sinon.mock($rootScope);
      scopeMock.expects('$on').once().withArgs('address.selected_address');
      scopeMock.expects('$on').once().withArgs('address.selected_place');
      scopeMock.expects('$on').once().withArgs('address.address_updated');
      scopeMock.expects('$on').once().withArgs('map.selected_store');
      scopeMock.expects('$on').once().withArgs('map.past_radius');

      controller = $controller('ShippingOptionsController', { $scope: $rootScope });
      scopeMock.verify();
    });

    it('should go to next step', function () {
      stateMock = sinon.mock($state);
      stateMock.expects('go').once().withArgs('payment');
      controller.nextStep();
      stateMock.verify();
    });

    it('should display address book', function () {
      ngDialogMock = sinon.mock(ngDialog);
      ngDialogMock.expects('open').once().withArgs({
        template: '<address-book selectable="true" type="shipping"></address-book>',
        plain: true,
      }).returns('foo');

      expect(controller.addressBook).to.be.null;
      controller.showAddressBook();
      $rootScope.$apply();
      expect(controller.addressBook).to.be.equal('foo');
    });

    it('should handle shipping address selection', function () {
      addressBook = {
        close: sinon.spy(),
      };

      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setShippingAddressById').once().withArgs(order, 42);

      controller.addressBook = addressBook;
      $rootScope.$broadcast('address.selected_address', { type: 'shipping', address: { id: 42 } });

      $rootScope.$apply();
      buildOrderMock.verify();
      expect(addressBook.close.called).to.be.true;
    });

    it('should update the address display if address is changed', function () {
      buildOrderMock = sinon.mock(buildOrder);
      order.shippingAddress = { id: 42, firstName: 'Original' };

      buildOrderMock.expects('setShippingAddressById')
          .once()
          .withArgs(order, 42)
          .returns($q.when(order.update(order.shippingAddress = { id: 42, firstName: 'Updated' })));

      $rootScope.$broadcast('address.address_updated', { id: 42, firstName: 'Updated' });

      $rootScope.$apply();
      buildOrderMock.verify();
      expect(controller.order.shippingAddress.firstName).to.be.equal('Updated');
    });

    it('should select a shipping method', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setShippingMethod').once().withArgs(order, 1, 2);

      controller.selectShippingMethod(1, 2);

      buildOrderMock.verify();
    });

    it('should select a store', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setPickupStore').once().withArgs(order, 42);

      controller.selectStore({ id: 42, lat: 1, lng: 1 });
      buildOrderMock.verify();
      expect(controller.center).to.be.deep.equal({ lat: 1, lng: 1 });
    });

    it('should check if there are stores', function () {
      expect(controller.hasStores()).to.be.false;
      controller.markers = { 1: { foo: 'bar' } };
      expect(controller.hasStores()).to.be.true;
    });

    it('should update map from coordinates', function () {
      geohashMock = sinon.mock(geohash);
      geohashMock.expects('encode').once().withArgs(42, 42).returns('foo123');
      geohashMock.expects('decode').once().withArgs('dhwupu4907e').returns({ latitude: 42, longitude: 42 });

      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setGeohash').once().withArgs(order, 'foo123').returns($q.when(order.update(stubs.getOrder().order)));

      controller.updateMap({ lat: 42, lng: 42 });

      $rootScope.$apply();
      geohashMock.verify();
      buildOrderMock.verify();

      expect(controller.center).to.be.deep.equal({ lat: 42, lng: 42 });
      expect(controller.markers).to.not.be.empty;
      expect(controller.markers[2]).to.exist;
      expect(controller.markers[2].name).to.be.equal('Pickup Store 2');
    });

    it('should select a store from map click', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setPickupStore').once().withArgs(order, 42);

      controller.markers = { 42: 'foobar' };
      $rootScope.$broadcast('map.selected_store', { id: 42 });
      buildOrderMock.verify();
      expect(controller.selectedStore).to.be.equal('foobar');
    });

    it('should select a store from map click', function () {
      controller.updateMap = sinon.spy();
      var lat = sinon.spy();
      var lng = sinon.spy();

      $rootScope.$broadcast('address.selected_place', { geometry: { location: { lat: lat, lng: lng } } });
      assert(lat.calledOnce);
      assert(lng.calledOnce);
    });

    it('should continue when current tab is homeDelivery and isHomeDeliverable is true', function () {
      order.packages = {
        1: {
          shippingQuotes: [
            {
              shippingMethod: 'regular',
            },
          ],
        },
      };

      controller.currentTab = 'homeDelivery';

      expect(controller.canContinue()).to.be.true;
    });

    it('should not continue when current tab is homeDelivery and isHomeDeliverable is false', function () {
      order.packages = {
        1: {
          shippingQuotes: [
            {
              shippingMethod: 'store',
            },
          ],
        },
      };

      controller.currentTab = 'homeDelivery';

      expect(controller.canContinue()).to.be.false;
    });

    it('should continue when current tab is storePickup and isStoreDeliverable is true and store is selected', function () {
      order.selectedStore = 'foo';
      order.packages = {
        1: {
          shippingQuotes: [
            {
              shippingMethod: 'store',
            },
          ],
        },
      };

      controller.currentTab = 'storePickup';

      expect(controller.canContinue()).to.be.true;
    });

    it('should continue when current tab is storePickup andand store is selected', function () {
      order.selectedStore = 'foo';

      controller.currentTab = 'storePickup';

      expect(controller.canContinue()).to.be.true;
    });

    it('should not continue when current tab is storePickup and store is not selected', function () {
      controller.currentTab = 'storePickup';

      expect(controller.canContinue()).to.be.false;
    });

    it('should remove item', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock
        .expects('removeItem')
        .once()
        .withArgs(order, 'ABC-123')
        .returns($q.when(order.update(stubs.getOrder().order)));

      controller.removeItem('ABC-123');
      $rootScope.$apply();
      buildOrderMock.verify();
    });

    it('should remove item and redirect to cart if there are no more items', function () {
      sinon.stub(order, 'countItems').returns(0);

      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock
        .expects('removeItem')
        .once()
        .withArgs(order, 'ABC-123')
        .returns($q.when({}));

      navigateMock = sinon.mock(navigate);
      navigateMock.expects('toCart').once();

      controller.removeItem('ABC-123');
      $rootScope.$apply();
      buildOrderMock.verify();
      navigateMock.verify();
    });

    it('should update map after past radius event', function () {
      controller.updateMap = sinon.stub();
      controller.updateMap.withArgs({ lat: 42, lng: 42 });

      $rootScope.$broadcast('map.past_radius', { lat: 42, lng: 42 });
    });
  });
});
