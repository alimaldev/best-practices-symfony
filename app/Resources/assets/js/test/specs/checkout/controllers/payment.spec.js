describe('Payment controller', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$controller', '$q', '$rootScope', '$state', 'navigate', 'event', 'order', 'buildOrder', 'placeOrder', 'hostedPayment', 'ngDialog', 'customer');
    $state.go = function () { return; };
  });

  bard.verifyNoOutstandingHttpRequests();

  it('should register listeners', function () {
    sinon.stub(buildOrder, 'getOrderInProgress').returns($q.reject());

    eventMock = sinon.mock(event);
    eventMock.expects('listen').once().withArgs('address.selected_address');
    eventMock.expects('listen').once().withArgs('form.extra_checkout_fields.validation');

    controller = $controller('PaymentController');
    $rootScope.$apply();

    eventMock.verify();
  });

  it('should update order data', function () {
    sinon.stub(customer, 'getDefaultBillingAddress').returns($q.reject());
    sinon.stub(buildOrder, 'getOrderInProgress', function (order) {
      return $q.when(order.update(stubs.getOrder().order));
    });

    controller = $controller('PaymentController');
    $rootScope.$apply();

    expect(controller.order).to.not.be.empty;
    expect(controller.isReady).to.be.true;
  });

  it('should redirect to cart if order has no items', function () {
    sinon.stub(customer, 'getDefaultBillingAddress').returns($q.reject());
    sinon.stub(buildOrder, 'getOrderInProgress').returns($q.when({}));

    navigateMock = sinon.mock(navigate);
    navigateMock.expects('toCart').once();

    controller = $controller('PaymentController');
    $rootScope.$apply();

    expect(controller.order).to.not.be.empty;
    expect(controller.isReady).to.be.false;
    navigateMock.verify();
  });

  it('should set default billing address', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().returns($q.when(order.update(stubs.getOrder().order)));
    buildOrderMock.expects('setBillingAddress').once().withArgs(order, 'foobar');

    customerMock = sinon.mock(customer);
    customerMock.expects('getDefaultBillingAddress').once().returns($q.when('foobar'));

    controller = $controller('PaymentController');
    $rootScope.$apply();

    expect(controller.order).to.not.be.empty;
    expect(controller.isReady).to.be.true;
    customerMock.verify();
    buildOrderMock.verify();
  });

  it('should set payment method', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('listen').once().withArgs('address.selected_address');
    eventMock.expects('listen').once().withArgs('form.extra_checkout_fields.validation');
    eventMock.expects('listen').once().withArgs('form.CashOnDelivery.validation').returns('foobar');

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().withArgs(order).returns($q.reject({}));
    buildOrderMock.expects('setPaymentMethod').once().withArgs(order, 'CashOnDelivery').returns($q.when(function () {
      expect(controller.refreshPaymentFormListener).to.be.equal('foobar');
      buildOrderMock.verify();
      eventMock.verify();
    }));

    controller = $controller('PaymentController');
    expect(controller.refreshPaymentFormListener).to.not.exist;

    controller.setPaymentMethod('CashOnDelivery');
    $rootScope.$apply();
  });

  it('should set payment method and refresh listener', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('listen').once().withArgs('address.selected_address');
    eventMock.expects('listen').once().withArgs('form.extra_checkout_fields.validation');
    eventMock.expects('listen').once().withArgs('form.CashOnDelivery.validation').returns('foobar');

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().withArgs(order).returns($q.reject({}));
    buildOrderMock.expects('setPaymentMethod').once().withArgs(order, 'CashOnDelivery').returns($q.when(function () {
      expect(controller.refreshPaymentFormListener).to.be.equal('foobar');
      buildOrderMock.verify();
      eventMock.verify();
    }));

    controller = $controller('PaymentController');
    controller.refreshPaymentFormListener = sinon.spy();

    controller.setPaymentMethod('CashOnDelivery');
    $rootScope.$apply();
  });

  it('should set payment method and clear installment selection', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('listen').once().withArgs('address.selected_address');
    eventMock.expects('listen').once().withArgs('form.extra_checkout_fields.validation');
    eventMock.expects('listen').once().withArgs('form.CashOnDelivery.validation').returns('foobar');

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().withArgs(order).returns($q.reject({}));
    buildOrderMock.expects('setPaymentMethod').once().withArgs(order, 'CashOnDelivery').returns($q.when(function () {
      expect(controller.refreshPaymentFormListener).to.be.equal('foobar');
      buildOrderMock.verify();
      eventMock.verify();
    }));

    controller = $controller('PaymentController');
    controller.refreshPaymentFormListener = sinon.spy();
    controller.paymentData.installments = 2;

    controller.setPaymentMethod('CashOnDelivery');
    $rootScope.$apply();
    expect(controller.paymentData).to.be.empty;
  });

  it('should setup payment method on activation', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('listen').once().withArgs('address.selected_address');
    eventMock.expects('listen').once().withArgs('form.extra_checkout_fields.validation');
    eventMock.expects('listen').once().withArgs('form.CashOnDelivery.validation').returns('foobar');

    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('getOrderInProgress').once().withArgs(order).returns($q.when(function () {
      expect(controller.refreshPaymentFormListener).to.be.equal('foobar');
      buildOrderMock.verify();
      eventMock.verify();
    }));

    sinon.stub(customer, 'getDefaultBillingAddress').returns($q.reject());

    order.update(stubs.getValidOrder().order);
    controller = $controller('PaymentController');
    expect(controller.refreshPaymentFormListener).to.not.exist;
    $rootScope.$apply();
  });

  describe('after activation', function () {
    beforeEach(function () {
      sinon.stub(customer, 'getDefaultBillingAddress').returns($q.reject());
      sinon.stub(buildOrder, 'getOrderInProgress', function (order) {
        order.update(stubs.getValidOrder().order);
        order.shippingAddress = { id: 1 };

        return $q.when(order);
      });
    });

    it('should remove item', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock
        .expects('removeItem')
        .once()
        .withArgs(order, 'ABC-123')
        .returns($q.when(order.update(stubs.getOrder().order)));

      controller = $controller('PaymentController');
      controller.removeItem('ABC-123');
      $rootScope.$apply();
      buildOrderMock.verify();
    });

    it('should remove item and redirect to cart if there are no more items', function () {
      sinon.stub(order, 'countItems').returns(0);

      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock
        .expects('removeItem')
        .once()
        .withArgs(order, 'ABC-123')
        .returns($q.when({}));

      navigateMock = sinon.mock(navigate);
      navigateMock.expects('toCart').twice();

      controller = $controller('PaymentController');
      controller.removeItem('ABC-123');
      $rootScope.$apply();
      buildOrderMock.verify();
      navigateMock.verify();
    });

    it('should update item quantity', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('updateQuantity').once().withArgs(order, 'ABC-123', 2).returns($q.when({}));

      controller = $controller('PaymentController');
      controller.updateQuantity('ABC-123', 2);
      $rootScope.$apply();
      buildOrderMock.verify();
    });

    it('should select installment', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('selectInstallmentQuantity').once().withArgs(order, 42).returns($q.when({}));

      controller = $controller('PaymentController');
      controller.selectInstallment(42);
      $rootScope.$apply();
      buildOrderMock.verify();
    });

    it('should not select invalid installment', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('selectInstallmentQuantity').never();

      controller = $controller('PaymentController');
      controller.selectInstallment(null);
      controller.selectInstallment();
      $rootScope.$apply();
      buildOrderMock.verify();
    });

    it('should check if order can be placed', function () {
      orderMock = sinon.mock(order);
      orderMock.expects('isValid').once().returns(true);

      controller = $controller('PaymentController');
      controller.isPendingPaymentInfo = false;
      controller.isPendingExtraInfo = false;
      $rootScope.$apply();

      expect(controller.canPlaceOrder()).to.be.true;
      orderMock.verify();
    });

    it('should allow order to be placed if there is pending info but is Zero_Payment', function () {
      orderMock = sinon.mock(order);
      orderMock.expects('isValid').once().returns(true);

      controller = $controller('PaymentController');
      controller.order.paymentMethod = 'Zero_Payment';
      controller.isPendingPaymentInfo = true;
      $rootScope.$apply();

      expect(controller.canPlaceOrder()).to.be.true;
      orderMock.verify();
    });

    it('should allow order to be placed if there is pending info but is completed via wallet', function () {
      orderMock = sinon.mock(order);
      orderMock.expects('isValid').once().returns(true);

      controller = $controller('PaymentController');
      order.wallet.totalPointsUsed = 100;
      order.grandTotal = 0;
      controller.isPendingPaymentInfo = true;
      $rootScope.$apply();

      expect(controller.canPlaceOrder()).to.be.true;
      orderMock.verify();
    });

    it('should not allow order to be placed if pending payment info', function () {
      controller = $controller('PaymentController');
      controller.isPendingPaymentInfo = true;
      $rootScope.$apply();

      expect(controller.canPlaceOrder()).to.be.false;
    });

    it('should not allow order to be placed if pending extra info', function () {
      controller = $controller('PaymentController');
      controller.isPendingPaymentInfo = false;
      controller.isPendingExtraInfo = true;
      $rootScope.$apply();

      expect(controller.canPlaceOrder()).to.be.false;
    });

    it('should not allow order to be placed if order is invalid', function () {
      orderMock = sinon.mock(order);
      orderMock.expects('isValid').once().returns(false);

      controller = $controller('PaymentController');
      $rootScope.$apply();

      expect(controller.canPlaceOrder()).to.be.false;
      orderMock.verify();
    });

    it('should finish and go to success page', function () {
      placeOrderMock = sinon.mock(placeOrder);
      placeOrderMock
        .expects('place')
        .once()
        .withArgs(order, {}, { foo: 'bar' })
        .returns($q.when({ redirect: null }));

      navigateMock = sinon.mock(navigate);
      navigateMock.expects('toCheckoutSuccess').once();

      controller = $controller('PaymentController');
      controller.extraFields = { foo: 'bar' };
      controller.finish();
      $rootScope.$apply();
      placeOrderMock.verify();
      navigateMock.verify();
      expect(controller.isPendingInfo()).to.be.true;
    });

    it('should finish and redirect to hosted payment page if required', function () {
      placeOrderMock = sinon.mock(placeOrder);
      placeOrderMock
        .expects('place')
        .once()
        .withArgs(order, {}, {})
        .returns($q.when({
          redirect: {
            method: 'POST',
            target: '/foo',
            body: 'foobar',
          },
        }));

      hostedPaymentMock = sinon.mock(hostedPayment);
      hostedPaymentMock.expects('redirect').once().withArgs('POST', '/foo', 'foobar');

      controller = $controller('PaymentController');
      controller.finish();
      $rootScope.$apply();
      placeOrderMock.verify();
      hostedPaymentMock.verify();
      expect(controller.isPendingInfo()).to.be.true;
    });

    it('should handle error when finishing', function () {
      placeOrderMock = sinon.mock(placeOrder);
      placeOrderMock.expects('place').once().returns($q.reject({}));

      controller = $controller('PaymentController');
      controller.finish();
      $rootScope.$apply();
      placeOrderMock.verify();
      expect(controller.isPendingInfo()).to.be.false;
    });

    it('should display address book', function () {
      ngDialogMock = sinon.mock(ngDialog);
      ngDialogMock.expects('open').once().withArgs({
        template: '<address-book selectable="true" type="billing"></address-book>',
        plain: true,
      }).returns('foo');

      controller = $controller('PaymentController');
      expect(controller.addressBook).to.be.null;
      controller.showAddressBook('billing');
      $rootScope.$apply();
      expect(controller.addressBook).to.be.equal('foo');
    });

    it('should handle shipping address selection', function () {
      addressBook = {
        close: sinon.spy(),
      };

      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setShippingAddressById').once().withArgs(order, 42);

      controller = $controller('PaymentController');
      controller.addressBook = addressBook;
      $rootScope.$apply();
      event.dispatch('address.selected_address', { type: 'shipping', address: { id: 42 } });
      buildOrderMock.verify();
      expect(addressBook.close.called).to.be.true;
    });

    it('should handle billing address selection', function () {
      addressBook = {
        close: sinon.spy(),
      };

      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setBillingAddress').once().withArgs(order, { foo: 'bar' });

      controller = $controller('PaymentController');
      controller.addressBook = addressBook;
      $rootScope.$apply();
      event.dispatch('address.selected_address', { type: 'billing', address: { foo: 'bar' } });
      buildOrderMock.verify();
      expect(addressBook.close.called).to.be.true;
    });

    it('should handle extra checkout fields form validation', function () {
      controller = $controller('PaymentController');
      $rootScope.$apply();

      event.dispatch('form.extra_checkout_fields.validation', { isValid: true });
      expect(controller.isPendingInfo()).to.be.false;

      event.dispatch('form.extra_checkout_fields.validation', { isValid: false });
      expect(controller.isPendingInfo()).to.be.true;
    });

    it('should display address book modal if there is no shipping address set', function () {
      ngDialogMock = sinon.mock(ngDialog);
      ngDialogMock.expects('open').once().withArgs({
        template: '<address-book selectable="true" type="shipping"></address-book>',
        plain: true,
      }).returns('foo');

      controller = $controller('PaymentController');
      order.shippingAddress = null;
      $rootScope.$apply();
      expect(controller.addressBook).to.be.equal('foo');
    });

    it('should check if payment methods can be shown', function () {
      controller = $controller('PaymentController');
      order.paymentMethod = 'Blah';
      order.undeliverables = [];
      expect(controller.canShowPaymentMethods()).to.be.true;
    });

    it('should not show payment methods if payment method is zero payment', function () {
      controller = $controller('PaymentController');
      order.paymentMethod = 'Zero_Payment';
      expect(controller.canShowPaymentMethods()).to.be.false;
    });

    it('should not show payment methods if order has undeliverables', function () {
      controller = $controller('PaymentController');
      order.undeliverables = ['foobar'];
      expect(controller.canShowPaymentMethods()).to.be.false;
    });

    it('should not show payment methods if order is completed via wallet', function () {
      controller = $controller('PaymentController');
      order.wallet.totalPointsUsed = 100;
      order.grandTotal = 0;
      expect(controller.canShowPaymentMethods()).to.be.false;
    });

    it('should show shipping options', function () {
      stateMock = sinon.mock($state);
      stateMock.expects('go').once().withArgs('options');

      controller = $controller('PaymentController');
      controller.showShippingOptions();

      stateMock.verify();
    });

    it('should get saved cards list when selecting credit card payment method', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setBinNumber').once().withArgs(order, '123456');
      buildOrderMock.expects('setPaymentMethod').once().withArgs(order, 'CreditCard').returns($q.when(function () {
        expect(controller.refreshPaymentFormListener).to.be.equal('foobar');
        buildOrderMock.verify();
      }));

      order.savedCards = [{ id: 1, firstDigits: '123456' }];

      controller = $controller('PaymentController');
      controller.refreshPaymentFormListener = sinon.spy();

      controller.setPaymentMethod('CreditCard');
      $rootScope.$apply();
      expect(controller.paymentData).to.be.deep.equal({ tokenId: 1 });
      expect(controller.canShowCardForm).to.be.false;
    });

    it('should not update saved cards list when there are no cards', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setPaymentMethod').once().withArgs(order, 'CreditCard').returns($q.when(function () {
        expect(controller.refreshPaymentFormListener).to.be.equal('foobar');
        buildOrderMock.verify();
      }));

      controller = $controller('PaymentController');
      controller.refreshPaymentFormListener = sinon.spy();

      controller.setPaymentMethod('CreditCard');
      $rootScope.$apply();
      expect(controller.paymentData).to.be.empty;
    });

    it('should set card form defaults', function () {
      controller = $controller('PaymentController');
      expect(controller.canShowCardForm).to.be.true;
    });

    it('should use saved card', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setBinNumber').once().withArgs(order, '123456');

      controller = $controller('PaymentController');
      controller.useCard(1, '123456');

      expect(controller.paymentData.tokenId).to.be.equal(1);
      expect(controller.canShowCardForm).to.be.false;
    });

    it('should show card form', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('setBinNumber').once().withArgs(order, '123456');

      controller = $controller('PaymentController');
      controller.useCard(1, '123456');
      controller.showCardForm();

      expect(controller.paymentData.tokenId).to.not.exist;
      expect(controller.canShowCardForm).to.be.true;
      expect(controller.order.creditCardBinNumber).to.be.null;
    });

    it('should finish with Transbank and customize loader', function () {
      placeOrderMock = sinon.mock(placeOrder);
      placeOrderMock
        .expects('place')
        .once()
        .withArgs(order, {}, { foo: 'bar' })
        .returns($q.when({ redirect: null }));

      navigateMock = sinon.mock(navigate);
      navigateMock.expects('toCheckoutSuccess').once();

      eventMock = sinon.mock(event);
      eventMock.expects('dispatch').once().withArgs('spinner.customize', { class: 'loading-overlay-webpay-icon' });

      controller = $controller('PaymentController');
      controller.extraFields = { foo: 'bar' };
      order.paymentMethod = 'TransbankWebpay_Payment';
      controller.finish();
      $rootScope.$apply();
      placeOrderMock.verify();
      navigateMock.verify();
      eventMock.verify();
      expect(controller.isPendingInfo()).to.be.true;
    });
  });
});
