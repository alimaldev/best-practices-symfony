describe('Address controller', function () {
  var controller;

  beforeEach(function () {
    bard.appModule('linio.checkout', function ($provide) {
      $provide.constant('config', { shippingOptionsEnabled: false });
    });

    bard.inject('$controller', '$q', '$rootScope', '$state', 'config', 'customer', 'order', 'buildOrder');
    $state.go = function () { return; };
  });

  bard.verifyNoOutstandingHttpRequests();

  it('should handle customer with existing addresses', function () {
    config.shippingOptionsEnabled = false;
    sinon.stub(customer, 'getAddresses').returns($q.when(stubs.getAddresses()));
    stateMock = sinon.mock($state);
    stateMock.expects('go').once().withArgs('payment');
    controller = $controller('AddressController', { $scope: $rootScope });

    $rootScope.$apply();
    stateMock.verify();
  });

  it('should handle customer with existing addresses and shipping options is enabled', function () {
    config.shippingOptionsEnabled = true;
    sinon.stub(customer, 'getAddresses').returns($q.when(stubs.getAddresses()));
    stateMock = sinon.mock($state);
    stateMock.expects('go').once().withArgs('options');
    controller = $controller('AddressController', { $scope: $rootScope });

    $rootScope.$apply();
    stateMock.verify();
  });

  it('should handle customer without addresses', function () {
    sinon.stub(customer, 'getAddresses').returns($q.when({}));
    controller = $controller('AddressController', { $scope: $rootScope });

    $rootScope.$apply();
    expect(controller.isReady).to.be.true;
  });

  it('should handle address creation event', function () {
    sinon.stub(customer, 'getAddresses').returns($q.when({}));
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('setShippingAddressById').once().withArgs(order, 42).returns($q.when([]));

    stateMock = sinon.mock($state);
    stateMock.expects('go').once().withArgs('payment');

    controller = $controller('AddressController', { $scope: $rootScope });
    $rootScope.$apply();
    $rootScope.$broadcast('address.new_address_created', { id: 42 });
    buildOrderMock.verify();
  });
});
