describe('luhnExceptions directive', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$compile');
  });

  it('should detect empty card number', function () {
    scope = $rootScope.$new();
    element = $compile('<form name="form"><input name="cc" ng-model="cc" cc-number luhn-exceptions="[]" /></form>')(scope);
    scope.$digest();

    scope.form.cc.$setViewValue('');
    scope.$apply();

    expect(scope.form.cc.$valid).to.be.true;
  });

  it('should detect valid card number via parent validator', function () {
    scope = $rootScope.$new();
    element = $compile('<form name="form"><input name="cc" ng-model="cc" cc-number luhn-exceptions="[]" /></form>')(scope);
    scope.$digest();

    scope.form.cc.$setViewValue('4242 4242 4242 4242');
    scope.$apply();

    expect(scope.form.cc.$valid).to.be.true;
  });

  it('should detect invalid card number via parent validator', function () {
    scope = $rootScope.$new();
    element = $compile('<form name="form"><input name="cc" ng-model="cc" cc-number luhn-exceptions="[]" /></form>')(scope);
    scope.$digest();

    scope.form.cc.$setViewValue('1100 1100 1100 1100');
    scope.$apply();

    expect(scope.form.cc.$valid).to.be.false;
  });

  it('should bypass card if bin is within exception list', function () {
    scope = $rootScope.$new();
    element = $compile('<form name="form"><input name="cc" ng-model="cc" cc-number luhn-exceptions="[\'589562\', \'402917\']" /></form>')(scope);
    scope.$digest();

    scope.form.cc.$setViewValue('5895 6266 2980 4003');
    scope.$apply();

    expect(scope.form.cc.$valid).to.be.true;
  });
});
