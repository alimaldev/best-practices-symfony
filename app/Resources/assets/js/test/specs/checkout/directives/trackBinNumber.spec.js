describe('trackBinNumber directive', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$compile', 'order', 'buildOrder');
  });

  it('should not track empty card number input field', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('setBinNumber').never();

    scope = $rootScope.$new();
    element = $compile('<input ng-model="cardNumber" track-bin-number></input>')(scope);
    scope.$digest();

    expect(element).to.have.length(1);
    buildOrderMock.verify();
  });

  it('should not track incomplete card number input field', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('setBinNumber').never();

    scope = $rootScope.$new();
    scope.cardNumber = '123456';
    element = $compile('<input ng-model="cardNumber" track-bin-number></input>')(scope);
    scope.$digest();

    expect(element).to.have.length(1);
    buildOrderMock.verify();
  });

  it('should track card number input field', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('setBinNumber').once().withArgs(order, '411111');

    scope = $rootScope.$new();
    scope.cardNumber = '4111 1111 1111 1111';
    element = $compile('<input ng-model="cardNumber" track-bin-number></input>')(scope);
    scope.$digest();

    expect(element).to.have.length(1);
    buildOrderMock.verify();
  });
});
