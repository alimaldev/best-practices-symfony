describe('prefill directive', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$compile');
  });

  it('should prefill model', function () {
    scope = $rootScope.$new();
    scope.foo = 'bar';
    scope.cardNumber = '';
    element = $compile('<input ng-model="cardNumber" prefill="foo"></input>')(scope);
    scope.$digest();

    expect(element).to.have.length(1);
    expect(scope.cardNumber).to.be.equal('bar');
  });

  it('should not prefill if model is not empty', function () {
    scope = $rootScope.$new();
    scope.foo = 'bar';
    scope.cardNumber = '123';
    element = $compile('<input ng-model="cardNumber" prefill="foo"></input>')(scope);
    scope.$digest();

    expect(element).to.have.length(1);
    expect(scope.cardNumber).to.be.equal('123');
  });
});
