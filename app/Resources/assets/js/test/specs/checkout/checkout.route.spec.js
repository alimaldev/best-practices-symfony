describe('checkout routing configuration', function () {
  beforeEach(function () {
    bard.appModule('linio.checkout');
    bard.inject('$rootScope', '$state');
    $state.go = function () { return; };
  });

  it('should change states normally if it has flow', function () {
    stateMock = sinon.mock($state);
    stateMock.expects('go').never();

    $rootScope.$broadcast('$stateChangeStart', { name: 'payment' }, null, { name: 'address' });
    stateMock.verify();
  });

  it('should redirect to address if it is a direct access', function () {
    stateMock = sinon.mock($state);
    stateMock.expects('go').once().withArgs('address');

    $rootScope.$broadcast('$stateChangeStart', { name: 'payment' }, null, { name: '' });
    stateMock.verify();
  });
});
