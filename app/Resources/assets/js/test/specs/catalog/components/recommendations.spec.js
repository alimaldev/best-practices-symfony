describe('recommendations component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.catalog', function ($provide) {
      $provide.constant('config', { recommendationsUrl: 'test-url' });
      $provide.service('jetloreFeedService', function () {});
    });

    bard.inject('$rootScope', '$componentController', '$q', 'search');
  });

  it('should be attached to the scope', function () {
    scope = $rootScope.$new();
    component = $componentController('recommendations', { $scope: scope });
    expect(scope.recommendations).to.be.equal(component);
  });

  it('should initialize feed list', function () {
    searchMock = sinon.mock(search);
    searchMock.expects('getFeedList').once().withArgs('foo').returns($q.when({ foo: { items: [] } }));
    searchMock.expects('getRecommendations').once().withArgs({ items: [] }).returns($q.when([1, 2, 3]));

    scope = $rootScope.$new();
    component = $componentController('recommendations', { $scope: scope });
    component.section = 'foo';
    component.$onInit();

    $rootScope.$apply();
    searchMock.verify();

    expect(component.sections).to.be.deep.equal({ foo: { items: [1, 2, 3] } });
  });

  it('should initialize feed list with sku binding', function () {
    searchMock = sinon.mock(search);
    searchMock.expects('getFeedList').once().withArgs('foo', 'SKU123').returns($q.when({ foo: { items: [] } }));
    searchMock.expects('getRecommendations').once().withArgs({ items: [] }).returns($q.when([1, 2, 3]));

    scope = $rootScope.$new();
    component = $componentController('recommendations', { $scope: scope });
    component.section = 'foo';
    component.sku = 'SKU123';
    component.$onInit();

    $rootScope.$apply();
    searchMock.verify();

    expect(component.sections).to.be.deep.equal({ foo: { items: [1, 2, 3] } });
  });
});
