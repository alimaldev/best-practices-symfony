describe('event service', function () {
  beforeEach(function () {
    bard.appModule('linio.catalog', function ($provide) {
      $provide.constant('config', { recommendationsUrl: 'http://birdie/', feedKey: 123 });
    });

    bard.inject('$rootScope', '$httpBackend', '$q', '$cookies', '$log', 'search', 'config');
  });

  it('should be created successfully', function () {
    expect(search).to.exist;
  });

  it('should be able to get feed list', function () {
    $httpBackend.when('GET', 'http://birdie/foo?key=123').respond(200, { status: 'ok', feeds: ['123'] });

    search.getFeedList('foo').then(function (sections) {
      expect(sections).to.be.deep.equal(['123']);
    });

    $httpBackend.flush();
  });

  it('should be able to get empty feed list on error', function () {
    $httpBackend.when('GET', 'http://birdie/foo?key=123').respond(200, { status: 'ok', feeds: null });

    search.getFeedList('foo').then(function (sections) {
      expect(sections).to.be.deep.equal([]);
    });

    $httpBackend.flush();
  });

  it('should be able to get feed list with user id', function () {
    cookiesMock = sinon.mock($cookies);
    cookiesMock.expects('get').once().withArgs('linio_id').returns('foobar');
    $httpBackend.when('GET', 'http://birdie/foo?key=123&uuid=foobar').respond(200, { status: 'ok', feeds: ['123'] });

    search.getFeedList('foo').then(function (sections) {
      expect(sections).to.be.deep.equal(['123']);
    });

    $httpBackend.flush();
  });

  it('should be able to handle error getting feed list', function () {
    $httpBackend.when('GET', 'http://birdie/foo?key=123').respond(400, { message: 'Oh no!' });
    search.getFeedList('foo').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: Oh no!');
    });

    $httpBackend.flush();
  });

  it('should be able to get feed list with sku binding', function () {
    $httpBackend.when('GET', 'http://birdie/foo?key=123&sku=SKU123').respond(200, { status: 'ok', feeds: ['123'] });

    search.getFeedList('foo', 'SKU123').then(function (sections) {
      expect(sections).to.be.deep.equal(['123']);
    });

    $httpBackend.flush();
  });

  it('should be able to get empty feed list on error with sku binding', function () {
    $httpBackend.when('GET', 'http://birdie/foo?key=123&sku=SKU123').respond(200, { status: 'ok', feeds: null });

    search.getFeedList('foo', 'SKU123').then(function (sections) {
      expect(sections).to.be.deep.equal([]);
    });

    $httpBackend.flush();
  });

  it('should be able to get feed list with user id and sku binding', function () {
    cookiesMock = sinon.mock($cookies);
    cookiesMock.expects('get').once().withArgs('linio_id').returns('foobar');
    $httpBackend.when('GET', 'http://birdie/foo?key=123&sku=SKU123&uuid=foobar').respond(200, { status: 'ok', feeds: ['123'] });

    search.getFeedList('foo', 'SKU123').then(function (sections) {
      expect(sections).to.be.deep.equal(['123']);
    });

    $httpBackend.flush();
  });

  it('should be able to handle error getting feed list with sku binding', function () {
    $httpBackend.when('GET', 'http://birdie/foo?key=123&sku=SKU123').respond(400, { message: 'Oh no!' });
    search.getFeedList('foo', 'SKU123').catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: Oh no!');
    });

    $httpBackend.flush();
  });

  it('should be able to get product recommendations', function () {
    var section = {
      url: 'http://foobar',
      type: 'products',
    };

    $httpBackend.when('GET', section.url).respond(200, { items: stubs.getProductRecommendations() });
    search.getRecommendations(section).then(function (products) {
      expect(products).to.be.deep.equal([
        {
          title: 'Bicicleta Lattizero',
          brand: 'Lattizero',
          image: 'https://media.linio.com.ec/p/lattizero-2480-6119-1-product.jpg',
          url: '//www.linio.com.ec/p/bicicleta-lattizero-mtb-rj-rim-26-talla-17-unisex-roja-osgkum',
          percentageOff: 16,
          originalPrice: 246.98,
          price: 209.93,
        },
        {
          title: 'Galaxy J1 Ace Duos Sm',
          brand: 'Samsung',
          image: 'https://media.linio.com.ec/p/samsung-7378-33401-1-product.jpg',
          url: '//www.linio.com.ec/p/samsung-galaxy-j1-ace-duos-sm-j110m-4g-lte-8gb-tpvqbs',
          price: 139.99,
        },
      ]);
    });

    $httpBackend.flush();
  });

  it('should be able to get category recommendations', function () {
    var section = {
      url: 'http://foobar',
      type: 'categories',
    };

    $httpBackend.when('GET', section.url).respond(200, { items: stubs.getCategoryRecommendations() });
    search.getRecommendations(section).then(function (categories) {
      expect(categories).to.be.deep.equal([
        {
          title: 'desbloqueados',
          image: 'https://media.linio.com.ec/p/samsung-7384-09848-1-product.jpg',
          url: '//www.linio.com.ec/c/celulares-y-smartphones/desbloqueados',
        },
        {
          title: 'celulares y smartphones',
          image: 'https://media.linio.com.ec/p/samsung-5335-46428-1-product.jpg',
          url: '//www.linio.com.ec/c/celulares-y-tablets/celulares-y-smartphones',
        },
      ]);
    });

    $httpBackend.flush();
  });

  it('should be able to handle error getting category recommendations', function () {
    var section = {
      url: 'http://foobar',
      type: 'categories',
    };

    $httpBackend.when('GET', section.url).respond(400, { message: 'Oh no!' });
    search.getRecommendations(section).catch(function (error) {
      expect(error).to.exist;
      expect($log.error.logs[0][0]).to.be.equal('Error: Oh no!');
    });

    $httpBackend.flush();
  });
});
