describe('invoiceRequestForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.account');
    bard.inject('$rootScope', '$componentController', '$q', 'customer');

    scope = $rootScope.$new();
    component = $componentController('invoiceRequestForm', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.invoiceRequestForm).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.invoices).to.be.empty;
    expect(component.showForm).to.be.true;
    expect(component.hasError).to.be.false;
  });

  it('should submit invoice request', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('requestInvoice').once().withArgs('123', 'foo').returns($q.when(['bar']));

    component.orderNumber = '123';
    component.input = 'foo';
    component.submit();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.hasError).to.be.false;
    expect(component.invoices).to.be.deep.equal(['bar']);
  });

  it('should handle error submitting invoice request', function () {
    customerMock = sinon.mock(customer);
    customerMock.expects('requestInvoice').once().withArgs('123', 'foo').returns($q.reject({}));

    component.orderNumber = '123';
    component.input = 'foo';
    component.submit();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.hasError).to.be.true;
    expect(component.invoices).to.be.empty;
  });
});
