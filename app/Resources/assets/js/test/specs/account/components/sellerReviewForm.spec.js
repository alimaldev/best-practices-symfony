describe('sellerReviewForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.account');
    bard.inject('$rootScope', '$componentController', '$q', 'customer');

    scope = $rootScope.$new();
    component = $componentController('sellerReviewForm', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.sellerReview).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.title).to.be.equal('');
    expect(component.detail).to.be.equal('');
    expect(component.rating).to.be.equal(0);
    expect(component.showForm).to.be.true;
    expect(component.hasError).to.be.false;
    expect(component.hasSuccess).to.be.false;
  });

  it('should not submit form without rating', function () {
    expect(component.canSubmit()).to.be.false;

    component.title = 'Foo';
    component.detail = 'Bar';
    expect(component.canSubmit()).to.be.false;

    component.rating = '4';
    expect(component.canSubmit()).to.be.true;
  });

  it('should not submit form without title', function () {
    expect(component.canSubmit()).to.be.false;

    component.detail = 'Bar';
    component.rating = '4';
    expect(component.canSubmit()).to.be.false;

    component.title = 'Foo';
    expect(component.canSubmit()).to.be.true;
  });

  it('should not submit form without detail', function () {
    expect(component.canSubmit()).to.be.false;

    component.title = 'Foo';
    component.rating = '4';
    expect(component.canSubmit()).to.be.false;

    component.detail = 'Bar';
    expect(component.canSubmit()).to.be.true;
  });

  it('should submit seller review', function () {
    var review = {
      rating: 3,
      title: 'foo',
      detail: 'barfoo',
    };

    customerMock = sinon.mock(customer);
    customerMock.expects('reviewSeller').once().withArgs('123', 1, review).returns($q.when({}));

    component.orderNumber = '123';
    component.sellerId = 1;
    component.rating = review.rating;
    component.title = review.title;
    component.detail = review.detail;
    component.submit();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.showForm).to.be.false;
    expect(component.hasSuccess).to.be.true;
    expect(component.hasError).to.be.false;
  });

  it('should handle error submitting seller review', function () {
    var review = {
      rating: 3,
      title: 'foo',
      detail: 'barfoo',
    };

    customerMock = sinon.mock(customer);
    customerMock.expects('reviewSeller').once().withArgs('123', 1, review).returns($q.reject({}));

    component.orderNumber = '123';
    component.sellerId = 1;
    component.rating = review.rating;
    component.title = review.title;
    component.detail = review.detail;
    component.submit();
    $rootScope.$apply();
    customerMock.verify();

    expect(component.showForm).to.be.true;
    expect(component.hasSuccess).to.be.false;
    expect(component.hasError).to.be.true;
  });
});
