describe('invoiceRequest component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.account');
    bard.inject('$rootScope', '$componentController', '$q', 'ngDialog');

    scope = $rootScope.$new();
    component = $componentController('invoiceRequest', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.invoiceRequest).to.be.equal(component);
    expect(component.addressBook).to.be.null;
    expect(component.selectedAddress).to.be.null;
    expect(component.listeners).to.be.empty;
  });

  it('should register listeners', function () {
    component.$onInit();
    $rootScope.$apply();

    expect(component.listeners).to.not.be.empty;
  });

  it('should be clearing listeners on destruct', function () {
    var spyListener = sinon.spy();
    component.listeners = [spyListener];
    component.$onDestroy();

    expect(spyListener.called).to.be.true;
  });

  it('should display address book', function () {
    ngDialogMock = sinon.mock(ngDialog);
    ngDialogMock.expects('open').once().withArgs({
      template: '<address-book selectable="true" type="shipping" />',
      plain: true,
    }).returns('foo');

    expect(component.addressBook).to.be.null;
    component.show();
    $rootScope.$apply();
    expect(component.addressBook).to.be.equal('foo');
  });

  it('should display invoice request form after address selection', function () {
    addressBook = {
      close: sinon.spy(),
    };

    ngDialogMock = sinon.mock(ngDialog);
    ngDialogMock.expects('open').once().withArgs({
      template: '<invoice-request-form input="invoiceRequest.selectedAddress" order-number="invoiceRequest.orderNumber" />',
      plain: true,
      scope: scope,
    }).returns('foo');

    component.addressBook = addressBook;
    component.$onInit();
    $rootScope.$broadcast('address.selected_address', { type: 'shipping', address: { id: 42 } });

    $rootScope.$apply();
    expect(addressBook.close.called).to.be.true;
  });
});
