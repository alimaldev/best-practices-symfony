describe('Order return controller', function () {
  var controller;

  beforeEach(function () {
    bard.appModule('linio.account');
    bard.inject('$controller', '$q', '$rootScope', 'customer', 'navigate');
  });

  bard.verifyNoOutstandingHttpRequests();

  it('should detect if return can be submitted', function () {
    controller = $controller('OrderReturnController', { $scope: $rootScope });
    expect(controller.canSubmit()).to.be.false;

    controller.items = {
      'ABC-123': { selected: false },
      'ABC-456': { selected: true },
    };

    expect(controller.canSubmit()).to.be.true;
  });

  it('should submit selected items for return', function () {
    navigateMock = sinon.mock(navigate);
    navigateMock.expects('to').once().withArgs('/account/order/123/return/confirmation');

    customerMock = sinon.mock(customer);
    customerMock
      .expects('returnOrderItem')
      .once()
      .withArgs('123', { sku: 'ABC-456', quantity: 1, selected: true })
      .returns($q.when());
    customerMock
      .expects('returnOrderItem')
      .once()
      .withArgs('123', { sku: 'ABC-789', quantity: 3, selected: true })
      .returns($q.when(function () {
        customerMock.verify();
        navigateMock.verify();

        expect(controller.hasSuccess).to.be.true;
        expect(controller.items['ABC-456'].selected).to.be.false;
        expect(controller.items['ABC-789'].selected).to.be.false;
      }));

    controller = $controller('OrderReturnController', { $scope: $rootScope });

    controller.items = {
      'ABC-123': { selected: false, quantity: 2 },
      'ABC-456': { selected: true, quantity: 1 },
      'ABC-789': { selected: true, quantity: 3 },
    };

    controller.submit('123');
    $rootScope.$apply();
  });
});
