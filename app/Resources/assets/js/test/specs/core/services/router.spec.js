describe('router service', function () {
  var routerProviderInstance;

  beforeEach(function () {
    bard.appModule('linio.core', function (routerProvider) {
      routerProviderInstance = routerProvider;
    });

    bard.inject('$rootScope', '$state', '$location', 'router', 'logger');
    $state.get = function () { };
  });

  it('should be created successfully', function () {
    expect(router).to.exist;
    routerProviderInstance.configure({ foo: 'bar' });
  });

  it('should get states', function () {
    stateMock = sinon.mock($state);
    stateMock.expects('get').once();

    router.getStates();
    stateMock.verify();
  });

  it('should handle state change error', function () {
    locationMock = sinon.mock($location);
    locationMock.expects('path').once().withArgs('/');

    loggerMock = sinon.mock(logger);
    loggerMock.expects('warning').once().withArgs('Unable to change state: oops', ['foobar']);

    $rootScope.$broadcast('$stateChangeError', 'foobar', {}, 'barfoo', {}, { data: 'oops' });
    locationMock.verify();
    loggerMock.verify();
  });

  it('should not handle concurrent state change error', function () {
    locationMock = sinon.mock($location);
    locationMock.expects('path').once().withArgs('/');

    loggerMock = sinon.mock(logger);
    loggerMock.expects('warning').once().withArgs('Unable to change state: oops', ['foobar']);

    $rootScope.$broadcast('$stateChangeError', 'foobar', {}, 'barfoo', {}, { data: 'oops' });
    $rootScope.$broadcast('$stateChangeError', 'foobar', {}, 'barfoo', {}, { data: 'oops' });
    locationMock.verify();
    loggerMock.verify();
  });
});
