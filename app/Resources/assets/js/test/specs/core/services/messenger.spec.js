describe('messenger service', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('messenger');
  });

  it('should be created successfully', function () {
    expect(messenger).to.exist;
  });

  it('should report error message', function () {
    messenger.error('foobar');
    report = messenger.report();
    expect(report.content).to.be.equal('foobar');
    expect(report.type).to.be.equal('error');
  });

  it('should report info message', function () {
    messenger.info('foobar');
    report = messenger.report();
    expect(report.content).to.be.equal('foobar');
    expect(report.type).to.be.equal('info');
  });

  it('should report success message', function () {
    messenger.success('foobar');
    report = messenger.report();
    expect(report.content).to.be.equal('foobar');
    expect(report.type).to.be.equal('success');
  });

  it('should report warning message', function () {
    messenger.warning('foobar');
    report = messenger.report();
    expect(report.content).to.be.equal('foobar');
    expect(report.type).to.be.equal('warning');
  });

  it('should shift message queue when reporting', function () {
    messenger.warning('foobar');
    messenger.info('foobar');
    expect(messenger.queue).to.have.length(2);

    messenger.report();
    expect(messenger.queue).to.have.length(1);

    messenger.report();
    expect(messenger.report()).to.be.empty;
  });

  it('should clear queue', function () {
    messenger.queue = ['foobar'];
    messenger.clear();
    expect(messenger.queue).to.be.empty;
  });
});
