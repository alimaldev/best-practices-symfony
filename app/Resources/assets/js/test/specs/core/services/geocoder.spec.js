describe('geocoder service', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$rootScope', 'geocoder');
  });

  it('should be created successfully', function () {
    expect(geocoder).to.exist;
  });

  it('should not get coordinates if Google Maps lib is not loaded', function () {
    google.maps.Geocoder = null;

    geocoder.getCoordinates('Rua Sem Nome, 45').catch(function (error) {
      expect(error).to.be.equal('Google Maps library not loaded.');
    });

    $rootScope.$apply();
  });

  it('should get coordinates from address', function () {
    var geocodeResult = [
      {
        geometry: {
          location: {
            lat: function () {
              return 42;
            },

            lng: function () {
              return 42;
            },

          },
        },
      },
    ];

    var geocodeStub = sinon.stub();
    geocodeStub
      .withArgs({ address: 'Rua Sem Nome, 45' })
      .callsArgWith(1, geocodeResult, 'OK');

    google.maps.Geocoder = function () {
      return {
        geocode: geocodeStub,
      };
    };

    geocoder.getCoordinates('Rua Sem Nome, 45').then(function (coordinates) {
      expect(coordinates).to.be.deep.equal({ lat: 42, lng: 42 });
    });

    $rootScope.$apply();
  });

  it('should detect error getting coordinates from address', function () {
    var geocodeStub = sinon.stub();
    geocodeStub
      .withArgs({ address: 'Rua Sem Nome, 45' })
      .callsArgWith(1, null, 'NOT_OK');

    google.maps.Geocoder = function () {
      return {
        geocode: geocodeStub,
      };
    };

    geocoder.getCoordinates('Rua Sem Nome, 45').catch(function (error) {
      expect(error).to.be.equal('NOT_OK');
    });

    $rootScope.$apply();
  });
});
