describe('event service', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$rootScope', 'event');
  });

  it('should be created successfully', function () {
    expect(event).to.exist;
  });

  it('should dispatch event', function () {
    rootScope = sinon.mock($rootScope);
    rootScope.expects('$broadcast').once().withArgs('foobar', { foo: 'bar' });

    event.dispatch('foobar', { foo: 'bar' });
    rootScope.verify();
  });

  it('should listen to event', function () {
    rootScope = sinon.mock($rootScope);
    rootScope.expects('$on').once().withArgs('foobar', null);

    event.listen('foobar', null);
    rootScope.verify();
  });
});
