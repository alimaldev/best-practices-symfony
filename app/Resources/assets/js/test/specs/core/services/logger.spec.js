describe('logger service', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$log', 'logger');
  });

  it('should be created successfully', function () {
    expect(logger).to.exist;
  });

  it('should send log messages directly to $log', function () {
    logger.log('foobar');
    expect($log.log.logs[0][0]).to.be.equal('foobar');
  });

  it('should log info', function () {
    logger.info('foobar');
    expect($log.info.logs[0][0]).to.be.equal('Info: foobar');
  });

  it('should log warnings', function () {
    logger.warning('foobar');
    expect($log.warn.logs[0][0]).to.be.equal('Warning: foobar');
  });

  it('should log errors', function () {
    logger.error('foobar');
    expect($log.error.logs[0][0]).to.be.equal('Error: foobar');
  });

  it('should log debug data', function () {
    logger.debug('foobar');
    expect($log.debug.logs[0][0]).to.be.equal('Debug: foobar');
  });
});
