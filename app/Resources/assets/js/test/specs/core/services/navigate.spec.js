describe('navigate service', function () {
  beforeEach(function () {
    bard.appModule('linio.core', function ($provide) {
      $provide.value('$window', { location: { href: '' } });
    });

    bard.inject('event', 'navigate', '$window');
  });

  it('should be created successfully', function () {
    expect(navigate).to.exist;
  });

  it('should navigate to cart', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('spinner.spin');

    navigate.toCart();
    eventMock.verify();
    expect($window.location.href).to.be.equal('/cart');
  });

  it('should navigate to checkout success', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('spinner.spin');

    navigate.toCheckoutSuccess();
    eventMock.verify();
    expect($window.location.href).to.be.equal('/checkout/success');
  });

  it('should navigate to login', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('spinner.spin');

    navigate.toLogin();
    eventMock.verify();
    expect($window.location.href).to.be.equal('/account/login');
  });

  it('should navigate to arbitrary page', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('spinner.spin');

    navigate.to('/foo/bar');
    eventMock.verify();
    expect($window.location.href).to.be.equal('/foo/bar');
  });
});
