describe('geolocation service', function () {
  it('should get current position from navigator', function () {
    var getCurrentPosition = sinon.stub();
    getCurrentPosition.yields({ coords: { latitude: 42, longitude: 42 } });

    bard.appModule('linio.core', function ($provide) {
      $provide.value('$window', { navigator: { geolocation: { getCurrentPosition: getCurrentPosition } } });
    });

    bard.inject('$rootScope', '$window', '$q', 'geolocation');

    geolocation.getCurrentCoordinates().then(function (coordinates) {
      expect(coordinates).to.be.deep.equal({ lat: 42, lng: 42 });
    });

    $rootScope.$apply();
  });

  it('should detect error getting current position from navigator', function () {
    var getCurrentPosition = sinon.stub();
    getCurrentPosition.callsArgWith(1, 'foobar');

    bard.appModule('linio.core', function ($provide) {
      $provide.value('$window', { navigator: { geolocation: { getCurrentPosition: getCurrentPosition } } });
    });

    bard.inject('$rootScope', '$window', '$q', 'geolocation');

    geolocation.getCurrentCoordinates().catch(function (error) {
      expect(error).to.be.equal('foobar');
    });

    $rootScope.$apply();
  });

  it('should detect missing geolocation feature', function () {
    bard.appModule('linio.core', function ($provide) {
      $provide.value('$window', { navigator: {} });
    });

    bard.inject('$rootScope', '$window', '$q', 'geolocation');

    geolocation.getCurrentCoordinates().catch(function (error) {
      expect(error).to.be.equal('Missing geolocation support.');
    });

    $rootScope.$apply();
  });
});
