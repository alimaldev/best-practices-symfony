describe('broadcastValidation directive', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$rootScope', '$compile', 'event');
  });

  it('should notify form validation updates', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('form.foobar.validation', {
      isValid: true,
      error: {},
    });

    scope = $rootScope;
    element = $compile('<form name="foobar" broadcast-validation></form>')(scope);
    scope.$digest();

    eventMock.verify();
    expect(element).to.have.length(1);
  });

  it('should not notify validation updates if it is not a form', function () {
    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').never();

    scope = $rootScope;
    element = $compile('<form name="foobar" broadcast-validation></form>')(scope);
    delete scope.foobar.$valid;
    scope.$digest();

    eventMock.verify();
    expect(element).to.have.length(1);
  });
});
