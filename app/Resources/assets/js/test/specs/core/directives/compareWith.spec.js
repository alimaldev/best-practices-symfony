describe('compareWith directive', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$rootScope', '$compile');
  });

  it('should be valid against equal field', function () {
    scope = $rootScope;
    element = $compile('<form name="form"><input name="foo" ng-model="foo" /><input name="bar" ng-model="bar" compare-with="foo" /></form>')(scope);
    scope.$digest();

    scope.form.foo.$setViewValue('123456');
    scope.form.bar.$setViewValue('123456');
    scope.$apply();

    expect(scope.form.bar.$valid).to.be.true;
  });

  it('should be invalid against different field', function () {
    scope = $rootScope;
    element = $compile('<form name="form"><input name="foo" ng-model="foo" /><input name="bar" ng-model="bar" compare-with="foo" /></form>')(scope);
    scope.$digest();

    scope.form.foo.$setViewValue('456789');
    scope.form.bar.$setViewValue('123456');
    scope.$apply();

    expect(scope.form.bar.$valid).to.be.false;
  });
});
