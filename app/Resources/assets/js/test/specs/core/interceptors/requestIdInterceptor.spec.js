describe('request id $http interceptor', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$q', 'navigate', 'requestIdInterceptor');
  });

  it('should be created successfully', function () {
    expect(requestIdInterceptor).to.exist;
  });

  it('should add unique request id to all requests', function () {
    var config = {
      method: 'GET',
      headers: {},
    };
    requestIdInterceptor.request(config);

    expect(config.headers['X-Request-ID']).to.exist;
    expect(config.headers['X-Request-ID']).to.match(/^[a-z0-9]{32}$/);
  });

  it('should not add unique request id to requests with credentials', function () {
    var config = {
      method: 'GET',
      withCredentials: true,
      headers: {},
    };
    requestIdInterceptor.request(config);

    expect(config.headers['X-Request-ID']).to.not.exist;
  });
});
