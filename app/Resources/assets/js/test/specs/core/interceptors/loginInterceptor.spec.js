describe('login $http interceptor', function () {
  beforeEach(function () {
    bard.appModule('linio.core');
    bard.inject('$q', 'navigate', 'loginInterceptor');
  });

  it('should be created successfully', function () {
    expect(loginInterceptor).to.exist;
  });

  it('should handle unauthorized response error', function () {
    navigateMock = sinon.mock(navigate);
    navigateMock.expects('toLogin').once();

    loginInterceptor.responseError({ status: 401 });
    navigateMock.verify();
  });

  it('should handle forbidden response error', function () {
    navigateMock = sinon.mock(navigate);
    navigateMock.expects('toLogin').once();

    loginInterceptor.responseError({ status: 403 });
    navigateMock.verify();
  });

  it('should not handle response errors', function () {
    navigateMock = sinon.mock(navigate);
    navigateMock.expects('toLogin').never();

    loginInterceptor.responseError({ status: 500 });
    navigateMock.verify();
  });
});
