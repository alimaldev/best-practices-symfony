describe('shippingQuoteForm component', function () {
  var component;
  var scope;

  beforeEach(function () {
    bard.appModule('linio.cart');
    bard.inject('$rootScope', '$componentController', '$q', 'resolveLocation', 'order', 'buildOrder', 'event');

    scope = $rootScope.$new();
    component = $componentController('shippingQuoteForm', { $scope: scope });
  });

  it('should be attached to the scope', function () {
    expect(scope.addressForm).to.be.equal(component);
  });

  it('should be initialized', function () {
    expect(component.input).to.be.null;
    expect(component.regions).to.be.empty;
    expect(component.municipalities).to.be.empty;
    expect(component.cities).to.be.empty;
  });

  it('should load regions', function () {
    sinon.stub(resolveLocation, 'getRegions').returns($q.when({ foo: 'bar' }));
    component.loadRegions();
    $rootScope.$apply();

    expect(component.regions).to.deep.equal({ foo: 'bar' });
  });

  it('should load municipalities', function () {
    sinon.stub(resolveLocation, 'getMunicipalities').returns($q.when({ foo: 'bar' }));
    component.loadMunicipalities();
    $rootScope.$apply();

    expect(component.municipalities).to.deep.equal({ foo: 'bar' });
  });

  it('should load cities', function () {
    sinon.stub(resolveLocation, 'getCities').returns($q.when({ foo: 'bar' }));
    component.loadCities();
    $rootScope.$apply();

    expect(component.cities).to.deep.equal({ foo: 'bar' });
  });

  it('should quote address', function () {
    buildOrderMock = sinon.mock(buildOrder);
    buildOrderMock.expects('setShippingAddress').once().returns($q.when());

    eventMock = sinon.mock(event);
    eventMock.expects('dispatch').once().withArgs('address.quote_set');

    component.quote();
    $rootScope.$apply();
    buildOrderMock.verify();
    eventMock.verify();
  });
});
