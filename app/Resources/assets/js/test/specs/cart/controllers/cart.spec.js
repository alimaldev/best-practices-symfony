describe('Cart controller', function () {
  var controller;

  beforeEach(function () {
    bard.appModule('linio.cart');
    bard.inject('$controller', '$q', '$rootScope', 'ngDialog', 'order', 'buildOrder');
  });

  beforeEach(function () {
    sinon.stub(buildOrder, 'getOrderInProgress', function (order) {
      var deferred = $q.defer();
      deferred.resolve();
      order.update(stubs.getOrder().order);

      return deferred.promise;
    });

    controller = $controller('CartController');
    $rootScope.$apply();
  });

  bard.verifyNoOutstandingHttpRequests();

  it('should be created successfully', function () {
    expect(controller).to.be.defined;
  });

  describe('after activate', function () {
    beforeEach(function () {
      $rootScope.$apply();
    });

    it('should have order data', function () {
      expect(controller.data).to.not.be.empty;
      expect(controller.isReady).to.be.true;
    });

    it('should have order items', function () {
      expect(controller.data).to.exist;
      expect(controller.data.subTotal).to.equal(177.52);
      expect(controller.data.grandTotal).to.equal(377.52);
      expect(controller.data.taxAmount).to.equal(28.34);
    });

    it('should remove item', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('removeItem').once().withArgs(order, '123');

      controller.removeItem('123');
      $rootScope.$apply();
    });

    it('should update item quantity', function () {
      buildOrderMock = sinon.mock(buildOrder);
      buildOrderMock.expects('updateQuantity').once().withArgs(order, '123', 2);

      controller.updateQuantity('123', 2);
      $rootScope.$apply();
    });

    it('should toggle shipping quote form appearance', function () {
      expect(controller.showShippingQuoteForm).to.be.false;
      controller.toggleShippingQuoteForm();
      expect(controller.showShippingQuoteForm).to.be.true;
    });

    it('should display login modal', function () {
      ngDialogMock = sinon.mock(ngDialog);
      ngDialogMock.expects('open').once();

      controller.showLoginModal();
    });

    it('should handle shipping quote being set', function () {
      $rootScope.$broadcast('address.quote_set');
      expect(controller.showShippingQuoteForm).to.be.false;
    });
  });
});
