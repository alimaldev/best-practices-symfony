<?php

use Linio\Frontend\Security\OAuth\OAuthFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Yaml\Yaml;

class AppKernel extends Kernel
{
    const DEFAULT_COUNTRY_CODE = 'mx';

    /**
     * @var string
     */
    protected $countryCode;

    public function __construct($environment, $debug, Request $request = null)
    {
        parent::__construct($environment, $debug);

        if (!is_null($request) && $request->headers->has('X-Auth-Store')) {
            $this->countryCode = strtolower($request->headers->get('X-Auth-Store'));
        } elseif (getenv('FRONTEND_COUNTRY')) {
            $this->countryCode = getenv('FRONTEND_COUNTRY');
        } else {
            if ($this->environment != 'prod') {
                $config = Yaml::parse(file_get_contents(__DIR__ . '/config/parameters.yml'));
                $this->countryCode = $config['parameters']['country_code'] ?? self::DEFAULT_COUNTRY_CODE;
            } else {
                throw new Exception('Environment variable "FRONTEND_COUNTRY" must be set.');
            }
        }
    }

    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Liip\ThemeBundle\LiipThemeBundle(),
            new Linio\DynamicFormBundle\DynamicFormBundle(),
            new Linio\Frontend\Bundle\FormatterBundle\FormatterBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new Ekino\Bundle\NewRelicBundle\EkinoNewRelicBundle(),
            new EWZ\Bundle\RecaptchaBundle\EWZRecaptchaBundle(),
            new Dunglas\AngularCsrfBundle\DunglasAngularCsrfBundle(),
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
        ];

        if (in_array($this->environment, ['dev', 'test', 'record'])) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Elao\WebProfilerExtraBundle\WebProfilerExtraBundle();
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
        }

        return $bundles;
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function prepareContainer(ContainerBuilder $container)
    {
        parent::prepareContainer($container);

        /** @var SecurityExtension $extension */
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new OAuthFactory());
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config/config_' . $this->environment . '.yml');

        if ($this->countryCode) {
            $loader->load(__DIR__ . '/config/countries/config_' . $this->countryCode . '.yml');

            $countryParametersFile = sprintf('%s/config/parameters.%s.yml', __DIR__, $this->countryCode);

            if (file_exists($countryParametersFile)) {
                $loader->load($countryParametersFile);
            }
        }
    }

    /**
     * {@inheritdoc}
     *
     * @api
     */
    public function getCacheDir()
    {
        return sprintf('%s/cache/%s/%s', $this->rootDir, $this->countryCode, $this->environment);
    }

    /**
     * {@inheritdoc}
     *
     * @api
     */
    public function getLogDir()
    {
        return sprintf('%s/logs/%s', $this->rootDir, $this->countryCode);
    }
}
