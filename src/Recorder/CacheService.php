<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use Linio\Component\Cache\CacheService as LinioCache;

class CacheService extends LinioCache
{
    /**
     * @var WriteFixture
     */
    protected $fixtureRecorder;

    /**
     * CacheServiceRecorder constructor.
     *
     * @param array $cacheConfig
     * @param WriteFixture $fixtureRecorder
     */
    public function __construct(array $cacheConfig, WriteFixture $fixtureRecorder)
    {
        parent::__construct($cacheConfig);
        $this->fixtureRecorder = $fixtureRecorder;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key)
    {
        $result = parent::get($key);

        $request = sprintf('%s:%s', $this->getNamespace(), $key);

        $this->fixtureRecorder->record('cache', $request, 0, serialize($result));

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getMulti(array $keys): array
    {
        $values = [];

        foreach ($keys as $key) {
            $values[$key] = $this->get($key);
        }

        return $values;
    }
}
