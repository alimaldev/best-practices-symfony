<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use GuzzleHttp\HandlerStack;
use Linio\Frontend\Communication\Bob4AliceClient as BaseBob4AliceClient;

class Bob4AliceClient extends BaseBob4AliceClient
{
    /**
     * @var WriteFixture
     */
    protected $fixtureRecorder;

    /**
     * {@inheritdoc}
     */
    public function __construct(string $baseUri, string $countryCode, WriteFixture $fixtureRecorder, array $config = [])
    {
        $this->fixtureRecorder = $fixtureRecorder;

        $config['handler'] = $config['handler'] ?? HandlerStack::create();
        $config['handler']->unshift($this->fixtureRecorder->getGuzzleMiddleWare());

        parent::__construct($baseUri, $countryCode, $config);
    }
}
