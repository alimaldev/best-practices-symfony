<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use GuzzleHttp\HandlerStack;
use Linio\Frontend\Search\Adapter\SearchClient as LinioSearchClient;

class SearchClient extends LinioSearchClient
{
    /**
     * @var WriteFixture
     */
    protected $fixtureRecorder;

    /**
     * SearchClient constructor.
     *
     * @param array $config
     * @param WriteFixture $fixtureRecorder
     */
    public function __construct(array $config, WriteFixture $fixtureRecorder)
    {
        $this->fixtureRecorder = $fixtureRecorder;

        $config['handler'] = $config['handler'] ?? HandlerStack::create();
        $config['handler']->unshift($this->fixtureRecorder->getGuzzleMiddleWare());

        parent::__construct($config);
    }
}
