<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use Psr\Http\Message\RequestInterface;

class HttpFixtureFormatter
{
    const DEFAULT_ACTION = 'default';

    /**
     * @param RequestInterface $request
     *
     * @return string
     */
    public function getAction(RequestInterface $request): string
    {
        $action = $this->getActionFromPath($request->getUri()->getPath());

        return !empty($action) ? $action : self::DEFAULT_ACTION;
    }

    /**
     * @param RequestInterface $request
     *
     * @return array
     */
    public function getRequestParameters(RequestInterface $request): array
    {
        $parameters = $this->getParametersFromQueryString($request->getUri()->getQuery());

        if (!empty((string) $request->getBody())) {
            $parameters['body'] = (string) $request->getBody();
        }

        return $parameters;
    }

    /**
     * @param string $query
     *
     * @return array
     */
    protected function getParametersFromQueryString(string $query): array
    {
        $parameters = [];

        parse_str($query, $parameters);

        return $parameters;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function getActionFromPath(string $path): string
    {
        $path = trim($path, '/');
        $path = str_replace('/', '_', $path);

        return $path;
    }
}
