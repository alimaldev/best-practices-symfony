<?php

declare(strict_types=1);

namespace Linio\Frontend\Recorder;

use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Promise\Promise;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

/**
 * Create file fixtures of third party API or cache calls.
 */
class WriteFixture
{
    /**
     * @var string
     */
    protected $fixtureStorePath;

    /**
     * @var string
     */
    protected $storeCode;

    /**
     * @var bool
     */
    protected $catchFixtures;

    /**
     * @var HttpFixtureFormatter
     */
    protected $formatter;

    public function __construct(
        string $fixtureStorePath,
        string $storeCode,
        bool $catchFixtures = false,
        HttpFixtureFormatter $formatter = null
    ) {
        if ($catchFixtures && !is_writable($fixtureStorePath)) {
            throw new RuntimeException(sprintf('Cannot write to %s.', $fixtureStorePath));
        }

        $this->fixtureStorePath = $fixtureStorePath;
        $this->storeCode = $storeCode;
        $this->catchFixtures = $catchFixtures;
        $this->formatter = $formatter;
    }

    /**
     * Returns Guzzle style middle ware that can be used to record Guzzle request & responses.
     *
     * @throws RuntimeException
     *
     * @return callable
     */
    public function getGuzzleMiddleWare(): callable
    {
        if (!$this->formatter instanceof HttpFixtureFormatter) {
            throw new RuntimeException('The HTTP Request formatter was not provided.');
        }

        // Yay!! JavaScript nesting hell
        $recordingHandler = function (callable $handler) {
            $responseWrapper = function (RequestInterface $request, array $options) use ($handler) {
                $recordSuccess = function (ResponseInterface $response) use ($request, $options) {
                    $this->record(
                        $this->formatter->getAction($request),
                        $this->formatter->getRequestParameters($request),
                        $response->getStatusCode(),
                        (string) $response->getBody()
                    );

                    return $response;
                };

                $recordFailure = function (BadResponseException $exception) use ($request, $options) {
                    $this->record(
                        $this->formatter->getAction($request),
                        $this->formatter->getRequestParameters($request),
                        $exception->getResponse()->getStatusCode(),
                        (string) $exception->getResponse()->getBody()
                    );

                    throw $exception;
                };

                /** @var Promise $promise */
                $promise = $handler($request, $options);

                return $promise->then($recordSuccess, $recordFailure);
            };

            return $responseWrapper;
        };

        return $recordingHandler;
    }

    /**
     * @param string $action
     * @param mixed $requestParameters
     * @param int $responseStatus
     * @param mixed $responsePayload
     */
    public function record(string $action, $requestParameters, int $responseStatus = 200, $responsePayload = '')
    {
        if (!$this->catchFixtures) {
            return;
        }

        $fixturePath = $this->generateFixtureFilePath($action, $requestParameters);

        $this->ensureIsDirectory(dirname($fixturePath));

        $fixtureData = $this->normalizeFixtureData($responseStatus, $requestParameters, $responsePayload);
        $this->writeFixtureFile($fixturePath, json_encode($fixtureData));
    }

    /**
     * Hashes the request parameters and returns a full path with filename to store the fixture data in.
     *
     * @param string $action
     * @param mixed $request
     *
     * @throws RuntimeException
     *
     * @return string
     */
    public function generateFixtureFilePath(string $action, $request): string
    {
        $basePath = sprintf(
            '%s%s%s%s%s', $this->fixtureStorePath, DIRECTORY_SEPARATOR, $this->storeCode, DIRECTORY_SEPARATOR, $action
        );

        if (!is_string($request)) {
            $request = json_encode($request);
        }

        $fixtureName = md5($request);

        return sprintf('%s%s%s.json', $basePath, DIRECTORY_SEPARATOR, $fixtureName);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function ensureIsDirectory(string $path)
    {
        if (!is_dir($path)) {
            $oldUMask = umask(0);
            mkdir($path, 0777, true);
            umask($oldUMask);
        }
    }

    /**
     * Normalizes the 3 elements into a structure that is deterministic and where the responsePayload is always
     * a string.
     *
     * @param int $status
     * @param mixed $request
     * @param mixed $response
     *
     * @return array
     */
    protected function normalizeFixtureData(int $status, $request, $response): array
    {
        $data = [
            'requestParameters' => $request,
            'responseStatus' => $status,
        ];

        if (!is_string($response)) {
            $response = json_encode($response);
        }

        $data['responsePayload'] = $response;

        return $data;
    }

    /**
     * @param string $file
     * @param string $data
     */
    protected function writeFixtureFile(string $file, string $data)
    {
        $data = rtrim($data) . "\n";
        file_put_contents($file, $data);
    }
}
