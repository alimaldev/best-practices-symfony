<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Linio\Frontend\Customer\Order\MakeDeliveryStatusFriendly;
use Linio\Frontend\Customer\Order\Package;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Extension;
use Twig_SimpleFunction;

class PackageDateExtension extends Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @var MakeDeliveryStatusFriendly
     */
    private $deliveryStatusFriendlyService;

    /**
     * @param MakeDeliveryStatusFriendly $deliveryStatusFriendly
     */
    public function setMakeDeliveryStatusFriendlyService(MakeDeliveryStatusFriendly $deliveryStatusFriendly)
    {
        $this->deliveryStatusFriendlyService = $deliveryStatusFriendly;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'date';
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new Twig_SimpleFunction('package_status', [$this, 'packageStatus']),
            new Twig_SimpleFunction('package_delivery_state', [$this, 'packageDeliveryState']),
        ];
    }

    /**
     * @param Package $package
     *
     * @return string
     */
    public function packageDeliveryState(Package $package)
    {
        switch ($package->getShipmentStatus()) {
            case 'early':
                return $this->translator->trans('order.delivery.early');

            case 'delayed':
                return $this->translator->trans('order.delivery.delayed');

            default:
                return $this->translator->trans('order.delivery.on_time');
        }
    }

    /**
     * @param Package $package
     *
     * @return string
     */
    public function packageStatus(Package $package): string
    {
        return $this->deliveryStatusFriendlyService->getPackageFriendlyStatus(
            $package
       );
    }
}
