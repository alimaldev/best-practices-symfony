<?php

namespace Linio\Frontend\Twig;

use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Marketing\AppIndexingAware;
use Twig_Extension;
use Twig_SimpleFunction;

class AppIndexingExtension extends Twig_Extension
{
    use AppIndexingAware;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('get_product_app_indexes', [$this, 'getProductLinks'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('get_category_app_indexes', [$this, 'getCategoryLinks', ['is_safe' => ['html']]]),
        ];
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    public function getProductLinks(Product $product)
    {
        $links = [];

        if ($product->getSku()) {
            $hrefs = $this->appIndexingService->getProductData($product->getSku());

            foreach ($hrefs as $href) {
                $links[] = sprintf('<link hreflang="%s" rel="alternate" href="%s"/>', $this->getHrefLang(), $href);
            }
        }

        return implode($links);
    }

    /**
     * @return string
     */
    protected function getHrefLang()
    {
        return str_replace('_', '-', $this->locale);
    }

    /**
     * @param Category $category
     *
     * @return array
     */
    public function getCategoryLinks(Category $category)
    {
        $links = [];

        if ($category->getSlug()) {
            $hrefs = $this->appIndexingService->getCategoryData($category->getSlug());

            foreach ($hrefs as $href) {
                $links[] = sprintf('<link hreflang="%s" rel="alternate" href="%s"/>', $this->getHrefLang(), $href);
            }
        }

        return implode($links);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_indexing_extension';
    }
}
