<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\DynamicFormBundle\Exception\NonExistentFormException;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Twig_Extension;
use Twig_SimpleFunction;

class PaymentMethodExtension extends Twig_Extension
{
    use DynamicFormAware;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('get_checkout_templates', [$this, 'getCheckoutTemplates']),
            new Twig_SimpleFunction('get_success_templates', [$this, 'getSuccessTemplates']),
            new Twig_SimpleFunction('get_error_templates', [$this, 'getErrorTemplates']),
            new Twig_SimpleFunction('get_payment_method_form', [$this, 'getPaymentMethodForm']),
        ];
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return array
     */
    public function getCheckoutTemplates(PaymentMethod $paymentMethod)
    {
        return [
            sprintf(':Checkout:payment/%s/checkout.html.twig', $paymentMethod->getName()),
            sprintf(':Checkout:payment/%s/checkout.html.twig', $this->getClassName($paymentMethod)),
            ':Checkout:payment/checkout.html.twig',
        ];
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return array
     */
    public function getSuccessTemplates(PaymentMethod $paymentMethod)
    {
        return [
            sprintf(':Checkout:payment/%s/success.html.twig', $paymentMethod->getName()),
            sprintf(':Checkout:payment/%s/success.html.twig', $this->getClassName($paymentMethod)),
            ':Checkout:payment/success.html.twig',
        ];
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return array
     */
    public function getErrorTemplates(PaymentMethod $paymentMethod)
    {
        return [
            sprintf(':Checkout:payment/%s/error.html.twig', $paymentMethod->getName()),
            sprintf(':Checkout:payment/%s/error.html.twig', $this->getClassName($paymentMethod)),
            ':Checkout:payment/error.html.twig',
        ];
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return \Symfony\Component\Form\FormView|null
     */
    public function getPaymentMethodForm(PaymentMethod $paymentMethod, $data = [], $options = ['csrf_protection' => false])
    {
        try {
            return $this->getDynamicFormFactory()->createForm($paymentMethod->getName(), $data, $options)->createView();
        } catch (NonExistentFormException $exception) {
            return;
        }
    }

    protected function getClassName($object)
    {
        return substr(strrchr(get_class($object), '\\'), 1);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'payment_method';
    }
}
