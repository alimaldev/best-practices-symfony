<?php

namespace Linio\Frontend\Twig;

use Symfony\Component\Form\FormView;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

class FormExtension extends Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('required_fields', [$this, 'getRequiredFields']),
            new Twig_SimpleFilter('optional_fields', [$this, 'getOptionalFields']),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('form_field', null, ['node_class' => 'Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode', 'is_safe' => ['html']]),
        ];
    }

    public function getRequiredFields(FormView $form)
    {
        $requiredFields = [];

        foreach ($form as $field) {
            if ($field->vars['required']) {
                $requiredFields[] = $field;
            }
        }

        return $requiredFields;
    }

    public function getOptionalFields(FormView $form)
    {
        $optionalFields = [];

        foreach ($form as $field) {
            if (!$field->vars['required']) {
                $optionalFields[] = $field;
            }
        }

        return $optionalFields;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'form_helper';
    }
}
