<?php

namespace Linio\Frontend\Twig;

use Linio\Controller\RouterAware;
use Linio\Frontend\Communication\Country\CountryAware;
use Linio\Frontend\Seo\SeoAware;
use RuntimeException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig_Extension;
use Twig_SimpleFunction;

class SeoExtension extends Twig_Extension
{
    use SeoAware;
    use CountryAware;
    use RouterAware;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @return Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('seo_title', [$this, 'seoTitle']),
            new Twig_SimpleFunction('seo_description', [$this, 'seoDescription']),
            new Twig_SimpleFunction('seo_robots', [$this, 'seoRobots']),
            new Twig_SimpleFunction('seo_text', [$this, 'seoText']),
            new Twig_SimpleFunction('text', [$this, 'text']),
            new Twig_SimpleFunction('seo_keywords', [$this, 'seoKeywords']),
            new Twig_SimpleFunction('seo_country', [$this, 'country']),
            new Twig_SimpleFunction('canonical_url', [$this, 'getCanonicalUrl']),
            new Twig_SimpleFunction('previous_catalog_page_url', [$this, 'getPreviousCatalogPageUrl']),
            new Twig_SimpleFunction('next_catalog_page_url', [$this, 'getNextCatalogPageUrl']),
        ];
    }

    /**
     * @return string
     */
    public function seoTitle()
    {
        return $this->seoService->getSeoEntity()->getTitle();
    }

    /**
     * @return string
     */
    public function seoDescription()
    {
        return $this->seoService->getSeoEntity()->getDescription();
    }

    /**
     * Rules:.
     *
     * 1. Everything defaults to INDEX,FOLLOW
     * 2. Campaign routes are not indexed (ALICE-50)
     * 3. Routes with query strings are not indexed (ALICE-35, ALICE-36, ALICE-50)
     * 4. Brand routes with multiple brands are not indexed (ALICE-50)
     *
     * @return string
     */
    public function seoRobots()
    {
        $metaValue = 'INDEX,FOLLOW';

        try {
            $routeInfo = $this->router->match($this->router->getContext()->getPathInfo());

            $query = $this->requestStack->getCurrentRequest()->query;

            $shouldAddPrefix = false;
            $shouldAddPrefix = $shouldAddPrefix || (isset($routeInfo['_route']) && $routeInfo['_route'] === 'frontend.catalog.index.brand' && substr_count('--', $routeInfo['brandSlug']) > 0);
            $shouldAddPrefix = $shouldAddPrefix || $query->count() > 0;

            $indexPrefix = $shouldAddPrefix ? 'NO' : '';
        } catch (RuntimeException $e) {
            $indexPrefix = '';
        }

        return $indexPrefix . $metaValue;
    }

    /**
     * @return string
     */
    public function seoText()
    {
        return $this->seoService->getSeoEntity()->getSeoText();
    }

    /**
     * @return string
     */
    public function text()
    {
        return $this->seoService->getSeoEntity()->getText();
    }

    /**
     * @return string
     */
    public function seoKeywords()
    {
        return $this->seoService->getSeoEntity()->getKeywords();
    }

    /**
     * @return string
     */
    public function country()
    {
        return strtoupper($this->getCountryCode());
    }

    /**
     * @return string|null
     */
    public function getCanonicalUrl()
    {
        $request = $this->requestStack->getCurrentRequest();
        $params = $request->query->all();

        if (isset($params['page']) && $params['page'] > 1) {
            return;
        }

        $uri = $this->requestStack->getCurrentRequest()->getPathInfo();

        return sprintf('%s://%s%s',
            $this->router->getContext()->getScheme(),
            $this->router->getContext()->getHost(),
            $uri);
    }

    /**
     * @return string
     */
    public function getPreviousCatalogPageUrl()
    {
        $request = $this->requestStack->getCurrentRequest();

        $page = $request->query->get('page', 1);

        if ($page == 1) {
            return;
        }

        $params = array_merge($request->attributes->get('_route_params'), $request->query->all());
        $params['page'] = $page - 1;

        if ($params['page'] == 1) {
            unset($params['page']);
        }

        return $this->generateUrl($request->attributes->get('_route'), $params, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param int $pageCount
     *
     * @return string
     */
    public function getNextCatalogPageUrl($pageCount)
    {
        $request = $this->requestStack->getCurrentRequest();

        $page = $request->query->get('page', 1);

        if ($page == $pageCount) {
            return;
        }

        $params = array_merge($request->attributes->get('_route_params'), $request->query->all());
        $params['page'] = $page + 1;

        return $this->generateUrl($request->attributes->get('_route'), $params, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName()
    {
        return 'seo_extension';
    }
}
