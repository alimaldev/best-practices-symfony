<?php

namespace Linio\Frontend\Twig;

use DateTime;
use ReflectionClass;
use ReflectionException;
use stdClass;
use Twig_Extension;
use Twig_SimpleFilter;

class DatePropertySortExtension extends Twig_Extension
{
    const SORT_DESC = 'descending';
    const SORT_ASC = 'ascending';

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('date_property_sort', [$this, 'sortByDateProperty']),
        ];
    }

    /**
     * @param array $objects
     * @param string $propertyName
     * @param string $direction
     *
     * @return array
     */
    public function sortByDateProperty(
        array $objects,
        $propertyName,
        $direction = self::SORT_DESC
    ) {
        $descSort = function ($a, $b) use ($propertyName) {
            return $this->getDate($propertyName, $a) <= $this->getDate($propertyName, $b);
        };

        $ascSort = function ($a, $b) use ($propertyName) {
            return $this->getDate($propertyName, $a) > $this->getDate($propertyName, $b);
        };

        if ($direction === self::SORT_DESC) {
            $sortFunc = $descSort;
        } else {
            $sortFunc = $ascSort;
        }

        usort($objects, $sortFunc);

        return $objects;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'property_sort';
    }

    /**
     * @param string $propertyName
     * @param stdClass $object
     *
     * @return DateTime
     */
    protected function getDate($propertyName, $object)
    {
        $reflect = new ReflectionClass($object);

        try {
            $property = $reflect->getProperty($propertyName);

            $isProperty = $property->isPublic();
        } catch (ReflectionException $e) {
            $isProperty = false;
        }

        try {
            $methodName = 'get' . ucfirst($propertyName);

            $method = $reflect->getMethod($methodName);

            $isMethod = !$isProperty && $method->isPublic();
        } catch (ReflectionException $e) {
            $methodName = null;

            $isMethod = false;
        }

        if ($isProperty) {
            return $object->$propertyName;
        }

        if ($isMethod) {
            return call_user_func([$object, $methodName]);
        }

        return new DateTime();
    }
}
