<?php

namespace Linio\Frontend\Twig;

use Linio\Component\Cache\CacheAware;
use Twig_Extension;
use Twig_SimpleFunction;

class AssetsExtension extends Twig_Extension
{
    use CacheAware;

    /**
     * @var string
     */
    protected $cssRevisionFile;

    /**
     * @var string
     */
    protected $cssPath;

    /**
     * @var string
     */
    protected $jsRevisionFile;

    /**
     * @var string
     */
    protected $jsPath;

    /**
     * @return Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('getCssAssets', [$this, 'getCssAssets']),
            new Twig_SimpleFunction('getJsAssets', [$this, 'getJsAssets']),
            new Twig_SimpleFunction('getSpecificCss', [$this, 'getSpecificCss']),
            new Twig_SimpleFunction('getSpecificJs', [$this, 'getSpecificJs']),
        ];
    }

    /**
     * @return array
     */
    public function getCssAssets()
    {
        return $this->getAssets($this->getCssRevisionFile(), $this->getCssPath());
    }

    /**
     * @return array
     */
    public function getJsAssets()
    {
        return $this->getAssets($this->getJsRevisionFile(), $this->getJsPath());
    }

    /**
     * @param string $revisionPath
     * @param string $path
     *
     * @return array
     */
    protected function getAssets($revisionPath, $path)
    {
        $assets = [];
        $revisionFile = $this->getRevisionFile($revisionPath);

        if ($revisionFile) {
            foreach ($revisionFile as $asset) {
                $assets[] = $path . $asset;
            }
        }

        return $assets;
    }

    /**
     * @param string $fileName
     *
     * @return null|string
     */
    public function getSpecificCss($fileName)
    {
        $revisionFile = $this->getRevisionFile($this->getCssRevisionFile());

        if ($revisionFile) {
            return $this->getCssPath() . $revisionFile[$fileName];
        }

        return;
    }

    /**
     * @param string $fileName
     *
     * @return null|string
     */
    public function getSpecificJs($fileName)
    {
        $revisionFile = $this->getRevisionFile($this->getJsRevisionFile());

        if ($revisionFile) {
            return $this->getJsPath() . $revisionFile[$fileName];
        }

        return;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'assets';
    }

    /**
     * @return string
     */
    public function getCssPath()
    {
        return $this->cssPath;
    }

    /**
     * @param string $cssPath
     *
     * @return $this
     */
    public function setCssPath($cssPath)
    {
        $this->cssPath = $cssPath;
    }

    /**
     * @return string
     */
    public function getCssRevisionFile()
    {
        return $this->cssRevisionFile;
    }

    /**
     * @param string $cssRevisionFile
     */
    public function setCssRevisionFile($cssRevisionFile)
    {
        $this->cssRevisionFile = $cssRevisionFile;
    }

    /**
     * @return string
     */
    public function getJsPath()
    {
        return $this->jsPath;
    }

    /**
     * @param string $jsPath
     */
    public function setJsPath($jsPath)
    {
        $this->jsPath = $jsPath;
    }

    /**
     * @return string
     */
    public function getJsRevisionFile()
    {
        return $this->jsRevisionFile;
    }

    /**
     * @param string $jsRevisionFile
     */
    public function setJsRevisionFile($jsRevisionFile)
    {
        $this->jsRevisionFile = $jsRevisionFile;
    }

    /**
     * @param string $filePath
     *
     * @return array|null
     */
    protected function getRevisionFile($filePath)
    {
        if ($filePath && $this->cacheService->contains($filePath)) {
            return $this->cacheService->get($filePath);
        }

        $data = null;

        if (file_exists($filePath)) {
            $data = json_decode(file_get_contents($filePath), true);
            $this->cacheService->set($filePath, $data);
        }

        return $data;
    }
}
