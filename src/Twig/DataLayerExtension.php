<?php

namespace Linio\Frontend\Twig;

use Linio\Frontend\Marketing\DataLayerAware;
use Linio\Frontend\Serializer\SerializerAware;
use Twig_Extension;
use Twig_SimpleFunction;

class DataLayerExtension extends Twig_Extension
{
    use DataLayerAware;
    use SerializerAware;

    /**
     * @return Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('get_data_layer', [$this->dataLayerService, 'getData']),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'data_layer_extension';
    }
}
