<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Linio\Component\Cache\CacheService;
use Linio\Controller\RouterAware;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Entity\Product;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

class SizeGuideLinkExtension extends Twig_Extension
{
    use RouterAware;

    const SIZE_GUIDE_SLUG_PREFIX = 'guia-de-tallas';

    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @var CacheService
     */
    protected $configurationCacheService;

    /**
     * @param CacheService $configurationCacheService
     */
    public function setConfigurationCacheService(CacheService $configurationCacheService)
    {
        $this->configurationCacheService = $configurationCacheService;
    }

    /**
     * @param CmsService $cmsService
     */
    public function __construct(CmsService $cmsService)
    {
        $this->cmsService = $cmsService;
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new Twig_SimpleFunction('size_guide_link', [$this, 'getSizeGuideLink'], ['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    public function getSizeGuideLink(Twig_Environment $twig, Product $product): string
    {
        if (!$this->isValidCategory($product)) {
            return '';
        }

        $slug = sprintf('%s-%s', self::SIZE_GUIDE_SLUG_PREFIX, $product->getBrand()->getSlug());
        $brandStaticPage = $this->cmsService->staticPage($slug);

        if (empty($brandStaticPage)) {
            $link = $this->generateUrl('frontend.default.static_page', ['slug' => self::SIZE_GUIDE_SLUG_PREFIX]);
        } else {
            $link = $this->generateUrl('frontend.default.static_page', ['slug' => $slug]);
        }

        return $twig->render(
            '::partial/components/catalog/size_guide.html.twig', [
            'link' => $link,
        ]);
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    protected function isValidCategory(Product $product): bool
    {
        $sizeGuideCategories = $this->configurationCacheService->get('size_guide_categories');

        return in_array($product->getCategory()->getId(), $sizeGuideCategories ?? []);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'size_guide_link_extension';
    }
}
