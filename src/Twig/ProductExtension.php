<?php

declare(strict_types=1);

namespace Linio\Frontend\Twig;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductService;
use Twig_Extension;
use Twig_SimpleFunction;

class ProductExtension extends Twig_Extension
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @param ProductService $productService
     */
    public function setProductService(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('product_image', [$this, 'getMainProductImage']),
            new Twig_SimpleFunction('product_available', [$this, 'isProductAvailable']),
        ];
    }

    /**
     * Returns the url for the given sku or Image.
     *
     * If the first argument is a sku (string), the product is fetched from cache.
     *
     * @param string|Image $item
     * @param string $size
     *
     * @return string|null
     */
    public function getMainProductImage($item, string $size)
    {
        if (!$item) {
            return;
        }

        if ($item instanceof Image) {
            return $this->getUrlFromImage($item, $size);
        }

        try {
            $product = $this->productService->getBySku($item);
        } catch (ProductNotFoundException $exception) {
            return;
        }

        $images = $product->getImages();

        foreach ($images as $image) {
            if ($image->isMain()) {
                return $this->getUrlFromImage($image, $size);
            }
        }

        // If no image is marked as main, use the first image
        if (!empty($images)) {
            return $this->getUrlFromImage(reset($images), $size);
        }
    }

    /**
     * @param Image $image
     * @param string $size
     *
     * @return string
     */
    protected function getUrlFromImage(Image $image, $size)
    {
        return sprintf('%s-%s.jpg', $image->getSlug(), $size);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_image_extension';
    }

    /**
     * @param string $sku
     *
     * @return bool
     */
    public function isProductAvailable(string $sku): bool
    {
        try {
            return $this->productService->getBySku($sku)->getStock() > 0;
        } catch (ProductNotFoundException $exception) {
            return false;
        }
    }
}
