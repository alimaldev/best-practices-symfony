<?php

namespace Linio\Frontend\Twig;

use Linio\Controller\RouterAware;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Feed\FeedAware;
use Linio\Frontend\Feed\ResultSetMapper\RecommendationMapper;
use Linio\Frontend\Product\ProductAware;
use Linio\Frontend\Serializer\SerializerAware;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

class ContentTypeExtension extends Twig_Extension
{
    use CategoryAware;
    use RouterAware;
    use ProductAware;
    use SerializerAware;
    use FeedAware;

    const CATEGORY_BANNER = 'category';
    const CONTENT_TYPE_SIMPLE_IMAGE = 'simple_image';
    const CONTENT_TYPE_SIMPLE_TEXT = 'simple_text';
    const IMAGE = 'simple_image';
    const CAROUSEL = 'carousel';
    const GRID = 'grid';
    const DFA = 'dfa';
    const MAIN_MENU_GRID = 'main_menu_grid';
    const MARKDOWN = 'markdown_text';
    const MENU = 'menu';
    const MULTILINE = 'multiline_text';
    const RECOMMENDATIONS = 'recommendations';
    const BANNER = 'banner';
    const BANNER_MENU = 'banner_menu';
    const CATEGORY_MENU = 'category';
    const CUSTOMIZED_MENU = 'customized';
    const TEXT = 'simple_text';
    const RECOMMENDATION_MANUAL = 'manual';
    const RECOMMENDATION_AUTOMATIC = 'automatic';
    const RECOMMENDATION_SKU = 'sku';
    const RECOMMENDATION_CATEGORY = 'category';
    const PLACEMENT = 'placement';

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @var RecommendationMapper
     */
    protected $recommendationMapper;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var bool
     */
    protected $cdnEnabled;

    /**
     * @var string
     */
    protected $cdnUrl;

    /**
     * @var string
     */
    protected $cmsDefaultUrl;

    /**
     * ContentTypeExtension constructor.
     *
     * @param CmsService $cmsService
     */
    public function __construct(CmsService $cmsService, LoggerInterface $logger)
    {
        $this->cmsService = $cmsService;
        $this->logger = $logger;
    }

    /**
     * @param RecommendationMapper $recommendationMapper
     */
    public function setRecommendationMapper($recommendationMapper)
    {
        $this->recommendationMapper = $recommendationMapper;
    }

    /**
     * @param bool $cdnEnabled
     */
    public function setCdnEnabled(bool $cdnEnabled)
    {
        $this->cdnEnabled = $cdnEnabled;
    }

    /**
     * @param string $cdnUrl
     */
    public function setCdnUrl(string $cdnUrl)
    {
        $this->cdnUrl = $cdnUrl;
    }

    /**
     * @param string $cmsDefaultUrl
     */
    public function setCmsDefaultUrl(string $cmsDefaultUrl)
    {
        $this->cmsDefaultUrl = $cmsDefaultUrl;
    }

    /**
     * @return Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('cms_simple_image', [$this, 'cmsSimpleImage'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_text', [$this, 'cmsText'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_multiline_text', [$this, 'cmsMultilineText'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_banner', [$this, 'cmsBanner'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_dfa', [$this, 'cmsDfa'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_markdown_text', [$this, 'cmsMarkdownText'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_menu', [$this, 'cmsMenu'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_grid', [$this, 'cmsGrid'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_carousel', [$this, 'cmsCarousel'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_section_grid', [$this, 'cmsSectionGrid'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_global', [$this, 'cmsGlobal'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_header_menu', [$this, 'cmsHeaderMenu'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_footer_menu', [$this, 'cmsFooterMenu'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('category_url', [$this, 'generateUrlFromCategoryId'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_recommendations', [$this, 'cmsRecommendations'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('render_recommendations', [$this, 'renderRecommendations'], ['is_safe' => ['html']]),
            new Twig_SimpleFunction('cms_main_menu', [$this, 'cmsMainMenu'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_main_menu_content', [$this, 'cmsMainMenuContent'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_render_dfa', [$this, 'renderDfa'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('cms_usp_menu', [$this, 'cmsUspMenu'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new Twig_SimpleFunction('update_cdn_url', [$this, 'updateCdnUrl']),
            new Twig_SimpleFunction('cms_placement', [$this, 'cmsPlacement'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'content_type_extension';
    }

    public function isSafe()
    {
        return true;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param Twig_Environment $twig
     * @param string $namespace
     * @param int $id
     *
     * @return string
     */
    public function cmsSimpleImage(Twig_Environment $twig, $namespace, $id)
    {
        $image = $this->cmsService->simpleImage($namespace, $id, $context = 'desktop');

        if (!$image) {
            return '';
        }

        return $this->renderSimpleImage($twig, $image);
    }

    /**
     * @param Twig_Environment $twig
     * @param array $image
     *
     * @return string
     */
    protected function renderSimpleImage(Twig_Environment $twig, array $image)
    {
        return $twig->render(
            ':Cms/content_type:simple_image.html.twig', [
            'image' => $image,
        ]);
    }

    /**
     * @param string $namespace
     * @param string $slug
     *
     * @return string
     */
    public function cmsText($namespace, $slug)
    {
        $text = $this->cmsService->simpleText($namespace, $slug);

        return $this->renderText($text);
    }

    /**
     * @param array $text
     *
     * @return string
     */
    protected function renderText(array $text)
    {
        return $text['text'] ?? '';
    }

    /**
     * @param string $namespace
     * @param int $id
     *
     * @return string
     */
    public function cmsMultilineText($namespace, $id)
    {
        $multilineText = $this->cmsService->multilineText($namespace, $id);

        return $this->renderMultilineText($multilineText);
    }

    /**
     * @param array $multilineText
     *
     * @return string
     */
    protected function renderMultilineText(array $multilineText)
    {
        return $multilineText['text'] ?? '';
    }

    /**
     * @param Twig_Environment $twig
     * @param string $namespace
     * @param int $id
     *
     * @return string
     */
    public function cmsBanner(Twig_Environment $twig, $namespace, $id)
    {
        $banner = $this->cmsService->banner($namespace, $id, $context = 'desktop');

        $this->renderBanner($twig, $banner);
    }

    /**
     * @param Twig_Environment $twig
     * @param array $banner
     *
     * @return string
     */
    protected function renderBanner(Twig_Environment $twig, array $banner)
    {
        if (!isset($banner['type'])) {
            return '';
        }

        if ($banner['type'] == self::CATEGORY_BANNER) {
            $banner['href'] = $this->generateUrlFromCategoryId($banner['href']);
        }

        return $twig->render(
            ':Cms/content_type:banner.html.twig', [
                'banner' => $banner,
        ]);
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public function cmsPlacement(string $key)
    {
        return $this->cmsService->placement($key);
    }

    /**
     * @param Twig_Environment $twig
     * @param string $namespace
     * @param int $id
     *
     * @return string
     */
    public function cmsDfa(Twig_Environment $twig, $namespace, $id)
    {
        $dfa = $this->cmsService->dfa($namespace, $id, 'desktop');

        return $this->renderDfa($twig, $dfa);
    }

    /**
     * @param Twig_Environment $twig
     * @param array $dfa
     *
     * @return string
     */
    public function renderDfa(Twig_Environment $twig, array $dfa, $ignoreAload = false)
    {
        //TODO: Dimensions should come from frontend manager and stored in the cache
        $imgDimensions = '';

        if (!isset($dfa['image_url'])) {
            $dfa['image_url'] = $dfa['data']['image_url'];
        }

        if (!isset($dfa['target_url'])) {
            $dfa['target_url'] = $dfa['data']['target_url'];
        }

        $dfa['ignore_aload'] = $ignoreAload;

        if (preg_match('/;sz=(?<width>\d+)x(?<height>\d+)/', $dfa['image_url'], $match)) {
            $imgDimensions = sprintf('width="%s" height="%s"', $match['width'], $match['height']);
        }

        $dfa['dimensions'] = $imgDimensions;

        return $twig->render(
            ':Cms:/content_type/dfa.html.twig', [
                'dfa' => $dfa,
            ]
        );
    }

    /**
     * @param string $namespace
     * @param string $id
     *
     * @return string
     */
    public function cmsMarkdownText($namespace, $id)
    {
        $markdownText = $this->cmsService->markdownText($namespace, $id, $context = 'desktop');

        return $this->renderMarkdownText($markdownText);
    }

    /**
     * @param array $markdownText
     *
     * @return string
     */
    protected function renderMarkdownText(array $markdownText)
    {
        return $markdownText['text'] ?? '';
    }

    /**
     * @param string $namespace
     * @param string $row
     *
     * @return string
     */
    public function cmsMenu($namespace, $row)
    {
        $menu = $this->cmsService->menu($namespace, $row, $context = 'desktop');

        if (!$menu) {
            return '';
        }

        return $this->renderMenu($menu);
    }

    /**
     * @param array $menu
     *
     * @return string
     */
    protected function renderMenu(array &$menu)
    {
        foreach ($menu as &$subMenu) {
            $subMenu = $this->setMenuUrlLink($subMenu);

            foreach ($subMenu['children'] as &$childSubMenu) {
                $childSubMenu = $this->setMenuUrlLink($childSubMenu);
            }
        }

        $outputMenu = '';

        foreach ($menu as $item) {
            $outputMenu .= sprintf('<a class="fly-menu-devision-sub-title" href="%s">%s</a>', $item['attributes']['href'], $item['attributes']['label']);
            foreach ($item['children'] as $subMenu) {
                $outputMenu .= sprintf('<a class="fly-menu-devision-sub-link" title="" href="%s">%s</a>', $subMenu['attributes']['href'], $subMenu['attributes']['label']);
            }
        }

        return $outputMenu;
    }

    /**
     * @param array $menu
     *
     * @return array
     */
    protected function setMenuUrlLink(array $menu)
    {
        if ($menu['type'] === self::CATEGORY_MENU) {
            $category = $this->categoryService->getCategory($menu['attributes']['category_id']);

            $route = $this->generateUrl('frontend.catalog.index.category', ['category' => $category->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL);
            $menu['attributes']['href'] = $route;
        }

        return $menu;
    }

    /**
     * @param Twig_Environment $twig
     * @param string $namespace
     * @param int $row
     *
     * @return string
     */
    public function cmsGrid(Twig_Environment $twig, $namespace, $row)
    {
        $grid = $this->cmsService->grid($namespace, $row);

        if (empty($grid)) {
            return '';
        }

        return $this->renderGrid($twig, $grid);
    }

    /**
     * @param Twig_Environment $twig
     * @param array $grid
     *
     * @return string
     */
    protected function renderGrid(Twig_Environment $twig, array $grid)
    {
        if (!isset($grid['cells'])) {
            return '';
        }

        $outputGrid = '';

        foreach ($grid['cells'] as $cell) {
            $outputGrid .= sprintf('<div class="col-sm-%s">%s</div>', $cell['cols'], $this->renderContentType($twig, $cell));
        }

        return $outputGrid;
    }

    /**
     * @param Twig_Environment $twig
     * @param array $contentType
     *
     * @return string
     */
    protected function renderContentType(Twig_Environment $twig, array $contentType)
    {
        switch ($contentType['content_type']) {
            case self::IMAGE:
                return $this->renderSimpleImage($twig, $contentType['data']);
                break;
            case self::DFA:
                return $this->renderDfa($twig, $contentType['data']);
                break;
            case self::MENU:
                return $this->renderMenu($contentType['data']);
                break;
            case self::MARKDOWN:
                return $this->renderMarkdownText($contentType['data']);
                break;
            case self::BANNER:
                return $this->renderBanner($twig, $contentType['data']);
                break;
            case self::TEXT:
                return $this->renderText($contentType['data']);
                break;
        }

        return '';
    }

    /**
     * @param Twig_Environment $twig
     * @param string $namespace
     * @param string $row
     * @param string $id
     *
     * @return string
     */
    public function cmsCarousel(Twig_Environment $twig, $namespace, $row, $id)
    {
        $carousel = $this->cmsService->carousel($namespace, $row, $id);

        if (empty($carousel)) {
            return '';
        }

        return $this->renderCarousel($twig, $carousel);
    }

    /**
     * @param Twig_Environment $twig
     * @param array $carousel
     *
     * @return string
     */
    protected function renderCarousel(Twig_Environment $twig, array $carousel)
    {
        if (!isset($grid['elements'])) {
            return '';
        }

        $carouselContent = '';

        foreach ($carousel['elements'] as $carouselItem) {
            $carouselContent .= $this->renderContentType($twig, $carouselItem);
        }

        return $carouselContent;
    }

    /**
     * @param Twig_Environment $twig
     * @param int $row
     * @param string $namespace
     *
     * @return string
     */
    public function cmsSectionGrid(Twig_Environment $twig, $row, $namespace = 'home')
    {
        $sectionGrid = $this->cmsService->grid($namespace, $row);

        if (empty($sectionGrid)) {
            return '';
        }

        $cells = $this->renderGrid($twig, $sectionGrid);

        $sectionGrid = <<<EOD
<div class="cms-block">
    <h3 class="cms-block-title">{$sectionGrid['title']}</h3>
    <div class="cms-block-content clearfix">
    {$cells}
    </div>
</div>
EOD;

        return $sectionGrid;
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function cmsGlobal($slug)
    {
        $cmsGlobal = $this->cmsService->getGlobal($slug);

        if (empty($cmsGlobal)) {
            return '';
        }

        return $this->renderMarkdownText($cmsGlobal);
    }

    /**
     * @param Twig_Environment $twig
     * @param string $slug
     * @param bool $listView
     *
     * @return string
     */
    public function cmsUspMenu(Twig_Environment $twig, $slug = 'home', $listView = false)
    {
        $menuCollection = $this->cmsService->getUspMenuFromCache($slug);

        if (empty($menuCollection)) {
            return '';
        }

        if (isset($menuCollection['revision_number'])) {
            unset($menuCollection['revision_number']);
        }

        foreach ($menuCollection as &$menu) {
            if ($menu['type'] === self::CATEGORY_MENU) {
                $menu['attributes']['href'] = $this->generateUrlFromCategoryId($menu['attributes']['category_id']);
            }
        }

        if ($listView) {
            return $twig->render(
                ':Cms/content_type:usp_menu_list.html.twig', [
                'menu' => $menuCollection,
            ]);
        }

        return $twig->render(
            ':Cms/content_type:usp_menu.html.twig', [
            'menu' => $menuCollection,
        ]);
    }

    /**
     * @param string $namespace
     * @param string $row
     *
     * @return array
     */
    public function cmsRecommendations($namespace, $row)
    {
        $recommendations = $this->cmsService->getRecommendationsFromCache($namespace, $row);

        $recommendations = $recommendations ? $recommendations : [];

        return $this->renderRecommendations($recommendations);
    }

    /**
     * @param array $recommendations
     *
     * @return array
     */
    public function renderRecommendations(array $recommendations)
    {
        $result = [
            'title' => isset($recommendations['title']) ? $recommendations['title'] : '',
            'products' => [],
        ];

        if (!isset($recommendations['type'])) {
            return $result;
        }

        if ($recommendations['type'] == self::RECOMMENDATION_AUTOMATIC) {
            $result['products'] = $this->feedService
                ->findOverallBestSellers()
                ->getProducts();
        }

        if ($recommendations['type'] == self::RECOMMENDATION_MANUAL) {
            if (!isset($recommendations['rules'][0]['type'])) {
                return $result['products'] = [];
            }

            $manualRecommendationType = $recommendations['rules'][0]['type'];

            if ($manualRecommendationType == self::RECOMMENDATION_SKU) {
                $result['products'] = $this->buildProductRecommendations(
                    $recommendations['rules'][0]['criteria']
                );
            }

            if ($manualRecommendationType == self::RECOMMENDATION_CATEGORY) {
                $categoryId = $recommendations['rules'][0]['criteria'][0];

                try {
                    $category = $this->categoryService->getCategory($categoryId);
                } catch (CategoryNotFoundException $exception) {
                    return $result;
                }

                $result['products'] = $this->feedService
                    ->findBestSellers($category)
                    ->getProducts();
            }
        }

        return $result;
    }

    /**
     * @param array $sku
     *
     * @return array
     */
    public function buildProductRecommendations(array $sku)
    {
        $products = $this->productService->getBySkus($sku);

        $productRecommendations = [];

        foreach ($products as $product) {
            $productRecommendations[] = $this->mapProductRecommendation($product);
        }

        return $productRecommendations;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function mapProductRecommendation(Product $product)
    {
        $simpleData = $product->getFirstSimple();

        $productRecommendation = [];
        $productRecommendation['title'] = $product->getName();
        $productRecommendation['brand'] = $product->getBrand()->getName();
        $productRecommendation['originalPrice'] = $simpleData->getOriginalPrice()->getMoneyAmount();
        $productRecommendation['percentageOff'] = intval($simpleData->getPercentageOff());
        $productRecommendation['price'] = $simpleData->getPrice()->getMoneyAmount();
        $productRecommendation['image'] = $product->getImages()[1]->getSlug() . '-product.jpg';
        $productRecommendation['color'] = null;
        $productRecommendation['sku'] = $product->getSku();

        $productRecommendation['url'] = $this->generateUrl(
            'frontend.catalog.detail',
            ['productSlug' => $product->getSlug()],
            true
        );

        return $productRecommendation;
    }

    /**
     * @return string
     */
    public function cmsHeaderMenu()
    {
        $menu = $this->cmsService->menu('global', 'header-menu');

        if (!$menu) {
            return '';
        }

        return $this->renderHeaderMenu($menu);
    }

    /**
     * @param array $menu
     *
     * @return string
     */
    public function renderHeaderMenu(array $menu)
    {
        $footerMenu = '';

        foreach ($menu as $item) {
            $footerMenu .= sprintf('<a class="navbar-top-item" href="%s">%s</a>', $item['attributes']['href'], $item['attributes']['label']);
        }

        return $footerMenu;
    }

    /**
     * @param string $slug
     * @param string $target
     *
     * @return string
     */
    public function cmsFooterMenu($slug, $target = '')
    {
        $menu = $this->cmsService->menu('global', $slug);

        if (!$menu) {
            return '';
        }

        return $this->renderFooterMenu($menu, $target);
    }

    /**
     * @param array $menu
     * @param string $target
     *
     * @return string
     */
    public function renderFooterMenu(array $menu, $target)
    {
        $footerMenu = '<div class="footer-columns">';

        foreach ($menu as &$item) {
            if (isset($item['attributes']['category_id'])) {
                try {
                    $item = $this->setMenuUrlLink($item);
                } catch (CategoryNotFoundException $exception) {
                    continue;
                }
            }

            if (isset($item['attributes']['href']) && isset($item['attributes']['label'])) {
                $footerMenu .= sprintf(
                    '<a href="%s" target="%s">%s</a>',
                    $item['attributes']['href'],
                    $target,
                    $item['attributes']['label']
                );
            }
        }

        $footerMenu .= '</div>';

        return $footerMenu;
    }

    /**
     * @param Twig_Environment $twig
     *
     * @return string
     */
    public function cmsMainMenu(Twig_Environment $twig)
    {
        $menuCollection = $this->cmsService->getMainMenuFromCache();

        if (empty($menuCollection)) {
            return '';
        }

        foreach ($menuCollection as &$item) {
            if ($item['type'] == self::CATEGORY_MENU) {
                $item['attributes']['href'] = $this->generateUrlFromCategoryId($item['attributes']['category_id']);
            }
        }

        return $twig->render(
            ':Cms:/content_type/main_menu.html.twig', [
                'menu' => $menuCollection,
        ]);
    }

    /**
     * @param Twig_Environment $twig
     *
     * @return string
     */
    public function cmsMainMenuContent(Twig_Environment $twig)
    {
        $menuCollection = $this->cmsService->getMainMenuFromCache();

        if (empty($menuCollection)) {
            return '';
        }

        $mainMenuContent = '';

        foreach ($menuCollection as $position => $menu) {
            $mainMenuContent .= $this->buildMainMenuContent($twig, $menu, $position + 1);
        }

        return $mainMenuContent;
    }

    /**
     * @param Twig_Environment $twig
     * @param array $menu
     * @param int $position
     *
     * @return string
     */
    public function buildMainMenuContent(Twig_Environment $twig, array $menu, $position)
    {
        $grid = '';

        if ($menu['type'] === self::CATEGORY_MENU) {
            $grid = $this->cmsService->mainMenuCategoryGrid($menu['attributes']['category_id']);
        } elseif ($menu['type'] === self::CUSTOMIZED_MENU) {
            $grid = $this->cmsService->mainMenuCustomGrid($menu['attributes']['main_menu_grid_slug']);
        }

        if (empty($grid)) {
            return '';
        }

        if (!empty($grid['menu'])) {
            $grid['menu'] = $this->completeMenuData($grid['menu']);
        }

        $grid['href'] = $this->getUrlForMainMenu($menu);
        $grid['grid'] = $this->completeGridContent($twig, $grid['grid']);

        return $twig->render(
            ':Cms:/content_type/home/grid.html.twig', [
                'position' => $position,
                'grid' => $grid,
            ]
        );
    }

    /**
     * @param array $menu
     *
     * @return string
     */
    protected function getUrlForMainMenu(array $menu)
    {
        return $menu['type'] === self::CATEGORY_MENU ? $this->generateUrlFromCategoryId($menu['attributes']['category_id']) : $menu['attributes']['href'];
    }

    /**
     * @param array $menuCollection
     *
     * @return array
     */
    protected function completeMenuData(array $menuCollection)
    {
        foreach ($menuCollection as &$menu) {
            foreach ($menu as &$item) {
                if ($item['type'] === self::CATEGORY_MENU) {
                    $item['attributes']['href'] = $this->generateUrlFromCategoryId(
                        $item['attributes']['category_id']
                    );
                }

                foreach ($item['children'] as &$child) {
                    if ($child['type'] === self::CATEGORY_MENU) {
                        $child['attributes']['href'] = $this->generateUrlFromCategoryId(
                            $child['attributes']['category_id']
                        );
                    }
                }
            }
        }

        return $menuCollection;
    }

    /**
     * @param Twig_Environment $twig
     * @param array $grid
     *
     * @return array
     */
    protected function completeGridContent(Twig_Environment $twig, array $grid)
    {
        foreach ($grid as &$cell) {
            $cell = $this->renderContentType($twig, $cell);
        }

        return $grid;
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function generateUrlFromCategoryId($id)
    {
        try {
            $category = $this->categoryService->getCategory($id);
        } catch (CategoryNotFoundException $e) {
            $this->logger->critical('Menu category not found.', ['id' => $id]);

            return '';
        }

        $parameters = ['category' => $category->getSlug()];

        return $this->generateUrl('frontend.catalog.index.category', $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param string $imagePath
     *
     * @return string
     */
    public function updateCdnUrl(string $imagePath): string
    {
        if (!$this->cdnEnabled) {
            return $imagePath;
        }

        if (strpos($imagePath, '/') == 0) {
            return $this->cdnUrl . $imagePath;
        }

        return str_replace($this->cmsDefaultUrl, $this->cdnUrl, $imagePath);
    }
}
