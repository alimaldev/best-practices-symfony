<?php

namespace Linio\Frontend\Twig;

use Linio\Controller\RouterAware;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Twig_Extension;
use Twig_SimpleFunction;

class SlugExtension extends Twig_Extension
{
    use RouterAware;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack($requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('catalogPath', [$this, 'getCatalogPath']),
            new Twig_SimpleFunction('productPath', [$this, 'getProductPath']),
        ];
    }

    /**
     * @param Category $category
     *
     * @throws ResourceNotFoundException
     *
     * @return string
     */
    public function getCategoryPath(Category $category)
    {
        $request = $this->requestStack->getCurrentRequest();

        $this->completeQueryIfOnSellerPage($request);

        $parameters = array_merge(
            $request->query->all(),
            ['category' => $category->getSlug()]
        );

        unset($parameters['page']);

        return $this->generateUrl('frontend.catalog.index.category', $parameters);
    }

    /**
     * @param Campaign $campaign
     *
     * @return string
     */
    public function getCampaignPath(Campaign $campaign)
    {
        if ($campaign->getParent() === null) {
            $slug = $campaign->getSlug();
        } else {
            $slug = sprintf('%s/%s', $campaign->getParent()->getSlug(), $campaign->getSlug());
        }

        $parameters = array_merge(
            $this->requestStack->getCurrentRequest()->query->all(),
            ['campaign' => $slug]
        );

        unset($parameters['page']);

        return $this->generateUrl('frontend.catalog.index.campaign', $parameters);
    }

    /**
     * @param Category $category
     *
     * @return string
     */
    public function getCatalogPath(Category $category)
    {
        if ($category instanceof Campaign) {
            return $this->getCampaignPath($category);
        }

        $parameters = $this->requestStack->getCurrentRequest()->attributes->all();

        if (isset($parameters['brand']) || isset($parameters['brandSlug'])) {
            return $this->getCategoryBrandPath($category, $this->getBrandFromParameters($parameters));
        }

        return $this->getCategoryPath($category);
    }

    /**
     * @param Category $category
     * @param Brand $brand
     *
     * @return string
     */
    public function getCategoryBrandPath(Category $category, Brand $brand)
    {
        $parameters = array_merge(
            $this->requestStack->getCurrentRequest()->query->all(),
            ['category' => $category->getSlug()],
            ['brand' => $brand->getSlug()]
        );

        unset($parameters['page']);

        return $this->generateUrl('frontend.catalog.index.category_brand', $parameters);
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    public function getProductPath(Product $product)
    {
        $parameters = ['productSlug' => $product->getSlug()];

        if ($product->getCampaignSlug()) {
            $parameters['campaign'] = $product->getCampaignSlug();
        }

        return $this->generateUrl('frontend.catalog.detail', $parameters);
    }

    /**
     * @param Brand $brand
     *
     * @return string
     */
    public function getBrandPath(Brand $brand)
    {
        return $this->generateUrl('frontend.catalog.index.brand', ['brand' => $brand->getSlug()]);
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName()
    {
        return 'slug_extension';
    }

    /**
     * @param array $parameters
     *
     * @return Brand
     */
    protected function getBrandFromParameters(array $parameters)
    {
        return isset($parameters['brand']) ? $parameters['brand'] : $parameters['brandSlug'];
    }

    /**
     * @param Request $request
     *
     * @throws ResourceNotFoundException
     */
    protected function completeQueryIfOnSellerPage($request)
    {
        $routeParameters = $this->router->match($request->getPathInfo());

        if ($routeParameters['_route'] == 'frontend.catalog.index.seller') {
            $request->query->add(['seller' => $routeParameters['seller']]);
        }
    }
}
