<?php

namespace Linio\Frontend\Twig;

use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

class PaginationExtension extends Twig_Extension
{
    const PAGINATION_HALF_COUNT = 2;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('paginate_range', [$this, 'paginateRange']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('unset_page_one', [$this, 'unsetPageOne']),
        ];
    }

    public function paginateRange($currentPage, $pageCount)
    {
        return range(
            $this->getStartCount($currentPage, $pageCount),
            $this->getEndCount($currentPage, $pageCount)
        );
    }

    /**
     * @param array $array
     * @param string|int $page
     *
     * @return array
     */
    public function unsetPageOne(array $array, $page)
    {
        if ($page == 1) {
            unset($array['page']);
        }

        return $array;
    }

    /**
     * @param int $currentPage
     * @param int $pageCount
     *
     * @return int
     */
    protected function getStartCount($currentPage, $pageCount)
    {
        if (($currentPage - self::PAGINATION_HALF_COUNT) <= 1) {
            return 1;
        } elseif ($pageCount == self::PAGINATION_HALF_COUNT * 2) {
            return 1;
        } elseif ($currentPage + self::PAGINATION_HALF_COUNT > $pageCount) {
            return $pageCount - (self::PAGINATION_HALF_COUNT * 2);
        } else {
            return $currentPage - self::PAGINATION_HALF_COUNT;
        }
    }

    /**
     * @param int $currentPage
     * @param int $pageCount
     *
     * @return int
     */
    protected function getEndCount($currentPage, $pageCount)
    {
        if (($currentPage + self::PAGINATION_HALF_COUNT) >= $pageCount) {
            return $pageCount;
        } elseif (($currentPage - self::PAGINATION_HALF_COUNT) < 1) {
            return min((self::PAGINATION_HALF_COUNT * 2) + 1, $pageCount);
        } else {
            return $currentPage + self::PAGINATION_HALF_COUNT;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'pagination';
    }
}
