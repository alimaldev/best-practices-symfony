<?php

declare(strict_types=1);

namespace Linio\Frontend\Seller;

use DateTime;
use Linio\Component\Cache\CacheService;
use Linio\Exception\NotFoundHttpException;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Seller\PaginatedReviews;
use Linio\Frontend\Entity\Seller\Rating;
use Linio\Frontend\Entity\Seller\RatingCollection;
use Linio\Frontend\Entity\Seller\RatingSummary;
use Linio\Frontend\Entity\Seller\Review;
use Linio\Frontend\Entity\Seller\ReviewCollection;
use Linio\Frontend\Entity\Seller\Seller;
use Linio\Frontend\Seller\Communication\SellerAdapter;

class SellerService
{
    const CACHE_DATA_PREFIX = 'data:';
    const CACHE_REVIEWS_PREFIX = 'review:';

    /**
     * @var CacheService
     */
    protected $cacheService;

    /**
     * @var SellerAdapter
     */
    protected $adapter;

    /**
     * @param CacheService $cacheService
     * @param SellerAdapter $adapter
     */
    public function __construct(CacheService $cacheService, SellerAdapter $adapter)
    {
        $this->cacheService = $cacheService;
        $this->adapter = $adapter;
    }

    /**
     * @param int $id
     *
     * @return Seller
     */
    public function get(int $id)
    {
        $sellerData = $this->cacheService->get(self::CACHE_DATA_PREFIX . $id);

        if (empty($sellerData)) {
            throw new NotFoundHttpException(
                sprintf('Seller with id %d not found', $id)
            );
        }

        return $this->mapSeller($id, $sellerData);
    }

    /**
     * @param int $id
     * @param int $page
     * @param int $pageSize
     *
     * @return PaginatedReviews
     */
    public function getReviews($id, $page, $pageSize)
    {
        $reviewsData = $this->cacheService->get(self::CACHE_REVIEWS_PREFIX . $id);

        if (!is_array($reviewsData) && !$reviewsData) {
            throw new NotFoundHttpException(
                sprintf('Seller with id %d not found', $id)
            );
        }

        return $this->mapSellerReviews($reviewsData, $page, $pageSize);
    }

    /**
     * @param Customer $customer
     * @param int $page
     * @param int $pageSize
     *
     * @return PaginatedResult
     */
    public function findCustomerReviews(Customer $customer, int $page, int $pageSize)
    {
        $paginatedResult = $this->adapter->findCustomerReviews($customer, $page, $pageSize);

        $results = [];

        foreach ($paginatedResult->getResult() as $orderId => $orderData) {
            $results[$orderId] = $orderData;

            foreach ($orderData['sellers'] as $sellerId => $data) {
                $seller = $this->get($sellerId);

                if (isset($results[$orderId]['sellers'][$sellerId]['name'])) {
                    $seller->setName($results[$orderId]['sellers'][$sellerId]['name']);
                }

                $results[$orderId]['sellers'][$sellerId]['seller'] = $seller;

                if (isset($results[$orderId]['sellers'][$sellerId]['review'])) {
                    /** @var SellerReview $review */
                    $review = $results[$orderId]['sellers'][$sellerId]['review'];
                    $review->setSeller($seller);
                }
            }
        }

        $paginatedResult->setResult($results);

        return $paginatedResult;
    }

    /**
     * @param int $id
     * @param array $sellerData
     *
     * @return Seller
     */
    protected function mapSeller(int $id, array $sellerData)
    {
        $ratings = new RatingCollection();
        $ratingQuantity = 0;

        foreach ($sellerData['rating_summary'] as $stars => $votes) {
            $ratings->add(new Rating((int) $stars, $votes));

            $ratingQuantity += $votes;
        }

        $ratingSummary = new RatingSummary((float) $sellerData['rating_average'], $ratingQuantity, $ratings);

        return new Seller(
            $id,
            $sellerData['name'],
            $sellerData['slug'],
            $sellerData['type'],
            $ratingSummary,
            $sellerData['operation_type']
        );
    }

    /**
     * @param array $reviews
     * @param int $page
     * @param int $pageSize
     *
     * @return PaginatedReviews
     */
    protected function mapSellerReviews(array $reviews, $page, $pageSize)
    {
        $collection = new ReviewCollection();
        $totalPages = (int) count($reviews) / $pageSize;

        $reviews = array_slice(
            $reviews, $page * $pageSize - $pageSize,
            $pageSize
        );

        foreach ($reviews as $review) {
            $collection->add(new Review(
                $review['title'],
                $review['detail'],
                $review['rating'],
                $review['reviewer'],
                new DateTime($review['reviewed_at'])
            ));
        }

        return new PaginatedReviews($page, $totalPages, $collection);
    }
}
