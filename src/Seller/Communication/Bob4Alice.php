<?php

declare(strict_types=1);

namespace Linio\Frontend\Seller\Communication;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Seller\Communication\Input\Bob4Alice as Bob4AliceInput;

class Bob4Alice implements SellerAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var Bob4AliceInput
     */
    protected $input;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param Bob4AliceInput $input
     */
    public function setInput(Bob4AliceInput $input)
    {
        $this->input = $input;
    }

    /**
     * @param Customer $customer
     * @param int $page
     * @param int $pageSize
     *
     * @throws InputException
     * @throws DomainException
     *
     * @return PaginatedResult
     */
    public function findCustomerReviews(Customer $customer, int $page, int $pageSize): PaginatedResult
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'page' => $page,
            'pageSize' => $pageSize,
        ];

        try {
            $response = $this->client->request('POST', '/customer/order/get-seller-reviews', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->input->toSellerReviews($responseBody);
    }
}
