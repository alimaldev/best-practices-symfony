<?php

declare(strict_types=1);

namespace Linio\Frontend\Seller\Communication\Input;

use DateTime;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductAware;

class Bob4Alice
{
    use ProductAware;

    /**
     * @param array $responseBody
     *
     * @return PaginatedResult
     */
    public function toSellerReviews(array $responseBody): PaginatedResult
    {
        $reviewsByOrder = [];

        foreach ($responseBody['result'] as $result) {
            $reviewsByOrder[$result['orderId']]['orderNumber'] = $result['orderNumber'];
            $reviewsByOrder[$result['orderId']]['orderDate'] = new DateTime($result['orderDate']);
            $reviewsByOrder[$result['orderId']]['sellers'][$result['sellerId']]['name'] = $result['sellerName'];

            foreach ($result['products'] as $productData) {
                try {
                    $product = $this->productService->getBySku($productData['simpleSku']);
                } catch (ProductNotFoundException $exception) {
                    $product = new Product($productData['simpleSku']);
                    $product->setName($productData['name']);
                }

                $reviewsByOrder[$result['orderId']]['sellers'][$result['sellerId']]['products'][] = $product;
            }

            if (!empty($result['review'])) {
                $reviewData = $result['review'];

                $sellerReview = new SellerReview();
                $sellerReview->setDetail($reviewData['reviewText']);
                $sellerReview->setTitle($reviewData['reviewTitle']);
                $sellerReview->setRating($reviewData['rating']);

                $reviewsByOrder[$result['orderId']]['sellers'][$result['sellerId']]['review'] = $sellerReview;
            } else {
                $reviewsByOrder[$result['orderId']]['sellers'][$result['sellerId']]['review'] = null;
            }
        }

        $paginatedResult = new PaginatedResult();
        $paginatedResult->setSize($responseBody['pagination']['size']);
        $paginatedResult->setCurrent($responseBody['pagination']['current']);
        $paginatedResult->setPages($responseBody['pagination']['total']);
        $paginatedResult->setItemCount($responseBody['pagination']['itemCount']);
        $paginatedResult->setResult($reviewsByOrder);

        return $paginatedResult;
    }
}
