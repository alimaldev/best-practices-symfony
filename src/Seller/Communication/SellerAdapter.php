<?php

declare(strict_types=1);

namespace Linio\Frontend\Seller\Communication;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

interface SellerAdapter
{
    /**
     * @param Customer $customer
     * @param int $page
     * @param int $pageSize
     *
     * @throws InputException
     * @throws DomainException
     *
     * @return PaginatedResult
     */
    public function findCustomerReviews(Customer $customer, int $page, int $pageSize): PaginatedResult;
}
