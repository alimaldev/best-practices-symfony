<?php

declare(strict_types=1);

namespace Linio\Frontend\Feed\Exception;

class SimilarProductException extends FeedException
{
}
