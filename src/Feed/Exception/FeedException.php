<?php

declare(strict_types=1);

namespace Linio\Frontend\Feed\Exception;

use Linio\Frontend\Exception\DomainException;

class FeedException extends DomainException
{
}
