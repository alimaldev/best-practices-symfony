<?php

namespace Linio\Frontend\Feed;

use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\Recommendation\RecommendationResult;

interface FeedAdapterInterface
{
    /**
     * @param string $sku
     * @param array $blacklistFilter
     *
     * @return RecommendationResult
     */
    public function findSimilar($sku, array $blacklistFilter = []);

    /**
     * @param array $blacklistFilter
     *
     * @return RecommendationResult
     */
    public function findOverallBestSellers(array $blacklistFilter = []);

    /**
     * @param Category|null $category
     * @param array $blacklistFilter
     * @param int $size
     *
     * @return RecommendationResult
     */
    public function findBestSellersByCategory(Category $category = null, array $blacklistFilter = [], $size = 10);

    /**
     * @param string $categoryUrlKey
     * @param array $blacklistFilter
     * @param int $size
     *
     * @return RecommendationResult
     */
    public function findMostViewedByCategory($categoryUrlKey, array $blacklistFilter = [], $size = 10);

    /**
     * @param array $blacklistFilter
     *
     * @return RecommendationResult
     */
    public function findOverallMostViewed(array $blacklistFilter = []);
}
