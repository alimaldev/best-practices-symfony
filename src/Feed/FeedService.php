<?php

namespace Linio\Frontend\Feed;

use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\Recommendation\RecommendationResult;

class FeedService
{
    /**
     * @var FeedAdapterInterface
     */
    protected $adapter;

    /**
     * @var array
     */
    protected $blacklistedCategories = [];

    /**
     * @param FeedAdapterInterface $adapter
     */
    public function setAdapter(FeedAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param array $blacklistedCategories
     */
    public function setBlacklistedCategories(array $blacklistedCategories)
    {
        $this->blacklistedCategories = $blacklistedCategories;
    }

    /**
     * @param string $sku
     * @param Category $productCategory
     *
     * @return RecommendationResult
     */
    public function findSimilar($sku, Category $productCategory)
    {
        $blacklistFilter = $this->getBlacklistFilter($productCategory);

        return $this->adapter->findSimilar($sku, $blacklistFilter);
    }

    /**
     * @return RecommendationResult
     */
    public function findOverallBestSellers()
    {
        return $this->adapter->findOverallBestSellers($this->blacklistedCategories);
    }

    /**
     * @return RecommendationResult
     */
    public function findOverallMostViewed()
    {
        return $this->adapter->findOverallMostViewed();
    }

    /**
     * @param Category $productCategory
     *
     * @return RecommendationResult
     */
    public function findBestSellers(Category $productCategory)
    {
        $blacklistFilter = $this->getBlacklistFilter($productCategory);

        return $this->adapter->findBestSellersByCategory($productCategory, $blacklistFilter);
    }

    /**
     * @param Category $productCategory
     *
     * @return RecommendationResult
     */
    public function findMostViewedByCategory(Category $productCategory)
    {
        $blacklistFilter = $this->getBlacklistFilter($productCategory);

        return $this->adapter->findMostViewedByCategory($productCategory->getUrlKey(), $blacklistFilter);
    }

    /**
     * @param Category $productCategory
     *
     * @return array
     */
    protected function getBlacklistFilter(Category $productCategory)
    {
        $blacklistFilter = $this->blacklistedCategories;
        $allCategoriesUrlKeys = $productCategory->getAllUrlKeys();

        if (count(array_intersect($this->blacklistedCategories, $allCategoriesUrlKeys) > 0)) {
            $blacklistFilter = array_diff($this->blacklistedCategories, $allCategoriesUrlKeys);
        }

        return $blacklistFilter;
    }
}
