<?php

namespace Linio\Frontend\Feed\Adapter;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Filesystem\Filesystem;

class MockHawkAdapter extends HawkAdapter
{
    /**
     * @var string
     */
    protected $responseFileName;

    /**
     * @param string $responseFileName
     */
    public function setResponseFileName($responseFileName)
    {
        $this->responseFileName = $responseFileName;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $mockContents = $this->getMockResponseBody();

        $handler = new MockHandler([
            new Response(200, [], $mockContents),
        ]);

        $this->client = new Client(['handler' => $handler]);
    }

    /**
     * @throws Exception
     *
     * @return string
     */
    protected function getMockResponseBody()
    {
        if (!(new Filesystem())->exists($this->responseFileName)) {
            throw new Exception(sprintf('Failed to find mock hawk response file: %s', $this->responseFileName));
        }

        return file_get_contents($this->responseFileName);
    }
}
