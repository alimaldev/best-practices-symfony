<?php

namespace Linio\Frontend\Feed\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use Linio\Component\Util\Json;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\Recommendation\RecommendationResult;
use Linio\Frontend\Feed\FeedAdapterInterface;
use Linio\Frontend\Feed\ResultSetMapper\RecommendationMapper;
use Linio\Frontend\Security\TokenStorageAware;
use LogicException;

class HawkAdapter implements FeedAdapterInterface
{
    use TokenStorageAware;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $authorizationKey;

    /**
     * @var RecommendationMapper
     */
    protected $recommendationMapper;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @param string $authorizationKey
     */
    public function setAuthorizationKey($authorizationKey)
    {
        $this->authorizationKey = $authorizationKey;
    }

    /**
     * @param RecommendationMapper $recommendationMapper
     */
    public function setRecommendationMapper($recommendationMapper)
    {
        $this->recommendationMapper = $recommendationMapper;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function findSimilar($sku, array $blacklistFilter = [])
    {
        $query['key'] = $this->authorizationKey;
        $query['method'] = 'default';
        $query['sku'] = $sku;

        if (!empty($blacklistFilter)) {
            $query['bl'] = $blacklistFilter;
        }

        $url = sprintf('%s%s', $this->baseUrl, 'morelikethis');

        try {
            $results = $this->client->get($url, ['query' => $this->buildQueryString($query), 'timeout' => 3]);
        } catch (TransferException $e) {
            return new RecommendationResult();
        }

        return $this->parseRecommendationResults($results);
    }

    /**
     * {@inheritdoc}
     */
    public function findOverallBestSellers(array $blacklistFilter = [])
    {
        return $this->findBestSellersByCategory(null, $blacklistFilter);
    }

    /**
     * @param array $blacklistFilter
     *
     * @return RecommendationResult
     */
    public function findOverallMostViewed(array $blacklistFilter = [])
    {
        return $this->findMostViewedByCategory(null, $blacklistFilter);
    }

    /**
     * {@inheritdoc}
     */
    public function findBestSellersByCategory(Category $category = null, array $blacklistFilter = [], $size = 10)
    {
        $query['key'] = $this->authorizationKey;
        $query['method'] = 'default';
        $query['from'] = 'now-1d';
        $query['size'] = $size;

        if ($category) {
            $conversion = ['á' => 'a', 'é' => 'e', 'í' => 'i', 'ó' => 'o', 'ú' => 'u', 'ñ' => 'n', ',' => ''];

            $query['category'] = strtr($category->getName(), $conversion);
        }

        if (!empty($blacklistFilter)) {
            $query['bl'] = $blacklistFilter;
        }

        $query['user_id'] = $this->getCustomer()->getLinioId();
        $query['web_id'] = $this->getCustomer()->getLinioId();

        $url = sprintf('%s%s', $this->baseUrl, 'bestsellers');

        try {
            $results = $this->client->get($url, ['query' => $this->buildQueryString($query), 'timeout' => 3]);
        } catch (TransferException $exception) {
            return new RecommendationResult();
        }

        return $this->parseRecommendationResults($results);
    }

    /**
     * This method is needed because Guzzle doesn't support duplicated querystring parameter names.
     *
     * @param array $queryParams
     *
     * @return string
     */
    protected function buildQueryString(array $queryParams)
    {
        $queryStringParts = [];

        foreach ($queryParams as $paramName => $paramValue) {
            if (!is_array($paramValue)) {
                $queryStringParts[] = sprintf('%s=%s', $paramName, $paramValue);
                continue;
            }

            foreach ($paramValue as $paramArrayValue) {
                $queryStringParts[] = sprintf('%s=%s', $paramName, $paramArrayValue);
            }
        }

        return implode('&', $queryStringParts);
    }

    /**
     * {@inheritdoc}
     */
    public function findMostViewedByCategory($categoryUrlKey = null, array $blacklistFilter = [], $size = 10)
    {
        $query['key'] = $this->authorizationKey;
        $query['method'] = 'default';
        $query['from'] = 'now-1d';
        $query['size'] = $size;

        if ($categoryUrlKey) {
            $query['category'] = $categoryUrlKey;
        }

        if (!empty($blacklistFilter)) {
            $query['bl'] = $blacklistFilter;
        }

        $url = sprintf('%s%s', $this->baseUrl, 'mostviewed');

        try {
            $results = $this->client->get($url, ['query' => $this->buildQueryString($query), 'timeout' => 3]);
        } catch (TransferException $exception) {
            return new RecommendationResult();
        }

        return $this->parseRecommendationResults($results);
    }

    /**
     * @param string $result
     *
     * @return RecommendationResult
     */
    protected function parseRecommendationResults($result)
    {
        try {
            $data = Json::decode($result->getBody()->getContents());
        } catch (LogicException $exception) {
            return new RecommendationResult();
        }

        if (!is_array($data)) {
            return new RecommendationResult();
        }

        $products = $this->recommendationMapper->map($data);

        if (empty($products)) {
            return new RecommendationResult();
        }

        $recommendationResult = new RecommendationResult();

        $recommendationResult->setTotalProductsFound(count($products));
        $recommendationResult->setProducts($products);

        return $recommendationResult;
    }
}
