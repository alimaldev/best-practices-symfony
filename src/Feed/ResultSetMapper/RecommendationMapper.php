<?php

namespace Linio\Frontend\Feed\ResultSetMapper;

use Exception;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Catalog\Recommendation\Product;
use Linio\Type\Money;
use Symfony\Component\HttpFoundation\RequestStack;

class RecommendationMapper implements MapperInterface
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public function map(array $results)
    {
        $recommendations = [];

        if (!isset($results['items'])) {
            return $recommendations;
        }

        foreach ($results['items'] as $product) {
            try {
                if (!isset($product['images'])) {
                    continue;
                }

                $recommendationProduct = new Product();

                $price = $product['price'];

                if (isset($price['previous']) && $price['previous'] > 0) {
                    $recommendationProduct->setPercentageOff(100 - (int) ($price['current'] * 100 / $price['previous']));
                    $recommendationProduct->setOriginalPrice(new Money($price['previous']));
                    $recommendationProduct->setPrice(new Money($price['current']));
                } else {
                    $recommendationProduct->setPrice(new Money($price['current']));
                }

                $recommendationProduct->setBrand($product['brand']['name']);
                $recommendationProduct->setImage(str_replace(['http:', 'https:'], '', $product['images'][0]));
                $recommendationProduct->setTitle($product['title']);
                $recommendationProduct->setUrl(
                    sprintf('%s://%s', $this->requestStack->getCurrentRequest()->getScheme(), $product['url'])
                );

                $recommendations[] = $recommendationProduct;
            } catch (Exception $exception) {
                continue;
            }
        }

        return $recommendations;
    }
}
