<?php

namespace Linio\Frontend\Feed;

trait FeedAware
{
    /**
     * @var FeedService
     */
    protected $feedService;

    /**
     * @param FeedService $feedService
     */
    public function setFeedService(FeedService $feedService)
    {
        $this->feedService = $feedService;
    }
}
