<?php

namespace Linio\Frontend\Seo;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Communication\Country\CountryAware;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\Seo;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Seller;

class SeoService
{
    use CacheAware;
    use CountryAware;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var Seo
     */
    protected $seoEntity;

    public function __construct()
    {
        $this->seoEntity = new Seo();
    }

    /**
     * @param string $namespace
     * @param string $context
     * @param string $id
     *
     * @return array
     */
    protected function getSeoData($namespace, $context = 'desktop', $id = null)
    {
        if ($id) {
            $entityKey = sprintf('%s:seo:%s:%s', $context, $namespace, $id);
        } else {
            $entityKey = sprintf('%s:seo:%s', $context, $namespace);
        }

        $seoData = $this->cacheService->get($entityKey);

        return $seoData ? $seoData : [];
    }

    /**
     * @param int $categoryId
     */
    public function setUpCategorySeo($categoryId)
    {
        $seoConfiguration = $this->getSeoData('category', 'desktop', $categoryId);

        if ($seoConfiguration) {
            $this->setSeoConfiguration($seoConfiguration);
        }
    }

    /**
     * @param Category $category
     * @param Brand $brand
     */
    public function setUpSeoForCategoryAndBrand(Category $category, Brand $brand)
    {
        $configuration = $this->getSeoData(
            'category_brand', 'desktop', sprintf('%s_%s', $category->getId(), $brand->getId())
        );

        $this->setSeoConfiguration($configuration);
    }

    /**
     * @param Product $product
     */
    public function setUpProductSeo(Product $product)
    {
        $seoConfiguration = $this->getSeoData('product', 'desktop', $product->getSku());

        if ($seoConfiguration) {
            $this->setSeoConfiguration($seoConfiguration);
        }

        if ($product->isMarketPlace()) {
            $this->seoEntity->setRobots('noindex,follow');
        }
    }

    public function setUpHomeSeo()
    {
        $seoConfiguration = $this->getSeoData('home');

        $this->setSeoConfiguration($seoConfiguration);
    }

    /**
     * @param Campaign $campaign
     */
    public function setupCampaignSeo(Campaign $campaign)
    {
        $seoData = null;

        if ($campaign->getCurrentSegment()) {
            $seoData = $this->getSeoData('segment', 'desktop', $campaign->getCurrentSegment()->getId());
        }

        if (empty($seoData)) {
            $seoData = $this->getSeoData('campaign', 'desktop', $campaign->getId());
        }

        $this->setSeoConfiguration($seoData);
    }

    /**
     * @param Brand $brandSlug
     */
    public function setupBrandSeo(Brand $brandSlug)
    {
        $seoConfiguration = $this->getSeoData('brand', 'desktop', $brandSlug->getId());

        $this->setSeoConfiguration($seoConfiguration);
    }

    /**
     * @param Seller $seller
     */
    public function setupSellerSeo(Seller $seller)
    {
        $seoConfiguration = $this->getSeoData('supplier', 'desktop', $seller->getSellerId());

        $this->setSeoConfiguration($seoConfiguration);
    }

    /**
     * @param array $staticPage
     */
    public function setUpStaticPageSeo(array $staticPage)
    {
        if (isset($staticPage['seo'])) {
            $this->setSeoConfiguration($staticPage['seo']);
        }
    }

    /**
     * @return Seo
     */
    public function getSeoEntity()
    {
        return $this->seoEntity;
    }

    /**
     * @param array $seoConfiguration
     */
    protected function setSeoConfiguration(array $seoConfiguration)
    {
        $this->seoEntity->setTitle(isset($seoConfiguration['title']) ? $seoConfiguration['title'] : null);
        $this->seoEntity->setDescription(isset($seoConfiguration['description']) ? $seoConfiguration['description'] : null);
        $this->seoEntity->setRobots(isset($seoConfiguration['robots']) ? $seoConfiguration['robots'] : null);
        $this->seoEntity->setKeywords(isset($seoConfiguration['keywords']) ? $seoConfiguration['keywords'] : null);
        $this->seoEntity->setSeoText(isset($seoConfiguration['seo_text']) ? $seoConfiguration['seo_text'] : null);
    }
}
