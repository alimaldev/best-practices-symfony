<?php

namespace Linio\Frontend\Seo;

trait SeoAware
{
    /**
     * @var SeoService
     */
    protected $seoService;

    /**
     * @codeCoverageIgnore
     *
     * @return SeoService
     */
    public function getSeoService()
    {
        return $this->seoService;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param SeoService $seoService
     */
    public function setSeoService(SeoService $seoService)
    {
        $this->seoService = $seoService;
    }
}
