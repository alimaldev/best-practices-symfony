<?php

declare(strict_types=1);

namespace Linio\Frontend\Seo\Exception;

class SeoNotFoundException extends SeoException
{
}
