<?php

declare(strict_types=1);

namespace Linio\Frontend\Seo\Exception;

use Linio\Frontend\Exception\DomainException;

class SeoException extends DomainException
{
}
