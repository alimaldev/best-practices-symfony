<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership;

trait ManageMembershipsAware
{
    /**
     * @var ManageMemberships
     */
    protected $manageMemberships;

    /**
     * @param ManageMemberships $manageMemberships
     */
    public function setManageMemberships(ManageMemberships $manageMemberships)
    {
        $this->manageMemberships = $manageMemberships;
    }
}
