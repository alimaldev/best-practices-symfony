<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership\Communication\LoyaltyProgram;

use DateTime;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

class Bob4Alice implements LoyaltyProgramAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param Customer $customer
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function signup(Customer $customer, LoyaltyProgram $loyaltyProgram)
    {
        $this->update($customer, $loyaltyProgram);
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return LoyaltyProgram
     */
    public function getProgram(Customer $customer): LoyaltyProgram
    {
        $requestBody = [
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/loyalty/get-account', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $loyaltyProgram = new LoyaltyProgram();
        $loyaltyProgram->setName($responseBody['programName']);
        $loyaltyProgram->setId($responseBody['registrationNumber']);
        $loyaltyProgram->setRegistrationDate(new DateTime($responseBody['registrationDate']));

        return $loyaltyProgram;
    }

    /**
     * @param Customer $customer
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function update(Customer $customer, LoyaltyProgram $loyaltyProgram)
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'registrationNumber' => $loyaltyProgram->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/loyalty/set-registration-number', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }
    }
}
