<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership\Communication\LoyaltyProgram;

use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

interface LoyaltyProgramAdapter
{
    /**
     * @param Customer $customer
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function signup(Customer $customer, LoyaltyProgram $loyaltyProgram);

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return LoyaltyProgram
     */
    public function getProgram(Customer $customer): LoyaltyProgram;

    /**
     * @param Customer $customer
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function update(Customer $customer, LoyaltyProgram $loyaltyProgram);
}
