<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership\Communication\Partnership;

use DateTime;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Membership\Partnership;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

class Bob4Alice implements PartnershipAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param Customer $customer
     * @param Partnership $partnership
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function signup(Customer $customer, Partnership $partnership)
    {
        $this->update($customer, $partnership);
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return Partnership[]
     */
    public function getPartnerships(Customer $customer): array
    {
        $requestBody = [
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/partnership/list', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $partnerships = [];

        foreach ($responseBody as $partnershipData) {
            $partnership = new Partnership($partnershipData['partnership']['code'], $partnershipData['identifier']);
            $partnership->setName($partnershipData['partnership']['name']);

            if (isset($partnershipData['level'])) {
                $partnership->setLevel($partnershipData['level']);
            }

            if (isset($partnershipData['lastStatusCheck'])) {
                $partnership->setLastStatusCheck(new DateTime($partnershipData['lastStatusCheck']));
            }

            if ($partnershipData['isActive'] === true) {
                $partnership->enable();
            }

            $partnerships[] = $partnership;
        }

        return $partnerships;
    }

    /**
     * @param Customer $customer
     * @param Partnership $partnership
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function update(Customer $customer, Partnership $partnership)
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnership->getCode(),
            'identifier' => $partnership->getAccountNumber(),
            'level' => $partnership->getLevel(),
        ];

        try {
            $response = $this->client->request('POST', '/partnership/update', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }
    }

    /**
     * @param Customer $customer
     * @param string $partnershipCode
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function remove(Customer $customer, string $partnershipCode)
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'partnershipCode' => $partnershipCode,
        ];

        try {
            $response = $this->client->request('POST', '/partnership/delete', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }
    }
}
