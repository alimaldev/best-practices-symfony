<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership\Communication\Partnership;

use Linio\Frontend\Customer\Membership\Partnership;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

interface PartnershipAdapter
{
    /**
     * @param Customer $customer
     * @param Partnership $partnership
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function signup(Customer $customer, Partnership $partnership);

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return Partnership[]
     */
    public function getPartnerships(Customer $customer): array;

    /**
     * @param Customer $customer
     * @param Partnership $partnership
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function update(Customer $customer, Partnership $partnership);

    /**
     * @param Customer $customer
     * @param string $partnershipCode
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function remove(Customer $customer, string $partnershipCode);
}
