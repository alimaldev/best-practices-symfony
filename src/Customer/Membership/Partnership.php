<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership;

use DateTime;

class Partnership
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $accountNumber;

    /**
     * @var string
     */
    protected $level;

    /**
     * @var bool
     */
    protected $active = false;

    /**
     * @var DateTime|null
     */
    protected $lastStatusCheck;

    /**
     * @param string $code
     * @param string $accountNumber
     */
    public function __construct(string $code, string $accountNumber = null)
    {
        $this->code = $code;
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function updateAccountNumber(string $accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel(string $level)
    {
        $this->level = $level;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    public function enable()
    {
        $this->active = true;
    }

    public function disable()
    {
        $this->active = false;
    }

    /**
     * @return DateTime|null
     */
    public function getLastStatusCheck()
    {
        return $this->lastStatusCheck;
    }

    /**
     * @param DateTime $lastStatusCheck
     */
    public function setLastStatusCheck(DateTime $lastStatusCheck)
    {
        $this->lastStatusCheck = $lastStatusCheck;
    }
}
