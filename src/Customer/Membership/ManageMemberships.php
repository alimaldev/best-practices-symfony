<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Membership;

use Linio\Frontend\Customer\Membership\Communication\LoyaltyProgram\LoyaltyProgramAdapter;
use Linio\Frontend\Customer\Membership\Communication\Partnership\PartnershipAdapter;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

class ManageMemberships
{
    /**
     * @var LoyaltyProgramAdapter
     */
    protected $loyaltyProgramAdapter;

    /**
     * @var PartnershipAdapter
     */
    protected $partnershipAdapter;

    /**
     * @param LoyaltyProgramAdapter $loyaltyProgramAdapter
     */
    public function setLoyaltyProgramAdapter(LoyaltyProgramAdapter $loyaltyProgramAdapter)
    {
        $this->loyaltyProgramAdapter = $loyaltyProgramAdapter;
    }

    /**
     * @param PartnershipAdapter $partnershipAdapter
     */
    public function setPartnershipAdapter(PartnershipAdapter $partnershipAdapter)
    {
        $this->partnershipAdapter = $partnershipAdapter;
    }

    /**
     * @param Customer $customer
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function signupForLoyaltyProgram(Customer $customer, LoyaltyProgram $loyaltyProgram)
    {
        $this->loyaltyProgramAdapter->signup($customer, $loyaltyProgram);
    }

    /**
     * @param Customer $customer
     * @param Partnership $partnership
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function signupForPartnership(Customer $customer, Partnership $partnership)
    {
        $this->partnershipAdapter->signup($customer, $partnership);
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return LoyaltyProgram
     */
    public function getLoyaltyProgram(Customer $customer): LoyaltyProgram
    {
        return $this->loyaltyProgramAdapter->getProgram($customer);
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return Partnership[]
     */
    public function getPartnerships(Customer $customer): array
    {
        return $this->partnershipAdapter->getPartnerships($customer);
    }

    /**
     * @param Customer $customer
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function updateLoyaltyProgram(Customer $customer, LoyaltyProgram $loyaltyProgram)
    {
        $this->loyaltyProgramAdapter->update($customer, $loyaltyProgram);
    }

    /**
     * @param Customer $customer
     * @param Partnership $partnership
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function updatePartnership(Customer $customer, Partnership $partnership)
    {
        $this->partnershipAdapter->update($customer, $partnership);
    }

    /**
     * @param Customer $customer
     * @param string $partnershipCode
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     */
    public function removePartnership(Customer $customer, string $partnershipCode)
    {
        $this->partnershipAdapter->remove($customer, $partnershipCode);
    }
}
