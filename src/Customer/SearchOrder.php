<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

use Linio\Frontend\Customer\Communication\Order\OrderAdapter;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;

class SearchOrder
{
    /**
     * @var OrderAdapter
     */
    protected $adapter;

    /**
     * @param OrderAdapter $adapter
     */
    public function setAdapter(OrderAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param Customer $customer
     * @param Search $search
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     * @throws InputException
     *
     * @return PaginatedResult
     */
    public function findOrders(Customer $customer, Search $search): PaginatedResult
    {
        if ($search->getToDate() < $search->getFromDate()) {
            throw new InputException(ExceptionMessage::ORDER_SEARCH_DATE_RANGE_INVALID);
        }

        return $this->adapter->findOrders($customer, $search);
    }

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order
     */
    public function findOrder(Customer $customer, string $orderNumber): Order
    {
        return $this->adapter->findOrder($customer, $orderNumber);
    }

    /**
     * @param Customer $customer
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order[]
     */
    public function findLoyaltyOrders(Customer $customer): array
    {
        return $this->adapter->findLoyaltyOrders($customer);
    }

    /**
     * @param Customer $customer
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order[]
     */
    public function findOrdersPendingBankConfirmation(Customer $customer): array
    {
        return $this->adapter->findOrdersPendingBankConfirmation($customer);
    }

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Shipment[]
     */
    public function findOrderTrackingHistory(Customer $customer, string $orderNumber): array
    {
        return $this->adapter->findOrderTrackingHistory($customer, $orderNumber);
    }

    /**
     * Returns an Order containing only items that are returnable and no packages.
     *
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order
     */
    public function findOrderWithReturnableItems(Customer $customer, string $orderNumber): Order
    {
        return $this->adapter->findOrderWithReturnableItems($customer, $orderNumber);
    }
}
