<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\AddressBook;

use DateTime;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Communication\Output\Bob4Alice\Address as AddressOutput;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Location\Exception\AddressException;
use Linio\Frontend\Request\PlatformInformation;

class Bob4Alice implements AddressBookAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var AddressOutput
     */
    protected $addressOutput;

    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @var int
     */
    protected $countryId;

    /**
     * @var bool
     */
    protected $useRegionId;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function setAddressOutput(AddressOutput $addressOutput)
    {
        $this->addressOutput = $addressOutput;
    }

    /**
     * @param PlatformInformation $platformInformation
     */
    public function setPlatformInformation(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId(int $countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @param bool $useRegionId
     */
    public function setUseRegionId(bool $useRegionId)
    {
        $this->useRegionId = $useRegionId;
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     *
     * @return array
     */
    public function getAddresses(Customer $customer): array
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/address/list', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new AddressException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->addressOutput->toAddresses($responseBody);
    }

    /**
     * @param Customer $customer
     * @param int $addressId
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     *
     * @return Address
     */
    public function getAddress(Customer $customer, int $addressId): Address
    {
        $addresses = $this->getAddresses($customer);

        foreach ($addresses as $address) {
            if ($address->getId() == $addressId) {
                return $address;
            }
        }

        throw new InputException(ExceptionMessage::ERROR_ADDRESS_NOT_EXISTENT);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function createAddress(Customer $customer, Address $address)
    {
        $requestBody = $this->mapAddressToRequest($customer, $address);

        try {
            $response = $this->client->request('POST', '/address/create', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new AddressException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $address->setId($responseBody['id']);
        $address->setCreatedAt(new DateTime($responseBody['createdAt']));
        $address->setUpdatedAt(new DateTime($responseBody['updatedAt']));
        $address->setDefaultShipping((bool) $responseBody['isDefaultShippingAddress']);
        $address->setDefaultBilling((bool) $responseBody['isDefaultBillingAddress']);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function updateAddress(Customer $customer, Address $address)
    {
        $requestBody = $this->mapAddressToRequest($customer, $address);

        try {
            $this->client->request('POST', '/address/update', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new AddressException($responseBody['code']);
        }
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function removeAddress(Customer $customer, Address $address)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'addressId' => $address->getId(),
        ];

        try {
            $this->client->request('POST', '/address/delete', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new AddressException($responseBody['code']);
        }
    }

    /**
     * @todo Add the missing 3 mappings
     *
     * @param Customer $customer
     * @param Address $address
     *
     * @return array
     */
    protected function mapAddressToRequest(Customer $customer, Address $address): array
    {
        $data = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'id' => $address->getId(),
            'title' => $address->getTitle(),
            'type' => $address->getType(),
            'firstName' => $address->getFirstName(),
            'lastName' => $address->getLastName(),
            'prefix' => $address->getPrefix(),
            'line1' => $address->getLine1(),
            'line2' => $address->getLine2(),
            'betweenStreet1' => $address->getBetweenStreet1(),
            'betweenStreet2' => $address->getBetweenStreet2(),
            'streetNumber' => $address->getStreetNumber(),
            'apartment' => $address->getApartment(),
            'lot' => $address->getLot(),
            'neighborhood' => $address->getNeighborhood(),
            'department' => $address->getDepartment(),
            'municipality' => $address->getMunicipality(),
            'urbanization' => $address->getUrbanization(),
            'city' => $address->getCity(),
            'cityId' => $address->getCityId(),
            'cityName' => $address->getCityName(),
            'region' => $address->getRegion(),
            'regionId' => $address->getRegionId(),
            'regionCode' => $address->getRegionCode(),
            'regionName' => $address->getRegionName(),
            'postcode' => $address->getPostcode(),
            'additionalInformation' => $address->getAdditionalInformation(),
            'phone' => $address->getPhone(),
            'mobilePhone' => $address->getMobilePhone(),
            'countryId' => $address->getCountryId() ?? $this->countryId,
            'countryCode' => $address->getCountryCode(),
            'countryName' => $address->getCountryName(),
            'taxIdentificationNumber' => $address->getTaxIdentificationNumber(),
            'isDefaultBillingAddress' => $address->isDefaultBilling(),
            'isDefaultShippingAddress' => $address->isDefaultShipping(),
            'maternalName' => $address->getMaternalName(),
            'invoiceType' => $address->getInvoiceType(),
        ];

        if ($this->useRegionId) {
            $data['region'] = null;
            $data['regionId'] = (int) $address->getRegion();
        }

        if ($address->getId()) {
            $data['id'] = $address->getId();
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultShipping(Customer $customer, Address $address)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'addressId' => $address->getId(),
        ];

        try {
            $this->client->request('POST', '/address/default-shipping', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new AddressException($responseBody['code']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultBilling(Customer $customer, Address $address)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'addressId' => $address->getId(),
        ];

        try {
            $this->client->request('POST', '/address/default-billing', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new AddressException($responseBody['code']);
        }
    }
}
