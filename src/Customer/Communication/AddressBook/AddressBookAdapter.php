<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\AddressBook;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Location\Exception\AddressException;

interface AddressBookAdapter
{
    /**
     * @param Customer $customer
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     *
     * @return Address[]
     */
    public function getAddresses(Customer $customer): array;

    /**
     * @param Customer $customer
     * @param int $addressId
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     *
     * @return Address
     */
    public function getAddress(Customer $customer, int $addressId): Address;

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function createAddress(Customer $customer, Address $address);

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function updateAddress(Customer $customer, Address $address);

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function removeAddress(Customer $customer, Address $address);

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function setDefaultShipping(Customer $customer, Address $address);

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException Thrown for client errors
     * @throws AddressException Thrown for server errors
     */
    public function setDefaultBilling(Customer $customer, Address $address);
}
