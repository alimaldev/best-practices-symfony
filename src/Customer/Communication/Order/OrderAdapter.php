<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Order;

use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;

interface OrderAdapter
{
    /**
     * @param Customer $customer
     * @param Search $search
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return PaginatedResult
     */
    public function findOrders(Customer $customer, Search $search): PaginatedResult;

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order
     */
    public function findOrder(Customer $customer, string $orderNumber): Order;

    /**
     * @param Customer $customer
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order[]
     */
    public function findLoyaltyOrders(Customer $customer): array;

    /**
     * @param Customer $customer
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order[]
     */
    public function findOrdersPendingBankConfirmation(Customer $customer): array;

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return array
     */
    public function findOrderTrackingHistory(Customer $customer, string $orderNumber): array;

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order
     */
    public function findOrderWithReturnableItems(Customer $customer, string $orderNumber): Order;
}
