<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Order;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Customer\Communication\Output\Bob4Alice\Order as OrderOutput;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\PaginatedResult;
use RuntimeException;

class Bob4Alice implements OrderAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var OrderOutput
     */
    protected $orderOutput;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param OrderOutput $orderOutput
     */
    public function setOrderOutput(OrderOutput $orderOutput)
    {
        $this->orderOutput = $orderOutput;
    }

    /**
     * @param Customer $customer
     * @param Search $search
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return PaginatedResult
     */
    public function findOrders(Customer $customer, Search $search): PaginatedResult
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'pageSize' => $search->getPageSize(),
            'page' => $search->getPage(),
            'fromDate' => $search->getFromDate()->format('Y-m-d'),
            'toDate' => $search->getToDate()->format('Y-m-d'),
        ];

        try {
            $response = $this->client->request('POST', '/customer/order/list', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        }

        return $this->orderOutput->toPaginatedOrders(Json::decode((string) $response->getBody()));
    }

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order
     */
    public function findOrder(Customer $customer, string $orderNumber): Order
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'orderNumber' => $orderNumber,
        ];

        try {
            $response = $this->client->request('POST', '/customer/order/get', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        }

        return $this->orderOutput->toOrder(Json::decode((string) $response->getBody()));
    }

    /**
     * @param Customer $customer
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return array
     */
    public function findLoyaltyOrders(Customer $customer): array
    {
        throw new RuntimeException(sprintf('Method not implemented: %s at %s:%s', __METHOD__, __FILE__, __LINE__));
    }

    /**
     * @param Customer $customer
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order[]
     */
    public function findOrdersPendingBankConfirmation(Customer $customer): array
    {
        // B4A was implemented with pagination, but we need all orders, so we'll use some sane defaults
        $requestBody = [
            'customerId' => $customer->getId(),
            'pageSize' => 100,
            'page' => 1,
        ];

        try {
            $response = $this->client->request('POST', '/customer/order/pending-bank-transfer', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        }

        return $this->orderOutput->toOrders(Json::decode((string) $response->getBody()));
    }

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return array
     */
    public function findOrderTrackingHistory(Customer $customer, string $orderNumber): array
    {
        $requestBody = [
            'customerId' => $customer->getId(),
            'orderNumber' => $orderNumber,
        ];

        try {
            $response = $this->client->request('POST', '/customer/order/tracking', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        }

        return $this->orderOutput->toOrderShipments(Json::decode((string) $response->getBody()));
    }

    /**
     * @param Customer $customer
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return Order
     */
    public function findOrderWithReturnableItems(Customer $customer, string $orderNumber): Order
    {
        $order = $this->findOrder($customer, $orderNumber);

        $items = $order->getItems();

        foreach ($items as $key => $item) {
            if (!$item->isReturnAllowed()) {
                unset($items[$key]);
            }
        }

        // Package information isn't needed here.
        // Plus if we have to keep the packages, we have to do some ugly code
        $order->setPackages([]);
        $order->setItems(array_values($items));

        return $order;
    }
}
