<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Coupon\Input;

use DateTime;
use Linio\Frontend\Entity\Customer\Coupon;
use Linio\Type\Money;

class Bob4Alice implements CustomerCouponInput
{
    /**
     * {@inheritdoc}
     */
    public function toCoupon(array $data): Coupon
    {
        $coupon = new Coupon();
        $coupon->setCode($data['code']);
        $coupon->setValidFrom(new DateTime($data['validFrom']));
        $coupon->setValidUntil(new DateTime($data['validUntil']));
        $coupon->setActive((bool) $data['isActive']);

        if ($data['discountType'] == Coupon::DISCOUNT_TYPE_PERCENT) {
            $coupon->setDiscountType(Coupon::DISCOUNT_TYPE_PERCENT);
            $coupon->setDiscountPercentage($data['discountPercentage'] / 100);
        } else {
            $coupon->setBalance(Money::fromCents($data['amountAvailable']));
            $coupon->setAmount(Money::fromCents($data['amount']));
            $coupon->setDiscountType(Coupon::DISCOUNT_TYPE_MONEY);
        }

        return $coupon;
    }

    /**
     * {@inheritdoc}
     */
    public function toCoupons(array $data): array
    {
        $coupons = [];

        foreach ($data as $coupon) {
            $coupons[] = $this->toCoupon($coupon);
        }

        return $coupons;
    }
}
