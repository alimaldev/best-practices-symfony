<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Coupon\Input;

use Linio\Frontend\Entity\Customer\Coupon;

interface CustomerCouponInput
{
    /**
     * @param array $data
     *
     * @return Coupon
     */
    public function toCoupon(array $data): Coupon;

    /**
     * @param array $data
     *
     * @return Coupon[]
     */
    public function toCoupons(array $data): array;
}
