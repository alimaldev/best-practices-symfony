<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Output\Bob4Alice;

use DateTime;
use Linio\Frontend\Location\Address as AddressEntity;

class Address
{
    /**
     * @param array $data
     *
     * @return AddressEntity[]
     */
    public function toAddresses(array $data)
    {
        $addresses = [];

        foreach ($data as $row) {
            $address = new AddressEntity();

            if (isset($row['id'])) {
                $address->setId($row['id']);
            }

            if (isset($row['title'])) {
                $address->setTitle($row['title']);
            }

            if (isset($row['type'])) {
                $address->setType($row['type']);
            }

            if (isset($row['firstName'])) {
                $address->setFirstName($row['firstName']);
            }

            if (isset($row['lastName'])) {
                $address->setLastName($row['lastName']);
            }

            if (isset($row['prefix'])) {
                $address->setPrefix($row['prefix']);
            }

            if (isset($row['line1'])) {
                $address->setLine1($row['line1']);
            }

            if (isset($row['line2'])) {
                $address->setLine2($row['line2']);
            }

            if (isset($row['betweenStreet1'])) {
                $address->setBetweenStreet1($row['betweenStreet1']);
            }

            if (isset($row['betweenStreet2'])) {
                $address->setBetweenStreet2($row['betweenStreet2']);
            }

            if (isset($row['streetNumber'])) {
                $address->setStreetNumber($row['streetNumber']);
            }

            if (isset($row['apartment'])) {
                $address->setApartment($row['apartment']);
            }

            if (isset($row['lot'])) {
                $address->setLot($row['lot']);
            }

            if (isset($row['neighborhood'])) {
                $address->setNeighborhood($row['neighborhood']);
            }

            if (isset($row['department'])) {
                $address->setDepartment($row['department']);
            }

            if (isset($row['municipality'])) {
                $address->setMunicipality($row['municipality']);
            }

            if (isset($row['urbanization'])) {
                $address->setUrbanization($row['urbanization']);
            }

            if (isset($row['city'])) {
                $address->setCity($row['city']);
            }

            if (isset($row['cityId'])) {
                $address->setCityId($row['cityId']);
            }

            if (isset($row['cityName'])) {
                $address->setCityName($row['cityName']);
            }

            if (isset($row['region'])) {
                $address->setRegion($row['region']);
            }

            if (isset($row['regionId'])) {
                $address->setRegionId($row['regionId']);
            }

            if (isset($row['regionCode'])) {
                $address->setRegionCode($row['regionCode']);
            }

            if (isset($row['regionName'])) {
                $address->setRegionName($row['regionName']);
            }

            if (isset($row['postcode'])) {
                $address->setPostcode($row['postcode']);
            }

            if (isset($row['additionalInformation'])) {
                $address->setAdditionalInformation($row['additionalInformation']);
            }

            if (isset($row['phone'])) {
                $address->setPhone($row['phone']);
            }

            if (isset($row['mobilePhone'])) {
                $address->setMobilePhone($row['mobilePhone']);
            }

            if (isset($row['countryId'])) {
                $address->setCountryId($row['countryId']);
            }

            if (isset($row['countryCode'])) {
                $address->setCountryCode($row['countryCode']);
            }

            if (isset($row['countryName'])) {
                $address->setCountryName($row['countryName']);
            }

            if (isset($row['taxIdentificationNumber'])) {
                $address->setTaxIdentificationNumber($row['taxIdentificationNumber']);
            }

            if (isset($row['isDefaultBillingAddress'])) {
                $address->setDefaultBilling($row['isDefaultBillingAddress']);
            }

            if (isset($row['isDefaultShippingAddress'])) {
                $address->setDefaultShipping($row['isDefaultShippingAddress']);
            }

            if (isset($row['maternalName'])) {
                $address->setMaternalName($row['maternalName']);
            }

            if (isset($row['invoiceType'])) {
                $address->setInvoiceType($row['invoiceType']);
            }

            if (isset($row['createdAt'])) {
                $address->setCreatedAt(new DateTime($row['createdAt']));
            }

            if (isset($row['updatedAt'])) {
                $address->setUpdatedAt(new DateTime($row['updatedAt']));
            }

            $addresses[] = $address;
        }

        return $addresses;
    }
}
