<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Communication\Output\Bob4Alice;

use DateTime;
use Linio\Frontend\Customer\Name;
use Linio\Frontend\Customer\Order as OrderEntity;
use Linio\Frontend\Customer\Order\Coupon;
use Linio\Frontend\Customer\Order\Package;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Payment;
use Linio\Frontend\Customer\Order\Payment\Installments;
use Linio\Frontend\Customer\Order\Shipping;
use Linio\Frontend\Customer\Order\Shipping\Address as ShippingAddress;
use Linio\Frontend\Customer\Order\Shipping\PickupStore;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Customer\Order\Shipping\TrackingEvent;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\Wallet;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductAware;
use Linio\Type\Date;
use Linio\Type\Money;

class Order
{
    use ProductAware;

    /**
     * @var string
     */
    protected $imageHost;

    /**
     * @param string $imageHost
     */
    public function setImageHost(string $imageHost)
    {
        $this->imageHost = $imageHost;
    }

    /**
     * @param array $data
     *
     * @return OrderEntity
     */
    public function toOrder(array $data): OrderEntity
    {
        $order = new OrderEntity($data['id']);
        $order->setOrderNumber($data['orderNumber']);
        $order->setFastLane($data['isFastLane']);
        $order->setSubtotal(Money::fromCents($data['subtotal']));
        $order->setGrandTotal(Money::fromCents($data['grandTotal']));
        $order->setStatusProgressConfiguration($data['statusProgressConfiguration']);
        $order->setCreatedAt(new DateTime($data['createdAt']));
        $order->setInvoiceAvailable($data['invoiceAvailable']);

        $this->mapCustomer($order, $data['customer']);
        $this->mapWallet($order, $data['wallet']);
        $this->mapPayment($order, $data['payment']);
        $this->mapShipping($order, $data['shipping']);
        $this->mapPackages($order, $data['packages']);
        $this->mapItems($order);
        $this->mapCoupon($order, $data['coupon']);

        return $order;
    }

    /**
     * @param array $data
     *
     * @return OrderEntity[]
     */
    public function toOrders(array $data): array
    {
        $orders = [];

        foreach ($data as $orderData) {
            $orders[] = $this->toOrder($orderData);
        }

        return $orders;
    }

    /**
     * @param array $data
     *
     * @return PaginatedResult
     */
    public function toPaginatedOrders(array $data): PaginatedResult
    {
        $orders = [];

        foreach ($data['result'] as $orderData) {
            $orders[] = $this->toOrder($orderData);
        }

        $paginatedResult = new PaginatedResult();
        $paginatedResult->setCurrent($data['pagination']['current']);
        $paginatedResult->setSize($data['pagination']['size']);
        $paginatedResult->setPages($data['pagination']['total']);
        $paginatedResult->setItemCount($data['pagination']['itemCount']);
        $paginatedResult->setResult($orders);

        return $paginatedResult;
    }

    /**
     * @param array $data
     *
     * @return Shipment[]
     */
    public function toOrderShipments(array $data): array
    {
        $shipments = [];

        foreach ($data as $shipmentData) {
            if ($shipmentData['trackingCode'] == Shipment::UNSENT_ITEMS) {
                continue;
            }

            $shipment = new Shipment($shipmentData['trackingCode']);
            $shipment->setTrackingUrl($shipmentData['trackingUrl'] ?? '');
            $shipment->setCarrier($shipmentData['carrier']);

            if (isset($shipmentData['updatedAt'])) {
                $shipment->setUpdatedAt(new DateTime($shipmentData['updatedAt']));
            }

            $items = [];

            foreach ($shipmentData['skus'] as $skuData) {
                try {
                    $product = $this->productService->getBySku($skuData['sku']);
                } catch (ProductNotFoundException $error) {
                    $product = new Product();
                    $product->setName($skuData['name']);
                }

                $items[] = [
                    'product' => $product,
                    'quantity' => $skuData['quantity'],
                ];
            }

            $shipment->setItems($items);

            $trackingEvents = [];

            foreach ($shipmentData['events'] as $eventData) {
                $trackingEvents[] = new TrackingEvent(
                    $eventData['code'],
                    $eventData['description'],
                    new DateTime($eventData['createdAt'])
                );

                $shipment->setTrackingEvents($trackingEvents);
            }

            $shipments[] = $shipment;
        }

        return $shipments;
    }

    private function mapCustomer(OrderEntity $order, array $customerData)
    {
        $customer = new Name($customerData['firstName'], $customerData['lastName']);

        $order->setCustomerName($customer);
    }

    private function mapWallet(OrderEntity $order, array $walletData)
    {
        $wallet = new Wallet();
        $wallet->setTotalPointsUsed(Money::fromCents($walletData['pointsUsed']));
        $wallet->setConversionRate($walletData['conversionRate']);

        $order->setWallet($wallet);
    }

    private function mapPayment(OrderEntity $order, array $paymentData)
    {
        $payment = new Payment();
        $payment->setPaymentMethod(PaymentMethodFactory::fromName($paymentData['paymentMethod']['name']));

        if (isset($paymentData['paymentData'])) {
            $payment->setAdditionalInformation($paymentData['paymentData']);
        }

        if ($paymentData['paymentData']['creditCardBrand'] != null) {
            $payment->setCreditCardBrand($paymentData['paymentData']['creditCardBrand']);
        }

        if ($paymentData['paymentData']['creditCardMaskedNumber'] != null) {
            $payment->setCreditCardMaskedNumber($paymentData['paymentData']['creditCardMaskedNumber']);
        }

        if ($paymentData['paymentData']['barcode'] != null) {
            $payment->setBarcode($paymentData['paymentData']['barcode']);
        }

        if ($paymentData['paymentData']['paymentUrl'] != null) {
            $payment->setPaymentUrl($paymentData['paymentData']['paymentUrl']);
        }

        $installmentsAmount = Money::fromCents($paymentData['installments']['amount']);
        $installmentsTotalInterest = Money::fromCents($paymentData['installments']['totalInterest']);
        $installments = new Installments($paymentData['installments']['number'], $installmentsAmount, $installmentsTotalInterest);

        $payment->setInstallments($installments);

        $order->setPayment($payment);
    }

    private function mapShipping(OrderEntity $order, array $shippingData)
    {
        $address = new ShippingAddress();

        if (isset($shippingData['pickupStore'])) {
            $addressData = $shippingData['pickupStore'];
        } else {
            $addressData = $shippingData['address'];
        }

        if (isset($addressData['id'])) {
            $address->setId($addressData['id']);
        }

        if (isset($addressData['title'])) {
            $address->setTitle($addressData['title']);
        }

        if (isset($addressData['type'])) {
            $address->setType($addressData['type']);
        }

        if (isset($addressData['firstName'])) {
            $address->setFirstName($addressData['firstName']);
        }

        if (isset($addressData['lastName'])) {
            $address->setLastName($addressData['lastName']);
        }

        if (isset($addressData['prefix'])) {
            $address->setPrefix($addressData['prefix']);
        }

        if (isset($addressData['line1'])) {
            $address->setLine1($addressData['line1']);
        }

        if (isset($addressData['line2'])) {
            $address->setLine2($addressData['line2']);
        }

        if (isset($addressData['betweenStreet1'])) {
            $address->setBetweenStreet1($addressData['betweenStreet1']);
        }

        if (isset($addressData['betweenStreet2'])) {
            $address->setBetweenStreet2($addressData['betweenStreet2']);
        }

        if (isset($addressData['streetNumber'])) {
            $address->setStreetNumber($addressData['streetNumber']);
        }

        if (isset($addressData['apartment'])) {
            $address->setApartment($addressData['apartment']);
        }

        if (isset($addressData['lot'])) {
            $address->setLot($addressData['lot']);
        }

        if (isset($addressData['neighborhood'])) {
            $address->setNeighborhood($addressData['neighborhood']);
        }

        if (isset($addressData['department'])) {
            $address->setDepartment($addressData['department']);
        }

        if (isset($addressData['municipality'])) {
            $address->setMunicipality($addressData['municipality']);
        }

        if (isset($addressData['urbanization'])) {
            $address->setUrbanization($addressData['urbanization']);
        }

        if (isset($addressData['city'])) {
            $address->setCity($addressData['city']);
        }

        if (isset($addressData['cityId'])) {
            $address->setCityId($addressData['cityId']);
        }

        if (isset($addressData['cityName'])) {
            $address->setCityName($addressData['cityName']);
        }

        if (isset($addressData['region'])) {
            $address->setRegion($addressData['region']);
        }

        if (isset($addressData['regionId'])) {
            $address->setRegionId($addressData['regionId']);
        }

        if (isset($addressData['regionCode'])) {
            $address->setRegionCode($addressData['regionCode']);
        }

        if (isset($addressData['regionName'])) {
            $address->setRegionName($addressData['regionName']);
        }

        if (isset($addressData['postcode'])) {
            $address->setPostcode($addressData['postcode']);
        }

        if (isset($addressData['additionalInformation'])) {
            $address->setAdditionalInformation($addressData['additionalInformation']);
        }

        if (isset($addressData['phone'])) {
            $address->setPhone($addressData['phone']);
        }

        if (isset($addressData['mobilePhone'])) {
            $address->setMobilePhone($addressData['mobilePhone']);
        }

        if (isset($addressData['countryId'])) {
            $address->setCountryId($addressData['countryId']);
        }

        if (isset($addressData['countryCode'])) {
            $address->setCountryCode($addressData['countryCode']);
        }

        if (isset($addressData['countryName'])) {
            $address->setCountryName($addressData['countryName']);
        }

        if (isset($addressData['taxIdentificationNumber'])) {
            $address->setTaxIdentificationNumber($addressData['taxIdentificationNumber']);
        }

        if (isset($addressData['nationalRegistrationNumber'])) {
            $address->setNationalRegistrationNumber($addressData['nationalRegistrationNumber']);
        }

        if (isset($addressData['isDefaultBillingAddress'])) {
            $address->setDefaultBilling($addressData['isDefaultBillingAddress']);
        }

        if (isset($addressData['isDefaultShippingAddress'])) {
            $address->setDefaultShipping($addressData['isDefaultShippingAddress']);
        }

        if (isset($addressData['maternalName'])) {
            $address->setMaternalName($addressData['maternalName']);
        }

        if (isset($addressData['invoiceType'])) {
            $address->setInvoiceType($addressData['invoiceType']);
        }

        if (isset($shippingData['pickupStore'])) {
            $pickupStore = new PickupStore($addressData['id']);
            $pickupStore->setName($addressData['name']);
            $pickupStore->setDescription($addressData['description']);
            $pickupStore->setItemCapacity($addressData['itemCapacity']);
            $pickupStore->setWeightCapacity($addressData['weightCapacity']);

            if (!empty($addressData['geoHash'])) {
                $pickupStore->setGeohash($addressData['geoHash']);
            }

            $address->setPickupStore($pickupStore);
        }

        $shipping = new Shipping();
        $shipping->setAmount(Money::fromCents($shippingData['amount']));
        $shipping->setDiscount(Money::fromCents($shippingData['discount']));
        $shipping->setAddress($address);

        $order->setShipping($shipping);
    }

    private function mapPackages(OrderEntity $order, array $packagesData)
    {
        foreach ($packagesData as $packageData) {
            $package = new Package($packageData['packageNumber']);
            $package->setAllowReturn($packageData['allowReturn'] == 1);
            $package->setAllowTracking($packageData['allowTracking'] == 1);
            $package->setAllowSellerReview($packageData['allowSellerReview'] == 1);
            $package->setStatus($packageData['status']);

            if ($packageData['shipmentStatus'] != null) {
                $package->setShipmentStatus($packageData['shipmentStatus']);
            }

            if ($packageData['progressStep'] != null) {
                $package->setProgressStep($packageData['progressStep']);
            }

            if ($packageData['expectedDeliveryDate'] != null) {
                $package->setExpectedDeliveryDate(new Date($packageData['expectedDeliveryDate']));
            }

            if ($packageData['updatedDeliveryDate'] != null) {
                $package->setUpdatedDeliveryDate(new Date($packageData['updatedDeliveryDate']));
            }

            if ($packageData['deliveredAt'] != null) {
                $package->setDeliveredAt(new Date($packageData['deliveredAt']));
            }

            foreach ($packageData['items'] as $itemData) {
                $item = $this->mapItem($itemData);

                $package->addItem($item);
            }

            $order->addPackage($package);
        }
    }

    /**
     * @param OrderEntity $order
     */
    private function mapItems(OrderEntity $order)
    {
        foreach ($order->getPackages() as $package) {
            foreach ($package->getItems() as $itemStatus) {
                foreach ($itemStatus as $item) {
                    $order->addItem($item);
                }
            }
        }
    }

    /**
     * @param array $itemData
     *
     * @return Item
     */
    private function mapItem(array $itemData): Item
    {
        $item = new Item($itemData['sku']);
        $item->setName($itemData['name']);
        $item->setQuantity($itemData['quantity']);
        $item->setUnitPrice(Money::fromCents($itemData['unitPrice']));
        $item->setPaidPrice(Money::fromCents($itemData['paidPrice']));
        $item->setLinioPlus($itemData['isLinioPlus']);
        $item->setImported($itemData['isImported']);
        $item->setVirtual($itemData['isVirtual']);
        $item->setOverweight($itemData['isOverweight']);
        $item->setAllowReturn($itemData['isReturnAllowed']);
        $item->setStatus($itemData['status'] ?? 'unknown');

        if ($itemData['sellerType'] == 'merchant') {
            if (!empty($itemData['seller'])) {
                $item->setSeller($itemData['seller']);
            } else {
                $item->setSeller('product.default_seller_name');
            }
        } else {
            $item->setSeller('Linio');
        }

        if ($itemData['statusProgressStep'] != null) {
            $item->setStatusProgressStep($itemData['statusProgressStep']);
        }

        $item->setStatusUpdatedAt(new DateTime($itemData['statusUpdatedAt']));

        if ($itemData['expectedDeliveryDate'] != null) {
            $item->setExpectedDeliveryDate(new Date($itemData['expectedDeliveryDate']));
        }

        if ($itemData['updatedDeliveryDate'] != null) {
            $item->setUpdatedDeliveryDate(new Date($itemData['updatedDeliveryDate']));
        }

        if ($itemData['deliveredAt'] != null) {
            $item->setDeliveredAt(new Date($itemData['deliveredAt']));
        }

        try {
            $product = $this->productService->getBySku($itemData['sku']);

            $item->setSlug($product->getSlug());

            if ($product->hasVariation()) {
                $simple = $product->getSimple($itemData['sku']);

                if ($simple) {
                    $variation = $simple->getAttribute('variation');

                    $item->setVariation($variation);
                    $item->setVariationType($product->getVariationType());
                }
            }
        } catch (ProductNotFoundException $error) {
        }

        if (isset($itemData['imageSlug'])) {
            $image = new Image();
            $image->setMain(true);
            $image->setPosition(1);
            $image->setSlug(sprintf('//%s/%s', $this->imageHost, $itemData['imageSlug']));

            $item->setImage($image);
        }

        return $item;
    }

    /**
     * @param OrderEntity $order
     * @param array $couponData
     */
    private function mapCoupon(OrderEntity $order, array $couponData)
    {
        if (!isset($couponData['code'])) {
            return;
        }

        $coupon = new Coupon($couponData['code'], $couponData['type']);
        $coupon->setAmount(Money::fromCents($couponData['amount']));
        $coupon->setPercent($couponData['percent'] / 100);

        $order->setCoupon($coupon);
    }
}
