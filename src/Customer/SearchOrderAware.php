<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

trait SearchOrderAware
{
    /**
     * @var SearchOrder
     */
    protected $searchOrder;

    /**
     * @param SearchOrder $searchOrder
     */
    public function setSearchOrder(SearchOrder $searchOrder)
    {
        $this->searchOrder = $searchOrder;
    }
}
