<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

use Linio\Frontend\Customer\Communication\AddressBook\AddressBookAdapter;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Location\Exception\AddressException;

class AddressBook
{
    /**
     * @var AddressBookAdapter
     */
    protected $adapter;

    /**
     * @param AddressBookAdapter $adapter
     */
    public function setAdapter(AddressBookAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException
     * @throws AddressException
     *
     * @return Address[]
     */
    public function getAddresses(Customer $customer): array
    {
        return $this->adapter->getAddresses($customer);
    }

    /**
     * @param Customer $customer
     * @param int $addressId
     *
     * @throws InputException
     * @throws AddressException
     *
     * @return Address
     */
    public function getAddress(Customer $customer, int $addressId): Address
    {
        return $this->adapter->getAddress($customer, $addressId);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException
     * @throws AddressException
     */
    public function createAddress(Customer $customer, Address $address)
    {
        $this->adapter->createAddress($customer, $address);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException
     * @throws AddressException
     */
    public function updateAddress(Customer $customer, Address $address)
    {
        $this->adapter->updateAddress($customer, $address);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException
     * @throws AddressException
     */
    public function removeAddress(Customer $customer, Address $address)
    {
        $this->adapter->removeAddress($customer, $address);
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException
     * @throws AddressException
     */
    public function setDefaultShipping(Customer $customer, Address $address)
    {
        $this->adapter->setDefaultShipping($customer, $address);
        $address->setDefaultShipping();
    }

    /**
     * @param Customer $customer
     * @param Address $address
     *
     * @throws InputException
     * @throws AddressException
     */
    public function setDefaultBilling(Customer $customer, Address $address)
    {
        $this->adapter->setDefaultBilling($customer, $address);
        $address->setDefaultBilling();
    }
}
