<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use InvalidArgumentException;
use Linio\Type\Money;

class Coupon
{
    const TYPE_FIXED = 'fixed';
    const TYPE_PERCENT = 'percent';

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var Money
     */
    protected $amount;

    /**
     * @var float
     */
    protected $percent = 0;

    /**
     * @param string $code
     * @param string $type
     *
     * @throws InvalidArgumentException
     */
    public function __construct(string $code, string $type)
    {
        $this->code = $code;
        $this->setType($type);
        $this->amount = new Money();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     */
    public function setAmount(Money $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getPercent(): float
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     */
    public function setPercent(float $percent)
    {
        $this->percent = $percent;
    }

    /**
     * @param string $type
     *
     * @throws InvalidArgumentException
     */
    private function setType(string $type)
    {
        if ($type != self::TYPE_FIXED && $type != self::TYPE_PERCENT) {
            throw new InvalidArgumentException('Coupon type is not valid');
        }

        $this->type = $type;
    }
}
