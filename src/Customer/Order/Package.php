<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Type\Date;

class Package
{
    /**
     * @var int
     */
    protected $number;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $shipmentStatus;

    /**
     * @var int
     */
    protected $progressStep;

    /**
     * @var Date
     */
    protected $expectedDeliveryDate;

    /**
     * @var Date
     */
    protected $updatedDeliveryDate;

    /**
     * @var Date
     */
    protected $deliveredAt;

    /**
     * @var bool
     */
    protected $allowReturn;

    /**
     * @var bool
     */
    protected $allowTracking;

    /**
     * @var bool
     */
    protected $allowSellerReview;

    /**
     * @var Item[]
     */
    protected $items = [];

    /**
     * @param int $number
     */
    public function __construct(int $number)
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getShipmentStatus()
    {
        return $this->shipmentStatus;
    }

    /**
     * @param string $shipmentStatus
     */
    public function setShipmentStatus($shipmentStatus)
    {
        $this->shipmentStatus = $shipmentStatus;
    }

    /**
     * @return int
     */
    public function getProgressStep()
    {
        return $this->progressStep;
    }

    /**
     * @param int $progressStep
     */
    public function setProgressStep(int $progressStep)
    {
        $this->progressStep = $progressStep;
    }

    /**
     * @return Date
     */
    public function getExpectedDeliveryDate()
    {
        return $this->expectedDeliveryDate;
    }

    /**
     * @param Date $expectedDeliveryDate
     */
    public function setExpectedDeliveryDate(Date $expectedDeliveryDate)
    {
        $this->expectedDeliveryDate = $expectedDeliveryDate;
    }

    /**
     * @return Date
     */
    public function getUpdatedDeliveryDate()
    {
        return $this->updatedDeliveryDate;
    }

    /**
     * @param Date $updatedDeliveryDate
     */
    public function setUpdatedDeliveryDate($updatedDeliveryDate)
    {
        $this->updatedDeliveryDate = $updatedDeliveryDate;
    }

    /**
     * @return Date
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    /**
     * @param Date $deliveredAt
     */
    public function setDeliveredAt(Date $deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;
    }

    /**
     * @return bool
     */
    public function isReturnAllowed(): bool
    {
        return $this->allowReturn;
    }

    /**
     * @param bool $allowReturn
     */
    public function setAllowReturn(bool $allowReturn)
    {
        $this->allowReturn = $allowReturn;
    }

    /**
     * @return bool
     */
    public function isTrackingAllowed(): bool
    {
        return $this->allowTracking;
    }

    /**
     * @param bool $allowTracking
     */
    public function setAllowTracking(bool $allowTracking)
    {
        $this->allowTracking = $allowTracking;
    }

    /**
     * @return bool
     */
    public function isSellerReviewAllowed(): bool
    {
        return $this->allowSellerReview;
    }

    /**
     * @param bool $allowSellerReview
     */
    public function setAllowSellerReview(bool $allowSellerReview)
    {
        $this->allowSellerReview = $allowSellerReview;
    }

    /**
     * An array of items grouped by status. $this->items['delivered'] = [Item1, Item2].
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        if (!isset($this->items[$item->getStatus()])) {
            $this->items[$item->getStatus()] = [];
        }

        $this->items[$item->getStatus()][] = $item;
    }
}
