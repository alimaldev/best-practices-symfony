<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Carbon\Carbon;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Linio\Frontend\Customer\Order\Package\Item;
use Symfony\Component\Translation\TranslatorInterface;

class MakeDeliveryStatusFriendly
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var DateTimeZone
     */
    protected $timezone;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone(string $timezone)
    {
        $this->timezone = new DateTimeZone($timezone);
    }

    /**
     * @param Package $package
     *
     * @return string
     */
    public function getPackageFriendlyStatus(Package $package): string
    {
        return $this->getFriendlyMessageFromStatus(
            $package->getStatus(),
            $package->getExpectedDeliveryDate(),
            $package->getUpdatedDeliveryDate(),
            $package->getDeliveredAt()
        );
    }

    /**
     * @param Item $item
     *
     * @return string
     */
    public function getItemFriendlyStatus(Item $item): string
    {
        return $this->getFriendlyMessageFromStatus(
            $item->getStatus(),
            $item->getExpectedDeliveryDate(),
            $item->getUpdatedDeliveryDate(),
            $item->getDeliveredAt()
        );
    }

    /**
     * @param DateTime|null $deliveredAt
     *
     * @return string
     */
    protected function getDeliveredFriendlyDate(DateTime $deliveredAt = null): string
    {
        if (!$deliveredAt instanceof DateTimeInterface) {
            return $this->translator->trans('customer.order.delivered');
        }

        $deliveredAt = new Carbon($deliveredAt->format('Y-m-d'), $this->timezone);
        $threeDaysBehind = Carbon::now($this->timezone)->subDays(3);

        switch (true) {
            case $deliveredAt->isYesterday():
                return $this->translator->trans('customer.order.delivered_yesterday');
                break;
            case $deliveredAt->isToday():
                return $this->translator->trans('customer.order.delivered_today');
                break;
            case $deliveredAt->gt($threeDaysBehind) && $deliveredAt->isPast():
                return $this->translator->trans(
                    'customer.order.delivered_on_weekday',
                    [
                        '%weekday%' => $this->translator->trans(
                            'global.weekdays.' . strtolower($deliveredAt->format('l'))
                        ),
                    ]
                );
                break;
            default:
                return $this->translator->trans(
                    'customer.order.delivered_on_date',
                    [
                        '%day%' => $deliveredAt->format('j'),
                        '%month%' => $this->translator->trans(
                            'global.months.' . strtolower($deliveredAt->format('F'))
                        ),
                    ]
                );
                break;
        }
    }

    /**
     * @param DateTime $originalDeliveryDate
     * @param DateTime|null $updatedDeliveryDate
     *
     * @return string
     */
    protected function getDeliveringFriendlyDate(DateTime $originalDeliveryDate, DateTime $updatedDeliveryDate = null): string
    {
        $deliveryDate = $updatedDeliveryDate ?? $originalDeliveryDate;
        $expectedDeliveryDate = new Carbon($deliveryDate->format('Y-m-d'), $this->timezone);
        $threeDaysAhead = Carbon::now($this->timezone)->addDays(3);

        switch (true) {
            case $expectedDeliveryDate->isToday():
                return $this->translator->trans('customer.order.delivering_today');
                break;
            case $expectedDeliveryDate->isTomorrow():
                return $this->translator->trans('customer.order.delivering_tomorrow');
                break;
            case $expectedDeliveryDate->lte($threeDaysAhead) && $expectedDeliveryDate->isFuture():
                return $this->translator->trans(
                    'customer.order.delivering_on_weekday',
                    [
                        '%weekday%' => $this->translator->trans(
                            'global.weekdays.' . strtolower($expectedDeliveryDate->format('l'))
                        ),
                    ]
                );
                break;
        }

        return $this->translator->trans(
            'customer.order.delivering_on_date',
            [
                '%day%' => $expectedDeliveryDate->format('j'),
                '%month%' => $this->translator->trans(
                    'global.months.' . strtolower($expectedDeliveryDate->format('F'))
                ),
            ]
        );
    }

    /**
     * @param string $status
     * @param DateTime $expectedDeliveryDate
     * @param DateTime|null $updatedDeliveryDate
     * @param DateTime|null $deliveredAt
     *
     * @return string
     */
    protected function getFriendlyMessageFromStatus(
        string $status,
        DateTime $expectedDeliveryDate = null,
        DateTime $updatedDeliveryDate = null,
        DateTime $deliveredAt = null
    ) {
        switch ($status) {
            case 'closed':
                return $this->translator->trans('customer.order.closed');
                break;
            case 'canceled':
                return $this->translator->trans('customer.order.canceled');
                break;
            case 'refundNeeded':
                return $this->translator->trans('customer.order.refund_needed');
                break;
            case 'refunded':
                return $this->translator->trans('customer.order.refunded');
                break;
            case 'pendingConfirmation':
                return $this->translator->trans('customer.order.pending_confirmation');
                break;
            case 'confirmed':
                return $this->translator->trans('customer.order.confirmed');
                break;
            case 'invalid':
                return $this->translator->trans('customer.order.invalid');
                break;
            case 'returnRequested':
                return $this->translator->trans('customer.order.return_requested');
                break;
            case 'delivered':
                return $this->getDeliveredFriendlyDate($deliveredAt);
                break;
        }

        if ($status == 'shipped' && $expectedDeliveryDate instanceof DateTime) {
            return $this->getDeliveringFriendlyDate($expectedDeliveryDate, $updatedDeliveryDate);
        }

        return $this->translator->trans('customer.order.shipping_soon');
    }
}
