<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Linio\Frontend\Customer\Order\Payment\Installments;
use Linio\Frontend\Order\Payment\PaymentMethod;

class Payment
{
    /**
     * @var PaymentMethod
     */
    protected $paymentMethod;

    /**
     * @var string
     */
    protected $creditCardMaskedNumber;

    /**
     * @var string
     */
    protected $creditCardBrand;

    /**
     * @var string
     */
    protected $barcode;

    /**
     * @var string
     */
    protected $paymentUrl;

    /**
     * @var Installments
     */
    protected $installments;

    /**
     * @var array
     */
    protected $additionalInformation = [];

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @param PaymentMethod $paymentMethod
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getCreditCardMaskedNumber()
    {
        return $this->creditCardMaskedNumber;
    }

    /**
     * @param string $creditCardMaskedNumber
     */
    public function setCreditCardMaskedNumber($creditCardMaskedNumber)
    {
        $this->creditCardMaskedNumber = $creditCardMaskedNumber;
    }

    /**
     * @return string
     */
    public function getCreditCardBrand()
    {
        return $this->creditCardBrand;
    }

    /**
     * @param string $creditCardBrand
     */
    public function setCreditCardBrand($creditCardBrand)
    {
        $this->creditCardBrand = $creditCardBrand;
    }

    /**
     * @return string
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getPaymentUrl()
    {
        return $this->paymentUrl;
    }

    /**
     * @param string $paymentUrl
     */
    public function setPaymentUrl($paymentUrl)
    {
        $this->paymentUrl = $paymentUrl;
    }

    /**
     * @return Installments
     */
    public function getInstallments(): Installments
    {
        return $this->installments;
    }

    /**
     * @param Installments $installments
     */
    public function setInstallments(Installments $installments)
    {
        $this->installments = $installments;
    }

    /**
     * @return array
     */
    public function getAdditionalInformation(): array
    {
        return $this->additionalInformation;
    }

    /**
     * @param array $additionalInformation
     */
    public function setAdditionalInformation(array $additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }
}
