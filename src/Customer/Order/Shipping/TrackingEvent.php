<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Shipping;

use DateTime;

class TrackingEvent
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @param string $code
     * @param string $description
     * @param DateTime $createdAt
     */
    public function __construct(string $code, string $description, DateTime $createdAt)
    {
        $this->code = $code;
        $this->description = $description;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}
