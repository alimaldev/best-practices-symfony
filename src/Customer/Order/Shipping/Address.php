<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Shipping;

use Linio\Frontend\Location\Address as CustomerAddress;

class Address extends CustomerAddress
{
    /**
     * @var string
     */
    protected $alternativeRecipientName;

    /**
     * @var string
     */
    protected $alternativeRecipientId;

    /**
     * @var string
     */
    protected $directionPrefix;

    /**
     * @var PickupStore
     */
    protected $pickupStore;

    /**
     * @var string
     */
    protected $nationalRegistrationNumber;

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->countryName;
    }

    /**
     * @param string $countryName
     */
    public function setCountry(string $countryName)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return string|null
     */
    public function getAlternativeRecipientName()
    {
        return $this->alternativeRecipientName;
    }

    /**
     * @param string $alternativeRecipientName
     */
    public function setAlternativeRecipientName(string $alternativeRecipientName)
    {
        $this->alternativeRecipientName = $alternativeRecipientName;
    }

    /**
     * @return string|null
     */
    public function getAlternativeRecipientId()
    {
        return $this->alternativeRecipientId;
    }

    /**
     * @param string $alternativeRecipientId
     */
    public function setAlternativeRecipientId(string $alternativeRecipientId)
    {
        $this->alternativeRecipientId = $alternativeRecipientId;
    }

    /**
     * @return string|null
     */
    public function getDirectionPrefix()
    {
        return $this->directionPrefix;
    }

    /**
     * @param string $directionPrefix
     */
    public function setDirectionPrefix(string $directionPrefix)
    {
        $this->directionPrefix = $directionPrefix;
    }

    /**
     * @return bool
     */
    public function isStorePickUp(): bool
    {
        return (bool) $this->pickupStore;
    }

    /**
     * @return PickupStore|null
     */
    public function getPickupStore()
    {
        return $this->pickupStore;
    }

    /**
     * @param PickupStore $pickupStore
     */
    public function setPickupStore(PickupStore $pickupStore)
    {
        $this->pickupStore = $pickupStore;
    }

    /**
     * @return string|null
     */
    public function getNationalRegistrationNumber()
    {
        return $this->nationalRegistrationNumber;
    }

    /**
     * @param string $nationalRegistrationNumber
     */
    public function setNationalRegistrationNumber(string $nationalRegistrationNumber)
    {
        $this->nationalRegistrationNumber = $nationalRegistrationNumber;
    }
}
