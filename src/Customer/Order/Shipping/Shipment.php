<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Shipping;

use DateTime;

class Shipment
{
    const UNSENT_ITEMS = 'NULL_TRACKING_CODE';

    /**
     * @var string
     */
    protected $trackingCode;

    /**
     * @var string
     */
    protected $trackingUrl;

    /**
     * @var string
     */
    protected $carrier;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var TrackingEvent[]
     */
    protected $trackingEvents = [];

    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @param string $trackingCode
     */
    public function __construct(string $trackingCode)
    {
        $this->trackingCode = $trackingCode;
        $this->updatedAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getTrackingCode(): string
    {
        return $this->trackingCode;
    }

    /**
     * @param string $trackingCode
     */
    public function setTrackingCode(string $trackingCode)
    {
        $this->trackingCode = $trackingCode;
    }

    /**
     * @return string
     */
    public function getTrackingUrl()
    {
        return $this->trackingUrl;
    }

    /**
     * @param string $trackingUrl
     */
    public function setTrackingUrl(string $trackingUrl)
    {
        $this->trackingUrl = $trackingUrl;
    }

    /**
     * @return string
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param string $carrier
     */
    public function setCarrier(string $carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return TrackingEvent[]
     */
    public function getTrackingEvents(): array
    {
        return $this->trackingEvents;
    }

    /**
     * @param TrackingEvent[] $trackingEvents
     */
    public function setTrackingEvents(array $trackingEvents)
    {
        $this->trackingEvents = $trackingEvents;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
