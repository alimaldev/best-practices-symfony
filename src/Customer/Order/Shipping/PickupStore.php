<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Shipping;

class PickupStore
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $geohash;

    /**
     * @var int
     */
    protected $itemCapacity;

    /**
     * @var int
     */
    protected $weightCapacity;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getGeohash()
    {
        return $this->geohash;
    }

    /**
     * @param string $geohash
     */
    public function setGeohash(string $geohash)
    {
        $this->geohash = $geohash;
    }

    /**
     * @return int
     */
    public function getItemCapacity()
    {
        return $this->itemCapacity;
    }

    /**
     * @param int $itemCapacity
     */
    public function setItemCapacity(int $itemCapacity)
    {
        $this->itemCapacity = $itemCapacity;
    }

    /**
     * @return int
     */
    public function getWeightCapacity()
    {
        return $this->weightCapacity;
    }

    /**
     * @param int $weightCapacity
     */
    public function setWeightCapacity(int $weightCapacity)
    {
        $this->weightCapacity = $weightCapacity;
    }
}
