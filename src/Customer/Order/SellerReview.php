<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Linio\Frontend\Entity\Seller\Seller;

class SellerReview
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $detail;

    /**
     * @var int
     */
    protected $rating;

    /**
     * @var Seller
     */
    protected $seller;

    /**
     * @param string $title
     * @param string $detail
     * @param int $rating
     */
    public function __construct(string $title = '', string $detail = '', int $rating = 5)
    {
        $this->title = $title;
        $this->detail = $detail;
        $this->rating = $rating;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDetail(): string
    {
        return $this->detail;
    }

    /**
     * @param string $detail
     */
    public function setDetail(string $detail)
    {
        $this->detail = $detail;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating(int $rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return Seller
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param Seller $seller
     */
    public function setSeller(Seller $seller)
    {
        $this->seller = $seller;
    }
}
