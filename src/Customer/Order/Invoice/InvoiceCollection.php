<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Invoice;

use Linio\Collection\TypedCollection;

class InvoiceCollection extends TypedCollection
{
    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof Invoice;
    }
}
