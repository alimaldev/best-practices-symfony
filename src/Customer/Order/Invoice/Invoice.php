<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Invoice;

class Invoice
{
    /**
     * @var string
     */
    protected $folio;

    /**
     * @var string
     */
    protected $xml;

    /**
     * @var string
     */
    protected $pdf;

    /**
     * @param string $folio
     * @param string $xml
     * @param string $pdf
     */
    public function __construct(string $folio, string $xml, string $pdf)
    {
        $this->folio = $folio;
        $this->xml = $xml;
        $this->pdf = $pdf;
    }

    /**
     * @return string
     */
    public function getFolio(): string
    {
        return $this->folio;
    }

    /**
     * @param string $folio
     */
    public function setFolio(string $folio)
    {
        $this->folio = $folio;
    }

    /**
     * @return string
     */
    public function getXml(): string
    {
        return $this->xml;
    }

    /**
     * @param string $xml
     */
    public function setXml(string $xml)
    {
        $this->xml = $xml;
    }

    /**
     * @return string
     */
    public function getPdf(): string
    {
        return $this->pdf;
    }

    /**
     * @param string $pdf
     */
    public function setPdf(string $pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @param string $format
     *
     * @return string
     */
    public function getDocument(string $format = 'pdf'): string
    {
        if ($format == 'pdf') {
            return $this->getPdf();
        }

        return $this->getXml();
    }
}
