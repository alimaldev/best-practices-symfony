<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Invoice;

use JsonSerializable;

class InvoiceResult implements JsonSerializable
{
    /**
     * @var InvoiceCollection
     */
    protected $invoices;

    public function __construct()
    {
        $this->invoices = new InvoiceCollection();
    }

    /**
     * @return InvoiceCollection
     */
    public function getInvoices(): InvoiceCollection
    {
        return $this->invoices;
    }

    /**
     * @param InvoiceCollection $invoices
     */
    public function setInvoices(InvoiceCollection $invoices)
    {
        $this->invoices = $invoices;
    }

    /**
     * @param Invoice $invoice
     */
    public function addInvoice(Invoice $invoice)
    {
        $this->invoices->offsetSet($invoice->getFolio(), $invoice);
    }

    /**
     * @param string $folio
     *
     * @return Invoice|null
     */
    public function getInvoice($folio)
    {
        return $this->invoices->offsetGet($folio);
    }

    /**
     * return string[].
     */
    public function getFolios(): array
    {
        return $this->invoices->getKeys();
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'invoices' => $this->invoices->getValues(),
        ];
    }
}
