<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Linio\Frontend\Customer\Order\Shipping\Address;
use Linio\Type\Money;

class Shipping
{
    /**
     * @var Money
     */
    protected $amount;

    /**
     * @var Money
     */
    protected $discount;

    /**
     * @var Address
     */
    protected $address;

    public function __construct()
    {
        $this->amount = new Money();
        $this->discount = new Money();
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     */
    public function setAmount(Money $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return Money
     */
    public function getDiscount(): Money
    {
        return $this->discount;
    }

    /**
     * @param Money $discount
     */
    public function setDiscount(Money $discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }
}
