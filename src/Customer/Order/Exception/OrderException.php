<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Exception;

use Linio\Frontend\Exception\DomainException;

class OrderException extends DomainException
{
}
