<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Payment;

use Linio\Type\Money;

class Installments
{
    /**
     * @var int
     */
    protected $number;

    /**
     * @var Money
     */
    protected $amount;

    /**
     * @var Money
     */
    protected $totalInterest;

    /**
     * @param int $number
     * @param Money $amount
     * @param Money $totalInterest
     */
    public function __construct(int $number, Money $amount, Money $totalInterest)
    {
        $this->number = $number;
        $this->amount = $amount;
        $this->totalInterest = $totalInterest;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @return Money
     */
    public function getTotalInterest(): Money
    {
        return $this->totalInterest;
    }

    /**
     * @return bool
     */
    public function hasInterest(): bool
    {
        return $this->totalInterest->getMoneyAmount() > 0;
    }
}
