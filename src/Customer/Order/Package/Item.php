<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order\Package;

use DateTime;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Type\Date;
use Linio\Type\Money;

class Item
{
    /**
     * @var string
     */
    protected $sku;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $variation;

    /**
     * @var string
     */
    protected $variationType;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var Money
     */
    protected $unitPrice;

    /**
     * @var Money
     */
    protected $paidPrice;

    /**
     * @var string
     */
    protected $seller;

    /**
     * @var bool
     */
    protected $linioPlus;

    /**
     * @var bool
     */
    protected $imported;

    /**
     * @var bool
     */
    protected $virtual;

    /**
     * @var bool
     */
    protected $overweight;

    /**
     * @var bool
     */
    protected $allowReturn;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var int
     */
    protected $statusProgressStep;

    /**
     * @var DateTime
     */
    protected $statusUpdatedAt;

    /**
     * @var Date
     */
    protected $expectedDeliveryDate;

    /**
     * @var Date
     */
    protected $updatedDeliveryDate;

    /**
     * @var Date
     */
    protected $deliveredAt;

    /**
     * @var Image|null
     */
    protected $image;

    /**
     * @param string $sku
     */
    public function __construct(string $sku)
    {
        $this->sku = $sku;

        $this->unitPrice = new Money();
        $this->paidPrice = new Money();
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * @param string $variation
     */
    public function setVariation(string $variation)
    {
        $this->variation = $variation;
    }

    /**
     * @return string
     */
    public function getVariationType()
    {
        return $this->variationType;
    }

    /**
     * @param string $variationType
     */
    public function setVariationType(string $variationType)
    {
        $this->variationType = $variationType;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return Money
     */
    public function getUnitPrice(): Money
    {
        return $this->unitPrice;
    }

    /**
     * @param Money $unitPrice
     */
    public function setUnitPrice(Money $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return Money
     */
    public function getPaidPrice(): Money
    {
        return $this->paidPrice;
    }

    /**
     * @param Money $paidPrice
     */
    public function setPaidPrice(Money $paidPrice)
    {
        $this->paidPrice = $paidPrice;
    }

    /**
     * @return string
     */
    public function getSeller(): string
    {
        return $this->seller;
    }

    /**
     * @param string $seller
     */
    public function setSeller(string $seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return bool
     */
    public function isLinioPlus(): bool
    {
        return $this->linioPlus;
    }

    /**
     * @param bool $linioPlus
     */
    public function setLinioPlus(bool $linioPlus)
    {
        $this->linioPlus = $linioPlus;
    }

    /**
     * @return bool
     */
    public function isImported(): bool
    {
        return $this->imported;
    }

    /**
     * @param bool $imported
     */
    public function setImported(bool $imported)
    {
        $this->imported = $imported;
    }

    /**
     * @return bool
     */
    public function isVirtual(): bool
    {
        return $this->virtual;
    }

    /**
     * @param bool $virtual
     */
    public function setVirtual(bool $virtual)
    {
        $this->virtual = $virtual;
    }

    /**
     * @return bool
     */
    public function isOverweight(): bool
    {
        return $this->overweight;
    }

    /**
     * @param bool $overweight
     */
    public function setOverweight(bool $overweight)
    {
        $this->overweight = $overweight;
    }

    /**
     * @return bool
     */
    public function isReturnAllowed(): bool
    {
        return $this->allowReturn;
    }

    /**
     * @param bool $allowReturn
     */
    public function setAllowReturn(bool $allowReturn)
    {
        $this->allowReturn = $allowReturn;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusProgressStep()
    {
        return $this->statusProgressStep;
    }

    /**
     * @param int $statusProgressStep
     */
    public function setStatusProgressStep(int $statusProgressStep)
    {
        $this->statusProgressStep = $statusProgressStep;
    }

    /**
     * @return DateTime
     */
    public function getStatusUpdatedAt(): DateTime
    {
        return $this->statusUpdatedAt;
    }

    /**
     * @param DateTime $statusUpdatedAt
     */
    public function setStatusUpdatedAt(DateTime $statusUpdatedAt)
    {
        $this->statusUpdatedAt = $statusUpdatedAt;
    }

    /**
     * @return Date
     */
    public function getExpectedDeliveryDate()
    {
        return $this->expectedDeliveryDate;
    }

    /**
     * @param Date $expectedDeliveryDate
     */
    public function setExpectedDeliveryDate(Date $expectedDeliveryDate)
    {
        $this->expectedDeliveryDate = $expectedDeliveryDate;
    }

    /**
     * @return Date
     */
    public function getUpdatedDeliveryDate()
    {
        return $this->updatedDeliveryDate;
    }

    /**
     * @param Date $updatedDeliveryDate
     */
    public function setUpdatedDeliveryDate($updatedDeliveryDate)
    {
        $this->updatedDeliveryDate = $updatedDeliveryDate;
    }

    /**
     * @return Date
     */
    public function getDeliveredAt()
    {
        return $this->deliveredAt;
    }

    /**
     * @param Date $deliveredAt
     */
    public function setDeliveredAt(Date $deliveredAt)
    {
        $this->deliveredAt = $deliveredAt;
    }

    /**
     * @return Image|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Image|null $image
     */
    public function setImage(Image $image)
    {
        $this->image = $image;
    }
}
