<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer\Order;

use Linio\Type\Date;

class Search
{
    /**
     * @var int
     */
    protected $pageSize = 10;

    /**
     * @var int
     */
    protected $page = 1;

    /**
     * @var Date
     */
    protected $fromDate;

    /**
     * @var Date
     */
    protected $toDate;

    public function __construct()
    {
        $this->fromDate = new Date('1969-01-01');
        $this->toDate = new Date();
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize)
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page)
    {
        $this->page = $page;
    }

    /**
     * @return Date
     */
    public function getFromDate(): Date
    {
        return $this->fromDate;
    }

    /**
     * @param Date $fromDate
     */
    public function setFromDate(Date $fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return Date
     */
    public function getToDate(): Date
    {
        return $this->toDate;
    }

    /**
     * @param Date $toDate
     */
    public function setToDate(Date $toDate)
    {
        $this->toDate = $toDate;
    }
}
