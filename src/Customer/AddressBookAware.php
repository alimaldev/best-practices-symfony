<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

trait AddressBookAware
{
    /**
     * @var AddressBook
     */
    protected $addressBook;

    /**
     * @return AddressBook
     */
    public function getAddressBook(): AddressBook
    {
        return $this->addressBook;
    }

    /**
     * @param AddressBook $addressBook
     */
    public function setAddressBook(AddressBook $addressBook)
    {
        $this->addressBook = $addressBook;
    }
}
