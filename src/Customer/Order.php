<?php

declare(strict_types=1);

namespace Linio\Frontend\Customer;

use DateTime;
use Linio\Frontend\Customer\Order\Coupon;
use Linio\Frontend\Customer\Order\Package;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Payment;
use Linio\Frontend\Customer\Order\Shipping;
use Linio\Frontend\Order\Wallet;
use Linio\Type\Money;

class Order
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $orderNumber;

    /**
     * @var bool
     */
    protected $fastLane;

    /**
     * @var Money
     */
    protected $subtotal;

    /**
     * @var Money
     */
    protected $grandTotal;

    /**
     * @var Name
     */
    protected $customerName;

    /**
     * @var Payment
     */
    protected $payment;

    /**
     * @var Wallet
     */
    protected $wallet;

    /**
     * @var Coupon|null
     */
    protected $coupon;

    /**
     * @var Shipping
     */
    protected $shipping;

    /**
     * @var Item[]
     */
    protected $items;

    /**
     * @var Package[]
     */
    protected $packages = [];

    /**
     * @var string[]
     */
    protected $statusProgressConfiguration = [];

    /**
     * @var bool
     */
    protected $invoiceAvailable = false;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
        $this->subtotal = new Money();
        $this->grandTotal = new Money();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber(string $orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return bool
     */
    public function isFastLane(): bool
    {
        return $this->fastLane;
    }

    /**
     * @param bool $fastLane
     */
    public function setFastLane(bool $fastLane)
    {
        $this->fastLane = $fastLane;
    }

    /**
     * @return Money
     */
    public function getSubtotal(): Money
    {
        return $this->subtotal;
    }

    /**
     * @param Money $subtotal
     */
    public function setSubtotal(Money $subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return Money
     */
    public function getGrandTotal(): Money
    {
        return $this->grandTotal;
    }

    /**
     * @param Money $grandTotal
     */
    public function setGrandTotal(Money $grandTotal)
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * @return Name
     */
    public function getCustomerName(): Name
    {
        return $this->customerName;
    }

    /**
     * @param Name $customerName
     */
    public function setCustomerName(Name $customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return Coupon|null
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param Coupon $coupon
     */
    public function setCoupon(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return Wallet
     */
    public function getWallet(): Wallet
    {
        return $this->wallet;
    }

    /**
     * @param Wallet $wallet
     */
    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * @return Shipping
     */
    public function getShipping(): Shipping
    {
        return $this->shipping;
    }

    /**
     * @param Shipping $shipping
     */
    public function setShipping(Shipping $shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return string[]
     */
    public function getStatusProgressConfiguration(): array
    {
        return $this->statusProgressConfiguration;
    }

    /**
     * @param string[] $statusProgressConfiguration
     */
    public function setStatusProgressConfiguration(array $statusProgressConfiguration)
    {
        $this->statusProgressConfiguration = $statusProgressConfiguration;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->items[] = $item;
    }

    /**
     * @param Item[] $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return Package[]
     */
    public function getPackages(): array
    {
        return $this->packages;
    }

    /**
     * @param Package $package
     */
    public function addPackage(Package $package)
    {
        $this->packages[] = $package;
    }

    /**
     * @return bool
     */
    public function isInvoiceAvailable(): bool
    {
        return $this->invoiceAvailable;
    }

    /**
     * @param bool $invoiceAvailable
     */
    public function setInvoiceAvailable(bool $invoiceAvailable)
    {
        $this->invoiceAvailable = $invoiceAvailable;
    }

    /**
     * @param Package[] $packages
     */
    public function setPackages(array $packages)
    {
        $this->packages = $packages;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return bool
     */
    public function hasPackagesAvailableForReturn(): bool
    {
        foreach ($this->packages as $package) {
            if ($package->isReturnAllowed()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasPackagesAvailableForReview(): bool
    {
        foreach ($this->packages as $package) {
            if ($package->isSellerReviewAllowed()) {
                return true;
            }
        }

        return false;
    }
}
