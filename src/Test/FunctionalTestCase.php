<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Security\AuthenticationToken;
use Linio\Frontend\Security\User;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class FunctionalTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var callable[]
     */
    protected $expectations = [];

    protected function getClient(): Client
    {
        if (!$this->client) {
            static::ensureKernelShutdown();

            static::$kernel = static::createKernel([]);
            static::$kernel->boot();

            foreach ($this->expectations as $expectation) {
                $expectation(static::$kernel);
            }

            $this->client = static::$kernel->getContainer()->get('test.client');
        }

        return $this->client;
    }

    protected function login()
    {
        $this->loginAs((new CustomerFixture())->getUser());
    }

    protected function loginAs(User $user)
    {
        $this->expectations[] = function (KernelInterface $kernel) use ($user) {
            $tokenStorage = $kernel->getContainer()->get('security.token_storage');
            $tokenStorage->setToken(new AuthenticationToken($user, 'foobar', 'linio_authentication_provider', ['ROLE_USER']));
        };
    }

    protected function expectOrderInCache(Order $order = null)
    {
        $this->expectCommunicationResponse((new RecalculateCartResponseFixture())->getJson());

        $this->expectations[] = function (KernelInterface $kernel) use ($order) {
            $buildOrder = $kernel->getContainer()->get('order.build');

            if (!$order) {
                $order = $buildOrder->getOrderInProgress((new CustomerFixture())->getCustomer());
                $recalculatedOrder = $buildOrder->recalculateOrder($order);
            }

            $buildOrder->saveOrderInProgress($recalculatedOrder->getOrder());
        };
    }

    protected function expectProductInCache(array $productData)
    {
        $this->expectations[] = function (KernelInterface $kernel) use ($productData) {
            /** @var ProductService $productService */
            $productService = $kernel->getContainer()->get('product.service');
            /** @var CacheService $cacheService */
            $cacheService = $this->prophesize(CacheService::class);
            $cacheService->get(ProductService::CACHE_PREFIX . $productData['sku'])->willReturn($productData);

            $productService->setCacheService($cacheService->reveal());
        };
    }

    protected function expectCategoryInCache(array $categoryData)
    {
        $this->expectations[] = function (KernelInterface $kernel) use ($categoryData) {
            /** @var CategoryService $categoryService */
            $categoryService = $kernel->getContainer()->get('category.service');
            /** @var CacheService $cacheService */
            $cacheService = $this->prophesize(CacheService::class);
            $cacheService->get($categoryData['id'])->willReturn($categoryData);

            $categoryService->setCacheService($cacheService->reveal());
        };
    }

    protected function expectCommunicationResponse(string $body, int $statusCode = 200, string $adapter = 'bob4alice')
    {
        $this->expectCommunicationResponses([new Response($statusCode, [], $body)], $adapter);
    }

    /**
     * @param Response[] $responses
     */
    protected function expectCommunicationResponses(array $responses, string $adapter = 'bob4alice')
    {
        $this->expectations[] = function (KernelInterface $kernel) use ($responses, $adapter) {
            $mock = new MockHandler($responses);
            $handler = HandlerStack::create($mock);
            $buildOrder = $kernel->getContainer()->get(sprintf('order.build.adapter.%s', $adapter));
            $buildOrder->setClient(new GuzzleClient(['handler' => $handler]));
        };
    }
}
