<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

class RecalculateCartCategoryFixture
{
    /**
     * @var array
     */
    protected $category = [];

    public function __construct()
    {
        $this->category = [
            'children' => [],
            'id' => '8295',
            'name' => 'Ropa para Perros',
            'path' => [
                    0 => [
                            'id' => '8181',
                            'name' => 'Mascotas',
                            'slug' => 'mascotas',
                            'url_key' => 'mascotas',
                        ],
                    1 => [
                            'id' => '8290',
                            'name' => 'Perros',
                            'slug' => 'mascotas/perros',
                            'url_key' => 'perros',
                        ],
                    2 => [
                            'id' => '8295',
                            'name' => 'Ropa para Perros',
                            'slug' => 'perros/ropa-para-perros',
                            'url_key' => 'ropa-para-perros',
                        ],
                ],
            'slug' => 'perros/ropa-para-perros',
            'url_key' => 'ropa-para-perros',
        ];
    }

    public function getArray()
    {
        return $this->category;
    }
}
