<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use DateTime;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\WishList\Product;
use Linio\Type\Money;

class WishListProductFixture
{
    /**
     * @var array
     */
    protected $product = [
        'id' => 5,
        'name' => 'iPhone',
        'configSku' => 'abc234654',
        'simpleSku' => 'abc234654-a1324b',
        'description' => 'A wonderful phone',
        'price' => 15000.0,
        'originalPrice' => 25000.0,
        'percentageOff' => 40.0,
        'wishListPrice' => 14500.00,
        'installments' => 12,
        'installmentPrice' => 600.0,
        'addedOn' => '2016-05-10T13:30:00-0400',
        'slug' => 'iphone',
        'url' => '/mapi/v1/p/iphone',
        'image' => 'https://media.linio.com.mx/some/path/to/iphone-product.jpg',
        'linioPlusLevel' => 1,
        'rating' => 4.3,
        'daysToDeliver' => 3,
        'imported' => true,
        'active' => true,
    ];

    /**
     * @var array
     */
    protected $inactiveProduct = [
        'id' => 5,
        'name' => 'iPhone',
        'configSku' => 'abc234654',
        'simpleSku' => 'abc234654-a1324b',
        'description' => '',
        'price' => 0.0,
        'originalPrice' => 0.0,
        'percentageOff' => 0.0,
        'wishListPrice' => 14500.00,
        'installments' => null,
        'installmentPrice' => 0.0,
        'addedOn' => '2016-05-10T13:30:00-0400',
        'slug' => null,
        'url' => null,
        'image' => null,
        'linioPlusLevel' => null,
        'rating' => 0,
        'daysToDeliver' => null,
        'imported' => null,
        'active' => false,
    ];

    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->product;
    }

    /**
     * @return array
     */
    public function getProductWithoutImagesArray(): array
    {
        $product = $this->product;

        $product['image'] = null;

        return $product;
    }

    public function getInactiveProductArray(): array
    {
        return $this->inactiveProduct;
    }

    /**
     * @return Product
     */
    public function getProductWithoutImages(): Product
    {
        $product = clone $this->getProduct();

        $product->setImages([]);

        return $product;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        $simple = new Simple();
        $simple->setSku($this->product['simpleSku']);

        $image = new Image();
        $image->setMain(true);
        $image->setSlug('media.linio.com.mx/some/path/to/iphone');

        $rating = new Rating();
        $rating->setAveragePoint($this->product['rating']);

        $product = new Product($simple);
        $product->setWishListId($this->product['id']);
        $product->setSku($this->product['configSku']);
        $product->setName($this->product['name']);
        $product->setDescription($this->product['description']);
        $product->setPrice(new Money(($this->product['price'])));
        $product->setOriginalPrice(new Money(($this->product['originalPrice'])));
        $product->setWishListPrice(new Money(($this->product['wishListPrice'])));
        $product->setInstallments($this->product['installments']);
        $product->setInstallmentPrice(new Money(($this->product['installmentPrice'])));
        $product->setAddedOn(new DateTime($this->product['addedOn']));
        $product->setSlug($this->product['slug']);
        $product->addImage($image);
        $product->setLinioPlusLevel($this->product['linioPlusLevel']);
        $product->setRating($rating);
        $product->setDeliveryTime($this->product['daysToDeliver']);
        $product->setImported($this->product['imported']);
        $product->activate();

        return $product;
    }

    /**
     * @return Product
     */
    public function getInactiveProduct(): Product
    {
        $simple = new Simple();
        $simple->setSku($this->inactiveProduct['simpleSku']);

        $product = new Product($simple);
        $product->setWishListId($this->inactiveProduct['id']);
        $product->setSku($this->inactiveProduct['configSku']);
        $product->setName($this->inactiveProduct['name']);
        $product->setWishListPrice(new Money($this->inactiveProduct['wishListPrice']));
        $product->setAddedOn(new DateTime($this->inactiveProduct['addedOn']));

        return $product;
    }
}
