<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use Linio\Component\Util\Json;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiTestCase extends FunctionalTestCase
{
    protected function get(string $uri): Response
    {
        return $this->request('GET', $uri);
    }

    protected function post(string $uri, string $body = null): Response
    {
        return $this->request('POST', $uri, $body);
    }

    protected function put(string $uri, string $body = null): Response
    {
        return $this->request('PUT', $uri, $body);
    }

    protected function delete(string $uri, string $body = null): Response
    {
        return $this->request('DELETE', $uri, $body);
    }

    protected function request(string $method, string $uri, string $body = null): Response
    {
        $this->getClient()->request($method, $uri, [], [], ['CONTENT_TYPE' => 'application/json'], $body);

        return $this->getClient()->getResponse();
    }

    protected function assertJsonResponse(Response $response, int $statusCode = Response::HTTP_OK)
    {
        $this->assertEquals($statusCode, $response->getStatusCode(), $response->getContent());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), $response->headers);
    }

    protected function assertJsonResponseEquals(Response $response, array $content, int $statusCode = Response::HTTP_OK)
    {
        $this->assertJsonResponse($response, $statusCode);
        $this->assertEquals($content, Json::decode($response->getContent()));
    }

    protected function assertJsonResponseContains(Response $response, string $content, int $statusCode = Response::HTTP_OK)
    {
        $this->assertJsonResponse($response, $statusCode);
        $expectedContent = '/' . preg_quote($content) . '/i';
        $this->assertRegExp($expectedContent, $response->getContent());
    }
}
