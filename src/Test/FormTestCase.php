<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension;
use Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FormTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @param bool $withConstraintViolations
     *
     * @return FormFactoryInterface
     */
    public function getFormFactory(bool $withConstraintViolations = false): FormFactoryInterface
    {
        $constraintViolationList = new ConstraintViolationList();

        if ($withConstraintViolations) {
            $constraintViolationList = new ConstraintViolationList([
                    new ConstraintViolation('Constraint violation foo.', '', [], '', '', ''),
                ]
            );
        }

        $validator = $this->createMock(ValidatorInterface::class);
        $validator->method('validate')->will($this->returnValue($constraintViolationList));

        $validatorCsrf = $this->createMock(CsrfTokenManager::class);
        $validatorCsrf->method('getToken')->will($this->returnValue('abc123'));

        return Forms::createFormFactoryBuilder()
            ->addTypeExtension(new FormTypeCsrfExtension($validatorCsrf))
            ->addTypeExtension(new FormTypeValidatorExtension($validator))
            ->getFormFactory();
    }
}
