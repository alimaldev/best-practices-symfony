<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Linio\Test\UnitTestCase;

class CommunicationTestCase extends UnitTestCase
{
    /**
     * @param string $body
     * @param int $statusCode
     *
     * @return GuzzleClient
     */
    public function createMockBob4AliceClient(string $body, int $statusCode = 200): GuzzleClient
    {
        $mock = new MockHandler([new Response($statusCode, [], $body)]);
        $handler = HandlerStack::create($mock);

        return new GuzzleClient(['handler' => $handler]);
    }

    /**
     * @param string $responseBody
     * @param string $uri
     * @param string $method
     *
     * @return GuzzleClient
     */
    public function createMockBob4AliceClientThatThrowsClientException(string $responseBody, string $uri, $method = 'POST'): GuzzleClient
    {
        $mock = new MockHandler([new ClientException($responseBody, new Request($method, $uri), new Response(400, [], $responseBody))]);
        $handler = HandlerStack::create($mock);

        return new GuzzleClient(['handler' => $handler]);
    }

    /**
     * @param string $responseBody
     * @param string $uri
     * @param string $method
     *
     * @return GuzzleClient
     */
    public function createMockBob4AliceClientThatThrowsServerException(string $responseBody, string $uri, string $method = 'POST'): GuzzleClient
    {
        $mock = new MockHandler([new ServerException($responseBody, new Request($method, $uri), new Response(500, [], $responseBody))]);
        $handler = HandlerStack::create($mock);

        return new GuzzleClient(['handler' => $handler]);
    }
}
