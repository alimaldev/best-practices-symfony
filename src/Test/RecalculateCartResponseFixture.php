<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

class RecalculateCartResponseFixture
{
    /**
     * @var array
     */
    protected $response = [];

    public function __construct()
    {
        $customer = (new CustomerFixture())->getCustomer();
        $this->response = [
            'customer' => [
                'id' => $customer->getId(),
                'linioPlus' => $customer->isSubscribedToLinioPlus(),
            ],
            'store' => [
                'id' => 1,
            ],
            'coupon' => [
                'code' => 'VALID_1',
                'discount' => 2000,
                'available' => [
                    'VALID_2' => 9000,
                ],
            ],
            'loyalty' => [
                'programName' => 'Club Premier',
                'registration' => '994000016',
                'points' => 37,
            ],
            'partnership' => [
                'available' => [
                    [
                        'name' => 'Clarin',
                        'code' => 'clarin',
                        'identifier' => '12345',
                        'level' => 'Gold',
                        'isActive' => true,
                        'appliedDiscount' => true,
                    ],
                ],
                'discount' => 0,
            ],
            'payment' => [
                'paymentMethod' => [
                    'name' => null,
                    'available' => [],
                    'allowed' => [],
                    'billingAddressRequired' => [],
                    'allowedByCoupon' => [],
                    'resolved' => null,
                ],
                'allowedCreditCards' => [],
                'creditCardBinNumber' => '543210',
                'installments' => [],
            ],
            'wallet' => [
                'isActive' => false,
                'totalDiscount' => null,
                'totalPointsUsed' => 0,
                'pointsBalance' => 1000,
                'shippingDiscount' => null,
                'pointsUsedShipping' => 0,
                'maxPointsForOrder' => 0,
                'conversionRate' => 0,
            ],
            'tax' => [
                'amount' => 2834,
            ],
            'shipping' => [
                'postcode' => '33020',
                'amount' => 20000,
                'originalAmount' => 20000,
                'linioPlusSavings' => 0,
                'discount' => 0,
                'packages' => [
                    '1' => [
                        'fulfillmentType' => 'warehouse',
                        'items' => [
                            '4P479PE04DZI0LAEC-17814' => 2,
                        ],
                        'quotes' => [
                            [
                                'shippingMethod' => 'regular',
                                'fee' => 10000,
                                'estimatedDeliveryDate' => '2016-03-16',
                                'pickupStoreId' => null,
                                'selected' => false,
                            ],
                            [
                                'shippingMethod' => 'express',
                                'fee' => 20000,
                                'estimatedDeliveryDate' => '2016-03-17',
                                'pickupStoreId' => null,
                                'selected' => true,
                            ],
                            [
                                'shippingMethod' => 'store',
                                'fee' => 5000,
                                'estimatedDeliveryDate' => '2016-03-18',
                                'pickupStoreId' => 2,
                                'selected' => false,
                            ],
                        ],
                    ],
                ],
                'allowStorePickup' => true,
                'pickupStores' => [
                    '2' => [
                        'id' => 2,
                        'name' => 'Pickup Store 2',
                        'description' => 'Blah blah blah',
                        'postcode' => '33020',
                        'geoHash' => 'dhwupu4907e',
                        'region' => 'Region',
                        'municipality' => 'Municipality',
                        'city' => 'City',
                        'sublocality' => 'Sublocality',
                        'neighborhood' => 'Neighborhood',
                        'addressLine1' => 'AddressLine1',
                        'addressLine2' => 'AddressLine2',
                        'apartment' => 'Apartment',
                        'referencePoint' => 'ReferencePoint',
                        'network' => [
                            'id' => 1,
                            'name' => 'NetworkName',
                        ],
                    ],
                ],
            ],
            'subtotal' => 17752,
            'grandTotal' => 37752,
            'grandTotalWithoutInterest' => null,
            'isFastLane' => false,
            'items' => [
                '4P479PE04DZI0LAEC-17814' => $this->getItem('Config_1', 2),
            ],
            'messages' => [
                'error' => [
                    'SKU0002-005' => 'ERROR_PRODUCT_OUT_OF_STOCK',
                ],
                'warning' => [],
                'success' => [],
            ],
        ];
    }

    public function clearItems()
    {
        $this->response['items'] = [];
        $this->response['shipping']['packages'] = [];
    }

    public function clearShippingAmount()
    {
        $this->response['shipping']['amount'] = null;
    }

    public function addError(string $sku, string $error)
    {
        $this->response['messages']['error'][$sku] = $error;
    }

    public function addPackage(int $id, array $items)
    {
        foreach ($items as $sku => $quantity) {
            $this->response['items'][$sku] = $this->getItem('Foobar', $quantity);
        }

        $this->response['shipping']['packages'][$id] = [
            'fulfillmentType' => 'warehouse',
            'items' => $items,
            'quotes' => [
                [
                    'shippingMethod' => 'regular',
                    'fee' => 10000,
                    'estimatedDeliveryDate' => '2016-03-16',
                    'pickupStoreId' => null,
                    'selected' => false,
                ],
                [
                    'shippingMethod' => 'express',
                    'fee' => 20000,
                    'estimatedDeliveryDate' => '2016-03-17',
                    'pickupStoreId' => null,
                    'selected' => true,
                ],
            ],
        ];
    }

    public function setPaymentMethod(array $paymentMethod)
    {
        $this->response['payment']['paymentMethod'] = $paymentMethod;
    }

    protected function getItem(string $name, int $quantity = 1)
    {
        return [
            'name' => $name,
            'quantity' => $quantity,
            'availableStock' => 5,
            'maxItemsToSell' => 5,
            'price' => [
                'unitPrice' => 9876,
                'originalPrice' => 12345,
                'paidPrice' => 17752,
                'totalDiscount' => 2000,
            ],
            'cartRule' => [
                'discount' => 0,
            ],
            'coupon' => [
                'discount' => 2000,
            ],
            'linioCreditCard' => [
                'discount' => 0,
            ],
            'wallet' => [
                'discount' => 0,
                'pointsUsed' => 0,
            ],
            'shipping' => [
                'amount' => 20000,
                'discount' => 0,
                'minimumDeliveryDate' => '2015-01-01',
            ],
            'tax' => [
                'amount' => 2834,
                'percent' => 1900,
            ],
            'linioPlus' => [
                'level' => 0,
                'quantity' => 0,
            ],
        ];
    }

    public function getJson()
    {
        return json_encode($this->response);
    }
}
