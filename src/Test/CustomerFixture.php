<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use DateTime;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Security\User;

class CustomerFixture
{
    /**
     * @var Customer
     */
    protected $customer;

    public function getCustomer(): Customer
    {
        return $this->populate(Customer::class);
    }

    public function getUser(): User
    {
        return $this->populate(User::class);
    }

    protected function populate(string $customerClass)
    {
        $address = new Address();
        $address->setDefaultBilling();
        $address->setId(1);
        $address->setFirstName('First' . rand(1, 100));
        $address->setLastName('Last' . rand(1, 100));
        $address->setLine1('Address 1');
        $address->setLine2('Address 2');
        $address->setCity('City Name');
        $address->setMunicipality('Municipality');
        $address->setNeighborhood('Neighborhood');
        $address->setRegion('Region');
        $address->setCountryId(rand(1, 200));
        $address->setPhone(
            rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9)
        );
        $address->setMobilePhone(
            rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9)
        );
        $address->setCreatedAt(new DateTime());
        $address->setUpdatedAt(new DateTime());

        $customer = new $customerClass();
        $customer->setId(1);
        $customer->setLinioId('MyLinioId');
        $customer->setEmail('customer@linio.com');
        $customer->setPassword('1234');
        $customer->setFirstName('Fake');
        $customer->setMiddleName('Customer');
        $customer->setLastName('One');
        $customer->setCreatedAt(new DateTime());
        $customer->setUpdatedAt(new DateTime());
        $customer->setVerifiedAt(new DateTime());
        $customer->setTaxIdentificationNumber('FakeTaxID');
        $customer->setBornDate(new DateTime('1942-01-01'));
        $customer->setGender('m');
        $customer->setBillingAddress($address);
        $customer->setShippingAddress($address);

        return $customer;
    }
}
