<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Linio\Frontend\Recorder\HttpFixtureFormatter;
use Linio\Frontend\Recorder\WriteFixture;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class FixtureLoadingHandler extends MockHandler
{
    /**
     * @var WriteFixture
     */
    protected $recorder;

    /**
     * @var HttpFixtureFormatter
     */
    protected $formatter;

    /**
     * @param WriteFixture $recorder
     * @param HttpFixtureFormatter $formatter
     * @param callable|null $onFulfilled
     * @param callable|null $onRejected
     */
    public function __construct(
        WriteFixture $recorder,
        HttpFixtureFormatter $formatter,
        callable $onFulfilled = null,
        callable $onRejected = null
    ) {
        $this->recorder = $recorder;
        $this->formatter = $formatter;

        parent::__construct([[$this, 'loadFixture']], $onFulfilled, $onRejected);
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(RequestInterface $request, array $options)
    {
        $responsePromise = parent::__invoke($request, $options);

        $this->append([$this, 'loadFixture']);

        return $responsePromise;
    }

    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface|GuzzleException
     */
    protected function loadFixture(RequestInterface $request)
    {
        $action = $this->formatter->getAction($request);
        $requestParameters = $this->formatter->getRequestParameters($request);

        $path = $this->recorder->generateFixtureFilePath($action, $requestParameters);

        $fixture = json_decode(file_get_contents($path), true);

        $response = new Response($fixture['responseStatus'], [], $fixture['responsePayload']);

        if ($response->getStatusCode() < 400) {
            return $response;
        }

        return RequestException::create($request, $response);
    }
}
