<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use DateTime;
use Linio\Frontend\Customer\Name;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Package;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Payment;
use Linio\Frontend\Customer\Order\Shipping;
use Linio\Frontend\Customer\Order\Shipping\Address;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Order\Payment\Method\Regular;
use Linio\Frontend\Order\Wallet;
use Linio\Type\Date;
use Linio\Type\Money;

class CustomerOrderFixture
{
    /**
     * @var array
     */
    protected $statuses = [
        'pendingConfirmation',
        'confirmed',
        'shipped',
        'delivered',
        'returned',
        'returnCompleted',
        'canceled',
        'invalid',
    ];

    /**
     * @var array
     */
    protected $skus = [
        'SA016EL0J5JAWLAEC-17544',
        'IL015EL0LL760LAEC-16524',
        'AD710EL08KTA8LAEC-80845',
        'KL690EL1KES0WLAEC-85822',
        'LE166EL0OFSU8LAEC-84918',
    ];

    /**
     * @var array
     */
    protected $itemNames = [
        'SA016EL0J5JAWLAEC-17544' => 'Pendrive Flash Memory 16 GB Sandisk Modelo Cruzer Fit-Negro',
        'IL015EL0LL760LAEC-16524' => 'Audífonos iLuv IEP205 Bubble Gum 2-Rojo',
        'AD710EL08KTA8LAEC-80845' => 'Disco Duro Externo 1TB Adata Usb 3.0 HV620',
        'KL690EL1KES0WLAEC-85822' => 'Kurve Inalámbrico estilizado mouse óptico - USB nano',
        'LE166EL0OFSU8LAEC-84918' => 'Laptop Lenovo Think Pad t420 Intel Core I5 Negro',
    ];

    /**
     * @return Order
     */
    public function createOrder(): Order
    {
        $installments = new Payment\Installments(1, Money::fromCents(rand(10, 200)), Money::fromCents(rand(10, 200)));

        $payment = new Payment();
        $payment->setPaymentMethod(new Regular('cod'));
        $payment->setInstallments($installments);

        $address = new Address();
        $address->setId(31010);
        $address->setFirstName('John');
        $address->setLastName('Test');
        $address->setStreetNumber('wer');
        $address->setApartment('wer');
        $address->setMunicipality('MERA');
        $address->setRegion('PASTAZA');
        $address->setPostcode('118015');
        $address->setPhone('(098) 138-6478');
        $address->setCountryCode('EC');
        $address->setCountryName('Ecuador');

        $shipping = new Shipping();
        $shipping->setAddress($address);
        $shipping->setAmount(Money::fromCents(rand(3, 50)));

        $order = new Order(rand(0, 100));
        $order->setCreatedAt(new DateTime('yesterday'));
        $order->setCustomerName(new Name('John', 'Test'));
        $order->setFastLane((bool) rand(0, 1));
        $order->setGrandTotal(Money::fromCents(rand(100, 10000)));
        $order->setOrderNumber((string) rand(200000000, 300000000));
        $order->setPayment($payment);
        $order->setShipping($shipping);
        $order->setWallet(new Wallet());
        $order->setStatusProgressConfiguration([
            'pendingConfirmation',
            'confirmed',
            'shipped',
            'delivered',
        ]);
        $order->setSubtotal(Money::fromCents(rand(100, 100000)));

        $packageCount = rand(1, 3);

        for ($p = 1; $p <= $packageCount; $p++) {
            $package = $this->createPackage($p, rand(1, 3));
            $order->addPackage($package);
        }

        return $order;
    }

    /**
     * @return array
     */
    public function createCacheProducts(): array
    {
        $products = [];

        foreach ($this->skus as $sku) {
            $image = new Image();
            $image->setSlug('http://media.linio.com.ec/p/converse-all-star-4288-22728-1');
            $image->setMain(true);

            $simple = new Simple();
            $simple->setSku($sku);

            $product = new Product($simple->getConfigSku());
            $product->addSimple($simple);
            $product->addImage($image);

            $products[] = $product;
        }

        return $products;
    }

    /**
     * @param int $number
     * @param int $itemCount
     *
     * @return Package
     */
    protected function createPackage(int $number, int $itemCount): Package
    {
        $package = new Package($number);
        $package->setStatus('delivered');
        $package->setAllowReturn((bool) rand(0, 1));
        $package->setAllowSellerReview((bool) rand(0, 1));
        $package->setAllowTracking((bool) rand(0, 1));
        $package->setDeliveredAt(new Date('yesterday'));
        $package->setExpectedDeliveryDate(new Date('yesterday'));
        $package->setProgressStep(rand(1, 3));
        $package->setUpdatedDeliveryDate(new Date('yesterday'));

        $items = [];

        for ($i = 0; $i < $itemCount; $i++) {
            $sku = $this->skus[rand(0, count($this->skus) - 1)];

            if (array_key_exists($sku, $items)) {
                $quantity = $items[$sku]->getQuantity() + 1;
                $items[$sku]->setQuantity($quantity);
            } else {
                $items[$sku] = $this->createItem($sku);
            }
        }

        foreach ($items as $item) {
            $package->addItem($item);
        }

        return $package;
    }

    /**
     * @param string $sku
     *
     * @return Item
     */
    protected function createItem(string $sku): Item
    {
        $image = new Image();
        $image->setSlug('//media.linio.com.co/p/some-product.jpg');

        $item = new Item($sku);
        $item->setAllowReturn((bool) rand(0, 1));
        $item->setImported((bool) rand(0, 1));
        $item->setLinioPlus((bool) rand(0, 1));
        $item->setName($this->itemNames[$sku]);
        $item->setOverweight((bool) rand(0, 1));
        $item->setPaidPrice(Money::fromCents(rand(100, 10000)));
        $item->setQuantity(1);
        $item->setSeller('Vendedor Marketplace');
        $item->setStatus($this->statuses[rand(0, 3)]);
        $item->setStatusProgressStep(rand(1, 3));
        $item->setStatusUpdatedAt(new DateTime('yesterday'));
        $item->setVirtual((bool) rand(0, 1));
        $item->setUnitPrice(Money::fromCents(rand(100, 10000)));
        $item->setExpectedDeliveryDate(new Date());
        $item->setUpdatedDeliveryDate(new Date());
        $item->setImage($image);

        return $item;
    }
}
