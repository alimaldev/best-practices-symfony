<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Recorder\WriteFixture;

class FixtureLoadingCacheService extends CacheService
{
    /**
     * @var string
     */
    protected $action = 'cache';

    /**
     * @var WriteFixture
     */
    protected $recorder;

    public function __construct(array $cacheConfig, WriteFixture $fixtureRecorder)
    {
        $this->recorder = $fixtureRecorder;

        parent::__construct($cacheConfig);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key)
    {
        $request = sprintf('%s:%s', $this->getNamespace(), $key);

        $fixtureFile = $this->recorder->generateFixtureFilePath($this->action, $request);

        if (!file_exists($fixtureFile)) {
            return;
        }

        $fixture = json_decode(file_get_contents($fixtureFile), true);

        return unserialize($fixture['responsePayload']);
    }

    /**
     * {@inheritdoc}
     */
    public function getMulti(array $keys): array
    {
        $values = [];

        foreach ($keys as $key) {
            $values[$key] = $this->get($key);
        }

        return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, $value): bool
    {
        $request = sprintf('%s:%s', $this->getNamespace(), $key);

        $this->recorder->record($this->action, $request, 200, serialize($value));

        return true;
    }
}
