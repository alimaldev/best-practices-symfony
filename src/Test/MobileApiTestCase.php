<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use Linio\Frontend\Recorder\HttpFixtureFormatter;
use Linio\Frontend\Recorder\WriteFixture;
use Linio\Frontend\Test\FixtureLoadingCacheService as CacheService;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MobileApiTestCase extends WebTestCase
{
    const TEST_RECORD_FILES_DIRECTORY = '/../tests/fixtures/third_party/tmp';

    /**
     * 1000 year token for user customer-501@linio.com, pass: rock4me.
     */
    const JWT = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXUyJ9.eyJleHAiOjMyOTk2OTk5Nzg0LCJ1c2VybmFtZSI6ImN1c3RvbWVyLTUwMUBsaW5pby5jb20iLCJ1c2VyX2lkIjo1MDEsImlhdCI6IjE0NjA5OTk3ODQifQ.DG15jwtqe1L3nrAy9_-LiwvxwnjPiuyA0XFUUJJxAef7R_pjqFH2Li4QwLeQhthffT-2NCW0uqbbmL0ZxSo3vQsK3QY5PjlGGAXWLr_6VvhncM1WAC_kRTaqJGqIBAnVHFfi1whR0ialYotegOem_JVLUtwH2AFT9FZX3cAEbCqjm5j7MdmcR67SHhLe7PPVK1a_7E5ATLAFpkucrMEWlhJL3f8PUVr9V2WAhvdGSb0R8CoDLZBfSd6jxXsCfhmToKJmM-hK2FSEYkEd7RjUWflo3n1lo7Q9-hd_mm9HrfSImLyXIS281ImrhycUeM4TXVdfmb_baEoz2I2jY0bKr_59p8Foxg_gSo4kSn7KhLUxJyxZlxAgVuQbVJKKgYtOkT5cBCa3trUuWgjqepnHkMI7Sc_acCY7wa6Ka7jUhH3Aw9QdSr8KANlj9SD7Jf-st8sQoMvHQ3J0eBhmkW2eDefrnnLcb16P_xyWmpTY_UmT6uHTU9_q1xR9TqPtqCvRRQjGhDUcFxDVYeWfjNspIWlNPpTQO3l6QUKB28zkToS8qmpAVyQmd5GTPS70vyriJb3rpo18pOvI7DTJxHY8fZn0W8gQippB_8gGWsch7gq8eAfIHFk-vy1KI8VUoWUH3P8BqBbPgXGck_zUEey2RVMtXcbCJpKuzObosm0cp5w';

    /**
     * @param array $options
     * @param array $server
     * @param bool $enableRecordFixtures
     *
     * @return Client
     */
    public static function createClient(array $options = [], array $server = [], $enableRecordFixtures = false): Client
    {
        $client = parent::createClient($options, $server);
        $container = $client->getContainer();

        // Configure default headers
        $client->setServerParameter('HTTP_X_AUTH_STORE', $container->getParameter('country_code'));
        $client->setServerParameter('CONTENT_TYPE', 'application/json');
        $client->setServerParameter('HTTP_ACCEPT', 'application/json');
        $client->setServerParameter('HTTP_HOST', 'linio.com');

        $fixtureStorePath = $container->getParameter('catcher_save_path');

        if ($enableRecordFixtures) {
            $fixtureStorePath = $client->getKernel()->getRootDir() . self::TEST_RECORD_FILES_DIRECTORY;

            if (!is_dir($fixtureStorePath)) {
                mkdir($fixtureStorePath);
            }
        }

        // Configure fixture loading container services
        $fixtureFormatter = new HttpFixtureFormatter();
        $fixtureRecorder = new WriteFixture(
            $fixtureStorePath,
            $container->getParameter('country_code'),
            $enableRecordFixtures
        );
        $guzzleFixtureLoadingHandler = new FixtureLoadingHandler($fixtureRecorder, $fixtureFormatter);
        $guzzleHandlerStack = HandlerStack::create($guzzleFixtureLoadingHandler);
        $guzzleFixtureLoadingClient = new GuzzleClient(['handler' => $guzzleHandlerStack]);

        $client->getContainer()->set('communication.adapter.oldbob.client', $guzzleFixtureLoadingClient);
        $client->getContainer()->set('communication.adapter.bob4alice.client', $guzzleFixtureLoadingClient);
        $client->getContainer()->set('search.adapter.hawk.client', $guzzleFixtureLoadingClient);
        $client->getContainer()->set(
            'cache.location.legacy',
            new CacheService($container->getParameter('cache_location_legacy'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.location.sepomex',
            new CacheService($container->getParameter('cache_location_sepomex'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.attribute',
            new CacheService($container->getParameter('cache_attribute'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.api.mobile.cms',
            new CacheService($container->getParameter('cache_api_mobile_cms'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.assets',
            new CacheService($container->getParameter('cache_assets'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.brand',
            new CacheService($container->getParameter('cache_brand'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.order',
            new CacheService($container->getParameter('cache_order'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.voucher',
            new CacheService($container->getParameter('cache_voucher'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.category',
            new CacheService($container->getParameter('cache_category'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.campaign',
            new CacheService($container->getParameter('cache_campaign'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.communication.mock',
            new CacheService($container->getParameter('cache_communication_mock'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.product',
            new CacheService($container->getParameter('cache_product'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.rating',
            new CacheService($container->getParameter('cache_rating'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.region',
            new CacheService($container->getParameter('cache_region'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.route',
            new CacheService($container->getParameter('cache_route'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.search.filter',
            new CacheService($container->getParameter('cache_search_filter'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.configuration',
            new CacheService($container->getParameter('cache_configuration'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.cms',
            new CacheService($container->getParameter('cache_cms'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.order.return',
            new CacheService($container->getParameter('cache_item_return'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.mobile.configuration',
            new CacheService($container->getParameter('cache_mobile_configuration'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.seller',
            new CacheService($container->getParameter('cache_seller'), $fixtureRecorder)
        );
        $client->getContainer()->set(
            'cache.auth',
            new CacheService($container->getParameter('cache_auth'), $fixtureRecorder)
        );

        return $client;
    }

    public function tearDown()
    {
        $testFilesDirectory = $this->createKernel()->getRootDir() . self::TEST_RECORD_FILES_DIRECTORY;

        if (is_dir($testFilesDirectory)) {
            $this->removeDirectory($testFilesDirectory);
        }
    }

    /**
     * @param string $directory
     */
    protected function removeDirectory(string $directory)
    {
        $resources = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($directory, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($resources as $resource) {
            if ($resource->isDir()) {
                rmdir($resource->getRealPath());
            } else {
                unlink($resource->getRealPath());
            }
        }

        rmdir($directory);
    }
}
