<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use Linio\Frontend\Entity\Customer\Coupon;

class CustomerCouponsResponseFixture
{
    /**
     * @var array
     */
    protected $response = [
        [
            'code' => 'P3rc3nTc0d3',
            'validFrom' => '2016-01-01',
            'validUntil' => '2017-01-01',
            'isActive' => 1,
            'discountType' => 'percent',
            'discountPercentage' => 10,
            'amountAvailable' => null,
            'amount' => null,
        ],
    ];

    /**
     * @return array
     */
    public function getResponse(): array
    {
        return $this->response;
    }

    /**
     * @return array
     */
    public function getCoupons(): array
    {
        $coupon = new Coupon();
        $coupon->setCode($this->response[0]['code']);
        $coupon->setValidFrom(new \DateTime($this->response[0]['validFrom']));
        $coupon->setValidUntil(new \DateTime($this->response[0]['validUntil']));
        $coupon->setActive(true);
        $coupon->setDiscountType(Coupon::DISCOUNT_TYPE_PERCENT);
        $coupon->setAmount($this->response[0]['discountPercentage'] / 100);

        return [$coupon];
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->response);
    }
}
