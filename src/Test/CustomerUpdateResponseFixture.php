<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use DateTime;
use Linio\Frontend\Entity\Customer;

class CustomerUpdateResponseFixture
{
    /**
     * @var array
     */
    protected $response = [];

    public function __construct()
    {
        $this->response = [
            'id' => 1237698,
            'linioId' => 'ooajld',
            'firstName' => 'Robert',
            'lastName' => 'Plant',
            'email' => 'robert.plant@linio.com',
            'gender' => 'male',
            'birthDate' => '1948-08-20',
            'nationalRegistrationNumber' => '392084092023',
            'taxIdentificationNumber' => '308402098230',
            'isReturningCustomer' => true,
        ];
    }

    /**
     * @return array
     */
    public function getResponse(): array
    {
        return $this->response;
    }

    public function getCustomer(): Customer
    {
        $customer = new Customer();
        $customer->setId($this->response['id']);
        $customer->setLinioId($this->response['linioId']);
        $customer->setFirstName($this->response['firstName']);
        $customer->setLastName($this->response['lastName']);
        $customer->setEmail($this->response['email']);
        $customer->setGender($this->response['gender']);
        $customer->setBornDate(new DateTime($this->response['birthDate']));
        $customer->setNationalRegistrationNumber($this->response['nationalRegistrationNumber']);
        $customer->setTaxIdentificationNumber($this->response['taxIdentificationNumber']);
        $customer->setReturningCustomer($this->response['isReturningCustomer']);

        return $customer;
    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->response);
    }
}
