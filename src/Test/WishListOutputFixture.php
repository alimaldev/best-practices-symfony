<?php

declare(strict_types=1);

namespace Linio\Frontend\Test;

use DateTime;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;
use Linio\Type\Money;

class WishListOutputFixture
{
    /**
     * @var array
     */
    protected $wishList = [
        'id' => 10,
        'name' => 'Christmas',
        'description' => 'My Christmas shopping list',
        'default' => false,
        'owner' => [
            'name' => 'Anka Gonzalez',
            'email' => 'customer-501@linio.com',
            'linioId' => '2xqyz',
        ],
        'createdAt' => '2016-05-10T13:28:25-0400',
        'visibility' => 'private',
        'accessHash' => '',
        'products' => [
            'abc234654-a1324b' => [
                'id' => 5,
                'name' => 'iPhone',
                'configSku' => 'abc234654',
                'simpleSku' => 'abc234654-a1324b',
                'description' => 'A wonderful phone',
                'price' => 15000.0,
                'originalPrice' => 25000.0,
                'percentageOff' => 40,
                'wishListPrice' => 14500.00,
                'installments' => 12,
                'installmentPrice' => 600.0,
                'addedOn' => '2016-05-10T13:30:00-0400',
                'slug' => 'iphone',
                'url' => '/mapi/v1/p/iphone',
                'image' => 'https://media.linio.com.mx/some/path/to/iphone-product.jpg',
                'linioPlusLevel' => 1,
                'rating' => 4.3,
                'daysToDeliver' => 3,
                'imported' => true,
            ],
        ],
    ];

    /**
     * @return array
     */
    public function getWishListArray(): array
    {
        return $this->wishList;
    }

    /**
     * @return WishList
     */
    public function getWishList(): WishList
    {
        $owner = new Customer();
        $owner->setFirstName('Anka');
        $owner->setLastName('Gonzalez');
        $owner->setEmail($this->wishList['owner']['email']);
        $owner->setLinioId($this->wishList['owner']['linioId']);

        $simple = new Simple();
        $simple->setSku($this->wishList['products']['abc234654-a1324b']['simpleSku']);

        $image = new Image();
        $image->setMain(true);
        $image->setSlug('media.linio.com.mx/some/path/to/iphone');

        $product = new Product($simple);
        $product->setConfigId($this->wishList['products']['abc234654-a1324b']['id']);
        $product->setSku('abc234654-a1324b');
        $product->setName($this->wishList['products']['abc234654-a1324b']['name']);
        $product->setDescription($this->wishList['products']['abc234654-a1324b']['description']);
        $product->setPrice(new Money(($this->wishList['products']['abc234654-a1324b']['price'])));
        $product->setOriginalPrice(new Money(($this->wishList['products']['abc234654-a1324b']['originalPrice'])));
        $product->setWishListPrice(new Money(($this->wishList['products']['abc234654-a1324b']['wishListPrice'])));
        $product->setInstallments($this->wishList['products']['abc234654-a1324b']['installments']);
        $product->setInstallmentPrice(new Money(($this->wishList['products']['abc234654-a1324b']['installmentPrice'])));
        $product->setAddedOn(new DateTime($this->wishList['products']['abc234654-a1324b']['addedOn']));
        $product->setSlug($this->wishList['products']['abc234654-a1324b']['slug']);
        $product->addImage($image);
        $product->setLinioPlusLevel($this->wishList['products']['abc234654-a1324b']['linioPlusLevel']);
        $product->setRating($this->wishList['products']['abc234654-a1324b']['rating']);
        $product->setDeliveryTime($this->wishList['products']['abc234654-a1324b']['daysToDeliver']);
        $product->setImported($this->wishList['products']['abc234654-a1324b']['imported']);

        $wishlist = new WishList($this->wishList['name'], $owner);
        $wishlist->setId($this->wishList['id']);
        $wishlist->setName($this->wishList['name']);
        $wishlist->setDescription($this->wishList['description']);
        $wishlist->setDefault($this->wishList['default']);
        $wishlist->setCreatedAt(new DateTime($this->wishList['createdAt']));
        $wishlist->setVisibility($this->wishList['visibility']);
        $wishlist->setAccessHash($this->wishList['accessHash']);
        $wishlist->addProduct($product);

        return $wishlist;
    }

    /**
     * @return WishList[]
     */
    public function getWishLists(): array
    {
        return [$this->getWishList()];
    }

    /**
     * @return WishList
     */
    public function getWishListWithoutProducts(): WishList
    {
        $wishList = clone $this->getWishList();
        $wishList->setProducts([]);

        return $wishList;
    }

    /**
     * @return array
     */
    public function getWishListWithoutProductsArray(): array
    {
        $wishList = $this->wishList;
        $wishList['products'] = [];

        return $wishList;
    }
}
