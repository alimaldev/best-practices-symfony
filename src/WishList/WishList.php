<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList;

use DateTime;
use Linio\Frontend\Entity\Customer;

class WishList
{
    const VISIBILITY_PUBLIC = 'public';
    const VISIBILITY_PRIVATE = 'private';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var Customer
     */
    protected $owner;

    /**
     * @var string
     */
    protected $visibility = self::VISIBILITY_PRIVATE;

    /**
     * @var string
     */
    protected $accessHash = '';

    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var bool
     */
    protected $default = false;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @param string $name
     * @param Customer $owner
     */
    public function __construct(string $name, Customer $owner)
    {
        $this->name = $name;
        $this->owner = $owner;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return Customer
     */
    public function getOwner(): Customer
    {
        return $this->owner;
    }

    /**
     * @param Customer $owner
     */
    public function setOwner(Customer $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getVisibility(): string
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     */
    public function setVisibility(string $visibility)
    {
        $this->visibility = $visibility;
    }

    /**
     * @return string
     */
    public function getAccessHash(): string
    {
        return $this->accessHash;
    }

    /**
     * @param string $accessHash
     */
    public function setAccessHash(string $accessHash)
    {
        $this->accessHash = $accessHash;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return bool
     */
    public function isDefault(): bool
    {
        return $this->default;
    }

    /**
     * @param bool $default
     */
    public function setDefault(bool $default)
    {
        $this->default = $default;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product)
    {
        $this->products[$product->getSku()] = $product;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->visibility === self::VISIBILITY_PUBLIC;
    }
}
