<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ValidateWishList extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            TextType::class,
            [
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                ],
            ]
        )
        ->add(
            'description',
            TextareaType::class,
            [
                'required' => false,
            ]
        )
        ->add(
            'default',
            CheckboxType::class,
            [
                'required' => false,
                'data' => false,
            ]
        );
    }
}
