<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product as CatalogProduct;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\WishList\Communication\WishListAdapter;
use Linio\Frontend\WishList\Exception\InvalidWishListException;
use Linio\Frontend\WishList\Exception\WishListException;
use Linio\Frontend\WishList\Exception\WishListNotFoundException;

class ManageWishList
{
    /**
     * @var WishListAdapter
     */
    protected $adapter;

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var CacheService
     */
    protected $wishListSkusCache;

    /**
     * @param ProductService $productService
     */
    public function setProductService(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param WishListAdapter $adapter
     * @param CacheService $wishListSkusCache
     */
    public function __construct(WishListAdapter $adapter, CacheService $wishListSkusCache)
    {
        $this->adapter = $adapter;
        $this->wishListSkusCache = $wishListSkusCache;
    }

    /**
     * @param WishList $wishList
     *
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function createWishList(WishList $wishList): WishList
    {
        return $this->adapter->createWishList($wishList);
    }

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function editWishList(WishList $wishList): WishList
    {
        return $this->fetchProductData(
            $this->adapter->editWishList($wishList)
        );
    }

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function getWishList(WishList $wishList): WishList
    {
        return $this->fetchProductData(
            $this->adapter->getWishList($wishList)
        );
    }

    /**
     * @param Customer $customer
     *
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList[]
     */
    public function getWishLists(Customer $customer): array
    {
        $updatedWishLists = [];

        $wishLists = $this->adapter->getWishLists($customer);

        foreach ($wishLists as $wishList) {
            $updatedWishLists[] = $this->fetchProductData($wishList);
        }

        return $updatedWishLists;
    }

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     */
    public function setDefaultWishList(WishList $wishList)
    {
        $this->adapter->setDefaultWishList($wishList);
    }

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     */
    public function deleteWishList(WishList $wishList)
    {
        $this->adapter->deleteWishList($wishList);
        $this->invalidateSkuCache($wishList->getOwner());
    }

    /**
     * @param WishList $wishList
     * @param Product $product
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function addProductToWishList(WishList $wishList, Product $product): WishList
    {
        $this->invalidateSkuCache($wishList->getOwner());

        return $this->fetchProductData(
            $this->adapter->addProductToWishList($wishList, $product)
        );
    }

    /**
     * @param WishList $wishList
     * @param Product $product
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function removeProductFromWishList(WishList $wishList, Product $product)
    {
        $this->adapter->removeProductFromWishList($wishList, $product);
        $this->invalidateSkuCache($wishList->getOwner());
    }

    /**
     * @param Customer $customer
     *
     * @throws InputException
     * @throws WishListException
     *
     * @return array
     */
    public function getAllSkusInWishLists(Customer $customer): array
    {
        if ($customer instanceof GuestUser) {
            return [];
        }

        $skusInCache = $this->wishListSkusCache->get((string) $customer->getId());

        if ($skusInCache !== null) {
            return $skusInCache;
        }

        $skus = $this->adapter->getAllSkusInWishLists($customer);

        $this->wishListSkusCache->set((string) $customer->getId(), $skus);

        return $skus;
    }

    /**
     * @param Customer $customer
     */
    protected function invalidateSkuCache(Customer $customer)
    {
        $this->wishListSkusCache->delete((string) $customer->getId());
    }

    /**
     * Converts Catalog Products in order to update the WishList Products with recent information.
     *
     * @param WishList $wishList
     *
     * @return WishList
     */
    protected function fetchProductData(WishList $wishList): WishList
    {
        $activeProducts = [];

        $cacheProducts = $this->productService->getBySkus(array_keys($wishList->getProducts()));

        $className = Product::class;

        /** @var CatalogProduct $product */
        foreach ($cacheProducts as $product) {

            /* @var Product $convertedProduct */
            $convertedProduct = unserialize(sprintf(
                'O:%d:"%s"%s',
                strlen($className),
                $className,
                strstr(strstr(serialize($product), '"'), ':')
            ));

            $wishListProduct = $wishList->getProducts()[$product->getSku()];

            $convertedProduct->activate();

            $this->setMissingProductFields($convertedProduct, $wishListProduct);

            $activeProducts[$convertedProduct->getSku()] = $convertedProduct;
        }

        $inactiveProducts = array_diff_key($wishList->getProducts(), $activeProducts);
        $wishListProducts = array_merge($inactiveProducts, $activeProducts);

        $wishList->setProducts($wishListProducts);

        return $wishList;
    }

    /**
     * @param Product $completeProduct
     * @param Product $sourceProduct
     */
    protected function setMissingProductFields(Product $completeProduct, Product $sourceProduct)
    {
        $completeProduct->setWishListId(
            $sourceProduct->getWishListId()
        );

        $selectedSimple = $completeProduct->getSimple($sourceProduct->getSelectedSimple()->getSku());

        if ($selectedSimple) {
            $completeProduct->setSelectedSimple($selectedSimple);
        } else {
            $completeProduct->setSelectedSimple($sourceProduct->getSelectedSimple());
        }

        $completeProduct->setWishListPrice(
            $sourceProduct->getWishListPrice()
        );

        $completeProduct->setAddedOn(
            $sourceProduct->getAddedOn()
        );
    }
}
