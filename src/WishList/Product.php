<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList;

use DateTime;
use Linio\Frontend\Entity\Product as CatalogProduct;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Type\Money;

class Product extends CatalogProduct
{
    /**
     * @var int
     */
    protected $wishListId;

    /**
     * @var Simple
     */
    protected $selectedSimple;

    /**
     * @var Money
     */
    protected $wishListPrice;

    /**
     * @var DateTime
     */
    protected $addedOn;

    /**
     * @var bool
     */
    protected $active = false;

    /**
     * @param Simple $simple
     */
    public function __construct(Simple $simple)
    {
        parent::__construct();

        $this->selectedSimple = $simple;
        $this->wishListPrice = new Money();
    }

    /**
     * @return int|null
     */
    public function getWishListId()
    {
        return $this->wishListId;
    }

    /**
     * @param int $wishListId
     */
    public function setWishListId(int $wishListId)
    {
        $this->wishListId = $wishListId;
    }

    /**
     * @return Simple
     */
    public function getSelectedSimple(): Simple
    {
        return $this->selectedSimple;
    }

    /**
     * @param Simple $selectedSimple
     */
    public function setSelectedSimple(Simple $selectedSimple)
    {
        $this->selectedSimple = $selectedSimple;
    }

    /**
     * @return Money
     */
    public function getWishListPrice(): Money
    {
        return $this->wishListPrice;
    }

    /**
     * @param Money $wishListPrice
     */
    public function setWishListPrice(Money $wishListPrice)
    {
        $this->wishListPrice = $wishListPrice;
    }

    /**
     * @return DateTime|null
     */
    public function getAddedOn()
    {
        return $this->addedOn;
    }

    /**
     * @param DateTime $addedOn
     */
    public function setAddedOn(DateTime $addedOn)
    {
        $this->addedOn = $addedOn;
    }

    public function activate()
    {
        $this->active = true;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }
}
