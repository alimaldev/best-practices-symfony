<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication\Output;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

interface WishListOutput
{
    /**
     * @param WishList $wishList
     *
     * @return array
     */
    public function toCreateWishList(WishList $wishList): array;

    /**
     * @param WishList $wishList
     *
     * @return array
     */
    public function toEdit(WishList $wishList): array;

    /**
     * @param WishList $wishList
     *
     * @return array
     */
    public function toWishListIdentifier(WishList $wishList): array;

    /**
     * @param Customer $customer
     *
     * @return array
     */
    public function toWishListCollection(Customer $customer): array;

    /**
     * @param WishList $wishList
     * @param Product $product
     *
     * @return array
     */
    public function toAddWishListProduct(WishList $wishList, Product $product): array;

    /**
     * @param WishList $wishList
     * @param Product $product
     *
     * @return array
     */
    public function toRemoveWishListProduct(WishList $wishList, Product $product): array;

    /**
     * @param Customer $customer
     *
     * @return array
     */
    public function toGetAllSkusInWishLists(Customer $customer): array;
}
