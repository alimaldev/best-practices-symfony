<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication\Output;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

class Bob4Alice implements WishListOutput
{
    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @param PlatformInformation $platformInformation
     */
    public function __construct(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    /**
     * {@inheritdoc}
     */
    public function toCreateWishList(WishList $wishList): array
    {
        return [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $wishList->getOwner()->getId(),
            'visibility' => $wishList->getVisibility(),
            'name' => $wishList->getName(),
            'description' => $wishList->getDescription(),
            'ownerName' => $wishList->getOwner()->getFullName(),
            'ownerEmail' => $wishList->getOwner()->getEmail(),
            'accessHash' => $wishList->getAccessHash(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toEdit(WishList $wishList): array
    {
        $wishListOutput = $this->toCreateWishList($wishList);

        $wishListOutput['wishlistId'] = $wishList->getId();

        return $wishListOutput;
    }

    /**
     * {@inheritdoc}
     */
    public function toWishListIdentifier(WishList $wishList): array
    {
        return [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toWishListCollection(Customer $customer): array
    {
        return [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toAddWishListProduct(WishList $wishList, Product $product): array
    {
        $simple = $product->getSelectedSimple();

        return [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $wishList->getOwner()->getId(),
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $simple->getSku(),
            'configSku' => $simple->getConfigSku(),
            'name' => $product->getName(),
            'price' => $product->getWishListPrice()->getAmount(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function toRemoveWishListProduct(WishList $wishList, Product $product): array
    {
        return [
            'storeId' => $this->platformInformation->getStoreId(),
            'wishlistId' => $wishList->getId(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
        ];
    }

    /**
     * @param Customer $customer
     *
     * @return array
     */
    public function toGetAllSkusInWishLists(Customer $customer): array
    {
        return [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];
    }
}
