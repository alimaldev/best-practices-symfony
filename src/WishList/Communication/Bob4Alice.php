<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\WishList\Communication\Input\WishListInput;
use Linio\Frontend\WishList\Communication\Output\WishListOutput;
use Linio\Frontend\WishList\Exception\InvalidWishListException;
use Linio\Frontend\WishList\Exception\WishListException;
use Linio\Frontend\WishList\Exception\WishListNotFoundException;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

class Bob4Alice implements WishListAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var WishListInput
     */
    protected $wishListInput;

    /**
     * @var WishListOutput
     */
    protected $wishListOutput;

    public function __construct(ClientInterface $clientInterface, WishListInput $wishListInput, WishListOutput $wishListOutput)
    {
        $this->client = $clientInterface;
        $this->wishListInput = $wishListInput;
        $this->wishListOutput = $wishListOutput;
    }

    /**
     * {@inheritdoc}
     */
    public function createWishList(WishList $wishList): WishList
    {
        try {
            $response = $this->client->request('POST', '/wishlist/create', [
                'json' => $this->wishListOutput->toCreateWishList($wishList),
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new WishListException($responseBody['code'], $responseBody);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->wishListInput->fromWishListDetail($responseBody, $wishList->getOwner());
    }

    /**
     * {@inheritdoc}
     */
    public function editWishList(WishList $wishList): WishList
    {
        try {
            $response = $this->client->request('POST', '/wishlist/edit', [
                'json' => $this->wishListOutput->toEdit($wishList),
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::ENTITY_NOT_FOUND) {
                throw new WishListNotFoundException(ExceptionMessage::ENTITY_NOT_FOUND);
            }

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new WishListException($responseBody['code'], $responseBody);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->wishListInput->fromWishListDetail($responseBody, $wishList->getOwner());
    }

    /**
     * {@inheritdoc}
     */
    public function getWishLists(Customer $customer): array
    {
        $wishListData = $this->wishListOutput->toWishListCollection($customer);
        try {
            $response = $this->client->request(
                'POST',
                '/wishlist/list',
                [
                    'json' => $wishListData,
                ]
            );
        } catch (ClientException $clientException) {
            $responseBody = Json::decode((string) $clientException->getResponse()->getBody());
            throw new InvalidWishListException($responseBody['code'], $responseBody);
        } catch (ServerException $serverException) {
            $responseBody = Json::decode((string) $serverException->getResponse()->getBody());
            throw new WishListException($responseBody['code'], $responseBody);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->wishListInput->fromWishLists($responseBody, $customer);
    }

    /**
     * {@inheritdoc}
     */
    public function getWishList(WishList $wishList): WishList
    {
        $wishListData = $this->wishListOutput->toWishListIdentifier($wishList);

        try {
            $response = $this->client->request(
                'POST',
                '/wishlist/detail',
                [
                    'json' => $wishListData,
                ]
            );
        } catch (ClientException $clientException) {
            $responseBody = Json::decode((string) $clientException->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::ENTITY_NOT_FOUND) {
                throw new WishListNotFoundException(ExceptionMessage::ENTITY_NOT_FOUND);
            }

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $serverException) {
            $responseBody = Json::decode((string) $serverException->getResponse()->getBody());
            throw new WishListException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->wishListInput->fromWishListDetail($responseBody, $wishList->getOwner());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultWishList(WishList $wishList)
    {
        $requestBody = $this->wishListOutput->toWishListIdentifier($wishList);

        try {
            $this->client->request('POST', '/wishlist/default', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::ENTITY_NOT_FOUND) {
                throw new WishListNotFoundException($responseBody['code']);
            }

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new WishListException($responseBody['code'], $responseBody);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function deleteWishList(WishList $wishList)
    {
        $requestBody = $this->wishListOutput->toWishListIdentifier($wishList);

        try {
            $this->client->request('POST', '/wishlist/delete', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::ENTITY_NOT_FOUND) {
                throw new WishListNotFoundException($responseBody['code']);
            }

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new WishListException($responseBody['code'], $responseBody);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function addProductToWishList(WishList $wishList, Product $product): WishList
    {
        $requestBody = $this->wishListOutput->toAddWishListProduct($wishList, $product);

        try {
            $response = $this->client->request('POST', '/wishlist/add-product', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::ENTITY_NOT_FOUND) {
                throw new WishListNotFoundException($responseBody['code']);
            }

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new WishListException($responseBody['code'], $responseBody);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->wishListInput->fromAddWishListProduct($responseBody, $wishList, $product);
    }

    /**
     * {@inheritdoc}
     */
    public function removeProductFromWishList(WishList $wishList, Product $product)
    {
        $requestBody = $this->wishListOutput->toRemoveWishListProduct($wishList, $product);

        try {
            $this->client->request('POST', '/wishlist/remove-product', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::ENTITY_NOT_FOUND) {
                throw new WishListNotFoundException($responseBody['code']);
            }

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new WishListException($responseBody['code'], $responseBody);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllSkusInWishLists(Customer $customer): array
    {
        $requestBody = $this->wishListOutput->toGetAllSkusInWishLists($customer);

        try {
            $response = $this->client->request('POST', '/wishlist/skus', ['json' => $requestBody]);
        } catch (ClientException $clientException) {
            $responseBody = Json::decode((string) $clientException->getResponse()->getBody());

            throw new InvalidWishListException($responseBody['code']);
        } catch (ServerException $serverException) {
            $responseBody = Json::decode((string) $serverException->getResponse()->getBody());

            throw new WishListException($responseBody['code']);
        }

        return Json::decode((string) $response->getBody());
    }
}
