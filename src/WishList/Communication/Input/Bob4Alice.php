<?php

namespace Linio\Frontend\WishList\Communication\Input;

use DateTime;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;
use Linio\Type\Money;

class Bob4Alice implements WishListInput
{
    /**
     * {@inheritdoc}
     */
    public function fromWishListDetail(array $wishListData, Customer $customer)
    {
        $wishList = new WishList($wishListData['name'] ?? '', $customer);
        $wishList->setId($wishListData['wishlistId']);
        $wishList->setVisibility($wishListData['visibility']);
        $wishList->setDescription($wishListData['description'] ?? '');
        $wishList->setDefault($wishListData['isDefault'] ?? false);

        if (isset($wishListData['createdAt'])) {
            $wishList->setCreatedAt(new DateTime($wishListData['createdAt']));
        }

        $products = [];

        if (!empty($wishListData['products'])) {
            foreach ($wishListData['products'] as $productData) {
                $simple = new Simple();
                $simple->setSku($productData['simpleSku']);
                $product = new Product($simple);

                $product->setWishListId($productData['wishlistItemId']);
                $product->setSku($productData['configSku']);
                $product->setName($productData['name'] ?? '');
                $product->setWishListPrice(new Money($productData['price']));
                $product->setAddedOn(new DateTime($productData['createdAt']));

                $products[$product->getSku()] = $product;
            }
        }

        $wishList->setProducts($products);

        return $wishList;
    }

    /**
     * {@inheritdoc}
     */
    public function fromWishLists(array $wishListsData, Customer $customer)
    {
        $wishLists = [];
        foreach ($wishListsData as $wishList) {
            $wishLists[] = $this->fromWishListDetail($wishList, $customer);
        }

        return $wishLists;
    }

    /**
     * {@inheritdoc}
     */
    public function fromAddWishListProduct(array $wishListData, WishList $wishList, Product $product)
    {
        if (!isset($wishListData['wishlistItemId'])) {
            return $wishList;
        }

        $product->setWishListId($wishListData['wishlistItemId']);
        $wishList->addProduct($product);

        return $wishList;
    }
}
