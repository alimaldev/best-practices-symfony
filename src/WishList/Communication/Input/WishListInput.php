<?php

namespace Linio\Frontend\WishList\Communication\Input;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

interface WishListInput
{
    /**
     * @param array $wishListData
     * @param Customer $customer
     *
     * @return WishList
     */
    public function fromWishListDetail(array $wishListData, Customer $customer);

    /**
     * @param array $wishListsData
     * @param Customer $customer
     *
     * @return WishList
     */
    public function fromWishLists(array $wishListsData, Customer $customer);

    /**
     * @param array $wishListData
     * @param WishList $wishList
     * @param Product $product
     *
     * @return WishList
     */
    public function fromAddWishListProduct(array $wishListData, WishList $wishList, Product $product);
}
