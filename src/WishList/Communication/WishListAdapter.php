<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Communication;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\WishList\Exception\InvalidWishListException;
use Linio\Frontend\WishList\Exception\WishListException;
use Linio\Frontend\WishList\Exception\WishListNotFoundException;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

interface WishListAdapter
{
    /**
     * @param WishList $wishList
     *
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function createWishList(WishList $wishList): WishList;

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function editWishList(WishList $wishList): WishList;

    /**
     * @param Customer $customer
     *
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList[]
     */
    public function getWishLists(Customer $customer): array;

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function getWishList(WishList $wishList): WishList;

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     */
    public function setDefaultWishList(WishList $wishList);

    /**
     * @param WishList $wishList
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     */
    public function deleteWishList(WishList $wishList);

    /**
     * @param WishList $wishList
     * @param Product $product
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return WishList
     */
    public function addProductToWishList(WishList $wishList, Product $product): WishList;

    /**
     * @param WishList $wishList
     * @param Product $product
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     */
    public function removeProductFromWishList(WishList $wishList, Product $product);

    /**
     * @param Customer $customer
     *
     * @throws WishListException
     * @throws InputException
     *
     * @return array
     */
    public function getAllSkusInWishLists(Customer $customer): array;
}
