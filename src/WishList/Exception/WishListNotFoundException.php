<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Exception;

use Linio\Exception\NotFoundHttpException;

class WishListNotFoundException extends NotFoundHttpException
{
}
