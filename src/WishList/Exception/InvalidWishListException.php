<?php

declare(strict_types=1);

namespace Linio\Frontend\WishList\Exception;

use Linio\Frontend\Exception\InputException;

class InvalidWishListException extends InputException
{
}
