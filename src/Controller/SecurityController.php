<?php

namespace Linio\Frontend\Controller;

use Linio\Controller\FormAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Form\LoginForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/", service="controller.security")
 */
class SecurityController
{
    use FormAware;
    use TemplatingAware;

    /**
     * @var AuthenticationUtils
     */
    protected $authenticationUtils;

    /**
     * @var bool
     */
    protected $isTelesalesEnabled = false;

    /**
     * @param AuthenticationUtils $authenticationUtils
     */
    public function setAuthenticationUtils(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @param bool $telesalesStatus
     */
    public function setTelesalesStatus($telesalesStatus)
    {
        $this->isTelesalesEnabled = $telesalesStatus;
    }

    /**
     * @Route("/account/login/{modal}", defaults={"modal" = false}, name="frontend.security.login")
     *
     * @param bool $modal
     *
     * @return Response
     */
    public function loginAction($modal = false)
    {
        $template = $modal ? ':Security/forms:login_form.html.twig' : ':Security:index.html.twig';

        return $this->render($template, [
            'form' => $this->formFactory->create(LoginForm::class)->createView(),
            'last_username' => $this->authenticationUtils->getLastUsername(),
            'error' => $this->authenticationUtils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/telesales/login", name="frontend.security.telesales.login")
     *
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function telesalesLoginAction()
    {
        if (!$this->isTelesalesEnabled) {
            throw new NotFoundHttpException();
        }

        return $this->render(':Security:telesales_login.html.twig', [
            'last_username' => $this->authenticationUtils->getLastUsername(),
            'error' => $this->authenticationUtils->getLastAuthenticationError(),
        ]);
    }
}
