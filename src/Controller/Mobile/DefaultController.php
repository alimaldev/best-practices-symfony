<?php

namespace Linio\Frontend\Controller\Mobile;

use Linio\Exception\NotFoundHttpException;
use Linio\Frontend\Api\Output\MobileConfiguration;
use Linio\Frontend\Communication\Customer\ItemReturnService;
use Linio\Frontend\Store\MobileConfigurationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/mapi", service="controller.mobile.default", defaults={"_format":"json"})
 */
class DefaultController
{
    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @var MobileConfigurationService
     */
    protected $storeService;

    /**
     * @var ItemReturnService
     */
    protected $itemReturnService;

    /**
     * @var MobileConfiguration
     */
    protected $mobileConfigurationOutput;

    /**
     * @param ItemReturnService $itemReturnService
     */
    public function setItemReturnService(ItemReturnService $itemReturnService)
    {
        $this->itemReturnService = $itemReturnService;
    }

    /**
     * @param MobileConfiguration $mobileConfigurationOutput
     */
    public function setMobileConfigurationOutput(MobileConfiguration $mobileConfigurationOutput)
    {
        $this->mobileConfigurationOutput = $mobileConfigurationOutput;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @param MobileConfigurationService $storeService
     */
    public function setStoreService(MobileConfigurationService $storeService)
    {
        $this->storeService = $storeService;
    }

    /**
     * @Route("", name="frontend.mobile.default", methods={"POST"})
     * @Route("/v1", name="frontend.mobile.default.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function indexAction()
    {
        return new JsonResponse('Linio Mobile API v1');
    }

    /**
     * @Route("/store", name="frontend.mobile.default.store", methods={"POST"});
     * @Route("/v1/store", name="frontend.mobile.default.store.v1", methods={"POST"});
     *
     * @return JsonResponse
     */
    public function storeAction()
    {
        $parseData = $this->storeService->getConfigParsedData($this->countryCode);
        $resultData = $this->mobileConfigurationOutput->fromStoreConfiguration($parseData);

        return new JsonResponse(['store' => $resultData]);
    }

    /**
     * @Route("/store/form/{formName}", name="frontend.mobile.default.store.form", methods={"POST"});
     * @Route("/v1/store/form/{formName}", name="frontend.mobile.default.store.form.v1", methods={"POST"});
     *
     * @param string $formName
     *
     * @return JsonResponse
     */
    public function formsAction($formName)
    {
        $parseData = $this->storeService->getConfigParsedData($this->countryCode);

        if (isset($parseData['dynamic_form'][$formName]) === false) {
            throw new NotFoundHttpException('store.dynamic_form_not_found');
        }

        $resultData = $this->mobileConfigurationOutput->fromDynamicFormConfiguration($parseData, $formName);

        return new JsonResponse($resultData);
    }

    /**
     * @Route("/store/item-return-configuration", name="frontend.mobile.default.store.item_return_configuration", methods={"POST"});
     * @Route("/v1/store/item-return-configuration", name="frontend.mobile.default.store.item_return_configuration.v1", methods={"POST"});
     *
     * @return JsonResponse
     */
    public function getItemReturnConfiguration()
    {
        $data['actions'] = $this->itemReturnService->getActions();
        $data['reasons'] = $this->itemReturnService->getReasons();

        $itemReturnConfigurationOutput = $this->mobileConfigurationOutput->fromItemReturnConfiguration($data);

        return new JsonResponse($itemReturnConfigurationOutput);
    }
}
