<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Exception\NotFoundHttpException;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Request\PlatformInformation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.cms", defaults={"_format":"json"})
 */
class CmsController
{
    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @param CmsService $cmsService
     * @param PlatformInformation $platformInformation
     */
    public function __construct(CmsService $cmsService, PlatformInformation $platformInformation)
    {
        $this->cmsService = $cmsService;
        $this->platformInformation = $platformInformation;
    }

    /**
     * @Route("cms/teaser", name="frontend.mobile.cms.home.teaser", methods={"POST"})
     * @Route("v1/cms/teaser", name="frontend.mobile.cms.home.teaser.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function homeTeaserAction(): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->carousel(
                CmsService::CONTENT_NAMESPACE_HOME,
                '1',
                CmsService::CONTENT_MOBILE
            )
        );
    }

    /**
     * @Route("cms/app-version", name="frontend.mobile.cms.app.version", methods={"POST"})
     * @Route("v1/cms/app-version", name="frontend.mobile.cms.app.version.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function appVersionAction(Request $request): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->simpleText(
                CmsService::CONTENT_NAMESPACE_GLOBAL,
                $this->platformInformation->getPlatform(),
                CmsService::CONTENT_MOBILE
            )
        );
    }

    /**
     * @Route("cms/suggester-image-url", name="frontend.mobile.cms.suggester.image.url", methods={"POST"})
     * @Route("v1/cms/suggester-image-url", name="frontend.mobile.cms.suggester.image.url.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function suggesterImageUrlAction(): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->simpleText(
                CmsService::CONTENT_NAMESPACE_GLOBAL,
                'suggester-image-url',
                CmsService::CONTENT_MOBILE
            )
        );
    }

    /**
     * @Route("cms/sku-plus", name="frontend.mobile.cms.sku.plus", methods={"POST"})
     * @Route("v1/cms/sku-plus", name="frontend.mobile.cms.sku.plus.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function skuPlusAction(): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->simpleText(
                CmsService::CONTENT_NAMESPACE_GLOBAL,
                'sku-plus',
                CmsService::CONTENT_MOBILE
            )
        );
    }

    /**
     * @Route("cms/static/{key}", name="frontend.mobile.cms.static", methods={"POST"})
     * @Route("v1/cms/static/{key}", name="frontend.mobile.cms.static.v1", methods={"POST"})
     *
     * @param string $key
     *
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function staticAction(string $key): JsonResponse
    {
        $content = $this->cmsService->markdownText(
            CmsService::CONTENT_NAMESPACE_GLOBAL,
            $key,
            CmsService::CONTENT_MOBILE
        );

        if (empty($content)) {
            throw new NotFoundHttpException(sprintf('Content for %s not found.', $key));
        }

        return new JsonResponse($content);
    }

    /**
     * @Route("cms/{paymentMethod}/success-page", name="frontend.mobile.cms.payment.method.success.page", methods={"POST"})
     * @Route("v1/cms/{paymentMethod}/success-page", name="frontend.mobile.cms.payment.method.success.page.v1", methods={"POST"})
     *
     * @param string $paymentMethod
     *
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function paymentMethodSuccessPageAction(string $paymentMethod): JsonResponse
    {
        $content = $this->cmsService->markdownText(
            CmsService::CONTENT_NAMESPACE_GLOBAL,
            $paymentMethod,
            CmsService::CONTENT_MOBILE
        );

        if (empty($content)) {
            throw new NotFoundHttpException(sprintf('Content for payment method %s not found.', $paymentMethod));
        }

        return new JsonResponse($content);
    }

    /**
     * @Route("cms/{key}/chart", name="frontend.mobile.cms.chart", methods={"POST"})
     * @Route("v1/cms/{key}/chart", name="frontend.mobile.cms.chart.v1", methods={"POST"})
     *
     * @param string $key
     *
     * @return JsonResponse
     */
    public function chartAction(string $key): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->markdownText(
                CmsService::CONTENT_NAMESPACE_GLOBAL,
                $key,
                CmsService::CONTENT_MOBILE
            )
        );
    }

    /**
     * @Route("cms/category-menu", name="frontend.mobile.cms.category.menu", methods={"POST"})
     * @Route("v1/cms/category-menu", name="frontend.mobile.cms.category.menu.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function categoryMenuAction(): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->getCategoryMenu()
        );
    }

    /**
     * @Route("cms/top-menu", name="frontend.mobile.cms.top.menu", methods={"POST"})
     * @Route("v1/cms/top-menu", name="frontend.mobile.cms.top.menu.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function topMenuAction(): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->buildMobileTopMenu()
        );
    }

    /**
     * @Route("cms/page-content", name="frontend.mobile.cms.page.content", methods={"POST"})
     * @Route("v1/cms/page-content", name="frontend.mobile.cms.page.content.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function pageContentAction(Request $request): JsonResponse
    {
        return new JsonResponse(
            $this->cmsService->buildMobilePageContent(
                $request->request->get('slug', '')
            )
        );
    }
}
