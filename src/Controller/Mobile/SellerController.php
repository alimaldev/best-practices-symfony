<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Api\Output\SellerOutput;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Seller\SellerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.seller", defaults={"_format":"json"})
 */
class SellerController
{
    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @var SellerOutput
     */
    protected $sellerOutput;

    /**
     * @param SellerService $sellerService
     * @param SellerOutput $sellerOutput
     */
    public function __construct(SellerService $sellerService, SellerOutput $sellerOutput)
    {
        $this->sellerService = $sellerService;
        $this->sellerOutput = $sellerOutput;
    }

    /**
     * @Route("seller/{seller}", name="frontend.mobile.seller", methods={"POST"})
     * @Route("v1/seller/{seller}", name="frontend.mobile.seller.v1", methods={"POST"})
     *
     * @ParamConverter("seller", converter="slug", options={"resolver"="seller"})
     *
     * @param Seller $seller
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sellerAction(Request $request, Seller $seller): JsonResponse
    {
        $page = $request->request->get('page', 1);
        $pageSize = $request->request->get('pageSize', 10);

        $sellerWithSummary = $this->sellerService->get($seller->getSellerId());
        $paginatedReviews = $this->sellerService->getReviews($seller->getSellerId(), $page, $pageSize);

        $sellerOutput = $this->sellerOutput->fromReviewSeller($sellerWithSummary);
        $paginationOutput = $this->sellerOutput->fromPaginatedReviews($paginatedReviews);

        return new JsonResponse([
            'seller' => $sellerOutput,
            'pagination' => $paginationOutput,
        ]);
    }
}
