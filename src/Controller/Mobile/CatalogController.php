<?php

namespace Linio\Frontend\Controller\Mobile;

use DateTime;
use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Frontend\Api\Output\CatalogResultOutput;
use Linio\Frontend\Api\Output\CategoryTreeOutput;
use Linio\Frontend\Api\Output\ProductOutput;
use Linio\Frontend\Api\Output\SellerOutput;
use Linio\Frontend\Catalog\CampaignVoucherService;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Communication\Catalog\CatalogAware;
use Linio\Frontend\Communication\Catalog\Exception\RatingSaveFailException;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\CreateReviewForm;
use Linio\Frontend\Product\ProductAware;
use Linio\Frontend\Search\SearchService;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Seller\SellerService;
use Linio\Frontend\SlugResolver\Exception\SlugNotFoundException;
use Linio\Frontend\SlugResolver\SlugResolverAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/mapi/", service="controller.mobile.catalog", defaults={"_format":"json"})
 */
class CatalogController
{
    use CategoryAware;
    use ProductAware;
    use RouterAware;
    use SlugResolverAware;
    use TokenStorageAware;
    use FormAware;
    use FlashMessageAware;
    use CatalogAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var CampaignVoucherService
     */
    protected $campaignVoucherService;

    /**
     * @var CatalogResultOutput
     */
    protected $catalogResultOutput;

    /**
     * @var SearchService
     */
    protected $searchService;

    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @var CategoryTreeOutput
     */
    protected $categoryTreeOutput;

    /**
     * @var ProductOutput
     */
    protected $productOutput;

    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @var SellerOutput
     */
    protected $sellerOutput;

    /**
     * @param SellerOutput $sellerOutput
     */
    public function setSellerOutput(SellerOutput $sellerOutput)
    {
        $this->sellerOutput = $sellerOutput;
    }

    /**
     * @param CmsService $cmsService
     */
    public function setCmsService(CmsService $cmsService)
    {
        $this->cmsService = $cmsService;
    }

    /**
     * @param ProductOutput $productOutput
     */
    public function setProductOutput(ProductOutput $productOutput)
    {
        $this->productOutput = $productOutput;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param CampaignVoucherService $campaignVoucherService
     */
    public function setCampaignVoucherService(CampaignVoucherService $campaignVoucherService)
    {
        $this->campaignVoucherService = $campaignVoucherService;
    }

    /**
     * @param CatalogResultOutput $catalogResultOutput
     */
    public function setCatalogResultOutput(CatalogResultOutput $catalogResultOutput)
    {
        $this->catalogResultOutput = $catalogResultOutput;
    }

    /**
     * @param SearchService $searchService
     */
    public function setSearchService(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * @param SellerService $sellerService
     */
    public function setSellerService(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * @param CategoryTreeOutput $categoryTreeOutput
     */
    public function setCategoryTreeOutput(CategoryTreeOutput $categoryTreeOutput)
    {
        $this->categoryTreeOutput = $categoryTreeOutput;
    }

    /**
     * @Route("p/{productSlug}", name="frontend.mobile.catalog.detail", requirements={"productSlug"="^[0-9a-zA-Z\-]+"}, methods={"POST"})
     * @Route("v1/p/{productSlug}", name="frontend.mobile.catalog.detail.v1", requirements={"productSlug"="^[0-9a-zA-Z\-]+"}, methods={"POST"})
     *
     * @param Request $request
     * @param $productSlug
     *
     * @return JsonResponse
     */
    public function detailAction(Request $request, $productSlug)
    {
        try {
            /** @var Product $product */
            $product = $this->slugResolverService->resolve('product', $productSlug);
        } catch (SlugNotFoundException $exception) {
            throw new NotFoundHttpException('catalog.query_not_found');
        }

        if ($product->getMarketplaceParentSku()) {
            $parent = $this->productService->getBySku($product->getMarketplaceParentSku());

            return $this->detailAction($request, $parent->getSlug());
        }

        if ($product->getSlug() !== $productSlug) {
            return $this->detailAction($request, $product->getSlug());
        }

        try {
            $campaign = $this->slugResolverService->resolve('campaign', $request->request->get('campaign', ''));
            $product->filterCampaignProducts($campaign);
            $this->campaignVoucherService->mapVouchersFromCampaign($product);
        } catch (SlugNotFoundException $exception) {
            // Do Nothing
        }

        return new JsonResponse($this->productOutput->fromProduct($product));
    }

    /**
     * @Route("search", name="frontend.mobile.catalog.search", methods={"POST"})
     * @Route("v1/search", name="frontend.mobile.catalog.search.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchAction(Request $request)
    {
        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);

        if ($request->request->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $request->request->get('seller'));

            if ($seller) {
                $request->request->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $searchResults = $this->searchService->searchProducts($request->request, $categoryTree);
        $categoryTree->sortByCount();

        return new JsonResponse([
            'categoryTree' => $this->categoryTreeOutput->fromCategory($categoryTree),
            'searchResult' => $this->catalogResultOutput->fromSearchResults($searchResults),
        ]);
    }

    /**
     * @Route("c/{category}/b/{brand}", name="frontend.mobile.catalog.category_brand", requirements={"category"="^.+", "brand"="^.+"}, methods={"POST"})
     * @Route("v1/c/{category}/b/{brand}", name="frontend.mobile.catalog.category_brand.v1", requirements={"category"="^.+", "brand"="^.+"}, methods={"POST"})
     * @ParamConverter("category", converter="slug", options={"resolver"="category"})
     * @ParamConverter("brand", converter="slug", options={"resolver"="brand"})
     *
     * @param Request $request
     * @param Category $category
     * @param Brand $brand
     *
     * @return JsonResponse
     */
    public function categoryBrandAction(Request $request, Category $category, Brand $brand)
    {
        $requestParameters = clone $request->request;
        $formattedSearchResult = [];

        $requestParameters->add(['category' => $category->getId()]);
        $requestParameters->add(['brand' => $brand->getId()]);

        if ($requestParameters->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestParameters->get('seller'));

            if ($seller) {
                $requestParameters->add(['seller.id' => (string) $seller->getSellerId()]);
            }
        }

        $categoryTree = $this->categoryService->buildTree($category->getId());

        $searchResult = $requestParameters->get('searchResults', false);

        if ($searchResult) {
            $searchResults = $this->searchService->searchProducts($requestParameters, $categoryTree);
            $formattedSearchResult = $this->catalogResultOutput->fromSearchResults($searchResults);
        }

        return new JsonResponse([
            'searchResult' => $formattedSearchResult,
            'categoryTree' => $this->categoryTreeOutput->fromCategory($categoryTree),
            'catalogTitle' => sprintf('%s / %s', $category->getName(), $brand->getName()),
        ]);
    }

    /**
     * @Route("c/{category}", name="frontend.mobile.catalog.category", requirements={"category"="^.+"}, methods={"POST"})
     * @Route("v1/c/{category}", name="frontend.mobile.catalog.category.v1", requirements={"category"="^.+"}, methods={"POST"})
     * @ParamConverter("category", converter="slug", options={"resolver"="category"})
     *
     * @param Request $request
     * @param Category $category
     *
     * @return JsonResponse
     */
    public function categoryAction(Request $request, Category $category)
    {
        $requestParameters = clone $request->request;
        $formattedSearchResult = [];

        if ($category->getId() != CategoryService::ROOT_CATEGORY) {
            $requestParameters->add(['category' => $category->getId()]);
        }

        if ($requestParameters->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestParameters->get('seller'));

            if ($seller) {
                $requestParameters->add(['seller.id' => (string) $seller->getSellerId()]);
            }
        }

        $categoryTree = $this->categoryService->buildTree($category->getId());

        $searchResultRequired = $requestParameters->get('searchResults', false);

        if ($searchResultRequired) {
            $searchResults = $this->searchService->searchProducts($requestParameters, $categoryTree);
            $formattedSearchResult = $this->catalogResultOutput->fromSearchResults($searchResults);
        }

        if (($request->request->count() == 1 && $request->request->has('searchResults')) || $request->request->count() == 0) {
            $pageContent = $this->cmsService->buildMobilePageContent((string) $category->getId(), CmsService::CONTENT_NAMESPACE_CATEGORY);
        }

        return new JsonResponse([
            'searchResult' => $formattedSearchResult,
            'categoryTree' => $this->categoryTreeOutput->fromCategory($categoryTree),
            'catalogTitle' => $category->getName(),
            'pageContent' => $pageContent ?? null,
        ]);
    }

    /**
     * @Route("add-review/{productSlug}", name="frontend.mobile.catalog.product.review", requirements={"productSlug"="^[0-9a-zA-Z\-]+"}, methods={"POST"})
     * @Route("v1/add-review/{productSlug}", name="frontend.mobile.catalog.product.review.v1", requirements={"productSlug"="^[0-9a-zA-Z\-]+"}, methods={"POST"})
     *
     * @param Request $request
     * @param $productSlug
     *
     * @return JsonResponse
     */
    public function addReviewAction(Request $request, $productSlug)
    {
        $customer = $this->getCustomer();

        try {
            /** @var Product $product */
            $product = $this->slugResolverService->resolve('product', $productSlug);
        } catch (SlugNotFoundException $exception) {
            throw new NotFoundHttpException('cart.product_not_found');
        }

        $reviewForm = $this->createForm(new CreateReviewForm(), new Review(), ['csrf_protection' => false]);
        $reviewForm->submit($request->request->all());

        if (!$reviewForm->isValid()) {
            throw new FormValidationException($reviewForm, 'reviews.create_failed');
        }

        /* @var Review $review */
        $review = $reviewForm->getData();
        $review->setSku($product->getSku());
        $review->setCreatedAt(new DateTime());

        if (!$customer instanceof GuestUser) {
            $review->setCustomerName(sprintf('%s %s', $customer->getFirstName(), $customer->getLastName()));
        }

        try {
            $this->catalogService->setRating($customer, $review);
        } catch (RatingSaveFailException $exception) {
            throw new InputException('reviews.create_failed', [], 0, $exception);
        }

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("cm/{campaign}", name="frontend.mobile.catalog.campaign", requirements={"campaign"="^.+"}, methods={"POST"})
     * @Route("v1/cm/{campaign}", name="frontend.mobile.catalog.campaign.v1", requirements={"campaign"="^.+"}, methods={"POST"})
     * @ParamConverter("campaign", converter="slug", options={"resolver"="campaign"})
     *
     * @param Request $request
     * @param Campaign $campaign
     *
     * @return JsonResponse
     */
    public function campaignAction(Request $request, Campaign $campaign)
    {
        $requestQuery = clone $request->request;

        $requestQuery->add(['campaign_skus' => $campaign->getSkus()]);

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $onlySearchResult = $request->request->get('searchResults', false);

        $searchResult = null;

        if ($onlySearchResult) {
            $searchResults = $this->searchService->getCampaignProducts($requestQuery, $campaign);
            $campaign->setCount($searchResults->getPrimarySearchResult()->getTotalItemsFound());
            $searchResult = $this->catalogResultOutput->fromSearchResults($searchResults);
        }

        $catalogTitle = $campaign->getName();

        if ($campaign->getCurrentSegment()) {
            $catalogTitle = sprintf('%s / %s', $campaign->getName(), $campaign->getCurrentSegment()->getName());
        }

        return new JsonResponse([
            'searchResult' => $searchResult,
            'categoryTree' => $this->categoryTreeOutput->fromCampaign($campaign),
            'catalogTitle' => $catalogTitle,
        ]);
    }

    /**
     * @Route("s/{seller}", name="frontend.mobile.catalog.seller", requirements={"seller"="^.+"},  methods={"POST"})
     * @Route("v1/s/{seller}", name="frontend.mobile.catalog.seller.v1", requirements={"seller"="^.+"},  methods={"POST"})
     * @ParamConverter("seller", converter="slug", options={"resolver"="seller"})
     *
     * @param Request $request
     * @param Seller $seller
     *
     * @return JsonResponse
     */
    public function sellerAction(Request $request, Seller $seller)
    {
        $requestQuery = clone $request->request;
        $formattedSearchResult = [];

        $requestQuery->add(['seller.id' => (string) $seller->getSellerId()]);
        $seller = $this->sellerService->get($seller->getSellerId());

        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);
        $searchResultRequired = $requestQuery->get('searchResults', false);

        if ($searchResultRequired) {
            $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
            $formattedSearchResult = $this->catalogResultOutput->fromSearchResults($searchResults);
        }

        return new JsonResponse([
            'searchResult' => $formattedSearchResult,
            'categoryTree' => $this->categoryTreeOutput->fromCategory($categoryTree),
            'catalogTitle' => $seller->getName(),
            'seller' => $this->sellerOutput->fromReviewSeller($seller),
        ]);
    }

    /**
     * @Route("b/{brandSlug}", name="frontend.mobile.catalog.index.brand", requirements={"brandSlug"="^.+"}, methods={"POST"})
     * @Route("v1/b/{brandSlug}", name="frontend.mobile.catalog.index.brand.v1", requirements={"brandSlug"="^.+"}, methods={"POST"})
     * @ParamConverter("brandSlug", converter="slug", options={"resolver"="brand"})
     *
     * @param Request $request
     * @param Brand $brandSlug
     *
     * @return JsonResponse
     */
    public function brandAction(Request $request, Brand $brandSlug)
    {
        $formattedSearchResult = [];

        $requestQuery = clone $request->request;
        $requestQuery->add(['brand' => $brandSlug->getId()]);

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);
        $searchResultRequired = $requestQuery->get('searchResults', false);

        if ($searchResultRequired) {
            $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
            $formattedSearchResult = $this->catalogResultOutput->fromSearchResults($searchResults);
        }

        return new JsonResponse([
            'searchResult' => $formattedSearchResult,
            'categoryTree' => $this->categoryTreeOutput->fromCategory($categoryTree),
            'catalogTitle' => $brandSlug->getName(),
        ]);
    }
}
