<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Form\ForgotPasswordForm;
use Linio\Frontend\Security\Jwt\TokenManager;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/mapi/", service="controller.mobile.security", defaults={"_format":"json"})
 */
class SecurityController
{
    use FormAware;
    use RouterAware;
    use CustomerAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param TokenManager $tokenManager
     */
    public function setTokenManager(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * @Route("auth/login", name="frontend.mobile_api.login")
     * @Route("v1/auth/login", name="frontend.mobile_api.login.v1")
     */
    public function loginAction()
    {
        throw new RuntimeException(
            'You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.'
        );
    }

    /**
     * @Route("oauth/login", name="frontend.mobile_api.oauth_login")
     * @Route("v1/oauth/login", name="frontend.mobile_api.oauth_login.v1")
     */
    public function oauthLoginAction()
    {
        throw new RuntimeException(
            'You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.'
        );
    }

    /**
     * @Route("auth/forgotten", name="frontend.mobile.auth.forgotten", methods={"POST"})
     * @Route("v1/auth/forgotten", name="frontend.mobile.auth.forgotten.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function forgottenAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(ForgotPasswordForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form, 'customer.forget_password_error');
        }

        $this->customerService->requestPasswordReset(
            $form->getData()['email'],
            $this->generateUrl('frontend.customer.password.recover', [], UrlGeneratorInterface::ABSOLUTE_URL)
        );

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("auth/logout", name="frontend.mobile.auth.logout")
     * @Route("v1/auth/logout", name="frontend.mobile.auth.logout.v1")
     */
    public function logoutAction()
    {
        // TODO: Add back the exception once we can get rid of the v1 route.
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
