<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Linio\Controller\FormAware;
use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Frontend\Api\CustomerTransputAware;
use Linio\Frontend\Api\Input\CustomerInput;
use Linio\Frontend\Api\Output\CustomerOrderOutput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\AccountChangePasswordForm;
use Linio\Frontend\Order\BuildOrderAware;
use Linio\Frontend\Security\AuthenticationToken;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Security\User;
use Linio\Frontend\Seller\SellerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/mapi/", service="controller.mobile.customer", defaults={"_format":"json"})
 */
class CustomerController
{
    use DynamicFormAware;
    use CustomerAware;
    use TokenStorageAware;
    use FormAware;
    use BuildOrderAware;
    use CustomerTransputAware;
    use SearchOrderAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var AuthenticationSuccessHandler
     */
    protected $authenticationSuccessHandler;

    /**
     * @var bool
     */
    protected $linioPlusEnabled;

    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @var bool
     */
    protected $sellerReviewsEnabled = false;

    /**
     * @var bool
     */
    protected $bankTransferConfirmationFormEnabled = false;

    /**
     * @var CustomerInput
     */
    protected $customerInput;

    /**
     * @var CustomerOrderOutput
     */
    protected $customerOrderOutput;

    /**
     * @param CustomerOrderOutput $customerOrderOutput
     */
    public function setCustomerOrderOutput(CustomerOrderOutput $customerOrderOutput)
    {
        $this->customerOrderOutput = $customerOrderOutput;
    }

    /**
     * @param CustomerInput $customerInput
     */
    public function setCustomerInput(CustomerInput $customerInput)
    {
        $this->customerInput = $customerInput;
    }

    /**
     * @param bool $bankTransferConfirmationFormEnabled
     */
    public function setBankTransferConfirmationFormEnabled(bool $bankTransferConfirmationFormEnabled)
    {
        $this->bankTransferConfirmationFormEnabled = $bankTransferConfirmationFormEnabled;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param AuthenticationSuccessHandler $authenticationSuccessHandler
     */
    public function setAuthenticationSuccessHandler(AuthenticationSuccessHandler $authenticationSuccessHandler)
    {
        $this->authenticationSuccessHandler = $authenticationSuccessHandler;
    }

    /**
     * @param bool $linioPlusEnabled
     */
    public function setLinioPlusEnabled(bool $linioPlusEnabled)
    {
        $this->linioPlusEnabled = $linioPlusEnabled;
    }

    /**
     * @param SellerService $sellerService
     */
    public function setSellerService(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * @param bool $sellerReviewsEnabled
     */
    public function setSellerReviewsEnabled(bool $sellerReviewsEnabled)
    {
        $this->sellerReviewsEnabled = $sellerReviewsEnabled;
    }

    /**
     * @Route("customer/profile", name="frontend.mobile.customer.profile", methods={"POST"})
     * @Route("v1/customer/profile", name="frontend.mobile.customer.profile.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function customerAction()
    {
        $customer = $this->getCustomer();
        $customerProfile = $this->customerOutput->fromCustomer($customer);

        return new JsonResponse($customerProfile);
    }

    /**
     * @Route("customer", name="frontend.mobile.customer.create", methods={"POST"})
     * @Route("v1/customer", name="frontend.mobile.customer.create.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $customer = new User();

        $form = $this->dynamicFormFactory->createForm('registration', $customer, ['csrf_protection' => false]);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new FormValidationException($form, 'customer.registration.error');
        }

        $customer->setIpAddress($request->getClientIp());

        $this->customerService->create($customer);

        $token = new AuthenticationToken($customer, $customer->getPassword(), 'default');

        $response = $this->authenticationSuccessHandler->onAuthenticationSuccess($request, $token);

        $this->buildOrder->mergeItems($this->getCustomer()->getLinioId(), $token->getUser());

        $this->tokenStorage->setToken($token);

        return $response;
    }

    /**
     * @Route("customer/update-profile", name="frontend.mobile.customer.profile.edit", methods={"POST"})
     * @Route("v1/customer/update-profile", name="frontend.mobile.customer.profile.edit.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function editProfileAction(Request $request)
    {
        /* @var User $customer */
        $customer = clone $this->getCustomer();

        $form = $this->dynamicFormFactory->createForm('profile_update', $customer, ['csrf_protection' => false]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form, 'customer.registration.error');
        }

        $customer->setIpAddress($request->getClientIp());

        $this->customerService->update($customer);
        $this->updateCustomer($customer);

        $success = $this->translator->trans('profile.edit.save.success');

        return new JsonResponse($success);
    }

    /**
     * @Route("customer/change-password", name="frontend.mobile.customer.change-password", methods={"POST"})
     * @Route("v1/customer/change-password", name="frontend.mobile.customer.change-password.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     *
     * @return JsonResponse
     */
    public function changePasswordAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(AccountChangePasswordForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form, ExceptionMessage::CUSTOMER_CHANGE_PASSWORD_ERROR);
        }

        $this->customerService->changePassword(
            $this->getCustomer(),
            $form->get('current')->getNormData(),
            $form->get('new_password')->getNormData()
        );

        return new JsonResponse();
    }

    /**
     * @Route("customer/plus-membership", name="frontend.mobile.customer.plus-membership", methods={"POST"})
     * @Route("v1/customer/plus-membership", name="frontend.mobile.customer.plus-membership.v1", methods={"POST"})
     *
     * @throws InputException
     *
     * @return JsonResponse
     */
    public function plusAction(): JsonResponse
    {
        if (!$this->linioPlusEnabled) {
            throw new InputException(ExceptionMessage::PLUS_MEMBERSHIP_UNAVAILABLE);
        }

        $plusMembership = $this->customerService->getLinioPlusMembership($this->getCustomer());

        return new JsonResponse($this->customerOutput->fromPlusMembership($plusMembership));
    }

    /**
     * @Route("customer/reviews", name="frontend.mobile.customer.reviews", methods={"POST"})
     * @Route("v1/customer/reviews", name="frontend.mobile.customer.reviews.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function reviewsAction(Request $request): JsonResponse
    {
        $page = $request->request->get('page', 1);
        $pageSize = $request->request->get('pageSize', 10);

        if (!$this->sellerReviewsEnabled) {
            throw new InputException(ExceptionMessage::FEATURE_NOT_AVAILABLE);
        }

        $paginatedCustomerReviews = $this->sellerService->findCustomerReviews($this->getCustomer(), $page, $pageSize);

        return new JsonResponse($this->customerOutput->fromPaginatedCustomerReviews($paginatedCustomerReviews));
    }

    /**
     * @Route("customer/bank-transfer-orders-pending-list", name="frontend.mobile.customer.bank.transfer.orders.pending.list", methods={"POST"})
     *
     * @throws InputException
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function bankTransferOrdersPendingListAction(): JsonResponse
    {
        if (!$this->bankTransferConfirmationFormEnabled) {
            throw new InputException(ExceptionMessage::FEATURE_NOT_AVAILABLE);
        }

        $orders = $this->searchOrder->findOrdersPendingBankConfirmation($this->getCustomer());

        $output = $this->customerOrderOutput->fromPendingOrders($orders);

        return new JsonResponse($output);
    }

    /**
     * @Route("customer/bank-transfer-confirmation", name="frontend.mobile.customer.bank.transfer.confirmation", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function bankTransferConfirmationAction(Request $request): JsonResponse
    {
        if (!$this->bankTransferConfirmationFormEnabled) {
            throw new InputException(ExceptionMessage::FEATURE_NOT_AVAILABLE);
        }

        $form = $this->dynamicFormFactory->createForm('bank_transfer_confirmation', null, ['csrf_protection' => false]);

        $bankTransferData = $request->request->get('bank_transfer_confirmation');
        $orders = $bankTransferData['orders'];

        $choices = [];

        foreach ($orders as $order) {
            $choices[$order] = $order;
        }

        $form->add('orders', ChoiceType::class, [
            'choices' => $choices,
            'label' => $this->translator->trans('account.bank_transfer_confirmation'),
            'expanded' => true,
            'multiple' => true,
            'required' => true,
        ]);

        $form->handleRequest($request);

        if (!$form->isValid()) {
            throw new FormValidationException($form, ExceptionMessage::INVALID_REQUEST);
        }

        $data = $form->getData();

        $customer = $this->getCustomer();
        $bankTransferConfirmation = $this->customerInput->fromConfirmBankTransfer($data);
        $bankTransferConfirmation->setCustomer($customer);

        foreach ($data['orders'] as $orderId) {
            $order = new Order($orderId);
            $bankTransferConfirmation->addOrder($order);
        }

        $this->customerService->createBankTransferConfirmation($bankTransferConfirmation);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
