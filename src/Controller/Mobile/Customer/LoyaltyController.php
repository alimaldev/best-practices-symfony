<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Input\LoyaltyProgramInput;
use Linio\Frontend\Api\Input\MobileLoyaltyProgram;
use Linio\Frontend\Api\Output\CustomerOrderOutput;
use Linio\Frontend\Api\Output\CustomerOutput;
use Linio\Frontend\Api\Output\MobileCustomerOrder;
use Linio\Frontend\Customer\Membership\ManageMembershipsAware;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.customer.loyalty", defaults={"_format":"json"})
 */
class LoyaltyController
{
    use ManageMembershipsAware;
    use TokenStorageAware;

    /**
     * @var MobileCustomerOrder
     */
    protected $customerOrderOutput;

    /**
     * @var MobileLoyaltyProgram
     */
    protected $loyaltyInput;

    /**
     * @var CustomerOutput
     */
    protected $customerOutput;

    /**
     * @param LoyaltyProgramInput $loyaltyInput
     */
    public function setLoyaltyInput(LoyaltyProgramInput $loyaltyInput)
    {
        $this->loyaltyInput = $loyaltyInput;
    }

    /**
     * @param CustomerOrderOutput $customerOrderOutput
     */
    public function setCustomerOrderOutput(CustomerOrderOutput $customerOrderOutput)
    {
        $this->customerOrderOutput = $customerOrderOutput;
    }

    /**
     * @param CustomerOutput $customerOutput
     */
    public function setCustomerOutput(CustomerOutput $customerOutput)
    {
        $this->customerOutput = $customerOutput;
    }

    /**
     * @Route("customer/loyalty/set-account", name="frontend.mobile.customer.loyalty.set_account", methods={"POST"})
     * @Route("v1/customer/loyalty/set-account", name="frontend.mobile.customer.loyalty.set_account.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function setLoyaltyAccountAction(Request $request): JsonResponse
    {
        $customer = $this->getCustomer();

        $loyaltyProgram = $this->loyaltyInput->fromLoyaltyProgram($request->request->all());

        $this->manageMemberships->updateLoyaltyProgram($customer, $loyaltyProgram);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/loyalty/information", name="frontend.mobile.customer.loyalty.get_information", methods={"POST"})
     * @Route("v1/customer/loyalty/information", name="frontend.mobile.customer.loyalty.get_information.v1", methods={"POST"})
     *
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function getLoyaltyInformationAction(): JsonResponse
    {
        $customer = $this->getCustomer();

        $loyaltyProgram = $this->manageMemberships->getLoyaltyProgram($customer);

        return new JsonResponse(
            $this->customerOutput->fromLoyaltyProgram($loyaltyProgram)
        );
    }

    /**
     * @Route("customer/loyalty/orders", name="frontend.mobile.customer.loyalty.get_orders", methods={"POST"})
     * @Route("v1/customer/loyalty/orders", name="frontend.mobile.customer.loyalty.get_orders.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function getLoyaltyOrdersAction(): JsonResponse
    {
    }
}
