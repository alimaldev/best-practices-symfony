<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Exception\NotFoundHttpException;
use Linio\Frontend\Api\Input\AddressInput;
use Linio\Frontend\Api\Input\ItemReturnInput;
use Linio\Frontend\Api\Input\MobileCustomerOrderSearch;
use Linio\Frontend\Api\Output\CustomerOrderOutput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.customer.order", defaults={"_format":"json"})
 */
class OrderController
{
    use CustomerAware;
    use TokenStorageAware;
    use SearchOrderAware;

    /**
     * @var MobileCustomerOrderSearch
     */
    protected $customerOrderSearchInput;

    /**
     * @var CustomerOrderOutput
     */
    protected $customerOrderOutput;

    /**
     * @var AddressInput
     */
    protected $addressInput;

    /**
     * @var ItemReturnInput
     */
    protected $itemReturnInput;

    /**
     * @var bool
     */
    protected $returnItemEnabled = false;

    /**
     * @param MobileCustomerOrderSearch $customerOrderSearchInput
     */
    public function setCustomerOrderSearchInput(MobileCustomerOrderSearch $customerOrderSearchInput)
    {
        $this->customerOrderSearchInput = $customerOrderSearchInput;
    }

    /**
     * @param CustomerOrderOutput $customerOrderOutput
     */
    public function setCustomerOrderOutput(CustomerOrderOutput $customerOrderOutput)
    {
        $this->customerOrderOutput = $customerOrderOutput;
    }

    /**
     * @param AddressInput $addressInput
     */
    public function setAddressInput(AddressInput $addressInput)
    {
        $this->addressInput = $addressInput;
    }

    /**
     * @param ItemReturnInput $itemReturnInput
     */
    public function setItemReturnInput(ItemReturnInput $itemReturnInput)
    {
        $this->itemReturnInput = $itemReturnInput;
    }

    /**
     * @param bool $returnItemEnabled
     */
    public function setReturnItemEnabled(bool $returnItemEnabled)
    {
        $this->returnItemEnabled = $returnItemEnabled;
    }

    /**
     * @Route("customer/order/pending", name="frontend.mobile.customer.order.pending", methods={"POST"})
     * @Route("v1/customer/order/pending", name="frontend.mobile.customer.order.pending.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function orderPendingAction(): JsonResponse
    {
        $orders = $this->searchOrder->findOrdersPendingBankConfirmation($this->getCustomer());

        return new JsonResponse($this->customerOrderOutput->fromPendingOrders($orders));
    }

    /**
     * @Route("customer/order", name="frontend.mobile.customer.order", methods={"POST"})
     * @Route("v1/customer/order", name="frontend.mobile.customer.order.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function orderAction(Request $request): JsonResponse
    {
        $search = $this->customerOrderSearchInput->fromCustomerOrderSearch($request->request->all());

        $paginatedResult = $this->searchOrder->findOrders($this->getCustomer(), $search);
        $orders = $this->customerOrderOutput->fromPaginatedOrderList($paginatedResult);

        return new JsonResponse($orders);
    }

    /**
     * @Route("customer/order/item-return", name="frontend.mobile.customer.order.return", methods={"POST"})
     * @Route("v1/customer/order/item-return", name="frontend.mobile.customer.order.return.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     * @throws OrderException When only some of the requested quantity could be returned
     *
     * @return JsonResponse
     */
    public function ItemReturnAction(Request $request): JsonResponse
    {
        if (!$this->returnItemEnabled) {
            throw new InputException(ExceptionMessage::ITEM_RETURN_UNAVAILABLE);
        }

        $returnRequest = $this->itemReturnInput->fromItemReturn($request->request->all());

        $order = $this->searchOrder->findOrder($this->getCustomer(), $returnRequest['orderNumber']);

        $this->customerService->createItemReturnRequest($this->getCustomer(), $order, $returnRequest);

        return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/order/request-invoice", name="frontend.mobile.customer.order.invoice", methods={"POST"})
     * @Route("v1/customer/order/request-invoice", name="frontend.mobile.customer.order.invoice.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function requestInvoiceAction(Request $request): JsonResponse
    {
        $billingAddress = $this->addressInput->fromPartialAddress($request->request->get('customerAddress'));

        $orderId = $request->request->get('orderId');

        $invoiceResult = $this->customerService->requestInvoice($orderId, $billingAddress);
        $this->getCustomer()->setInvoices($invoiceResult->getInvoices());

        return new JsonResponse($invoiceResult->getFolios());
    }

    /**
     * @Route("customer/order/{orderNumber}", name="frontend.mobile.customer.order.detail", methods={"POST"})
     * @Route("v1/customer/order/{orderNumber}", name="frontend.mobile.customer.order.detail.v1", methods={"POST"})
     *
     * @param string $orderNumber
     *
     * @throws NotFoundHttpException
     * @throws OrderException
     *
     * @return JsonResponse
     */
    public function orderDetailsAction(string $orderNumber): JsonResponse
    {
        try {
            $order = $this->searchOrder->findOrder($this->getCustomer(), $orderNumber);
        } catch (OrderException $exception) {
            if ($exception->getMessage() == ExceptionMessage::INVALID_REQUEST) {
                throw new NotFoundHttpException(ExceptionMessage::CUSTOMER_ORDER_NOT_FOUND);
            }

            throw $exception;
        }

        $outputOrder = $this->customerOrderOutput->fromOrder($order);

        return new JsonResponse($outputOrder);
    }

    /**
     * @Route("customer/order/{orderNumber}/tracking", name="frontend.mobile.customer.order.tracking", methods={"POST"})
     * @Route("v1/customer/order/{orderNumber}/tracking", name="frontend.mobile.customer.order.tracking.v1", methods={"POST"})
     *
     * @param string $orderNumber
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function orderTrackingAction(string $orderNumber): JsonResponse
    {
        $shipments = $this->searchOrder->findOrderTrackingHistory($this->getCustomer(), $orderNumber);

        return new JsonResponse($this->customerOrderOutput->fromShipments($shipments));
    }
}
