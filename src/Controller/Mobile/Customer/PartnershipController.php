<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Input\PartnershipInput;
use Linio\Frontend\Api\Output\PartnershipOutput;
use Linio\Frontend\Customer\Membership\ManageMemberships;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.customer.partnership", defaults={"_format":"json"})
 */
class PartnershipController
{
    use TokenStorageAware;

    /**
     * @var PartnershipInput
     */
    protected $partnershipInput;

    /**
     * @var ManageMemberships
     */
    protected $manageMemberships;

    /**
     * @var PartnershipOutput
     */
    protected $partnershipOutput;

    /**
     * PartnershipController constructor.
     *
     * @param PartnershipInput $partnershipInput
     * @param ManageMemberships $manageMemberships
     * @param PartnershipOutput $partnershipOutput
     */
    public function __construct(PartnershipInput $partnershipInput, ManageMemberships $manageMemberships, PartnershipOutput $partnershipOutput)
    {
        $this->partnershipInput = $partnershipInput;
        $this->partnershipOutput = $partnershipOutput;
        $this->manageMemberships = $manageMemberships;
    }

    /**
     * @Route("customer/partnership/signup", name="frontend.mobile.customer.partnership.sign_up", methods={"POST"})
     * @Route("v1/customer/partnership/signup", name="frontend.mobile.customer.partnership.sign_up.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function signUpPartnershipAction(Request $request): JsonResponse
    {
        $partnership = $this->partnershipInput->fromPartnershipData($request->request->all());

        $this->manageMemberships->signupForPartnership($this->getCustomer(), $partnership);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/partnership", name="frontend.mobile.customer.partnership", methods={"POST"})
     * @Route("v1/customer/partnership", name="frontend.mobile.customer.partnership.v1", methods={"POST"})
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function getPartnershipAction(): JsonResponse
    {
        $partnerships = $this->manageMemberships->getPartnerships($this->getCustomer());

        return new JsonResponse($this->partnershipOutput->fromPartnerships($partnerships));
    }

    /**
     * @Route("customer/partnership/update", name="frontend.mobile.customer.partnership.update", methods={"POST"})
     * @Route("v1/customer/partnership/update", name="frontend.mobile.customer.partnership.update.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function updatePartnershipAction(Request $request): JsonResponse
    {
        $partnership = $this->partnershipInput->fromPartnershipData($request->request->all());

        $this->manageMemberships->updatePartnership($this->getCustomer(), $partnership);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/partnership/{partnershipCode}/remove", name="frontend.mobile.customer.partnership.remove", methods={"POST"})
     * @Route("v1/customer/partnership/{partnershipCode}/remove", name="frontend.mobile.customer.partnership.remove.v1", methods={"POST"})
     *
     * @param string $partnershipCode
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function removePartnershipAction(string $partnershipCode): JsonResponse
    {
        $this->manageMemberships->removePartnership($this->getCustomer(), $partnershipCode);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
