<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Input\SellerInput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\SearchOrder;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.seller.review", defaults={"_format":"json"})
 */
class SellerReviewController
{
    use CustomerAware;
    use TokenStorageAware;

    /**
     * @var SearchOrder
     */
    protected $searchOrder;

    /**
     * @var SellerInput
     */
    protected $sellerInput;

    /**
     * SellerReviewController constructor.
     *
     * @param SearchOrder $searchOrder
     * @param SellerInput $sellerInput
     */
    public function __construct(SearchOrder $searchOrder, SellerInput $sellerInput)
    {
        $this->searchOrder = $searchOrder;
        $this->sellerInput = $sellerInput;
    }

    /**
     * @Route("seller/review", name="frontend.mobile.customer.review.seller", methods={"POST"})
     * @Route("v1/seller/review", name="frontend.mobile.customer.review.seller.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws OrderException Thrown for client errors
     * @throws OrderException Thrown for server errors
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return JsonResponse
     */
    public function reviewSellerAction(Request $request)
    {
        $customer = $this->getCustomer();

        $review = $this->sellerInput->toSellerReview($request->request->all());

        $order = $this->searchOrder->findOrder($customer, $request->request->get('orderNumber'));

        $this->customerService->reviewSeller($customer, $review, $order);

        return new JsonResponse([], JsonResponse::HTTP_CREATED);
    }
}
