<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Input\StoredCreditCardInput;
use Linio\Frontend\Api\Output\StoredCreditCardOutput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.customer.stored_card", defaults={"_format":"json"})
 */
class StoredCardController
{
    use CustomerAware;
    use TokenStorageAware;

    /**
     * @var bool
     */
    protected $storeCards;

    /**
     * @var StoredCreditCardInput
     */
    protected $storedCreditCardInput;

    /**
     * @var StoredCreditCardOutput
     */
    protected $storedCreditCardOutput;

    /**
     * @param StoredCreditCardInput $storedCreditCardInput
     */
    public function setStoredCreditCardInput(StoredCreditCardInput $storedCreditCardInput)
    {
        $this->storedCreditCardInput = $storedCreditCardInput;
    }

    /**
     * @param StoredCreditCardOutput $storedCreditCardOutput
     */
    public function setStoredCreditCardOutput(StoredCreditCardOutput $storedCreditCardOutput)
    {
        $this->storedCreditCardOutput = $storedCreditCardOutput;
    }

    /**
     * @param bool $storeCards
     */
    public function setStoreCards(bool $storeCards)
    {
        $this->storeCards = $storeCards;
    }

    /**
     * @Route("customer/credit-cards", name="frontend.mobile.customer.stored_card.get", methods={"POST"})
     * @Route("v1/customer/credit-cards", name="frontend.mobile.customer.stored_card.get.v1", methods={"POST"})
     *
     * @throws InputException
     *
     * @return JsonResponse
     */
    public function getCardsAction(): JsonResponse
    {
        if (!$this->storeCards) {
            throw new InputException(ExceptionMessage::FEATURE_NOT_AVAILABLE);
        }

        $cards = $this->customerService->getCreditCards($this->getCustomer());

        return new JsonResponse(
            $this->storedCreditCardOutput->fromCreditCards($cards)
        );
    }

    /**
     * @Route("customer/add-credit-card", name="frontend.mobile.customer.stored_card.add", methods={"POST"})
     * @Route("v1/customer/add-credit-card", name="frontend.mobile.customer.stored_card.add.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException
     * @throws FormValidationException
     *
     * @return JsonResponse
     */
    public function addCreditCardAction(Request $request): JsonResponse
    {
        if (!$this->storeCards) {
            throw new InputException(ExceptionMessage::FEATURE_NOT_AVAILABLE);
        }

        $creditCard = $this->storedCreditCardInput->fromCreditCard($request->request->all());

        $customer = $this->getCustomer();

        $this->customerService->addCreditCard($creditCard, $customer);

        return $this->getCardsAction();
    }

    /**
     * @Route("customer/remove-card/{cardId}", name="frontend.mobile.customer.stored_card.remove", requirements={"cardId":"\d+"})", methods={"POST"})
     * @Route("v1/customer/remove-card/{cardId}", name="frontend.mobile.customer.stored_card.remove.v1", requirements={"cardId":"\d+"})", methods={"POST"})
     *
     * @param int $cardId
     *
     * @throws InputException
     *
     * @return JsonResponse
     */
    public function removeCreditCardAction(int $cardId): JsonResponse
    {
        if (!$this->storeCards) {
            throw new InputException(ExceptionMessage::FEATURE_NOT_AVAILABLE);
        }

        $customer = $this->getCustomer();
        $this->customerService->removeCreditCard($cardId, $customer);

        return $this->getCardsAction();
    }
}
