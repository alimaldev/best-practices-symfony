<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Input\WishListInput;
use Linio\Frontend\Api\Input\WishListProductInput;
use Linio\Frontend\Api\Output\WishListOutput;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\WishList\Exception\InvalidWishListException;
use Linio\Frontend\WishList\Exception\WishListException;
use Linio\Frontend\WishList\Exception\WishListNotFoundException;
use Linio\Frontend\WishList\ManageWishList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.customer.wish_list", defaults={"_format":"json"})
 */
class WishListController
{
    use TokenStorageAware;

    /**
     * @var ManageWishList
     */
    protected $manageWishList;

    /**
     * @var WishListInput
     */
    protected $wishListInput;

    /**
     * @var WishListOutput
     */
    protected $wishListOutput;

    /**
     * @var WishListProductInput
     */
    protected $wishListProductInput;

    /**
     * @param WishListProductInput $wishListProductInput
     */
    public function setWishListProductInput(WishListProductInput $wishListProductInput)
    {
        $this->wishListProductInput = $wishListProductInput;
    }

    /**
     * @param ManageWishList $manageWishList
     * @param WishListInput $wishListInput
     * @param WishListOutput $wishListOutput
     */
    public function __construct(
        ManageWishList $manageWishList,
        WishListInput $wishListInput,
        WishListOutput $wishListOutput
    ) {
        $this->manageWishList = $manageWishList;
        $this->wishListInput = $wishListInput;
        $this->wishListOutput = $wishListOutput;
    }

    /**
     * @Route("customer/wish-list/create", name="frontend.mobile.customer.wish_list.create", methods={"POST"})
     * @Route("v1/customer/wish-list/create", name="frontend.mobile.customer.wish_list.create.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function createAction(Request $request): JsonResponse
    {
        $wishList = $this->wishListInput->fromCreateWishList($request->request->all(), $this->getCustomer());

        return new JsonResponse($this->wishListOutput->fromWishList($this->manageWishList->createWishList($wishList)));
    }

    /**
     * @Route("customer/wish-list/edit/{wishListId}", name="frontend.mobile.customer.wish_list.edit", methods={"POST"}, requirements={"wishListId":"\d+"})
     * @Route("v1/customer/wish-list/edit/{wishListId}", name="frontend.mobile.customer.wish_list.edit.v1", methods={"POST"}, requirements={"wishListId":"\d+"})
     *
     * @param Request $request
     * @param int $wishListId
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function editAction(Request $request, int $wishListId): JsonResponse
    {
        $wishList = $this->wishListInput->fromEditWishList($wishListId, $request->request->all(), $this->getCustomer());

        return new JsonResponse($this->wishListOutput->fromWishList($this->manageWishList->editWishList($wishList)));
    }

    /**
     * @Route("customer/wish-lists", name="frontend.mobile.customer.wish_lists", methods={"POST"})
     * @Route("v1/customer/wish-lists", name="frontend.mobile.customer.wish_lists.v1", methods={"POST"})
     *
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function getWishListsAction(): JsonResponse
    {
        $wishLists = $this->manageWishList->getWishLists($this->getCustomer());

        return new JsonResponse($this->wishListOutput->fromWishLists($wishLists));
    }

    /**
     * @Route("customer/wish-list/{wishListId}", name="frontend.mobile.customer.wish_list", methods={"POST"}, requirements={"wishListId":"\d+"})
     * @Route("v1/customer/wish-list/{wishListId}", name="frontend.mobile.customer.wish_list.v1", methods={"POST"}, requirements={"wishListId":"\d+"})
     *
     * @param int $wishListId
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function getWishListAction(int $wishListId): JsonResponse
    {
        $wishList = $this->manageWishList->getWishList(
            $this->wishListInput->fromId($wishListId, $this->getCustomer())
        );

        return new JsonResponse($this->wishListOutput->fromWishList($wishList));
    }

    /**
     * @Route("customer/wish-list/set-default/{wishListId}", name="frontend.mobile.customer.wish_list.set_default", methods={"POST"}, requirements={"wishListId":"\d+"})
     * @Route("v1/customer/wish-list/set-default/{wishListId}", name="frontend.mobile.customer.wish_list.set_default.v1", methods={"POST"}, requirements={"wishListId":"\d+"})
     *
     * @param int $wishListId
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function setDefaultWishList(int $wishListId): JsonResponse
    {
        $customer = $this->getCustomer();

        $wishList = $this->wishListInput->fromId($wishListId, $customer);
        $this->manageWishList->setDefaultWishList($wishList);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/wish-list/delete/{wishListId}", name="frontend.mobile.customer.wish_list.delete", methods={"POST"}, requirements={"wishListId":"\d+"})
     * @Route("v1/customer/wish-list/delete/{wishListId}", name="frontend.mobile.customer.wish_list.delete.v1", methods={"POST"}, requirements={"wishListId":"\d+"})
     *
     * @param int $wishListId
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function deleteWishListAction(int $wishListId): JsonResponse
    {
        $customer = $this->getCustomer();

        $wishList = $this->wishListInput->fromId($wishListId, $customer);
        $this->manageWishList->deleteWishList($wishList);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/wish-list/add-product/{wishListId}", name="frontend.mobile.customer.wish_list.add_product", methods={"POST"}, requirements={"wishListId":"\d+"})
     * @Route("v1/customer/wish-list/add-product/{wishListId}", name="frontend.mobile.customer.wish_list.add_product.v1", methods={"POST"}, requirements={"wishListId":"\d+"})
     *
     * @param Request $request
     * @param int $wishListId
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function addProductAction(Request $request, int $wishListId): JsonResponse
    {
        $wishList = $this->manageWishList->addProductToWishList(
            $this->wishListInput->fromId($wishListId, $this->getCustomer()),
            $this->wishListProductInput->fromAddProduct($request->request->all())
        );

        return new JsonResponse($this->wishListOutput->fromWishList($wishList));
    }

    /**
     * @Route("customer/wish-list/remove-product/{wishListId}", name="frontend.mobile.customer.wish_list.remove_product", methods={"POST"}, requirements={"wishListId":"\d+"})
     * @Route("v1/customer/wish-list/remove-product/{wishListId}", name="frontend.mobile.customer.wish_list.remove_product.v1", methods={"POST"}, requirements={"wishListId":"\d+"})
     *
     * @param Request $request
     * @param int $wishListId
     *
     * @throws WishListNotFoundException
     * @throws InvalidWishListException
     * @throws WishListException
     *
     * @return JsonResponse
     */
    public function removeProductAction(Request $request, int $wishListId): JsonResponse
    {
        $productData = [
            'simpleSku' => $request->request->get('simpleSku'),
        ];

        $this->manageWishList->removeProductFromWishList(
            $this->wishListInput->fromId($wishListId, $this->getCustomer()),
            $this->wishListProductInput->fromProductToRemove($productData)
        );

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
