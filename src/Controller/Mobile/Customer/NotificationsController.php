<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\NewsletterTransputAware;
use Linio\Frontend\Newsletter\ManageNewsletterAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/mapi/", service="controller.mobile.customer.notifications", defaults={"_format":"json"})
 */
class NotificationsController
{
    use TokenStorageAware;
    use ManageNewsletterAware;
    use NewsletterTransputAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("customer/notifications/update-subscriptions", name="frontend.mobile.customer.notifications.save", methods={"POST"})
     * @Route("v1/customer/notifications/update-subscriptions", name="frontend.mobile.customer.notifications.save.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateSubscriptionsAction(Request $request): JsonResponse
    {
        $customer = $this->getCustomer();

        $newsletterSubscription = $this->manageNewsletterSubscription->getNewsletterSubscription($customer);

        $this->manageNewsletterSubscription->updateNewsletterSubscription($customer, $this->newsletterInput->fromNewsletterSettings($request->request->all(), $newsletterSubscription));

        $success = $this->translator->trans('newsletter.update_subscription');

        return new JsonResponse($success);
    }

    /**
     * @Route("customer/notifications", name="frontend.mobile.customer.notifications", methods={"POST"})
     * @Route("v1/customer/notifications", name="frontend.mobile.customer.notifications.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function notificationsAction(): JsonResponse
    {
        return new JsonResponse($this->newsletterOutput->fromNewsletterSubscription($this->manageNewsletterSubscription->getNewsletterSubscription($this->getCustomer())));
    }

    /**
     * @Route("customer/notifications/unsubscribe", name="frontend.mobile.customer.notifications.unsubscribe", methods={"POST"})
     * @Route("v1/customer/notifications/unsubscribe", name="frontend.mobile.customer.notifications.unsubscribe.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function unsubscribeAction(): JsonResponse
    {
        $this->manageNewsletterSubscription->unsubscribe($this->getCustomer()->getEmail());

        return new JsonResponse(
            $this->translator->trans('newsletter.unsubscribed_message'),
            JsonResponse::HTTP_CREATED
        );
    }
}
