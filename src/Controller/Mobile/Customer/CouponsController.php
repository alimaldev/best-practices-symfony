<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Output\MobileCustomerCoupon;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/mapi/", service="controller.mobile.customer.coupons", defaults={"_format":"json"})
 */
class CouponsController
{
    use CustomerAware;
    use TokenStorageAware;

    /**
     * @var MobileCustomerCoupon
     */
    protected $customerCouponOutput;

    /**
     * @param MobileCustomerCoupon $customerCouponOutput
     */
    public function setCustomerCouponOutput(MobileCustomerCoupon $customerCouponOutput)
    {
        $this->customerCouponOutput = $customerCouponOutput;
    }

    /**
     * @Route("customer/coupon", name="frontend.mobile.customer.coupon", methods={"POST"})
     * @Route("v1/customer/coupon", name="frontend.mobile.customer.coupon.v1", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function couponDetailAction(): JsonResponse
    {
        $coupons = $this->customerService->getCoupons($this->getCustomer());

        return new JsonResponse($this->customerCouponOutput->fromCoupons($coupons));
    }
}
