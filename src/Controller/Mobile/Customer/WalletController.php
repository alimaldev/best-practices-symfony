<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\Frontend\Api\Output\CustomerWalletOutput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\RegisterWalletException;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.wallet")
 */
class WalletController
{
    use TokenStorageAware;
    use CustomerAware;

    /**
     * @var bool
     */
    protected $walletEnabled;

    /**
     * @var CustomerWalletOutput
     */
    protected $walletOutput;

    /**
     * @param bool $isWalletEnabled
     */
    public function setWalletEnabled(bool $isWalletEnabled)
    {
        $this->walletEnabled = $isWalletEnabled;
    }

    /**
     * @param CustomerWalletOutput $walletOutput
     */
    public function setWalletOutput(CustomerWalletOutput $walletOutput)
    {
        $this->walletOutput = $walletOutput;
    }

    /**
     * @Route("customer/wallet", name="frontend.mobile.customer.wallet", methods={"POST"})
     * @Route("v1/customer/wallet", name="frontend.mobile.customer.wallet.v1", methods={"POST"})
     *
     * @throws InputException
     * @throws CustomerException
     *
     * @return JsonResponse
     */
    public function walletDetailAction(): JsonResponse
    {
        if (!$this->walletEnabled) {
            throw new InputException(ExceptionMessage::WALLET_PROGRAM_UNAVAILABLE);
        }

        $wallet = $this->customerService->getWallet($this->getCustomer());

        return new JsonResponse($this->walletOutput->fromCustomerWallet($wallet));
    }

    /**
     * @Route("customer/register-wallet", name="frontend.mobile.customer.register_wallet", methods={"POST"})
     * @Route("v1/customer/register-wallet", name="frontend.mobile.customer.register_wallet.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException
     * @throws RegisterWalletException
     *
     * @return JsonResponse
     */
    public function registerWallet(Request $request): JsonResponse
    {
        if (!$this->walletEnabled) {
            throw new InputException(ExceptionMessage::WALLET_PROGRAM_UNAVAILABLE);
        }

        $accountNumber = $request->request->get('account');

        if (empty($accountNumber)) {
            throw new InputException(ExceptionMessage::WALLET_INVALID_ACCOUNT_NUMBER);
        }

        $this->customerService->registerWallet(
            $this->getCustomer(),
            $accountNumber,
            $request->request->get('holderName'),
            $request->request->get('holderLastname')
        );

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
