<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile\Customer;

use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Exception\NotFoundHttpException;
use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Customer\AddressBookAware;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Location\Exception\AddressException;
use Linio\Frontend\Location\Exception\InvalidAddressException;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Security\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/mapi/", service="controller.mobile.customer.address", defaults={"_format":"json"})
 */
class AddressController
{
    use TokenStorageAware;
    use DynamicFormAware;
    use AddressBookAware;
    use AddressTransputAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("customer/addresses", name="frontend.mobile.customer.address.get", methods={"POST"})
     * @Route("v1/customer/addresses", name="frontend.mobile.customer.address.get.v1", methods={"POST"})
     *
     * @throws InputException
     *
     * @return JsonResponse
     */
    public function getAddressesAction(): JsonResponse
    {
        $customerAddresses = $this->addressBook->getAddresses($this->getCustomer());

        if (empty($customerAddresses)) {
            throw new InvalidAddressException(ExceptionMessage::ERROR_ADDRESS_NOT_REGISTERED);
        }

        return new JsonResponse($this->addressOutput->fromAddresses($customerAddresses));
    }

    /**
     * @Route("customer/addresses/create", name="frontend.mobile.customer.address.create", methods={"POST"})
     * @Route("v1/customer/addresses/create", name="frontend.mobile.customer.address.create.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAddressAction(Request $request): JsonResponse
    {
        $form = $this->getDynamicFormFactory()->createForm('address', new Address(), ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form, 'address_book.add_address_error');
        }

        /** @var User $customer */
        $customer = $this->getCustomer();
        $address = $form->getData();

        $this->addressBook->createAddress($customer, $address);

        return new JsonResponse($this->addressOutput->fromAddress($address));
    }

    /**
     * @Route("customer/addresses/delete/{addressId}", name="frontend.mobile.customer.address.delete", methods={"POST"})
     * @Route("v1/customer/addresses/delete/{addressId}", name="frontend.mobile.customer.address.delete.v1", methods={"POST"})
     *
     * @param int $addressId
     *
     * @throws AddressException
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function removeAction(int $addressId): JsonResponse
    {
        $customer = $this->getCustomer();

        try {
            $address = $this->addressBook->getAddress($this->getCustomer(), $addressId);
        } catch (InputException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $this->addressBook->removeAddress($customer, $address);

        return new JsonResponse($this->translator->trans('address_book.address_removed_successfully'));
    }

    /**
     * @Route("customer/addresses/edit/{addressId}", name="frontend.mobile.customer.address.edit", methods={"POST"})
     * @Route("v1/customer/addresses/edit/{addressId}", name="frontend.mobile.customer.address.edit.v1", methods={"POST"})
     *
     * @param Request $request
     * @param int $addressId
     *
     * @return JsonResponse
     */
    public function editAddressAction(Request $request, int $addressId): JsonResponse
    {
        /* @var User $customer */
        $customer = $this->getCustomer();

        try {
            $address = $this->addressBook->getAddress($customer, $addressId);
        } catch (InputException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $form = $this->getDynamicFormFactory()->createForm('address', $address, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $this->addressBook->updateAddress($customer, $address);

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route("customer/addresses/set-shipping-default/{addressId}", name="frontend.mobile.customer.address.set_shipping_default", methods={"POST"})
     * @Route("v1/customer/addresses/set-shipping-default/{addressId}", name="frontend.mobile.customer.address.set_shipping_default.v1", methods={"POST"})
     *
     * @param int $addressId
     *
     * @throws AddressException
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function setDefaultShippingAddressAction(int $addressId): JsonResponse
    {
        $customer = $this->getCustomer();

        try {
            $address = $this->addressBook->getAddress($customer, $addressId);
        } catch (InputException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $this->addressBook->setDefaultShipping($customer, $address);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("customer/addresses/set-billing-default/{addressId}", name="frontend.mobile.customer.address.set_billing_default", methods={"POST"})
     * @Route("v1/customer/addresses/set-billing-default/{addressId}", name="frontend.mobile.customer.address.set_billing_default.v1", methods={"POST"})
     *
     * @param int $addressId
     *
     * @throws AddressException
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function setDefaultBillingAddressAction(int $addressId): JsonResponse
    {
        $customer = $this->getCustomer();

        try {
            $address = $this->addressBook->getAddress($customer, $addressId);
        } catch (InputException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $this->addressBook->setDefaultBilling($customer, $address);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
