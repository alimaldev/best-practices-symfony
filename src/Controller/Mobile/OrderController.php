<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Controller\SessionAware;
use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Api\OrderTransputAware;
use Linio\Frontend\Customer\AddressBookAware;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\BuildOrderAware;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\PlaceOrderAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.order")
 */
class OrderController
{
    use AddressBookAware;
    use TokenStorageAware;
    use BuildOrderAware;
    use PlaceOrderAware;
    use AddressTransputAware;
    use OrderTransputAware;
    use DynamicFormAware;
    use SessionAware;

    /**
     * @Route("order", name="frontend.mobile.order.in_progress", methods={"POST"})
     * @Route("v1/order", name="frontend.mobile.order.in_progress.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getOrderInProgressAction(Request $request): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->recalculateOrder($order);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/add-item", name="frontend.mobile.order.add.item", methods={"POST"})
     * @Route("v1/order/add-item", name="frontend.mobile.order.add.item.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addProductAction(Request $request): JsonResponse
    {
        $sku = $request->request->get('sku');

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->addProduct($order, new Item($sku));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/remove-item", name="frontend.mobile.order.remove.item", methods={"POST"})
     * @Route("v1/order/remove-item", name="frontend.mobile.order.remove.item.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeProductAction(Request $request): JsonResponse
    {
        $sku = $request->request->get('sku');

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->removeProduct($order, new Item($sku));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/update-quantity", name="frontend.mobile.order.update.quantity", methods={"POST"})
     * @Route("v1/order/update-quantity", name="frontend.mobile.order.update.quantity.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateQuantityAction(Request $request): JsonResponse
    {
        $item = new Item($request->request->get('sku'), $request->request->get('quantity'));

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->updateQuantity($order, $item);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/apply-coupon", name="frontend.mobile.order.add_coupon", methods={"POST"})
     * @Route("v1/order/apply-coupon", name="frontend.mobile.order.add_coupon.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function applyCouponAction(Request $request): JsonResponse
    {
        $couponCode = $request->request->get('coupon');
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->applyCoupon($order, new Coupon($couponCode));

        return new JsonResponse(
            $this->orderOutput->fromRecalculatedOrder($recalculatedOrder)
        );
    }

    /**
     * @Route("order/remove-coupon", name="frontend.mobile.order.remove_coupon", methods={"POST"})
     * @Route("v1/order/remove-coupon", name="frontend.mobile.order.remove_coupon.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeCouponAction(Request $request): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->removeCoupon($order);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/shipping-quote-address", name="frontend.mobile.order.shipping_quote_address", methods={"POST"})
     * @Route("v1/order/shipping-quote-address", name="frontend.mobile.order.shipping_quote_address.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     *
     * @return JsonResponse
     */
    public function setShippingAddressAction(Request $request): JsonResponse
    {
        $form = $this->dynamicFormFactory->createForm('cart_shipping_address', new Address(),  ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $shippingAddress = $form->getViewData();
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setShippingAddress($order, $shippingAddress);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/customer-shipping-address", name="frontend.mobile.order.shipping.address.method", methods={"POST"})
     * @Route("v1/order/customer-shipping-address", name="frontend.mobile.order.shipping.address.method.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setCustomerShippingAddressAction(Request $request): JsonResponse
    {
        $customer = $this->getCustomer();
        $shippingAddress = $this->addressBook->getAddress($customer, $request->request->get('addressId'));

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setShippingAddress($order, $shippingAddress);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/update-shipping-method", name="frontend.mobile.order.update_shipping_method", methods={"POST"})
     * @Route("v1/order/update-shipping-method", name="frontend.mobile.order.update_shipping_method.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateShippingMethodAction(Request $request): JsonResponse
    {
        $packageId = $request->request->get('packageId');
        $shippingMethod = $request->request->get('shippingMethod');

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->updateShippingMethod($order, $packageId, $shippingMethod);

        return new JsonResponse(
            $this->orderOutput->fromRecalculatedOrder($recalculatedOrder)
        );
    }

    /**
     * @Route("order/update-payment-method", name="frontend.mobile.order.update.payment.method", methods={"POST"})
     * @Route("v1/order/update-payment-method", name="frontend.mobile.order.update.payment.method.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updatePaymentMethodAction(Request $request): JsonResponse
    {
        $paymentMethodName = $request->request->get('paymentMethod');

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->updatePaymentMethod($order,
            PaymentMethodFactory::fromName($paymentMethodName));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/installment/{quantity}", name="frontend.mobile.order.set_installments", methods={"POST"}, requirements={"quantity":"\d+"})
     * @Route("v1/order/installment/{quantity}", name="frontend.mobile.order.set_installments.v1", methods={"POST"}, requirements={"quantity":"\d+"})
     *
     * @param Request $request
     * @param int $quantity
     *
     * @return JsonResponse
     */
    public function selectInstallmentQuantityAction(Request $request, int $quantity): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->selectInstallmentQuantity($order, $quantity);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/set-pickup-store/{pickupStoreId}", name="frontend.mobile.order.set_pickup_store", methods={"POST"}, requirements={"pickupStoreId":"\d+"})
     * @Route("v1/order/set-pickup-store/{pickupStoreId}", name="frontend.mobile.order.set_pickup_store.v1", methods={"POST"}, requirements={"pickupStoreId":"\d+"})
     *
     * @param Request $request
     * @param int $pickupStoreId
     *
     * @return JsonResponse
     */
    public function setPickupStoreAction(Request $request, int $pickupStoreId): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setPickupStore($order, $pickupStoreId);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/set-geohash/{geohash}", name="frontend.mobile.order.set_geohash", methods={"POST"})
     * @Route("v1/order/set-geohash/{geohash}", name="frontend.mobile.order.set_geohash.v1", methods={"POST"})
     *
     * @param Request $request
     * @param string $geohash
     *
     * @return JsonResponse
     */
    public function setGeohashAction(Request $request, string $geohash): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setGeohash($order, $geohash);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/set-bin/{binNumber}", name="frontend.mobile.order.set_bin_number", methods={"POST"})
     * @Route("v1/order/set-bin/{binNumber}", name="frontend.mobile.order.set_bin_number.v1", methods={"POST"})
     *
     * @param Request $request
     * @param string $binNumber
     *
     * @return JsonResponse
     */
    public function setBinNumberAction(Request $request, string $binNumber): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setBinNumber($order, $binNumber);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/wallet/{points}", name="frontend.mobile.order.use.wallet.points", methods={"POST"})
     * @Route("v1/order/wallet/{points}", name="frontend.mobile.order.use.wallet.points.v1", methods={"POST"})
     *
     * @param Request $request
     * @param int $points
     *
     * @return JsonResponse
     */
    public function useWalletPointsAction(Request $request, int $points): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->useWalletPoints($order, $points);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("order/place", name="frontend.mobile.order.place-order", methods={"POST"})
     * @Route("v1/order/place", name="frontend.mobile.order.place-order.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function placeOrderAction(Request $request): JsonResponse
    {
        $orderInProgress = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));

        $order = $this->orderInput->fromPlaceOrder($request->request->all(), $orderInProgress);
        $order->setIpAddress($request->getClientIp());

        $completedOrder = $this->placeOrder->placeOrder($order);

        $this->session->set('completed_order', $completedOrder->getOrderNumber());

        return new JsonResponse($this->orderOutput->fromCompletedOrder($completedOrder));
    }
}
