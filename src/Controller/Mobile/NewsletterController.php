<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Api\Output\NewsletterOutput;
use Linio\Frontend\Newsletter\ManageNewsletterAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/mapi/", service="controller.mobile.newsletter", defaults={"_format":"json"})
 */
class NewsletterController
{
    use ManageNewsletterAware;

    /**
     * @var NewsletterOutput
     */
    protected $newsletterOutput;

    /**
     * @param NewsletterOutput $newsletterOutput
     */
    public function setNewsletterOutput(NewsletterOutput $newsletterOutput)
    {
        $this->newsletterOutput = $newsletterOutput;
    }

    /**
     * @Route("subscribe-email-newsletter", methods={"POST"}, name="frontend.mobile.newsletter.subscribe")
     * @Route("v1/subscribe-email-newsletter", methods={"POST"}, name="frontend.mobile.newsletter.subscribe.v1")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function subscribeAction(Request $request): JsonResponse
    {
        $email = $request->request->get('email');

        $source = $request->request->get('source', 'mobapi');

        $clientIp = $request->getClientIp();

        $subscription = $this->manageNewsletterSubscription->subscribe($email, $source, $clientIp);

        return new JsonResponse($this->newsletterOutput->fromPromotionalNewsletterSubscription($subscription));
    }
}
