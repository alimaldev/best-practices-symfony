<?php

namespace Linio\Frontend\Controller\Mobile;

use Linio\Frontend\Location\ResolveLocationAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/mapi/", service="controller.mobile.address", defaults={"_format":"json"})
 */
class AddressController
{
    use ResolveLocationAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("geo/regions", name="frontend.mobile.address.regions", methods={"POST"})
     * @Route("v1/geo/regions", name="frontend.mobile.address.regions.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function regionsAction(Request $request)
    {
        $regions = $this->getResolveLocation()->getRegions($request->request);

        if (empty($regions)) {
            throw new NotFoundHttpException('geo.regions.not_found');
        }

        return new JsonResponse($regions);
    }

    /**
     * @Route("geo/municipalities", name="frontend.mobile.address.municipalities", methods={"POST"})
     * @Route("v1/geo/municipalities", name="frontend.mobile.address.municipalities.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function municipalitiesAction(Request $request)
    {
        $municipalities = $this->getResolveLocation()->getMunicipalities($request->request);

        if (empty($municipalities)) {
            throw new NotFoundHttpException('geo.municipalities.not_found');
        }

        return new JsonResponse($municipalities);
    }

    /**
     * @Route("geo/cities", name="frontend.mobile.address.cities", methods={"POST"})
     * @Route("v1/geo/cities", name="frontend.mobile.address.cities.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function citiesAction(Request $request)
    {
        $cities = $this->getResolveLocation()->getCities($request->request);

        if (empty($cities)) {
            throw new NotFoundHttpException('geo.cities.not_found');
        }

        return new JsonResponse($cities);
    }

    /**
     * @Route("geo/neighborhoods", name="frontend.mobile.address.neighborhoods", methods={"POST"})
     * @Route("v1/geo/neighborhoods", name="frontend.mobile.address.neighborhoods.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function neighborhoodsAction(Request $request)
    {
        $neighborhoods = $this->getResolveLocation()->getNeighborhoods($request->request);

        if (empty($neighborhoods)) {
            throw new NotFoundHttpException('geo.neighborhoods.not_found');
        }

        return new JsonResponse($neighborhoods);
    }

    /**
     * @Route("geo/postcode/resolve", name="frontend.mobile.address.postcode.resolve", methods={"POST"})
     * @Route("v1/geo/postcode/resolve", name="frontend.mobile.address.postcode.resolve.v1", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function resolvePostcodeAction(Request $request)
    {
        $address = $this->getResolveLocation()->resolvePostcode($request->request->get('postcode'));

        if (empty($address)) {
            throw new NotFoundHttpException('geo.address.not_found');
        }

        return new JsonResponse($address);
    }
}
