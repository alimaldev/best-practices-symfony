<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller;

use Linio\Controller\RouterAware;
use Linio\Controller\SessionAware;
use Linio\Controller\TemplatingAware;
use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Marketing\DataLayerAware;
use Linio\Frontend\Order\BuildOrder;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\PlaceOrder;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/checkout", service="controller.checkout")
 */
class CheckoutController
{
    use TemplatingAware;
    use RouterAware;
    use TokenStorageAware;
    use SessionAware;
    use DataLayerAware;
    use SearchOrderAware;
    use DynamicFormAware;

    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @var PlaceOrder
     */
    protected $placeOrder;

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }

    /**
     * @param PlaceOrder $placeOrder
     */
    public function setPlaceOrder(PlaceOrder $placeOrder)
    {
        $this->placeOrder = $placeOrder;
    }

    /**
     * @Route("/shipping-address", name="frontend.checkout.step1")
     * @Route("/shipping-options", name="frontend.checkout.step2")
     * @Route("/payment", name="frontend.checkout.step3")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $order = $this->buildOrder->getOrderInProgress($this->getCustomer());

        if ($order->isEmpty()) {
            return $this->redirectToRoute('frontend.cart.index');
        }

        $this->dataLayerService->populateCheckout($order);

        return $this->render(':Checkout:index.html.twig');
    }

    /**
     * @Route("/external-payment", name="frontend.checkout.external_payment")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function processExternalPayment(Request $request): RedirectResponse
    {
        $completedOrderNumber = $this->session->get('completed_order');
        $completedOrderId = $this->session->get('completed_order_id');

        if (!$completedOrderNumber) {
            return $this->redirectToRoute('frontend.checkout.error');
        }

        $parameters = array_merge($request->request->all(), $request->query->all());
        $externalPaymentResult = $this->placeOrder->processExternalPayment($completedOrderNumber, $completedOrderId, (int) $this->getCustomer()->getId(), $parameters);

        if ($externalPaymentResult->isSuccess()) {
            return $this->redirectToRoute('frontend.checkout.success');
        }

        return $this->redirectToRoute('frontend.checkout.error');
    }

    /**
     * @Route("/post-process/{paymentMethod}", name="frontend.checkout.post_process", requirements={"paymentMethod"="[a-z]+"})
     *
     * @param Request $request
     * @param string $paymentMethod
     *
     * @return Response
     */
    public function postProcessExternalPayment(Request $request, string $paymentMethod): Response
    {
        $postProcessResult = $this->placeOrder->postProcessExternalPayment($paymentMethod, $request->query->all());

        return new Response($postProcessResult['message']);
    }

    /**
     * @Route("/update-status/{paymentMethod}/{action}", name="frontend.checkout.update_payment_status", requirements={"paymentMethod"="[a-zA-Z_]+", "action"="[a-zA-Z_]+"}, defaults={"action"=null})
     *
     * @param Request $request
     * @param string $paymentMethod
     * @param null|string $action
     *
     * @return Response
     */
    public function updatePaymentStatus(Request $request, string $paymentMethod, string $action = null): Response
    {
        $paymentStatusResult = $this->placeOrder->updatePaymentStatus($paymentMethod, (string) $request->getContent(), $request->headers->all(), $action);

        return new Response($paymentStatusResult['response']);
    }

    /**
     * @Route("/success", name="frontend.checkout.success")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function successAction(Request $request): Response
    {
        $completedOrderNumber = $this->session->get('completed_order');

        if (!$completedOrderNumber) {
            return $this->redirectToRoute('frontend.root.home');
        }

        $order = $this->searchOrder->findOrder($this->getCustomer(), $completedOrderNumber);
        $this->session->remove('completed_order');

        // TODO: Remove this hacky workaround when Datalayer is rewriting
        $completedOrder = unserialize($this->session->get('completed_order_object'));
        $this->session->remove('completed_order_object');

        $this->dataLayerService->populateSuccess($completedOrder, $request->cookies);

        return $this->render(':Checkout:success.html.twig', [
            'order' => $order,
            'completedOrder' => $completedOrder,
        ]);
    }

    /**
     * @Route("/error", name="frontend.checkout.error")
     *
     * @return Response
     */
    public function errorAction(Request $request): Response
    {
        $paymentMethod = PaymentMethodFactory::fromName((string) $request->query->get('p'));

        return $this->render(':Checkout:error.html.twig', ['paymentMethod' => $paymentMethod]);
    }

    /**
     * @Route("/state/shipping-address", name="frontend.checkout.state.address")
     *
     * @return Response
     */
    public function addressStateAction(): Response
    {
        return $this->render(':Checkout:step/address.html.twig');
    }

    /**
     * @Route("/state/shipping-options", name="frontend.checkout.state.options")
     *
     * @return Response
     */
    public function optionsStateAction(): Response
    {
        return $this->render(':Checkout:step/options.html.twig');
    }

    /**
     * @Route("/state/payment", name="frontend.checkout.state.payment")
     *
     * @return Response
     */
    public function paymentStateAction(): Response
    {
        $order = $this->buildOrder->getOrderInProgress($this->getCustomer());
        $recalculatedOrder = $this->buildOrder->recalculateOrder($order);
        $extraFieldsForm = $this->getDynamicFormFactory()->createForm('extra_checkout_fields', [], ['csrf_protection' => false]);

        return $this->render(':Checkout:step/payment.html.twig', [
            'order' => $recalculatedOrder->getOrder(),
            'extraFieldsForm' => $extraFieldsForm->createView(),
        ]);
    }
}
