<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Customer;

use Linio\Controller\TemplatingAware;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Seller\SellerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/account/reviews", service="controller.customer.review.seller")
 */
class SellerReviewController
{
    use TemplatingAware;
    use TokenStorageAware;

    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @var bool
     */
    protected $sellerReviewsEnabled = false;

    /**
     * @param SellerService $sellerService
     */
    public function setSellerService(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * @param bool $sellerReviewsEnabled
     */
    public function setSellerReviewsEnabled(bool $sellerReviewsEnabled)
    {
        $this->sellerReviewsEnabled = $sellerReviewsEnabled;
    }

    /**
     * @Route("/{page}/{pageSize}", name="frontend.customer.seller.reviews", defaults={"page"=1, "pageSize"=10})
     *
     * @param int $page
     * @param int $pageSize
     *
     * @return Response
     */
    public function indexAction(int $page, int $pageSize)
    {
        if (!$this->sellerReviewsEnabled) {
            throw new NotFoundHttpException();
        }

        $reviewableOrders = $this->sellerService->findCustomerReviews($this->getCustomer(), $page, $pageSize);

        return $this->render(':Account/SellerReview:index.html.twig', [
            'reviewableOrders' => $reviewableOrders,
        ]);
    }
}
