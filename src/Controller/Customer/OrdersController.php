<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Customer;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Form\Customer\ItemReturnRequestForm;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/account/order", service="controller.customer.orders")
 */
class OrdersController
{
    use CustomerAware;
    use RouterAware;
    use TemplatingAware;
    use TokenStorageAware;
    use FlashMessageAware;
    use FormAware;
    use SearchOrderAware;

    /**
     * @var bool
     */
    protected $returnItemEnabled = false;

    /**
     * @param bool $returnItemEnabled
     */
    public function setReturnItemEnabled(bool $returnItemEnabled)
    {
        $this->returnItemEnabled = $returnItemEnabled;
    }

    /**
     * @Route("/{page}", name="frontend.customer.orders.list")
     *
     * @param int $page
     *
     * @return Response
     */
    public function ordersAction(int $page = 1): Response
    {
        $search = new Search();
        $search->setPage($page);

        return $this->render(':Customer/orders:list.html.twig', [
            'paginatedResult' => $this->searchOrder->findOrders($this->getCustomer(), $search),
        ]);
    }

    /**
     * @Route("/details/{orderNumber}", name="frontend.customer.orders.detail")
     *
     * @param string $orderNumber
     *
     * @return Response
     */
    public function orderDetailsAction(string $orderNumber): Response
    {
        try {
            $order = $this->searchOrder->findOrder($this->getCustomer(), $orderNumber);
        } catch (OrderException $exception) {
            if ($exception->getMessage() == ExceptionMessage::ENTITY_NOT_FOUND) {
                $this->addFlash('error', 'order.not_found');

                return $this->redirectToRoute('frontend.customer.orders.list');
            }

            throw $exception;
        }

        return $this->render(':Customer/orders:detail.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/tracking/{orderNumber}", name="frontend.customer.orders.tracking")
     *
     * @param string $orderNumber
     *
     * @return Response
     */
    public function orderTrackingAction(string $orderNumber): Response
    {
        try {
            $order = $this->searchOrder->findOrder($this->getCustomer(), $orderNumber);
            $trackingHistory = $this->searchOrder->findOrderTrackingHistory($this->getCustomer(), $orderNumber);
        } catch (OrderException $exception) {
            if ($exception->getMessage() == ExceptionMessage::ENTITY_NOT_FOUND) {
                $this->addFlash('error', 'order.not_found');

                return $this->redirectToRoute('frontend.customer.orders.list');
            }

            throw $exception;
        }

        return $this->render(':Customer/orders:tracking.html.twig', [
            'order' => $order,
            'trackingHistory' => $trackingHistory,
        ]);
    }

    /**
     * @Route("/{orderNumber}/return", name="frontend.customer.orders.return")
     *
     * @param string $orderNumber
     *
     * @return Response
     */
    public function orderReturnAction(string $orderNumber): Response
    {
        if (!$this->returnItemEnabled) {
            throw new NotFoundHttpException();
        }

        try {
            $order = $this->searchOrder->findOrder($this->getCustomer(), $orderNumber);
        } catch (OrderException $exception) {
            if ($exception->getMessage() == ExceptionMessage::ENTITY_NOT_FOUND) {
                $this->addFlash('error', 'order.not_found');

                return $this->redirectToRoute('frontend.customer.orders.list');
            }

            throw $exception;
        }

        $form = $this->createForm(ItemReturnRequestForm::class);

        return $this->render(':Customer/orders:return.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{orderNumber}/return/confirmation", name="frontend.customer.orders.return_confirmation")
     *
     * @param string $orderNumber
     *
     * @return Response
     */
    public function returnConfirmationAction(string $orderNumber): Response
    {
        if (!$this->returnItemEnabled) {
            throw new NotFoundHttpException();
        }

        try {
            $order = $this->searchOrder->findOrder($this->getCustomer(), $orderNumber);
        } catch (OrderException $exception) {
            $this->addFlash('error', 'order.not_found');

            return $this->redirectToRoute('frontend.customer.orders.list');
        }

        return $this->render(
            ':partial/components/customer:return_processing.html.twig',
            [
                'order' => $order,
            ]
        );
    }

    /**
     * @Route(
     *     "/invoice/{folio}.{_format}",
     *     name="frontend.customer.order.download.invoice",
     *     defaults={"_format": "pdf"},
     *     requirements={
     *         "_format": "pdf|xml",
     *     }
     * )
     *
     * @param string $folio
     * @param string $_format
     *
     * @return Response
     */
    public function invoiceAction(string $folio, string $_format): Response
    {
        $invoice = $this->getCustomer()->getInvoices()->offsetGet($folio);

        if (!$invoice) {
            throw new NotFoundHttpException(sprintf('Invoice with folio %s not found.', $folio));
        }

        $response = new Response($invoice->getDocument($_format));

        $d = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf('%s.%s', $folio, $_format)
        );

        $response->headers->set('Content-Disposition', $d);

        return $response;
    }
}
