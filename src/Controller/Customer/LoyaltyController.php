<?php

namespace Linio\Frontend\Controller\Customer;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Customer\Membership\ManageMembershipsAware;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\Customer\LoyaltyProgramForm;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/account/loyalty", service="controller.customer.loyalty")
 */
class LoyaltyController
{
    use TemplatingAware;
    use TokenStorageAware;
    use FormAware;
    use FlashMessageAware;
    use RouterAware;
    use ManageMembershipsAware;
    use SearchOrderAware;

    /**
     * @var string
     */
    protected $loyaltyProgram;

    /**
     * @var bool
     */
    protected $loyaltyOrdersEnabled;

    /**
     * @param string $loyaltyProgram
     */
    public function setLoyaltyProgram(string $loyaltyProgram)
    {
        $this->loyaltyProgram = $loyaltyProgram;
    }

    /**
     * @param bool $loyaltyOrdersEnabled
     */
    public function setLoyaltyOrdersEnabled(bool $loyaltyOrdersEnabled)
    {
        $this->loyaltyOrdersEnabled = $loyaltyOrdersEnabled;
    }

    /**
     * @Route(name="frontend.customer.loyalty.account", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function accountAction(Request $request)
    {
        $customer = $this->getCustomer();

        $form = $this->formFactory->create(LoyaltyProgramForm::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $loyaltyProgram = new LoyaltyProgram();
                $loyaltyProgram->setName($this->loyaltyProgram);
                $loyaltyProgram->setId($form->get('loyaltyId')->getData());

                $this->manageMemberships->signupForLoyaltyProgram($customer, $loyaltyProgram);
                $this->addFlash('success', 'customer.loyalty.save_loyalty_id_success');

                return $this->render(':Customer/loyalty:account.html.twig', [
                    'loyaltyProgram' => $loyaltyProgram,
                ]);
            } catch (InputException $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        // This could have gone before the if statement to remove some duplication in code
        // but then we would have had to have made two calls, a get and a set when posting.
        // When this becomes an angular form, this will be much cleaner.
        try {
            $loyaltyProgram = $this->manageMemberships->getLoyaltyProgram($customer);
        } catch (InputException $exception) {
            $loyaltyProgram = new LoyaltyProgram();
            $loyaltyProgram->setName($this->loyaltyProgram);
        }

        return $this->render(':Customer/loyalty:account.html.twig', [
            'form' => $form->createView(),
            'loyaltyProgram' => $loyaltyProgram,
        ]);
    }

    /**
     * @Route("/orders", name="frontend.customer.loyalty.orders")
     *
     * @return Response
     */
    public function ordersAction()
    {
        if (!$this->loyaltyOrdersEnabled) {
            return $this->redirectToRoute('frontend.customer.orders.list');
        }

        $orders = $this->searchOrder->findLoyaltyOrders($this->getCustomer());

        return $this->render(
            ':Customer/loyalty:orders.html.twig',
            [
                'orders' => $orders,
            ]
        );
    }
}
