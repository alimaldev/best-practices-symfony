<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Customer;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Exception\HttpException;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Form\AddCreditCardForm;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/account", service="controller.customer.card")
 */
class CardController
{
    use CustomerAware;
    use FormAware;
    use TemplatingAware;
    use TokenStorageAware;
    use RouterAware;
    use FlashMessageAware;

    /**
     * @var bool
     */
    protected $storeCards;

    /**
     * @param bool $storeCards
     */
    public function setStoreCards(bool $storeCards)
    {
        $this->storeCards = $storeCards;
    }

    /**
     * @Route("/cards", name="frontend.customer.cards")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function cardsAction(Request $request)
    {
        if (!$this->storeCards) {
            throw new NotFoundHttpException($request->getUri());
        }

        try {
            $cards = $this->customerService->getCreditCards($this->getCustomer());
        } catch (CustomerException $exception) {
            throw new HttpException($exception->getMessage(), 500, $exception->getCode());
        }

        $form = $this->getFormFactory()->create(AddCreditCardForm::class);

        return $this->render(
            '::partial/components/customer/cards.html.twig',
            [
                'cards' => $cards,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/cards-add-save", name="frontend.customer.cards.add.save")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function addCreditCardAction(Request $request)
    {
        if (!$this->storeCards) {
            throw new NotFoundHttpException($request->getUri());
        }

        $form = $this->getFormFactory()->create(AddCreditCardForm::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $creditCard = $form->getData();

            try {
                $this->customerService->addCreditCard($creditCard, $this->getCustomer());
                $this->addFlash('success', 'saved_cards.add_card_success');
            } catch (CustomerException $exception) {
                $this->addFlash('error', 'saved_cards.' . $exception->getMessage());
            }
        } else {
            foreach ($form->getErrors(true, true) as $error) {
                $this->addFlash('error', $error->getMessage());
            }
        }

        return $this->redirectToRoute('frontend.customer.cards');
    }

    /**
     * @Route("/cards-remove/{cardId}", name="frontend.customer.cards.remove", requirements={"cardId":"\d+"})")
     *
     * @param Request $request
     * @param int $cardId
     *
     * @return RedirectResponse
     */
    public function removeCreditCardAction(Request $request, int $cardId)
    {
        if (!$this->storeCards) {
            throw new NotFoundHttpException($request->getUri());
        }

        $customer = $this->getCustomer();
        $this->customerService->removeCreditCard($cardId, $customer);

        $this->addFlash('success', 'saved_cards.delete_card_success');

        return $this->redirectToRoute('frontend.customer.cards');
    }
}
