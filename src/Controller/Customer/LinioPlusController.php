<?php

namespace Linio\Frontend\Controller\Customer;

use Linio\Controller\TemplatingAware;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class LinioPlusController.
 *
 * @Route("/account/plus", service="controller.customer.plus")
 */
class LinioPlusController
{
    use TokenStorageAware;
    use CustomerAware;
    use TemplatingAware;

    /**
     * @Route(name="frontend.customer.plus.index")
     */
    public function plusAction()
    {
        $viewParams = [
            'customer' => $this->getCustomer(),
        ];

        if ($this->getCustomer()->isSubscribedToLinioPlus()) {
            $viewParams['membership'] = $this->customerService->getLinioPlusMembership($this->getCustomer());
            $view = $this->render(':Customer/plus:subscribed.html.twig', $viewParams);
        } elseif (!$this->getCustomer()->isSubscribedToLinioPlus() && $this->getCustomer()->wasSubscribedToLinioPlus()) {
            $viewParams['membership'] = $this->customerService->getLinioPlusMembership($this->getCustomer());
            $view = $this->render(':Customer/plus:expired.html.twig', $viewParams);
        } else {
            $view = $this->render(':Customer/plus:not_subscribed.html.twig', $viewParams);
        }

        return $view;
    }
}
