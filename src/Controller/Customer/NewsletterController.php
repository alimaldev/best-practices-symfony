<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Customer;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Newsletter\Exception\NewsletterException;
use Linio\Frontend\Newsletter\ManageNewsletterAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/account/notifications", service="controller.customer.newsletter")
 */
class NewsletterController
{
    use ManageNewsletterAware;
    use TemplatingAware;
    use TokenStorageAware;
    use FormAware;
    use RouterAware;
    use FlashMessageAware;

    /**
     * @Route(name="frontend.customer.notifications", methods={"GET"})
     *
     * @return Response
     */
    public function viewAction(): Response
    {
        return $this->render('::partial/components/customer/notifications.html.twig', [
            'newsletterSubscription' => $this->manageNewsletterSubscription->getNewsletterSubscription($this->getCustomer()),
        ]);
    }

    /**
     * @Route(name="frontend.customer.notifications.save", methods={"POST"})
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function saveAction(Request $request): RedirectResponse
    {
        $customer = $this->getCustomer();

        $newsletterSubscription = $this->manageNewsletterSubscription->getNewsletterSubscription($customer);
        $newsletterSubscription->setFrequency($request->request->getInt('frequency'));

        if ($request->request->getBoolean('subscribedToSms')) {
            $newsletterSubscription->subscribeToSms();
            $newsletterSubscription->setPhoneNumber($request->request->get('phoneNumber'));
        } else {
            $newsletterSubscription->unsubscribeFromSms();
        }

        try {
            foreach ($newsletterSubscription->getPreferences() as $preference) {
                if (empty($request->request->get('preferences'))) {
                    $preference->disable();
                    continue;
                }

                if (in_array($preference->getId(), $request->request->get('preferences')['id'])) {
                    $preference->enable();
                } else {
                    $preference->disable();
                }
            }

            $this->manageNewsletterSubscription->updateNewsletterSubscription($customer, $newsletterSubscription);
        } catch (NewsletterException $exception) {
            $this->addFlash('error', $exception->getMessage());

            return $this->redirectToRoute('frontend.customer.notifications');
        }

        $this->addFlash('success', 'newsletter.update_subscription');

        return $this->redirectToRoute('frontend.customer.notifications');
    }

    /**
     * @Route("/unsubscribe", name="frontend.customer.notifications.unsubscribe", methods={"GET"})
     *
     * @return RedirectResponse
     */
    public function unsubscribeAction(): RedirectResponse
    {
        $this->manageNewsletterSubscription->unsubscribe($this->getCustomer()->getEmail());

        return $this->redirectToRoute('frontend.customer.notifications');
    }
}
