<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller\Account;

use Linio\Controller\TemplatingAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/account/address-book", service="controller.account.address_book")
 */
class AddressBookController
{
    use TemplatingAware;

    /**
     * @Route(name="frontend.account.address_book.address.list")
     */
    public function listAddressesAction()
    {
        return $this->render(':Account/AddressBook:listAddresses.html.twig');
    }
}
