<?php

namespace Linio\Frontend\Controller;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Newsletter\ManageNewsletterAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route(service="controller.newsletter")
 */
class NewsletterController
{
    use FlashMessageAware;
    use ManageNewsletterAware;
    use RouterAware;
    use TemplatingAware;

    /**
     * @Route("/newsletter/subscribe", name="frontend.newsletter.subscribe")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function subscribeAction(Request $request)
    {
        $email = $request->query->get('email');
        $source = $request->query->get('source');
        $clientIp = $request->getClientIp();

        $subscription = $this->manageNewsletterSubscription->subscribe($email, $source, $clientIp);

        $this->addFlash('newsletterStatus', $subscription->isSubscribedToEmail());
        $this->addFlash('newsletterCoupon', $subscription->getCoupon());

        return $this->redirectToRoute('frontend.default.index');
    }

    /**
     * @Route("/newsletter/unsubscribe", name="frontend.newsletter.unsubscribe")
     * @Route("/gracias-por-desuscribirte", name="frontend.newsletter.unsubscribe.legacy")
     * @Method("GET")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function unsubscribeAction(Request $request)
    {
        $email = $request->query->get('unsign_email');

        $this->manageNewsletterSubscription->unsubscribe($email);

        return $this->render(':Newsletter:unsubscribe.html.twig');
    }
}
