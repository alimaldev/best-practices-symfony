<?php

namespace Linio\Frontend\Controller;

use Linio\Controller\TemplatingAware;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Seller\SellerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/seller", service="controller.seller")
 */
class SellerController
{
    use TemplatingAware;

    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @param SellerService $sellerService
     */
    public function __construct(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * @Route("/{seller}/reviews/{page}/{pageSize}", name="frontend.seller.reviews", methods={"GET"}, defaults={"page" = 1, "pageSize" = 10})
     * @ParamConverter("seller", converter="slug", options={"resolver"="seller"})
     *
     * @param Seller $seller
     * @param int $page
     * @param int $pageSize
     *
     * @return Response
     */
    public function sellerReviewsAction(Seller $seller, $page, $pageSize)
    {
        // TODO: Discuss if we can merge route:seller and seller:data to have just one seller cache.

        $sellerWithSummary = $this->sellerService->get($seller->getSellerId());

        $reviews = $this->sellerService->getReviews($seller->getSellerId(), $page, $pageSize);

        return $this->render(':Seller:reviews.html.twig', [
            'seller' => $sellerWithSummary,
            'paginatedReviews' => $reviews,
        ]);
    }
}
