<?php

namespace Linio\Frontend\Controller;

use Linio\Controller\TemplatingAware;
use Linio\DynamicFormBundle\DynamicFormAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route(service="controller.component")
 */
class ComponentController
{
    use TemplatingAware;
    use DynamicFormAware;

    /**
     * @Route("/ng/address-form", name="frontend.component.address_form")
     *
     * @return Response
     */
    public function addressFormAction(): Response
    {
        $addressForm = $this->getDynamicFormFactory()->createForm('address', [], ['csrf_protection' => false]);

        return $this->render(':Component:addressForm.html.twig', [
            'form' => $addressForm->createView(),
        ]);
    }

    /**
     * @Route("/ng/invoice-request-form", name="frontend.component.invoice_request_form")
     *
     * @return Response
     */
    public function invoiceRequestFormAction(): Response
    {
        $invoiceRequestForm = $this->getDynamicFormFactory()->createForm('billing_address', [], ['csrf_protection' => false]);

        return $this->render(':Component:invoiceRequestForm.html.twig', [
            'form' => $invoiceRequestForm->createView(),
        ]);
    }

    /**
     * @Route("/ng/shipping-quote-form", name="frontend.component.shipping_quote_form")
     *
     * @return Response
     */
    public function shippingQuoteFormAction(): Response
    {
        $shippingQuoteForm = $this->getDynamicFormFactory()->createForm('cart_shipping_address', [], ['csrf_protection' => false]);

        return $this->render(':Component:shippingQuoteForm.html.twig', [
            'form' => $shippingQuoteForm->createView(),
        ]);
    }

    /**
     * @Route("/ng/order-metadata", name="frontend.component.order_metadata")
     *
     * @return Response
     */
    public function orderMetadataAction(): Response
    {
        $orderMetadata = $this->getDynamicFormFactory()->createForm('extra_checkout_fields', [], ['csrf_protection' => false]);

        return $this->render(':Component:orderMetadata.html.twig', [
            'form' => $orderMetadata->createView(),
        ]);
    }
}
