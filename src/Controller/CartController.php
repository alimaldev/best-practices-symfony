<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Marketing\DataLayerAware;
use Linio\Frontend\Order\BuildOrder;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/cart", service="controller.cart")
 */
class CartController
{
    use RouterAware;
    use TemplatingAware;
    use DataLayerAware;
    use FlashMessageAware;
    use TokenStorageAware;

    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }

    /**
     * @Route(name="frontend.cart.index")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        $order = $this->buildOrder->getOrderInProgress($this->getCustomer());

        if ($order->isEmpty()) {
            $this->dataLayerService->populateCart(new Order($this->getCustomer()->getLinioId()));

            return $this->render(':Cart:empty.html.twig');
        }

        $recalculatedOrder = $this->buildOrder->recalculateOrder($order);
        $this->dataLayerService->populateCart($recalculatedOrder->getOrder());

        return $this->render(':Cart:index.html.twig');
    }

    /**
     * @Route("/product", methods={"POST"}, name="frontend.cart.product.add")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function addProductAction(Request $request): RedirectResponse
    {
        $sku = $request->request->get('sku');

        try {
            $this->buildOrder->addProduct($this->buildOrder->getOrderInProgress($this->getCustomer()), new Item($sku));
        } catch (DomainException $exception) {
            $this->addFlash('error', $exception->getMessage());

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->redirectToRoute('frontend.cart.index');
    }
}
