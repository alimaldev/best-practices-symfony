<?php

namespace Linio\Frontend\Controller\Catalog;

use DateTime;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Exception;
use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Frontend\Communication\Catalog\CatalogAware;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Form\CreateReviewForm;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/review", service="controller.catalog.product.review")
 */
class ProductReviewController
{
    use FormAware;
    use TokenStorageAware;
    use RouterAware;
    use FlashMessageAware;
    use CatalogAware;

    /**
     * @Route("/{product}", name="frontend.catalog.product.review", methods={"POST"})
     * @ParamConverter("product", converter="slug", options={"resolver"="product"})
     *
     * @param Request $request
     * @param Product $product
     *
     * @return RedirectResponse
     */
    public function createAction(Request $request, Product $product)
    {
        $customer = $this->getCustomer();

        $reviewForm = $this->createForm(new CreateReviewForm(), new Review());

        if ($customer instanceof GuestUser) {
            $reviewForm->add(
                'captcha',
                'ewz_recaptcha',
                [
                    'mapped' => false,
                    'constraints' => [
                        new RecaptchaTrue(),
                    ],
                    'error_bubbling' => true,
                ]
            );
        }

        $reviewForm->handleRequest($request);

        if ($reviewForm->isValid()) {
            /* @var Review $review */
            $review = $reviewForm->getData();
            $review->setSku($product->getSku());
            $review->setCreatedAt(new DateTime());

            if (!$customer instanceof GuestUser) {
                $review->setCustomerName(sprintf('%s %s', $customer->getFirstName(), $customer->getLastName()));
            }

            try {
                $this->catalogService->setRating($customer, $review);
                $this->addFlash('success', 'reviews.create_success');
            } catch (Exception $e) {
                $this->addFlash('error', 'reviews.create_failed');
            }
        }

        foreach ($reviewForm->getErrors(true, true) as $error) {
            $this->addFlash('error', $error->getMessage());
        }

        return $this->redirectToRoute('frontend.catalog.detail', ['productSlug' => $product->getSlug()]);
    }
}
