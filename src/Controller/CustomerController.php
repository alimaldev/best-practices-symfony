<?php

declare(strict_types=1);

namespace Linio\Frontend\Controller;

use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Exception;
use Linio\Controller\FlashMessageAware;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Frontend\Api\Input\CustomerInput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerEmailExistsException;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\RegisterWalletException;
use Linio\Frontend\Communication\Customer\Exception\UnregisteredWalletException;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Form\AccountChangePasswordForm;
use Linio\Frontend\Form\ForgotPasswordForm;
use Linio\Frontend\Form\ResetPasswordForm;
use Linio\Frontend\Order\BuildOrder;
use Linio\Frontend\Security\AuthenticationToken;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Security\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/account", service="controller.customer")
 */
class CustomerController
{
    use TemplatingAware;
    use TokenStorageAware;
    use FormAware;
    use FlashMessageAware;
    use CustomerAware;
    use RouterAware;
    use DynamicFormAware;
    use SearchOrderAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @var bool
     */
    protected $isWalletEnabled;

    /**
     * @var CustomerInput
     */
    protected $customerInput;

    /**
     * @param bool $isWalletEnabled
     */
    public function setIsWalletEnabled($isWalletEnabled)
    {
        $this->isWalletEnabled = $isWalletEnabled;
    }

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param CustomerInput $customerInput
     */
    public function setCustomerInput(CustomerInput $customerInput)
    {
        $this->customerInput = $customerInput;
    }

    /**
     * @Route(name="frontend.customer.index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $search = new Search();
        $search->setPageSize(2);

        return $this->render(':Customer:index.html.twig', [
            'paginatedResult' => $this->searchOrder->findOrders($this->getCustomer(), $search),
        ]);
    }

    /**
     * @Route("/create/{modal}", defaults={"modal" = false}, name="frontend.customer.create", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param bool $modal
     *
     * @return Response
     */
    public function createAction(Request $request, $modal = false)
    {
        $customer = new User();

        $form = $this->dynamicFormFactory->createForm('registration', $customer);

        $form->handleRequest($request);

        $template = $modal ? ':Security/forms:create_account.html.twig' : ':Customer:create.html.twig';

        if ($form->isValid()) {
            $customer->setIpAddress($request->getClientIp());

            try {
                $this->customerService->create($customer);

                $token = new AuthenticationToken($customer, $customer->getPassword(), 'default');

                // This has to take place before setToken otherwise we won't be getting the anonymous customer
                $this->buildOrder->mergeItems($this->getCustomer()->getLinioId(), $token->getUser());

                $this->tokenStorage->setToken($token);

                return $this->redirect($request->getSession()->get('_security.default.target_path', $this->generateUrl('frontend.default.index')));
            } catch (CustomerEmailExistsException $exception) {
                $this->addFlash('error', $exception->getMessage());
            } catch (CustomerException $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        foreach ($form->getErrors(true) as $error) {
            $this->addFlash('error', $error->getMessage());
        }

        return $this->render($template, ['form' => $form->createView()]);
    }

    /**
     * @Route("/password/recover/request", name="frontend.customer.password.recover.request")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function requestPasswordResetAction(Request $request)
    {
        $form = $this->formFactory->createBuilder(ForgotPasswordForm::class)
            ->add(
                'captcha',
                'ewz_recaptcha',
                ['mapped' => false, 'constraints' => [new RecaptchaTrue()], 'error_bubbling' => true]
            )
            ->setAction($this->generateUrl('frontend.customer.password.recover.request'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->customerService->requestPasswordReset($form->getData()['email'], $this->generateUrl('frontend.customer.password.recover', [], UrlGeneratorInterface::ABSOLUTE_URL));

                $this->addFlash('success', 'customer.password.recover.request.success');

                return $this->redirectToRoute('frontend.security.login');
            } catch (CustomerEmailExistsException $exception) {
                $this->addFlash('error', $exception->getMessage());
            } catch (CustomerException $exception) {
                $this->addFlash('error', $exception->getMessage());
            }
        }

        foreach ($form->getErrors(true) as $error) {
            $this->addFlash('error', $error->getMessage());
        }

        return $this->render(
            ':Security/recover:request_recovery.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/password/recover/{resetToken}", name="frontend.customer.password.recover", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param string $resetToken
     *
     * @return Response
     */
    public function resetPasswordAction(Request $request, string $resetToken = '')
    {
        $form = $this->formFactory->create(ResetPasswordForm::class);

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->customerService->resetPassword($resetToken, $form->getData()['email'], $form->getData()['password']);

                $this->addFlash('success', 'Contraseña cambiada con éxito');

                return $this->redirectToRoute('frontend.security.login');
            } catch (CustomerException $exception) {
                $this->addFlash('error', 'Ocurrio un error');
            }
        }

        foreach ($form->getErrors(true) as $error) {
            $this->addFlash('error', $error->getMessage());
        }

        return $this->render(
            ':Security/recover:change_password.html.twig',
            [
                'form' => $form->createView(),
                'resetToken' => $resetToken,
            ]
        );
    }

    /**
     * @Route("/change-password", name="frontend.customer.password.change")
     *
     * @return Response
     */
    public function changePassword(Request $request)
    {
        $form = $this->getFormFactory()->create(AccountChangePasswordForm::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->customerService->changePassword(
                    $this->getCustomer(),
                    $form->get('current')->getNormData(),
                    $form->get('new_password')->getNormData()
                );

                $this->addFlash('success', 'profile.edit.change_password.save.success');

                return $this->redirectToRoute('frontend.customer.profile.edit');
            } catch (CustomerException $exception) {
                $this->addFlash('error', 'profile.edit.change_password.save.failure');
            }
        }

        return $this->render(
            '::partial/components/customer/change_password.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/profile-edit", name="frontend.customer.profile.edit")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function editProfileAction(Request $request)
    {
        /* @var User $customer */
        $customer = clone $this->getCustomer();
        $customer->setIpAddress($request->getClientIp());

        $form = $this->getDynamicFormFactory()->createForm(
            'profile_update',
            $customer,
            [
                'action' => $this->generateUrl('frontend.customer.profile.edit'),
            ]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $this->getCustomerService()->update($customer);

                $this->updateCustomer($customer);

                $this->addFlash('success', 'profile.edit.save.success');
            } catch (Exception $exception) {
                $this->addFlash('error', 'profile.edit.save.failure');
            }
        }

        foreach ($form->getErrors(true) as $error) {
            $this->addFlash('error', $error->getMessage());
        }

        return $this->render(
            '::partial/components/customer/account_profile.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/coupons", name="frontend.customer.coupon")
     *
     * @return Response
     */
    public function couponsAction()
    {
        return $this->render(
            ':Customer:coupons.html.twig',
            [
                'coupons' => $this->customerService->getCoupons($this->getCustomer()),
            ]
        );
    }

    /**
     * @Route("/wallet", name="frontend.customer.wallet.detail")
     *
     * @return Response|RedirectResponse
     */
    public function walletDetailAction()
    {
        if (!$this->isWalletEnabled) {
            return $this->redirectToRoute('frontend.customer.index');
        }

        try {
            $wallet = $this->customerService->getWallet($this->getCustomer());
        } catch (UnregisteredWalletException $exception) {
            return $this->redirectToRoute('frontend.customer.wallet.create');
        } catch (CustomerException $exception) {
            $this->addFlash('error', $exception->getMessage());

            return $this->redirectToRoute('frontend.customer.index');
        }

        return $this->render(
            '::partial/components/customer/wallet.html.twig',
            ['wallet' => $wallet]
        );
    }

    /**
     * @Route("/register-wallet", name="frontend.customer.wallet.create", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response|RedirectResponse
     */
    public function registerWalletAction(Request $request)
    {
        if (!$this->isWalletEnabled) {
            return $this->redirectToRoute('frontend.customer.index');
        }

        if ($this->getCustomer()->hasWallet()) {
            return $this->redirectToRoute('frontend.customer.wallet.detail');
        }

        $accountForm = $this->dynamicFormFactory->createForm('wallet_account', [], ['csrf_protection' => false]);
        $accountForm->handleRequest($request);

        if ($accountForm->isValid()) {
            $walletData = $accountForm->getData();

            try {
                $this->customerService->registerWallet(
                    $this->getCustomer(),
                    $walletData['account'],
                    $walletData['holderName'],
                    $walletData['holderLastname']
                );

                $this->addFlash('success', 'electronic_wallet.membership_activated');
            } catch (RegisterWalletException $exception) {
                $this->addFlash('error', $exception->getMessage());

                return $this->render(
                    '::partial/components/customer/register_wallet.html.twig',
                    ['form' => $accountForm->createView()]
                );
            }

            return $this->redirectToRoute('frontend.customer.wallet.detail');
        }

        return $this->render(
            '::partial/components/customer/register_wallet.html.twig',
            ['form' => $accountForm->createView()]
        );
    }

    /**
     * @Route("/bank-transfer-confirmation", name="frontend.customer.bank.transfer.confirmation", methods={"GET", "POST"})
     *
     * @todo Refactor this to a split call with the submission being done via angular
     *
     * @param Request $request
     *
     * @return Response
     */
    public function bankTransferConfirmationAction(Request $request): Response
    {
        $form = $this->dynamicFormFactory->createForm('bank_transfer_confirmation');
        $orders = $this->searchOrder->findOrdersPendingBankConfirmation($this->getCustomer());
        $choices = [];

        foreach ($orders as $order) {
            $choices[$order->getId()] = sprintf('Pedido #%d (Bs. %s)', $order->getOrderNumber(), $order->getGrandTotal()->getMoneyAmount());
        }

        $form->add('orders', ChoiceType::class, [
            'choices' => $choices,
            'label' => $this->translator->trans('account.bank_transfer_confirmation'),
            'expanded' => true,
            'multiple' => true,
            'required' => true,
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $customer = $this->getCustomer();
            $bankTransferConfirmation = $this->customerInput->fromConfirmBankTransfer($data);
            $bankTransferConfirmation->setCustomer($customer);

            // Temporarily not adding this to the input itself as we would have to fetch the orders three times
            // It's already bad enough we do it twice
            foreach ($data['orders'] as $orderId) {
                $order = new Order($orderId);
                $bankTransferConfirmation->addOrder($order);
            }

            $bankTransferConfirmed = false;

            try {
                $this->customerService->createBankTransferConfirmation($bankTransferConfirmation);
                $bankTransferConfirmed = true;
            } catch (CustomerException $exception) {
                $this->addFlash('error', $exception->getMessage());
            }

            if ($bankTransferConfirmed) {
                return $this->redirectToRoute('frontend.customer.bank.transfer.confirmation');
            }
        }

        return $this->render(':Customer:bank_transfer_confirmation.html.twig', [
            'orders' => $orders,
            'form' => $form->createView(),
        ]);
    }
}
