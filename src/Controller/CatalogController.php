<?php

namespace Linio\Frontend\Controller;

use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Linio\Component\Cache\CacheService;
use Linio\Controller\FormAware;
use Linio\Controller\RouterAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Catalog\CampaignVoucherService;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Feed\FeedAware;
use Linio\Frontend\Form\CreateReviewForm;
use Linio\Frontend\Helper\HashIds;
use Linio\Frontend\Marketing\DataLayerAware;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductAware;
use Linio\Frontend\Search\SearchAware;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Seller\SellerService;
use Linio\Frontend\Seo\SeoAware;
use Linio\Frontend\Serializer\ProductDetailFilteredSimpleCollectionJsonSerializer as SimpleCollectionSerializer;
use Linio\Frontend\Serializer\SerializerAware;
use Linio\Frontend\SlugResolver\Exception\SlugNotFoundException;
use Linio\Frontend\SlugResolver\SlugResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route(service="controller.catalog")
 */
class CatalogController
{
    use TemplatingAware;
    use SearchAware;
    use CategoryAware;
    use SerializerAware;
    use SeoAware;
    use RouterAware;
    use FormAware;
    use TokenStorageAware;
    use DataLayerAware;
    use FeedAware;
    use ProductAware;

    /**
     * @var SlugResolverService
     */
    protected $slugResolverService;

    /**
     * @var array
     */
    protected $sortByOptions;

    /**
     * @var HashIds
     */
    protected $hashIds;

    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @var CampaignVoucherService
     */
    protected $campaignVoucherService;

    /**
     * @var bool
     */
    protected $isRecommendationEnabled;

    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var CacheService
     */
    protected $configurationCacheService;

    /**
     * @param CacheService $configurationCacheService
     */
    public function setConfigurationCacheService(CacheService $configurationCacheService)
    {
        $this->configurationCacheService = $configurationCacheService;
    }

    /**
     * @param SlugResolverService $slugResolverService
     */
    public function setSlugResolverService($slugResolverService)
    {
        $this->slugResolverService = $slugResolverService;
    }

    /**
     * @param array $options
     */
    public function setSortByOptions(array $options)
    {
        $this->sortByOptions = $options;
    }

    /**
     * @param HashIds $hashIds
     */
    public function setHashIds(HashIds $hashIds)
    {
        $this->hashIds = $hashIds;
    }

    /**
     * @param CmsService $cmsService
     */
    public function setCmsService(CmsService $cmsService)
    {
        $this->cmsService = $cmsService;
    }

    /**
     * @param CampaignVoucherService $campaignVoucherService
     */
    public function setCampaignVoucherService(CampaignVoucherService $campaignVoucherService)
    {
        $this->campaignVoucherService = $campaignVoucherService;
    }

    /**
     * @param bool $isRecommendationEnabled
     */
    public function setIsRecommendationEnabled($isRecommendationEnabled)
    {
        $this->isRecommendationEnabled = $isRecommendationEnabled;
    }

    /**
     * @param SellerService $sellerService
     */
    public function setSellerService(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/c/{category}/b/{brand}", name="frontend.catalog.index.category_brand", requirements={"category"="^.+", "brand"="^[0-9a-zA-Z\-]+"})
     * @ParamConverter("category", converter="slug", options={"resolver"="category"})
     * @ParamConverter("brand", converter="slug", options={"resolver"="brand"})
     *
     * @param Request $request
     * @param Category $category
     * @param Brand $brand
     *
     * @return Response
     */
    public function categoryBrandAction(Request $request, Category $category, Brand $brand)
    {
        if (!empty($request->getQueryString()) && $this->isCrawler($request)) {
            return $this->redirect($request->getPathInfo(), 301);
        }

        $requestQuery = clone $request->query;
        $requestQuery->add(['category' => $category->getId()]);
        $requestQuery->add(['brand' => $brand->getId()]);

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $categoryTree = $this->categoryService->buildTree($category->getId());
        $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
        $facetCollection = $searchResults->getPrimarySearchResult()->getFacetCollection();

        foreach ($facetCollection as $facet) {
            if ($facet->getName() == 'brand') {
                $facetCollection->removeElement($facet);
            }
        }

        $searchResults->getPrimarySearchResult()->setFacetCollection($facetCollection);

        $this->seoService->setUpSeoForCategoryAndBrand($category, $brand);

        $this->dataLayerService->populateCategoryBrand($category, $searchResults->getOriginalSearchResult(), $brand);

        return $this->render(
            ':Catalog:category_brand.html.twig',
            [
                'searchResults' => $searchResults,
                'categoryTree' => $categoryTree,
                'sortByOptions' => $this->sortByOptions,
                'catalogTitle' => sprintf('%s / %s', $category->getName(), $brand->getName()),
                'brand' => $brand,
            ]
        );
    }

    /**
     * @Route("/c/{category}", name="frontend.catalog.index.category", requirements={"category"="^.+"})
     * @ParamConverter("category", converter="slug", options={"resolver"="category"})
     *
     * @param Request  $request
     * @param Category $category
     *
     * @return Response
     */
    public function categoryAction(Request $request, Category $category)
    {
        if (!empty($request->getQueryString()) && $this->isCrawler($request)) {
            return $this->redirect($request->getPathInfo(), 301);
        }

        if (!$request->query->has('q')) {
            $departmentPage = $this->cmsService->staticPage($category->getId(), 'category');
            $this->seoService->setUpStaticPageSeo($departmentPage);
            $categoryName = $category->getName();

            if ($departmentPage) {
                return $this->render(':Catalog:department.html.twig', [
                    'data' => $departmentPage,
                    'category' => $categoryName,
                ]);
            }
        }

        $catalogGrids = $this->cmsService->getCatalogGrids('category', $category->getId());

        $requestQuery = clone $request->query;

        if ($category->getId() != CategoryService::ROOT_CATEGORY) {
            $requestQuery->add(['category' => $category->getId()]);
        }

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => (string) $seller->getSellerId()]);
            }
        }

        $categoryTree = $this->categoryService->buildTree($category->getId());
        $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
        $this->seoService->setUpCategorySeo($category->getId());

        $this->dataLayerService->populateCategory($category, $searchResults->getOriginalSearchResult());

        return $this->render(':Catalog:category.html.twig', [
            'searchResults' => $searchResults,
            'categoryTree' => $categoryTree,
            'sortByOptions' => $this->sortByOptions,
            'catalogTitle' => $category->getName(),
            'catalogGrids' => $catalogGrids,
            'category' => $category,
        ]);
    }

    /**
     * @Route("/b/{brandSlug}", name="frontend.catalog.index.brand", requirements={"brandSlug"="^[0-9a-zA-Z\-]+"})
     * @ParamConverter("brandSlug", converter="slug", options={"resolver"="brand"})
     *
     * @param Request $request
     * @param Brand $brandSlug
     *
     * @return Response
     */
    public function brandAction(Request $request, Brand $brandSlug)
    {
        if (!empty($request->getQueryString()) && $this->isCrawler($request)) {
            return $this->redirect($request->getPathInfo(), 301);
        }

        if (!$request->query->has('q')) {
            $brandStaticPage = $this->cmsService->staticPage($brandSlug->getId(), 'brand');
            $this->seoService->setUpStaticPageSeo($brandStaticPage);

            if ($brandStaticPage) {
                return $this->render(':Cms/content_type:static_page.html.twig', [
                    'data' => $brandStaticPage,
                ]);
            }
        }

        $catalogGrids = $this->cmsService->getCatalogGrids('brand', $brandSlug->getId());

        $requestQuery = clone $request->query;
        $requestQuery->add(['brand' => $brandSlug->getId()]);

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $this->seoService->setupBrandSeo($brandSlug);

        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);
        $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
        $facetCollection = $searchResults->getPrimarySearchResult()->getFacetCollection();

        foreach ($facetCollection as $facet) {
            if ($facet->getName() == 'brand') {
                $facetCollection->removeElement($facet);
            }
        }

        $searchResults->getPrimarySearchResult()->setFacetCollection($facetCollection);
        $this->seoService->setUpCategorySeo(CategoryService::ROOT_CATEGORY);

        $this->dataLayerService->populateBrand($brandSlug);

        return $this->render(
            ':Catalog:brand.html.twig',
            [
                'searchResults' => $searchResults,
                'categoryTree' => $categoryTree,
                'sortByOptions' => $this->sortByOptions,
                'catalogTitle' => $brandSlug->getName(),
                'catalogGrids' => $catalogGrids,
            ]
        );
    }

    /**
     * @Route("/s/{seller}", name="frontend.catalog.index.seller", requirements={"seller"="^[0-9a-zA-Z\-]+"})
     * @ParamConverter("seller", converter="slug", options={"resolver"="seller"})
     *
     * @param Request $request
     * @param Seller $seller
     *
     * @return Response
     */
    public function sellerAction(Request $request, Seller $seller)
    {
        if (!empty($request->getQueryString()) && $this->isCrawler($request)) {
            return $this->redirect($request->getPathInfo(), 301);
        }

        $catalogGrids = $this->cmsService->getCatalogGrids('supplier', $seller->getSellerId());

        $requestQuery = clone $request->query;

        $requestQuery->add(['seller.id' => (string) $seller->getSellerId()]);

        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);
        $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
        $this->seoService->setupSellerSeo($seller);

        $seller = $this->sellerService->get($seller->getSellerId());

        return $this->render(
            ':Catalog:seller.html.twig',
            [
                'searchResults' => $searchResults,
                'categoryTree' => $categoryTree,
                'sortByOptions' => $this->sortByOptions,
                'catalogTitle' => $seller->getName(),
                'catalogGrids' => $catalogGrids,
                'seller' => $seller,
            ]
        );
    }

    /**
     * @Route("/p/{productSlug}", name="frontend.catalog.detail", requirements={"productSlug"="^[0-9a-zA-Z\-]+"})
     *
     * @param Request $request
     * @param $productSlug
     *
     * @return Response
     */
    public function detailAction(Request $request, $productSlug)
    {
        /** @var Product $product */
        $product = $this->slugResolverService->resolve('product', $productSlug);

        $showOtherSellers = true;

        if ($request->query->get('s')) {
            $product = $this->productService->getPlacedBySeller($product, $request->query->get('s'));
            $showOtherSellers = false;
        } else {
            if ($product->getMarketplaceParentSku()) {
                try {
                    $parent = $this->productService->getBySku($product->getMarketplaceParentSku());

                    return $this->redirectToRoute('frontend.catalog.detail', ['productSlug' => $parent->getSlug()], 301);
                } catch (ProductNotFoundException $exception) {
                    // Display the children product.
                }
            }

            if ($product->getSlug() !== $productSlug) {
                return $this->redirectToRoute('frontend.catalog.detail', ['productSlug' => $product->getSlug()], 301);
            }
        }

        $disclaimerMessages = $this->getProductDisclaimers($product);

        $categoryTree = $this->categoryService->buildTree($product->getCategory()->getId());

        $campaignSlug = $request->query->get('campaign');

        if ($campaignSlug) {
            try {
                $campaign = $this->slugResolverService->resolve('campaign', $campaignSlug);
                $product->filterCampaignProducts($campaign);
                $this->campaignVoucherService->mapVouchersFromCampaign($product);
            } catch (SlugNotFoundException $exception) {
                // Do Nothing
            }
        }

        $this->seoService->setUpProductSeo($product);

        $reviewForm = $this->createForm(new CreateReviewForm(), new Review(), [
            'action' => $this->generateUrl('frontend.catalog.product.review', ['product' => $product->getSlug()]),
        ]);

        if ($this->getCustomer() instanceof GuestUser) {
            $reviewForm->add('captcha', 'ewz_recaptcha', [
                'mapped' => false,
                'constraints' => [
                    new RecaptchaTrue(),
                ],
                'error_bubbling' => true,
            ]);
        }

        $simpleSerializer = new SimpleCollectionSerializer();

        $imagesJson = $this->serializer->serialize($product->getImages(), 'json');

        $simplesJson = $simpleSerializer->serialize($product->getSimples());

        $firstSimpleJson = $simpleSerializer->serialize($product->getFirstSimple());

        $productSku = $this->serializer->serialize($product->getSku(), 'json');

        $productJsonTemplate = '{"images":%s,"simples":%s,"firstSimple":%s,"sku":%s, "name":"%s"}';

        $productJson = sprintf($productJsonTemplate, $imagesJson, $simplesJson, $firstSimpleJson, $productSku, htmlspecialchars($product->getName()));

        $this->dataLayerService->populateProduct($product);

        return $this->render(':Catalog:detail.html.twig', [
            'product' => $product,
            'productJson' => $productJson,
            'categoryTree' => $categoryTree,
            'reviewForm' => $reviewForm->createView(),
            'disclaimerMessages' => $disclaimerMessages,
            'showOtherSellers' => $showOtherSellers,
        ]);
    }

    /**
     * @Route("/cm/{campaign}", name="frontend.catalog.index.campaign", requirements={"campaign"="^.+"})
     * @ParamConverter("campaign", converter="slug", options={"resolver"="campaign"})
     *
     * @param Request $request
     * @param Campaign $campaign
     *
     * @return Response
     */
    public function campaignAction(Request $request, Campaign $campaign)
    {
        if (!empty($request->getQueryString()) && $this->isCrawler($request)) {
            return $this->redirect($request->getPathInfo(), 301);
        }

        $slug = rtrim(substr($request->getPathInfo(), 4), '/');

        if (!$request->query->has('q') && $campaign->getSlug() == $slug) {
            $campaignStaticPage = $this->cmsService->staticPage($campaign->getId(), 'campaign');
            $this->seoService->setUpStaticPageSeo($campaignStaticPage);

            if ($campaignStaticPage) {
                return $this->render(':Cms/content_type:static_page.html.twig', [
                    'data' => $campaignStaticPage,
                ]);
            }
        }

        $catalogGrids = [];
        $currentSegment = $campaign->getCurrentSegment();

        if ($currentSegment) {
            $catalogGrids = $this->cmsService->getCatalogGrids('segment', $currentSegment->getId());
        }

        if (empty(array_filter($catalogGrids))) {
            $catalogGrids = $this->cmsService->getCatalogGrids('campaign', $campaign->getId());
        }

        $requestQuery = clone $request->query;

        $requestQuery->add(['campaign_skus' => $campaign->getSkus()]);

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $this->seoService->setupCampaignSeo($campaign);

        $searchResults = $this->searchService->getCampaignProducts($requestQuery, $campaign);
        $campaign->setCount($searchResults->getOriginalSearchResult()->getTotalItemsFound());

        if ($currentSegment) {
            $catalogTitle = sprintf('%s / %s', $campaign->getName(), $campaign->getCurrentSegment()->getName());
        } else {
            $catalogTitle = $campaign->getName();
        }

        return $this->render(
            ':Catalog:index.html.twig',
            [
                'searchResults' => $searchResults,
                'categoryTree' => $campaign,
                'sortByOptions' => $this->sortByOptions,
                'catalogTitle' => $catalogTitle,
                'catalogGrids' => $catalogGrids,
            ]
        );
    }

    /**
     * @Route("/search", name="frontend.catalog.index.search")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request)
    {
        if (empty(trim($request->query->get('q')))) {
            return $this->redirectToRoute('frontend.default.index');
        }

        if (!empty($request->getQueryString()) && $this->isCrawler($request)) {
            return $this->redirect($request->getPathInfo(), 301);
        }

        // If the customer is searching for a sku, redirect them to the product details
        if (preg_match('/^[A-Z0-9]{12,}(\-\d+)?$/', $request->query->get('q')) === 1) {
            try {
                $product = $this->productService->getBySku($request->query->get('q'));

                return $this->redirectToRoute('frontend.catalog.detail', ['productSlug' => $product->getSlug()]);
            } catch (ProductNotFoundException $exception) {
                // Do nothing if it doesn't exist
            }
        }

        $requestQuery = clone $request->query;

        if ($requestQuery->has('seller')) {
            $seller = $this->slugResolverService->resolve('seller', $requestQuery->get('seller'));

            if ($seller) {
                $requestQuery->add(['seller.id' => $seller->getSellerId()]);
            }
        }

        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);
        $searchResults = $this->searchService->searchProducts($requestQuery, $categoryTree);
        $this->seoService->setUpCategorySeo(CategoryService::ROOT_CATEGORY);

        $categoryTree->sortByCount();

        $this->dataLayerService->populateSearch();

        return $this->render(
            ':Catalog:index.html.twig',
            [
                'searchResults' => $searchResults,
                'categoryTree' => $categoryTree,
                'sortByOptions' => $this->sortByOptions,
                'catalogTitle' => 'Resultados de la búsqueda',
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isCrawler(Request $request)
    {
        $userAgent = $request->headers->get('User-Agent');
        $bots = [
            'bingbot',
            'Googlebot',
            'YandexImages',
            'Baiduspider',
        ];
        $regex = sprintf('/%s/', implode('|', $bots));

        if (preg_match($regex, $userAgent)) {
            return true;
        }

        return false;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function getProductDisclaimers(Product $product)
    {
        $disclaimerCategories = $this->configurationCacheService->get('disclaimer_categories');
        $disclaimerMessages = [];

        if (!isset($disclaimerCategories)) {
            return $disclaimerMessages;
        }

        foreach ($disclaimerCategories as $disclaimerBlock) {
            if (in_array($product->getCategory()->getId(), $disclaimerBlock['categories'])) {
                $disclaimerMessages[] = $this->translator->trans($disclaimerBlock['message']);
            }
        }

        return $disclaimerMessages;
    }
}
