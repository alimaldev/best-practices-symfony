<?php

namespace Linio\Frontend\Controller;

use Linio\Controller\TemplatingAware;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Seo\SeoAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route(service="controller.cms")
 */
class CmsController
{
    use TemplatingAware;
    use SeoAware;

    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @param CmsService $cmsService
     */
    public function setCmsService(CmsService $cmsService)
    {
        $this->cmsService = $cmsService;
    }

    /**
     * @Route("/sp/{slug}", name="frontend.default.static_page", requirements={"slug": ".+"})
     *
     * @param Request $request
     * @param string $slug
     *
     * @return Response
     */
    public function staticPageAction(Request $request, $slug)
    {
        $staticPageData = $this->cmsService->staticPage($slug);
        $this->seoService->setUpStaticPageSeo($staticPageData);

        if (!$staticPageData) {
            throw new NotFoundHttpException($request->getUri() . ' from ' . $request->headers->get('referer'));
        }

        return $this->render(':Cms/content_type:static_page.html.twig', ['data' => $staticPageData]);
    }
}
