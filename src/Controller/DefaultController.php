<?php

namespace Linio\Frontend\Controller;

use Linio\Controller\FlashMessageAware;
use Linio\Controller\TemplatingAware;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Cms\CmsService;
use Linio\Frontend\Marketing\DataLayerAware;
use Linio\Frontend\Seo\SeoAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route(service="controller.default")
 */
class DefaultController
{
    use FlashMessageAware;
    use TemplatingAware;
    use CategoryAware;
    use SeoAware;
    use DataLayerAware;

    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @param CmsService $cmsService
     */
    public function setCmsService(CmsService $cmsService)
    {
        $this->cmsService = $cmsService;
    }

    /**
     * @Route(name="frontend.default.index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $categoryTree = $this->categoryService->buildTree(CategoryService::ROOT_CATEGORY);
        $this->seoService->setUpHomeSeo();

        $this->dataLayerService->populateHome();

        $newsletterStatus = $this->session->getFlashBag()->get('newsletterStatus', []);
        $newsletterCoupon = $this->session->getFlashBag()->get('newsletterCoupon', []);

        return $this->render(':Default:index.html.twig', [
            'categoryTree' => $categoryTree,
            'newsletterStatus' => array_pop($newsletterStatus),
            'newsletterCoupon' => array_pop($newsletterCoupon),
        ]);
    }

    /**
     * @Route("/directorio", name="frontend.default.site-directory")
     */
    public function siteDirectoryAction()
    {
        $siteDirectory = $this->cmsService->buildSiteDirectory();

        return $this->render(':partial:directory.html.twig', [
            'siteDirectory' => $siteDirectory,
        ]);
    }
}
