<?php

namespace Linio\Frontend\EventListener;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class TelesalesListener
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->get('_route') !== 'frontend.security.telesales.login') {
            return;
        }

        $utm = [];
        $utm['source'] = $request->query->get('utm_source', '');
        $utm['medium'] = $request->query->get('utm_medium', '');
        $utm['campaign'] = $request->query->get('utm_campaign', '');

        $this->session->set('telesales/utm', $utm);
    }
}
