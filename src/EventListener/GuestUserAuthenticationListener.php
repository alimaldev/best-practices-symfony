<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Security\GuestUser;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class GuestUserAuthenticationListener implements ListenerInterface
{
    const LINIO_ID_HEADER = 'X-LINIO-ID';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        string $secret,
        LoggerInterface $logger = null,
        AuthenticationManagerInterface $authenticationManager = null
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->secret = $secret;
        $this->authenticationManager = $authenticationManager;
        $this->logger = $logger;
    }

    /**
     * Handles anonymous authentication.
     *
     * @param GetResponseEvent $event A GetResponseEvent instance
     */
    public function handle(GetResponseEvent $event)
    {
        if ($this->tokenStorage->getToken() !== null) {
            return;
        }

        $linioId = $event->getRequest()->headers->get(self::LINIO_ID_HEADER);

        if (!$linioId) {
            $linioId = $event->getRequest()->cookies->get('linio_id');
        }

        $guestUser = new GuestUser($linioId, $event->getRequest()->getClientIp());

        try {
            $token = new AnonymousToken($this->secret, $guestUser, []);

            if ($this->authenticationManager !== null) {
                $token = $this->authenticationManager->authenticate($token);
            }

            $this->tokenStorage->setToken($token);

            $this->createInfoLog('Populated the TokenStorage with an anonymous Token.');
        } catch (AuthenticationException $failed) {
            $this->createInfoLog('Anonymous authentication failed.', ['exception' => $failed]);
        }
    }

    /**
     * @param string $message
     * @param array $context
     */
    private function createInfoLog(string $message, array $context = [])
    {
        if ($this->logger !== null) {
            $this->logger->info($message, $context);
        }
    }
}
