<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Linio\Frontend\Security\Jwt\Exception\JwtExtractionFailedException;
use Linio\Frontend\Security\Jwt\TokenManager;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class JwtAuthenticationFailureListener
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @param TokenManager $tokenManager
     */
    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        try {
            $this->tokenManager->removeTokenExtractedFromRequest($event->getRequest());
        } catch (JwtExtractionFailedException $error) {
            // Do nothing.
        }

        throw new AuthenticationException();
    }
}
