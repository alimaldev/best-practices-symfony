<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestIdListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $headers = $event->getRequest()->headers;

        if ($headers->has('X-Request-ID')) {
            return;
        }

        $headers->set('X-Request-ID', uniqid());
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $event->getResponse()->headers->set('X-Request-ID', $event->getRequest()->headers->get('X-Request-ID'));
    }
}
