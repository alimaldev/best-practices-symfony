<?php

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Product\ProductService;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ProductDebugLogListener
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($event->getRequest()->query->has('SDEBUG')) {
            $this->productService->enableDebugLog();
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->productService->getDebugLog()) {
            $content = sprintf('%s<!-- %s -->', $event->getResponse()->getContent(), $this->productService->getDebugLog());
            $event->getResponse()->setContent($content);
        }
    }

    /**
     * @param ProductService $cmsService
     */
    public function setProductService(ProductService $cmsService)
    {
        $this->productService = $cmsService;
    }
}
