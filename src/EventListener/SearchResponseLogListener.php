<?php

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Search\Adapter\HawkAdapter;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class SearchResponseLogListener
{
    /**
     * @var HawkAdapter
     */
    protected $hawkAdapter;

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($event->getRequest()->query->has('SDEBUG')) {
            $this->hawkAdapter->enableSearchResponseLog();
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->hawkAdapter->getSearchResponseLog()) {
            $content = sprintf('%s<!-- %s -->', $event->getResponse()->getContent(), $this->hawkAdapter->getSearchResponseLog());
            $event->getResponse()->setContent($content);
        }
    }

    /**
     * @param HawkAdapter $hawkAdapter
     */
    public function setHawkAdapter(HawkAdapter $hawkAdapter)
    {
        $this->hawkAdapter = $hawkAdapter;
    }
}
