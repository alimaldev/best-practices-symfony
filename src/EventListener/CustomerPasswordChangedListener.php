<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Event\CustomerPasswordChangedEvent;
use Linio\Frontend\Security\Jwt\TokenManager;

class CustomerPasswordChangedListener
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @param TokenManager $tokenManager
     */
    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    public function onCustomerPasswordChanged(CustomerPasswordChangedEvent $event)
    {
        $this->tokenManager->flushTokens($event->getUser());
    }
}
