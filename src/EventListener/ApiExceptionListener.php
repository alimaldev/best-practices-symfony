<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Linio\Exception\NotFoundHttpException as LinioNotFoundException;
use Linio\Frontend\Api\Response\ErrorJsonResponse;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as SymfonyNotFoundException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Translation\TranslatorInterface;

class ApiExceptionListener
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!in_array('application/json', $event->getRequest()->getAcceptableContentTypes())) {
            return;
        }

        $exception = $event->getException();

        switch (true) {
            case $exception instanceof InputException:
                $response = $this->buildErrorJsonResponse($exception, ErrorJsonResponse::HTTP_BAD_REQUEST);
                $level = LogLevel::INFO;
                break;
            case $exception instanceof DomainException:
                $response = $this->buildErrorJsonResponse($exception);
                $level = LogLevel::ERROR;
                break;
            case $exception instanceof SymfonyNotFoundException:
            case $exception instanceof ResourceNotFoundException:
            case $exception instanceof LinioNotFoundException:
                $exception = new DomainException(
                    $exception->getMessage(),
                    [],
                    ErrorJsonResponse::HTTP_NOT_FOUND,
                    $exception
                );
                $response = $this->buildErrorJsonResponse($exception, ErrorJsonResponse::HTTP_NOT_FOUND);
                $level = LogLevel::WARNING;
                break;
            default:
                $response = new ErrorJsonResponse($this->translator->trans('error_500_page.something_is_wrong'));
                $level = LogLevel::CRITICAL;
        }

        $event->setResponse($response);
        $this->logger->log($level, $exception->getMessage(), ['exception' => $exception]);
    }

    protected function buildErrorJsonResponse(DomainException $exception, int $statusCode = ErrorJsonResponse::HTTP_INTERNAL_SERVER_ERROR): ErrorJsonResponse
    {
        $translatedMessage = $this->translator->trans($exception->getMessage());

        // This is the only way to handle fallback messages
        if ($translatedMessage == $exception->getMessage()) {
            $translatedMessage = $this->translator->trans('error_500_page.something_is_wrong');
        }

        return new ErrorJsonResponse($translatedMessage, $statusCode, $exception->getContext());
    }
}
