<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use DateTime;
use Linio\Frontend\Security\TokenStorageAware;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class LinioIdListener
{
    use TokenStorageAware;

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->getRequest()->getSession()) {
            return;
        }

        // Don't set the cookie when on ^/(_(profiler|wdt|error)|css|images|js)/
        if (preg_match('/^\/(_(profiler|wdt|error)|css|images|js)/', $event->getRequest()->getPathInfo())) {
            return;
        }

        $linioId = $this->getCustomer()->getLinioId();
        $cookie = new Cookie('linio_id', $linioId, new DateTime('+1000 years'), '/', null, false, false);

        $event->getResponse()->headers->setCookie($cookie);
        $event->getResponse()->headers->set(
            GuestUserAuthenticationListener::LINIO_ID_HEADER,
            $linioId
        );
    }
}
