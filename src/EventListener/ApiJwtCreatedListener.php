<?php

namespace Linio\Frontend\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Linio\Frontend\Security\User;

class ApiJwtCreatedListener
{
    /**
     * @param JWTCreatedEvent $event
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();

        $payload = $event->getData();

        $payload['user_id'] = $user->getId();

        $event->setData($payload);
    }
}
