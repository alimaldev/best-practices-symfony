<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Ekino\Bundle\NewRelicBundle\NewRelic\NewRelicInteractorInterface;
use Linio\Frontend\Security\TokenStorageAware;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class NewRelicListener
{
    use TokenStorageAware;

    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @var NewRelicInteractorInterface
     */
    protected $newRelic;

    /**
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @param NewRelicInteractorInterface $newRelic
     */
    public function setNewRelic(NewRelicInteractorInterface $newRelic)
    {
        $this->newRelic = $newRelic;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $customer = $this->getCustomer();

        $this->newRelic->addCustomParameter('linio_id', $customer->getLinioId());
        $this->newRelic->addCustomParameter('customer_id', $customer->getId());
        $this->newRelic->addCustomParameter('store', $this->countryCode);
        $this->newRelic->addCustomParameter('requestId', $event->getRequest()->headers->get('X-Request-ID'));
    }
}
