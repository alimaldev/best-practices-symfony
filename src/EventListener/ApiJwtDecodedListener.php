<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Linio\Frontend\Security\Jwt\TokenManager;

class ApiJwtDecodedListener
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @param TokenManager $tokenManager
     */
    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * @param JWTDecodedEvent $event
     */
    public function onJWTDecoded(JWTDecodedEvent $event)
    {
        if (!$event->getRequest()) {
            return;
        }

        $token = $this->tokenManager->getTokenFromRequest($event->getRequest());

        if (!$this->tokenManager->isPayloadValid($token) || !$this->tokenManager->tokenExists($token)) {
            $event->markAsInvalid();
        }
    }
}
