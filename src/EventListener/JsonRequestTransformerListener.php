<?php

namespace Linio\Frontend\EventListener;

use Exception;
use Linio\Component\Util\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class JsonRequestTransformerListener
{
    protected $jsonContentTypes = ['json', 'application/json'];

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (!$this->isJsonRequest($request)) {
            return;
        }

        try {
            $data = Json::decode($request->getContent());
            $request->request->replace(is_array($data) ? $data : []);
        } catch (Exception $e) {
            $response = Response::create($e->getMessage(), Response::HTTP_BAD_REQUEST);
            $event->setResponse($response);
        }
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function isJsonRequest(Request $request)
    {
        return in_array($request->getContentType(), $this->jsonContentTypes);
    }
}
