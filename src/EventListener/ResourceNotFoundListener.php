<?php

namespace Linio\Frontend\EventListener;

use Linio\Exception\NotFoundHttpException as LinioNotFoundException;
use Linio\Frontend\Helper\HashIds;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\SlugResolver\Exception\SlugNotFoundException;
use Linio\Frontend\SlugResolver\SlugResolverService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as SymfonyNotFoundException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class ResourceNotFoundListener
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var HashIds
     */
    protected $hashIds;

    /**
     * @var SlugResolverService
     */
    protected $slugResolverService;

    /**
     * @param RouterInterface $router
     * @param HashIds $hashIds
     * @param SlugResolverService $slugResolverService
     */
    public function __construct(RouterInterface $router, HashIds $hashIds, SlugResolverService $slugResolverService)
    {
        $this->router = $router;
        $this->hashIds = $hashIds;
        $this->slugResolverService = $slugResolverService;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     *
     * @return RedirectResponse
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $request = $event->getRequest();

        if (!($exception instanceof SymfonyNotFoundException) && !($exception instanceof SlugNotFoundException) && !($exception instanceof LinioNotFoundException)) {
            return;
        }

        try {
            $matches = [];

            if (substr($request->getPathInfo(), -5) === '.html' && preg_match('/^(\/|.+\-)(?<productId>\d+)\.html$/', $request->getPathInfo(), $matches)) {
                $productHashId = $this->hashIds->getHashId($matches['productId']);

                try {
                    $product = $this->slugResolverService->resolve('product', $productHashId);

                    $params = ['productSlug' => $product->getSlug()];

                    if ($request->query->count() > 0) {
                        $params = array_merge($params, $request->query->all());
                    }

                    $event->setResponse(new RedirectResponse($this->router->generate('frontend.catalog.detail', $params), 301));

                    return;
                } catch (SlugNotFoundException $exception) {
                    // Do nothing, continue trying to find a redirect for product.
                }
            }

            $originalPathInfo = rtrim($request->getPathInfo(), '/');
            $url = $this->slugResolverService->resolveLegacy($originalPathInfo)->getUrl();
            $url = rtrim($url, '?/');

            if ($url === $originalPathInfo) {
                throw new SlugNotFoundException('Slug matches the originally requested slug. Exiting to prevent redirect loop.');
            }

            $queryParameters = $request->getQueryString();

            if (!empty($queryParameters)) {
                $url = sprintf('%s/?%s', $url, $queryParameters);
            }

            // Prevent infinite redirects by making sure the url matches
            $this->router->match($url);

            $event->setResponse(new RedirectResponse($url, 301));

            return;
        } catch (SlugNotFoundException $exception) {
            // Do nothing, continue to Symfony's not found exception.
        } catch (ProductNotFoundException $exception) {
            // Do nothing, continue to Symfony's not found exception.
        } catch (ResourceNotFoundException $exception) {
            // Do nothing, continue to Symfony's not found exception.
        }

        $event->setException(new SymfonyNotFoundException());
    }
}
