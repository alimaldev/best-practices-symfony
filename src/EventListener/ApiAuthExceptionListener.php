<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Api\Response\ErrorJsonResponse;
use Linio\Frontend\Exception\ExceptionMessage;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Translation\TranslatorInterface;

class ApiAuthExceptionListener
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (!in_array('application/json', $event->getRequest()->getAcceptableContentTypes())) {
            return;
        }

        if (!$this->shouldCatch($event->getException())) {
            return;
        }

        $exception = $event->getException();
        $this->logger->log(LogLevel::DEBUG, $exception->getMessage(), ['exception' => $exception]);

        $response = new ErrorJsonResponse($this->translator->trans(ExceptionMessage::UNAUTHORIZED), ErrorJsonResponse::HTTP_UNAUTHORIZED);

        $event->setResponse($response);
    }

    protected function shouldCatch(\Throwable $throwable): bool
    {
        return
            $throwable instanceof AuthenticationException
            || $throwable instanceof AccessDeniedException
            || $throwable instanceof AccessDeniedHttpException
            || $throwable instanceof UnauthorizedHttpException
        ;
    }
}
