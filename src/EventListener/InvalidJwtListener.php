<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Linio\Frontend\Api\Response\ErrorJsonResponse;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Security\Jwt\Exception\JwtExtractionFailedException;
use Linio\Frontend\Security\Jwt\TokenManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;

class InvalidJwtListener
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TokenManager $tokenManager
     * @param TranslatorInterface $translator
     */
    public function __construct(TokenManager $tokenManager, TranslatorInterface $translator)
    {
        $this->tokenManager = $tokenManager;
        $this->translator = $translator;
    }

    /**
     * @param JWTInvalidEvent $event
     */
    public function onInvalidJwt(JWTInvalidEvent $event)
    {
        try {
            $this->tokenManager->removeTokenExtractedFromRequest($event->getRequest());
        } catch (JwtExtractionFailedException $error) {
            // Do nothing.
        }

        $response = new ErrorJsonResponse(
            $this->translator->trans(ExceptionMessage::JWT_INVALID),
            JsonResponse::HTTP_UNAUTHORIZED
        );

        $event->setResponse($response);
    }
}
