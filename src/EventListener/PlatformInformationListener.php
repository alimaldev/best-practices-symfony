<?php

declare(strict_types=1);

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Request\PlatformInformation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class PlatformInformationListener
{
    /**
     * @var array
     */
    protected $storeIdMap;

    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @param array $storeIdMap
     * @param PlatformInformation $platformInformation
     */
    public function __construct(array $storeIdMap, PlatformInformation $platformInformation)
    {
        $this->storeIdMap = $storeIdMap;
        $this->platformInformation = $platformInformation;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $platform = $this->getPlatformId($event->getRequest());

        $this->platformInformation->setPlatform($platform);
        $this->platformInformation->setStoreId($this->storeIdMap[$platform]);
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getPlatformId(Request $request): string
    {
        $userAgent = $request->headers->get('User-Agent');

        if (!$userAgent) {
            return 'desktop';
        }

        if (strpos($userAgent, 'linio_android') !== false) {
            return 'android';
        }

        if (strpos($userAgent, 'linio_ios') !== false) {
            return 'ios';
        }

        return 'desktop';
    }
}
