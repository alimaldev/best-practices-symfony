<?php

namespace Linio\Frontend\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Linio\Frontend\Order\BuildOrder;
use Linio\Frontend\Security\Jwt\TokenManager;
use Linio\Frontend\Security\User;

class AuthenticationSuccessListener
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @param TokenManager $tokenManager
     * @param BuildOrder $buildOrder
     */
    public function __construct(TokenManager $tokenManager, BuildOrder $buildOrder)
    {
        $this->tokenManager = $tokenManager;
        $this->buildOrder = $buildOrder;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();
        $data = $event->getData();
        $request = $event->getRequest();

        if (!($this->supportsClass(get_class($user)))) {
            return;
        }

        $data['id'] = $user->getId();
        $data['linioId'] = $user->getLinioId();
        $data['username'] = $user->getUsername();
        $data['firstName'] = $user->getFirstName();
        $data['lastName'] = $user->getLastName();
        $data['linioPlus'] = $user->isSubscribedToLinioPlus();

        $event->setData($data);

        $this->tokenManager->add($data['token'], $user);

        if ($request->headers->has(GuestUserAuthenticationListener::LINIO_ID_HEADER)) {
            $this->buildOrder->mergeItems(
                $request->headers->get(GuestUserAuthenticationListener::LINIO_ID_HEADER),
                $user
            );
        }
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    protected function supportsClass($class)
    {
        return $class === User::class;
    }
}
