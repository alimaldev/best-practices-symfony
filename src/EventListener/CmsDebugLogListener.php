<?php

namespace Linio\Frontend\EventListener;

use Linio\Frontend\Cms\CmsService;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class CmsDebugLogListener
{
    /**
     * @var CmsService
     */
    protected $cmsService;

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($event->getRequest()->query->has('SDEBUG')) {
            $this->cmsService->enableDebugLog();
        }
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->cmsService->getDebugLog()) {
            $content = sprintf('%s<!-- %s -->', $event->getResponse()->getContent(), $this->cmsService->getDebugLog());
            $event->getResponse()->setContent($content);
        }
    }

    /**
     * @param CmsService $cmsService
     */
    public function setCmsService(CmsService $cmsService)
    {
        $this->cmsService = $cmsService;
    }
}
