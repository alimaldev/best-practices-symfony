<?php

namespace Linio\Frontend\Entity\Product;

use DateTime;
use Linio\Frontend\Entity\Product;
use Linio\Type\Dictionary;
use Linio\Type\Money;

class Simple
{
    /**
     * @var string
     */
    protected $sku;

    /**
     * @var bool
     */
    protected $isActive = true;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @var Money
     */
    protected $price;

    /**
     * @var Money
     */
    protected $originalPrice;

    /**
     * @var float
     */
    protected $percentageOff;

    /**
     * @var Money
     */
    protected $savedAmount;

    /**
     * @var int
     */
    protected $stock;

    /**
     * @var float
     */
    protected $taxPercent;

    /**
     * A reference to its config product.
     *
     * @var Product
     */
    protected $product;

    /**
     * @var Attribute[]
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $linioPlusLevel;

    /**
     * @var bool
     */
    protected $hasFreeShipping;

    /**
     * @var int
     */
    protected $minimumDeliveryDays;

    /**
     * @var int
     */
    protected $maximumDeliveryDays;

    /**
     * @var bool
     */
    protected $expressInvoiceAvailable;

    /**
     * @var string
     */
    protected $conditionTypeNote;

    /**
     * @var string
     */
    protected $packageContent;

    /**
     * @var string
     */
    protected $warranty;

    public function __construct()
    {
        $this->price = new Money();
        $this->originalPrice = new Money();
        $this->attributes = new Dictionary();
    }

    /**
     * @return float
     */
    public function getTaxPercent()
    {
        return $this->taxPercent;
    }

    /**
     * @param float $taxPercent
     */
    public function setTaxPercent($taxPercent)
    {
        $this->taxPercent = $taxPercent;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Money $price
     */
    public function setPrice(Money $price)
    {
        $this->price = $price;
    }

    /**
     * @return Money
     */
    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    /**
     * @param Money $originalPrice
     */
    public function setOriginalPrice(Money $originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return bool
     */
    public function isOutOfStock()
    {
        return $this->stock == 0;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getConfigSku()
    {
        return strpos($this->sku, '-') !== false ? substr($this->sku, 0, strpos($this->sku, '-')) : null;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes->toArray();
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function addAttribute($key, $value)
    {
        $this->attributes->set($key, $value);
    }

    /**
     * @param string $key
     */
    public function removeAttribute($key)
    {
        $this->attributes->remove($key);
    }

    /**
     * @return int
     */
    public function getLinioPlusLevel()
    {
        return $this->linioPlusLevel;
    }

    /**
     * @param int $linioPlusLevel
     */
    public function setLinioPlusLevel($linioPlusLevel)
    {
        $this->linioPlusLevel = $linioPlusLevel;
    }

    /**
     * @return bool
     */
    public function hasFreeShipping()
    {
        return $this->hasFreeShipping;
    }

    /**
     * @param bool $hasFreeShipping
     */
    public function setHasFreeShipping($hasFreeShipping)
    {
        $this->hasFreeShipping = $hasFreeShipping;
    }

    /**
     * @param string $attribute
     *
     * @return string
     */
    public function getAttribute($attribute)
    {
        return $this->attributes->get($attribute, '');
    }

    /**
     * @return int
     */
    public function getMinimumDeliveryDays()
    {
        return $this->minimumDeliveryDays;
    }

    /**
     * @param int $minimumDeliveryDays
     */
    public function setMinimumDeliveryDays($minimumDeliveryDays)
    {
        $this->minimumDeliveryDays = $minimumDeliveryDays;
    }

    /**
     * @return int
     */
    public function getMaximumDeliveryDays()
    {
        return $this->maximumDeliveryDays;
    }

    /**
     * @param int $maximumDeliveryDays
     */
    public function setMaximumDeliveryDays($maximumDeliveryDays)
    {
        $this->maximumDeliveryDays = $maximumDeliveryDays;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return bool
     */
    public function hasSpecialPrice()
    {
        return $this->originalPrice->isPositive() && $this->originalPrice > $this->price;
    }

    /**
     * @return float
     */
    public function getPercentageOff()
    {
        if ($this->percentageOff !== null) {
            return $this->percentageOff;
        }

        if ($this->hasSpecialPrice()) {
            $this->percentageOff = 100 - (($this->price->getMoneyAmount() * 100) / $this->originalPrice->getMoneyAmount());
        } else {
            $this->percentageOff = 0.0;
        }

        return $this->percentageOff;
    }

    /**
     * @return Money
     */
    public function getSavedAmount()
    {
        if ($this->savedAmount !== null) {
            return $this->savedAmount;
        }

        if ($this->hasSpecialPrice()) {
            $this->savedAmount = new Money($this->originalPrice->getMoneyAmount() - $this->price->getMoneyAmount());
        } else {
            $this->savedAmount = new Money(0);
        }

        return $this->savedAmount;
    }

    /**
     * @return bool
     */
    public function isExpressInvoiceAvailable()
    {
        return $this->expressInvoiceAvailable;
    }

    /**
     * @param bool $expressInvoiceAvailable
     */
    public function setExpressInvoiceAvailable($expressInvoiceAvailable)
    {
        $this->expressInvoiceAvailable = $expressInvoiceAvailable;
    }

    /**
     * @return string
     */
    public function getConditionTypeNote()
    {
        return $this->conditionTypeNote;
    }

    /**
     * @param string $conditionTypeNote
     */
    public function setConditionTypeNote($conditionTypeNote)
    {
        $this->conditionTypeNote = $conditionTypeNote;
    }

    /**
     * @return string
     */
    public function getPackageContent()
    {
        return $this->packageContent;
    }

    /**
     * @param string $packageContent
     */
    public function setPackageContent($packageContent)
    {
        $this->packageContent = $packageContent;
    }

    /**
     * @return string
     */
    public function getWarranty()
    {
        return $this->warranty;
    }

    /**
     * @param string $warranty
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;
    }
}
