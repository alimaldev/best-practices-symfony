<?php

namespace Linio\Frontend\Entity\Product\Review;

class VoteSummary
{
    /**
     * @var int
     */
    protected $stars = 0;

    /**
     * @var int
     */
    protected $votes = 0;

    /**
     * @return int
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @return int
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @param int $stars
     */
    public function setStars($stars)
    {
        $this->stars = $stars;
    }

    /**
     * @param int $votes
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;
    }
}
