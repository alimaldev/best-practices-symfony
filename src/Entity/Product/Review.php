<?php

namespace Linio\Frontend\Entity\Product;

use Linio\Frontend\Entity\Product\Review\Vote;

class Review
{
    /**
     * @var string
     */
    protected $sku;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var string
     */
    protected $customerName;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var float
     */
    protected $experienceAverage;

    /**
     * @var int
     */
    protected $ratingScore;

    /**
     * @var Vote[]
     */
    protected $votes = [];

    /**
     * @var bool
     */
    protected $confirmedPurchase = false;

    /**
     * @var bool
     */
    protected $customerVerified = false;

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return float
     */
    public function getExperienceAverage()
    {
        return $this->experienceAverage;
    }

    /**
     * @return Vote[]
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * @return bool
     */
    public function getConfirmedPurchase(): bool
    {
        return $this->confirmedPurchase;
    }

    /**
     * @return bool
     */
    public function getCustomerVerified(): bool
    {
        return $this->customerVerified;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param string $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param float $experienceAverage
     */
    public function setExperienceAverage($experienceAverage)
    {
        $this->experienceAverage = $experienceAverage;
    }

    /**
     * @return int
     */
    public function getRatingScore()
    {
        return $this->ratingScore;
    }

    /**
     * @param int $ratingScore
     */
    public function setRatingScore($ratingScore)
    {
        $this->ratingScore = $ratingScore;
    }

    /**
     * @param Vote[] $votes
     */
    public function setVotes($votes)
    {
        $this->votes = $votes;
    }

    /**
     * @param Vote $vote
     */
    public function addVote(Vote $vote)
    {
        $this->votes[] = $vote;
    }

    /**
     * @param bool $confirmedPurchase
     */
    public function setConfirmedPurchase(bool $confirmedPurchase)
    {
        $this->confirmedPurchase = $confirmedPurchase;
    }

    /**
     * @param bool $customerVerified
     */
    public function setCustomerVerified(bool $customerVerified)
    {
        $this->customerVerified = $customerVerified;
    }
}
