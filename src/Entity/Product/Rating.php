<?php

namespace Linio\Frontend\Entity\Product;

use Linio\Frontend\Entity\Product\Review\VoteSummary;

class Rating
{
    /**
     * @var int
     */
    protected $averagePoint = 0;

    /**
     * @var int
     */
    protected $reviewsQuantity = 0;

    /**
     * @var VoteSummary[]
     */
    protected $voteSummary = [];

    /**
     * @var Review[]
     */
    protected $reviews = [];

    /**
     * @return int
     */
    public function getAveragePoint()
    {
        return $this->averagePoint;
    }

    /**
     * @return int
     */
    public function getReviewsQuantity()
    {
        return $this->reviewsQuantity;
    }

    /**
     * @return VoteSummary[]
     */
    public function getVoteSummary()
    {
        return $this->voteSummary;
    }

    /**
     * @return int
     */
    public function getVoteSummaryTotals()
    {
        return count($this->reviews);
    }

    /**
     * @return Review[]
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param int $averagePoint
     */
    public function setAveragePoint($averagePoint)
    {
        $this->averagePoint = $averagePoint;
    }

    /**
     * @param int $reviewsQuantity
     */
    public function setReviewsQuantity($reviewsQuantity)
    {
        $this->reviewsQuantity = $reviewsQuantity;
    }

    /**
     * @param VoteSummary[] $voteSummary
     */
    public function setVoteSummary($voteSummary)
    {
        $this->voteSummary = $voteSummary;
    }

    /**
     * @param Review[] $reviews
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }

    /**
     * @param Review $review
     */
    public function addReview(Review $review)
    {
        $this->reviews[] = $review;
    }
}
