<?php

namespace Linio\Frontend\Entity\Product\Media;

class Image
{
    /**
     * @var int
     */
    protected $position;

    /**
     * @var bool
     */
    protected $main;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $sprite;

    public function __construct()
    {
        $this->main = false;
        $this->position = 0;
    }

    /**
     * @return bool
     */
    public function isMain()
    {
        return $this->main;
    }

    /**
     * @param bool $main
     */
    public function setMain($main)
    {
        $this->main = $main;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getSprite()
    {
        return $this->sprite;
    }

    /**
     * @param string $sprite
     */
    public function setSprite($sprite)
    {
        $this->sprite = $sprite;
    }
}
