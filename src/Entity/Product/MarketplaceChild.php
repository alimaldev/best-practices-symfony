<?php

namespace Linio\Frontend\Entity\Product;

use Linio\Frontend\Entity\Catalog\CampaignVoucher;
use Linio\Frontend\Entity\Seller;

class MarketplaceChild extends Simple
{
    /**
     * @var int
     */
    protected $deliveryTime;

    /**
     * @var Seller
     */
    protected $seller;

    /**
     * @var CampaignVoucher
     */
    protected $campaignVoucher;

    /**
     * @var bool
     */
    protected $imported;

    /**
     * @var string
     */
    protected $fulfillmentType;

    /**
     * @return int
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * @param int $deliveryTime
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;
    }

    /**
     * @return Seller
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param Seller $seller
     */
    public function setSeller(Seller $seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return CampaignVoucher
     */
    public function getCampaignVoucher()
    {
        return $this->campaignVoucher;
    }

    /**
     * @param CampaignVoucher $campaignVoucher
     */
    public function setCampaignVoucher(CampaignVoucher $campaignVoucher)
    {
        $this->campaignVoucher = $campaignVoucher;
    }

    /**
     * @return bool
     */
    public function isImported()
    {
        return $this->imported;
    }

    /**
     * @param bool $imported
     */
    public function setIsImported($imported)
    {
        $this->imported = $imported;
    }

    /**
     * @return string
     */
    public function getFulfillmentType()
    {
        return $this->fulfillmentType;
    }

    /**
     * @param string $fulfillmentType
     */
    public function setFulfillmentType(string $fulfillmentType = null)
    {
        $this->fulfillmentType = $fulfillmentType;
    }
}
