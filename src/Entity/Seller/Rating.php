<?php

namespace Linio\Frontend\Entity\Seller;

class Rating
{
    /**
     * @var int
     */
    protected $stars;

    /**
     * @var int
     */
    protected $votes;

    /**
     * @param int $stars
     * @param int $votes
     */
    public function __construct($stars, $votes)
    {
        $this->stars = $stars;
        $this->votes = $votes;
    }

    /**
     * @return int
     */
    public function getStars()
    {
        return $this->stars;
    }

    /**
     * @return int
     */
    public function getVotes()
    {
        return $this->votes;
    }
}
