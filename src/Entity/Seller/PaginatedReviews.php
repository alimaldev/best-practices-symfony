<?php

namespace Linio\Frontend\Entity\Seller;

class PaginatedReviews
{
    /**
     * @var int
     */
    protected $currentPage;

    /**
     * @var int
     */
    protected $totalPages;

    /**
     * @var ReviewCollection
     */
    protected $reviews;

    /**
     * @param int $currentPage
     * @param int $totalPages
     * @param ReviewCollection $reviews
     */
    public function __construct($currentPage, $totalPages, ReviewCollection $reviews)
    {
        $this->currentPage = $currentPage;
        $this->totalPages = $totalPages;
        $this->reviews = $reviews;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getTotalPages()
    {
        return $this->totalPages;
    }

    /**
     * @return ReviewCollection
     */
    public function getReviews()
    {
        return $this->reviews;
    }
}
