<?php

namespace Linio\Frontend\Entity\Seller;

use DateTime;

class Review
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var int
     */
    protected $experienceAverage;

    /**
     * @var string
     */
    protected $customerName;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @param string $title
     * @param string $comment
     * @param int $experienceAverage
     * @param string $customerName
     * @param DateTime $createdAt
     */
    public function __construct($title, $comment, $experienceAverage, $customerName, DateTime $createdAt)
    {
        $this->title = $title;
        $this->comment = $comment;
        $this->experienceAverage = $experienceAverage;
        $this->customerName = $customerName;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return int
     */
    public function getExperienceAverage()
    {
        return $this->experienceAverage;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
