<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity\Seller;

class Seller
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $operationType;

    /**
     * @var RatingSummary
     */
    protected $ratingSummary;

    /**
     * @param int $id
     * @param string $name
     * @param string $slug
     * @param string $type
     * @param RatingSummary $ratingSummary
     * @param string $operationType
     */
    public function __construct(
        int $id,
        string $name,
        string $slug,
        string $type,
        RatingSummary $ratingSummary,
        string $operationType = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
        $this->type = $type;
        $this->operationType = $operationType;
        $this->ratingSummary = $ratingSummary;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getOperationType()
    {
        return $this->operationType;
    }

    /**
     * @return RatingSummary
     */
    public function getRatingSummary(): RatingSummary
    {
        return $this->ratingSummary;
    }
}
