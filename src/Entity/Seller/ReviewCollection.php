<?php

namespace Linio\Frontend\Entity\Seller;

use Linio\Collection\TypedCollection;

class ReviewCollection extends TypedCollection
{
    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof Review;
    }
}
