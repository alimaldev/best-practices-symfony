<?php

namespace Linio\Frontend\Entity\Seller;

class RatingSummary
{
    /**
     * @var float
     */
    protected $averagePoint;

    /**
     * @var int
     */
    protected $reviewsQuantity;

    /**
     * @var RatingCollection
     */
    protected $ratings;

    /**
     * @param float $averagePoint
     * @param int $reviewsQuantity
     * @param RatingCollection $ratings
     */
    public function __construct($averagePoint, $reviewsQuantity, RatingCollection $ratings)
    {
        $this->averagePoint = $averagePoint;
        $this->reviewsQuantity = $reviewsQuantity;
        $this->ratings = $ratings;
    }

    /**
     * @return float
     */
    public function getAveragePoint()
    {
        return $this->averagePoint;
    }

    /**
     * @return int
     */
    public function getReviewsQuantity()
    {
        return $this->reviewsQuantity;
    }

    /**
     * @return RatingCollection
     */
    public function getRatings()
    {
        return $this->ratings;
    }
}
