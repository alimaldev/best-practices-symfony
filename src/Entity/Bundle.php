<?php

namespace Linio\Frontend\Entity;

use DateTime;
use Linio\Frontend\Entity\Bundle\Product as BundleProduct;
use Linio\Type\Money;

class Bundle
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var BundleProduct[]
     */
    protected $products = [];

    /**
     * @var Money
     */
    protected $savingPrice;

    /**
     * @var float
     */
    protected $discount;

    /**
     * @var DateTime
     */
    protected $startDate;

    /**
     * @var DateTime
     */
    protected $endDate;

    public function __construct()
    {
        $this->savingPrice = new Money();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return BundleProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param BundleProduct[] $products
     */
    public function setProducts(array $products)
    {
        $this->products = $products;
    }

    /**
     * @param BundleProduct $product
     */
    public function addProduct(BundleProduct $product)
    {
        $this->products[] = $product;
    }

    /**
     * @return Money
     */
    public function getRegularPrice()
    {
        $total = new Money();

        foreach ($this->products as $product) {
            $total = $total->add($product->getPrice());
        }

        return $total;
    }

    /**
     * @return Money
     */
    public function getSavingPrice()
    {
        return $this->savingPrice;
    }

    /**
     * @param Money $savingPrice
     */
    public function setSavingPrice(Money $savingPrice)
    {
        $this->savingPrice = $savingPrice;
    }

    /**
     * @return Money
     */
    public function getSavingTotal()
    {
        return $this->getRegularPrice()->subtract($this->getSavingPrice());
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $endDate
     */
    public function setEndDate(DateTime $endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $now = new DateTime();

        if ($this->startDate && $now < $this->startDate) {
            return false;
        }

        if ($this->endDate && $now > $this->endDate) {
            return false;
        }

        return true;
    }
}
