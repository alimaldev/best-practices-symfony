<?php

namespace Linio\Frontend\Entity\Address;

use Linio\Collection\TypedCollection;

class RegionCollection extends TypedCollection
{
    /**
     * @param City $value
     *
     * @return bool
     */
    public function isValidType($value)
    {
        return $value instanceof Region;
    }
}
