<?php

namespace Linio\Frontend\Entity\Address;

class Municipality
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var CityCollection
     */
    public $cities;

    public function __construct()
    {
        $this->cities = new CityCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return CityCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param City $city
     */
    public function addCity(City $city)
    {
        $this->cities->add($city);
    }
}
