<?php

namespace Linio\Frontend\Entity\Address;

class Region
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;

    /**
     * @var MunicipalityCollection
     */
    protected $municipalities;

    public function __construct()
    {
        $this->municipalities = new MunicipalityCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return MunicipalityCollection
     */
    public function getMunicipalities()
    {
        return $this->municipalities;
    }

    /**
     * @param Municipality $municipality
     */
    public function addMunicipality(Municipality $municipality)
    {
        $this->municipalities->add($municipality);
    }
}
