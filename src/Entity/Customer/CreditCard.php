<?php

namespace Linio\Frontend\Entity\Customer;

use JsonSerializable;

class CreditCard implements JsonSerializable
{
    const BRAND_VISA = 'visa';
    const BRAND_MASTERCARD = 'mastercard';
    const BRAND_DISCOVER = 'discover';
    const BRAND_AMEX = 'amex';
    const BRAND_DINERS_CLUB = 'diners_club';
    const BRAND_JCB = 'jcb';
    const BRAND_SWITCH = 'switch';
    const BRAND_SOLO = 'solo';
    const BRAND_DANKORT = 'dankort';
    const BRAND_MAESTRO = 'maestro';
    const BRAND_FORBRUGSFORENINGEN = 'forbrugsforeningen';
    const BRAND_LASER = 'laser';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $brand;

    /**
     * @var string
     */
    protected $cardholderName;

    /**
     * @var string
     */
    protected $cardholderFirstName;

    /**
     * @var string
     */
    protected $cardholderLastName;

    /**
     * @var string
     */
    protected $cardNumber;

    /**
     * @var \DateTime
     */
    protected $expirationDate;

    /**
     * @var string
     */
    protected $paymentMethod;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getCardholderName()
    {
        return $this->cardholderName;
    }

    /**
     * @param string $cardholderName
     */
    public function setCardholderName($cardholderName)
    {
        $this->cardholderName = $cardholderName;
    }

    /**
     * @return string
     */
    public function getCardholderFirstName()
    {
        return $this->cardholderFirstName;
    }

    /**
     * @param string $cardholderFirstName
     */
    public function setCardholderFirstName($cardholderFirstName)
    {
        $this->cardholderFirstName = $cardholderFirstName;
    }

    /**
     * @return string
     */
    public function getCardholderLastName()
    {
        return $this->cardholderLastName;
    }

    /**
     * @param string $cardholderLastName
     */
    public function setCardholderLastName($cardholderLastName)
    {
        $this->cardholderLastName = $cardholderLastName;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getLastDigits()
    {
        return substr($this->cardNumber, -4, 4);
    }

    /**
     * @return string
     */
    public function getFirstDigits()
    {
        return substr($this->cardNumber, 0, 6);
    }

    /**
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param \DateTime $expirationDate
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'brand' => $this->getBrand(),
            'cardholderName' => $this->getCardholderName(),
            'lastDigits' => $this->getLastDigits(),
            'firstDigits' => $this->getFirstDigits(),
            'paymentMethod' => $this->getPaymentMethod(),
        ];
    }
}
