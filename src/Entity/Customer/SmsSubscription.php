<?php

namespace Linio\Frontend\Entity\Customer;

class SmsSubscription
{
    const SMS_SUBSCRIBED = 'subscribed';
    const SMS_UNSUBSCRIBED = 'unsubscribed';
    const SMS_SUBSCRIBED_AGAIN = 'subscribed_again';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $customerId;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var \DateTime
     */
    protected $createDate;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var \DateTime
     */
    protected $updateDate;

    /**
     * @var int
     */
    protected $storeId;

    /**
     * SmsSubscription constructor.
     *
     * @param int $id
     * @param int $customerId
     * @param string $status
     * @param string $phone
     */
    public function __construct($id, $customerId, $status, $phone)
    {
        $this->id = $id;
        $this->customerId = $customerId;
        $this->status = $status;
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param \DateTime $createDate
     */
    public function setCreateDate(\DateTime $createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param \DateTime $updateDate
     */
    public function setUpdateDate(\DateTime $updateDate)
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * @param int $storeId
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;
    }

    /**
     * @return bool
     */
    public function isSubscribed()
    {
        return $this->status == self::SMS_SUBSCRIBED || $this->status == self::SMS_SUBSCRIBED_AGAIN;
    }
}
