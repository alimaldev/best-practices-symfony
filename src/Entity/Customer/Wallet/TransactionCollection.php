<?php

namespace Linio\Frontend\Entity\Customer\Wallet;

use Linio\Collection\TypedCollection;

class TransactionCollection extends TypedCollection
{
    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof Transaction;
    }
}
