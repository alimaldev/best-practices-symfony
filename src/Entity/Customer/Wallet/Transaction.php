<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity\Customer\Wallet;

use DateTime;
use Linio\Type\Money;

class Transaction
{
    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @var Money
     */
    protected $charge;

    /**
     * @var Money
     */
    protected $payment;

    /**
     * @var DateTime|null
     */
    protected $validUntil;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var Money
     */
    protected $balance;

    public function __construct(
        DateTime $createdAt,
        Money $charge,
        Money $payment,
        string $status,
        Money $balance,
        DateTime $validUntil = null
    ) {
        $this->createdAt = $createdAt;
        $this->charge = $charge;
        $this->payment = $payment;
        $this->status = $status;
        $this->balance = $balance;
        $this->validUntil = $validUntil;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Money
     */
    public function getCharge(): Money
    {
        return $this->charge;
    }

    /**
     * @param Money $charge
     */
    public function setCharge(Money $charge)
    {
        $this->charge = $charge;
    }

    /**
     * @return Money
     */
    public function getPayment(): Money
    {
        return $this->payment;
    }

    /**
     * @param Money $payment
     */
    public function setPayment(Money $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return DateTime|null
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param DateTime $validUntil
     */
    public function setValidUntil(DateTime $validUntil)
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return Money
     */
    public function getBalance(): Money
    {
        return $this->balance;
    }

    /**
     * @param Money $balance
     */
    public function setBalance(Money $balance)
    {
        $this->balance = $balance;
    }
}
