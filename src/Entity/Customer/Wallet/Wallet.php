<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity\Customer\Wallet;

class Wallet
{
    /**
     * @var string
     */
    protected $account;
    /**
     * @var Points
     */
    protected $accruedPoints;

    /**
     * @var Points
     */
    protected $usedPoints;

    /**
     * @var Points
     */
    protected $expiredPoints;

    /**
     * @var TransactionCollection
     */
    protected $transactions;

    /**
     * @var Points
     */
    protected $balance;

    public function __construct()
    {
        $this->transactions = new TransactionCollection();
        $this->accruedPoints = new Points();
        $this->usedPoints = new Points();
        $this->expiredPoints = new Points();
        $this->balance = new Points();
    }

    /**
     * @return string
     */
    public function getAccount(): string
    {
        return $this->account;
    }

    /**
     * @param string $account
     */
    public function setAccount(string $account)
    {
        $this->account = $account;
    }

    /**
     * @return Points
     */
    public function getAccruedPoints(): Points
    {
        return $this->accruedPoints;
    }

    /**
     * @param Points $accruedPoints
     */
    public function setAccruedPoints(Points $accruedPoints)
    {
        $this->accruedPoints = $accruedPoints;
    }

    /**
     * @return Points
     */
    public function getUsedPoints(): Points
    {
        return $this->usedPoints;
    }

    /**
     * @param Points $usedPoints
     */
    public function setUsedPoints(Points $usedPoints)
    {
        $this->usedPoints = $usedPoints;
    }

    /**
     * @return Points
     */
    public function getExpiredPoints(): Points
    {
        return $this->expiredPoints;
    }

    /**
     * @param Points $expiredPoints
     */
    public function setExpiredPoints(Points $expiredPoints)
    {
        $this->expiredPoints = $expiredPoints;
    }

    /**
     * @return TransactionCollection
     */
    public function getTransactions(): TransactionCollection
    {
        return $this->transactions;
    }

    /**
     * @param TransactionCollection $transactions
     */
    public function setTransactions(TransactionCollection $transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return Points
     */
    public function getBalance(): Points
    {
        return $this->balance;
    }

    /**
     * @param Points $balance
     */
    public function setBalance(Points $balance)
    {
        $this->balance = $balance;
    }

    /**
     * @param Transaction $transaction
     */
    public function addTransaction(Transaction $transaction)
    {
        $this->transactions->add($transaction);
    }
}
