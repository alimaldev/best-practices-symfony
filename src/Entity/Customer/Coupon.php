<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity\Customer;

use DateTime;
use Linio\Type\Money;

class Coupon
{
    const DISCOUNT_TYPE_PERCENT = 'percent';
    const DISCOUNT_TYPE_MONEY = 'money';

    /**
     * @var string
     */
    protected $code;

    /**
     * @var bool
     */
    protected $active;

    /**
     * @var DateTime
     */
    protected $validFrom;

    /**
     * @var DateTime
     */
    protected $validUntil;

    /**
     * @var string
     */
    protected $discountType;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var float
     */
    protected $balance;

    /**
     * @var float
     */
    protected $discountPercentage;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }

    /**
     * @return DateTime
     */
    public function getValidFrom(): DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param DateTime $validSince
     */
    public function setValidFrom(DateTime $validSince)
    {
        $this->validFrom = $validSince;
    }

    /**
     * @return DateTime
     */
    public function getValidUntil(): DateTime
    {
        return $this->validUntil;
    }

    /**
     * @param DateTime $validUntil
     */
    public function setValidUntil(DateTime $validUntil)
    {
        $this->validUntil = $validUntil;
    }

    /**
     * @return string
     */
    public function getDiscountType(): string
    {
        return $this->discountType;
    }

    /**
     * @param string $discountType
     */
    public function setDiscountType(string $discountType)
    {
        $this->discountType = $discountType;
    }

    /**
     * @return Money|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param Money|null $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float|Money|null
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float|Money|null $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return float|null
     */
    public function getDiscountPercentage()
    {
        return $this->discountPercentage;
    }

    /**
     * @param float $discountPercentage
     */
    public function setDiscountPercentage(float $discountPercentage)
    {
        $this->discountPercentage = $discountPercentage;
    }
}
