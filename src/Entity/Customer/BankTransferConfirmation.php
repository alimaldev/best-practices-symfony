<?php

namespace Linio\Frontend\Entity\Customer;

use DateTime;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Entity\Customer;

class BankTransferConfirmation
{
    /**
     * @var string
     */
    protected $bankId;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var string
     */
    protected $number;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var Order[]
     */
    protected $orders = [];

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @return string
     */
    public function getBankId()
    {
        return $this->bankId;
    }

    /**
     * @param string $bankId
     */
    public function setBankId($bankId)
    {
        $this->bankId = $bankId;
    }

    /**
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order $order
     */
    public function addOrder(Order $order)
    {
        $this->orders[] = $order;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }
}
