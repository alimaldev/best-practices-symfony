<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity\Customer;

use DateTime;
use Linio\Type\Money;

class LinioPlusMembership
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $customerId;

    /**
     * @var DateTime
     */
    protected $startDate;

    /**
     * @var DateTime
     */
    protected $expirationDate;

    /**
     * @var DateTime;
     */
    protected $lastUpdated;

    /**
     * @var string
     */
    protected $reference;

    /**
     * @var bool
     */
    protected $active = false;

    /**
     * @var DateTime
     */
    protected $cancellationDate;

    /**
     * @var Money
     */
    protected $balance;

    /**
     * @var DateTime
     */
    protected $createDate;

    /**
     * @var int
     */
    protected $walletSubscription;

    public function __construct($id, $customerId, DateTime $startDate, DateTime $expirationDate, bool $active)
    {
        $this->id = intval($id);
        $this->customerId = intval($customerId);
        $this->startDate = $startDate;
        $this->expirationDate = $expirationDate;
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = intval($id);
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = intval($customerId);
    }

    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param DateTime $expirationDate
     */
    public function setExpirationDate(DateTime $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param DateTime $lastUpdated
     */
    public function setLastUpdated(DateTime $lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = intval($active);
    }

    /**
     * @return DateTime
     */
    public function getCancellationDate()
    {
        return $this->cancellationDate;
    }

    /**
     * @param DateTime $cancellationDate
     */
    public function setCancellationDate(DateTime $cancellationDate)
    {
        $this->cancellationDate = $cancellationDate;
    }

    /**
     * @return Money
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param Money $balance
     */
    public function setBalance(Money $balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param DateTime $createDate
     */
    public function setCreateDate(DateTime $createDate)
    {
        $this->createDate = $createDate;
    }

    /**
     * @return int
     */
    public function getWalletSubscription()
    {
        return $this->walletSubscription;
    }

    /**
     * @param int $walletSubscription
     */
    public function setWalletSubscription($walletSubscription)
    {
        $this->walletSubscription = intval($walletSubscription);
    }
}
