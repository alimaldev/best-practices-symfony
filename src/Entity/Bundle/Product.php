<?php

namespace Linio\Frontend\Entity\Bundle;

use Linio\Type\Money;

class Product
{
    /**
     * @var string
     */
    protected $sku;

    /**
     * @var bool
     */
    protected $selected;

    /**
     * @var string
     */
    protected $sprite;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Money
     */
    protected $price;

    /**
     * @var Money
     */
    protected $originalPrice;

    public function __construct()
    {
        $this->price = new Money();
        $this->originalPrice = new Money();
        $this->selected = true;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isSelected()
    {
        return $this->selected;
    }

    /**
     * @param bool $selected
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;
    }

    /**
     * @return string
     */
    public function getSprite()
    {
        return $this->sprite;
    }

    /**
     * @param string $sprite
     */
    public function setSprite($sprite)
    {
        $this->sprite = $sprite;
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Money $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Money
     */
    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    /**
     * @param Money $originalPrice
     */
    public function setOriginalPrice(Money $originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }
}
