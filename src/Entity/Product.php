<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity;

use Carbon\Carbon;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Seo\SeoAware;
use Linio\Type\Dictionary;
use Linio\Type\Money;

class Product
{
    use SeoAware;

    const FULFILLMENT_TYPE_SOLD = 'sold';

    /**
     * @var string
     */
    protected $sku;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Brand
     */
    protected $brand;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var int
     */
    protected $linioPlusLevel;

    /**
     * @var Money
     */
    protected $unitPrice;

    /**
     * @var int
     */
    protected $installments;

    /**
     * @var Money
     */
    protected $installmentPrice;

    /**
     * @var Simple[]
     */
    protected $simples = [];

    /**
     * @var Simple
     */
    protected $firstSimple;

    /**
     * @var MarketplaceChild[]
     */
    protected $marketplaceChildren = [];

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var int
     */
    protected $stock;

    /**
     * @var int
     */
    protected $deliveryTime;

    /**
     * @var bool
     */
    protected $hasFreeShipping = false;

    /**
     * @var bool
     */
    protected $hasFreeStorePickup = false;

    /**
     * @var int
     */
    protected $shippingTimeFrom;

    /**
     * @var int
     */
    protected $shippingTimeTo;

    /**
     * @var Dictionary
     */
    protected $attributes;

    /**
     * @var Rating
     */
    protected $rating;

    /**
     * @var Image[]
     */
    protected $images = [];

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var Seller
     */
    protected $seller;

    /**
     * @var bool
     */
    protected $returnAllowed;

    /**
     * @var bool
     */
    protected $postPaymentAllowed;

    /**
     * @var Bundle[]
     */
    protected $bundles;

    /**
     * @var Product[];
     */
    protected $groupedProducts;

    /**
     * @var string
     */
    protected $variationType;

    /**
     * @var string
     */
    protected $fulfillmentType;

    /**
     * @var string|null
     */
    protected $marketplaceParentSku;

    /**
     * @var Money
     */
    protected $price;

    /**
     * @var Money
     */
    protected $originalPrice;

    /**
     * @var float
     */
    protected $percentageOff;

    /**
     * @var Money
     */
    protected $savedAmount;

    /**
     * @var string
     */
    protected $configId;

    /**
     * @var string
     */
    protected $attributeSet;

    /**
     * Used for filtering configs when coming from a campaign.
     *
     * @var string
     */
    protected $campaignSlug;

    /**
     * @var string
     */
    protected $eanCode;

    /**
     * @var bool
     */
    protected $imported;

    /**
     * @var bool
     */
    protected $isOversized;

    /**
     * @var bool
     */
    protected $masterAndChildrenImported = false;

    /**
     * @var bool
     */
    protected $inWishLists = false;

    /**
     * @var int
     */
    protected $supplierWarrantyMonths = 0;

    /**
     * @var bool
     */
    protected $hasVariation = false;

    /**
     * @var bool
     */
    protected $promiseDelivery = false;

    public function __construct(string $sku = null)
    {
        $this->sku = $sku;
        $this->unitPrice = new Money();
        $this->installmentPrice = new Money();
        $this->attributes = new Dictionary();
        $this->rating = new Rating();
        $this->brand = new Brand();
        $this->price = new Money();
        $this->originalPrice = new Money();
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getLinioPlusLevel()
    {
        return $this->linioPlusLevel;
    }

    /**
     * @param int $linioPlusLevel
     */
    public function setLinioPlusLevel($linioPlusLevel)
    {
        $this->linioPlusLevel = $linioPlusLevel;
    }

    /**
     * @return Money
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param Money $unitPrice
     */
    public function setUnitPrice(Money $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return int
     */
    public function getInstallments()
    {
        return $this->installments;
    }

    /**
     * @param int $installments
     */
    public function setInstallments($installments)
    {
        $this->installments = $installments;
    }

    /**
     * @return Money
     */
    public function getInstallmentPrice()
    {
        return $this->installmentPrice;
    }

    /**
     * @param Money $installmentPrice
     */
    public function setInstallmentPrice(Money $installmentPrice)
    {
        $this->installmentPrice = $installmentPrice;
    }

    /**
     * @return Simple[]
     */
    public function getSimples()
    {
        return $this->simples;
    }

    /**
     * @param Simple[] $simples
     */
    public function setSimples($simples)
    {
        $this->simples = $simples;
    }

    /**
     * @return MarketplaceChild[]
     */
    public function getMarketplaceChildren()
    {
        return $this->marketplaceChildren;
    }

    /**
     * @param MarketplaceChild[] $marketplaceChildren
     */
    public function setMarketplaceChildren($marketplaceChildren)
    {
        $this->marketplaceChildren = $marketplaceChildren;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return int
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * @return int
     */
    public function getDeliveryTimeFromFirstSimple()
    {
        if ($this->getFirstSimple() instanceof MarketplaceChild) {
            return $this->getFirstSimple()->getDeliveryTime();
        }

        return $this->deliveryTime;
    }

    /**
     * @param int $deliveryTime
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;
    }

    /**
     * @return bool
     */
    public function hasFreeShipping()
    {
        return $this->hasFreeShipping;
    }

    /**
     * @param bool $hasFreeShipping
     */
    public function setHasFreeShipping($hasFreeShipping)
    {
        $this->hasFreeShipping = $hasFreeShipping;
    }

    /**
     * @return bool
     */
    public function hasFreeStorePickup(): bool
    {
        return $this->hasFreeStorePickup;
    }

    /**
     * @param bool $hasFreeStorePickup
     */
    public function setHasFreeStorePickup(bool $hasFreeStorePickup)
    {
        $this->hasFreeStorePickup = $hasFreeStorePickup;
    }

    /**
     * @return int
     */
    public function getShippingTimeFrom()
    {
        return $this->shippingTimeFrom;
    }

    /**
     * @param int $shippingTimeFrom
     */
    public function setShippingTimeFrom($shippingTimeFrom)
    {
        $this->shippingTimeFrom = $shippingTimeFrom;
    }

    /**
     * @return int
     */
    public function getShippingTimeTo()
    {
        return $this->shippingTimeTo;
    }

    /**
     * @param int $shippingTimeTo
     */
    public function setShippingTimeTo($shippingTimeTo)
    {
        $this->shippingTimeTo = $shippingTimeTo;
    }

    /**
     * @return Dictionary
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param Dictionary $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        return $this->attributes->get($key);
    }

    /**
     * @return Rating
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param Rating $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return bool
     */
    public function isOutOfStock()
    {
        return $this->stock <= 0;
    }

    /**
     * @return Image[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param Image[] $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Seller
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param Seller $seller
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return bool
     */
    public function isImported()
    {
        return $this->imported;
    }

    /**
     * @param bool $imported
     */
    public function setImported($imported)
    {
        $this->imported = $imported;
    }

    /**
     * @return bool
     */
    public function isReturnAllowed()
    {
        return $this->returnAllowed;
    }

    /**
     * @param bool $returnAllowed
     */
    public function setReturnAllowed($returnAllowed)
    {
        $this->returnAllowed = (bool) $returnAllowed;
    }

    /**
     * @return bool
     */
    public function isPostPaymentAllowed()
    {
        return $this->postPaymentAllowed;
    }

    /**
     * @param bool $postPaymentAllowed
     */
    public function setPostPaymentAllowed($postPaymentAllowed)
    {
        $this->postPaymentAllowed = (bool) $postPaymentAllowed;
    }

    /**
     * @return Bundle[]
     */
    public function getBundles()
    {
        return $this->bundles;
    }

    /**
     * @param Bundle[] $bundles
     */
    public function setBundles($bundles)
    {
        $this->bundles = $bundles;
    }

    /**
     * @param Bundle $bundle
     */
    public function addBundle(Bundle $bundle)
    {
        $this->bundles[] = $bundle;
    }

    /**
     * @return bool
     */
    public function hasBundles()
    {
        return (bool) $this->bundles;
    }

    /**
     * @return Product[]
     */
    public function getGroupedProducts()
    {
        return $this->groupedProducts;
    }

    /**
     * @param Product[] $groupedProducts
     */
    public function setGroupedProducts($groupedProducts)
    {
        $this->groupedProducts = $groupedProducts;
    }

    /**
     * @return Simple|MarketplaceChild|null
     */
    public function getFirstSimple()
    {
        if ($this->firstSimple) {
            return $this->firstSimple;
        }

        if ($this->isMarketPlace()) {
            $this->firstSimple = reset($this->marketplaceChildren) ?: null;

            return $this->firstSimple;
        }

        $this->firstSimple = reset($this->simples) ?: null;

        foreach ($this->simples as $simple) {
            if (!$simple->isOutOfStock() && $simple->isActive()) {
                $this->firstSimple = $simple;
            }
        }

        return $this->firstSimple;
    }

    /**
     * @param string $skuSimple
     *
     * @return bool
     */
    public function hasSimple($skuSimple)
    {
        return isset($this->simples[$skuSimple]);
    }

    /**
     * @param Simple $simple
     */
    public function addSimple(Simple $simple)
    {
        $this->simples[$simple->getSku()] = $simple;

        $simple->setProduct($this);
    }

    /**
     * @param $skuSimple
     *
     * @return Simple|null
     */
    public function getSimple($skuSimple)
    {
        if ($this->hasMarketplaceChild($skuSimple)) {
            return $this->marketplaceChildren[$skuSimple];
        } elseif ($this->hasSimple($skuSimple)) {
            return $this->simples[$skuSimple];
        }

        return;
    }

    /**
     * @param Image $image
     */
    public function addImage(Image $image)
    {
        $this->images[$image->getPosition()] = $image;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addAttribute($key, $value)
    {
        $this->attributes->set($key, $value);
    }

    /**
     * @param string $key
     */
    public function removeAttribute($key)
    {
        $this->attributes->remove($key);
    }

    /**
     * @param string $skuSimple
     *
     * @return bool
     */
    public function hasMarketplaceChild($skuSimple)
    {
        return isset($this->marketplaceChildren[$skuSimple]);
    }

    /**
     * @param MarketplaceChild $marketplaceChild
     */
    public function addMarketplaceChild(MarketplaceChild $marketplaceChild)
    {
        $this->marketplaceChildren[$marketplaceChild->getSku()] = $marketplaceChild;

        $marketplaceChild->setProduct($this);
    }

    /**
     * @param $skuSimple
     *
     * @return Simple|null
     */
    public function getMarketplaceChild($skuSimple)
    {
        if ($this->hasMarketplaceChild($skuSimple)) {
            return $this->marketplaceChildren[$skuSimple];
        }

        return;
    }

    /**
     * @return bool
     */
    public function isMarketPlace()
    {
        return !empty($this->marketplaceChildren);
    }

    /**
     * @param Product $groupedProduct
     */
    public function addGroupedProduct(Product $groupedProduct)
    {
        $this->groupedProducts[] = $groupedProduct;
    }

    /**
     * @return string
     */
    public function getVariationType()
    {
        return $this->variationType;
    }

    /**
     * @param string $variationType
     */
    public function setVariationType($variationType)
    {
        $this->variationType = $variationType;
    }

    /**
     * @return string
     */
    public function getFulfillmentType()
    {
        return $this->fulfillmentType;
    }

    /**
     * @param string $fulfillmentType
     */
    public function setFulfillmentType($fulfillmentType)
    {
        $this->fulfillmentType = $fulfillmentType;
    }

    /**
     * @return string|null
     */
    public function getMarketplaceParentSku()
    {
        return $this->marketplaceParentSku;
    }

    /**
     * @param string|null $marketplaceParentSku
     */
    public function setMarketplaceParentSku($marketplaceParentSku)
    {
        $this->marketplaceParentSku = $marketplaceParentSku;
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Money $price
     */
    public function setPrice(Money $price)
    {
        $this->price = $price;
    }

    /**
     * @return Money
     */
    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    /**
     * @param Money $originalPrice
     */
    public function setOriginalPrice(Money $originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return bool
     */
    public function hasSpecialPrice()
    {
        return $this->originalPrice->isPositive() && $this->originalPrice > $this->price;
    }

    /**
     * @return float
     */
    public function getPercentageOff()
    {
        if ($this->percentageOff !== null) {
            return $this->percentageOff;
        }

        if ($this->hasSpecialPrice()) {
            $this->percentageOff = 100 - (($this->price->getMoneyAmount() * 100) / $this->originalPrice->getMoneyAmount());
        } else {
            $this->percentageOff = 0.0;
        }

        return $this->percentageOff;
    }

    /**
     * @return Money
     */
    public function getSavedAmount()
    {
        if ($this->savedAmount !== null) {
            return $this->savedAmount;
        }

        if ($this->hasSpecialPrice()) {
            $this->savedAmount = new Money($this->originalPrice->getMoneyAmount() - $this->price->getMoneyAmount());
        } else {
            $this->savedAmount = new Money(0);
        }

        return $this->savedAmount;
    }

    /**
     * @return string
     */
    public function getConfigId()
    {
        return $this->configId;
    }

    /**
     * @param string $configId
     */
    public function setConfigId($configId)
    {
        $this->configId = $configId;
    }

    /**
     * @return string
     */
    public function getAttributeSet()
    {
        return $this->attributeSet;
    }

    /**
     * @param string $attributeSet
     */
    public function setAttributeSet($attributeSet)
    {
        $this->attributeSet = $attributeSet;
    }

    public function getCampaignSlug()
    {
        return $this->campaignSlug;
    }

    /**
     * @param string $campaignSlug
     */
    public function setCampaignSlug($campaignSlug)
    {
        $this->campaignSlug = $campaignSlug;
    }

    /**
     * @param Campaign $campaign
     */
    public function filterCampaignProducts(Campaign $campaign)
    {
        $campaignFilters = $campaign->getFilters();

        usort($this->marketplaceChildren, function ($a, $b) use ($campaign, $campaignFilters) {
            $productCampaignFilters = $campaignFilters[$this->getSku()] ?? [];

            $aInArray = in_array($a->getConfigSku(), $productCampaignFilters);
            $bInArray = in_array($b->getConfigSku(), $productCampaignFilters);

            if ($aInArray && $bInArray) {
                if ($a->getPrice() > $b->getPrice()) {
                    return 1;
                } elseif ($a->getPrice() === $b->getPrice()) {
                    return 0;
                } else {
                    return -1;
                }
            } elseif ($aInArray) {
                return -1;
            } elseif ($bInArray) {
                return 1;
            }

            if ($a->getPrice() > $b->getPrice()) {
                return 1;
            } elseif ($a->getPrice() === $b->getPrice()) {
                return 0;
            } else {
                return -1;
            }
        });

        // Reset the simple so the next firstSimple() call will use the first available.
        $this->firstSimple = null;
    }

    /**
     * @param Carbon $date
     *
     * @return bool
     */
    public function isDeliveredBy(Carbon $date)
    {
        $now = Carbon::now();
        $delivery = $now->addWeekdays($this->getDeliveryTimeFromFirstSimple());

        if ($delivery->diffInDays($date) <= 30) {
            return $delivery->lte($date);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isDeliveredByChristmas()
    {
        $christmas = Carbon::create(null, 12, 21);

        return $this->isDeliveredBy($christmas);
    }

    /**
     * @return string
     */
    public function getEanCode()
    {
        return $this->eanCode;
    }

    /**
     * @param string $eanCode
     */
    public function setEanCode($eanCode)
    {
        $this->eanCode = $eanCode;
    }

    /**
     * @return bool
     */
    public function isOversized()
    {
        return $this->isOversized;
    }

    /**
     * @param bool $isOversized
     */
    public function setIsOversized($isOversized)
    {
        $this->isOversized = $isOversized;
    }

    /**
     * @return bool
     */
    public function areMasterAndChildrenImported(): bool
    {
        return $this->masterAndChildrenImported;
    }

    /**
     * @param bool $masterAndChildrenImported
     */
    public function setMasterAndChildrenImported(bool $masterAndChildrenImported)
    {
        $this->masterAndChildrenImported = $masterAndChildrenImported;
    }

    /**
     * @return bool
     */
    public function isInWishLists(): bool
    {
        return $this->inWishLists;
    }

    public function addedToWishLists()
    {
        $this->inWishLists = true;
    }

    /**
     * @return bool
     */
    public function hasSupplierWarranty(): bool
    {
        return $this->supplierWarrantyMonths > 0;
    }

    /**
     * @param int $supplierWarrantyMonths
     */
    public function setSupplierWarrantyMonths(int $supplierWarrantyMonths)
    {
        $this->supplierWarrantyMonths = $supplierWarrantyMonths;
    }

    /**
     * @return int
     */
    public function getSupplierWarrantyMonths(): int
    {
        return $this->supplierWarrantyMonths;
    }

    /**
     * @return bool
     */
    public function hasVariation(): bool
    {
        return $this->hasVariation;
    }

    /**
     * @param bool $hasVariation
     */
    public function setHasVariation(bool $hasVariation)
    {
        $this->hasVariation = $hasVariation;
    }

    /**
     * @return bool
     */
    public function hasPromiseDelivery(): bool
    {
        return $this->promiseDelivery;
    }

    /**
     * @param bool $promiseDelivery
     */
    public function setHasPromiseDelivery(bool $promiseDelivery)
    {
        $this->promiseDelivery = $promiseDelivery;
    }

    /**
     * @return bool
     */
    public function allSimplesAreLinioPlus(): bool
    {
        foreach ($this->getSimples() as $simple) {
            if ($simple->getLinioPlusLevel() == 0) {
                return false;
            }
        }

        return true;
    }
}
