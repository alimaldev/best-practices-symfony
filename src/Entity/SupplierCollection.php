<?php

namespace Linio\Frontend\Entity;

use Linio\Collection\TypedCollection;

class SupplierCollection extends TypedCollection
{
    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isValidType($value)
    {
        return $value instanceof Supplier;
    }
}
