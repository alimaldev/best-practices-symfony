<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity;

use DateTime;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Customer\Order\Invoice\InvoiceCollection;
use Linio\Frontend\Location\Address;

class Customer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $linioId;

    /**
     * @var int
     */
    protected $oAuthId;

    /**
     * @var int
     */
    protected $fkLanguage;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $emailDoubleCheck;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $newPassword;

    /**
     * @var string
     */
    protected $passwordDoubleCheck;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $middleName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var DateTime
     */
    protected $bornDate;

    /**
     * @var string
     */
    protected $nationalRegistrationNumber;

    /**
     * @var bool
     */
    protected $subscribedToNewsletter = false;

    /**
     * @var bool
     */
    protected $subscribedToSmsNewsletter = false;

    /**
     * @var string
     */
    protected $phoneNumber;

    /**
     * @var string
     */
    protected $smsPhone;

    /**
     * @var bool
     */
    protected $acceptTerms;

    /**
     * @var string
     */
    protected $taxIdentificationNumber;

    /**
     * @var string
     */
    protected $ipAddress;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @var DateTime
     */
    protected $verifiedAt;

    /**
     * @var Address
     */
    protected $shippingAddress;

    /**
     * @var Address
     */
    protected $billingAddress;

    /**
     * @var Address[]
     */
    protected $addresses = [];

    /**
     * @var LoyaltyProgram
     */
    protected $loyaltyProgram;

    /**
     * @var bool
     */
    protected $subscribedToLinioPlus = false;

    /**
     * @var bool
     */
    protected $wasSubscribedToLinioPlus = false;

    /**
     * @var InvoiceCollection
     */
    protected $invoices;

    /**
     * @var bool
     */
    protected $hasWallet;

    /**
     * @var string
     */
    protected $impersonatedBy;

    /**
     * True if the customer has ordered before, false otherwise.
     *
     * @var bool
     */
    protected $returningCustomer = false;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLinioId()
    {
        return $this->linioId;
    }

    /**
     * @param string $linioId
     */
    public function setLinioId($linioId)
    {
        $this->linioId = $linioId;
    }

    /**
     * @return int
     */
    public function getOAuthId()
    {
        return $this->oAuthId;
    }

    /**
     * @param int $oAuthId
     */
    public function setOAuthId($oAuthId)
    {
        $this->oAuthId = $oAuthId;
    }

    /**
     * @return int
     */
    public function getFkLanguage()
    {
        return $this->fkLanguage;
    }

    /**
     * @param int $fkLanguage
     */
    public function setFkLanguage($fkLanguage)
    {
        $this->fkLanguage = $fkLanguage;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmailDoubleCheck()
    {
        return $this->emailDoubleCheck;
    }

    /**
     * @param string $emailDoubleCheck
     */
    public function setEmailDoubleCheck($emailDoubleCheck)
    {
        $this->emailDoubleCheck = $emailDoubleCheck;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPasswordDoubleCheck()
    {
        return $this->passwordDoubleCheck;
    }

    /**
     * @param string $passwordDoubleCheck
     */
    public function setPasswordDoubleCheck($passwordDoubleCheck)
    {
        $this->passwordDoubleCheck = $passwordDoubleCheck;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return DateTime
     */
    public function getBornDate()
    {
        return $this->bornDate;
    }

    /**
     * @param DateTime $bornDate
     */
    public function setBornDate(DateTime $bornDate)
    {
        $this->bornDate = $bornDate;
    }

    /**
     * @return string
     */
    public function getNationalRegistrationNumber()
    {
        return $this->nationalRegistrationNumber;
    }

    /**
     * @param string $nationalRegistrationNumber
     */
    public function setNationalRegistrationNumber($nationalRegistrationNumber)
    {
        $this->nationalRegistrationNumber = $nationalRegistrationNumber;
    }

    /**
     * @return bool
     */
    public function isSubscribedToNewsletter(): bool
    {
        return $this->subscribedToNewsletter;
    }

    /**
     * @param bool $newsletterAccept
     */
    public function setSubscribedToNewsletter(bool $newsletterAccept)
    {
        $this->subscribedToNewsletter = $newsletterAccept;
    }

    /**
     * @return bool
     */
    public function isSubscribedToSmsNewsletter(): bool
    {
        return $this->subscribedToSmsNewsletter;
    }

    /**
     * @param bool $smsNewsAccept
     */
    public function setSubscribedToSmsNewsletter(bool $smsNewsAccept)
    {
        $this->subscribedToSmsNewsletter = $smsNewsAccept;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getSmsPhone()
    {
        return $this->smsPhone;
    }

    /**
     * @param string $smsPhone
     */
    public function setSmsPhone($smsPhone)
    {
        $this->smsPhone = $smsPhone;
    }

    /**
     * @return bool
     */
    public function hasAcceptTerms()
    {
        return $this->acceptTerms;
    }

    /**
     * @return string
     */
    public function getTaxIdentificationNumber()
    {
        return $this->taxIdentificationNumber;
    }

    /**
     * @param string $taxIdentificationNumber
     */
    public function setTaxIdentificationNumber($taxIdentificationNumber)
    {
        $this->taxIdentificationNumber = $taxIdentificationNumber;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return DateTime
     */
    public function getVerifiedAt()
    {
        return $this->verifiedAt;
    }

    /**
     * @param DateTime $verifiedAt
     */
    public function setVerifiedAt(DateTime $verifiedAt)
    {
        $this->verifiedAt = $verifiedAt;
    }

    /**
     * @return Address[] Address
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param array $addresses
     */
    public function setAddresses(array $addresses)
    {
        $this->addresses = $addresses;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param string $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * @param bool $termsAccept
     */
    public function setAcceptTerms($termsAccept)
    {
        $this->acceptTerms = $termsAccept;
    }

    /**
     * @return bool
     */
    public function notHasAddress()
    {
        return empty($this->addresses);
    }

    /**
     * @param int $id
     *
     * @return Address|null
     */
    public function getAddress($id)
    {
        if (array_key_exists($id, $this->addresses)) {
            return $this->addresses[$id];
        }
    }

    /**
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        if ($address->isDefaultBilling()) {
            $this->billingAddress = null;
        }

        if ($address->isDefaultShipping()) {
            $this->shippingAddress = null;
        }

        if (array_key_exists($address->getId(), $this->addresses)) {
            unset($this->addresses[$address->getId()]);
        }
    }

    public function clearDefaultShippingAddress()
    {
        $address = $this->getShippingAddress();

        if (!$address) {
            return;
        }

        $address->setDefaultShipping(false);

        $this->setAddress($address);

        $this->shippingAddress = null;
    }

    /**
     * @return Address
     */
    public function getShippingAddress()
    {
        if (!$this->shippingAddress) {
            $isDefault = function ($address) {
                /* @var $address Address */
                return $address->isDefaultShipping();
            };

            $this->shippingAddress = current(
                array_filter($this->addresses, $isDefault)
            );
        }

        return $this->shippingAddress;
    }

    /**
     * @param Address $shippingAddress
     */
    public function setShippingAddress(Address $shippingAddress)
    {
        foreach ($this->addresses as $address) {
            $address->setDefaultShipping(false);
        }

        $shippingAddress->setDefaultShipping();
        $this->shippingAddress = $shippingAddress;

        $this->setAddress($shippingAddress);
    }

    public function clearDefaultBillingAddress()
    {
        $address = $this->getBillingAddress();

        if (!$address) {
            return;
        }

        $address->setDefaultBilling(false);

        $this->setAddress($address);

        $this->billingAddress = null;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        if (!$this->billingAddress) {
            $isDefault = function ($address) {
                /* @var $address Address */
                return $address->isDefaultBilling();
            };

            $this->billingAddress = current(
                array_filter($this->addresses, $isDefault)
            );
        }

        return $this->billingAddress;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        foreach ($this->addresses as $address) {
            $address->setDefaultBilling(false);
        }

        $billingAddress->setDefaultBilling();

        $this->billingAddress = $billingAddress;

        $this->setAddress($billingAddress);
    }

    /**
     * @param Address $customerAddress
     */
    public function setAddress(Address $customerAddress)
    {
        $this->addresses[$customerAddress->getId()] = clone $customerAddress;
    }

    /**
     * @return LoyaltyProgram
     */
    public function getLoyaltyProgram()
    {
        return $this->loyaltyProgram;
    }

    /**
     * @param LoyaltyProgram $loyaltyProgram
     */
    public function setLoyaltyProgram(LoyaltyProgram $loyaltyProgram)
    {
        $this->loyaltyProgram = $loyaltyProgram;
    }

    /**
     * @return bool
     */
    public function isSubscribedToLinioPlus()
    {
        return $this->subscribedToLinioPlus;
    }

    /**
     * @param bool $subscribedToLinioPlus
     */
    public function setSubscribedToLinioPlus($subscribedToLinioPlus)
    {
        $this->subscribedToLinioPlus = (bool) $subscribedToLinioPlus;
    }

    /**
     * @return bool
     */
    public function wasSubscribedToLinioPlus()
    {
        return $this->wasSubscribedToLinioPlus;
    }

    /**
     * @param bool $wasSubscribedToLinioPlus
     */
    public function setWasSubscribedToLinioPlus($wasSubscribedToLinioPlus)
    {
        $this->wasSubscribedToLinioPlus = (bool) $wasSubscribedToLinioPlus;
    }

    /**
     * @return InvoiceCollection
     */
    public function getInvoices()
    {
        return $this->invoices;
    }

    /**
     * @param InvoiceCollection $invoices
     */
    public function setInvoices(InvoiceCollection $invoices)
    {
        $this->invoices = $invoices;
    }

    /**
     * @return bool
     */
    public function hasWallet()
    {
        return $this->hasWallet;
    }

    /**
     * @param bool $hasWallet
     */
    public function setHasWallet($hasWallet)
    {
        $this->hasWallet = $hasWallet;
    }

    /**
     * @param string $impersonatedBy
     */
    public function impersonate($impersonatedBy)
    {
        $this->impersonatedBy = $impersonatedBy;
    }

    /**
     * @return string
     */
    public function getImpersonatedBy()
    {
        return $this->impersonatedBy;
    }

    /**
     * @return bool
     */
    public function isImpersonated()
    {
        return (bool) $this->impersonatedBy;
    }

    /**
     * @return bool
     */
    public function isReturningCustomer(): bool
    {
        return $this->returningCustomer;
    }

    /**
     * @param bool $returningCustomer
     */
    public function setReturningCustomer(bool $returningCustomer)
    {
        $this->returningCustomer = $returningCustomer;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return trim(sprintf('%s %s', $this->firstName, $this->lastName));
    }
}
