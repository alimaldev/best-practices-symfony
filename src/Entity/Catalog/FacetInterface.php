<?php

namespace Linio\Frontend\Entity\Catalog;

interface FacetInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getLabel();

    /**
     * @param string $label
     */
    public function setLabel($label);

    /**
     * @return string
     */
    public function getTemplateName();

    /**
     * @return bool
     */
    public function isActive();
}
