<?php

declare(strict_types=1);

namespace Linio\Frontend\Entity\Catalog;

class SearchResults
{
    /**
     * @var SearchResult
     */
    protected $originalSearchResult;

    /**
     * @var SearchResult|null
     */
    protected $didYouMeanSearchResult;

    /**
     * @param SearchResult $originalSearchResult
     */
    public function __construct(SearchResult $originalSearchResult)
    {
        $this->originalSearchResult = $originalSearchResult;
    }

    /**
     * @param SearchResult $didYouMeanSearchResult
     */
    public function setDidYouMeanSearchResult(SearchResult $didYouMeanSearchResult)
    {
        $this->didYouMeanSearchResult = $didYouMeanSearchResult;
    }

    /**
     * @return SearchResult
     */
    public function getOriginalSearchResult(): SearchResult
    {
        return $this->originalSearchResult;
    }

    /**
     * @return SearchResult|null
     */
    public function getDidYouMeanSearchResult()
    {
        return $this->didYouMeanSearchResult;
    }

    /**
     * @return SearchResult
     */
    public function getPrimarySearchResult(): SearchResult
    {
        return $this->didYouMeanSearchResult ?? $this->originalSearchResult;
    }

    /**
     * @return int
     */
    public function getTotalItemsFound(): int
    {
        if ($this->didYouMeanSearchResult) {
            return $this->originalSearchResult->getTotalItemsFound() + $this->didYouMeanSearchResult->getTotalItemsFound();
        }

        return $this->originalSearchResult->getTotalItemsFound();
    }
}
