<?php

namespace Linio\Frontend\Entity\Catalog;

use InvalidArgumentException;
use JsonSerializable;
use Linio\Frontend\Entity\Catalog\Aggregation\CategoryAggregation;

class Category implements JsonSerializable
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $urlKey;

    /**
     * @var bool
     */
    protected $isCurrent = false;

    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @var Category[]
     */
    protected $path = [];

    /**
     * @var Category[]
     */
    protected $children = [];

    /**
     * @var Category
     */
    protected $parent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int[]
     */
    public function getAllIds()
    {
        $result = [$this->getId()];

        foreach ($this->children as $child) {
            if ($child->getChildren()) {
                $result += $child->getAllIds();
            } else {
                $result[] = $child->getId();
            }
        }

        return $result;
    }

    /**
     * @return string[]
     */
    public function getAllUrlKeys()
    {
        $result = [];

        foreach ($this->path as $category) {
            $result[] = $category->getUrlKey();
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getUrlKey()
    {
        return $this->urlKey;
    }

    /**
     * @param string $urlKey
     */
    public function setUrlKey($urlKey)
    {
        $this->urlKey = $urlKey;
    }

    /**
     * @return bool
     */
    public function isCurrent()
    {
        return $this->isCurrent;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return Category[]
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param Category $category
     *
     * @return bool
     */
    public function hasChild(Category $category)
    {
        return isset($this->children[$category->getId()]);
    }

    /**
     * @return Category[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param bool $isCurrent
     */
    public function setIsCurrent($isCurrent)
    {
        $this->isCurrent = (bool) $isCurrent;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @param Category[] $path
     */
    public function setPath(array $path)
    {
        $this->path = $path;
    }

    /**
     * @param Category $category
     */
    public function addPath(Category $category)
    {
        $this->path[] = $category;
    }

    /**
     * @param Category[] $children
     */
    public function setChildren(array $children)
    {
        $this->children = $children;
    }

    /**
     * @param Category $category
     */
    public function addChild(Category $category)
    {
        $this->children[$category->getId()] = $category;
    }

    /**
     * @param Category $parent
     */
    public function setParent(Category $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @param CategoryAggregation $aggregation
     */
    public function updateAllCounters(CategoryAggregation $aggregation)
    {
        try {
            $this->count = $aggregation->getItem($this->id)->getCount();
        } catch (InvalidArgumentException $e) {
            $this->count = 0;
        }

        foreach ($this->children as $child) {
            $child->updateAllCounters($aggregation);
        }
    }

    /**
     * @return Category|null
     */
    public function getCurrent()
    {
        if ($this->isCurrent) {
            $current = $this;

            return $current;
        }

        $current = null;

        foreach ($this->children as $child) {
            $current = $child->getCurrent();
            if ($current) {
                break;
            }
        }

        return $current;
    }

    /*
     * Used to sort the categories by count on the search page.
     */
    public function sortByCount()
    {
        $children = $this->getChildren();

        uasort($children, function ($a, $b) {
            if ($a->getCount() == $b->getCount()) {
                return 0;
            } elseif ($a->getCount() > $b->getcount()) {
                return -1;
            }

            return 1;
        });

        $this->setChildren($children);
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'urlKey' => $this->urlKey,
            'isCurrent' => $this->isCurrent,
            'count' => $this->count,
            'path' => $this->path,
            'children' => $this->children,
        ];
    }
}
