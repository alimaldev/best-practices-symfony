<?php

namespace Linio\Frontend\Entity\Catalog\Aggregation;

use Linio\Frontend\Entity\Catalog\Aggregation;

class BrandAggregation extends Aggregation
{
    const CODE = 'brand';

    public function __construct()
    {
        $this->name = 'Brand';
        $this->code = self::CODE;
        $this->queryParam = 'brand';
        $this->cacheKey = 'brand';
        $this->cacheField = 'name';
    }
}
