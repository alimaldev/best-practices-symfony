<?php

namespace Linio\Frontend\Entity\Catalog\Aggregation;

use Linio\Frontend\Entity\Catalog\Aggregation;

class CategoryAggregation extends Aggregation
{
    const CODE = 'category';

    public function __construct()
    {
        $this->name = 'Category';
        $this->code = self::CODE;
        $this->queryParam = 'category';
    }
}
