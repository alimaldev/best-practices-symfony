<?php

namespace Linio\Frontend\Entity\Catalog;

use Linio\Collection\TypedCollection;

class AggregationCollection extends TypedCollection
{
    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isValidType($value)
    {
        return $value instanceof Aggregation;
    }
}
