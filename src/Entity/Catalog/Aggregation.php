<?php

namespace Linio\Frontend\Entity\Catalog;

use InvalidArgumentException;
use Linio\Frontend\Entity\Catalog\Aggregation\Item;

class Aggregation
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $queryParam;

    /**
     * @var string
     */
    protected $cacheKey;

    /**
     * @var string
     */
    protected $cacheField;

    /**
     * @var Item[]
     */
    protected $items;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getQueryParam()
    {
        return $this->queryParam;
    }

    /**
     * @return string
     */
    public function getCacheKey()
    {
        return $this->cacheKey;
    }

    /**
     * @return string
     */
    public function getCacheField()
    {
        return $this->cacheField;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->items[$item->getId()] = $item;
    }

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param int $key
     *
     * @throws InvalidArgumentException
     *
     * @return Item
     */
    public function getItem($key)
    {
        if (!isset($this->items[$key])) {
            throw new InvalidArgumentException();
        }

        return $this->items[$key];
    }

    /**
     * @param Item[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @param string $queryParam
     */
    public function setQueryParam($queryParam)
    {
        $this->queryParam = $queryParam;
    }

    /**
     * @param string $cacheKey
     */
    public function setCacheKey($cacheKey)
    {
        $this->cacheKey = $cacheKey;
    }

    /**
     * @param string $cacheField
     */
    public function setCacheField($cacheField)
    {
        $this->cacheField = $cacheField;
    }
}
