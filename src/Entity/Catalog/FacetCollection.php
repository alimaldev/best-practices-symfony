<?php

namespace Linio\Frontend\Entity\Catalog;

use Linio\Collection\TypedCollection;

class FacetCollection extends TypedCollection
{
    /**
     * @param FacetInterface $value
     *
     * @return bool
     */
    public function isValidType($value)
    {
        return $value instanceof FacetInterface;
    }
}
