<?php

namespace Linio\Frontend\Entity\Catalog;

class Campaign extends Category
{
    /**
     * @var string[]
     */
    protected $skus;

    /**
     * @var Campaign
     */
    protected $currentSegment;

    /**
     * @var array
     */
    protected $filters;

    /**
     * @return string[]
     */
    public function getSkus()
    {
        return $this->skus;
    }

    /**
     * @param string[] $skus
     */
    public function setSkus($skus)
    {
        $this->skus = $skus;
    }

    /**
     * @param array $skus
     */
    public function addSkus(array $skus)
    {
        $this->skus = array_merge($this->skus, $skus);
    }

    /**
     * @return Campaign
     */
    public function getCurrentSegment()
    {
        return $this->currentSegment;
    }

    /**
     * @param Campaign $segment
     */
    public function setCurrentSegment(Campaign $segment)
    {
        $this->currentSegment = $segment;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     */
    public function setFilters(array $filters)
    {
        $this->filters = $filters;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'parentId' => $this->parent ? $this->parent->getId() : null,
            'skus' => $this->skus,
            'currentSegment' => $this->currentSegment,
            'filters' => $this->filters,
            'urlKey' => $this->urlKey,
            'isCurrent' => $this->isCurrent,
            'count' => $this->count,
            'path' => $this->path,
            'children' => $this->children,
        ];
    }
}
