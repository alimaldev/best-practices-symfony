<?php

namespace Linio\Frontend\Entity\Catalog\Facets;

use Linio\Frontend\Entity\Catalog\FacetInterface;
use Linio\Type\Money;

class Range implements FacetInterface
{
    /**
     * @var Money
     */
    protected $minimum;

    /**
     * @var Money
     */
    protected $maximum;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Money
     */
    protected $activeMinimum;

    /**
     * @var Money
     */
    protected $activeMaximum;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var string
     */
    protected $label;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Money
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * @return Money
     */
    public function getMaximum()
    {
        return $this->maximum;
    }

    /**
     * @param Money $minimum
     */
    public function setMinimum(Money $minimum)
    {
        $this->minimum = $minimum;
    }

    /**
     * @param Money $maximum
     */
    public function setMaximum(Money $maximum)
    {
        $this->maximum = $maximum;
    }

    /**
     * @return Money
     */
    public function getActiveMinimum()
    {
        return $this->activeMinimum;
    }

    /**
     * @param Money $activeMinimum
     */
    public function setActiveMinimum(Money $activeMinimum)
    {
        $this->activeMinimum = $activeMinimum;
    }

    /**
     * @return Money
     */
    public function getActiveMaximum()
    {
        return $this->activeMaximum;
    }

    /**
     * @param Money $activeMaximum
     */
    public function setActiveMaximum(Money $activeMaximum)
    {
        $this->activeMaximum = $activeMaximum;
    }

    /**
     * @return Money
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return '::partial/components/filter/range.html.twig';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getActiveMaximum() || (bool) $this->getActiveMinimum();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
