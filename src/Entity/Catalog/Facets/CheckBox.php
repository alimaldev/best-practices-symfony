<?php

namespace Linio\Frontend\Entity\Catalog\Facets;

use Linio\Frontend\Entity\Catalog\FacetInterface;

class CheckBox implements FacetInterface
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var array
     */
    protected $values = [];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $activeValue;

    /**
     * @var string
     */
    protected $label;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues(array $values)
    {
        $this->values = $values;
    }

    /**
     * @return string
     */
    public function getActiveValue()
    {
        return $this->activeValue;
    }

    /**
     * @param string $activeValue
     */
    public function setActiveValue($activeValue)
    {
        $this->activeValue = $activeValue;
    }

    /**
     * @param string $value
     * @param int $count
     */
    public function addValue($value, $count)
    {
        $this->values[] = [
            'value' => $value,
            'count' => $count,
        ];
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return '::partial/components/filter/checkbox.html.twig';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool) $this->getActiveValue();
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
