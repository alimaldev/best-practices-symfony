<?php

namespace Linio\Frontend\Entity\Catalog\Facets;

use Linio\Frontend\Entity\Catalog\FacetInterface;

class MultipleChoice implements FacetInterface
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var array
     */
    protected $values = [];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $activeValues;

    /**
     * @var int
     */
    protected $count;

    /**
     * @var string
     */
    protected $label;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        $sortedValues = $this->values;
        uksort($sortedValues, 'strnatcasecmp');

        return $sortedValues;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @param string $key
     * @param int $count
     */
    public function addValue($key, $value, $count)
    {
        $this->values[$key] = [
            'value' => $value,
            'count' => $count,
        ];
    }

    /**
     * @return array
     */
    public function getActiveValues()
    {
        return $this->activeValues;
    }

    /**
     * @param array $activeValues
     */
    public function setActiveValues($activeValues)
    {
        $this->activeValues = $activeValues;
    }

    /**
     * @param string $value
     */
    public function addActiveValue($value)
    {
        $this->activeValues[] = $value;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getTemplateName()
    {
        return '::partial/components/filter/multiple_choice.html.twig';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->getActiveValues() > 0;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
