<?php

namespace Linio\Frontend\Entity\Catalog\Query;

abstract class Criteria
{
    /**
     * @var string
     */
    protected $field;

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }
}
