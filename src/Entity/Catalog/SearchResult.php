<?php

namespace Linio\Frontend\Entity\Catalog;

use Linio\Frontend\Entity\Product;

class SearchResult
{
    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var AggregationCollection
     */
    protected $aggregations;

    /**
     * @var int
     */
    protected $totalItemsFound;

    /**
     * @var string
     */
    protected $originalQuery;

    /**
     * @var string|null
     */
    protected $didYouMeanTerm;

    /**
     * @var string[]
     */
    protected $guidedSearchTerms = [];

    /**
     * @var string[]
     */
    protected $relatedQueries = [];

    /**
     * @var int
     */
    protected $currentPage;

    /**
     * @var FacetCollection
     */
    protected $facetCollection;

    /**
     * @var int
     */
    protected $pageCount;

    public function __construct()
    {
        $this->aggregations = new AggregationCollection();
        $this->facetCollection = new FacetCollection();
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return AggregationCollection
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @param AggregationCollection $aggregations
     */
    public function setAggregations(AggregationCollection $aggregations)
    {
        $this->aggregations = $aggregations;
    }

    /**
     * @return int
     */
    public function getTotalItemsFound()
    {
        return $this->totalItemsFound;
    }

    /**
     * @param int $totalItemsFound
     */
    public function setTotalItemsFound($totalItemsFound)
    {
        $this->totalItemsFound = $totalItemsFound;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->products);
    }

    /**
     * @return string
     */
    public function getOriginalQuery()
    {
        return $this->originalQuery;
    }

    /**
     * @param string $originalQuery
     */
    public function setOriginalQuery($originalQuery)
    {
        $this->originalQuery = $originalQuery;
    }

    /**
     * @return null|string
     */
    public function getDidYouMeanTerm()
    {
        return $this->didYouMeanTerm;
    }

    /**
     * @param null|string $didYouMeanTerm
     */
    public function setDidYouMeanTerm($didYouMeanTerm)
    {
        $this->didYouMeanTerm = $didYouMeanTerm;
    }

    /**
     * @return string[]
     */
    public function getGuidedSearchTerms()
    {
        return $this->guidedSearchTerms;
    }

    /**
     * @param string[] $guidedSearchTerms
     */
    public function setGuidedSearchTerms(array $guidedSearchTerms)
    {
        $this->guidedSearchTerms = $guidedSearchTerms;
    }

    /**
     * @return string[]
     */
    public function getRelatedQueries()
    {
        return $this->relatedQueries;
    }

    /**
     * @param string[] $relatedQueries
     */
    public function setRelatedQueries(array $relatedQueries)
    {
        $this->relatedQueries = $relatedQueries;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }

    /**
     * @param int $pageCount
     */
    public function setPageCount($pageCount)
    {
        $this->pageCount = $pageCount;
    }

    /**
     * @return FacetCollection
     */
    public function getFacetCollection()
    {
        return $this->facetCollection;
    }

    /**
     * @param FacetCollection $facetCollection
     */
    public function setFacetCollection($facetCollection)
    {
        $this->facetCollection = $facetCollection;
    }
}
