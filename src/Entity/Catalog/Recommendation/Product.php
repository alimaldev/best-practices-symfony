<?php

namespace Linio\Frontend\Entity\Catalog\Recommendation;

use JsonSerializable;
use Linio\Type\Money;

class Product implements JsonSerializable
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $brand;

    /**
     * @var Money
     */
    protected $price;

    /**
     * @var Money
     */
    protected $originalPrice;

    /**
     * @var int
     */
    protected $percentageOff = 0;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var string
     */
    protected $sku;

    public function __construct()
    {
        $this->originalPrice = new Money();
        $this->price = new Money();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param Money $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Money
     */
    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    /**
     * @param Money $originalPrice
     */
    public function setOriginalPrice($originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return float
     */
    public function getPercentageOff()
    {
        return $this->percentageOff;
    }

    /**
     * @param float $percentageOff
     */
    public function setPercentageOff($percentageOff)
    {
        $this->percentageOff = $percentageOff;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'title' => $this->title,
            'brand' => $this->brand,
            'price' => $this->price->getMoneyAmount(),
            'originalPrice' => $this->originalPrice->getMoneyAmount(),
            'percentageOff' => $this->percentageOff,
            'url' => $this->url,
            'image' => $this->image,
            'sku' => $this->sku,
        ];
    }
}
