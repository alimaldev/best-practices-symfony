<?php

namespace Linio\Frontend\Entity\Catalog\Recommendation;

class RecommendationResult
{
    /**
     * @var Product[]
     */
    protected $products = [];

    /**
     * @var int
     */
    protected $totalProductsFound = 0;

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return int
     */
    public function getTotalProductsFound()
    {
        return $this->totalProductsFound;
    }

    /**
     * @param int $totalProductsFound
     */
    public function setTotalProductsFound($totalProductsFound)
    {
        $this->totalProductsFound = $totalProductsFound;
    }
}
