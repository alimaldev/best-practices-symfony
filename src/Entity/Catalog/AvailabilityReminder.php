<?php

namespace Linio\Frontend\Entity\Catalog;

class AvailabilityReminder
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var bool
     */
    protected $acceptNewsletter;

    /**
     * @var string
     */
    protected $simple;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var int
     */
    protected $fkCustomer;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param bool $acceptNewsletter
     */
    public function setAcceptNewsletter($acceptNewsletter)
    {
        $this->acceptNewsletter = $acceptNewsletter;
    }

    /**
     * @param string $simple
     */
    public function setSimple($simple)
    {
        $this->simple = $simple;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @param int $fkCustomer
     */
    public function setFkCustomer($fkCustomer)
    {
        $this->fkCustomer = $fkCustomer;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function getAcceptNewsletter()
    {
        return $this->acceptNewsletter;
    }

    /**
     * @return string
     */
    public function getSimple()
    {
        return $this->simple;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return int
     */
    public function getFkCustomer()
    {
        return $this->fkCustomer;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
}
