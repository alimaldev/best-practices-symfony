<?php

namespace Linio\Frontend\SlugResolver;

trait SlugResolverAware
{
    /**
     * @var SlugResolverService
     */
    protected $slugResolverService;

    /**
     * @codeCoverageIgnore
     *
     * @return SlugResolverService
     */
    public function getSlugResolverService()
    {
        return $this->slugResolverService;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param SlugResolverService $slugResolver
     */
    public function setSlugResolverService(SlugResolverService $slugResolver)
    {
        $this->slugResolverService = $slugResolver;
    }
}
