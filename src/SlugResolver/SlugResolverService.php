<?php

declare(strict_types=1);

namespace Linio\Frontend\SlugResolver;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Catalog\Redirect;
use Linio\Frontend\SlugResolver\Exception\ResolverNotRegisteredException;
use Linio\Frontend\SlugResolver\Exception\SlugNotFoundException;

class SlugResolverService
{
    use CacheAware;

    /**
     * @var ResolverInterface[]
     */
    protected $resolvers = [];

    /**
     * @param string $resolverName
     * @param string $slug
     *
     * @return mixed
     */
    public function resolve(string $resolverName, string $slug)
    {
        $resolver = $this->getResolver($resolverName);

        if (!$slug) {
            throw new SlugNotFoundException($slug);
        }

        $data = $this->getCacheData($resolver, $slug);

        return $resolver->resolve($data);
    }

    /**
     * @param string $slug
     *
     * @throws SlugNotFoundException
     *
     * @return Redirect
     */
    public function resolveLegacy(string $slug): Redirect
    {
        $resolver = $this->getResolver('redirect');

        try {
            $data = $this->getCacheData($resolver, $slug);
        } catch (SlugNotFoundException $exception) {
            $data = $this->getCombinedLegacyData($resolver, $slug);
        }

        if (empty($data['url'])) {
            throw new SlugNotFoundException($slug);
        }

        return $resolver->resolve($data);
    }

    /**
     * @param ResolverInterface $resolver
     * @param string $slug
     *
     * @return array
     */
    protected function getCombinedLegacyData(ResolverInterface $resolver, string $slug): array
    {
        $slugs = explode('/', $slug);
        $url = '';
        $isAfterCategory = false;

        foreach ($slugs as $slug) {
            try {
                $data = $this->getCacheData($resolver, $slug);
            } catch (SlugNotFoundException $exception) {
                continue;
            }

            $isCategory = strpos($data['url'], '/c/') === 0;

            if ($isCategory && $isAfterCategory) {
                $data['url'] = substr($data['url'], 2);
            }

            $url .= $data['url'];

            if ($isCategory) {
                $isAfterCategory = true;
            }
        }

        return ['url' => $url];
    }

    /**
     * @param string $name
     *
     * @throws ResolverNotRegisteredException
     *
     * @return ResolverInterface
     */
    protected function getResolver(string $name): ResolverInterface
    {
        if (!isset($this->resolvers[$name])) {
            throw new ResolverNotRegisteredException();
        }

        return $this->resolvers[$name];
    }

    /**
     * @param ResolverInterface $resolver
     * @param string $slug
     *
     * @throws SlugNotFoundException
     *
     * @return mixed
     */
    protected function getCacheData(ResolverInterface $resolver, string $slug)
    {
        $data = $this->cacheService->get($resolver->getCacheKey() . ':' . ltrim($resolver->parseSlug($slug), '/'));

        if (!$data) {
            throw new SlugNotFoundException($slug);
        }

        return $data;
    }

    /**
     * @param ResolverInterface $resolver
     */
    public function addResolver(ResolverInterface $resolver)
    {
        $this->resolvers[$resolver->getName()] = $resolver;
    }

    /**
     * @return array
     */
    public function getAvailableResolvers(): array
    {
        return array_keys($this->resolvers);
    }
}
