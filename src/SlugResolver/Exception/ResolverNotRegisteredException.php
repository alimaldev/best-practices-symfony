<?php

declare(strict_types=1);

namespace Linio\Frontend\SlugResolver\Exception;

use Linio\Frontend\Exception\ExceptionMessage;

class ResolverNotRegisteredException extends SlugResolverException
{
    public function __construct()
    {
        parent::__construct(ExceptionMessage::RESOLVER_NOT_FOUND);
    }
}
