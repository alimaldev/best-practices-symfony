<?php

declare(strict_types=1);

namespace Linio\Frontend\SlugResolver\Exception;

use Linio\Frontend\Product\Exception\CatalogException;

class SlugResolverException extends CatalogException
{
}
