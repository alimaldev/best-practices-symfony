<?php

declare(strict_types=1);

namespace Linio\Frontend\SlugResolver\Exception;

class SlugNotFoundException extends SlugResolverException
{
}
