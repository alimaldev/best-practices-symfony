<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException;
use Linio\Frontend\SlugResolver\ResolverInterface;

class CategoryResolver implements ResolverInterface
{
    use CacheAware;

    /**
     * @param array $data
     *
     * @throws InvalidResolverDataException
     *
     * @return mixed
     */
    public function resolve(array $data)
    {
        if (empty($data['id']) || empty($data['name'])) {
            throw new InvalidResolverDataException();
        }

        $category = new Category();
        $category->setId($data['id']);
        $category->setName($data['name']);

        $categorySlug = $this->getCategorySlug($data['id']);

        if (!is_null($categorySlug)) {
            $category->setSlug($categorySlug);
        }

        return $category;
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string
    {
        return $slug;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return 'route:category';
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName(): string
    {
        return 'category';
    }

    /**
     * @param string $categoryId
     *
     * @return string
     */
    protected function getCategorySlug($categoryId)
    {
        $cacheCategory = $this->cacheService->get($categoryId);

        return $cacheCategory ? $cacheCategory['slug'] : null;
    }
}
