<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Frontend\Catalog\CampaignService;
use Linio\Frontend\Catalog\Exception\CampaignSegmentNotFoundException;
use Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException;
use Linio\Frontend\SlugResolver\ResolverInterface;

class CampaignResolver implements ResolverInterface
{
    /**
     * @var CampaignService
     */
    protected $campaignService;

    /**
     * @param CampaignService $campaignService
     */
    public function setCampaignService(CampaignService $campaignService)
    {
        $this->campaignService = $campaignService;
    }

    /**
     * @param array $data
     *
     * @throws InvalidResolverDataException
     *
     * @return mixed
     */
    public function resolve(array $data)
    {
        if (empty($data['id']) || empty($data['slug'])) {
            throw new InvalidResolverDataException();
        }

        try {
            return $this->campaignService->getCampaign($data['id'], $data['slug']);
        } catch (CampaignSegmentNotFoundException $exception) {
            throw new InvalidResolverDataException();
        }
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string
    {
        return $slug;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return 'route:campaign';
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName(): string
    {
        return 'campaign';
    }
}
