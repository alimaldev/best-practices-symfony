<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Frontend\Product\ProductAware;
use Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException;
use Linio\Frontend\SlugResolver\ResolverInterface;

class ProductResolver implements ResolverInterface
{
    use ProductAware;

    /**
     * @param array $data
     *
     * @throws InvalidResolverDataException
     *
     * @return mixed
     */
    public function resolve(array $data)
    {
        if (empty($data['sku'])) {
            throw new InvalidResolverDataException();
        }

        return $this->productService->getBySku($data['sku']);
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string
    {
        $startSlug = strrpos($slug, '-');

        if ($startSlug === false) {
            return $slug;
        }

        return substr($slug, ++$startSlug);
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return 'route:product';
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName(): string
    {
        return 'product';
    }
}
