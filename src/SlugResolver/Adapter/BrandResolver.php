<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException;
use Linio\Frontend\SlugResolver\ResolverInterface;

class BrandResolver implements ResolverInterface
{
    const BRAND_CACHE_KEY = 'brand';

    use CacheAware;

    /**
     * @param array $data
     *
     * @throws InvalidResolverDataException
     *
     * @return Brand
     */
    public function resolve(array $data)
    {
        if (empty($data['id']) || empty($data['name'])) {
            throw new InvalidResolverDataException();
        }

        $brand = new Brand();
        $brand->setId($data['id']);
        $brand->setName($data['name']);
        $brand->setSlug($this->getBrandSlug($data['id']));

        return $brand;
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string
    {
        return $slug;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return 'route:brand';
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName(): string
    {
        return 'brand';
    }

    /**
     * @param string $brandId
     *
     * @return string
     */
    protected function getBrandSlug($brandId)
    {
        $key = sprintf('%s:%s', self::BRAND_CACHE_KEY, $brandId);
        $cacheBrand = $this->cacheService->get($key);

        return $cacheBrand ? $cacheBrand['slug'] : null;
    }
}
