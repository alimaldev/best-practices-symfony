<?php

declare(strict_types=1);

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Frontend\Entity\Seller;
use Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException;
use Linio\Frontend\SlugResolver\ResolverInterface;

class SellerResolver implements ResolverInterface
{
    /**
     * @param array $data
     *
     * @throws InvalidResolverDataException
     *
     * @return Seller
     */
    public function resolve(array $data)
    {
        if (empty($data['id']) || empty($data['name'])) {
            throw new InvalidResolverDataException();
        }

        $seller = new Seller();
        $seller->setSellerId($data['id']);
        $seller->setName($data['name']);

        return $seller;
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string
    {
        return $slug;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return 'route:seller';
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName(): string
    {
        return 'seller';
    }
}
