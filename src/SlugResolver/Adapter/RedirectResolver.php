<?php

namespace Linio\Frontend\SlugResolver\Adapter;

use Linio\Frontend\Entity\Catalog\Redirect;
use Linio\Frontend\SlugResolver\Exception\InvalidResolverDataException;
use Linio\Frontend\SlugResolver\ResolverInterface;

class RedirectResolver implements ResolverInterface
{
    /**
     * @param array $data
     *
     * @throws InvalidResolverDataException
     *
     * @return Redirect
     */
    public function resolve(array $data)
    {
        if (!isset($data['url']) || empty($data['url'])) {
            throw new InvalidResolverDataException();
        }

        $redirect = new Redirect();
        $redirect->setUrl($data['url']);

        return $redirect;
    }

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string
    {
        return $slug;
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getCacheKey(): string
    {
        return 'route:redirect';
    }

    /**
     * @codeCoverageIgnore
     *
     * @return string
     */
    public function getName(): string
    {
        return 'redirect';
    }
}
