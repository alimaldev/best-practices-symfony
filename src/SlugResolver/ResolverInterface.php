<?php

declare(strict_types=1);

namespace Linio\Frontend\SlugResolver;

interface ResolverInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function resolve(array $data);

    /**
     * @param string $slug
     *
     * @return string
     */
    public function parseSlug(string $slug): string;

    /**
     * @return string
     */
    public function getCacheKey(): string;

    /**
     * @return string
     */
    public function getName(): string;
}
