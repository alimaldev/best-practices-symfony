<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter;

class Preference
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * @param int $id
     * @param string $name
     * @param bool $enabled
     */
    public function __construct(int $id, string $name, bool $enabled)
    {
        $this->id = $id;
        $this->name = $name;
        $this->enabled = $enabled;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function enable()
    {
        $this->enabled = true;
    }

    public function disable()
    {
        $this->enabled = false;
    }
}
