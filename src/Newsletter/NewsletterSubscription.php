<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter;

class NewsletterSubscription
{
    const DEFAULT_FREQUENCY = 2;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $phoneNumber;

    /**
     * @var bool
     */
    protected $emailSubscribed = false;

    /**
     * @var bool
     */
    protected $smsSubscribed = false;

    /**
     * @var int
     */
    protected $frequency = self::DEFAULT_FREQUENCY;

    /**
     * @var array
     */
    protected $frequencies = [];

    /**
     * @var Preference[]
     */
    protected $preferences = [];

    /**
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return bool
     */
    public function isSubscribedToEmail(): bool
    {
        return $this->emailSubscribed;
    }

    public function subscribeToEmail()
    {
        $this->emailSubscribed = true;
    }

    public function unsubscribeFromEmail()
    {
        $this->emailSubscribed = false;
    }

    /**
     * @return bool
     */
    public function isSubscribedToSms(): bool
    {
        return $this->smsSubscribed;
    }

    public function subscribeToSms()
    {
        $this->smsSubscribed = true;
    }

    public function unsubscribeFromSms()
    {
        $this->smsSubscribed = false;
    }

    /**
     * @return int
     */
    public function getFrequency(): int
    {
        return $this->frequency;
    }

    /**
     * @param int $frequency
     */
    public function setFrequency(int $frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * @return Preference[]
     */
    public function getPreferences(): array
    {
        return $this->preferences;
    }

    /**
     * @param Preference[] $preferences
     */
    public function setPreferences(array $preferences)
    {
        $this->preferences = $preferences;
    }

    /**
     * @return array
     */
    public function getFrequencies(): array
    {
        return $this->frequencies;
    }

    /**
     * @param array $frequencies
     */
    public function setFrequencies(array $frequencies)
    {
        $this->frequencies = $frequencies;
    }
}
