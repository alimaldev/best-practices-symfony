<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter\Exception;

use Linio\Frontend\Exception\DomainException;

class NewsletterException extends DomainException
{
}
