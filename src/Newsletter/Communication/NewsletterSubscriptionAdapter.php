<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter\Communication;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Newsletter\Exception\NewsletterException;
use Linio\Frontend\Newsletter\NewsletterSubscription;
use Linio\Frontend\Newsletter\PromotionalNewsletterSubscription;

interface NewsletterSubscriptionAdapter
{
    /**
     * @param PromotionalNewsletterSubscription $newsletterSubscription
     * @param string $clientIp
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     *
     * @return PromotionalNewsletterSubscription
     */
    public function subscribe(PromotionalNewsletterSubscription $newsletterSubscription, string $clientIp): PromotionalNewsletterSubscription;

    /**
     * @param string $email
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     */
    public function unsubscribe(string $email);

    /**
     * @param Customer $customer
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     *
     * @return NewsletterSubscription
     */
    public function getNewsletterSubscription(Customer $customer): NewsletterSubscription;

    /**
     * @param Customer $customer
     * @param NewsletterSubscription $newsletterSubscription
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     */
    public function updateNewsletterSubscription(Customer $customer, NewsletterSubscription $newsletterSubscription);
}
