<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter\Communication;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Newsletter\Exception\NewsletterException;
use Linio\Frontend\Newsletter\NewsletterSubscription;
use Linio\Frontend\Newsletter\Preference;
use Linio\Frontend\Newsletter\PromotionalNewsletterSubscription;
use Linio\Frontend\Request\PlatformInformation;

class Bob4Alice implements NewsletterSubscriptionAdapter
{
    const STATUS_SUBSCRIBED = 'subscribed';
    const STATUS_UNSUBSCRIBED = 'unsubscribed';

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param PlatformInformation $platformInformation
     */
    public function setPlatformInformation(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    /**
     * @param PromotionalNewsletterSubscription $newsletterSubscription
     * @param string $clientIp
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     *
     * @return PromotionalNewsletterSubscription
     */
    public function subscribe(PromotionalNewsletterSubscription $newsletterSubscription, string $clientIp): PromotionalNewsletterSubscription
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $newsletterSubscription->getEmail(),
            'clientIp' => $clientIp,
        ];

        try {
            $response = $this->client->request('POST', '/newsletter/subscribe-email', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $subscribedNewsletterSubscription = new PromotionalNewsletterSubscription($newsletterSubscription->getEmail());
        $subscribedNewsletterSubscription->subscribeToEmail();
        $subscribedNewsletterSubscription->setSource($newsletterSubscription->getSource());

        if (isset($responseBody['couponCode'])) {
            $subscribedNewsletterSubscription->setCoupon($responseBody['couponCode']);
        }

        return $subscribedNewsletterSubscription;
    }

    /**
     * @param string $email
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     */
    public function unsubscribe(string $email)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $email,
        ];

        try {
            $this->client->request('POST', '/newsletter/unsubscribe-email', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        }
    }

    /**
     * @param Customer $customer
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     *
     * @return NewsletterSubscription
     */
    public function getNewsletterSubscription(Customer $customer): NewsletterSubscription
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/newsletter/status', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $newsletterSubscription = new NewsletterSubscription($responseBody['email']);
        $newsletterSubscription->setFrequencies($responseBody['frequencies']);

        if (isset($responseBody['frequency'])) {
            $newsletterSubscription->setFrequency($responseBody['frequency']);
        }

        if ($responseBody['emailStatus'] == self::STATUS_SUBSCRIBED) {
            $newsletterSubscription->subscribeToEmail();
        }

        if (isset($responseBody['phone'])) {
            $newsletterSubscription->setPhoneNumber($responseBody['phone']);
        }

        if ($responseBody['smsStatus'] == self::STATUS_SUBSCRIBED) {
            $newsletterSubscription->subscribeToSms();
        }

        $preferences = [];

        foreach ($responseBody['preferences'] as $preferenceData) {
            $preferences[] = new Preference(
                $preferenceData['newsletterPreferenceId'],
                $preferenceData['name'],
                $preferenceData['enabled']
            );
        }

        $newsletterSubscription->setPreferences($preferences);

        return $newsletterSubscription;
    }

    /**
     * @param Customer $customer
     * @param NewsletterSubscription $newsletterSubscription
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     */
    public function updateNewsletterSubscription(Customer $customer, NewsletterSubscription $newsletterSubscription)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'email' => $newsletterSubscription->getEmail(),
            'emailStatus' => $newsletterSubscription->isSubscribedToEmail() ? self::STATUS_SUBSCRIBED : self::STATUS_UNSUBSCRIBED,
            'gender' => $customer->getGender(),
            'frequency' => $newsletterSubscription->getFrequency(),
            'phone' => $newsletterSubscription->getPhoneNumber(),
            'smsStatus' => $newsletterSubscription->isSubscribedToSms() ? self::STATUS_SUBSCRIBED : self::STATUS_UNSUBSCRIBED,
            'preferenceIds' => [],
        ];

        foreach ($newsletterSubscription->getPreferences() as $preference) {
            if ($preference->isEnabled()) {
                $requestBody['preferenceIds'][] = $preference->getId();
            }
        }

        try {
            $this->client->request('POST', '/newsletter/edit', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new NewsletterException($responseBody['code']);
        }
    }
}
