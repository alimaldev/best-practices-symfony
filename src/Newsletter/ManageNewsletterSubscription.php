<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Newsletter\Communication\NewsletterSubscriptionAdapter;
use Linio\Frontend\Newsletter\Exception\NewsletterException;

class ManageNewsletterSubscription
{
    /**
     * @var NewsletterSubscriptionAdapter
     */
    protected $adapter;

    /**
     * @param NewsletterSubscriptionAdapter $adapter
     */
    public function setAdapter(NewsletterSubscriptionAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $email
     * @param string $source
     * @param string $clientIp
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     *
     * @return PromotionalNewsletterSubscription
     */
    public function subscribe(string $email, string $source, string $clientIp): PromotionalNewsletterSubscription
    {
        $newsletterSubscription = new PromotionalNewsletterSubscription($email);
        $newsletterSubscription->setSource($source);
        $newsletterSubscription->subscribeToEmail();

        return $this->adapter->subscribe($newsletterSubscription, $clientIp);
    }

    /**
     * @param string $email
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     */
    public function unsubscribe(string $email)
    {
        $this->adapter->unsubscribe($email);
    }

    /**
     * @param Customer $customer
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     *
     * @return NewsletterSubscription
     */
    public function getNewsletterSubscription(Customer $customer): NewsletterSubscription
    {
        return $this->adapter->getNewsletterSubscription($customer);
    }

    /**
     * @param Customer $customer
     * @param NewsletterSubscription $newsletterSubscription
     *
     * @throws NewsletterException Thrown for client errors
     * @throws NewsletterException Thrown for server errors
     */
    public function updateNewsletterSubscription(Customer $customer, NewsletterSubscription $newsletterSubscription)
    {
        if ($newsletterSubscription->getFrequency() > 0) {
            $newsletterSubscription->subscribeToEmail();
        }

        // Always update the email with the latest from the customer
        $newsletterSubscription->setEmail($customer->getEmail());
        $this->adapter->updateNewsletterSubscription($customer, $newsletterSubscription);
    }
}
