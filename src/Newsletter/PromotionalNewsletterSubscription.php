<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter;

class PromotionalNewsletterSubscription extends NewsletterSubscription
{
    /**
     * @var string
     */
    protected $coupon;

    /**
     * @var string
     */
    protected $source;

    /**
     * @return string
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param string $coupon
     */
    public function setCoupon(string $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source)
    {
        $this->source = $source;
    }
}
