<?php

declare(strict_types=1);

namespace Linio\Frontend\Newsletter;

trait ManageNewsletterAware
{
    /**
     * @var ManageNewsletterSubscription
     */
    protected $manageNewsletterSubscription;

    /**
     * @return ManageNewsletterSubscription
     */
    public function getManageNewsletterSubscription(): ManageNewsletterSubscription
    {
        return $this->manageNewsletterSubscription;
    }

    /**
     * @param ManageNewsletterSubscription $manageNewsletterSubscription
     */
    public function setManageNewsletterSubscription(ManageNewsletterSubscription $manageNewsletterSubscription)
    {
        $this->manageNewsletterSubscription = $manageNewsletterSubscription;
    }
}
