<?php

namespace Linio\Frontend\Handler;

use Linio\Component\Input\Handler\AbstractHandler;
use Linio\Frontend\Location\Address;

class CustomerAddressHandler extends AbstractHandler
{
    public function define()
    {
        $customerAddress = $this->add('customerAddress', Address::class);
        $customerAddress->add('idCustomerAddress', 'int', ['required' => false]);
        $customerAddress->add('fkCustomer', 'int', ['required' => false]);
        $customerAddress->add('firstName', 'string', ['required' => false]);
        $customerAddress->add('lastName', 'string', ['required' => false]);
        $customerAddress->add('address1', 'string', ['required' => false]);
        $customerAddress->add('street_number', 'string', ['required' => false]);
        $customerAddress->add('address2', 'string', ['required' => false]);
        $customerAddress->add('city', 'string', ['required' => false]);
        $customerAddress->add('municipality', 'string', ['required' => false]);
        $customerAddress->add('neighborhood', 'string', ['required' => false]);
        $customerAddress->add('region', 'string', ['required' => false]);
        $customerAddress->add('fkCountry', 'int', ['required' => false]);
        $customerAddress->add('phone', 'string', ['required' => false]);
        $customerAddress->add('prefix', 'string', ['required' => false]);
        $customerAddress->add('mobilePhone', 'string', ['required' => false]);
        $customerAddress->add('postcode', 'string', ['required' => false]);
        $customerAddress->add('defaultBilling', 'boolean', ['required' => false]);
        $customerAddress->add('defaultShipping', 'boolean', ['required' => false]);
        $customerAddress->add('company', 'string', ['required' => false]);
        $customerAddress->add('taxIdentificationNumber', 'string', ['required' => false]);
        $customerAddress->add('regime', 'string', ['required' => false]);
        $customerAddress->add('fkCustomerAddressRegion', 'int', ['required' => false]);
        $customerAddress->add('betweenStreet1', 'string', ['required' => false]);
        $customerAddress->add('betweenStreet2', 'string', ['required' => false]);
        $customerAddress->add('additionalInformation', 'string', ['required' => false]);
        $customerAddress->add('idSalesOrderAddress', 'int', ['required' => false]);
    }
}
