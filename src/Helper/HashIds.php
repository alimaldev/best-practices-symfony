<?php

namespace Linio\Frontend\Helper;

class HashIds
{
    const MAX_ID = 2100000000;

    public function getHashId($id)
    {
        $padded = str_pad($id, 9, '0', STR_PAD_LEFT);
        $scrambled = $padded[8] . $padded[0] . $padded[7] . $padded[2] . $padded[4] . $padded[5] . $padded[1] . $padded[6] . $padded[3];

        return base_convert(self::MAX_ID - $scrambled, 10, 36);
    }

    public function getId($hashId)
    {
        $scrambled = str_pad(abs(base_convert($hashId, 36, 10) - self::MAX_ID), 9, '0', STR_PAD_LEFT);
        $padded = $scrambled[1] . $scrambled[6] . $scrambled[3] . $scrambled[8] . $scrambled[4] . $scrambled[5] . $scrambled[7] . $scrambled[2] . $scrambled[0];

        return (int) $padded;
    }
}
