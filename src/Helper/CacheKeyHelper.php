<?php

namespace Linio\Frontend\Helper;

trait CacheKeyHelper
{
    /**
     * @param string $prefix
     * @param string $key
     *
     * @return string
     */
    public function addPrefixToKey($prefix, $key)
    {
        return $prefix . ':' . $key;
    }

    /**
     * @param string $prefix
     * @param array  $keys
     * @param bool   $isKeyValue
     *
     * @return array
     */
    public function addPrefixToKeys($prefix, array $keys, $isKeyValue = false)
    {
        $prefixedKeys = [];

        foreach ($keys as $key => $value) {
            if ($isKeyValue) {
                $prefixedKeys[$this->addPrefixToKey($prefix, $key)] = $value;
            } else {
                $prefixedKeys[$key] = $this->addPrefixToKey($prefix, $value);
            }
        }

        return $prefixedKeys;
    }
}
