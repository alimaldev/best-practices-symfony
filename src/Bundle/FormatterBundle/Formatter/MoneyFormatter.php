<?php

namespace Linio\Frontend\Bundle\FormatterBundle\Formatter;

use Linio\Type\Money;

class MoneyFormatter
{
    /**
     * @var string
     */
    protected $currencyCode;

    /**
     * @var string
     */
    protected $currencySymbol;

    /**
     * @var int
     */
    protected $decimalPrecision;

    /**
     * @var string
     */
    protected $decimalPoint;

    /**
     * @var string
     */
    protected $thousandsSeparator;

    /**
     * @param string $currencyCode
     * @param string $currencySymbol
     * @param string $decimalPoint
     * @param string $thousandsSeparator
     * @param int $decimalPrecision
     */
    public function __construct($currencyCode, $currencySymbol, $decimalPoint, $thousandsSeparator, $decimalPrecision = 2)
    {
        $this->currencyCode = $currencyCode;
        $this->currencySymbol = $currencySymbol;
        $this->decimalPoint = $decimalPoint;
        $this->thousandsSeparator = $thousandsSeparator;
        $this->decimalPrecision = $decimalPrecision;
    }

    /**
     * @param Money $value
     *
     * @return string
     */
    public function format(Money $value = null)
    {
        if (!$value) {
            return;
        }

        return $this->currencySymbol . number_format($value->getMoneyAmount(), $this->decimalPrecision, $this->decimalPoint, $this->thousandsSeparator);
    }

    /**
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->currencySymbol;
    }

    /**
     * @return int
     */
    public function getDecimalPrecision()
    {
        return $this->decimalPrecision;
    }

    /**
     * @return string
     */
    public function getDecimalPoint()
    {
        return $this->decimalPoint;
    }

    /**
     * @return string
     */
    public function getThousandsSeparator()
    {
        return $this->thousandsSeparator;
    }
}
