<?php

namespace Linio\Frontend\Bundle\FormatterBundle\Formatter;

use DateTimeInterface;
use IntlDateFormatter;

class DateFormatter
{
    /**
     * @var string
     */
    protected $shortFormat;

    /**
     * @var string
     */
    protected $longFormat;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @param string $shortFormat
     * @param string $longFormat
     * @param string|null $locale
     */
    public function __construct($shortFormat, $longFormat, $locale = null)
    {
        $this->shortFormat = $shortFormat;
        $this->longFormat = $longFormat;
        $this->locale = $locale;
    }

    public function format(DateTimeInterface $value = null)
    {
        if (!$value) {
            return;
        }

        return $value->format($this->shortFormat);
    }

    public function formatLong(DateTimeInterface $value = null)
    {
        if (!$value) {
            return;
        }

        return $value->format($this->longFormat);
    }

    public function formatLocale(DateTimeInterface $value = null)
    {
        if (!$value) {
            return;
        }

        $formatter = new IntlDateFormatter($this->locale, IntlDateFormatter::LONG, IntlDateFormatter::NONE);

        return $formatter->format($value);
    }
}
