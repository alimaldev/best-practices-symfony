<?php

declare(strict_types=1);

namespace Linio\Frontend\Bundle\FormatterBundle\Formatter;

use Linio\Frontend\Entity\Customer\Wallet\Points;

class PointsFormatter
{
    /**
     * @var string
     */
    protected $pointsSymbol;

    /**
     * @var int
     */
    protected $decimalPrecision;

    /**
     * @var string
     */
    protected $decimalPoint;

    /**
     * @var string
     */
    protected $thousandsSeparator;

    /**
     * @param string $pointsSymbol
     * @param string $decimalPoint
     * @param string $thousandsSeparator
     * @param int $decimalPrecision
     */
    public function __construct(
        string $pointsSymbol,
        string $decimalPoint = '',
        string $thousandsSeparator = '',
        int $decimalPrecision = 0
    ) {
        $this->decimalPoint = $decimalPoint;
        $this->thousandsSeparator = $thousandsSeparator;
        $this->decimalPrecision = $decimalPrecision;
        $this->pointsSymbol = $pointsSymbol;
    }

    /**
     * @param Points $points
     *
     * @return string
     */
    public function format(Points $points = null): string
    {
        if (!$points) {
            return '';
        }

        return sprintf('%s%s%s',
            $this->pointsSymbol,
            $this->decimalPrecision ? '' : ' ',
            number_format(
                $points->getMoneyAmount(),
                $this->decimalPrecision,
                $this->decimalPoint,
                $this->thousandsSeparator
            )
        );
    }
}
