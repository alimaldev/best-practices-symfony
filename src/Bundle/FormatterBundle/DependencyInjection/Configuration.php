<?php

namespace Linio\Frontend\Bundle\FormatterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('formatter');

        $rootNode
            ->children()
                ->arrayNode('date')->isRequired()->children()
                    ->scalarNode('short')->isRequired()->end()
                    ->scalarNode('long')->isRequired()->end()
                    ->scalarNode('locale')->isRequired()->end()
                ->end()->end()
                ->arrayNode('currency')->isRequired()->children()
                    ->scalarNode('code')->isRequired()->end()
                    ->scalarNode('symbol')->isRequired()->end()
                    ->scalarNode('decimal_point')->isRequired()->end()
                    ->scalarNode('thousands_separator')->isRequired()->end()
                    ->integerNode('decimal_precision')->defaultValue(2)->end()
                ->end()->end()
                ->arrayNode('points')->isRequired()->children()
                    ->scalarNode('symbol')->isRequired()->end()
                    ->scalarNode('decimal_point')->defaultValue('')->end()
                    ->scalarNode('thousands_separator')->defaultValue('')->end()
                    ->integerNode('decimal_precision')->defaultValue(0)->end()
                ->end()->end()
            ->end();

        return $treeBuilder;
    }
}
