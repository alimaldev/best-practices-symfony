<?php

namespace Linio\Frontend\Formatter;

use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Type\Money;

class MoneyFormatterTest extends \PHPUnit_Framework_TestCase
{
    public function testIsFormattingMoney()
    {
        $formatter = new MoneyFormatter('BRL', 'R$', ',', '.');
        $this->assertEquals('R$1.500,54', $formatter->format(new Money(1500.54)));
        $this->assertEquals('BRL', $formatter->getCurrencyCode());
    }

    public function testIsNotFormattingEmptyMoney()
    {
        $formatter = new MoneyFormatter('BRL', 'R$', ',', '.');
        $this->assertEmpty($formatter->format(null));
    }
}
