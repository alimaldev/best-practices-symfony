<?php

namespace Linio\Frontend\Formatter;

use DateTime;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;

class DateFormatterTest extends \PHPUnit_Framework_TestCase
{
    public function testIsFormattingShortDates()
    {
        $formatter = new DateFormatter('d/m/Y', 'l jS \of F Y h:i:s A');
        $this->assertEquals('22/01/1990', $formatter->format(new DateTime('1990-01-22 09:52')));
    }

    public function testIsFormattingLongDates()
    {
        $formatter = new DateFormatter('d/m/Y', 'l jS \of F Y h:i:s A');
        $this->assertEquals('Monday 22nd of January 1990 09:52:00 AM', $formatter->formatLong(new DateTime('1990-01-22 09:52')));
    }

    public function testIsFormattingLocaleDates()
    {
        $formatter = new DateFormatter('d/m/Y', 'l jS \of F Y h:i:s A', 'es_MX');
        $this->assertEquals('22 de enero de 1990', $formatter->formatLocale(new DateTime('1990-01-22 09:52')));
    }

    public function testIsNotFormattingEmptyShortDates()
    {
        $formatter = new DateFormatter('d/m/Y', 'l jS \of F Y h:i:s A');
        $this->assertEmpty($formatter->format(null));
    }

    public function testIsNotFormattingEmptyLongDates()
    {
        $formatter = new DateFormatter('d/m/Y', 'l jS \of F Y h:i:s A');
        $this->assertEmpty($formatter->formatLong(null));
    }

    public function testIsNotFormattingEmptyLocaleDates()
    {
        $formatter = new DateFormatter('d/m/Y', 'l jS \of F Y h:i:s A', 'es_MX');
        $this->assertEmpty($formatter->formatLocale(null));
    }
}
