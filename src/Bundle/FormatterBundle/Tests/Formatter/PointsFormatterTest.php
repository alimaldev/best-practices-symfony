<?php

declare(strict_types=1);

namespace Linio\Frontend\Formatter;

use Linio\Frontend\Bundle\FormatterBundle\Formatter\PointsFormatter;
use Linio\Frontend\Entity\Customer\Wallet\Points;
use PHPUnit_Framework_TestCase;

class PointsFormatterTest extends PHPUnit_Framework_TestCase
{
    public function testIsFormattingPoints()
    {
        $formatter = new PointsFormatter('PTS', '', ',');

        $this->assertEquals('PTS 1,500', $formatter->format(new Points(1500)));
    }

    public function testIsFormattingPointsAsMoney()
    {
        $formatter = new PointsFormatter('$', '.', ',', 2);

        $this->assertEquals('$1,500.54', $formatter->format(Points::fromCents(150054)));
    }

    public function testIsNotFormattingEmptyPoints()
    {
        $formatter = new PointsFormatter('PTS', '', ',');

        $this->assertEmpty($formatter->format(null));
    }
}
