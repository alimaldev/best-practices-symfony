<?php

namespace Linio\Frontend\Twig;

use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\PointsFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Twig\FormatterExtension;
use Linio\Frontend\Entity\Customer\Wallet\Points;
use Linio\Type\Money;

class FormatterExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testIsFormattingMoney()
    {
        $value = new Money(100);
        $formatter = $this->prophesize(MoneyFormatter::class);
        $formatter->format($value)->willReturn('R$100,00');

        $extension = new FormatterExtension();
        $extension->setMoneyFormatter($formatter->reveal());
        $this->assertEquals('R$100,00', $extension->formatMoney($value));
    }

    public function testIsFormattingPercentage()
    {
        $extension = new FormatterExtension();
        $this->assertEquals('45%', $extension->formatPercentage(45.66));
    }

    public function testIsFormattingDate()
    {
        $value = new \DateTime('2015-01-01');
        $formatter = $this->prophesize(DateFormatter::class);
        $formatter->format($value)->willReturn('2015-01-01');

        $extension = new FormatterExtension();
        $extension->setDateFormatter($formatter->reveal());
        $this->assertEquals('2015-01-01', $extension->formatDate($value));
    }

    public function testIsFormattingLongDate()
    {
        $value = new \DateTime('2015-01-01');
        $formatter = $this->prophesize(DateFormatter::class);
        $formatter->formatLong($value)->willReturn('Thu 01, Jan 2015');

        $extension = new FormatterExtension();
        $extension->setDateFormatter($formatter->reveal());
        $this->assertEquals('Thu 01, Jan 2015', $extension->formatLongDate($value));
    }

    public function testIsFormattingPoints()
    {
        $points = new Points(1500);

        $formatter = $this->prophesize(PointsFormatter::class);
        $formatter->format($points)->shouldBeCalled()->willReturn('PTS 1,500');

        $extension = new FormatterExtension();
        $extension->setPointsFormatter($formatter->reveal());

        $actual = $extension->formatPoints($points);

        $this->assertEquals('PTS 1,500', $actual);
    }
}
