<?php

namespace Linio\Frontend\Bundle\FormatterBundle\Twig;

use DateTimeInterface;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\PointsFormatter;
use Linio\Frontend\Entity\Customer\Wallet\Points;
use Linio\Type\Money;
use Twig_Extension;
use Twig_SimpleFilter;

class FormatterExtension extends Twig_Extension
{
    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /**
     * @var DateFormatter
     */
    protected $dateFormatter;

    /**
     * @var PointsFormatter
     */
    protected $pointsFormatter;

    /**
     * @return Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('format_money', [$this, 'formatMoney']),
            new Twig_SimpleFilter('format_percentage', [$this, 'formatPercentage']),
            new Twig_SimpleFilter('format_date', [$this, 'formatDate']),
            new Twig_SimpleFilter('format_long_date', [$this, 'formatLongDate']),
            new Twig_SimpleFilter('format_points', [$this, 'formatPoints']),
        ];
    }

    /**
     * @param Money $value
     *
     * @return string
     */
    public function formatMoney(Money $value)
    {
        return $this->moneyFormatter->format($value);
    }

    /**
     * @param float $value
     *
     * @return string
     */
    public function formatPercentage($value)
    {
        return floor($value) . '%';
    }

    /**
     * @param DateTimeInterface $value
     *
     * @return string
     */
    public function formatDate(DateTimeInterface $value)
    {
        return $this->dateFormatter->format($value);
    }

    /**
     * @param DateTimeInterface $value
     *
     * @return string
     */
    public function formatLongDate(DateTimeInterface $value)
    {
        return $this->dateFormatter->formatLong($value);
    }

    /**
     * @param Points $points
     *
     * @return string
     */
    public function formatPoints(Points $points)
    {
        return $this->pointsFormatter->format($points);
    }

    /**
     * @param MoneyFormatter $moneyFormatter
     */
    public function setMoneyFormatter(MoneyFormatter $moneyFormatter)
    {
        $this->moneyFormatter = $moneyFormatter;
    }

    /**
     * @param DateFormatter $dateFormatter
     */
    public function setDateFormatter(DateFormatter $dateFormatter)
    {
        $this->dateFormatter = $dateFormatter;
    }

    /**
     * @param PointsFormatter $pointsFormatter
     */
    public function setPointsFormatter(PointsFormatter $pointsFormatter)
    {
        $this->pointsFormatter = $pointsFormatter;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'formatter_extension';
    }
}
