<?php

namespace Linio\Frontend\Transformer;

use Linio\Frontend\Entity\Product\Simple;

class ProductDetailFilteredSimpleTransformer
{
    public function transform(Simple $simpleProduct)
    {
        return [
            'sku' => $simpleProduct->getSku(),
            'stock' => $simpleProduct->getStock(),
            'active' => $simpleProduct->isActive(),
            'attributes' => $simpleProduct->getAttributes(),
            'price' => [
                'moneyAmount' => $simpleProduct->getPrice()->getMoneyAmount(),
            ],
            'originalPrice' => [
                'moneyAmount' => $simpleProduct->getOriginalPrice()->getMoneyAmount(),
            ],
            'hasSpecialPrice' => $simpleProduct->hasSpecialPrice(),
            'percentageOff' => $simpleProduct->getPercentageOff(),
            'savedAmount' => [
                'moneyAmount' => $simpleProduct->getSavedAmount()->getMoneyAmount(),
            ],
            'linioPlusLevel' => $simpleProduct->getLinioPlusLevel(),
            'hasFreeShipping' => $simpleProduct->hasFreeShipping(),
        ];
    }
}
