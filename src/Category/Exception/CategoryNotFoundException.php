<?php

declare(strict_types=1);

namespace Linio\Frontend\Category\Exception;

class CategoryNotFoundException extends CategoryException
{
}
