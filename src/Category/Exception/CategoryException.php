<?php

declare(strict_types=1);

namespace Linio\Frontend\Category\Exception;

use Linio\Frontend\Product\Exception\CatalogException;

class CategoryException extends CatalogException
{
}
