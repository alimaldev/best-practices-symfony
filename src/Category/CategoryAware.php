<?php

namespace Linio\Frontend\Category;

trait CategoryAware
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * @codeCoverageIgnore
     *
     * @return CategoryService
     */
    public function getCategoryService()
    {
        return $this->categoryService;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param CategoryService $categoryService
     */
    public function setCategoryService(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
}
