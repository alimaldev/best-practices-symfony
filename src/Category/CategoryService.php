<?php

namespace Linio\Frontend\Category;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Entity\Catalog\Category;

class CategoryService
{
    use CacheAware;

    const ROOT_CATEGORY = 1;

    /**
     * @param int $id
     *
     * @throws CategoryNotFoundException
     *
     * @return Category
     */
    public function getCategory($id)
    {
        $category = $this->cacheService->get($id);

        if (!$category) {
            throw new CategoryNotFoundException(sprintf('Category id=(%s) not found.', $id));
        }

        $mapper = new CategoryMapper();

        return $mapper->map($category);
    }

    /**
     * @param mixed $categoryId
     *
     * @return Category
     */
    public function buildTree($categoryId)
    {
        $category = $this->getCategory($categoryId);
        $firstParent = $this->getFirstParent($category);
        $path = $category->getPath();

        // Remove root and first parent from path. We should find a more elegant way to handle it.
        array_shift($path);

        $lastParent = $firstParent;

        foreach ($path as $parent) {
            $children = $lastParent->getChildren();
            $children[$parent->getId()] = $this->getCategory($parent->getId());
            $lastParent->setChildren($children);

            if ($lastParent->hasChild($category)) {
                $children[$category->getId()]->setIsCurrent(true);
            }

            $lastParent = $children[$parent->getId()];
        }

        foreach ($path as $currentPath) {
            $firstParent->addPath($currentPath);
        }

        return $firstParent;
    }

    /**
     * @param Category $category
     *
     * @return Category
     */
    public function getFirstParent(Category $category)
    {
        $root = $this->getCategory(self::ROOT_CATEGORY);

        foreach ($category->getPath() as $parent) {
            if ($parent->getId() == $root->getId()) {
                continue;
            }

            // Loads from cache in order to get its children.
            return $this->getCategory($parent->getId());
        }

        // This case only happens when the $category is is the root category.
        return $category;
    }
}
