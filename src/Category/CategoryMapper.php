<?php

namespace Linio\Frontend\Category;

use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Catalog\Category;

class CategoryMapper implements MapperInterface
{
    /**
     * @param array $data
     *
     * @return Category|Category[]
     */
    public function map(array $data)
    {
        $categories = [];

        if (isset($data['id'])) {
            $categories = $this->createCategory($data);
        } else {
            foreach ($data as $category) {
                $categories[] = $this->createCategory($category);
            }
        }

        return $categories;
    }

    /**
     * @param array    $data
     * @param Category $parent
     *
     * @return Category
     */
    protected function createCategory(array $data, Category $parent = null)
    {
        $category = new Category();
        $category->setId($data['id']);
        $category->setName($data['name']);
        $category->setSlug($data['slug']);
        $category->setUrlKey($data['url_key']);

        if ($parent) {
            $category->setParent($parent);
        }

        $this->buildPath($category, $data);
        $this->buildChildren($category, $data);

        return $category;
    }

    /**
     * @param Category $category
     * @param array    $data
     */
    protected function buildPath($category, array $data)
    {
        if (!isset($data['path'])) {
            return;
        }

        foreach ($data['path'] as $path) {
            $category->addPath($this->createCategory($path));
        }
    }

    /**
     * @param Category $category
     * @param array    $data
     */
    protected function buildChildren(Category $category, array $data)
    {
        if (!isset($data['children'])) {
            return;
        }

        foreach ($data['children'] as $child) {
            $category->addChild($this->createCategory($child, $category));
        }
    }
}
