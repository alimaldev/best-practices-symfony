<?php

namespace Linio\Frontend\Search\Adapter;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

class SearchClient extends Client
{
    /**
     * SearchClient constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $config['handler'] = $config['handler'] ?? HandlerStack::create();

        parent::__construct($config);
    }
}
