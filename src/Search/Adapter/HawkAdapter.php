<?php

declare(strict_types=1);

namespace Linio\Frontend\Search\Adapter;

use GuzzleHttp\ClientInterface;
use Linio\Component\Util\Json;
use Linio\Frontend\Communication\Country\CountryAware;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Catalog\Facets\CheckBox;
use Linio\Frontend\Entity\Catalog\Facets\MultipleChoice;
use Linio\Frontend\Entity\Catalog\Facets\Range;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Product\ProductHelper;
use Linio\Frontend\Rating\RatingAware;
use Linio\Frontend\Search\ResultSetMapper\AggregationMapper;
use Linio\Frontend\Search\SearchAdapterInterface;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\WishList\ManageWishList;
use Linio\Type\Money;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\HttpFoundation\ParameterBag;

class HawkAdapter implements SearchAdapterInterface
{
    use LoggerAwareTrait;
    use RatingAware;
    use TokenStorageAware;
    use CountryAware;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var ManageWishList
     */
    protected $manageWishList;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $authorizationKey;

    /**
     * @var AggregationMapper
     */
    protected $aggregationMapper;

    /**
     * @var string
     */
    protected $defaultSort = 'relevance';

    /**
     * @var bool
     */
    protected $searchResponseLog = false;

    /**
     * @var string
     */
    protected $imageHost;

    /**
     * The threshold of the number search results Hawk has to find before a search is considered a valid search.
     * According to Business 3 is a sane default.
     *
     * @var int
     */
    protected $didYouMeanThreshold = 3;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param ManageWishList $manageWishList
     */
    public function setManageWishList(ManageWishList $manageWishList)
    {
        $this->manageWishList = $manageWishList;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param string $authorizationKey
     */
    public function setAuthorizationKey(string $authorizationKey)
    {
        $this->authorizationKey = $authorizationKey;
    }

    /**
     * @param AggregationMapper $aggregationMapper
     */
    public function setAggregationMapper(AggregationMapper $aggregationMapper)
    {
        $this->aggregationMapper = $aggregationMapper;
    }

    /**
     * @param string $imageHost
     */
    public function setImageHost(string $imageHost)
    {
        $this->imageHost = $imageHost;
    }

    /**
     * @param int $didYouMeanThreshold
     */
    public function setDidYouMeanThreshold(int $didYouMeanThreshold)
    {
        $this->didYouMeanThreshold = $didYouMeanThreshold;
    }

    /**
     * @param ParameterBag $requestQuery
     * @param Category $categoryTree
     * @param FacetCollection $facetCollection
     * @param bool $didYouMean If false, results must be exact
     * @param array $fields
     *
     * @return SearchResult
     */
    public function searchProducts(
        ParameterBag $requestQuery,
        Category $categoryTree,
        FacetCollection $facetCollection,
        bool $didYouMean = false,
        array $fields = []): SearchResult
    {
        $itemsPerPage = 60;
        $currentPage = (int) $requestQuery->get('page', 1);

        $filters = [];

        $query = [];
        $query['q'] = $requestQuery->get('q');
        $query['size'] = $itemsPerPage;
        $query['page'] = $currentPage;
        $query['gs'] = 10;
        $query['rq'] = 8;
        $query['dum'] = $didYouMean ? 'replace' : 'info';
        $query['dum_count'] = $this->didYouMeanThreshold;
        $query['category_dept'] = $this->getCategoryDepth($categoryTree);
        $query['sort'] = $requestQuery->get('sortBy', $this->defaultSort);
        $query['filters'] = $this->mapActiveFilters($filters, $requestQuery, $facetCollection);
        $query['facets'] = [];
        $query['facet_count'] = 100;
        $query['facet_include_filtered'] = true;

        $query['identifiers'][] = [
            'type' => 'user_id',
            'value' => $this->getCustomer()->getLinioId(),
        ];

        $query['identifiers'][] = [
            'type' => 'web_id',
            'value' => $this->getCustomer()->getLinioId(),
        ];

        if (!empty($fields)) {
            $query['fields'] = $fields;
        }

        if ($requestQuery->has('skus')) {
            $query['top_product_ids'] = $this->parseSkuList($requestQuery->get('skus'));
        }

        if (empty($query['filters'])) {
            unset($query['filters']);
        }

        foreach ($facetCollection as $facet) {
            $query['facets'][] = $facet->getName();
        }

        $url = sprintf('%s%s?key=%s', $this->baseUrl, 'search/', $this->authorizationKey);

        $response = $this->client->request(
            'POST',
            $url,
            [
                'headers' => ['Accept-Encoding' => 'gzip'],
                'json' => $query,
            ]
        );

        $bodyContents = (string) $response->getBody();
        if ($this->searchResponseLog) {
            $this->searchResponseLog = $bodyContents;
        }

        $data = Json::decode($bodyContents);
        $this->mapFilters($facetCollection, $data);

        $searchResult = new SearchResult();
        $searchResult->setTotalItemsFound($data['count']);
        $searchResult->setGuidedSearchTerms(isset($data['guided_search']) ? $data['guided_search'] : []);
        $searchResult->setRelatedQueries(isset($data['related_queries']) ? $data['related_queries'] : []);
        $searchResult->setOriginalQuery($requestQuery->get('q'));
        $searchResult->setDidYouMeanTerm(
            (isset($data['dum']) && isset($data['dum']['sq'])) ? $data['dum']['sq'] : null
        );
        $searchResult->setProducts($this->mapProducts(isset($data['items']) ? $data['items'] : []));
        $searchResult->setCurrentPage((int) $currentPage);
        $searchResult->setPageCount((int) ceil($data['count'] / $itemsPerPage));
        $searchResult->setFacetCollection($facetCollection);

        if (isset($data['facets'])) {
            $parsedAggregations = [];

            if (isset($data['facets']['category'])) {
                $parsedAggregations['category'] = $this->parseCategoryAggregations($data['facets']['category']);
            }
            $aggregations = $this->aggregationMapper->map($parsedAggregations);
            $searchResult->setAggregations($aggregations);
        }

        return $searchResult;
    }

    /**
     * @param string $sku
     *
     * @return SearchResult
     */
    public function findSimilar($sku)
    {
        $query = $this->client->getConfig('query');
        $query['method'] = 'default';
        $query['sku'] = $sku;

        $response = $this->client->get('moreLikeThis', ['query' => $query]);
        $data = Json::decode($response->getBody()->getContents());

        $searchResult = new SearchResult();
        $searchResult->setTotalItemsFound(count($data));
        $searchResult->setProducts($this->mapSimilarProducts($data));

        return $searchResult;
    }

    /**
     * @param Category $categoryTree
     * @param int $depth
     *
     * @return int
     */
    protected function getCategoryDepth($categoryTree, $depth = 1)
    {
        foreach ($categoryTree->getChildren() as $child) {
            if (count($child->getChildren()) > 0) {
                $this->getCategoryDepth(current($categoryTree->getChildren()));

                $depth++;

                break;
            }
        }

        $depth = $depth + 3;

        return $depth;
    }

    /**
     * @param array $data
     *
     * @return Product[]
     */
    public function mapProducts(array $data)
    {
        $products = [];

        foreach ($data as $item) {
            $product = new Product();

            $itemData = isset($item['payload']) ? $item['payload'] : $item;

            $sku = isset($itemData['sku']) ? $itemData['sku'] : '';

            if (!isset($itemData['slug'])) {
                continue;
            }

            $product->setSku($sku);
            $product->setSlug($itemData['slug']);
            $product->setConfigId(isset($itemData['config_id']) ? $itemData['config_id'] : '');
            $product->setDeliveryTime(isset($itemData['delivery_time']) ? $itemData['delivery_time'] : 0);
            $product->setHasFreeShipping(isset($itemData['free_shipping']) ? $itemData['free_shipping'] : false);
            $product->setHasFreeStorePickup((bool) ($item['attributes']['free_pickup'][0] ?? false));
            $product->setLinioPlusLevel(isset($itemData['linio_plus_level']) ? $itemData['linio_plus_level'] : 0);

            $product->setBrand($this->buildBrand($item['brand']));
            // TODO: This should be proper managed by hawk.
            $product->setStock(isset($item['stock_count']) ? $item['stock_count'] : 0);
            $product->setName($item['title']);
            $product->setUnitPrice(new Money($item['price']['current']));
            $product->setImported($item['seller']['international'] ?? false);
            $product->setMasterAndChildrenImported(isset($itemData['are_all_international']) ? $itemData['are_all_international'] : false);

            if (in_array($sku, $this->manageWishList->getAllSkusInWishLists($this->getCustomer()))) {
                $product->addedToWishLists();
            }

            $rating = $this->ratingService->getBySku($sku);

            if ($rating) {
                $product->setRating($rating);
            }

            $seller = new Seller();
            $seller->setSellerId($item['seller']['id'] ?? '');
            $seller->setName($item['seller']['name'] ?? '');
            $seller->setSlug($item['seller']['slug'] ?? '');
            $seller->setType($item['seller']['type'] ?? '');
            $seller->setOperationType($item['seller']['operation_type'] ?? '');
            $seller->setRating($item['seller']['rating'] ?? '');
            $product->setSeller($seller);

            foreach ($item['attributes'] as $key => $values) {
                $product->addAttribute($key, $values[0]);
            }

            if (isset($itemData['images'])) {
                foreach ($itemData['images'] as $imageItem) {
                    $image = new Image();
                    $image->setMain((bool) $imageItem['main']);
                    $image->setPosition($imageItem['position']);
                    $image->setSlug(sprintf('//%s%s', $this->imageHost, $imageItem['slug']));
                    $product->addImage($image);
                }
            }

            $product->setPrice(new Money($item['price']['current']));

            // Has special price
            if (isset($item['price']['previous'])) {
                $specialFromDate = isset($item['price']['special_from_date']) ? new \DateTime($item['price']['special_from_date']) : null;
                $specialToDate = isset($item['price']['special_to_date']) ? new \DateTime($item['price']['special_to_date']) : null;
                $price = ProductHelper::getPrice($item['price']['previous'], $item['price']['current'], $specialFromDate, $specialToDate);

                if ($price == $item['price']['current']) {
                    $product->setOriginalPrice(new Money($item['price']['previous']));
                } else {
                    $product->setPrice(new Money($item['price']['previous']));
                }
            }

            $products[] = $product;
        }

        return $products;
    }

    /**
     * @param array $data
     *
     * @return Product[]
     */
    public function mapSimilarProducts($data)
    {
        $products = [];

        foreach ($data as $item) {
            $product = new Product();
            $product->setSku($item['id']);
            $product->setBrand($this->buildBrand($item['brand']));
            $product->setSlug($item['url']);
            $product->setName($item['title']);
            $product->setUnitPrice(new Money($item['price']));

            $products[] = $product;
        }

        return $products;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function parseCategoryAggregations($data)
    {
        $result = [];

        foreach ($data as $category) {
            $result[] = [
                'key' => $category['id'],
                'count' => $category['count'],
            ];

            if (isset($category['items'])) {
                $result = array_merge($result, $this->parseCategoryAggregations($category['items']));
            }
        }

        return $result;
    }

    /**
     * @param string $skuList
     *
     * @return array
     */
    protected function parseSkuList($skuList)
    {
        $skus = explode(',', $skuList);

        foreach ($skus as &$sku) {
            $sku = sprintf('%s:%s', $this->countryCode, $sku);
        }

        return $skus;
    }

    /**
     * @param $filters
     * @param ParameterBag $requestQuery
     * @param FacetCollection $facetCollection
     *
     * @return array
     */
    public function mapActiveFilters($filters, ParameterBag $requestQuery, FacetCollection $facetCollection)
    {
        if ($requestQuery->has('category')) {
            $filters['category'][] = (string) $requestQuery->get('category');
            $requestQuery->remove('category');
        }

        if ($requestQuery->has('seller.id')) {
            $filters['seller.id'][] = $requestQuery->get('seller.id');
            $requestQuery->remove('seller.id');
        }

        if ($requestQuery->has('campaign_skus')) {
            $filters['sku'] = $requestQuery->get('campaign_skus');
        }

        foreach ($facetCollection->getValues() as $filter) {
            if (!$filter->isActive()) {
                continue;
            }

            /** @var Range $filter */
            if ($filter instanceof Range) {
                if ($filter->getActiveMinimum()) {
                    $filter->getActiveMinimum()->setScale(0);
                    $filters['price_min'][] = (string) $filter->getActiveMinimum()->getMoneyAmount();
                }

                if ($filter->getActiveMaximum()) {
                    $filter->getActiveMaximum()->setScale(0);
                    $filters['price_max'][] = (string) $filter->getActiveMaximum()->getMoneyAmount();
                }
            }

            /** @var CheckBox $filter */
            if ($filter instanceof CheckBox) {
                $filters[$filter->getName()] = [$filter->getActiveValue()];
            }

            /** @var MultipleChoice $filter */
            if ($filter instanceof MultipleChoice) {
                foreach ($filter->getActiveValues() as $value) {
                    $filters[$filter->getName()][] = $value;
                }
            }
        }

        return $filters;
    }

    public function mapFilters(FacetCollection $facetCollection, $data)
    {
        if (!isset($data['facets'])) {
            return;
        }

        $facets = [];

        foreach ($facetCollection as $facet) {
            if (!isset($data['facets'][$facet->getName()])) {
                $facetCollection->removeElement($facet);
                continue;
            }

            $facets[$facet->getName()] = $facet;
        }

        foreach ($data['facets'] as $name => $value) {
            if (isset($facets[$name])) {
                $facet = $facets[$name];
            } else {
                $facet = new MultipleChoice();
                $facet->setName($name);
            }

            switch (true) {
                case $facet instanceof Range:
                    if (isset($value['min'])) {
                        $minimum = new Money($value['min']);
                        $minimum->setScale(0);
                        $facet->setMinimum($minimum);
                    }

                    if (isset($value['max'])) {
                        $maximum = new Money($value['max']);
                        $maximum->setScale(0);
                        $facet->setMaximum($maximum);
                    }
                    break;

                case $facet instanceof CheckBox:
                    foreach ($value as $facetValue) {
                        if (!isset($facetValue['text'])) {
                            $this->logger->warning('Invalid facet data for ' . $name, $value);
                            continue;
                        }

                        $facet->addValue($facetValue['text'], $facetValue['count']);
                    }
                    break;
                case $facet instanceof MultipleChoice:
                    foreach ($value as $facetValue) {
                        if (!isset($facetValue['text'])) {
                            $this->logger->warning('Invalid facet data for ' . $name, $value);
                            continue;
                        }

                        $id = isset($facetValue['id']) ? $facetValue['id'] : $facetValue['text'];
                        $facet->addValue($facetValue['text'], $id, $facetValue['count']);
                    }
                    break;
            }
        }
    }

    public function enableSearchResponseLog()
    {
        $this->searchResponseLog = true;
    }

    /**
     * @return array
     */
    public function getSearchResponseLog()
    {
        return $this->searchResponseLog;
    }

    /**
     * @param array $brandData
     *
     * @return Brand
     */
    protected function buildBrand(array $brandData)
    {
        $brand = new Brand();

        if (isset($brandData['id'])) {
            $brand->setId($brandData['id']);
        }

        if (isset($brandData['name'])) {
            $brand->setName($brandData['name']);
        }

        if (isset($brandData['slug'])) {
            $brand->setSlug($brandData['slug']);
        }

        return $brand;
    }
}
