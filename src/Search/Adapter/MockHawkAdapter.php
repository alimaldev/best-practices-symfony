<?php

namespace Linio\Frontend\Search\Adapter;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Filesystem\Filesystem;

class MockHawkAdapter extends HawkAdapter
{
    /**
     * @var string
     */
    protected $filename;

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function setClient(ClientInterface $client)
    {
        $mockContents = $this->getMockResponseBody();

        $handler = new MockHandler([
            new Response(200, [], $mockContents),
        ]);

        $this->client = new Client(['handler' => $handler]);
    }

    protected function getMockResponseBody()
    {
        if (!(new Filesystem())->exists($this->filename)) {
            throw new Exception(sprintf('Failed to find mock hawk response file: %s', $this->filename));
        }

        return file_get_contents($this->filename);
    }
}
