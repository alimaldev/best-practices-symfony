<?php

declare(strict_types=1);

namespace Linio\Frontend\Search\Exception;

use Linio\Frontend\Product\Exception\CatalogException;

class AggregationNotFoundException extends CatalogException
{
}
