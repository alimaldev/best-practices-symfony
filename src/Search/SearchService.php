<?php

declare(strict_types=1);

namespace Linio\Frontend\Search;

use Linio\Frontend\Entity\Catalog\Aggregation\CategoryAggregation;
use Linio\Frontend\Entity\Catalog\AggregationCollection;
use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Catalog\SearchResults;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\Search\ResultSetMapper\AggregationMapper;
use Symfony\Component\HttpFoundation\ParameterBag;

class SearchService
{
    /**
     * @var SearchAdapterInterface
     */
    protected $adapter;

    /**
     * @var AggregationMapper
     */
    protected $aggregationMapper;

    /**
     * @var FilterQueryMapper
     */
    protected $filterQueryMapper;

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var int
     */
    protected $didYouMeanThreshold = 0;

    /**
     * @param FilterQueryMapper $filterQueryMapper
     */
    public function setFilterQueryMapper(FilterQueryMapper $filterQueryMapper)
    {
        $this->filterQueryMapper = $filterQueryMapper;
    }

    /**
     * @param ProductService $productService
     */
    public function setProductService(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param SearchAdapterInterface $searchAdapter
     */
    public function setAdapter(SearchAdapterInterface $searchAdapter)
    {
        $this->adapter = $searchAdapter;
    }

    /**
     * @param AggregationMapper $aggregationMapper
     */
    public function setAggregationMapper(AggregationMapper $aggregationMapper)
    {
        $this->aggregationMapper = $aggregationMapper;
    }

    /**
     * @param int $didYouMeanThreshold
     */
    public function setDidYouMeanThreshold(int $didYouMeanThreshold)
    {
        $this->didYouMeanThreshold = $didYouMeanThreshold;
    }

    /**
     * @param ParameterBag $query
     * @param Category $categoryTree
     *
     * @return SearchResults
     */
    public function searchProducts(ParameterBag $query, Category $categoryTree): SearchResults
    {
        $facetCollection = $this->filterQueryMapper->map($query->all(), $categoryTree);
        $result = $this->adapter->searchProducts(clone $query, $categoryTree, $facetCollection, false);
        $results = new SearchResults($result);

        if ($result->getDidYouMeanTerm() && $result->count() <= $this->didYouMeanThreshold) {
            $didYouMeanResult = $this->adapter->searchProducts(clone $query, $categoryTree, $facetCollection, true);

            $results->setDidYouMeanSearchResult($didYouMeanResult);
        }

        if (!$categoryTree instanceof Campaign) {
            $this->updateCategoryTree($categoryTree, $results->getPrimarySearchResult()->getAggregations());
        }

        return $results;
    }

    /**
     * @param string $sku
     *
     * @return SearchResult
     */
    public function findSimilar($sku)
    {
        return $this->adapter->findSimilar($sku);
    }

    /**
     * @param Category              $categoryTree
     * @param AggregationCollection $aggregations
     */
    protected function updateCategoryTree($categoryTree, $aggregations)
    {
        /** @var CategoryAggregation $categoryAggregation */
        $categoryAggregation = $aggregations->remove(CategoryAggregation::CODE);

        if ($categoryAggregation) {
            $categoryTree->updateAllCounters($categoryAggregation);
        }
    }

    /**
     * @param ParameterBag $requestQuery
     * @param Campaign $campaign
     *
     * @return SearchResults
     */
    public function getCampaignProducts(ParameterBag $requestQuery, Campaign $campaign): SearchResults
    {
        $searchResults = $this->searchProducts($requestQuery, $campaign);
        $searchResult = $searchResults->getOriginalSearchResult();

        $products = $searchResult->getProducts();

        /** @var Product $product */
        foreach ($products as $key => $product) {
            if (array_key_exists($product->getSku(), $campaign->getFilters())) {
                try {
                    $cacheProduct = $this->productService->getBySku($product->getSku());
                } catch (ProductNotFoundException $exception) {
                    continue;
                }

                $cacheProduct->filterCampaignProducts($campaign);

                $product->setPrice($cacheProduct->getFirstSimple()->getPrice());
                $product->setOriginalPrice($cacheProduct->getFirstSimple()->getOriginalPrice());
                $product->setLinioPlusLevel($cacheProduct->getFirstSimple()->getLinioPlusLevel());
                $product->setHasFreeShipping($cacheProduct->getFirstSimple()->hasFreeShipping());
                $product->setCampaignSlug($campaign->getSlug());
            }
        }

        $searchResult->setProducts($products);

        return $searchResults;
    }
}
