<?php

namespace Linio\Frontend\Search\ResultSetMapper;

use Doctrine\Common\Inflector\Inflector;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Catalog\Aggregation;
use Linio\Frontend\Entity\Catalog\Aggregation\Item;
use Linio\Frontend\Entity\Catalog\AggregationCollection;
use Linio\Frontend\Search\Exception\AggregationNotFoundException;

class AggregationMapper implements MapperInterface
{
    /**
     * @var CacheService[]
     */
    protected $cacheServices = [];

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     *
     * @param array $data
     *
     * @return AggregationCollection
     */
    public function map(array $data)
    {
        $aggregations = new AggregationCollection();

        foreach ($data as $aggregationName => $items) {
            try {
                $aggregation = $this->createAggregation($aggregationName);
            } catch (AggregationNotFoundException $e) {
                continue;
            }
            $this->createItems($aggregation, $items);
            $aggregations->set($aggregation->getCode(), $aggregation);
        }

        return $aggregations;
    }

    /**
     * @param Aggregation $aggregation
     * @param array       $items
     */
    protected function createItems($aggregation, array $items)
    {
        if ($this->hasCache($aggregation) && $items) {
            $cacheData = $this->getCacheData($aggregation, $items);
        } else {
            $cacheData = [];
        }

        foreach ($items as $item) {
            $aggregation->addItem($this->createItem($item, $cacheData));
        }
    }

    /**
     * @param Aggregation $aggregation
     *
     * @return bool
     */
    protected function hasCache(Aggregation $aggregation)
    {
        return $aggregation->getCacheKey() && isset($this->cacheServices[$aggregation->getCode()]);
    }

    /**
     * @param Aggregation $aggregation
     * @param Item[]      $items
     *
     * @return array
     */
    protected function getCacheData($aggregation, $items)
    {
        $result = [];
        $cacheKeys = [];

        foreach ($items as $item) {
            $cacheKeys[] = $aggregation->getCacheKey() . ':' . $item['key'];
        }

        $data = $this->cacheServices[$aggregation->getCode()]->getMulti($cacheKeys);

        foreach ($data as $key => $value) {
            $result[$this->getCacheId($key)] = $value[$aggregation->getCacheField()];
        }

        return $result;
    }

    /**
     * @param string $cacheKey
     *
     * @return mixed
     */
    protected function getCacheId($cacheKey)
    {
        return (int) substr(strrchr($cacheKey, ':'), 1);
    }

    /**
     * @param string $name
     *
     * @throws AggregationNotFoundException
     *
     * @return Aggregation
     */
    protected function createAggregation($name)
    {
        $className = sprintf('Linio\\Frontend\\Entity\\Catalog\\Aggregation\\%sAggregation', Inflector::classify($name));

        if (!class_exists($className)) {
            throw new AggregationNotFoundException($name);
        }

        return new $className();
    }

    /**
     * @param array $data
     * @param array $cache
     *
     * @return Item
     */
    protected function createItem(array $data, array $cache = [])
    {
        if (isset($cache[$data['key']])) {
            $name = $cache[$data['key']];
        } else {
            $name = $data['key'];
        }

        $item = new Item();
        $item->setId($data['key']);
        $item->setName($name);
        $item->setCount($data['count']);

        return $item;
    }

    /**
     * @return CacheService[]
     */
    public function getCacheServices()
    {
        return $this->cacheServices;
    }

    /**
     * @param string       $aggregationCode
     * @param CacheService $cacheService
     */
    public function addCacheService($aggregationCode, CacheService $cacheService)
    {
        $this->cacheServices[$aggregationCode] = $cacheService;
    }
}
