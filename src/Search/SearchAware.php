<?php

namespace Linio\Frontend\Search;

trait SearchAware
{
    /**
     * @var SearchService
     */
    protected $searchService;

    /**
     * @return SearchService
     */
    public function getSearchService()
    {
        return $this->searchService;
    }

    /**
     * @param SearchService $searchService
     */
    public function setSearchService(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }
}
