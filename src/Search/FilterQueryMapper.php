<?php

namespace Linio\Frontend\Search;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Catalog\Facets\CheckBox;
use Linio\Frontend\Entity\Catalog\Facets\MultipleChoice;
use Linio\Frontend\Entity\Catalog\Facets\Range;
use Linio\Type\Money;

class FilterQueryMapper
{
    /**
     * @var array
     */
    protected static $checkboxFilters = ['linio_plus_filter'];
    /**
     * @var array
     */
    protected static $rangeFilters = ['price'];

    /**
     * @var array
     */
    protected static $defaultFilters = ['brand', 'price', 'is_international'];

    /**
     * @var CacheService
     */
    protected $filterCacheService;

    /**
     * @var CacheService
     */
    protected $attributeCacheService;

    /**
     * @var bool
     */
    protected $linioPlusEnabled;

    /**
     * @param CacheService $filterCacheService
     */
    public function setFilterCacheService(CacheService $filterCacheService)
    {
        $this->filterCacheService = $filterCacheService;
    }

    /**
     * @param CacheService $attributeCacheService
     */
    public function setAttributeCacheService(CacheService $attributeCacheService)
    {
        $this->attributeCacheService = $attributeCacheService;
    }

    /**
     * @param bool $linioPlusEnabled
     */
    public function setLinioPlusEnabled($linioPlusEnabled)
    {
        $this->linioPlusEnabled = $linioPlusEnabled;
    }

    /**
     * @param array $data
     * @param Category $categoryTree
     *
     * @return mixed
     */
    public function map(array $data, Category $categoryTree)
    {
        $categoryFilters = $this->getCategoryFilters($categoryTree);
        $availableFilters = array_unique(array_merge(static::$defaultFilters, $categoryFilters));
        $facetCollection = new FacetCollection();

        if ($this->linioPlusEnabled === true) {
            $availableFilters[] = 'linio_plus_filter';
        }

        foreach ($availableFilters as $filterName) {
            if (in_array($filterName, static::$checkboxFilters)) {
                $filter = $this->createCheckBoxFilter($filterName, $data);
            } elseif (in_array($filterName, static::$rangeFilters)) {
                $filter = $this->createRangeFilter($filterName, $data);
            } else {
                $filter = $this->createMultipleChoiceFilter($filterName, $data);
            }

            $facetCollection->add($filter);
        }

        return $facetCollection;
    }

    /**
     * @param string $filterName
     * @param array $data
     *
     * @return CheckBox
     */
    protected function createCheckBoxFilter($filterName, array $data)
    {
        $filter = new CheckBox();
        $filter->setName($filterName);
        $filter->setLabel($this->getAttributeLabel($filterName));

        if (isset($data[$filterName])) {
            $filter->setActiveValue($data[$filterName]);
        }

        return $filter;
    }

    /**
     * @param string $filterName
     * @param array $data
     *
     * @return Range
     */
    protected function createRangeFilter($filterName, array $data)
    {
        $filter = new Range();
        $filter->setName($filterName);
        $filter->setLabel($this->getAttributeLabel($filterName));
        $minimum = new Money(100);
        $maximum = new Money(5000);
        $minimum->setScale(0);
        $maximum->setScale(0);
        $filter->setMinimum($minimum);
        $filter->setMaximum($maximum);

        if (isset($data[$filterName]) && strpos($data[$filterName], '-') > 0) {
            list($activeMinimum, $activeMaximum) = explode('-', $data[$filterName]);
            $activeMinimum = new Money($activeMinimum);
            $activeMaximum = new Money($activeMaximum);
            $activeMinimum->setScale(0);
            $activeMaximum->setScale(0);
            $filter->setActiveMinimum($activeMinimum);
            $filter->setActiveMaximum($activeMaximum);
        }

        return $filter;
    }

    /**
     * @param string $filterName
     * @param array $data
     *
     * @return MultipleChoice
     */
    protected function createMultipleChoiceFilter($filterName, array $data)
    {
        $filter = new MultipleChoice();
        $filter->setName($filterName);
        $filter->setLabel($this->getAttributeLabel($filterName));

        if (isset($data[$filterName])) {
            if (is_array($data[$filterName])) {
                foreach ($data[$filterName] as $value) {
                    $filter->addActiveValue($value);
                }
            } else {
                $filter->addActiveValue($data[$filterName]);
            }
        }

        return $filter;
    }

    /**
     * @param string $key
     *
     * @return string
     */
    protected function getAttributeLabel($key)
    {
        $attributes = $this->attributeCacheService->get('attributes');

        return isset($attributes[$key]) ? $attributes[$key] : $key;
    }

    /**
     * @param Category $categoryTree
     *
     * @return array
     */
    public function getCategoryFilters(Category $categoryTree)
    {
        $categories = $categoryTree->getPath();
        $lastCategory = end($categories);

        if ($lastCategory instanceof Category) {
            $filters = $this->filterCacheService->get($lastCategory->getId());

            if (is_array($filters)) {
                return $filters;
            }
        }

        return [];
    }
}
