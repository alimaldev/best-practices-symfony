<?php

declare(strict_types=1);

namespace Linio\Frontend\Search;

use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Symfony\Component\HttpFoundation\ParameterBag;

interface SearchAdapterInterface
{
    /**
     * @param ParameterBag $requestQuery
     * @param Category $categoryTree
     * @param FacetCollection $facetCollection
     * @param bool $didYouMean If false, results must be exact
     * @param array $fields
     *
     * @return SearchResult
     */
    public function searchProducts(
        ParameterBag $requestQuery,
        Category $categoryTree,
        FacetCollection $facetCollection,
        bool $didYouMean = false,
        array $fields = []): SearchResult;

    /**
     * @param string $sku
     *
     * @return SearchResult
     */
    public function findSimilar($sku);
}
