<?php

namespace Linio\Frontend\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler;

class LogoutSuccessHandler extends DefaultLogoutSuccessHandler
{
    /**
     * {@inheritdoc}
     */
    public function onLogoutSuccess(Request $request)
    {
        $request->cookies->remove('linio_id');

        return parent::onLogoutSuccess($request);
    }
}
