<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\Jwt;

use Linio\Frontend\Security\Jwt\Exception\JwtExtractionFailedException;
use Linio\Frontend\Security\User;
use Namshi\JOSE\SimpleJWS;
use Symfony\Component\HttpFoundation\Request;

class TokenManager
{
    /**
     * @var TokenStorage
     */
    protected $jwtStorage;

    /**
     * @var TokenExtractor
     */
    protected $tokenExtractor;

    /**
     * @param TokenExtractor $tokenExtractor
     */
    public function setTokenExtractor(TokenExtractor $tokenExtractor)
    {
        $this->tokenExtractor = $tokenExtractor;
    }

    /**
     * @param TokenStorage $jwtStorage
     */
    public function __construct(TokenStorage $jwtStorage)
    {
        $this->jwtStorage = $jwtStorage;
    }

    /**
     * @param string $token
     * @param User $user
     */
    public function add(string $token, User $user)
    {
        $this->jwtStorage->add($token, $user);
    }

    /**
     * @param User $user
     *
     * @return string[]
     */
    public function getTokens(User $user): array
    {
        return $this->jwtStorage->getTokens($user);
    }

    /**
     * @param string $token
     */
    public function remove(string $token)
    {
        $this->jwtStorage->remove($token, $this->buildUserFromToken($token));
    }

    /**
     * @param User $user
     */
    public function flushTokens(User $user)
    {
        $this->jwtStorage->flushTokens($user);
    }

    /**
     * @param Request $request
     */
    public function removeTokenExtractedFromRequest(Request $request)
    {
        $token = $this->tokenExtractor->extract($request);

        $this->jwtStorage->remove($token, $this->buildUserFromToken($token));
    }

    /**
     * @param Request $request
     *
     * @throws JwtExtractionFailedException
     *
     * @return string
     */
    public function getTokenFromRequest(Request $request): string
    {
        return $this->tokenExtractor->extract($request);
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public function isPayloadValid(string $token): bool
    {
        $payload = SimpleJWS::load($token)->getPayload();

        return isset($payload['user_id']);
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public function tokenExists(string $token): bool
    {
        $user = $this->buildUserFromToken($token);

        return $this->jwtStorage->tokenExists($token, $user);
    }

    /**
     * @param string $token
     *
     * @return User
     */
    protected function buildUserFromToken(string $token): User
    {
        $payload = SimpleJWS::load($token)->getPayload();

        $user = new User();
        $user->setId($payload['user_id']);

        return $user;
    }
}
