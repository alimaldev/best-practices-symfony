<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\Jwt;

use Linio\Frontend\Exception\DomainException;

class StoreTokenException extends DomainException
{
}
