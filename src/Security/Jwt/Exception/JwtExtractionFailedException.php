<?php

namespace Linio\Frontend\Security\Jwt\Exception;

use Linio\Frontend\Exception\InputException;

class JwtExtractionFailedException extends InputException
{
}
