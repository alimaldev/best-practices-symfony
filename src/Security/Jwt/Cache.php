<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\Jwt;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Security\User;

class Cache implements TokenStorage
{
    /**
     * @var CacheService
     */
    protected $cacheService;

    /**
     * @param CacheService $cacheService
     */
    public function __construct(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $token, User $user)
    {
        $currentUserTokens = $this->getTokens($user);

        $this->saveTokens(array_unique(array_merge($currentUserTokens, [md5($token)])), $user);
    }

    /**
     * {@inheritdoc}
     */
    public function getTokens(User $user): array
    {
        return $this->cacheService->get((string) $user->getId()) ?? [];
    }

    /**
     * {@inheritdoc}
     */
    public function tokenExists(string $token, User $user): bool
    {
        return in_array(md5($token), $this->getTokens($user), true);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(string $token, User $user)
    {
        $currentUserTokens = $this->getTokens($user);

        $this->saveTokens(array_diff($currentUserTokens, [md5($token)]), $user);
    }

    /**
     * {@inheritdoc}
     */
    public function flushTokens(User $user)
    {
        $this->saveTokens([], $user);
    }

    /**
     * @param array $tokens
     * @param User $user
     *
     * @throws StoreTokenException
     */
    protected function saveTokens(array $tokens, User $user)
    {
        if (!$this->cacheService->set((string) $user->getId(), $tokens)) {
            throw new StoreTokenException(ExceptionMessage::TOKEN_NOT_UPDATED);
        }
    }
}
