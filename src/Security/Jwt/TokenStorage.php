<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\Jwt;

use Linio\Frontend\Security\User;

interface TokenStorage
{
    /**
     * @param string $token
     * @param User $user
     *
     * @throws StoreTokenException
     */
    public function add(string $token, User $user);

    /**
     * @param User $user
     *
     * @return string[]
     */
    public function getTokens(User $user): array;

    /**
     * @param string $token
     * @param User $user
     *
     * @return bool
     */
    public function tokenExists(string $token, User $user): bool;

    /**
     * @param string $token
     * @param User $user
     *
     * @throws StoreTokenException
     */
    public function remove(string $token, User $user);

    /**
     * @param User $user
     */
    public function flushTokens(User $user);
}
