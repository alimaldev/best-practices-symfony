<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\Jwt;

use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Security\Jwt\Exception\JwtExtractionFailedException;
use Symfony\Component\HttpFoundation\Request;

class TokenExtractor
{
    /**
     * @var string
     */
    protected $jwtAuthorizationName;

    /**
     * @param string $jwtAuthorizationName
     */
    public function __construct(string $jwtAuthorizationName)
    {
        $this->jwtAuthorizationName = $jwtAuthorizationName;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function extract(Request $request): string
    {
        $authorizationHeader = explode(' ', $request->headers->get($this->jwtAuthorizationName, ''));

        if (count($authorizationHeader) !== 2) {
            throw new JwtExtractionFailedException(ExceptionMessage::JWT_NOT_FOUND);
        }

        return $authorizationHeader[1];
    }
}
