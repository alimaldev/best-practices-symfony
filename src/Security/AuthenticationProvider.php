<?php

namespace Linio\Frontend\Security;

use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Entity\Customer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;

class AuthenticationProvider implements SimpleFormAuthenticatorInterface
{
    use CustomerAware;

    /**
     * {@inheritdoc}
     *
     * @throws BadCredentialsException
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        try {
            $customer = $this->customerService->login($token);
        } catch (InvalidLoginCredentialsException $e) {
            throw new BadCredentialsException('Invalid username or password.');
        }

        $user = $this->castCustomerToUser($customer);

        $token->setUser($user);
        $token->setAuthenticated(true);

        return $token;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof AuthenticationToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * {@inheritdoc}
     */
    public function createToken(Request $request, $username, $password, $providerKey)
    {
        return new AuthenticationToken($username, $password, $providerKey, ['ROLE_USER']);
    }

    /**
     * Cast a Customer instance to a User instance. Hackish way...
     *
     * @param Customer $customer
     *
     * @return User
     */
    protected function castCustomerToUser(Customer $customer)
    {
        $className = User::class;

        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($customer), '"'), ':')
        ));
    }
}
