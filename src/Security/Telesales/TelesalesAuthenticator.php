<?php

namespace Linio\Frontend\Security\Telesales;

use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Security\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;

class TelesalesAuthenticator implements SimpleFormAuthenticatorInterface
{
    use CustomerAware;

    /**
     * @param TokenInterface $token
     * @param UserProviderInterface $userProvider
     * @param string $providerKey
     *
     * @throws BadCredentialsException
     *
     * @return TokenInterface
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        try {
            $customer = $this->customerService->loginTelesales($token);
        } catch (InvalidLoginCredentialsException $e) {
            throw new BadCredentialsException('Invalid username or password.');
        }

        $user = $this->castCustomerToUser($customer);

        $token->setUser($user);
        $token->setAuthenticated(true);

        return $token;
    }

    /**
     * @param TokenInterface $token
     * @param string $providerKey
     *
     * @return bool
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof TelesalesAuthenticationToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param Request $request
     * @param string $username
     * @param string $password
     * @param string $providerKey
     *
     * @return TelesalesAuthenticationToken
     */
    public function createToken(Request $request, $username, $password, $providerKey)
    {
        return new TelesalesAuthenticationToken(
            $username,
            $request->request->get('bobUsername'),
            $password,
            $providerKey,
            ['ROLE_USER', 'ROLE_TELESALES_AGENT']
        );
    }

    /**
     * Cast a Customer instance to a User instance. Hackish way...
     *
     * @param Customer $customer
     *
     * @return User
     */
    protected function castCustomerToUser(Customer $customer)
    {
        $className = User::class;

        return unserialize(
            sprintf(
                'O:%d:"%s"%s',
                strlen($className),
                $className,
                strstr(strstr(serialize($customer), '"'), ':')
            )
        );
    }
}
