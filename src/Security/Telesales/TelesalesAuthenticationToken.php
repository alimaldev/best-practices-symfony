<?php

namespace Linio\Frontend\Security\Telesales;

use Linio\Frontend\Security\AuthenticationToken;

class TelesalesAuthenticationToken extends AuthenticationToken
{
    /**
     * @var string
     */
    protected $bobUsername;

    /**
     * {@inheritdoc}
     */
    public function __construct($user, $bobUsername, $credentials, $providerKey, $roles = [])
    {
        $this->bobUsername = $bobUsername;

        parent::__construct($user, $credentials, $providerKey, $roles);
    }

    /**
     * @return string
     */
    public function getBobUsername()
    {
        return $this->bobUsername;
    }
}
