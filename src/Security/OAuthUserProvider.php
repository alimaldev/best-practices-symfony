<?php

declare(strict_types=1);

namespace Linio\Frontend\Security;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Security\OAuth\OauthToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

class OAuthUserProvider extends AuthenticationProvider implements OAuthAwareUserProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response): UserInterface
    {
        try {
            $customer = $this->customerService->loginOAuth(
                $this->createTokenFromOAuthUserResponse($response)
            );
        } catch (InvalidLoginCredentialsException $exception) {
            throw new UsernameNotFoundException($exception->getMessage());
        }

        return $this->castCustomerToUser($customer);
    }

    /**
     * @param UserResponseInterface $response
     *
     * @return TokenInterface
     */
    protected function createTokenFromOAuthUserResponse(UserResponseInterface $response): TokenInterface
    {
        return new OauthToken(
            $response->getEmail(),
            $response->getResourceOwner()->getName(),
            $response->getUsername(),
            $response->getFirstName(),
            $response->getLastName()
        );
    }
}
