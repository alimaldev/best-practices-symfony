<?php

namespace Linio\Frontend\Security;

use Linio\Frontend\Order\BuildOrder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

class LoginSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }

    /**
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->options['use_referer'] = true;

        if ($request->cookies->has('linio_id')) {
            $this->buildOrder->mergeItems($request->cookies->get('linio_id'), $token->getUser());
        }

        if ($request->headers->get('referer') == $this->router->generate('frontend.cart.index', [], true)) {
            $request->getSession()->set('_security.default.target_path', $this->router->generate('frontend.checkout.step1'));
        }

        return parent::onAuthenticationSuccess($request, $token);
    }
}
