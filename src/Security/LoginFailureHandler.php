<?php

namespace Linio\Frontend\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;

class LoginFailureHandler extends DefaultAuthenticationFailureHandler
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param RouterInterface $router
     */
    public function setRouter(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $targetPath = $request->headers->get('referer');

        if ($targetPath === $this->router->generate('frontend.security.login', [], true)) {
            $targetPath = $this->router->generate('frontend.default.index');
        }

        $request->getSession()->set('_security.default.target_path', $targetPath);

        return parent::onAuthenticationFailure($request, $exception);
    }
}
