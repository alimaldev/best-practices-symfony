<?php

declare(strict_types=1);

namespace Linio\Frontend\Security;

use Linio\Frontend\Entity\Customer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @codeCoverageIgnore
 */
trait TokenStorageAware
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return TokenStorageInterface
     */
    public function getTokenStorage(): TokenStorageInterface
    {
        return $this->tokenStorage;
    }

    /**
     * Returns GuestCustomer if the customer is not logged in or a session token has not been set.
     *
     * @param string $linioId
     * @param string $clientIp
     *
     * @return Customer
     */
    public function getCustomer(string $linioId = null, string $clientIp = null): Customer
    {
        if (!$this->tokenStorage->getToken()) {
            return new GuestUser($linioId, $clientIp);
        }

        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @param UserInterface $user
     */
    public function updateCustomer(UserInterface $user)
    {
        $this->tokenStorage->getToken()->setUser($user);
        $this->tokenStorage->setToken($this->tokenStorage->getToken());
    }
}
