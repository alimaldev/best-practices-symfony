<?php

declare(strict_types=1);

namespace Linio\Frontend\Security;

use DateTime;
use Linio\Frontend\Entity\Customer;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class GuestUser extends Customer implements AdvancedUserInterface
{
    /**
     * @param string $linioId
     * @param string $clientIp
     */
    public function __construct(string $linioId = null, string $clientIp = null)
    {
        $this->linioId = $linioId ?? uniqid('_', true);

        // sane defaults
        $this->firstName = 'anon.';
        $this->email = 'anon';
        $this->ipAddress = $clientIp;
        $this->subscribedToLinioPlus = false;
        $this->wasSubscribedToLinioPlus = false;
        $this->subscribedToNewsletter = false;
        $this->subscribedToSmsNewsletter = false;
        $this->createdAt = $this->updatedAt = new DateTime();
        $this->hasWallet = false;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired(): bool
    {
        // Not a user account that is expired
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked(): bool
    {
        // This is not a user some one can login with
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired(): bool
    {
        // This account has no credentials therefore they can't expire
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles(): array
    {
        return ['IS_AUTHENTICATED_ANONYMOUSLY'];
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->setPassword(null);
    }
}
