<?php

declare(strict_types=1);

namespace Linio\Frontend\Security;

use Linio\Frontend\Security\Jwt\TokenManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class MobileLogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @param TokenManager $tokenManager
     */
    public function setTokenManager(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * {@inheritdoc}
     */
    public function onLogoutSuccess(Request $request)
    {
        $this->tokenManager->removeTokenExtractedFromRequest($request);

        return new JsonResponse();
    }
}
