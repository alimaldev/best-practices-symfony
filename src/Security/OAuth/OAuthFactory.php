<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\OAuth;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

class OAuthFactory extends AbstractFactory
{
    /**
     * {@inheritdoc}
     */
    public function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId): string
    {
        $providerId = 'security.authentication.provider.' . $id;

        $container
            ->setDefinition($providerId, new DefinitionDecorator('security.authentication.oauth.provider'))
            ->addArgument(new Reference($userProviderId))
        ;

        return $providerId;
    }

    /**
     * {@inheritdoc}
     */
    protected function getListenerId(): string
    {
        return 'security.authentication.oauth.listener';
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition(): string
    {
        return 'pre_auth';
    }

    /**
     * {@inheritdoc}
     */
    public function getKey(): string
    {
        return 'api_oauth';
    }

    /**
     * {@inheritdoc}
     */
    public function addConfiguration(NodeDefinition $builder)
    {
        parent::addConfiguration($builder);
    }
}
