<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\OAuth;

use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Security\User;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class OAuthProvider implements AuthenticationProviderInterface
{
    use CustomerAware;

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token): bool
    {
        return $token instanceof OauthToken;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(TokenInterface $token): TokenInterface
    {
        try {
            $customer = $this->customerService->loginOAuth($token);
        } catch (InvalidLoginCredentialsException $error) {
            throw new AuthenticationException($error->getMessage());
        }

        $token->setUser($this->castCustomerToUser($customer));

        return $token;
    }

    /**
     * Copy of AuthenticationProvider implementation.
     *
     * @param Customer $customer
     *
     * @return User
     */
    protected function castCustomerToUser(Customer $customer): User
    {
        $className = User::class;

        return unserialize(
            sprintf(
                'O:%d:"%s"%s',
                strlen($className),
                $className,
                strstr(strstr(serialize($customer), '"'), ':')
            )
        );
    }
}
