<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\OAuth;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class OauthToken extends AbstractToken
{
    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $provider;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @param string $email
     * @param string $provider
     * @param string $username
     * @param string $firstName
     * @param string $lastName
     */
    public function __construct(string $email, string $provider, string $username, string $firstName, string $lastName)
    {
        $this->email = $email;
        $this->provider = $provider;
        $this->username = $username;
        $this->firstName = $firstName;
        $this->lastName = $lastName;

        parent::__construct([]);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(): string
    {
        return $this->email;
    }
}
