<?php

declare(strict_types=1);

namespace Linio\Frontend\Security\OAuth;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;

class OAuthListener extends AbstractAuthenticationListener
{
    /**
     * {@inheritdoc}
     */
    protected function requiresAuthentication(Request $request): bool
    {
        if (!$request->isMethod('POST')) {
            return false;
        }

        return parent::requiresAuthentication($request);
    }

    /**
     * {@inheritdoc}
     */
    protected function attemptAuthentication(Request $request): TokenInterface
    {
        $token = $this->createTokenFromRequest($request);

        return $this->authenticationManager->authenticate($token);
    }

    /**
     * @param Request $request
     *
     * @return OauthToken
     */
    protected function createTokenFromRequest(Request $request): OauthToken
    {
        return new OauthToken(
            $request->request->get('email'),
            $request->request->get('provider'),
            $request->request->get('username'),
            $request->request->get('firstName'),
            $request->request->get('lastName')
        );
    }
}
