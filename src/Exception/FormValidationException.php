<?php

declare(strict_types=1);

namespace Linio\Frontend\Exception;

use Symfony\Component\Form\FormInterface;

class FormValidationException extends InputException
{
    /**
     * @param FormInterface $form
     * @param string|null $message
     */
    public function __construct(FormInterface $form, string $message = null)
    {
        $contexts = [];
        $message = $message ?? 'INVALID_REQUEST';

        foreach ($form->getErrors(true, true) as $error) {
            $contexts[$error->getOrigin()->getName()][] = $error->getMessage();
        }

        parent::__construct($message, $contexts);
    }
}
