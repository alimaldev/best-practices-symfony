<?php

declare(strict_types=1);

namespace Linio\Frontend\Exception;

class DomainException extends \DomainException
{
    /**
     * @var array
     */
    protected $context = [];

    public function __construct(string $message, array $context = [], int $code = 0, \Throwable $previous = null)
    {
        $this->context = $context;
        parent::__construct($message, $code, $previous);
    }

    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @param array $context
     */
    public function setContext(array $context)
    {
        $this->context = $context;
    }

    /**
     * @param array $additionalContext
     */
    public function addContext(array $additionalContext)
    {
        $this->context = array_merge($this->context, $additionalContext);
    }
}
