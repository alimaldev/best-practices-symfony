<?php

declare(strict_types=1);

namespace Linio\Frontend\Exception;

class InputException extends DomainException
{
}
