<?php

namespace Linio\Frontend\Location;

trait ResolveLocationAware
{
    /**
     * @var ResolveLocation
     */
    protected $resolveLocation;

    /**
     * @return ResolveLocation
     */
    public function getResolveLocation()
    {
        return $this->resolveLocation;
    }

    /**
     * @param ResolveLocation $resolveLocation
     */
    public function setResolveLocation($resolveLocation)
    {
        $this->resolveLocation = $resolveLocation;
    }
}
