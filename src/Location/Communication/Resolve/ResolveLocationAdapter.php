<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Communication\Resolve;

use Linio\Frontend\Entity\Address\City;
use Linio\Frontend\Entity\Address\Country;
use Linio\Frontend\Entity\Address\Municipality;
use Linio\Frontend\Entity\Address\Region;
use Symfony\Component\HttpFoundation\ParameterBag;

interface ResolveLocationAdapter
{
    /**
     * Resolves a postcode into a full address. The result must be an array containing:
     * - region
     * - municipality
     * - city
     * - neighborhoods<array>.
     *
     * @param string $postcode
     *
     * @return array
     */
    public function resolvePostcode(string $postcode): array;

    /**
     * @return Country[]
     */
    public function getCountries(): array;

    /**
     * @param ParameterBag $data
     *
     * @return Region[]
     */
    public function getRegions(ParameterBag $data): array;

    /**
     * @param ParameterBag $data
     *
     * @return Municipality[]
     */
    public function getMunicipalities(ParameterBag $data): array;

    /**
     * @param ParameterBag $data
     *
     * @return City[]
     */
    public function getCities(ParameterBag $data): array;

    /**
     * @param ParameterBag $data
     *
     * @return City[]
     */
    public function getNeighborhoods(ParameterBag $data): array;
}
