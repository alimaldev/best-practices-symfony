<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Communication\Resolve;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\InputException;

class Bob4Alice extends Legacy
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $postcode
     *
     * @throws InputException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return array
     */
    public function resolvePostcode(string $postcode): array
    {
        try {
            $response = $this->client->request('POST', '/addressing/postcode/resolve', ['json' => ['postcode' => $postcode]]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InputException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return [
            'region' => $responseBody['region'],
            'municipality' => $responseBody['municipality'],
            'city' => $responseBody['city'],
            'neighborhoods' => [],
        ];
    }
}
