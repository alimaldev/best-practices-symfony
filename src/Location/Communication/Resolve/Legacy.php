<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Communication\Resolve;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Address\City;
use Linio\Frontend\Entity\Address\Country;
use Linio\Frontend\Entity\Address\Municipality;
use Linio\Frontend\Entity\Address\Region;
use Symfony\Component\HttpFoundation\ParameterBag;

class Legacy implements ResolveLocationAdapter
{
    use CacheAware;

    const CACHE_KEY = 'legacy';

    /**
     * @var array
     */
    protected $data;

    /**
     * {@inheritdoc}
     */
    public function resolvePostcode(string $postcode): array
    {
        $data = $this->loadData();
        $foundRow = null;

        foreach ($data as $row) {
            if ($row['postcode'] == $postcode) {
                $foundRow = $row;

                break;
            }
        }

        if ($foundRow === null) {
            return [];
        }

        return [
            'region' => $foundRow['region'],
            'municipality' => $foundRow['municipality'],
            'city' => $foundRow['city'],
            'neighborhoods' => [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getCountries(): array
    {
        $data = $this->loadData();
        $countries = [];

        foreach ($data as $row) {
            $country = new Country();
            $country->setId($row['country']);
            $country->setName($row['country']);

            $countries[] = $country;
        }

        $countries = array_values(array_unique($countries));

        return $countries;
    }

    /**
     * {@inheritdoc}
     */
    public function getRegions(ParameterBag $data): array
    {
        $places = $this->loadData();

        if ($data !== null) {
            $countryId = $data->get('country');
        }
        $regions = [];

        foreach ($places as $row) {
            if (isset($countryId) && isset($row['country']) && $row['country'] != $countryId) {
                continue;
            }

            $region = new Region();
            $region->setId(trim($row['region']));
            $region->setName(trim($row['region']));

            $regions[] = $region;
        }

        $regions = array_values(array_unique($regions));

        return $regions;
    }

    /**
     * {@inheritdoc}
     */
    public function getMunicipalities(ParameterBag $data): array
    {
        $regionId = $data->get('region');
        $places = $this->loadData();
        $municipalities = [];

        foreach ($places as $row) {
            if ($row['region'] != $regionId) {
                continue;
            }

            $municipality = new Municipality();
            $municipality->setId(trim($row['municipality']));
            $municipality->setName(trim($row['municipality']));

            $municipalities[] = $municipality;
        }

        $municipalities = array_values(array_unique($municipalities));

        return $municipalities;
    }

    /**
     * {@inheritdoc}
     */
    public function getCities(ParameterBag $data): array
    {
        $municipalityId = $data->get('municipality');
        $places = $this->loadData();
        $cities = [];

        foreach ($places as $row) {
            if ($row['municipality'] != $municipalityId) {
                continue;
            }

            $name = $row['city'] ?? '';
            $name = trim($name);

            $city = new City();
            $city->setId($name);
            $city->setName($name);

            $cities[] = $city;
        }

        $cities = array_values(array_unique($cities));

        return $cities;
    }

    public function getNeighborhoods(ParameterBag $data): array
    {
        return [];
    }

    /**
     * @return array
     */
    protected function loadData(): array
    {
        if ($this->data !== null) {
            return $this->data;
        }

        $this->data = $this->cacheService->get(static::CACHE_KEY);

        return $this->data;
    }
}
