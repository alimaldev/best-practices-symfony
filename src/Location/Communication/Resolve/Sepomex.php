<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Communication\Resolve;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Address\City;
use Linio\Frontend\Entity\Address\Municipality;
use Linio\Frontend\Entity\Address\Neighborhood;
use Linio\Frontend\Entity\Address\Region;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Careful... In Mexico the municipality and city is inverted. That's why getMunicipalities loads cities and vice-versa.
 */
class Sepomex implements ResolveLocationAdapter
{
    use CacheAware;

    /**
     * @var array
     */
    protected $data;

    /**
     * {@inheritdoc}
     */
    public function resolvePostcode(string $postcode): array
    {
        $data = $this->cacheService->get('postcode:' . $postcode);

        if ($data === null) {
            return [];
        }

        return [
            'region' => $data['region_id'],
            'municipality' => $data['municipality'],
            'city' => $data['city'],
            'neighborhoods' => array_values($data['neighborhoods']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getCountries(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getRegions(ParameterBag $data): array
    {
        $places = $this->cacheService->get('regions');
        $regions = [];

        foreach ($places as $regionId => $regionName) {
            $region = new Region();
            $region->setId($regionId);
            $region->setName($regionName);

            $regions[] = $region;
        }

        return $regions;
    }

    /**
     * {@inheritdoc}
     */
    public function getMunicipalities(ParameterBag $data): array
    {
        $regionId = $data->get('region');
        $city = $data->get('city');
        $places = $this->cacheService->get(sprintf('%s:%s', $regionId, $city));
        $municipalities = [];

        if (!$places) {
            return $municipalities;
        }

        foreach ($places as $municipalityName) {
            $municipality = new Municipality();
            $municipality->setId($municipalityName);
            $municipality->setName($municipalityName);
            $municipalities[] = $municipality;
        }

        return $municipalities;
    }

    /**
     * {@inheritdoc}
     */
    public function getCities(ParameterBag $data): array
    {
        $regionId = $data->get('region');
        $places = $this->cacheService->get($regionId);
        $cities = [];

        if (!$places) {
            return $cities;
        }

        foreach ($places as $cityName) {
            $city = new City();
            $city->setId($cityName);
            $city->setName($cityName);

            $cities[] = $city;
        }

        return $cities;
    }

    /**
     * @param ParameterBag $data
     *
     * @return array
     */
    public function getNeighborhoods(ParameterBag $data): array
    {
        $regionId = $data->get('region');
        $city = $data->get('city');
        $municipality = $data->get('municipality');
        $places = $this->cacheService->get(sprintf('%s:%s:%s', $regionId, $city, $municipality));
        $neighborhoods = [];

        if (!$places) {
            return $neighborhoods;
        }

        foreach ($places as $neighborhoodName) {
            $neighborhood = new Neighborhood();
            $neighborhood->setId($neighborhoodName);
            $neighborhood->setName($neighborhoodName);
            $neighborhoods[] = $neighborhood;
        }

        return $neighborhoods;
    }
}
