<?php

declare(strict_types=1);

namespace Linio\Frontend\Location;

use Linio\Frontend\Entity\Address\City;
use Linio\Frontend\Entity\Address\Country;
use Linio\Frontend\Entity\Address\Municipality;
use Linio\Frontend\Entity\Address\Region;
use Linio\Frontend\Location\Communication\Resolve\ResolveLocationAdapter;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * TODO: Refactor methods to receive the exactly needed data.
 */
class ResolveLocation
{
    /**
     * @var ResolveLocationAdapter
     */
    protected $adapter;

    /**
     * @param ResolveLocationAdapter $adapter
     */
    public function setAdapter(ResolveLocationAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param string $postcode
     *
     * @return array
     */
    public function resolvePostcode(string $postcode): array
    {
        return $this->adapter->resolvePostcode($postcode);
    }

    /**
     * @return Country[]
     */
    public function getCountries(): array
    {
        return $this->adapter->getCountries();
    }

    /**
     * @param ParameterBag|null $data
     *
     * @return Region[]
     */
    public function getRegions(ParameterBag $data = null): array
    {
        $data = $data ?? new ParameterBag();

        return $this->adapter->getRegions($data);
    }

    /**
     * @param ParameterBag $data
     *
     * @return Municipality[]
     */
    public function getMunicipalities(ParameterBag $data): array
    {
        return $this->adapter->getMunicipalities($data);
    }

    /**
     * @param ParameterBag $data
     *
     * @return City[]
     */
    public function getCities(ParameterBag $data): array
    {
        return $this->adapter->getCities($data);
    }

    /**
     * @param ParameterBag $data
     *
     * @return City[]
     */
    public function getNeighborhoods(ParameterBag $data): array
    {
        return $this->adapter->getNeighborhoods($data);
    }
}
