<?php

declare(strict_types=1);

namespace Linio\Frontend\Location;

use DateTime;

class Address
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * @var string
     */
    protected $line1;

    /**
     * @var string
     */
    protected $line2;

    /**
     * @var string
     */
    protected $betweenStreet1;

    /**
     * @var string
     */
    protected $betweenStreet2;

    /**
     * @var string
     */
    protected $streetNumber;

    /**
     * @var string
     */
    protected $apartment;

    /**
     * @var string
     */
    protected $lot;

    /**
     * @var string
     */
    protected $neighborhood;

    /**
     * @var string
     */
    protected $department;

    /**
     * @var string
     */
    protected $municipality;

    /**
     * @var string
     */
    protected $urbanization;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $cityId;

    /**
     * @var string
     */
    protected $cityName;

    /**
     * @var string
     */
    protected $region;

    /**
     * @var string
     */
    protected $regionId;

    /**
     * @var string
     */
    protected $regionCode;

    /**
     * @var string
     */
    protected $regionName;

    /**
     * @var string
     */
    protected $postcode;

    /**
     * @var string
     */
    protected $additionalInformation;

    /**
     * @var string
     */
    protected $phone;

    /**
     * @var string
     */
    protected $mobilePhone;

    /**
     * @var int
     */
    protected $countryId;

    /**
     * @var string
     */
    protected $countryCode;

    /**
     * @var string
     */
    protected $countryName;

    /**
     * @var string
     */
    protected $taxIdentificationNumber;

    /**
     * @var bool
     */
    protected $defaultBilling;

    /**
     * @var bool
     */
    protected $defaultShipping;

    /**
     * @var string
     */
    protected $maternalName;

    /**
     * @var string
     */
    protected $invoiceType;

    /**
     * @var string
     */
    protected $company;

    /**
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title = null)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type = null)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName = null)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName = null)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix(string $prefix = null)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     */
    public function setLine1(string $line1 = null)
    {
        $this->line1 = $line1;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param string $line2
     */
    public function setLine2(string $line2 = null)
    {
        $this->line2 = $line2;
    }

    /**
     * @return string
     */
    public function getBetweenStreet1()
    {
        return $this->betweenStreet1;
    }

    /**
     * @param string $betweenStreet1
     */
    public function setBetweenStreet1(string $betweenStreet1 = null)
    {
        $this->betweenStreet1 = $betweenStreet1;
    }

    /**
     * @return string
     */
    public function getBetweenStreet2()
    {
        return $this->betweenStreet2;
    }

    /**
     * @param string $betweenStreet2
     */
    public function setBetweenStreet2(string $betweenStreet2 = null)
    {
        $this->betweenStreet2 = $betweenStreet2;
    }

    /**
     * @return string
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * @param string $streetNumber
     */
    public function setStreetNumber(string $streetNumber = null)
    {
        $this->streetNumber = $streetNumber;
    }

    /**
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * @param string $apartment
     */
    public function setApartment(string $apartment = null)
    {
        $this->apartment = $apartment;
    }

    /**
     * @return string
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @param string $lot
     */
    public function setLot(string $lot = null)
    {
        $this->lot = $lot;
    }

    /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     */
    public function setNeighborhood(string $neighborhood = null)
    {
        $this->neighborhood = $neighborhood;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment(string $department = null)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getMunicipality()
    {
        return $this->municipality;
    }

    /**
     * @param string $municipality
     */
    public function setMunicipality(string $municipality = null)
    {
        $this->municipality = $municipality;
    }

    /**
     * @return string
     */
    public function getUrbanization()
    {
        return $this->urbanization;
    }

    /**
     * @param string $urbanization
     */
    public function setUrbanization(string $urbanization = null)
    {
        $this->urbanization = $urbanization;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city = null)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId(int $cityId)
    {
        $this->cityId = $cityId;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     */
    public function setCityName(string $cityName = null)
    {
        $this->cityName = $cityName;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region = null)
    {
        $this->region = $region;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * @param int $regionId
     */
    public function setRegionId(int $regionId)
    {
        $this->regionId = $regionId;
    }

    /**
     * @return string
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * @param string $regionCode
     */
    public function setRegionCode(string $regionCode = null)
    {
        $this->regionCode = $regionCode;
    }

    /**
     * @return string
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * @param string $regionName
     */
    public function setRegionName(string $regionName = null)
    {
        $this->regionName = $regionName;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode = null)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param string $additionalInformation
     */
    public function setAdditionalInformation(string $additionalInformation = null)
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone = null)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param string $mobilePhone
     */
    public function setMobilePhone(string $mobilePhone = null)
    {
        $this->mobilePhone = $mobilePhone;
    }

    /**
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId(int $countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode = null)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param string $countryName
     */
    public function setCountryName(string $countryName = null)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return string
     */
    public function getTaxIdentificationNumber()
    {
        return $this->taxIdentificationNumber;
    }

    /**
     * @param string $taxIdentificationNumber
     */
    public function setTaxIdentificationNumber(string $taxIdentificationNumber = null)
    {
        $this->taxIdentificationNumber = $taxIdentificationNumber;
    }

    /**
     * @return bool
     */
    public function isDefaultBilling()
    {
        return $this->defaultBilling;
    }

    /**
     * @param bool $defaultBilling
     */
    public function setDefaultBilling(bool $defaultBilling = true)
    {
        $this->defaultBilling = $defaultBilling;
    }

    /**
     * @return bool
     */
    public function isDefaultShipping()
    {
        return $this->defaultShipping;
    }

    /**
     * @param bool $defaultShipping
     */
    public function setDefaultShipping(bool $defaultShipping = true)
    {
        $this->defaultShipping = $defaultShipping;
    }

    /**
     * @return string
     */
    public function getMaternalName()
    {
        return $this->maternalName;
    }

    /**
     * @param string $maternalName
     */
    public function setMaternalName(string $maternalName = null)
    {
        $this->maternalName = $maternalName;
    }

    /**
     * @return string
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * @param string $invoiceType
     */
    public function setInvoiceType(string $invoiceType = null)
    {
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company = null)
    {
        $this->company = $company;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
