<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Exception;

use Linio\Frontend\Exception\InputException;

class InvalidAddressException extends InputException
{
}
