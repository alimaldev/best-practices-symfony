<?php

declare(strict_types=1);

namespace Linio\Frontend\Location\Exception;

use Linio\Frontend\Exception\DomainException;

class AddressException extends DomainException
{
}
