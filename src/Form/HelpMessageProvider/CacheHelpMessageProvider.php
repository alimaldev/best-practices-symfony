<?php

declare(strict_types=1);

namespace Linio\Frontend\Form\HelpMessageProvider;

use Linio\Component\Cache\CacheAware;
use Linio\DynamicFormBundle\HelpMessageProvider;

class CacheHelpMessageProvider implements HelpMessageProvider
{
    use CacheAware;

    /**
     * @param string $key
     *
     * @return string
     */
    public function getHelpMessage($key)
    {
        $message = $this->cacheService->get($key);

        if (!$message) {
            return '';
        }

        return $message['text'];
    }
}
