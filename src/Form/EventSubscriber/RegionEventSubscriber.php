<?php

namespace Linio\Frontend\Form\EventSubscriber;

use Linio\Frontend\Location\ResolveLocationAware;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\ParameterBag;

class RegionEventSubscriber implements EventSubscriberInterface
{
    use ResolveLocationAware;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        if (!$form->has('region') && !$form->has('regionId')) {
            return;
        }

        $data = $event->getData();

        if ($form->has('country') && empty($data['country'])) {
            return;
        }

        $parameterBag = new ParameterBag($data);
        $regions = [];

        foreach ($this->resolveLocation->getRegions($parameterBag) as $region) {
            $regions[$region->getId()] = $region->getName();
        }

        if ($form->has('region')) {
            $fieldName = 'region';
        } else {
            $fieldName = 'regionId';
        }

        $form->add($fieldName, ChoiceType::class, [
            'choices' => $regions,
        ]);
    }
}
