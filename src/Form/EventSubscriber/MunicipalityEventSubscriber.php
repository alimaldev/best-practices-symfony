<?php

namespace Linio\Frontend\Form\EventSubscriber;

use Linio\Frontend\Location\ResolveLocationAware;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\ParameterBag;

class MunicipalityEventSubscriber implements EventSubscriberInterface
{
    use ResolveLocationAware;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        if (!$form->has('municipality')) {
            return;
        }

        $field = $form->get('municipality');

        if (!$field->getConfig()->getType()->getInnerType() instanceof ChoiceType) {
            return;
        }

        $data = $event->getData();

        if (!isset($data['region']) && !isset($data['regionId'])) {
            return;
        }

        $parameterBag = new ParameterBag($data);
        $municipalities = [];

        foreach ($this->resolveLocation->getMunicipalities($parameterBag) as $municipality) {
            $municipalities[$municipality->getId()] = $municipality->getName();
        }

        $form->add('municipality', ChoiceType::class, [
            'choices' => $municipalities,
        ]);
    }
}
