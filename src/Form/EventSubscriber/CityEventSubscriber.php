<?php

namespace Linio\Frontend\Form\EventSubscriber;

use Linio\Frontend\Location\ResolveLocationAware;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\ParameterBag;

class CityEventSubscriber implements EventSubscriberInterface
{
    use ResolveLocationAware;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        if (!$form->has('city')) {
            return;
        }

        $field = $form->get('city');

        if (!$field->getConfig()->getType()->getInnerType() instanceof ChoiceType) {
            return;
        }

        $data = $event->getData();

        if (!isset($data['municipality'])) {
            return;
        }

        $parameterBag = new ParameterBag($data);
        $cities = [];

        foreach ($this->resolveLocation->getCities($parameterBag) as $city) {
            $cities[$city->getId()] = $city->getName();
        }

        $form->add('city', ChoiceType::class, [
            'choices' => $cities,
        ]);
    }
}
