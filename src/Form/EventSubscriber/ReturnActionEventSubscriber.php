<?php

namespace Linio\Frontend\Form\EventSubscriber;

use Linio\Frontend\Communication\Customer\ItemReturnAware;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReturnActionEventSubscriber implements EventSubscriberInterface
{
    use ItemReturnAware;

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'setReturnActions',
        ];
    }

    public function setReturnActions(FormEvent $event)
    {
        $form = $event->getForm();

        $options = [
            'label' => 'order.returns.choose_return_action',
            'required' => true,
            'multiple' => false,
            'expanded' => false,
            'placeholder' => 'order.returns.empty_choice',
            'choices' => $this->itemReturnService->getActions(),
            'empty_data' => null,
            'constraints' => [
                new NotBlank(),
            ],
        ];

        $form->add('action', ChoiceType::class, $options);
    }
}
