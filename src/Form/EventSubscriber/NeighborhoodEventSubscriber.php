<?php

namespace Linio\Frontend\Form\EventSubscriber;

use Linio\Frontend\Location\ResolveLocationAware;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\ParameterBag;

class NeighborhoodEventSubscriber implements EventSubscriberInterface
{
    use ResolveLocationAware;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        if (!$form->has('neighborhood')) {
            return;
        }

        $field = $form->get('neighborhood');

        if (!$field->getConfig()->getType()->getInnerType() instanceof ChoiceType) {
            return;
        }

        $data = $event->getData();

        if (!isset($data['municipality'])) {
            return;
        }

        $parameterBag = new ParameterBag($data);
        $neighborhoods = [];

        foreach ($this->resolveLocation->getNeighborhoods($parameterBag) as $municipality) {
            $neighborhoods[$municipality->getId()] = $municipality->getName();
        }

        $form->add('neighborhood', ChoiceType::class, [
            'choices' => $neighborhoods,
        ]);
    }
}
