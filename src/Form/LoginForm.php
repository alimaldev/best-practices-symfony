<?php

namespace Linio\Frontend\Form;

use Linio\Controller\RouterAware;
use Linio\Frontend\Form\Type\EmailType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginForm extends AbstractType
{
    use RouterAware;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction($this->generateUrl('frontend.security.login.check'))
            ->add('email', EmailType::class, [
                'attr' => [
                    'class' => 'login-form-input form-style',
                ],
                'label_attr' => [
                    'class' => 'login-form-label',
                ],
                'label' => 'login.form.email.label',
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'class' => 'login-form-input form-style',
                    'placeholder' => 'login.form.password.placeholder',
                ],
                'label_attr' => [
                    'class' => 'login-form-label',
                ],
                'label' => 'login.form.password.label',
            ])
            ->add('login', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-action btn-m btn-block font-size-tall margin-mini-top',
                ],
                'label' => 'login.form.login',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_token_id' => 'authenticate',
        ]);
    }
}
