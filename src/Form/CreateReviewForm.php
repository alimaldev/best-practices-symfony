<?php

namespace Linio\Frontend\Form;

use Linio\Frontend\Entity\Product\Review;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateReviewForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ratingScore', HiddenType::class, [
            'label' => 'reviews.qualification',
            'label_attr' => ['class' => 'control-label'],
            'data' => 1,
        ]);
        $builder->add('title', TextType::class, [
            'label' => 'product.title_review',
            'label_attr' => ['class' => 'control-label'],
            'attr' => ['class' => 'form-control'],
        ]);
        $builder->add('comment', TextareaType::class, [
            'label' => 'reviews.comment',
            'label_attr' => ['class' => 'control-label', 'rows' => 3],
            'attr' => ['class' => 'form-control'],
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Review::class,
            ]
        );
    }
}
