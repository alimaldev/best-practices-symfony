<?php

declare(strict_types=1);

namespace Linio\Frontend\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PartnershipForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('code', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'trim' => true,
        ]);
        $builder->add('accountNumber', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
            'trim' => true,
        ]);
        $builder->add('level', TextType::class, [
            'required' => false,
        ]);
    }
}
