<?php

declare(strict_types=1);

namespace Linio\Frontend\Form\Customer;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ItemReturnRequestForm extends AbstractType
{
    /**
     * @var EventSubscriberInterface
     */
    protected $reasonsSubscriber;

    /**
     * @var EventSubscriberInterface
     */
    protected $actionSubscriber;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('orderNumber', TextType::class, ['required' => true, 'constraints' => [new NotBlank()]]);
        $builder->add('sku', TextType::class, ['required' => true, 'constraints' => [new NotBlank()]]);
        $builder->add('quantity', IntegerType::class, ['required' => true]);
        $builder->addEventSubscriber($this->reasonsSubscriber);
        $builder->addEventSubscriber($this->actionSubscriber);
        $builder->add('comment', TextareaType::class, [
            'label' => 'order.returns.provide_description',
            'required' => true,
            'attr' => [
                'placeholder' => 'order.returns.comment_placeholder',
            ],
            'empty_data' => null,
            'trim' => true,
            'constraints' => [
                new NotBlank(),
            ],
        ]);
    }

    /**
     * @param EventSubscriberInterface $reasonsSubscriber
     */
    public function setReasonsSubscriber(EventSubscriberInterface $reasonsSubscriber)
    {
        $this->reasonsSubscriber = $reasonsSubscriber;
    }

    /**
     * @param EventSubscriberInterface $actionsSubscriber
     */
    public function setActionsSubscriber(EventSubscriberInterface $actionsSubscriber)
    {
        $this->actionSubscriber = $actionsSubscriber;
    }
}
