<?php

declare(strict_types=1);

namespace Linio\Frontend\Form;

use Linio\Frontend\Form\Type\EmailType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PromotionalNewsletterForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
            'label' => 'Email',
            'label_attr' => ['class' => 'login-form-input'],
        ]);
    }
}
