<?php

declare(strict_types=1);

namespace Linio\Frontend\Form;

use Linio\Frontend\Customer\Order\SellerReview;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SellerReviewForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('orderNumber', TextType::class, ['mapped' => false, 'constraints' => [new NotBlank()]]);
        $builder->add('sellerId', NumberType::class, ['mapped' => false, 'constraints' => [new NotBlank()]]);
        $builder->add('rating', NumberType::class, ['constraints' => [new NotBlank()]]);
        $builder->add('title', TextType::class);
        $builder->add('detail', TextType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SellerReview::class,
        ]);
    }
}
