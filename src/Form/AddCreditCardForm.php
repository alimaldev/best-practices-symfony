<?php

namespace Linio\Frontend\Form;

use Linio\Controller\RouterAware;
use Linio\Frontend\Entity\Customer\CreditCard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Luhn;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddCreditCardForm extends AbstractType
{
    use RouterAware;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction($this->generateUrl('frontend.customer.cards.add.save'));
        $builder->add(
            'cardHolderFirstName',
            TextType::class,
            [
                'label' => 'saved_cards.owner_name',
                'attr' => [
                    'placeholder' => 'saved_cards.name_as_in_the_card',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ]
        );
        $builder->add(
            'cardHolderLastName',
            TextType::class,
            [
                'label' => 'saved_cards.owner_last_name',
                'attr' => [
                    'placeholder' => 'saved_cards.name_as_in_the_card',
                ],
                'constraints' => [
                    new NotBlank(),
                ],
            ]
        );
        $builder->add(
            'cardNumber',
            TextType::class,
            [
                'label' => 'saved_cards.card_number',
                'attr' => [
                    'placeholder' => 'saved_cards.default_card_number',
                    'mask' => '9999 9999 9999 9999',
                ],
                'constraints' => [
                    new NotBlank(),
                    new Luhn(),
                ],
            ]
        );
        $builder->add(
            'expirationDate',
            DateType::class,
            [
                'label' => 'saved_cards.expiration',
                'widget' => 'choice',
                'empty_value' => [
                    'year' => 'saved_cards.expiration_year',
                    'month' => 'saved_cards.expiration_month',
                    'day' => 'Day',
                ],
                'format' => 'd-MM-yyyy',
                'input' => 'datetime',
                'years' => range(date('Y'), date('Y') + 15),
                'days' => [1],
            ]
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => CreditCard::class,
            ]
        );
    }
}
