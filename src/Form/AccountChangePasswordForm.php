<?php

namespace Linio\Frontend\Form;

use Linio\Controller\RouterAware;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AccountChangePasswordForm extends AbstractType
{
    use RouterAware;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'current',
                PasswordType::class,
                [
                    'label' => 'profile.active_password',
                    'label_attr' => [
                        'class' => 'full-width pull-left padding-small-bottom',
                    ],
                    'attr' => [
                        'class' => 'form-style full-width',
                        'placeholder' => 'saved_cards.type_current_password',
                    ],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ],
                ]
            )
            ->add(
                'new_password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'first_name' => 'new',
                    'first_options' => [
                        'label' => 'profile.new_password',
                        'label_attr' => [
                            'class' => 'full-width pull-left padding-small-bottom',
                        ],
                        'attr' => [
                            'class' => 'form-style full-width',
                            'placeholder' => 'saved_cards.type_new_password',
                        ],
                        'required' => true,
                    ],
                    'second_name' => 'new_repeat',
                    'second_options' => [
                        'label' => 'profile.confirm_new_password',
                        'label_attr' => [
                            'class' => 'full-width pull-left padding-small-bottom',
                        ],
                        'attr' => [
                            'class' => 'form-style full-width',
                            'placeholder' => 'saved_cards.confirm_new_password',
                        ],
                        'required' => true,
                    ],
                    'options' => [
                        'attr' => [
                            'class' => 'padding-xlarge-bottom padding-xlarge-right',
                        ],
                    ],
                ]
            )
            ->setAction($this->generateUrl('frontend.customer.password.change'));
    }
}
