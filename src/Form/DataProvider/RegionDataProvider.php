<?php

namespace Linio\Frontend\Form\DataProvider;

use Linio\DynamicFormBundle\DataProvider;
use Linio\Frontend\Location\ResolveLocationAware;

class RegionDataProvider implements DataProvider
{
    use ResolveLocationAware;

    /**
     * Retrieve data for collection fields.
     *
     * @return array
     */
    public function getData()
    {
        $regions = $this->getResolveLocation()->getRegions();
        $data = [];

        foreach ($regions as $region) {
            $data[$region->getId()] = $region->getName();
        }

        return $data;
    }
}
