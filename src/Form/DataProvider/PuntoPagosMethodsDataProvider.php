<?php

namespace Linio\Frontend\Form\DataProvider;

use Linio\Component\Cache\CacheAware;
use Linio\DynamicFormBundle\DataProvider;

class PuntoPagosMethodsDataProvider implements DataProvider
{
    use CacheAware;

    /**
     * Retrieve data for collection fields.
     *
     * @return array
     */
    public function getData()
    {
        $paymentMethods = $this->cacheService->get('payment:PuntoPagos_HostedPaymentPage_PaymentMethods');
        $data = [];

        if (!$paymentMethods) {
            return $data;
        }

        foreach ($paymentMethods as $id => $paymentMethod) {
            $data[$id] = $paymentMethod;
        }

        return $data;
    }
}
