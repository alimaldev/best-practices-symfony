<?php

namespace Linio\Frontend\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstallmentType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('minimum_installment', 1);
        $resolver->setAllowedTypes('minimum_installment', 'integer');

        $resolver->setDefault('maximum_installment', 36);
        $resolver->setAllowedTypes('maximum_installment', 'integer');

        $resolver->setDefault('choices', function (Options $options) {
            $installments = range($options['minimum_installment'], $options['maximum_installment']);

            return array_combine($installments, $installments);
        });
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
