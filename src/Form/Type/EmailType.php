<?php

namespace Linio\Frontend\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmailType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => true,
            'trim' => true,
            'constraints' => [
                new NotBlank(),
            ],
        ]);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['attr'] = array_merge($view->vars['attr'], [
            'placeholder' => 'login.form.email.placeholder',
            'pattern' => '^[a-zA-Z0-9._%+\-]+@([a-zA-Z0-9-]{1,}\.)+[a-zA-Z]{2,}$',
        ]);
    }

    public function getParent()
    {
        return TextType::class;
    }
}
