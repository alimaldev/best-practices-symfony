<?php

namespace Linio\Frontend\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExpirationYearType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('year_format', 'Y');
        $resolver->setAllowedTypes('year_format', 'string');

        $resolver->setDefault('year_range', 20);
        $resolver->setAllowedTypes('year_range', 'integer');

        $resolver->setDefault('choices', function (Options $options) {
            $currentYear = date($options['year_format']);
            $years = range($currentYear, $currentYear + $options['year_range']);

            return array_combine($years, $years);
        });
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
