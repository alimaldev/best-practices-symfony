<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class Bob4AliceClient extends GuzzleClient
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param string $baseUri
     * @param string $countryCode
     * @param array $config
     */
    public function __construct(string $baseUri, string $countryCode, array $config = [])
    {
        $handlerStack = $config['handler'] ?? HandlerStack::create();

        $config['handler'] = $handlerStack;
        $config['base_uri'] = $baseUri;
        $config['headers'] = [
            'X-Store' => $countryCode,
        ];

        $handlerStack->push(Middleware::mapRequest(function (RequestInterface $request) {
            return $request->withHeader(
                'X-Request-ID',
                $this->requestStack->getCurrentRequest()->headers->get('X-Request-ID')
            );
        }));

        parent::__construct($config);
    }
}
