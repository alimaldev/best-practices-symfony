<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer;

use Linio\Frontend\Communication\Customer\Exception\CustomerEmailExistsException;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Communication\Customer\Exception\RegisterWalletException;
use Linio\Frontend\Communication\Customer\Exception\UnregisteredWalletException;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Invoice\InvoiceResult;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\BankTransferConfirmation;
use Linio\Frontend\Entity\Customer\Coupon;
use Linio\Frontend\Entity\Customer\CreditCard;
use Linio\Frontend\Entity\Customer\LinioPlusMembership;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Security\Telesales\TelesalesAuthenticationToken;
use Linio\Frontend\Security\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface CustomerAdapterInterface
{
    /**
     * @param Customer $customer
     *
     * @throws CustomerEmailExistsException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function create(Customer $customer);

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function update(Customer $customer);

    /**
     * @param TokenInterface $token
     *
     * @throws InvalidLoginCredentialsException Thrown for client errors
     * @throws InvalidLoginCredentialsException Thrown for server errors
     *
     * @return Customer
     */
    public function login(TokenInterface $token): Customer;

    /**
     * @param TokenInterface $token
     *
     * @throws InvalidLoginCredentialsException Thrown for client errors
     * @throws InvalidLoginCredentialsException Thrown for server errors
     *
     * @return Customer
     */
    public function loginOAuth(TokenInterface $token): Customer;

    /**
     * @param TelesalesAuthenticationToken $token
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Customer
     */
    public function loginTelesales(TelesalesAuthenticationToken $token): Customer;

    /**
     * @param string $username
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return User
     */
    public function getUser(string $username): User;

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return CreditCard[]
     */
    public function getCreditCards(Customer $customer): array;

    /**
     * @param CreditCard $creditCard
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function addCreditCard(CreditCard $creditCard, Customer $customer);

    /**
     * @param int $creditCardId
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function removeCreditCard(int $creditCardId, Customer $customer);

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Coupon[]
     */
    public function getCoupons(Customer $customer): array;

    /**
     * @param string $email
     * @param string $url
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function requestResetPassword(string $email, string $url);

    /**
     * @param string $token
     * @param string $email
     * @param string $password
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function resetPassword(string $token, string $email, string $password);

    /**
     * @param Customer $customer
     * @param string $currentPassword
     * @param string $newPassword
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function changePassword(Customer $customer, string $currentPassword, string $newPassword);

    /**
     * @param string $orderId
     * @param Address $billingAddress
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return \Linio\Frontend\Customer\Order\Invoice\InvoiceResult
     */
    public function requestInvoice(string $orderId, Address $billingAddress): InvoiceResult;

    /**
     * @param Customer $customer
     * @param Order $order
     * @param array $returnData
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     * @throws OrderException When only some of the requested quantity could be returned
     */
    public function createItemReturnRequest(Customer $customer, Order $order, array $returnData);

    /**
     * @param Customer $customer
     * @param string $account
     * @param string $holderName
     * @param string $holderLastname
     *
     * @throws RegisterWalletException Thrown for client errors
     * @throws RegisterWalletException Thrown for server errors
     */
    public function registerWallet(Customer $customer, string $account, string $holderName, string $holderLastname);

    /**
     * @param Customer $customer
     *
     * @throws UnregisteredWalletException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Wallet
     */
    public function getWallet(Customer $customer): Wallet;

    /**
     * @param int $customerId
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return LinioPlusMembership
     */
    public function getLinioPlusMembershipDetails(int $customerId): LinioPlusMembership;

    /**
     * @param BankTransferConfirmation $bankTransferConfirmation
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function createBankTransferConfirmation(BankTransferConfirmation $bankTransferConfirmation);

    /**
     * @param Customer $customer
     * @param SellerReview $review
     * @param Order $order
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function reviewSeller(Customer $customer, SellerReview $review, Order $order);
}
