<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Adapter;

use DateTime;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Api\Output\AddressOutput;
use Linio\Frontend\Communication\Customer\CustomerAdapterInterface;
use Linio\Frontend\Communication\Customer\Exception\CustomerEmailExistsException;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Communication\Customer\Exception\RegisterWalletException;
use Linio\Frontend\Communication\Customer\Exception\UnregisteredWalletException;
use Linio\Frontend\Communication\Customer\Input\CustomerInput;
use Linio\Frontend\Customer\Communication\Coupon\Input\CustomerCouponInput;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Invoice\InvoiceResult;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\BankTransferConfirmation;
use Linio\Frontend\Entity\Customer\Coupon;
use Linio\Frontend\Entity\Customer\CreditCard;
use Linio\Frontend\Entity\Customer\LinioPlusMembership;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\Security\OAuth\OauthToken;
use Linio\Frontend\Security\Telesales\TelesalesAuthenticationToken;
use Linio\Frontend\Security\User;
use Linio\Type\Money;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class Bob4AliceAdapter implements CustomerAdapterInterface
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var int
     */
    protected $countryId;

    /**
     * @var CustomerCouponInput
     */
    protected $customerCouponInput;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var CustomerInput
     */
    protected $customerInput;

    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @var AddressOutput
     */
    protected $addressOutput;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param CustomerCouponInput $customerCouponInput
     */
    public function setCustomerCouponInput(CustomerCouponInput $customerCouponInput)
    {
        $this->customerCouponInput = $customerCouponInput;
    }

    /**
     * @param CustomerInput $customerInput
     */
    public function setCustomerInput(CustomerInput $customerInput)
    {
        $this->customerInput = $customerInput;
    }

    /**
     * @param PlatformInformation $platformInformation
     */
    public function setPlatformInformation(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    /**
     * @param AddressOutput $addressOutput
     */
    public function setAddressOutput(AddressOutput $addressOutput)
    {
        $this->addressOutput = $addressOutput;
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerEmailExistsException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function create(Customer $customer)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'firstName' => $customer->getFirstName(),
            'lastName' => $customer->getLastName(),
            'email' => $customer->getEmail(),
            'password' => $customer->getPassword(),
            'birthDate' => $customer->getBornDate() ? $customer->getBornDate()->format('Y-m-d') : '',
            'gender' => $customer->getGender(),
            'mobilePhone' => $customer->getSmsPhone(),
            'nationalRegistrationNumber' => $customer->getNationalRegistrationNumber(),
            'taxIdentificationNumber' => $customer->getTaxIdentificationNumber(),
            'subscribeToNewsletter' => $customer->isSubscribedToNewsletter(),
            'subscribeToSms' => $customer->isSubscribedToSmsNewsletter(),
            'utmSource' => 'testSource',
            'utmMedium' => 'testMedium',
            'utmCampaign' => 'testCampaign',
        ];

        try {
            $response = $this->client->request('POST', '/customer/create', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == 'CUSTOMER_DUPLICATE_EMAIL') {
                throw new CustomerEmailExistsException('customer.email.exists.true');
            }

            // TODO: Add all errors to the exception
            foreach ($responseBody['errors'] as $error) {
                // B4A doesn't seem to have an INVALID_TAX_IDENTIFICATION_NUMBER
                if ($error['message'] == 'INVALID_NATIONAL_REGISTRATION_NUMBER') {
                    throw new CustomerException(ExceptionMessage::INVALID_NATIONAL_REGISTRATION_NUMBER);
                }
            }

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $customer->setId($responseBody['id']);
        $customer->setLinioId($responseBody['linioId']);
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function update(Customer $customer)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'firstName' => $customer->getFirstName(),
            'lastName' => $customer->getLastName(),
            'birthDate' => $customer->getBornDate() ? $customer->getBornDate()->format('Y-m-d') : '',
            'gender' => $customer->getGender(),
            'nationalRegistrationNumber' => $customer->getNationalRegistrationNumber(),
            'taxIdentificationNumber' => $customer->getTaxIdentificationNumber(),
        ];

        try {
            $this->client->request('POST', '/customer/update', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param TokenInterface $token
     *
     * @throws InvalidLoginCredentialsException Thrown for client errors
     * @throws InvalidLoginCredentialsException Thrown for server errors
     *
     * @return Customer
     */
    public function login(TokenInterface $token): Customer
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $token->getUsername(),
            'password' => $token->getCredentials(),
        ];

        try {
            $response = $this->client->request('POST', '/auth/login', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidLoginCredentialsException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidLoginCredentialsException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->mapLoginResponseToCustomer($responseBody);
    }

    /**
     * @param TokenInterface $token
     *
     * @throws InvalidLoginCredentialsException Thrown for client errors
     * @throws InvalidLoginCredentialsException Thrown for server errors
     *
     * @return Customer
     */
    public function loginOAuth(TokenInterface $token): Customer
    {
        /* @var OauthToken $token */

        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $token->getEmail(),
            'oAuthProvider' => $this->getOAuthProviderName($token->getProvider()),
            'oAuthIdentifier' => $token->getUsername(),
            'firstName' => $token->getFirstName(),
            'lastName' => $token->getLastName(),
            'birthDate' => null,
            'gender' => null,
            'utmSource' => null,
            'utmMedium' => null,
            'utmCampaign' => null,
        ];

        try {
            $response = $this->client->request('POST', '/auth/login-oauth', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidLoginCredentialsException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidLoginCredentialsException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $customer = $this->mapLoginResponseToCustomer($responseBody);

        return $customer;
    }

    /**
     * @param TelesalesAuthenticationToken $token
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Customer
     */
    public function loginTelesales(TelesalesAuthenticationToken $token): Customer
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $token->getUsername(),
            'telesalesUsername' => $token->getBobUsername(),
            'telesalesPassword' => $token->getCredentials(),
        ];

        try {
            $response = $this->client->request('POST', '/auth/telesales', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidLoginCredentialsException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new InvalidLoginCredentialsException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $customer = $this->mapLoginResponseToCustomer($responseBody);
        $customer->impersonate($responseBody['assistedSalesOperator']);

        return $customer;
    }

    /**
     * @param string $username
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return User
     */
    public function getUser(string $username): User
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $username,
        ];

        try {
            $response = $this->client->request('POST', '/auth/remembered', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->mapLoginResponseToUser($responseBody);
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return CreditCard[]
     */
    public function getCreditCards(Customer $customer): array
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/credit-card/list', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $creditCards = [];

        foreach ($responseBody as $row) {
            $creditCard = new CreditCard();
            $creditCard->setId($row['id']);
            $creditCard->setCardholderName($row['holderName']);
            $creditCard->setCardNumber($row['maskedNumber']);
            $creditCard->setBrand($row['brand']);
            $creditCard->setExpirationDate(new DateTime($row['expirationDate']));

            $creditCards[] = $creditCard;
        }

        return $creditCards;
    }

    /**
     * @param CreditCard $creditCard
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function addCreditCard(CreditCard $creditCard, Customer $customer)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'holderName' => sprintf('%s %s', $creditCard->getCardholderFirstName(), $creditCard->getCardholderLastName()),
            'expirationDate' => $creditCard->getExpirationDate()->format('Y-m-d H:i:s'),
            'creditCardNumber' => $creditCard->getCardNumber(),
            'brand' => $creditCard->getBrand(),
        ];

        try {
            $this->client->request('POST', '/credit-card/create', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param int $creditCardId
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function removeCreditCard(int $creditCardId, Customer $customer)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'creditCardId' => $creditCardId,
        ];

        try {
            $response = $this->client->request('POST', '/credit-card/delete', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Coupon[]
     */
    public function getCoupons(Customer $customer): array
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/coupon/list', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->customerCouponInput->toCoupons($responseBody);
    }

    /**
     * @param string $email
     * @param string $url
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function requestResetPassword(string $email, string $url)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $email,
            'resetUrl' => $url,
        ];

        try {
            $this->client->request('POST', '/auth/request-password-reset', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if (isset($responseBody['code']) && $responseBody['code'] == ExceptionMessage::CUSTOMER_EMAIL_NOT_FOUND) {
                throw new CustomerEmailExistsException($responseBody['code']);
            }

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param string $token
     * @param string $email
     * @param string $password
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function resetPassword(string $token, string $email, string $password)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $email,
            'resetToken' => $token,
            'newPassword' => $password,
        ];

        try {
            $response = $this->client->request('POST', '/auth/reset-password', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code'], $responseBody);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code'], $responseBody);
        }
    }

    /**
     * @param Customer $customer
     * @param string $currentPassword
     * @param string $newPassword
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function changePassword(Customer $customer, string $currentPassword, string $newPassword)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'email' => $customer->getEmail(),
            'currentPassword' => $currentPassword,
            'newPassword' => $newPassword,
        ];

        try {
            $this->client->request('POST', '/customer/change-password', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param string $orderId
     * @param Address $billingAddress
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return InvoiceResult
     */
    public function requestInvoice(string $orderId, Address $billingAddress): InvoiceResult
    {
        $address = $this->addressOutput->fromAddress($billingAddress);
        $address['isBilling'] = true;

        if (!isset($address['regionId'])) {
            $address['regionId'] = $address['region'];
        }

        $requestBody = [
            'orderId' => $orderId,
            'address' => $address,
        ];

        try {
            $response = $this->client->request('POST', '/invoice/create', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new OrderException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->customerInput->toInvoiceResult($responseBody);
    }

    /**
     * @param Customer $customer
     * @param Order $order
     * @param array $returnData
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     * @throws OrderException When only some of the requested quantity could be returned
     */
    public function createItemReturnRequest(Customer $customer, Order $order, array $returnData)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'orderId' => $order->getId(),
            'simpleSku' => $returnData['sku'],
            'quantity' => $returnData['quantity'],
            'reasonId' => $returnData['reason'],
            'actionId' => $returnData['action'],
            'comment' => $returnData['comment'],
        ];

        try {
            $response = $this->client->request('POST', '/item/return', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $failedItemCount = 0;

        foreach ($responseBody as $itemId => $status) {
            if ($status == 'fail') {
                $failedItemCount++;
            }
        }

        if ($failedItemCount > 0) {
            throw new OrderException('order.returns.not_returnable');
        }
    }

    /**
     * @param Customer $customer
     * @param string $account
     * @param string $holderName
     * @param string $holderLastname
     *
     * @throws RegisterWalletException Thrown for client errors
     * @throws RegisterWalletException Thrown for server errors
     */
    public function registerWallet(Customer $customer, string $account, string $holderName, string $holderLastname)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'walletIdentifier' => $account,
            'walletProvider' => 'Linio_Wallet',
            'holderName' => $holderName,
            'holderLastname' => $holderLastname,
        ];

        try {
            $this->client->request('POST', '/customer/wallet/associate', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new RegisterWalletException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new RegisterWalletException($responseBody['code']);
        }
    }

    /**
     * @param Customer $customer
     *
     * @throws UnregisteredWalletException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Wallet
     */
    public function getWallet(Customer $customer): Wallet
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
        ];

        try {
            $response = $this->client->request('POST', '/customer/wallet/statement', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            if ($responseBody['code'] == ExceptionMessage::WALLET_NOT_FOUND) {
                throw new UnregisteredWalletException($responseBody['code']);
            }

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        return $this->customerInput->toWallet($responseBody);
    }

    /**
     * @param int $customerId
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return LinioPlusMembership
     */
    public function getLinioPlusMembershipDetails(int $customerId): LinioPlusMembership
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customerId,
        ];

        try {
            $response = $this->client->request('POST', '/customer/linio-plus/subscription', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $membership = new LinioPlusMembership(
            $responseBody['id'],
            $customerId,
            new DateTime($responseBody['startDate']),
            new DateTime($responseBody['endDate']),
            $responseBody['isActive']
        );

        $membership->setBalance(Money::fromCents($responseBody['balance']));
        $membership->setReference($responseBody['reference']);

        if (!empty($responseBody['walletSubscriptionId'])) {
            $membership->setWalletSubscription($responseBody['walletSubscriptionId']);
        }

        if (!empty($responseBody['updatedAt'])) {
            $membership->setLastUpdated(new DateTime($responseBody['updatedAt']));
        }

        return $membership;
    }

    /**
     * @param BankTransferConfirmation $bankTransferConfirmation
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function createBankTransferConfirmation(BankTransferConfirmation $bankTransferConfirmation)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $bankTransferConfirmation->getCustomer()->getId(),
            'transferNumber' => $bankTransferConfirmation->getNumber(),
            'date' => $bankTransferConfirmation->getDate() ? $bankTransferConfirmation->getDate()->format('Y-m-d') : '',
            'amount' => (int) $bankTransferConfirmation->getAmount(),
            'bankId' => (int) $bankTransferConfirmation->getBankId(),
            'orderIds' => [],
        ];

        foreach ($bankTransferConfirmation->getOrders() as $order) {
            $requestBody['orderIds'][] = $order->getId();
        }

        try {
            $response = $this->client->request('POST', '/customer/order/confirm-bank-transfer', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param Customer $customer
     * @param SellerReview $review
     * @param Order $order
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function reviewSeller(Customer $customer, SellerReview $review, Order $order)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'customerId' => $customer->getId(),
            'orderId' => $order->getId(),
            'sellerId' => $review->getSeller()->getId(),
            'rating' => $review->getRating(),
            'reviewTitle' => $review->getTitle(),
            'reviewText' => $review->getDetail(),
            'nickname' => null,
            'ipAddress' => $this->requestStack->getCurrentRequest()->getClientIp(),
        ];

        try {
            $response = $this->client->request('POST', '/customer/order/review-seller', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CustomerException($responseBody['code']);
        }
    }

    /**
     * @param array $responseBody
     *
     * @return Customer
     */
    protected function mapLoginResponseToCustomer(array $responseBody): Customer
    {
        $customer = new Customer();
        $customer->setId($responseBody['id']);
        $customer->setLinioId($responseBody['linioId']);
        $customer->setFirstName($responseBody['firstName']);
        $customer->setLastName($responseBody['lastName']);
        $customer->setEmail($responseBody['email']);
        $customer->setGender($responseBody['gender']);
        $customer->setReturningCustomer($responseBody['isReturningCustomer']);
        $customer->setSubscribedToLinioPlus($responseBody['isLinioPlusSubscriber']);

        if (!empty($responseBody['birthDate'])) {
            $customer->setBornDate(new DateTime($responseBody['birthDate']));
        }

        if (!empty($responseBody['nationalRegistrationNumber'])) {
            $customer->setNationalRegistrationNumber($responseBody['nationalRegistrationNumber']);
        }

        if (!empty($responseBody['taxIdentificationNumber'])) {
            $customer->setTaxIdentificationNumber($responseBody['taxIdentificationNumber']);
        }

        return $customer;
    }

    /**
     * @param array $responseBody
     *
     * @return User
     */
    protected function mapLoginResponseToUser(array $responseBody): User
    {
        $customer = new User();
        $customer->setId($responseBody['id']);
        $customer->setLinioId($responseBody['linioId']);
        $customer->setFirstName($responseBody['firstName']);
        $customer->setLastName($responseBody['lastName']);
        $customer->setEmail($responseBody['email']);
        $customer->setGender($responseBody['gender']);
        $customer->setReturningCustomer($responseBody['isReturningCustomer']);
        $customer->setSubscribedToLinioPlus($responseBody['isLinioPlusSubscriber']);

        if (!empty($responseBody['birthDate'])) {
            $customer->setBornDate(new DateTime($responseBody['birthDate']));
        }

        if (!empty($responseBody['nationalRegistrationNumber'])) {
            $customer->setNationalRegistrationNumber($responseBody['nationalRegistrationNumber']);
        }

        if (!empty($responseBody['taxIdentificationNumber'])) {
            $customer->setTaxIdentificationNumber($responseBody['taxIdentificationNumber']);
        }

        return $customer;
    }

    /**
     * @param string $oAuthProvider
     *
     * @throws InvalidLoginCredentialsException
     *
     * @return string
     */
    protected function getOAuthProviderName(string $oAuthProvider): string
    {
        $providerMap = [
            'facebook' => 'Facebook',
            'gplus' => 'Google',
        ];

        if (!isset($providerMap[$oAuthProvider])) {
            throw new InvalidLoginCredentialsException('Invalid oauth provider');
        }

        return $providerMap[$oAuthProvider];
    }
}
