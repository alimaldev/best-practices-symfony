<?php

namespace Linio\Frontend\Communication\Customer;

trait ItemReturnAware
{
    /**
     * @var ItemReturnService
     */
    protected $itemReturnService;

    /**
     * @param ItemReturnService $itemReturnService
     */
    public function setItemReturnService(ItemReturnService $itemReturnService)
    {
        $this->itemReturnService = $itemReturnService;
    }
}
