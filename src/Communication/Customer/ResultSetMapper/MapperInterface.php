<?php

namespace Linio\Frontend\Communication\Customer\ResultSetMapper;

interface MapperInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function map(array $data);
}
