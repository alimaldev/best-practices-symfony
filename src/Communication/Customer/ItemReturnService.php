<?php

namespace Linio\Frontend\Communication\Customer;

use Linio\Component\Cache\CacheAware;

class ItemReturnService
{
    use CacheAware;

    const RETURN_CACHE_KEY = 'returns';

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @return array
     */
    public function getReasons()
    {
        $config = $this->getReturnConfigData();

        return isset($config['reasons']) ? $config['reasons'] : [];
    }

    /**
     * @return array
     */
    public function getActions()
    {
        $config = $this->getReturnConfigData();

        return isset($config['actions']) ? $config['actions'] : [];
    }

    /**
     * @return array
     */
    protected function getReturnConfigData()
    {
        if (empty($this->data)) {
            $this->data = $this->cacheService->get(static::RETURN_CACHE_KEY);
        }

        return $this->data;
    }
}
