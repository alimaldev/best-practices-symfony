<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer;

use Linio\Frontend\Communication\Country\CountryAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerEmailExistsException;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Communication\Customer\Exception\InvalidLoginCredentialsException;
use Linio\Frontend\Communication\Customer\Exception\RegisterWalletException;
use Linio\Frontend\Communication\Customer\Exception\UnregisteredWalletException;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Exception\OrderException;
use Linio\Frontend\Customer\Order\Invoice\InvoiceResult;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\BankTransferConfirmation;
use Linio\Frontend\Entity\Customer\Coupon;
use Linio\Frontend\Entity\Customer\CreditCard;
use Linio\Frontend\Entity\Customer\LinioPlusMembership;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;
use Linio\Frontend\Event\CustomerPasswordChangedEvent;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Security\Telesales\TelesalesAuthenticationToken;
use Linio\Frontend\Security\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CustomerService
{
    use CountryAware;

    /**
     * @var CustomerAdapterInterface
     */
    protected $adapter;

    /**
     * @var bool
     */
    protected $countryRequiresTaxId = false;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @param CustomerAdapterInterface $adapter
     */
    public function setAdapter(CustomerAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param bool $requiresId
     */
    public function setCountryRequiresId(bool $requiresId)
    {
        $this->countryRequiresTaxId = $requiresId;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerEmailExistsException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function create(Customer $customer)
    {
        // Subscribe to SMS Newsletter business rules, see https://linioit.atlassian.net/browse/ALICE-832
        if ($customer->isSubscribedToNewsletter() && $customer->getSmsPhone()) {
            $customer->setSubscribedToSmsNewsletter(true);
        }

        return $this->adapter->create($customer);
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function update(Customer $customer)
    {
        return $this->adapter->update($customer);
    }

    /**
     * @param TokenInterface $token
     *
     * @throws InvalidLoginCredentialsException Thrown for client errors
     * @throws InvalidLoginCredentialsException Thrown for server errors
     *
     * @return Customer
     */
    public function login(TokenInterface $token): Customer
    {
        return $this->adapter->login($token);
    }

    /**
     * @param TokenInterface $token
     *
     * @throws InvalidLoginCredentialsException Thrown for client errors
     * @throws InvalidLoginCredentialsException Thrown for server errors
     *
     * @return Customer
     */
    public function loginOAuth(TokenInterface $token): Customer
    {
        return $this->adapter->loginOAuth($token);
    }

    /**
     * @param TelesalesAuthenticationToken $token
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Customer
     */
    public function loginTelesales(TelesalesAuthenticationToken $token): Customer
    {
        return $this->adapter->loginTelesales($token);
    }

    /**
     * @param string $username
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return User
     */
    public function getUser($username): User
    {
        return $this->adapter->getUser($username);
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return CreditCard[]
     */
    public function getCreditCards(Customer $customer): array
    {
        return $this->adapter->getCreditCards($customer);
    }

    /**
     * @param CreditCard $creditCard
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function addCreditCard(CreditCard $creditCard, Customer $customer)
    {
        $this->adapter->addCreditCard($creditCard, $customer);
    }

    /**
     * @param int $creditCardId
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function removeCreditCard(int $creditCardId, Customer $customer)
    {
        $this->adapter->removeCreditCard($creditCardId, $customer);
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Coupon[]
     */
    public function getCoupons(Customer $customer): array
    {
        return $this->adapter->getCoupons($customer);
    }

    /**
     * @param string $email
     * @param string $url
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function requestPasswordReset(string $email, string $url)
    {
        $this->adapter->requestResetPassword($email, $url);
    }

    /**
     * @param string $resetToken
     * @param string $email
     * @param string $password
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function resetPassword(string $resetToken, string $email, string $password)
    {
        $this->adapter->resetPassword($resetToken, $email, $password);

        $customer = $this->adapter->getUser($email);

        $this->eventDispatcher->dispatch(
            CustomerPasswordChangedEvent::NAME,
            new CustomerPasswordChangedEvent($customer)
        );
    }

    /**
     * @param Customer $customer
     * @param string $currentPassword
     * @param string $newPassword
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function changePassword(Customer $customer, string $currentPassword, string $newPassword)
    {
        $this->adapter->changePassword($customer, $currentPassword, $newPassword);

        $this->eventDispatcher->dispatch(
            CustomerPasswordChangedEvent::NAME,
            new CustomerPasswordChangedEvent($customer)
        );
    }

    /**
     * @param string $orderId
     * @param Address $billingAddress
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return InvoiceResult
     */
    public function requestInvoice(string $orderId, Address $billingAddress): InvoiceResult
    {
        return $this->adapter->requestInvoice($orderId, $billingAddress);
    }

    /**
     * @param Customer $customer
     * @param Order $order
     * @param array $returnData
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     * @throws OrderException When only some of the requested quantity could be returned
     */
    public function createItemReturnRequest(Customer $customer, Order $order, array $returnData)
    {
        return $this->adapter->createItemReturnRequest($customer, $order, $returnData);
    }

    /**
     * @param Customer $customer
     * @param string $account
     * @param string $holderName
     * @param string $holderLastname
     *
     * @throws RegisterWalletException Thrown for client errors
     * @throws RegisterWalletException Thrown for server errors
     */
    public function registerWallet(Customer $customer, string $account, string $holderName, string $holderLastname)
    {
        $this->adapter->registerWallet($customer, $account, $holderName, $holderLastname);
        $customer->setHasWallet(true);
    }

    /**
     * @param Customer $customer
     *
     * @throws UnregisteredWalletException
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return Wallet
     */
    public function getWallet(Customer $customer): Wallet
    {
        return $this->adapter->getWallet($customer);
    }

    /**
     * @param Customer $customer
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     *
     * @return LinioPlusMembership
     */
    public function getLinioPlusMembership(Customer $customer): LinioPlusMembership
    {
        return $this->adapter->getLinioPlusMembershipDetails($customer->getId());
    }

    /**
     * @param BankTransferConfirmation $bankTransferConfirmation
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function createBankTransferConfirmation(BankTransferConfirmation $bankTransferConfirmation)
    {
        $this->adapter->createBankTransferConfirmation($bankTransferConfirmation);
    }

    /**
     * @param Customer $customer
     * @param SellerReview $review
     * @param Order $order
     *
     * @throws CustomerException Thrown for client errors
     * @throws CustomerException Thrown for server errors
     */
    public function reviewSeller(Customer $customer, SellerReview $review, Order $order)
    {
        $this->adapter->reviewSeller($customer, $review, $order);
    }

    /**
     * @param Customer $customer
     *
     * @return bool
     */
    public function isTaxInformationNeeded(Customer $customer): bool
    {
        return $this->countryRequiresTaxId && !($customer->getNationalRegistrationNumber() || $customer->getTaxIdentificationNumber());
    }
}
