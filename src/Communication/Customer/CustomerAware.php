<?php

namespace Linio\Frontend\Communication\Customer;

trait CustomerAware
{
    /**
     * @var CustomerService
     */
    protected $customerService;

    /**
     * @return CustomerService
     */
    public function getCustomerService()
    {
        return $this->customerService;
    }

    /**
     * @param CustomerService $customerService
     */
    public function setCustomerService(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }
}
