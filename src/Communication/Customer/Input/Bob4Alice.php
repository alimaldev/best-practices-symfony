<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Input;

use DateTime;
use Linio\Frontend\Customer\Order\Invoice\Invoice;
use Linio\Frontend\Customer\Order\Invoice\InvoiceResult;
use Linio\Frontend\Entity\Customer\Wallet\Points;
use Linio\Frontend\Entity\Customer\Wallet\Transaction;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;

class Bob4Alice implements CustomerInput
{
    const WALLET_TRANSACTION_TYPE_CHARGE = 'withdraw';
    const WALLET_TRANSACTION_TYPE_PAYMENT = 'deposit';
    const WALLET_INVALID_TRANSACTION_STATUSES = ['pending', 'reverse'];

    /**
     * @param array $walletData
     *
     * @return Wallet
     */
    public function toWallet(array $walletData): Wallet
    {
        $wallet = new Wallet();
        $wallet->setAccount($walletData['accountNumber']);
        $wallet->setAccruedPoints(Points::fromCents($walletData['totalDeposit'] ?? 0));
        $wallet->setUsedPoints(Points::fromCents($walletData['totalWithdraw'] ?? 0));
        $wallet->setExpiredPoints(Points::fromCents($walletData['totalExpired'] ?? 0));
        $wallet->setBalance(Points::fromCents($walletData['balance'] ?? 0));

        $currentBalance = new Points();

        foreach ($walletData['transactions'] as $transaction) {
            if (in_array($transaction['status'], self::WALLET_INVALID_TRANSACTION_STATUSES)) {
                continue;
            }

            $transaction = $this->mapTransaction($transaction, $currentBalance);
            $currentBalance = $transaction->getBalance();

            $wallet->addTransaction($transaction);
        }

        return $wallet;
    }
    /**
     * @param array $invoiceData
     *
     * @return InvoiceResult
     */
    public function toInvoiceResult(array $invoiceData): InvoiceResult
    {
        $invoiceResult = new InvoiceResult();

        foreach ($invoiceData as $folio => $folioData) {
            $invoice = new Invoice((string) $folio, base64_decode($folioData['xml']), $folioData['pdf']);
            $invoiceResult->addInvoice($invoice);
        }

        return $invoiceResult;
    }

    /**
     * @param array $transactionData
     * @param Points $currentBalance
     *
     * @return Transaction
     */
    protected function mapTransaction(array $transactionData, Points $currentBalance): Transaction
    {
        $charge = new Points();
        $payment = new Points();

        switch ($transactionData['type']) {
            case self::WALLET_TRANSACTION_TYPE_CHARGE:
                $charge = Points::fromCents($transactionData['amount']);
                $currentBalance = $currentBalance->subtract($charge);

                break;
            case self::WALLET_TRANSACTION_TYPE_PAYMENT:
                $payment = Points::fromCents($transactionData['amount']);
                $currentBalance = $currentBalance->add($payment);

                break;
        }

        return new Transaction(
            new DateTime($transactionData['date']),
            $charge,
            $payment,
            $transactionData['status'],
            $currentBalance,
            isset($transactionData['expirationDate']) ? new DateTime($transactionData['expirationDate']) : null
        );
    }
}
