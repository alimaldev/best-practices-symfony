<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Input;

use Linio\Frontend\Customer\Order\Invoice\InvoiceResult;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;

interface CustomerInput
{
    /**
     * @param array $walletData
     *
     * @return Wallet
     */
    public function toWallet(array $walletData): Wallet;

    /**
     * @param array $invoiceData
     *
     * @return InvoiceResult
     */
    public function toInvoiceResult(array $invoiceData): InvoiceResult;
}
