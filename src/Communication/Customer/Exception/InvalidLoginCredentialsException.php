<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Exception;

class InvalidLoginCredentialsException extends CustomerException
{
    const CUSTOMER_LOGIN_FAILURE = 'CUSTOMER_LOGIN_FAILURE';
}
