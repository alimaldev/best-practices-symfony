<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Exception;

use Linio\Exception\NotFoundHttpException as LinioNotFoundException;

class UnregisteredWalletException extends LinioNotFoundException
{
}
