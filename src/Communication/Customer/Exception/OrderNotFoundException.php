<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Exception;

use Linio\Frontend\Customer\Order\Exception\OrderException;

class OrderNotFoundException extends OrderException
{
}
