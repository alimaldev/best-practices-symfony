<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Exception\Order;

use Linio\Frontend\Exception\DomainException;

class InvoiceException extends DomainException
{
}
