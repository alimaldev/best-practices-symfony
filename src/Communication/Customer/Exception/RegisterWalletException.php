<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Customer\Exception;

class RegisterWalletException extends CustomerException
{
}
