<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Catalog\Exception;

use Linio\Frontend\Exception\DomainException;

class RatingSaveFailException extends DomainException
{
}
