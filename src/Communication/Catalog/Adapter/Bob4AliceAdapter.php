<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Catalog\Adapter;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Communication\Catalog\CatalogAdapterInterface;
use Linio\Frontend\Communication\Catalog\Exception\RatingSaveFailException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Request\PlatformInformation;
use Symfony\Component\HttpFoundation\RequestStack;

class Bob4AliceAdapter implements CatalogAdapterInterface
{
    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param PlatformInformation $platformInformation
     */
    public function setPlatformInformation(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param Customer $customer
     * @param Review $review
     *
     * @throws RatingSaveFailException Thrown for client errors
     * @throws RatingSaveFailException Thrown for server errors
     */
    public function setRating(Customer $customer, Review $review)
    {
        $requestBody = [
            'storeId' => $this->platformInformation->getStoreId(),
            'configSku' => $review->getSku(),
            'ratingOptionId' => $review->getRatingScore(),
            'title' => $review->getTitle(),
            'comment' => $review->getComment(),
            'customerId' => $customer->getId(),
            'customerName' => $review->getCustomerName(),
            'ipAddress' => $this->requestStack->getCurrentRequest()->getClientIp(),
        ];

        try {
            $this->client->request('POST', '/product/add-rating', [
                'json' => $requestBody,
            ]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new RatingSaveFailException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new RatingSaveFailException($responseBody['code']);
        }
    }
}
