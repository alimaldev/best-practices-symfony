<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Catalog;

use Linio\Frontend\Communication\Catalog\Exception\RatingSaveFailException;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Product\Review;

class CatalogService
{
    /**
     * @var CatalogAdapterInterface
     */
    protected $adapter;

    /**
     * @param CatalogAdapterInterface $adapter
     */
    public function setAdapter(CatalogAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param Customer $customer
     * @param Review $review
     *
     * @throws RatingSaveFailException Thrown for client errors
     * @throws RatingSaveFailException Thrown for server errors
     */
    public function setRating(Customer $customer, Review $review)
    {
        return $this->adapter->setRating($customer, $review);
    }
}
