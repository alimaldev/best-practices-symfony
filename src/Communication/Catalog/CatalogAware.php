<?php

declare(strict_types=1);

namespace Linio\Frontend\Communication\Catalog;

trait CatalogAware
{
    /**
     * @var CatalogService
     */
    protected $catalogService;

    /**
     * @return CatalogService
     */
    public function getCatalogService(): CatalogService
    {
        return $this->catalogService;
    }

    /**
     * @param CatalogService $catalogService
     */
    public function setCatalogService(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }
}
