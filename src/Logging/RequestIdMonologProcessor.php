<?php

declare(strict_types=1);

namespace Linio\Frontend\Logging;

use Linio\Frontend\Communication\Country\CountryAware;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestIdMonologProcessor
{
    use CountryAware;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function __invoke(array $record): array
    {
        // Ensure we have a request (maybe we're in a console command)
        if (!$this->requestStack->getCurrentRequest()) {
            $record['context']['requestId'] = '';

            return $record;
        }

        $record['context']['requestId'] = $this->requestStack->getMasterRequest()->headers->get('X-Request-ID');
        $record['context']['countryCode'] = $this->countryCode;

        return $record;
    }
}
