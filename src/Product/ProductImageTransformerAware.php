<?php

namespace Linio\Frontend\Product;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\Media\Image;
use Symfony\Component\HttpFoundation\RequestStack;

trait ProductImageTransformerAware
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param Image[] $productImages
     *
     * @return array
     */
    public function getImagesTransformed(array $productImages)
    {
        $images = [];

        foreach ($productImages as $index => $image) {
            $images[] = $this->getImageOutput($image);
        }

        return $images;
    }

    /**
     * @param Image[] $images
     *
     * @return array
     */
    public function transformMainImage(array $images): array
    {
        foreach ($images as $image) {
            if ($image->isMain()) {
                return $this->getImageOutput($image);
            }
        }

        return $this->getImageOutput(reset($images));
    }

    /**
     * @param Image $image
     *
     * @return array
     */
    protected function getImageOutput(Image $image): array
    {
        return [
            'url' => $this->getUrlFromImage($image, 'product'),
            'isMain' => $image->isMain(),
        ];
    }

    /**
     * @param Image $image
     * @param string $size
     *
     * @return string
     */
    public function getUrlFromImage(Image $image, $size)
    {
        $slug = ltrim($image->getSlug(), 'https://');

        return sprintf(
            '%s://%s-%s.jpg',
            $this->requestStack->getCurrentRequest()->getScheme(),
            $slug,
            $size
        );
    }
}
