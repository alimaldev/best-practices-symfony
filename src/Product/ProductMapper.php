<?php

declare(strict_types=1);

namespace Linio\Frontend\Product;

use DateTime;
use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Bundle;
use Linio\Frontend\Entity\Bundle\Product as BundleProduct;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Media\Image;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Rating\RatingAware;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\WishList\ManageWishList;
use Linio\Type\Money;

class ProductMapper implements MapperInterface
{
    use CategoryAware;
    use RatingAware;
    use CacheAware;
    use TokenStorageAware;

    /**
     * @var string
     */
    protected $imageHost;

    /**
     * @var ManageWishList
     */
    protected $manageWishList;

    /**
     * @param string $imageHost
     */
    public function setImageHost(string $imageHost)
    {
        $this->imageHost = $imageHost;
    }

    /**
     * @param ManageWishList $manageWishList
     */
    public function setManageWishList(ManageWishList $manageWishList)
    {
        $this->manageWishList = $manageWishList;
    }

    /**
     * @param array $data
     *
     * @return Product
     */
    public function map(array $data)
    {
        $product = new Product();
        $product->setSku($data['sku']);
        $product->setName($data['name']);
        $product->setSlug($data['slug']);
        $product->setStock($data['stock']);
        $product->setHasFreeShipping(isset($data['free_shipping']) ? $data['free_shipping'] : false);
        $product->setHasFreeStorePickup((bool) ($data['attributes']['free_pickup'] ?? false));
        $product->setLinioPlusLevel(isset($data['linio_plus_level']) ? $data['linio_plus_level'] : 0);
        $product->setDeliveryTime($data['delivery_time']);
        $product->setVariationType($data['variation_type']);
        $product->setFulfillmentType(isset($data['fulfillment_type']) ? $data['fulfillment_type'] : null);
        $product->setMarketplaceParentSku(isset($data['marketplace_parent_sku']) ? $data['marketplace_parent_sku'] : null);
        $product->setConfigId(isset($data['config_id']) ? $data['config_id'] : null);
        $product->setAttributeSet(isset($data['attribute_set']) ? $data['attribute_set'] : '');
        $product->setEanCode(isset($data['ean_code']) ? $data['ean_code'] : '');

        if (isset($data['seller']['international'])) {
            $product->setImported($data['seller']['international']);
        } else {
            $product->setImported($data['is_international'] ?? false);
        }

        $product->setIsOversized(isset($data['is_oversized']) ? $data['is_oversized'] : false);
        $product->setMasterAndChildrenImported($data['are_all_international'] ?? false);
        $product->setSupplierWarrantyMonths(isset($data['attributes']['supplier_warranty_months']) ? (int) $data['attributes']['supplier_warranty_months'] : 0);

        if (in_array($data['sku'], $this->manageWishList->getAllSkusInWishLists($this->getCustomer()))) {
            $product->addedToWishLists();
        }

        $brand = new Brand();
        $brand->setId($data['brand']['id']);
        $brand->setName($data['brand']['name']);
        $brand->setSlug($data['brand']['slug']);

        $product->setBrand($brand);

        $product->setDescription($data['description']);

        foreach ($data['attributes'] as $key => $value) {
            $product->addAttribute($key, $value);
        }

        $rating = $this->ratingService->getBySku($data['sku']);
        if ($rating) {
            $product->setRating($rating);
        }

        foreach ($data['images'] as $imageData) {
            $image = new Image();
            $image->setMain($imageData['main']);
            $image->setPosition($imageData['position']);
            $image->setSlug(sprintf('//%s%s', $this->imageHost, $imageData['slug']));
            $product->addImage($image);
        }

        if (count($data['categories']) > 0 && !empty(reset($data['categories']))) {
            $category = end($data['categories'][0]);
            $category = $this->getCategoryService()->getCategory($category['id']);
            $product->setCategory($category);
        } else {
            $category = $this->getCategoryService()->getCategory(CategoryService::ROOT_CATEGORY);
            $product->setCategory($category);
        }

        $seller = new Seller();
        $seller->setSellerId($data['seller']['id']);
        $seller->setName($data['seller']['name']);
        $seller->setSlug($data['seller']['slug']);
        $seller->setType($data['seller']['type']);
        $seller->setOperationType($data['seller']['operation_type']);
        $seller->setRating($data['seller']['rating']);

        $product->setSeller($seller);

        $configuration = $this->cacheService->get('fulfillment');
        $graceTime = (int) $configuration['shipment_routing_grace_window'];

        if (empty($data['marketplace_products'])) {
            foreach ($data['simples'] as $simpleData) {
                $specialFromDate = $simpleData['special_from_date'] ? new DateTime($simpleData['special_from_date']) : null;
                $specialToDate = $simpleData['special_to_date'] ? new DateTime($simpleData['special_to_date']) : null;
                $specialPrice = $simpleData['special_price'];
                $unitPrice = $simpleData['price'];
                $price = ProductHelper::getPrice($unitPrice, $specialPrice, $specialFromDate, $specialToDate);

                $simple = new Simple();
                $simple->setStock($simpleData['stock']);
                $simple->setSku($simpleData['sku']);
                $simple->setIsActive($simpleData['active']);
                $simple->setCreatedAt(new DateTime($simpleData['created_at']));
                $simple->setPrice(new Money($price));
                $simple->setOriginalPrice(($unitPrice != $specialPrice) ? new Money($unitPrice) : new Money());
                $simple->setLinioPlusLevel(isset($simpleData['linio_plus_level']) ? $simpleData['linio_plus_level'] : false);
                $simple->setHasFreeShipping(isset($simpleData['free_shipping']) ? (bool) $simpleData['free_shipping'] : false);
                $simple->setExpressInvoiceAvailable(isset($simpleData['express_invoice_enabled']) ? $simpleData['express_invoice_enabled'] : false);
                $simple->setTaxPercent(isset($simpleData['tax_percentage']) ? $simpleData['tax_percentage'] : 0.0);

                if (isset($simpleData['attributes']['variation']) && $simpleData['attributes']['variation'] != '...') {
                    $product->setHasVariation(true);
                }

                if (!empty($simpleData['attributes']['promise_delivery_badge'])) {
                    $product->setHasPromiseDelivery(true);
                }

                foreach ($simpleData['attributes'] as $key => $value) {
                    $simple->addAttribute($key, $value);
                }

                $minimumDeliveryTime = (int) $simple->getAttribute('min_delivery_time');
                $maximumDeliveryTime = (int) $simple->getAttribute('max_delivery_time');

                if ($graceTime > 0) {
                    $simple->setMinimumDeliveryDays($maximumDeliveryTime);
                    $simple->setMaximumDeliveryDays($maximumDeliveryTime + $graceTime);
                } else {
                    $simple->setMinimumDeliveryDays($minimumDeliveryTime);
                    $simple->setMaximumDeliveryDays($maximumDeliveryTime);
                }

                $product->addSimple($simple);
            }
        }

        foreach ($data['bundles'] as $bundleData) {
            $bundle = $this->createBundle($bundleData);

            if ($bundle->isValid()) {
                $product->addBundle($bundle);
            }
        }

        foreach ($data['grouped_products'] as $groupedData) {
            $groupedProduct = $this->createGroupedProduct($groupedData);
            $product->addGroupedProduct($groupedProduct);
        }

        foreach ($data['marketplace_products'] as $marketplaceData) {
            $marketplaceChild = $this->createMarketplaceChild($marketplaceData);

            $product->addMarketplaceChild($marketplaceChild);

            if (empty($product->getSimples())) {
                $marketplaceChild->setExpressInvoiceAvailable(isset($data['simples'][0]['express_invoice_enabled']) ? $data['simples'][0]['express_invoice_enabled'] : false);
                $marketplaceChild->setTaxPercent(isset($data['simples'][0]['tax_percentage']) ? $data['simples'][0]['express_invoice_enabled'] : 0.0);
                $product->addSimple($marketplaceChild);
            }
        }

        if ($product->isMarketPlace()) {
            $fistSimple = $product->getFirstSimple();
            $product->setSeller($fistSimple->getSeller());
            $product->setImported((bool) $fistSimple->isImported());
        }

        return $product;
    }

    /**
     * @param array $bundleData
     *
     * @return Bundle
     */
    protected function createBundle(array $bundleData)
    {
        $bundle = new Bundle();
        $bundle->setName($bundleData['name']);
        $bundle->setType($bundleData['type']);
        $bundle->setDiscount($bundleData['discount']);
        $bundle->setSavingPrice(new Money($bundleData['saving_price']));

        if ($bundleData['start_date']) {
            $bundle->setStartDate(new DateTime($bundleData['start_date']));
        }

        if ($bundleData['end_date']) {
            $bundle->setEndDate(new DateTime($bundleData['end_date']));
        }

        foreach ($bundleData['products'] as $data) {
            $specialFromDate = $data['special_from_date'] ? new DateTime($data['special_from_date']) : null;
            $specialToDate = $data['special_to_date'] ? new DateTime($data['special_to_date']) : null;
            $unitPrice = $data['price'];
            $specialPrice = $data['special_price'];
            $price = ProductHelper::getPrice($unitPrice, $specialPrice, $specialFromDate, $specialToDate);

            $product = new BundleProduct();
            $product->setSku($data['sku']);
            $product->setName($data['name']);
            $product->setPrice(new Money($price));
            $product->setOriginalPrice(($unitPrice != $specialPrice) ? new Money($unitPrice) : new Money());
            $bundle->addProduct($product);
        }

        return $bundle;
    }

    /**
     * @param array $groupedData
     *
     * @return Product
     */
    protected function createGroupedProduct(array $groupedData)
    {
        $product = new Product();
        $product->setSku($groupedData['sku']);
        $product->setName($groupedData['name']);
        $product->setSlug($groupedData['slug']);

        $image = new Image();
        $image->setMain($groupedData['image']['main']);
        $image->setPosition($groupedData['image']['position']);
        $image->setSlug($groupedData['image']['slug']);
        $image->setSprite($groupedData['image']['sprite']);
        $product->addImage($image);

        return $product;
    }

    /**
     * @param array $marketplaceData
     *
     * @return MarketplaceChild
     */
    protected function createMarketplaceChild(array $marketplaceData)
    {
        $seller = new Seller();
        $seller->setSellerId($marketplaceData['seller']['id']);
        $seller->setName($marketplaceData['seller']['name']);
        $seller->setSlug($marketplaceData['seller']['slug']);
        $seller->setType($marketplaceData['seller']['type']);
        $seller->setRating($marketplaceData['seller']['rating']);
        $seller->setOperationType($marketplaceData['seller']['operation_type']);

        $marketplaceChild = new MarketplaceChild();
        $marketplaceChild->setSku($marketplaceData['sku']);
        $marketplaceChild->setDeliveryTime($marketplaceData['delivery_time']);
        $marketplaceChild->setStock($marketplaceData['stock']);
        $marketplaceChild->setSeller($seller);
        $marketplaceChild->setLinioPlusLevel(isset($marketplaceData['linio_plus_level']) ? $marketplaceData['linio_plus_level'] : 0);
        $marketplaceChild->setHasFreeShipping(isset($marketplaceData['free_shipping']) ? (bool) $marketplaceData['free_shipping'] : false);
        $marketplaceChild->setConditionTypeNote(isset($marketplaceData['condition_type_note']) ? $marketplaceData['condition_type_note'] : null);
        $marketplaceChild->setWarranty(isset($marketplaceData['product_warranty']) ? $marketplaceData['product_warranty'] : null);
        $marketplaceChild->setPackageContent(isset($marketplaceData['package_content']) ? $marketplaceData['package_content'] : null);
        $marketplaceChild->setIsImported(isset($marketplaceData['is_international']) ? $marketplaceData['is_international'] : false);
        $marketplaceChild->setExpressInvoiceAvailable($marketplaceData['express_invoice_enabled'] ?? false);
        $marketplaceChild->setFulfillmentType($marketplaceData['fulfillment_type'] ?? null);

        $specialFromDate = isset($marketplaceData['special_from_date']) ? new DateTime($marketplaceData['special_from_date']) : null;
        $specialToDate = isset($marketplaceData['special_to_date']) ? new DateTime($marketplaceData['special_to_date']) : null;

        $marketPlacePrice = (float) $marketplaceData['price'];
        $marketPlaceSpecialPrice = (float) $marketplaceData['special_price'];
        $price = ProductHelper::getPrice($marketPlacePrice, $marketPlaceSpecialPrice, $specialFromDate, $specialToDate);
        $marketplaceChild->setPrice(new Money($price));
        $marketplaceChild->setOriginalPrice(($price != $marketPlaceSpecialPrice) ? new Money() : new Money($marketPlacePrice));

        return $marketplaceChild;
    }
}
