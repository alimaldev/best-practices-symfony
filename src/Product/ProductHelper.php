<?php

namespace Linio\Frontend\Product;

use DateTime;
use InvalidArgumentException;

class ProductHelper
{
    /**
     * @param float         $price
     * @param float         $specialPrice
     * @param DateTime|null $specialFromDate
     * @param DateTime|null $specialToDate
     *
     * @return float
     */
    public static function getPrice($price, $specialPrice, $specialFromDate, $specialToDate)
    {
        if ($specialFromDate && !$specialFromDate instanceof DateTime) {
            throw new InvalidArgumentException('$specialFromDate must be a \DateTime object.');
        }

        if ($specialToDate && !$specialToDate instanceof DateTime) {
            throw new InvalidArgumentException('$specialToDate must be a \DateTime object.');
        }

        if ($specialPrice && !$specialFromDate && !$specialToDate) {
            return $specialPrice;
        }

        if (!$specialPrice || !$specialFromDate) {
            return $price;
        }

        $today = new DateTime();
        $today->setTime(0, 0, 0);

        if ($specialFromDate !== null) {
            $specialFromDate->setTime(0, 0, 0);
        }

        if ($specialToDate !== null) {
            $specialToDate->setTime(0, 0, 0);
        }

        if ($specialToDate !== null && $specialFromDate <= $today && $specialToDate >= $today) {
            return $specialPrice;
        }

        if ($specialFromDate <= $today && $specialToDate === null) {
            return $specialPrice;
        }

        return $price;
    }
}
