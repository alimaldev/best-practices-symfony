<?php

namespace Linio\Frontend\Product;

trait ProductAware
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @codeCoverageIgnore
     *
     * @return ProductService
     */
    public function getProductService()
    {
        return $this->productService;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param ProductService $productService
     */
    public function setProductService(ProductService $productService)
    {
        $this->productService = $productService;
    }
}
