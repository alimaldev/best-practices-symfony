<?php

declare(strict_types=1);

namespace Linio\Frontend\Product\Exception;

class ProductNotFoundException extends ProductException
{
}
