<?php

declare(strict_types=1);

namespace Linio\Frontend\Product\Exception;

use Linio\Frontend\Exception\DomainException;

class ProductException extends DomainException
{
}
