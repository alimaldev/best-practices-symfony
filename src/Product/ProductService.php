<?php

declare(strict_types=1);

namespace Linio\Frontend\Product;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Product\Exception\ProductNotFoundException;

class ProductService
{
    use CacheAware;

    const CACHE_PREFIX = 'product:';

    /**
     * @var ProductMapper
     */
    protected $productMapper;

    /**
     * @var bool
     */
    protected $isDebugLogEnabled = false;

    /**
     * @var string
     */
    protected $debugLog = '';

    /**
     * @param string $skuConfig
     *
     * @throws ProductNotFoundException
     *
     * @return Product
     */
    public function getBySku(string $skuConfig): Product
    {
        $sku = $this->getSkuFromSimple($skuConfig);
        $product = $this->getContentFromCache(self::CACHE_PREFIX . $sku);

        if (!$product) {
            throw new ProductNotFoundException($skuConfig);
        }

        return $this->getProductMapper()->map($product);
    }

    /**
     * Only return products that are found in cache.
     * Do not throw exceptions for products not found.
     *
     * @param array $skusConfig
     *
     * @return array
     */
    public function getBySkus(array $skusConfig): array
    {
        if (empty($skusConfig)) {
            return [];
        }

        $productKeys = [];

        foreach ($skusConfig as $skuConfig) {
            $sku = $this->getSkuFromSimple($skuConfig);

            $productKeys[] = self::CACHE_PREFIX . $sku;
        }

        $productsData = $this->getMultipleContentFromCache($productKeys);

        if (empty($productsData)) {
            return [];
        }

        $products = [];

        foreach ($productsData as $productData) {
            $products[] = $this->getProductMapper()->map($productData);
        }

        return $products;
    }

    /**
     * @param Product $product
     * @param string $sellerSlug
     *
     * @throws ProductNotFoundException
     *
     * @return Product
     */
    public function getPlacedBySeller(Product $product, string $sellerSlug): Product
    {
        if ($this->isSameSeller($product->getSeller(), $sellerSlug)) {
            return $product;
        }

        foreach ($product->getMarketplaceChildren() as $marketplaceChild) {
            if ($this->isSameSeller($marketplaceChild->getSeller(), $sellerSlug)) {
                return $this->getBySku($marketplaceChild->getSku());
            }
        }

        if (!$product->getMarketplaceParentSku()) {
            return $product;
        }

        $master = $this->getBySku($product->getMarketplaceParentSku());

        if ($this->isSameSeller($master->getSeller(), $sellerSlug)) {
            return $master;
        }

        foreach ($master->getMarketplaceChildren() as $marketplaceChild) {
            if ($this->isSameSeller($marketplaceChild->getSeller(), $sellerSlug)) {
                return $this->getBySku($marketplaceChild->getSku());
            }
        }

        return $product;
    }

    /**
     * Get pure SKU, without simple id.
     *
     * @param string $simpleSku
     *
     * @return string
     */
    public function getSkuFromSimple(string $simpleSku): string
    {
        if (strpos($simpleSku, '-') !== false) {
            return explode('-', $simpleSku)[0];
        }

        return $simpleSku;
    }

    /**
     * @return ProductMapper
     */
    public function getProductMapper(): ProductMapper
    {
        return $this->productMapper;
    }

    /**
     * @param ProductMapper $productMapper
     */
    public function setProductMapper(ProductMapper $productMapper)
    {
        $this->productMapper = $productMapper;
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    private function getContentFromCache(string $key)
    {
        $content = $this->cacheService->get($key);

        if ($this->isDebugLogEnabled) {
            $this->debugLog .= "\n" . $key . ' = ' . var_export($content, true);
        }

        return $content;
    }

    /**
     * @param array $keys
     *
     * @return array
     */
    private function getMultipleContentFromCache(array $keys): array
    {
        $content = $this->cacheService->getMulti($keys);

        if ($this->isDebugLogEnabled) {
            foreach ($keys as $key) {
                if (isset($content[$key])) {
                    $this->debugLog .= "\n" . $key . ' = ' . var_export($content[$key], true);
                } else {
                    $this->debugLog .= "\n" . $key . ' = NOT FOUND';
                }
            }
        }

        return $content;
    }

    public function enableDebugLog()
    {
        $this->isDebugLogEnabled = true;
    }

    /**
     * @return string
     */
    public function getDebugLog(): string
    {
        return $this->debugLog;
    }

    /**
     * @param Seller|null $seller
     * @param string $slug
     *
     * @return bool
     */
    protected function isSameSeller($seller, string $slug): bool
    {
        return $seller && $seller->getSlug() === $slug;
    }
}
