<?php

declare(strict_types=1);

namespace Linio\Frontend\Api;

use Linio\Frontend\Api\Input\OrderInput;
use Linio\Frontend\Api\Output\OrderOutput;

trait OrderTransputAware
{
    /**
     * @var OrderInput
     */
    protected $orderInput;

    /**
     * @var OrderOutput
     */
    protected $orderOutput;

    /**
     * @param OrderInput $orderInput
     */
    public function setOrderInput(OrderInput $orderInput)
    {
        $this->orderInput = $orderInput;
    }

    /**
     * @param OrderOutput $orderOutput
     */
    public function setOrderOutput(OrderOutput $orderOutput)
    {
        $this->orderOutput = $orderOutput;
    }
}
