<?php

declare(strict_types=1);

namespace Linio\Frontend\Api;

use Linio\Frontend\Api\Output\CustomerOutput;

trait CustomerTransputAware
{
    /**
     * @var CustomerOutput
     */
    protected $customerOutput;

    /**
     * @param CustomerOutput $customerOutput
     */
    public function setCustomerOutput(CustomerOutput $customerOutput)
    {
        $this->customerOutput = $customerOutput;
    }
}
