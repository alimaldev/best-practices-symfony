<?php

declare(strict_types=1);

namespace Linio\Frontend\Api;

use Linio\Frontend\Api\Input\AddressInput;
use Linio\Frontend\Api\Output\AddressOutput;

trait AddressTransputAware
{
    /**
     * @var AddressInput
     */
    protected $addressInput;

    /**
     * @var AddressOutput
     */
    protected $addressOutput;

    /**
     * @param AddressInput $addressInput
     */
    public function setAddressInput(AddressInput $addressInput)
    {
        $this->addressInput = $addressInput;
    }

    /**
     * @param AddressOutput $addressOutput
     */
    public function setAddressOutput(AddressOutput $addressOutput)
    {
        $this->addressOutput = $addressOutput;
    }
}
