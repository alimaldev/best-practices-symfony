<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller;

use Linio\Controller\FormAware;
use Linio\Frontend\Form\PromotionalNewsletterForm;
use Linio\Frontend\Newsletter\Exception\NewsletterException;
use Linio\Frontend\Newsletter\ManageNewsletterAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/newsletter", service="controller.api.newsletter")
 */
class NewsletterController
{
    use ManageNewsletterAware;
    use FormAware;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @Route("/subscribe", methods={"POST","GET"}, name="frontend.api.newsletter.subscribe")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function subscribeAction(Request $request): JsonResponse
    {
        $source = $request->query->get('source', 'HomePage');
        $clientIp = $request->getClientIp();

        $form = $this->formFactory->create(PromotionalNewsletterForm::class, null, ['csrf_protection' => false]);
        $form->submit(array_merge($request->query->all(), $request->request->all()));

        if (!$form->isValid()) {
            // TODO: Fix the JS code so it works properly with exceptions
            // Then throw a FormValidationException with ExceptionMessage::NEWSLETTER_INVALID_EMAIL_OR_ALREADY_SUBSCRIBED
            return new JsonResponse(['status' => 'already_subscribed']);
        }

        try {
            $newsletterSubscription = $this->manageNewsletterSubscription->subscribe(
                $form->getData()['email'],
                $source,
                $clientIp
            );
        } catch (NewsletterException $exception) {
            return new JsonResponse(['status' => 'already_subscribed']);
        }

        return new JsonResponse([
            'status' => 'success',
            'email' => $newsletterSubscription->getEmail(),
            'coupon' => $newsletterSubscription->getCoupon(),
        ]);
    }
}
