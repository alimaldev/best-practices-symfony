<?php

namespace Linio\Frontend\Api\Controller;

use Linio\Component\Input\InputTrait;
use Linio\Component\Util\Json;
use Linio\Controller\FormAware;
use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Communication\Customer\Exception\CustomerException;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Frontend\Serializer\SerializerAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @Route("/customer", service="controller.api.customer")
 */
class CustomerController extends Controller
{
    use CustomerAware;
    use DynamicFormAware;
    use FormAware;
    use InputTrait;
    use SerializerAware;
    use TokenStorageAware;
    use SearchOrderAware;
    use AddressTransputAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route("/cards", name="frontend.api.customer.card.get")
     *
     * @return Response
     */
    public function getCardsAction()
    {
        $response = new JsonResponse();

        try {
            $cards = $this->customerService->getCreditCards($this->getCustomer());
        } catch (CustomerException $e) {
            $response->setData(['messages' => $e->getMessage()]);
            $response->setStatusCode($response::HTTP_BAD_REQUEST);

            return $response;
        }

        $response->setContent(Json::encode($cards));

        return $response;
    }

    /**
     * @Route("/tax-id", name="frontend.api.customer.register.tax_id", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setTaxIdAction(Request $request)
    {
        if (!$this->customerService->isTaxInformationNeeded($this->getCustomer())) {
            return new JsonResponse();
        }

        $form = $this->dynamicFormFactory->createForm('tax_id', $this->getCustomer(), ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            /** @var Customer $customer */
            $customer = $form->getData();
            $customer->setIpAddress($request->getClientIp());
            $this->customerService->update($customer);
            $this->updateCustomer($customer);

            return new JsonResponse();
        }

        $errors = [];
        /** @var FormError $error */
        foreach ($form->getErrors(true, true) as $error) {
            $errors[$error->getOrigin()->getName()] = $error->getMessage();
        }

        return new JsonResponse($errors, 400);
    }

    /**
     * @Route("/request-invoice", name="frontend.api.customer.request_invoice", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function requestInvoiceAction(Request $request)
    {
        $billingAddress = $this->addressInput->fromPartialAddress($request->request->get('customerAddress'));

        $orderId = $request->request->get('orderId');

        $invoiceResult = $this->customerService->requestInvoice($orderId, $billingAddress);
        $this->getCustomer()->setInvoices($invoiceResult->getInvoices());

        return new JsonResponse($invoiceResult->getFolios());
    }
}
