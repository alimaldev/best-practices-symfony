<?php

namespace Linio\Frontend\Api\Controller;

use Linio\Frontend\Rating\RatingAware;
use Linio\Frontend\Serializer\SerializerAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/", service="controller.api.catalog")
 */
class CatalogController
{
    use RatingAware;
    use SerializerAware;

    /**
     * @Route("/product/{sku}/rating", methods={"GET"}, name="frontend.api.catalog.product.rating")
     *
     * @param string $sku
     *
     * @return JsonResponse
     */
    public function getProductRatingAction($sku)
    {
        $response = new JsonResponse();

        $productRatings = $this->ratingService->getBySku($sku);
        $response->setContent($this->serializer->serialize($productRatings, 'json'));

        return $response;
    }
}
