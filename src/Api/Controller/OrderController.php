<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller;

use Linio\Controller\SessionAware;
use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Api\OrderTransputAware;
use Linio\Frontend\Customer\AddressBookAware;
use Linio\Frontend\Order\BuildOrderAware;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\PlaceOrderAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/order", service="controller.api.order")
 */
class OrderController
{
    use SessionAware;
    use AddressBookAware;
    use TokenStorageAware;
    use BuildOrderAware;
    use PlaceOrderAware;
    use AddressTransputAware;
    use OrderTransputAware;

    /**
     * @Route(methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getOrderInProgressAction(Request $request): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->recalculateOrder($order);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/stats", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getOrderStatsAction(Request $request): JsonResponse
    {
        $order = $this->buildOrder->getOrderInProgress($this->getCustomer(), false);

        return new JsonResponse(['count' => count($order->getItems())]);
    }

    /**
     * @Route("/item/{sku}", methods={"DELETE"})
     *
     * @param Request $request
     * @param string $sku
     *
     * @return JsonResponse
     */
    public function removeItemAction(Request $request, string $sku): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->removeProduct($order, new Item($sku));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/item/{sku}", methods={"PUT"})
     *
     * @param Request $request
     * @param string $sku
     *
     * @return JsonResponse
     */
    public function updateQuantityAction(Request $request, string $sku): JsonResponse
    {
        $item = new Item($sku, $request->request->get('quantity'));
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->updateQuantity($order, $item);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/coupon/{code}", methods={"PUT"})
     *
     * @param Request $request
     * @param string $code
     *
     * @return JsonResponse
     */
    public function applyCouponAction(Request $request, string $code): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->applyCoupon($order, new Coupon($code));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/coupon", methods={"DELETE"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeCouponAction(Request $request): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->removeCoupon($order);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/shipping-address", methods={"PUT"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setShippingAddressAction(Request $request): JsonResponse
    {
        $shippingAddress = $this->addressInput->fromLocationParts($request->request->get('address'));
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setShippingAddress($order, $shippingAddress);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/shipping-address/{shippingAddressId}", methods={"PUT"}, requirements={"shippingAddressId":"\d+"})
     *
     * @param Request $request
     * @param int $shippingAddressId
     *
     * @return JsonResponse
     */
    public function setShippingAddressByIdAction(Request $request, int $shippingAddressId): JsonResponse
    {
        $customer = $this->getCustomer();
        $shippingAddress = $this->addressBook->getAddress($customer, $shippingAddressId);

        if (!$shippingAddress) {
            return new JsonResponse([], JsonResponse::HTTP_NOT_FOUND);
        }

        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setShippingAddress($order, $shippingAddress);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/package/{packageId}", methods={"PUT"}, requirements={"packageId":"\d+"})
     *
     * @param Request $request
     * @param int $packageId
     *
     * @return JsonResponse
     */
    public function updateShippingMethodAction(Request $request, int $packageId): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->updateShippingMethod($order, $packageId, $request->request->get('shippingMethod'));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/payment-method/{paymentMethodName}", methods={"PUT"})
     *
     * @param Request $request
     * @param string $paymentMethodName
     *
     * @return JsonResponse
     */
    public function updatePaymentMethodAction(Request $request, string $paymentMethodName): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->updatePaymentMethod($order, PaymentMethodFactory::fromName($paymentMethodName));

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/pickup-store/{pickupStoreId}", methods={"PUT"}, requirements={"pickupStoreId":"\d+"})
     *
     * @param Request $request
     * @param int $pickupStoreId
     *
     * @return JsonResponse
     */
    public function setPickupStoreAction(Request $request, int $pickupStoreId): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setPickupStore($order, $pickupStoreId);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/geohash/{geohash}", methods={"PUT"})
     *
     * @param Request $request
     * @param string $geohash
     *
     * @return JsonResponse
     */
    public function setGeohash(Request $request, string $geohash): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setGeohash($order, $geohash);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/bin/{binNumber}", methods={"PUT"})
     *
     * @param Request $request
     * @param string $binNumber
     *
     * @return JsonResponse
     */
    public function setBinNumberAction(Request $request, string $binNumber): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->setBinNumber($order, $binNumber);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/installment/{quantity}", methods={"PUT"}, requirements={"quantity":"\d+"})
     *
     * @param Request $request
     * @param int $quantity
     *
     * @return JsonResponse
     */
    public function selectInstallmentQuantityAction(Request $request, int $quantity): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->selectInstallmentQuantity($order, $quantity);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/wallet/{points}", methods={"PUT"}, requirements={"points":"\d+"})
     *
     * @param Request $request
     * @param int $points
     *
     * @return JsonResponse
     */
    public function useWalletPointsAction(Request $request, int $points): JsonResponse
    {
        $order = $this->buildOrder->mergeOrderWithCache($this->getCustomer(), $request->request->get('order'));
        $recalculatedOrder = $this->buildOrder->useWalletPoints($order, $points);

        return new JsonResponse($this->orderOutput->fromRecalculatedOrder($recalculatedOrder));
    }

    /**
     * @Route("/place", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function placeOrderAction(Request $request): JsonResponse
    {
        $orderInProgress = $this->buildOrder->getOrderInProgress($this->getCustomer(), false);
        $order = $this->orderInput->fromPlaceOrder($request->request->all(), $orderInProgress);
        $order->setIpAddress($request->getClientIp());
        $completedOrder = $this->placeOrder->placeOrder($order);
        $this->session->set('completed_order', $completedOrder->getOrderNumber());
        $this->session->set('completed_order_id', $completedOrder->getId());

        // TODO: Remove this hacky workaround when the Datalayer is rewritten
        $this->session->set('completed_order_object', serialize($completedOrder));

        return new JsonResponse($this->orderOutput->fromCompletedOrder($completedOrder));
    }
}
