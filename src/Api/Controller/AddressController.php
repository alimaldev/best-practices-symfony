<?php

namespace Linio\Frontend\Api\Controller;

use Linio\Frontend\Api\Response\ErrorJsonResponse;
use Linio\Frontend\Location\ResolveLocationAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/address", service="controller.api.address")
 */
class AddressController
{
    use ResolveLocationAware;

    /**
     * @Route("/postcode", name="frontend.api.address.postcode")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postcodeAction(Request $request)
    {
        $addressData = $this->resolveLocation->resolvePostcode($request->query->get('postcode'));

        if (!$addressData) {
            return new ErrorJsonResponse('', JsonResponse::HTTP_NOT_FOUND);
        }

        return new JsonResponse($addressData);
    }

    /**
     * @Route("/regions", name="frontend.api.address.regions")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function regionsAction(Request $request)
    {
        $regions = $this->resolveLocation->getRegions($request->query);

        return new JsonResponse($regions);
    }

    /**
     * @Route("/municipalities", name="frontend.api.address.municipalities")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function municipalitiesAction(Request $request)
    {
        $municipalities = $this->resolveLocation->getMunicipalities($request->query);

        return new JsonResponse($municipalities);
    }

    /**
     * @Route("/cities", name="frontend.api.address.cities")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function citiesAction(Request $request)
    {
        $cities = $this->resolveLocation->getCities($request->query);

        return new JsonResponse($cities);
    }

    /**
     * @Route("/neighborhoods", name="frontend.api.address.neighborhoods")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function neighborhoodsAction(Request $request)
    {
        $neighborhoods = $this->resolveLocation->getNeighborhoods($request->query);

        return new JsonResponse($neighborhoods);
    }
}
