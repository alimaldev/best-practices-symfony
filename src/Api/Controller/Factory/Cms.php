<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\CmsController;

class Cms extends Base
{
    /**
     * @return CmsController
     */
    public function create()
    {
        return new CmsController(
            $this->serviceContainer->get('cms.service'),
            $this->serviceContainer->get('request.platform_information')
        );
    }
}
