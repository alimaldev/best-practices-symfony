<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\CustomerController;

class Customer extends Base
{
    /**
     * @return CustomerController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $customerOutput = $this->serviceContainer->get('output.api.mobile.customer');
                $customerInput = $this->serviceContainer->get('input.api.customer');
                $customerOrderOutput = $this->serviceContainer->get('output.api.mobile.customer.order');
                break;
        }

        $translator = $this->serviceContainer->get('translator');
        $customerService = $this->serviceContainer->get('communication.customer.service');
        $sellerService = $this->serviceContainer->get('seller.service');
        $authenticationSuccessHandler = $this->serviceContainer->get('lexik_jwt_authentication.handler.authentication_success');
        $dynamicFormFactory = $this->serviceContainer->get('dynamic_form.factory');
        $tokenStorage = $this->serviceContainer->get('security.token_storage');
        $formFactory = $this->serviceContainer->get('form.factory');
        $buildOrder = $this->serviceContainer->get('order.build');
        $linioEnabled = $this->serviceContainer->getParameter('linio_plus_enabled');
        $sellerReviewsEnabled = $this->serviceContainer->getParameter('seller_reviews_enabled');
        $bankTransferConfirmationFormEnabled = $this->serviceContainer->getParameter('bank_transfer_confirmation_form_enabled');
        $searchOrder = $this->serviceContainer->get('customer.order.search');

        $controller = new CustomerController();
        $controller->setTranslator($translator);
        $controller->setCustomerService($customerService);
        $controller->setAuthenticationSuccessHandler($authenticationSuccessHandler);
        $controller->setDynamicFormFactory($dynamicFormFactory);
        $controller->setTokenStorage($tokenStorage);
        $controller->setFormFactory($formFactory);
        $controller->setBuildOrder($buildOrder);
        $controller->setCustomerOutput($customerOutput);
        $controller->setLinioPlusEnabled($linioEnabled);
        $controller->setSellerReviewsEnabled($sellerReviewsEnabled);
        $controller->setSellerService($sellerService);
        $controller->setBankTransferConfirmationFormEnabled($bankTransferConfirmationFormEnabled);
        $controller->setCustomerInput($customerInput);
        $controller->setSearchOrder($searchOrder);
        $controller->setCustomerOrderOutput($customerOrderOutput);

        return $controller;
    }
}
