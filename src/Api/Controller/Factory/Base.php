<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class Base
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var ContainerInterface
     */
    protected $serviceContainer;

    /**
     * @param ContainerInterface $serviceContainer
     */
    public function setServiceContainer(ContainerInterface $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
    }

    /**
     * @param RequestStack $requestStack
     */
    public function setRequestStack(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    abstract public function create();

    /**
     * @return int
     */
    protected function getVersion(): int
    {
        $currentRequest = $this->requestStack->getCurrentRequest();

        if ($currentRequest->headers->has('X-Version')) {
            return (int) $currentRequest->headers->get('X-Version');
        }

        $uri = $currentRequest->getRequestUri();

        if (strpos($uri, '/mapi/v1/') !== false) {
            return 1;
        }

        return 0;
    }
}
