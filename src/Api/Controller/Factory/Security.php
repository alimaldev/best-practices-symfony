<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\SecurityController;

class Security extends Base
{
    /**
     * @return SecurityController
     */
    public function create()
    {
        $translator = $this->serviceContainer->get('translator');
        $tokenManager = $this->serviceContainer->get('security.auth.token_manager');
        $customerService = $this->serviceContainer->get('communication.customer.service');
        $router = $this->serviceContainer->get('router');
        $formFactory = $this->serviceContainer->get('form.factory');

        $controller = new SecurityController();
        $controller->setTranslator($translator);
        $controller->setTokenManager($tokenManager);
        $controller->setCustomerService($customerService);
        $controller->setRouter($router);
        $controller->setFormFactory($formFactory);

        return $controller;
    }
}
