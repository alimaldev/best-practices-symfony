<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\NewsletterController;

class Newsletter extends Base
{
    /**
     * @return NewsletterController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $newsletterOutput = $this->serviceContainer->get('output.api.mobile.newsletter');
                break;
        }

        $manageNewsletterSubscription = $this->serviceContainer->get('newsletter.manage');

        $controller = new NewsletterController();
        $controller->setNewsletterOutput($newsletterOutput);
        $controller->setManageNewsletterSubscription($manageNewsletterSubscription);

        return $controller;
    }
}
