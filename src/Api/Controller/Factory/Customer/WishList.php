<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\WishListController;

class WishList extends Base
{
    /**
     * @return WishListController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $wishListInput = $this->serviceContainer->get('input.api.mobile.wish_list');
                $wishListOutput = $this->serviceContainer->get('output.api.mobile.wish_list');
                $wishListProductInput = $this->serviceContainer->get('input.api.mobile.wish_list_product');
                break;
        }

        $controller = new WishListController(
            $this->serviceContainer->get('wish_list.manage'),
            $wishListInput,
            $wishListOutput
        );

        $controller->setWishListProductInput($wishListProductInput);
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));

        return $controller;
    }
}
