<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\LoyaltyController;

class Loyalty extends Base
{
    /**
     * @return LoyaltyController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $customerOrderOutput = $this->serviceContainer->get('output.api.mobile.customer.order');
                $loyaltyInput = $this->serviceContainer->get('input.api.mobile.loyalty');
                $customerOutput = $this->serviceContainer->get('output.api.mobile.customer');
                break;
        }

        $controller = new LoyaltyController();
        $controller->setCustomerOrderOutput($customerOrderOutput);
        $controller->setCustomerOutput($customerOutput);
        $controller->setLoyaltyInput($loyaltyInput);
        $controller->setManageMemberships($this->serviceContainer->get('customer.memberships.manage'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));

        return $controller;
    }
}
