<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\WalletController;

class Wallet extends Base
{
    /**
     * @return WalletController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $walletOutput = $this->serviceContainer->get('output.api.mobile.customer.wallet');
                break;
        }

        $controller = new WalletController();
        $controller->setWalletEnabled($this->serviceContainer->getParameter('wallet_enabled'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setCustomerService($this->serviceContainer->get('communication.customer.service'));
        $controller->setWalletOutput($walletOutput);

        return $controller;
    }
}
