<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\NotificationsController;

class Notifications extends Base
{
    /**
     * @return NotificationsController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $newsletterInput = $this->serviceContainer->get('input.api.newsletter');
                $newsletterOutput = $this->serviceContainer->get('output.api.mobile.newsletter');
                break;
        }

        $controller = new NotificationsController($this->serviceContainer->get('translator'));
        $controller->setManageNewsletterSubscription($this->serviceContainer->get('newsletter.manage'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setNewsletterInput($newsletterInput);
        $controller->setNewsletterOutput($newsletterOutput);

        return $controller;
    }
}
