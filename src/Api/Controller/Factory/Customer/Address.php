<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\AddressController;

class Address extends Base
{
    /**
     * @return AddressController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $addressInput = $this->serviceContainer->get('input.api.mobile.address');
                $addressOutput = $this->serviceContainer->get('output.api.mobile.address');
                break;
        }

        $controller = new AddressController();
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setDynamicFormFactory($this->serviceContainer->get('dynamic_form.factory'));
        $controller->setAddressBook($this->serviceContainer->get('customer.address.book'));
        $controller->setTranslator($this->serviceContainer->get('translator'));
        $controller->setAddressInput($addressInput);
        $controller->setAddressOutput($addressOutput);

        return $controller;
    }
}
