<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\CouponsController;

class Coupons extends Base
{
    /**
     * @return CouponsController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $couponOutput = $this->serviceContainer->get('output.api.mobile.customer.coupon');
                break;
        }

        $controller = new CouponsController();
        $controller->setCustomerService($this->serviceContainer->get('communication.customer.service'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setCustomerCouponOutput($couponOutput);

        return $controller;
    }
}
