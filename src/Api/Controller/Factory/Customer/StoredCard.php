<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\StoredCardController;

class StoredCard extends Base
{
    /**
     * @return StoredCardController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $storedCardInput = $this->serviceContainer->get('input.api.mobile.stored_credit_card');
                $storedCardOutput = $this->serviceContainer->get('output.api.mobile.stored_credit_card');
                break;
        }

        $controller = new StoredCardController();
        $controller->setStoredCreditCardInput($storedCardInput);
        $controller->setStoredCreditCardOutput($storedCardOutput);
        $controller->setStoreCards($this->serviceContainer->getParameter('store_cards'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setCustomerService($this->serviceContainer->get('communication.customer.service'));

        return $controller;
    }
}
