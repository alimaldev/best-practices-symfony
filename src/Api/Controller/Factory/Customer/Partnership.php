<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\PartnershipController;

class Partnership extends Base
{
    /**
     * @return PartnershipController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $partnershipInput = $this->serviceContainer->get('input.api.mobile.partnership');
                $partnershipOutput = $this->serviceContainer->get('output.api.mobile.partnership');
                break;
        }

        $controller = new PartnershipController(
            $partnershipInput,
            $this->serviceContainer->get('customer.memberships.manage'),
            $partnershipOutput
        );

        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));

        return $controller;
    }
}
