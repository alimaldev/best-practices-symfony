<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory\Customer;

use Linio\Frontend\Api\Controller\Factory\Base;
use Linio\Frontend\Controller\Mobile\Customer\OrderController;

class Order extends Base
{
    /**
     * @return OrderController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
                $customerOrderOutput = $this->serviceContainer->get('output.api.mobile.customer.order');
                $addressInput = $this->serviceContainer->get('input.api.mobile.address');
                $itemReturnInput = $this->serviceContainer->get('input.api.mobile.item.return');
                $customerOrderSearchInput = $this->serviceContainer->get('input.api.customer.order');
                break;
            default:
                $addressInput = $this->serviceContainer->get('input.api.mobile.address');
                $itemReturnInput = $this->serviceContainer->get('input.api.mobile.item.return');
                $customerOrderSearchInput = $this->serviceContainer->get('input.api.customer.order');
                $customerOrderOutput = $this->serviceContainer->get('output.api.customer.order');
                break;
        }

        $controller = new OrderController();

        $controller->setReturnItemEnabled($this->serviceContainer->getParameter('return_item_enabled'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setCustomerService($this->serviceContainer->get('communication.customer.service'));
        $controller->setSearchOrder($this->serviceContainer->get('customer.order.search'));
        $controller->setCustomerOrderSearchInput($customerOrderSearchInput);
        $controller->setCustomerOrderOutput($customerOrderOutput);
        $controller->setAddressInput($addressInput);
        $controller->setItemReturnInput($itemReturnInput);

        return $controller;
    }
}
