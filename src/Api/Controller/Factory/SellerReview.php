<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\Customer\SellerReviewController;

class SellerReview extends Base
{
    /**
     * @return SellerReviewController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
                $sellerInput = $this->serviceContainer->get('input.api.mobile.seller');
                break;
            default:
                $sellerInput = $this->serviceContainer->get('input.api.seller');
                break;
        }

        $controller = new SellerReviewController(
            $this->serviceContainer->get('customer.order.search'),
            $sellerInput
        );

        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setCustomerService($this->serviceContainer->get('communication.customer.service'));

        return $controller;
    }
}
