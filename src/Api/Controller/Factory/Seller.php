<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\SellerController;

class Seller extends Base
{
    /**
     * @return SellerController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $sellerOutput = $this->serviceContainer->get('output.api.mobile.seller');
                break;
        }

        $controller = new SellerController(
            $this->serviceContainer->get('seller.service'),
            $sellerOutput
        );

        return $controller;
    }
}
