<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\AddressController;

class Address extends Base
{
    /**
     * @return AddressController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $translator = $this->serviceContainer->get('translator');
                $locationResolve = $this->serviceContainer->get('location.resolve');
                break;
        }

        $controller = new AddressController($translator);
        $controller->setResolveLocation($locationResolve);

        return $controller;
    }
}
