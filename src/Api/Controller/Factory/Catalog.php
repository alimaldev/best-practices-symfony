<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\CatalogController;

class Catalog extends Base
{
    /**
     * @return CatalogController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
                $catalogResultOutput = $this->serviceContainer->get('output.api.mobile.catalog.result.v2');
                $categoryTreeOutput = $this->serviceContainer->get('output.api.mobile.category_tree');
                $productOutput = $this->serviceContainer->get('output.api.mobile.product_detail');
                $sellerOutput = $this->serviceContainer->get('output.api.mobile.seller');
                break;
            case 1:
            default:
                $catalogResultOutput = $this->serviceContainer->get('output.api.mobile.catalog.result.v1');
                $categoryTreeOutput = $this->serviceContainer->get('output.api.mobile.category_tree');
                $productOutput = $this->serviceContainer->get('output.api.mobile.product_detail');
                $sellerOutput = $this->serviceContainer->get('output.api.mobile.seller');
                break;
        }

        $controller = new CatalogController();
        $controller->setSearchService($this->serviceContainer->get('search.service'));
        $controller->setCategoryService($this->serviceContainer->get('category.service'));
        $controller->setCatalogResultOutput($catalogResultOutput);
        $controller->setSlugResolverService($this->serviceContainer->get('slug.resolver.service'));
        $controller->setProductService($this->serviceContainer->get('product.service'));
        $controller->setCampaignVoucherService($this->serviceContainer->get('campaign.voucher.service'));
        $controller->setTranslator($this->serviceContainer->get('translator'));
        $controller->setRouter($this->serviceContainer->get('router'));
        $controller->setFormFactory($this->serviceContainer->get('form.factory'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setCatalogService($this->serviceContainer->get('communication.catalog.service'));
        $controller->setSellerService($this->serviceContainer->get('seller.service'));
        $controller->setCategoryTreeOutput($categoryTreeOutput);
        $controller->setProductOutput($productOutput);
        $controller->setCmsService($this->serviceContainer->get('cms.service'));
        $controller->setSellerOutput($sellerOutput);

        return $controller;
    }
}
