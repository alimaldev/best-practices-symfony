<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\OrderController;

class Order extends Base
{
    /**
     * @return OrderController
     */
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
                $addressInput = $this->serviceContainer->get('input.api.mobile.address');
                $orderInput = $this->serviceContainer->get('input.api.order');
                $orderOutput = $this->serviceContainer->get('output.api.mobile.order');
                break;
            default:
                $addressInput = $this->serviceContainer->get('input.api.address');
                $orderInput = $this->serviceContainer->get('input.api.order');
                $orderOutput = $this->serviceContainer->get('output.api.order');
                break;
        }

        $controller = new OrderController();
        $controller->setDynamicFormFactory($this->serviceContainer->get('dynamic_form.factory'));
        $controller->setTokenStorage($this->serviceContainer->get('security.token_storage'));
        $controller->setAddressBook($this->serviceContainer->get('customer.address.book'));
        $controller->setBuildOrder($this->serviceContainer->get('order.build'));
        $controller->setPlaceOrder($this->serviceContainer->get('order.place'));
        $controller->setSession($this->serviceContainer->get('session'));
        $controller->setAddressInput($addressInput);
        $controller->setOrderOutput($orderOutput);
        $controller->setOrderInput($orderInput);

        return $controller;
    }
}
