<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Factory;

use Linio\Frontend\Controller\Mobile\DefaultController;

class Home extends Base
{
    public function create()
    {
        $version = $this->getVersion();

        switch ($version) {
            case 2:
            case 1:
            default:
                $mobileConfigurationOutput = $this->serviceContainer->get('output.api.mobile.configuration');
                break;
        }

        $controller = new DefaultController();
        $controller->setCountryCode($this->serviceContainer->getParameter('country_code'));
        $controller->setItemReturnService($this->serviceContainer->get('communication.customer.item_return.service'));
        $controller->setMobileConfigurationOutput($mobileConfigurationOutput);
        $controller->setStoreService($this->serviceContainer->get('store.service'));

        return $controller;
    }
}
