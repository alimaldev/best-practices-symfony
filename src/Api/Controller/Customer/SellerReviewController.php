<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Customer;

use Linio\Frontend\Api\Input\SellerInput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/customer", service="controller.api.customer.seller_reviews")
 */
class SellerReviewController
{
    use CustomerAware;
    use TokenStorageAware;
    use SearchOrderAware;

    /**
     * @var SellerInput
     */
    protected $input;

    /**
     * @var bool
     */
    protected $sellerReviewsEnabled = false;

    /**
     * @param SellerInput $input
     */
    public function setInput(SellerInput $input)
    {
        $this->input = $input;
    }

    /**
     * @param bool $sellerReviewsEnabled
     */
    public function setSellerReviewsEnabled(bool $sellerReviewsEnabled)
    {
        $this->sellerReviewsEnabled = $sellerReviewsEnabled;
    }

    /**
     * @Route("/review-seller", name="frontend.api.customer.review.seller", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function reviewAction(Request $request): JsonResponse
    {
        if (!$this->sellerReviewsEnabled) {
            throw new NotFoundHttpException();
        }

        $customer = $this->getCustomer();
        $review = $this->input->toSellerReview($request->request->all());
        $order = $this->searchOrder->findOrder($customer, $request->request->get('orderNumber'));

        $this->customerService->reviewSeller($customer, $review, $order);

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}
