<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Customer;

use Linio\Controller\FormAware;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Customer\Membership\ManageMembershipsAware;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\Customer\LoyaltyProgramForm;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/customer/loyalty", service="controller.api.customer.loyalty")
 */
class LoyaltyController
{
    use ManageMembershipsAware;
    use FormAware;
    use TokenStorageAware;

    /**
     * @var string
     */
    protected $loyaltyProgram;

    public function setLoyaltyProgram(string $loyaltyProgram)
    {
        $this->loyaltyProgram = $loyaltyProgram;
    }

    /**
     * @Route("/signup", name="frontend.api.customer.loyalty.signup", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function signupLoyaltyAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(LoyaltyProgramForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $formData = $form->getData();
        $loyaltyProgram = new LoyaltyProgram();
        $loyaltyProgram->setName($this->loyaltyProgram);
        $loyaltyProgram->setId($formData['loyaltyId']);
        $this->manageMemberships->signupForLoyaltyProgram($this->getCustomer(), $loyaltyProgram);

        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/update", name="frontend.api.customer.loyalty.update", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function updatePartnershipAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(LoyaltyProgramForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $formData = $form->getData();
        $loyaltyProgram = new LoyaltyProgram();
        $loyaltyProgram->setName($this->loyaltyProgram);
        $loyaltyProgram->setId($formData['loyaltyId']);
        $this->manageMemberships->updateLoyaltyProgram($this->getCustomer(), $loyaltyProgram);

        return new JsonResponse(null, 204);
    }
}
