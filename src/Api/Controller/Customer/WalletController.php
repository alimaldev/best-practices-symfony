<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Customer;

use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/customer/wallet", service="controller.api.customer.wallet")
 */
class WalletController
{
    use CustomerAware;
    use TokenStorageAware;

    /**
     * @Route("/register", name="frontend.api.customer.register.wallet", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws InputException
     *
     * @return JsonResponse
     */
    public function registerAction(Request $request): JsonResponse
    {
        if (!$request->request->has('account')) {
            throw new InputException('electronic_wallet.error.invalid_account_number');
        }

        $this->customerService->registerWallet(
            $this->getCustomer(),
            $request->request->get('account'),
            $request->request->get('holderName'),
            $request->request->get('holderLastname')
        );

        return new JsonResponse(null, JsonResponse::HTTP_CREATED);
    }
}
