<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Customer;

use Linio\Controller\FormAware;
use Linio\Frontend\Customer\Membership\ManageMembershipsAware;
use Linio\Frontend\Customer\Membership\Partnership;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\PartnershipForm;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/customer/partnership", service="controller.api.customer.partnership")
 */
class PartnershipController
{
    use ManageMembershipsAware;
    use FormAware;
    use TokenStorageAware;

    /**
     * @Route("/signup", name="frontend.api.customer.partnership.signup", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function signupPartnershipAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(PartnershipForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $formData = $form->getData();

        $partnership = new Partnership($formData['code'], $formData['accountNumber']);

        if (isset($formData['level'])) {
            $partnership->setLevel($formData['level']);
        }

        $this->manageMemberships->signupForPartnership($this->getCustomer(), $partnership);

        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/update", name="frontend.api.customer.partnership.update", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function updatePartnershipAction(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(PartnershipForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $formData = $form->getData();

        $partnership = new Partnership($formData['code'], $formData['accountNumber']);

        if (isset($formData['level'])) {
            $partnership->setLevel($formData['level']);
        }

        $this->manageMemberships->updatePartnership($this->getCustomer(), $partnership);

        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/{partnershipCode}/remove", name="frontend.api.customer.partnership.remove", methods={"POST"})
     *
     * @param string $partnershipCode
     *
     * @throws InputException
     * @throws DomainException
     *
     * @return JsonResponse
     */
    public function removePartnershipAction(string $partnershipCode): JsonResponse
    {
        $this->manageMemberships->removePartnership($this->getCustomer(), $partnershipCode);

        return new JsonResponse(null, 204);
    }
}
