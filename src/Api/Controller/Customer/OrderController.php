<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Customer;

use Linio\Controller\FormAware;
use Linio\Frontend\Api\Output\CustomerOrderOutput;
use Linio\Frontend\Communication\Customer\CustomerAware;
use Linio\Frontend\Customer\SearchOrderAware;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Form\Customer\ItemReturnRequestForm;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/customer/order", service="controller.api.customer.order")
 */
class OrderController
{
    use CustomerAware;
    use FormAware;
    use TokenStorageAware;
    use SearchOrderAware;

    /**
     * @var CustomerOrderOutput
     */
    protected $customerOrderOutput;

    /**
     * @var bool
     */
    protected $returnItemEnabled = false;

    /**
     * @param CustomerOrderOutput $customerOrderOutput
     */
    public function setCustomerOrderOutput(CustomerOrderOutput $customerOrderOutput)
    {
        $this->customerOrderOutput = $customerOrderOutput;
    }

    /**
     * @param bool $returnItemEnabled
     */
    public function setReturnItemEnabled(bool $returnItemEnabled)
    {
        $this->returnItemEnabled = $returnItemEnabled;
    }

    /**
     * @Route("/{orderNumber}/tracking", name="frontend.api.customer.order.tracking", methods={"POST"})
     *
     * @param string $orderNumber
     *
     * @return JsonResponse
     */
    public function trackingAction(string $orderNumber): JsonResponse
    {
        $shipments = $this->searchOrder->findOrderTrackingHistory($this->getCustomer(), $orderNumber);

        return new JsonResponse($this->customerOrderOutput->fromShipments($shipments));
    }

    /**
     * @Route("/return-item", name="frontend.api.customer.orders.return", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws FormValidationException
     *
     * @return Response
     */
    public function returnItemAction(Request $request): Response
    {
        if (!$this->returnItemEnabled) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(ItemReturnRequestForm::class, null, ['csrf_protection' => false]);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        $returnRequest = $form->getData();

        $order = $this->searchOrder->findOrder($this->getCustomer(), $returnRequest['orderNumber']);

        return new JsonResponse($this->customerService->createItemReturnRequest($this->getCustomer(), $order, $returnRequest));
    }
}
