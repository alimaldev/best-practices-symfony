<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Controller\Customer;

use Linio\DynamicFormBundle\DynamicFormAware;
use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Customer\AddressBookAware;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Security\TokenStorageAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/customer", service="controller.api.customer.address")
 */
class AddressController
{
    use AddressBookAware;
    use AddressTransputAware;
    use DynamicFormAware;
    use TokenStorageAware;

    /**
     * @Route("/addresses", methods={"GET"}, name="frontend.api.customer.address.get")
     *
     * @return JsonResponse
     */
    public function getAddressesAction(): JsonResponse
    {
        $addresses = $this->addressBook->getAddresses($this->getCustomer());

        return new JsonResponse($this->addressOutput->fromAddresses($addresses));
    }

    /**
     * @Route("/address/create", name="frontend.api.customer.address.create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAddressAction(Request $request): JsonResponse
    {
        $form = $this->getDynamicFormFactory()->createForm('address', new Address(), [
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $formException = new FormValidationException($form);
            $formException->addContext(['input' => $request->request->all()]);

            throw $formException;
        }

        /** @var Address $address */
        $address = $form->getData();
        $this->addressBook->createAddress($this->getCustomer(), $address);

        return new JsonResponse($this->addressOutput->fromAddress($address));
    }

    /**
     * @Route("/address/update", methods={"POST"}, name="frontend.api.customer.address.update")
     *
     * @param Request $request
     *
     * @throws FormValidationException
     *
     * @return JsonResponse
     */
    public function updateAddressAction(Request $request): JsonResponse
    {
        $address = $this->addressInput->fromPartialAddress($request->request->all());
        $form = $this->getDynamicFormFactory()->createForm('address', $address, [
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $formException = new FormValidationException($form);
            $formException->addContext(['input' => $request->request->all()]);

            throw $formException;
        }

        $this->addressBook->updateAddress($this->getCustomer(), $address);

        return new JsonResponse($this->addressOutput->fromAddress($address));
    }

    /**
     * @Route("/address/remove", name="frontend.api.customer.address.delete", requirements={"addressId":"\d+"}, methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeAddressAction(Request $request): JsonResponse
    {
        $address = new Address();
        $address->setId($request->request->get('id'));

        $customer = $this->getCustomer();

        $this->addressBook->removeAddress($customer, $address);

        $response = new JsonResponse(['id' => $address->getId()]);

        return $response;
    }

    /**
     * @Route("/address/set-default/{type}/{addressId}", name="frontend.api.customer.address.set_default", requirements={"type": "(shipping|billing)", "addressId":"\d+"}, methods={"PATCH"})
     *
     * @param string $type
     * @param int $addressId
     *
     * @return JsonResponse
     */
    public function setDefaultAddressAction(string $type, int $addressId): JsonResponse
    {
        $customer = $this->getCustomer();
        $address = $this->addressBook->getAddress($customer, $addressId);

        if ($type == 'billing') {
            $this->addressBook->setDefaultBilling($customer, $address);
        } else {
            $this->addressBook->setDefaultShipping($customer, $address);
        }

        return new JsonResponse();
    }
}
