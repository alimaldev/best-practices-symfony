<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Customer\CreditCard;

class MobileStoredCreditCard implements StoredCreditCardOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromCreditCard(CreditCard $creditCard): array
    {
        $explodedName = explode(' ', trim($creditCard->getCardholderName() ?? ''), 2);
        $firstName = $explodedName[0];
        $lastName = $explodedName[1];

        $creditCardData = [
            'id' => $creditCard->getId(),
            'brand' => $creditCard->getBrand(),
            'cardHolderName' => $creditCard->getCardholderName(),
            'cardHolderFirstName' => $creditCard->getCardholderFirstName() ?? $firstName,
            'cardHolderLastName' => $creditCard->getCardholderLastName() ?? $lastName,
            'firstDigits' => $creditCard->getFirstDigits(),
            'lastDigits' => $creditCard->getLastDigits(),
            'expirationDate' => $creditCard->getExpirationDate()->format('Y-m-d'),
            'paymentMethod' => $creditCard->getPaymentMethod(),
        ];

        return $creditCardData;
    }

    /**
     * {@inheritdoc}
     */
    public function fromCreditCards(array $creditCards): array
    {
        $creditCardsData = [];
        foreach ($creditCards as $creditCard) {
            $creditCardsData[] = $this->fromCreditCard($creditCard);
        }

        return $creditCardsData;
    }
}
