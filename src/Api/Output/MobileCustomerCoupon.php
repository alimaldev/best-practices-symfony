<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Customer\Coupon;

class MobileCustomerCoupon implements CustomerCouponOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromCoupons(array $coupons): array
    {
        $couponsOutput = [];

        foreach ($coupons as $coupon) {
            $couponsOutput[] = $this->fromCoupon($coupon);
        }

        return $couponsOutput;
    }

    /**
     * {@inheritdoc}
     */
    public function fromCoupon(Coupon $coupon): array
    {
        $couponOutput = [];
        $couponOutput['code'] = $coupon->getCode();
        $couponOutput['isActive'] = $coupon->isActive();
        $couponOutput['validFrom'] = $coupon->getValidFrom()->format(DateTime::ISO8601);
        $couponOutput['validUntil'] = $coupon->getValidUntil()->format(DateTime::ISO8601);
        $couponOutput['discountType'] = $coupon->getDiscountType();

        switch ($coupon->getDiscountType()) {
            case Coupon::DISCOUNT_TYPE_MONEY:
                $couponOutput['amount'] = $coupon->getAmount()->getMoneyAmount();
                $couponOutput['amountAvailable'] = $coupon->getBalance()->getMoneyAmount();
                $couponOutput['discountPercentage'] = null;
                break;
            case Coupon::DISCOUNT_TYPE_PERCENT:
            default:
                $couponOutput['discountPercentage'] = $coupon->getDiscountPercentage();
                $couponOutput['amount'] = null;
                $couponOutput['amountAvailable'] = null;
                break;
        }

        return $couponOutput;
    }
}
