<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Newsletter\NewsletterSubscription;
use Linio\Frontend\Newsletter\Preference;
use Linio\Frontend\Newsletter\PromotionalNewsletterSubscription;

class MobileNewsletter implements NewsletterOutput
{
    /**
     * @param NewsletterSubscription $subscription
     *
     * @return array
     */
    public function fromNewsletterSubscription(NewsletterSubscription $subscription): array
    {
        return [
            'email' => $subscription->getEmail(),
            'phoneNumber' => $subscription->getPhoneNumber(),
            'emailSubscribed' => $subscription->isSubscribedToEmail(),
            'smsSubscribed' => $subscription->isSubscribedToSms(),
            'frequency' => $subscription->getFrequency(),
            'frequencies' => $subscription->getFrequencies(),
            'preferences' => $this->fromPreferences($subscription->getPreferences()),
        ];
    }

    /**
     * @param PromotionalNewsletterSubscription $subscription
     *
     * @return array
     */
    public function fromPromotionalNewsletterSubscription(PromotionalNewsletterSubscription $subscription): array
    {
        return [
            'coupon' => $subscription->getCoupon(),
            'source' => $subscription->getSource(),
            'email' => $subscription->getEmail(),
            'emailSubscribed' => $subscription->isSubscribedToEmail(),
        ];
    }

    /**
     * @param []Preference $preferences
     *
     * @return array
     */
    protected function fromPreferences(array $preferences): array
    {
        $transformed = [];

        /** @var Preference $preference */
        foreach ($preferences as $preference) {
            $transformed[] = [
                'id' => $preference->getId(),
                'name' => $preference->getName(),
                'enabled' => $preference->isEnabled(),
            ];
        }

        return $transformed;
    }
}
