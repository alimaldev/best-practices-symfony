<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Catalog\SearchResults;

interface CatalogResultOutput
{
    /**
     * @param SearchResult $searchResult
     *
     * @return array
     */
    public function fromSearchResult(SearchResult $searchResult): array;

    /**
     * @param SearchResults $searchResults
     *
     * @return array
     */
    public function fromSearchResults(SearchResults $searchResults): array;
}
