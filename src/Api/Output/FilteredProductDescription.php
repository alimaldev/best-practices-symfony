<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

class FilteredProductDescription implements FilteredProductDescriptionOutput
{
    /**
     * {@inheritdoc}
     */
    public function filteredProductDescription(string $description): string
    {
        $regExNewlineTab = '/\n|\t/';

        if (!preg_match($regExNewlineTab, $description)) {
            $regExpNewLine = '/<\/h\d{1,2}>|<\/p>/';
            $regExpTab = '/<li>/';
            $description = preg_replace($regExpNewLine, "\n\n", $description);
            $description = preg_replace($regExpTab, "\t", $description);
        }
        $search = ['\n', '\t', '&nbsp;'];
        $replace = ["\n", "\t", ' '];
        $description = str_replace($search,  $replace, $description);
        $description = strip_tags($description);
        $description = preg_replace('/\ +/', ' ', $description);
        $description = preg_replace('/(\ \n)+|(\n\ )+/', "\n", $description);
        $description = preg_replace('/(\n\n\n)+/', "\n\n", $description);
        $description = preg_replace('/(\n\t\n\t)+/', "\n", $description);
        $description = trim($description);

        return $description;
    }

    /**
     * {@inheritdoc}
     */
    public function filteredShortDescription(string $attribute): array
    {
        $search = ['\n', '\t', '&nbsp;'];
        $replace = ["\n", "\t", ' '];

        $shortDescription = str_replace($search, $replace, $attribute);
        $shortDescription = strip_tags($shortDescription, '<li>');
        $shortDescription = preg_replace('/\ +/', ' ', $shortDescription);
        $shortDescription = trim($shortDescription);

        // If there are no <li> left, then we assume it's a single sentence description & return early
        if (strpos($shortDescription, '<li') === false) {
            return [$shortDescription];
        }

        // If it doesn't start with <li> then we assume there's additional text that needs to be treated as an <li>
        if (strpos($shortDescription, '<li') !== 0) {
            $plain = substr($shortDescription, 0, strpos($shortDescription, '<li'));
            $tagged = substr($shortDescription, strlen($plain));

            $shortDescription = sprintf('<li>%s</li>%s', $plain, $tagged);
        }

        $shortDescription = preg_replace("#</li>([^<>\n]+)<li>#", '</li><li>$1</li><li>', $shortDescription); // Add any left over plain text to the list.
        $shortDescription = preg_replace('/li><li/', "li>\n<li", $shortDescription);
        $shortDescription = strip_tags($shortDescription);
        $list = explode("\n", $shortDescription);

        foreach ($list as $i => $descriptionItem) {
            $list[$i] = trim($descriptionItem, " \t\n\r\0\x0B<>");
        }

        return $list;
    }
}
