<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;

interface CategoryTreeOutput
{
    /**
     * @param Category $category
     *
     * @return array
     */
    public function fromCategory(Category $category) : array;

    /**
     * @param Campaign $campaign
     * @param bool $rootObject
     *
     * @return array
     */
    public function fromCampaign(Campaign $campaign, bool $rootObject = true): array;
}
