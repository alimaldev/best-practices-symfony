<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\WishList\Product;
use Linio\Frontend\WishList\WishList;

class MobileWishList implements WishListOutput
{
    /**
     * @var WishListProductOutput
     */
    protected $wishListProductOutput;

    /**
     * @param WishListProductOutput $wishListProductOutput
     */
    public function setWishListProductOutput(WishListProductOutput $wishListProductOutput)
    {
        $this->wishListProductOutput = $wishListProductOutput;
    }

    /**
     * {@inheritdoc}
     */
    public function fromWishList(WishList $wishList): array
    {
        return [
            'id' => $wishList->getId(),
            'name' => $wishList->getName(),
            'description' => $wishList->getDescription(),
            'default' => $wishList->isDefault(),
            'owner' => [
                'name' => $wishList->getOwner()->getFullName(),
                'email' => $wishList->getOwner()->getEmail(),
                'linioId' => $wishList->getOwner()->getLinioId(),
            ],
            'createdAt' => $wishList->getCreatedAt() ? $wishList->getCreatedAt()->format(DateTime::ISO8601) : null,
            'visibility' => $wishList->getVisibility(),
            'accessHash' => $wishList->getAccessHash(),
            'products' => $wishList->getProducts() ? $this->getProductsOutput($wishList->getProducts()) : [],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fromWishLists(array $wishLists): array
    {
        $wishListsOutput = [];

        foreach ($wishLists as $wishList) {
            $wishListsOutput[] = $this->fromWishList($wishList);
        }

        return $wishListsOutput;
    }

    /**
     * @param Product[] $products
     *
     * @return array
     */
    protected function getProductsOutput(array $products): array
    {
        $productsOutput = [];

        foreach ($products as $product) {
            $productsOutput[$product->getSku()] = $this->wishListProductOutput->fromProduct($product);
        }

        return $productsOutput;
    }
}
