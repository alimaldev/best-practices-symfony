<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

interface PartnershipOutput
{
    /**
     * @param array $partnerships
     *
     * @return array
     */
    public function fromPartnerships(array $partnerships): array;
}
