<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Entity\PaginatedResult;

class DefaultCustomerOrder implements CustomerOrderOutput
{
    /**
     * @var DateFormatter
     */
    protected $dateFormatter;

    /**
     * @param DateFormatter $dateFormatter
     */
    public function setDateFormatter(DateFormatter $dateFormatter)
    {
        $this->dateFormatter = $dateFormatter;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function fromOrder(Order $order): array
    {
        // TODO: Implement fromOrder() method.
    }

    /**
     * @param PaginatedResult $paginatedResult
     *
     * @return array
     */
    public function fromPaginatedOrderList(PaginatedResult $paginatedResult): array
    {
        // TODO: Implement fromPaginatedOrderList() method.
    }

    /**
     * @param Shipment[] $shipments
     *
     * @return array
     */
    public function fromShipments(array $shipments): array
    {
        $response = [];

        foreach ($shipments as $shipment) {
            $shipmentData = [
                'trackingCode' => $shipment->getTrackingCode(),
                'trackingUrl' => $shipment->getTrackingUrl(),
                'carrier' => $shipment->getCarrier(),
                'updatedAt' => $shipment->getUpdatedAt()->format('Y-m-d H:i:s'),
                'items' => [],
                'events' => [],
            ];

            foreach ($shipment->getItems() as $item) {
                $shipmentData['items'][] = [
                    'product' => $item['product']->getSku(),
                    'quantity' => $item['quantity'],
                ];
            }

            foreach ($shipment->getTrackingEvents() as $trackingEvent) {
                $shipmentData['events'][] = [
                    'code' => $trackingEvent->getCode(),
                    'description' => $trackingEvent->getDescription(),
                    'createdAt' => $trackingEvent->getCreatedAt()->format('Y-m-d H:i:s'),
                ];
            }

            $response[] = $shipmentData;
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function fromPendingOrders(array $orders): array
    {
        $pendingOrders = [];

        foreach ($orders as $order) {
            $pendingOrders[] = [
                'id' => $order->getId(),
                'orderNumber' => $order->getOrderNumber(),
                'purchaseDate' => $order->getCreatedAt() ? $this->dateFormatter->formatLocale($order->getCreatedAt()) : null,
                'grandTotal' => $order->getGrandTotal()->getMoneyAmount(),
                'status' => $order->getStatusProgressConfiguration(),
                'productCount' => count($order->getItems()),
            ];
        }

        return $pendingOrders;
    }
}
