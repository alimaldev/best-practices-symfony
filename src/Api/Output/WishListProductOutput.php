<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\WishList\Product;

interface WishListProductOutput
{
    /**
     * @param Product $product
     *
     * @return array
     */
    public function fromProduct(Product $product): array;
}
