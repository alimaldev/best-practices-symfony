<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

interface FilteredProductDescriptionOutput
{
    /**
     * @param string $description
     *
     * @return string
     */
    public function filteredProductDescription(string $description): string;

    /**
     * @param string $attribute
     *
     * @return array
     */
    public function filteredShortDescription(string $attribute): array;
}
