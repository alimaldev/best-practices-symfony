<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Entity\Seller\PaginatedReviews;
use Linio\Frontend\Entity\Seller\Review;
use Linio\Frontend\Entity\Seller\Seller as ReviewSeller;

class MobileSeller implements SellerOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromSeller(Seller $seller): array
    {
        return [
            'id' => $seller->getSellerId(),
            'name' => $seller->getName(),
            'slug' => $seller->getSlug(),
            'rating' => $seller->getRating(),
            'type' => $seller->getType(),
            'operationType' => $seller->getOperationType(),
            'import' => $seller->isImport(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fromReviewSeller(ReviewSeller $seller): array
    {
        return [
            'id' => $seller->getId(),
            'name' => $seller->getName(),
            'slug' => $seller->getSlug(),
            'type' => $seller->getType(),
            'operationType' => $seller->getOperationType(),
            'ratingSummary' => $this->sellerRatingSummary($seller),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fromReview(Review $review): array
    {
        return [
            'title' => $review->getTitle(),
            'customerName' => $review->getCustomerName(),
            'comment' => $review->getComment(),
            'createdAt' => $review->getCreatedAt()->format(DateTime::ISO8601),
            'experienceAverage' => $review->getExperienceAverage(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fromPaginatedReviews(PaginatedReviews $paginatedReviews): array
    {
        $reviews = $paginatedReviews->getReviews();

        $outputPagination = [
            'currentPage' => $paginatedReviews->getCurrentPage(),
            'totalPages' => $paginatedReviews->getTotalPages(),
        ];

        foreach ($reviews as $review) {
            $outputPagination['reviews'][] = $this->fromReview($review);
        }

        return $outputPagination;
    }

    /**
     * @param ReviewSeller $seller
     *
     * @return array
     */
    protected function sellerRatingSummary(ReviewSeller $seller): array
    {
        $ratings = [];

        foreach ($seller->getRatingSummary()->getRatings() as $rating) {
            $ratings[] = [
                'stars' => $rating->getStars(),
                'votes' => $rating->getVotes(),
            ];
        }

        return [
            'averagePoint' => $seller->getRatingSummary()->getAveragePoint(),
            'ratings' => $ratings,
            'reviewsQuantity' => $seller->getRatingSummary()->getReviewsQuantity(),
        ];
    }
}
