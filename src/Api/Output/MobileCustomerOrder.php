<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\MakeDeliveryStatusFriendly;
use Linio\Frontend\Customer\Order\Package;
use Linio\Frontend\Customer\Order\Package\Item;
use Linio\Frontend\Customer\Order\Payment;
use Linio\Frontend\Customer\Order\Payment\Installments;
use Linio\Frontend\Customer\Order\Shipping;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Order\Wallet;
use Linio\Frontend\Product\ProductImageTransformerAware;
use Linio\Frontend\Product\ProductService;
use Symfony\Component\Translation\TranslatorInterface;

class MobileCustomerOrder implements CustomerOrderOutput
{
    use AddressTransputAware;
    use ProductImageTransformerAware;

    /**
     * @var MakeDeliveryStatusFriendly
     */
    protected $deliveryStatusFriendlyService;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var DateFormatter
     */
    protected $dateFormatter;

    /**
     * @param MakeDeliveryStatusFriendly $deliveryStatusFriendly
     */
    public function setMakeDeliveryStatusFriendlyService(MakeDeliveryStatusFriendly $deliveryStatusFriendly)
    {
        $this->deliveryStatusFriendlyService = $deliveryStatusFriendly;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param ProductService $productService
     */
    public function setProductService(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @param DateFormatter $dateFormatter
     */
    public function setDateFormatter(DateFormatter $dateFormatter)
    {
        $this->dateFormatter = $dateFormatter;
    }

    /**
     * @param PaginatedResult $paginatedResult
     *
     * @return array
     */
    public function fromPaginatedOrderList(PaginatedResult $paginatedResult): array
    {
        /** @var Order[] $orders */
        $orders = $paginatedResult->getResult();
        $result = [
            'page' => $paginatedResult->getCurrent(),
            'pageCount' => $paginatedResult->getPages(),
            'ordersPerPage' => $paginatedResult->getSize(),
            'ordersFound' => $paginatedResult->getItemCount(),
        ];

        foreach ($orders as $order) {
            $result['orders'][] = $this->fromOrder($order);
        }

        return $result;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function fromOrder(Order $order): array
    {
        $packages = $this->getPackages($order);
        $shipping = $this->getShipping($order->getShipping());
        $payment = $this->getPayment($order->getPayment());

        $result = [
            'orderId' => $order->getId(),
            'orderNumber' => $order->getOrderNumber(),
            'subTotal' => $order->getSubtotal()->getMoneyAmount(),
            'grandTotal' => $order->getGrandTotal()->getMoneyAmount(),
            'createdAt' => $order->getCreatedAt() ? $order->getCreatedAt()->format(DATE_ISO8601) : null,
            'shipping' => $shipping,
            'payment' => $payment,
            'wallet' => $this->getWallet($order->getWallet()),
            'packages' => $packages,
            'customer' => sprintf(
                '%s %s',
                $order->getCustomerName()->getFirstName(),
                $order->getCustomerName()->getLastName()
            ),
            'invoiceAvailable' => $order->isInvoiceAvailable(),
        ];

        return $result;
    }

    /**
     * @param Shipment[] $shipments
     *
     * @return array
     */
    public function fromShipments(array $shipments): array
    {
        $response = [];

        foreach ($shipments as $shipment) {
            $shipmentData = [
                'trackingCode' => $shipment->getTrackingCode(),
                'trackingUrl' => $shipment->getTrackingUrl(),
                'carrier' => $shipment->getCarrier(),
                'updatedAt' => $shipment->getUpdatedAt()->format(DATE_ISO8601),
                'items' => [],
                'events' => [],
            ];

            foreach ($shipment->getItems() as $item) {
                $shipmentData['items'][] = [
                    'product' => $item['product']->getSku(),
                    'quantity' => $item['quantity'],
                ];
            }

            foreach ($shipment->getTrackingEvents() as $trackingEvent) {
                $shipmentData['events'][] = [
                    'code' => $trackingEvent->getCode(),
                    'description' => $trackingEvent->getDescription(),
                    'createdAt' => $trackingEvent->getCreatedAt()->format(DATE_ISO8601),
                ];
            }

            $response[] = $shipmentData;
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function fromPendingOrders(array $orders): array
    {
        $pendingOrders = [];

        foreach ($orders as $order) {
            $pendingOrders[] = [
                'id' => $order->getId(),
                'orderNumber' => $order->getOrderNumber(),
                'purchaseDate' => $order->getCreatedAt() ? $this->dateFormatter->formatLocale($order->getCreatedAt()) : null,
                'grandTotal' => $order->getGrandTotal()->getMoneyAmount(),
                'status' => $order->getStatusProgressConfiguration(),
                'productCount' => count($order->getItems()),
            ];
        }

        return $pendingOrders;
    }

    /**
     * @param Shipping $shipping
     *
     * @return array
     */
    protected function getShipping(Shipping $shipping): array
    {
        return [
            'amount' => $shipping->getAmount() ? $shipping->getAmount()->getMoneyAmount() : null,
            'discount' => $shipping->getDiscount() ? $shipping->getDiscount()->getMoneyAmount() : null,
            'address' => $this->addressOutput->fromAddress($shipping->getAddress()),
        ];
    }

    /**
     * @param Payment $payment
     *
     * @return array
     */
    protected function getPayment(Payment $payment): array
    {
        $paymentData = [
            'paymentMethod' => [
                'name' => $payment->getPaymentMethod()->getName(),
                'label' => $this->translator->trans(sprintf(
                    'payment_methods.%s.display_name', $payment->getPaymentMethod()->getName()
                )),
            ],
            'barcode' => $payment->getBarcode(),
            'installments' => $payment->getInstallments() ? $this->getInstallments($payment->getInstallments()) : null,
            'url' => $payment->getPaymentUrl() ?? null,
            'creditCard' => null,
        ];

        if ($payment->getCreditCardMaskedNumber()) {
            $paymentData['creditCard'] = [
                'maskedNumber' => $payment->getCreditCardMaskedNumber(),
                'brand' => $payment->getCreditCardBrand(),
            ];
        }

        return $paymentData;
    }

    /**
     * @param Installments $installments
     *
     * @return array
     */
    protected function getInstallments(Installments $installments): array
    {
        return [
            'amount' => $installments->getAmount()->getMoneyAmount(),
            'number' => $installments->getNumber(),
            'totalInterest' => $installments->getTotalInterest()->getMoneyAmount(),
        ];
    }

    /**
     * @param Wallet $wallet
     *
     * @return array
     */
    protected function getWallet(Wallet $wallet): array
    {
        return [
            'totalDiscount' => $wallet->getTotalDiscount()->getMoneyAmount(),
            'totalPointsUsed' => $wallet->getTotalPointsUsed()->getMoneyAmount(),
            'pointsBalance' => $wallet->getPointsBalance()->getMoneyAmount(),
            'shippingDiscount' => $wallet->getShippingDiscount()->getMoneyAmount(),
            'pointsUsedForShipping' => $wallet->getPointsUsedForShipping()->getMoneyAmount(),
            'conversionRate' => $wallet->getConversionRate(),
        ];
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function getPackages(Order $order): array
    {
        $packages = [];

        foreach ($order->getPackages() as $package) {
            $packages[] = [
                'number' => $package->getNumber(),
                'status' => $package->getStatus(),
                'shipmentStatus' => $package->getShipmentStatus(),
                'progressStep' => $package->getProgressStep(),
                'expectedDeliveryDate' => $package->getExpectedDeliveryDate() ? $package->getExpectedDeliveryDate()->format(DATE_ISO8601) : null,
                'updatedDeliveryDate' => $package->getUpdatedDeliveryDate() ? $package->getUpdatedDeliveryDate()->format(DATE_ISO8601) : null,
                'deliveredAt' => $package->getDeliveredAt() ? $package->getDeliveredAt()->format(DATE_ISO8601) : null,
                'allowReturn' => $package->isReturnAllowed(),
                'allowTracking' => $package->isTrackingAllowed(),
                'allowSellerReview' => $package->isSellerReviewAllowed(),
                'items' => $this->getPackageItemsByStatus($package),
            ];
        }

        return $packages;
    }

    /**
     * @param Package $package
     *
     * @return array
     *
     * @internal param array $progressConfig
     */
    protected function getPackageItemsByStatus(Package $package): array
    {
        $items = [];

        foreach ($package->getItems() as $status => $packageItems) {
            /** @var Item $item */
            foreach ($packageItems as $item) {
                $itemData = [
                    'sku' => $item->getSku(),
                    'name' => $item->getName(),
                    'quantity' => $item->getQuantity(),
                    'unitPrice' => $item->getUnitPrice()->getMoneyAmount(),
                    'paidPrice' => $item->getPaidPrice()->getMoneyAmount(),
                    'seller' => $this->translator->trans($item->getSeller()),
                    'linioPlus' => $item->isLinioPlus(),
                    'imported' => $item->isImported(),
                    'virtual' => $item->isVirtual(),
                    'overweight' => $item->isOverweight(),
                    'allowReturn' => $item->isReturnAllowed(),
                    'statusProgressStep' => $item->getStatusProgressStep(),
                    'statusUpdatedAt' => $item->getStatusUpdatedAt()->format(DATE_ISO8601),
                    'image' => $item->getImage() ? $this->getUrlFromImage($item->getImage(), 'product') : '',
                    'status' => $this->deliveryStatusFriendlyService->getItemFriendlyStatus(
                        $item
                    ),
                    'expectedDeliveryDate' => $item->getExpectedDeliveryDate() ? $this->dateFormatter->formatLocale($item->getExpectedDeliveryDate()) : null,
                    'updatedDeliveryDate' => $item->getUpdatedDeliveryDate() ? $this->dateFormatter->formatLocale($item->getUpdatedDeliveryDate()) : null,
                ];

                $items[$status][] = $itemData;
            }
        }

        return $items;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function getSkusFromPackages(Order $order)
    {
        $skus = [];

        foreach ($order->getPackages() as $package) {
            foreach ($package->getItems() as $status => $items) {

                /** @var Item $item */
                foreach ($items as $item) {
                    $skus[] = $item->getSku();
                }
            }
        }

        return $skus;
    }
}
