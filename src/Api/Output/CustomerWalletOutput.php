<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Customer\Wallet\Wallet;

interface CustomerWalletOutput
{
    /**
     * @param Wallet $wallet
     *
     * @return array
     */
    public function fromCustomerWallet(Wallet $wallet): array;
}
