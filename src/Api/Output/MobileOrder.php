<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Api\AddressTransputAware;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\DateFormatter;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Items;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\InstallmentOption;
use Linio\Frontend\Order\Payment\InstallmentOptions;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\RecalculatedOrder;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\Packages;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Symfony\Component\Translation\TranslatorInterface;

class MobileOrder implements OrderOutput
{
    use AddressTransputAware;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /**
     * @var SellerOutput
     */
    protected $sellerOutput;

    /**
     * @var FilteredProductDescriptionOutput
     */
    protected $filteredProductDescriptionOutput;

    /**
     * @var DateFormatter;
     */
    protected $dateFormatter;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param MoneyFormatter $moneyFormatter
     */
    public function setMoneyFormatter(MoneyFormatter $moneyFormatter)
    {
        $this->moneyFormatter = $moneyFormatter;
    }

    /**
     * @param SellerOutput $sellerOutput
     */
    public function setSellerOutput(SellerOutput $sellerOutput)
    {
        $this->sellerOutput = $sellerOutput;
    }

    /**
     * @param FilteredProductDescriptionOutput $filteredProductDescriptionOutput
     */
    public function setFilteredProductDescriptionOutput(FilteredProductDescriptionOutput $filteredProductDescriptionOutput)
    {
        $this->filteredProductDescriptionOutput = $filteredProductDescriptionOutput;
    }

    /**
     * @param DateFormatter $dateFormatter
     */
    public function setDateFormatter(DateFormatter $dateFormatter)
    {
        $this->dateFormatter = $dateFormatter;
    }

    /**
     * {@inheritdoc}
     */
    public function fromRecalculatedOrder(RecalculatedOrder $recalculatedOrder): array
    {
        $order = $recalculatedOrder->getOrder();

        return [
            'order' => [
                'customer' => $this->getCustomer($order),
                'subTotal' => $order->getSubTotal()->getMoneyAmount(),
                'grandTotal' => $order->getGrandTotal()->getMoneyAmount(),
                'taxAmount' => $order->getTaxAmount()->getMoneyAmount(),
                'shippingAmount' => $order->getShippingAmount() ? $order->getShippingAmount()->getMoneyAmount() : null,
                'originalShippingAmount' => $order->getOriginalShippingAmount() ? $order->getOriginalShippingAmount()->getMoneyAmount() : null,
                'shippingDiscountAmount' => $order->getShippingDiscountAmount()->getMoneyAmount(),
                'coupon' => $this->getCoupon($order),
                'totalDiscountAmount' => $order->getTotalDiscountAmount()->getMoneyAmount(),
                'linioPlusSavedAmount' => $order->getLinioPlusSavedAmount()->getMoneyAmount(),
                'loyaltyPointsAccrued' => $order->getLoyaltyPointsAccrued(),
                'availablePaymentMethods' => $this->getAvailablePaymentMethods($order->getAvailablePaymentMethods()),
                'paymentMethod' => $order->getPaymentMethod() ? $order->getPaymentMethod()->getName() : null,
                'creditCardBinNumber' => $order->getPaymentMethod() instanceof CreditCard ? $order->getPaymentMethod()->getBinNumber() : null,
                'shippingAddress' => $order->getShippingAddress() ? $this->addressOutput->fromAddress($order->getShippingAddress()) : null,
                'items' => $this->getItems($order),
                'packages' => $this->getPackages($order->getPackages()),
                'undeliverables' => $this->getUndeliverableItems($recalculatedOrder->getUndeliverables()),
                'wallet' => $this->getWallet($order),
                'partnerships' => $this->getPartnerships($order),
                'partnershipDiscount' => $order->getPartnershipDiscount()->getMoneyAmount(),
                'installmentOptions' => $this->getInstallmentOptions($order->getInstallmentOptions()),
                'highestInstallment' => $this->getInstallmentOption($order->getInstallmentOptions()->getHighest()),
                'lowestInstallment' => $this->getInstallmentOption($order->getInstallmentOptions()->getLowest()),
                'pickupStores' => $this->getPickupStores($order),
                'geohash' => $order->getGeohash(),
            ],
            'messages' => $this->parseMessages($recalculatedOrder),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fromCompletedOrder(CompletedOrder $completedOrder): array
    {
        $order = [
            'grandTotal' => $completedOrder->getGrandTotal()->getMoneyAmount(),
            'orderNumber' => $completedOrder->getOrderNumber(),
            'paymentMethod' => $completedOrder->getPaymentMethod() ? $completedOrder->getPaymentMethod()->getName() : null,
            'redirect' => null,
        ];

        if ($completedOrder->hasPaymentRedirect()) {
            $order['redirect'] = [
                'method' => $completedOrder->getPaymentRedirect()->getMethod(),
                'target' => $completedOrder->getPaymentRedirect()->getTarget(),
                'body' => $completedOrder->getPaymentRedirect()->getBody(),
            ];
        }

        return $order;
    }

    /**
     * @param Order $order
     *
     * @return array|null
     */
    protected function getCoupon(Order $order)
    {
        if (!$order->getCoupon()) {
            return;
        }

        return [
            'code' => $order->getCoupon()->getCode(),
            'discount' => $order->getCoupon()->getDiscount()->getMoneyAmount(),
        ];
    }

    /**
     * @param Order $order
     *
     * @return array|null
     */
    protected function getCustomer(Order $order)
    {
        $customer = $order->getCustomer();

        if (!$customer) {
            return;
        }

        return [
            'loyaltyProgram' => $customer->getLoyaltyProgram() ? $customer->getLoyaltyProgram()->getName() : null,
            'loyaltyId' => $customer->getLoyaltyProgram() ? $customer->getLoyaltyProgram()->getId() : null,
            'nationalRegistrationNumber' => $customer->getNationalRegistrationNumber(),
            'taxIdentificationNumber' => $customer->getTaxIdentificationNumber(),
        ];
    }

    /**
     * @param PaymentMethod[] $paymentMethods
     *
     * @return array
     */
    protected function getAvailablePaymentMethods(array $paymentMethods): array
    {
        $availablePaymentMethods = [];

        foreach ($paymentMethods as $paymentMethod) {
            $availablePaymentMethods[$paymentMethod->getName()] = [
                'allowed' => $paymentMethod->isAllowed(),
                'allowedByCoupon' => $paymentMethod->isAllowedByCoupon(),
                'requireBillingAddress' => $paymentMethod->isBillingAddressRequired(),
                'paymentMethodLabel' => $this->translator->trans(
                    sprintf('payment_methods.%s.display_name', $paymentMethod->getName())
                ),
            ];
        }

        return $availablePaymentMethods;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function getItems(Order $order): array
    {
        $orderItems = [];

        /** @var Item $item */
        foreach ($order->getItems() as $item) {
            $simple = $item->getProduct()->getSimple($item->getSku());
            $seller = $item->getProduct()->getSeller();
            $brand = $item->getProduct()->getBrand();

            $orderItems[$item->getSku()] = [
                'sku' => $item->getSku(),
                'slug' => $item->getProduct()->getSlug(),
                'name' => $item->getProduct()->getName(),
                'description' => $this->filteredProductDescriptionOutput->filteredProductDescription($item->getProduct()->getDescription() ?? ''),
                'variation' => $simple ? $simple->getAttribute('variation') : null,
                'variationType' => $item->getProduct()->getVariationType(),
                'seller' => isset($seller) ? $this->sellerOutput->fromSeller($seller) : null,
                'percentageOff' => $simple ? $simple->getPercentageOff() : null,
                'unitPrice' => $item->getUnitPrice()->getMoneyAmount(),
                'originalPrice' => $item->getOriginalPrice()->getMoneyAmount(),
                'paidPrice' => $item->getPaidPrice()->getMoneyAmount(),
                'taxAmount' => $item->getTaxAmount()->getMoneyAmount(),
                'shippingAmount' => $item->getShippingAmount()->getMoneyAmount(),
                'subtotal' => $item->getSubtotal()->getMoneyAmount(),
                'quantity' => $item->getQuantity(),
                'availableQuantity' => $item->getAvailableQuantity(),
                'maxItemsToSell' => $item->getMaxItemsToSell(),
                'image' => $item->getImage(),
                'linioPlusLevel' => $item->getLinioPlusLevel(),
                'linioPlusEnabledQuantity' => $item->getLinioPlusEnabledQuantity(),
                'deliveredByChristmas' => $item->getProduct()->isDeliveredByChristmas(),
                'minimumDeliveryDate' => $item->getMinimumDeliveryDate() ? $this->dateFormatter->formatLocale($item->getMinimumDeliveryDate()) : null,
                'freeShipping' => $simple ? $simple->hasFreeShipping() : null,
                'freeStorePickup' => $item->getProduct()->hasFreeStorePickup(),
                'imported' => $item->getProduct()->isImported(),
                'oversized' => $item->getProduct()->isOversized(),
                'brand' => $this->getBrandAsArray($brand),
                'category' => $this->getCategoryAsArray($item->getProduct()),
            ];
        }

        return $orderItems;
    }

    /**
     * @param Packages $packages
     *
     * @return array
     */
    protected function getPackages(Packages $packages): array
    {
        $orderPackages = [];

        /** @var Package $package */
        foreach ($packages as $package) {
            $orderPackage = [
                'items' => [],
                'shippingQuotes' => [],
            ];

            /** @var Item $item */
            foreach ($package->getItems() as $item) {
                $orderPackage['items'][$item->getSku()] = $item->getQuantity();
            }

            /** @var ShippingQuote $shippingQuote */
            foreach ($package->getShippingQuotes() as $shippingQuote) {
                $orderPackage['shippingQuotes'][] = [
                    'shippingMethod' => $shippingQuote->getShippingMethod(),
                    'fee' => $shippingQuote->getFee()->getMoneyAmount(),
                    'estimatedDeliveryDate' => $shippingQuote->getEstimatedDeliveryDate() ? $this->dateFormatter->formatLocale($shippingQuote->getEstimatedDeliveryDate()) : null,
                    'deliveredByChristmas' => $shippingQuote->isDeliveredByChristmas(),
                    'freeShipping' => $shippingQuote->hasFreeShipping(),
                    'selected' => $shippingQuote->isSelected(),
                    'name' => $this->translator->trans('checkout.shipping_methods.' . $shippingQuote->getShippingMethod()),
                ];
            }

            $orderPackages[$package->getId()] = $orderPackage;
        }

        return $orderPackages;
    }

    /**
     * @param Items $items
     *
     * @return array
     */
    protected function getUndeliverableItems(Items $items): array
    {
        $undeliverableItems = [];

        foreach ($items as $item) {
            $undeliverableItems[] = $item->getSku();
        }

        return $undeliverableItems;
    }

    /**
     * @param InstallmentOptions $installmentOptions
     *
     * @return array
     */
    protected function getInstallmentOptions(InstallmentOptions $installmentOptions): array
    {
        $installments = [];

        foreach ($installmentOptions as $installmentOption) {
            $option = [
                'installments' => $installmentOption->getInstallments(),
                'interestFee' => $installmentOption->getInterestFee()->getMoneyAmount(),
                'total' => $installmentOption->getTotal()->getMoneyAmount(),
                'totalInterest' => $installmentOption->getTotalInterest()->getMoneyAmount(),
                'amount' => $installmentOption->getAmount()->getMoneyAmount(),
                'selected' => $installmentOption->isSelected(),
                'paymentMethodName' => $installmentOption->getPaymentMethodName(),
                'hasBankInterestFee' => $installmentOption->hasBankInterestFee(),
            ];

            $option['description'] = $this->translator->transChoice('checkout.installment_option', $installmentOption->getInstallments(), [
                '%amount%' => $this->moneyFormatter->format($installmentOption->getAmount()),
                '%total%' => $this->moneyFormatter->format($installmentOption->getTotal()),
                '%interest%' => $installmentOption->hasInterest() ? $this->translator->trans('checkout.with_interest') : $this->translator->trans('checkout.without_interest'),
                '%count%' => $installmentOption->getInstallments(),
            ]);

            $installments[] = $option;
        }

        return $installments;
    }

    /**
     * @param InstallmentOption|null $installmentOption
     *
     * @return int|null
     */
    protected function getInstallmentOption(InstallmentOption $installmentOption = null)
    {
        if (!$installmentOption) {
            return;
        }

        return $installmentOption->getInstallments();
    }

    /**
     * @param Order $order
     *
     * @return array|null
     */
    protected function getPickupStores(Order $order)
    {
        $pickupStores = [];

        /** @var PickupStore $pickupStore */
        foreach ($order->getPickupStores() as $pickupStore) {
            $pickupStoreData = [
                'id' => $pickupStore->getId(),
                'name' => $pickupStore->getName(),
                'description' => $pickupStore->getDescription(),
                'postcode' => $pickupStore->getPostcode(),
                'geohash' => $pickupStore->getGeohash(),
                'region' => $pickupStore->getRegion(),
                'municipality' => $pickupStore->getMunicipality(),
                'city' => $pickupStore->getCity(),
                'subLocality' => $pickupStore->getSubLocality(),
                'neighborhood' => $pickupStore->getNeighborhood(),
                'line1' => $pickupStore->getLine1(),
                'line2' => $pickupStore->getLine2(),
                'apartment' => $pickupStore->getApartment(),
                'referencePoint' => $pickupStore->getReferencePoint(),
                'network' => [
                    'id' => $pickupStore->getNetworkId(),
                    'name' => $pickupStore->getNetworkName(),
                ],
                'selected' => $pickupStore->isSelected(),
            ];

            $pickupStores[$pickupStore->getId()] = $pickupStoreData;
        }

        if (!$pickupStores) {
            return;
        }

        return $pickupStores;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function getPartnerships(Order $order)
    {
        $partnerships = [];

        foreach ($order->getPartnerships() as $partnership) {
            $partnerships[] = [
                'name' => $this->translator->trans(sprintf('partnership.%s.name', $partnership->getCode())),
                'description' => $this->translator->trans(sprintf('partnership.%s.description', $partnership->getCode())),
                'code' => $partnership->getCode(),
                'accountNumber' => $partnership->getAccountNumber(),
                'level' => $partnership->getLevel(),
                'appliedDiscount' => $partnership->isDiscountApplied(),
                'active' => $partnership->isActive(),
            ];
        }

        return $partnerships;
    }

    /**
     * @param Order $order
     *
     * @return array|null
     */
    protected function getWallet(Order $order)
    {
        if (!$order->getWallet()) {
            return;
        }

        return [
            'totalDiscount' => $order->getWallet()->getTotalDiscount()->getMoneyAmount(),
            'totalPointsUsed' => $order->getWallet()->getTotalPointsUsed()->getMoneyAmount(),
            'pointsBalance' => $order->getWallet()->getPointsBalance()->getMoneyAmount(),
            'shippingDiscount' => $order->getWallet()->getShippingDiscount()->getMoneyAmount(),
            'pointsUsedForShipping' => $order->getWallet()->getPointsUsedForShipping()->getMoneyAmount(),
            'maxPointsForOrder' => $order->getWallet()->getMaxPointsForOrder()->getAmount(),
            'conversionRate' => $order->getWallet()->getConversionRate(),
        ];
    }

    /**
     * @param Product $product
     *
     * @return array|null
     */
    protected function getCategoryAsArray(Product $product)
    {
        if (!$product->getCategory()) {
            return;
        }

        return [
            'id' => $product->getCategory()->getId(),
            'name' => $product->getCategory()->getName(),
            'slug' => $product->getCategory()->getSlug(),
            'urlKey' => $product->getCategory()->getUrlKey(),
            'isCurrent' => $product->getCategory()->isCurrent(),
            'count' => $product->getCategory()->getCount(),
        ];
    }

    /**
     * @param RecalculatedOrder $recalculatedOrder
     *
     * @return array
     */
    protected function parseMessages(RecalculatedOrder $recalculatedOrder): array
    {
        $messages = [
            'errors' => [],
            'warnings' => [],
            'success' => null,
        ];

        foreach ($recalculatedOrder->getErrors() as $context => $error) {
            $messages['errors'][] = $this->translator->trans($error, ['%context%' => $context]);
        }

        foreach ($recalculatedOrder->getWarnings() as $context => $warnings) {
            foreach ($warnings as $warning) {
                $messages['warnings'][] = $this->translator->trans($warning, ['%context%' => $context]);
            }
        }

        if ($recalculatedOrder->getSuccessMessage()) {
            $messages['success'] = $this->translator->trans($recalculatedOrder->getSuccessMessage());
        }

        return $messages;
    }

    /**
     * @param Brand $brand
     *
     * @return array
     */
    protected function getBrandAsArray(Brand $brand): array
    {
        return [
            'id' => $brand->getId(),
            'name' => $brand->getName(),
            'slug' => $brand->getSlug(),
        ];
    }
}
