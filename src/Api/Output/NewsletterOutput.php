<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Newsletter\NewsletterSubscription;
use Linio\Frontend\Newsletter\PromotionalNewsletterSubscription;

interface NewsletterOutput
{
    /**
     * @param PromotionalNewsletterSubscription $subscription
     *
     * @return array
     */
    public function fromPromotionalNewsletterSubscription(PromotionalNewsletterSubscription $subscription): array;

    /**
     * @param NewsletterSubscription $subscription
     *
     * @return array
     */
    public function fromNewsletterSubscription(NewsletterSubscription $subscription): array;
}
