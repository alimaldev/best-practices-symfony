<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Simple;

interface ProductOutput
{
    /**
     * @param Product $product
     *
     * @return array
     */
    public function fromProduct(Product $product): array;

    /**
     * @param MarketplaceChild $child
     *
     * @return array
     */
    public function fromFilteredMarketplaceChild(MarketplaceChild $child): array;

    /**
     * @param Simple $simple
     *
     * @return array
     */
    public function fromFilteredSimple(Simple $simple): array;
}
