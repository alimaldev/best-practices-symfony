<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Customer\Order\Shipping\Address as ShippingAddress;
use Linio\Frontend\Location\Address;

interface AddressOutput
{
    /**
     * @param Address $address
     *
     * @return array
     */
    public function fromAddress(Address $address): array;

    /**
     * @param array $addresses
     *
     * @return array
     */
    public function fromAddresses(array $addresses): array;

    /**
     * @param ShippingAddress $shippingAddress
     *
     * @return array
     */
    public function fromShippingAddress(ShippingAddress $shippingAddress): array;
}
