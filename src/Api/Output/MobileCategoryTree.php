<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Catalog\Campaign;
use Linio\Frontend\Entity\Catalog\Category;

class MobileCategoryTree implements CategoryTreeOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromCategory(Category $category) : array
    {
        $categoryOutput = [
            'id' => (string) $category->getId(),
            'name' => $category->getName(),
            'slug' => $category->getSlug(),
            'urlKey' => $category->getUrlKey(),
            'isCurrent' => $category->isCurrent(),
            'count' => $category->getCount(),
            'path' => [],
            'children' => [],
        ];

        foreach ($category->getPath() as $pathItem) {
            $categoryOutput['path'][] = $this->fromCategory($pathItem);
        }

        foreach ($category->getChildren() as $child) {
            $categoryOutput['children'][(string) $child->getId()] = $this->fromCategory($child);
        }

        return $categoryOutput;
    }

    /**
     * {@inheritdoc}
     */
    public function fromCampaign(Campaign $campaign, bool $rootObject = true): array
    {
        $campaignOutput = [
            'id' => (string) $campaign->getId(),
            'name' => $campaign->getName(),
            'slug' => $campaign->getSlug(),
            'urlKey' => $campaign->getUrlKey(),
            'skus' => $campaign->getSkus(),
            'filters' => $campaign->getFilters(),
            'isCurrent' => $campaign->isCurrent(),
            'count' => $campaign->getCount(),
            'path' => [],
            'children' => [],
        ];

        // We only need path & children for the root level object
        if (!$rootObject) {
            return $campaignOutput;
        }

        foreach ($campaign->getPath() as $pathItem) {
            $campaignOutput['path'][] = $this->fromCampaign($pathItem, false);
        }

        foreach ($campaign->getChildren() as $child) {
            $campaignOutput['children'][(string) $child->getId()] = $this->fromCampaign($child, false);
        }

        return $campaignOutput;
    }
}
