<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output\Mobile\V2;

use Linio\Frontend\Api\Output\Mobile\V1\CatalogResult as CatalogResultV1;
use Linio\Frontend\Entity\Catalog\SearchResults;

class CatalogResult extends CatalogResultV1
{
    /**
     * {@inheritdoc}
     */
    public function fromSearchResults(SearchResults $searchResults): array
    {
        $didYouMean = $searchResults->getDidYouMeanSearchResult();
        $original = $searchResults->getOriginalSearchResult();
        $didYouMeanOutput = null;
        $originalOutput = null;

        if (!empty($didYouMean) && $didYouMean->getTotalItemsFound() != 0) {
            $didYouMeanOutput = $this->fromSearchResult($didYouMean);
        }

        if (!empty($original) && $original->getTotalItemsFound() != 0) {
            $originalOutput = $this->fromSearchResult($original);
        }

        $searchResults = [
            'didYouMean' => $didYouMeanOutput,
            'original' => $originalOutput,
        ];

        return $searchResults;
    }
}
