<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output\Mobile\V1;

use Linio\Controller\RouterAware;
use Linio\Frontend\Api\Output\CatalogResultOutput;
use Linio\Frontend\Api\Output\SellerOutput;
use Linio\Frontend\Entity\Catalog\FacetCollection;
use Linio\Frontend\Entity\Catalog\FacetInterface;
use Linio\Frontend\Entity\Catalog\Facets\CheckBox;
use Linio\Frontend\Entity\Catalog\Facets\MultipleChoice;
use Linio\Frontend\Entity\Catalog\Facets\Range;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Catalog\SearchResults;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\ProductImageTransformerAware;

class CatalogResult implements CatalogResultOutput
{
    use RouterAware;
    use ProductImageTransformerAware;

    /**
     * @var SellerOutput
     */
    protected $sellerOutput;

    public function setSellerOutput(SellerOutput $output)
    {
        $this->sellerOutput = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function fromSearchResult(SearchResult $searchResult): array
    {
        $data = [
            'totalItemsFound' => (int) $searchResult->getTotalItemsFound(),
            'didYouMean' => $searchResult->getDidYouMeanTerm(),
            'relatedQueries' => $searchResult->getRelatedQueries(),
            'currentPage' => (int) $searchResult->getCurrentPage(),
            'pageCount' => (int) $searchResult->getPageCount(),
            'guidedSearchTerms' => $searchResult->getGuidedSearchTerms(),
            'filters' => $this->mapFilters($searchResult->getFacetCollection()),
            'products' => [],
        ];

        foreach ($searchResult->getProducts() as $product) {
            $data['products'][] = $this->transformProduct($product);
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function fromSearchResults(SearchResults $searchResults): array
    {
        $searchResult = $searchResults->getPrimarySearchResult();

        return $this->fromSearchResult($searchResult);
    }

    /**
     * @param FacetCollection $facetCollection
     *
     * @return array
     */
    protected function mapFilters(FacetCollection $facetCollection)
    {
        $filters = [];

        /** @var FacetInterface $filter */
        foreach ($facetCollection->getValues() as $filter) {
            $transformedFilter = $this->transformFilter($filter);

            if (!empty($transformedFilter['values'])) {
                $filters[$filter->getName()] = $transformedFilter;
            }
        }

        return $filters;
    }

    /**
     * @param FacetInterface $filter
     *
     * @return array
     */
    protected function transformFilter(FacetInterface $filter)
    {
        $transformed = [
            'label' => $filter->getLabel(),
            'activeValues' => [],
            'values' => [],
        ];

        switch (true) {
            case $filter instanceof Range:
                $filter->getMinimum()->setScale(0);
                $filter->getMaximum()->setScale(0);

                $transformed['values'] = [
                    'min' => $filter->getMinimum()->getMoneyAmount(),
                    'max' => $filter->getMaximum()->getMoneyAmount(),
                ];

                if ($filter->getActiveMinimum() && $filter->getActiveMaximum()) {
                    $transformed['activeValues'] = [
                        'min' => $filter->getActiveMinimum()->getMoneyAmount(),
                        'max' => $filter->getActiveMaximum()->getMoneyAmount(),
                    ];
                }
                break;
            case $filter instanceof CheckBox:
                $transformed['activeValues'] = [$filter->getActiveValue()];
                $transformed['values'] = $filter->getValues();
                break;
            case $filter instanceof MultipleChoice:
                $transformed['activeValues'] = $filter->getActiveValues();
                $transformed['values'] = $filter->getValues();
                break;
        }

        return $transformed;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function transformProduct(Product $product)
    {
        $productItem = [
            'sku' => $product->getSku(),
            'name' => $product->getName() ?: '',
            'previousPrice' => $product->getOriginalPrice()->getMoneyAmount(),
            'actualPrice' => $product->getPrice()->getMoneyAmount(),
            'percentageOff' => $product->getPercentageOff(),
            'linioPlusLevel' => (int) $product->getLinioPlusLevel(),
            'hasFreeShipping' => (bool) $product->hasFreeShipping(),
            'isOutOfStock' => (bool) $product->isOutOfStock(),
            'deliveryTime' => (int) $product->getDeliveryTime(),
            'path' => $this->getProductPath($product),
            'image' => $this->getProductImage($product),
            'inWishLists' => $product->isInWishLists(),
            'oversized' => $product->isOversized(),
            'freeStorePickup' => $product->hasFreeStorePickup(),
        ];

        if ($product->getSeller()) {
            $productItem['seller'] = $this->sellerOutput->fromSeller($product->getSeller());
        } else {
            $productItem['seller'] = null;
        }

        return $productItem;
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    protected function getProductPath(Product $product)
    {
        return $this->generateUrl(
            'frontend.mobile.catalog.detail.v1',
            ['productSlug' => $product->getSlug()]
        );
    }

    /**
     * @param Product $product
     *
     * @return string
     */
    protected function getProductImage(Product $product)
    {
        $mainImage = null;

        foreach ($product->getImages() as $image) {
            if ($image->isMain()) {
                $mainImage = $image;
                break;
            }
        }

        // If no image is marked as main, use the first one
        if (is_null($mainImage) && !empty($product->getImages())) {
            $mainImage = current($product->getImages());
        }

        // If there are no images, return just an empty structure
        if (is_null($mainImage)) {
            return '';
        }

        $transformedCollection = $this->getImagesTransformed([$mainImage]);
        $transformed = current($transformedCollection);

        return $transformed['url'];
    }
}
