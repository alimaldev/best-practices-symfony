<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Entity\Seller\PaginatedReviews;
use Linio\Frontend\Entity\Seller\Review;
use Linio\Frontend\Entity\Seller\Seller as ReviewSeller;

interface SellerOutput
{
    /**
     * @param Seller $seller
     *
     * @return array
     */
    public function fromSeller(Seller $seller): array;

    /**
     * @param ReviewSeller $seller
     *
     * @return array
     */
    public function fromReviewSeller(ReviewSeller $seller): array;

    /**
     * @param Review $review
     *
     * @return array
     */
    public function fromReview(Review $review): array;

    /**
     * @param PaginatedReviews $paginatedReviews
     *
     * @return array
     */
    public function fromPaginatedReviews(PaginatedReviews $paginatedReviews): array;
}
