<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Entity\Customer\Wallet\Transaction;
use Linio\Frontend\Entity\Customer\Wallet\TransactionCollection;
use Linio\Frontend\Entity\Customer\Wallet\Wallet;

class MobileCustomerWallet implements CustomerWalletOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromCustomerWallet(Wallet $wallet): array
    {
        return [
            'account' => $wallet->getAccount(),
            'accruedPoints' => $wallet->getAccruedPoints()->getMoneyAmount(),
            'usedPoints' => $wallet->getUsedPoints()->getMoneyAmount(),
            'expiredPoints' => $wallet->getExpiredPoints()->getMoneyAmount(),
            'balance' => $wallet->getBalance()->getMoneyAmount(),
            'transactions' => $this->outputWalletTransactions($wallet->getTransactions()),
        ];
    }

    /**
     * @param TransactionCollection $transactions
     *
     * @return array
     */
    protected function outputWalletTransactions(TransactionCollection $transactions): array
    {
        $transactionsOutput = [];

        /** @var Transaction $transaction */
        foreach ($transactions as $transaction) {
            $transactionsOutput[] = [
                'createdAt' => $transaction->getCreatedAt()->format(DateTime::ISO8601),
                'charge' => $transaction->getCharge()->getMoneyAmount(),
                'payment' => $transaction->getPayment()->getMoneyAmount(),
                'validUntil' => $transaction->getValidUntil() ? $transaction->getValidUntil()->format(DateTime::ISO8601) : null,
                'status' => $transaction->getStatus(),
                'balance' => $transaction->getBalance()->getMoneyAmount(),
            ];
        }

        return $transactionsOutput;
    }
}
