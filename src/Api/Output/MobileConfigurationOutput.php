<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

interface MobileConfigurationOutput
{
    /**
     * @param array $parseData
     *
     * @return array
     */
    public function fromStoreConfiguration(array $parseData): array;

    /**
     * @param array $parseData
     * @param string $formName
     *
     * @return array
     */
    public function fromDynamicFormConfiguration(array $parseData, string $formName = ''): array;

    /**
     * @param array $parseData
     *
     * @return array
     */
    public function fromItemReturnConfiguration(array $parseData): array;
}
