<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Controller\RouterAware;
use Linio\Frontend\Product\ProductImageTransformerAware;
use Linio\Frontend\WishList\Product;

class MobileWishListProduct implements WishListProductOutput
{
    use RouterAware;
    use ProductImageTransformerAware;

    /**
     * @var FilteredProductDescriptionOutput
     */
    protected $productDescriptionFilter;

    /**
     * @param FilteredProductDescriptionOutput $productDescriptionFilter
     */
    public function setProductDescriptionFilter(FilteredProductDescriptionOutput $productDescriptionFilter)
    {
        $this->productDescriptionFilter = $productDescriptionFilter;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    public function fromProduct(Product $product): array
    {
        return [
            'id' => $product->getWishListId(),
            'name' => $product->getName(),
            'configSku' => $product->getSku(),
            'simpleSku' => $product->getSelectedSimple()->getSku(),
            'description' => $this->productDescriptionFilter->filteredProductDescription($product->getDescription() ?? ''),
            'price' => $product->getPrice()->getMoneyAmount(),
            'originalPrice' => $product->getOriginalPrice()->getMoneyAmount(),
            'percentageOff' => $product->getPercentageOff(),
            'wishListPrice' => $product->getWishListPrice()->getMoneyAmount(),
            'installments' => $product->getInstallments(),
            'installmentPrice' => $product->getInstallmentPrice()->getMoneyAmount(),
            'addedOn' => $product->getAddedOn() ? $product->getAddedOn()->format(DateTime::ISO8601) : null,
            'slug' => $product->getSlug(),
            'url' => $product->getSlug() ? $this->generateUrl('frontend.mobile.catalog.detail.v1', ['productSlug' => $product->getSlug()]) : null,
            'image' => $product->getImages() ? $this->transformMainImage($product->getImages())['url'] : null,
            'linioPlusLevel' => $product->getLinioPlusLevel(),
            'rating' => $product->getRating() ? $product->getRating()->getAveragePoint() : null,
            'daysToDeliver' => $product->getDeliveryTime(),
            'imported' => $product->isImported(),
            'active' => $product->isActive(),
        ];
    }
}
