<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Customer\Membership\Partnership;

class MobilePartnership implements PartnershipOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromPartnerships(array $partnerships): array
    {
        $outputPartnerships = [];

        /* @var Partnership $partnership */
        foreach ($partnerships as $partnership) {
            $outputPartnerships[] = [
                'partnership' => $this->fromPartnership($partnership),
                'accountNumber' => $partnership->getAccountNumber(),
                'level' => $partnership->getLevel(),
                'active' => $partnership->isActive(),
                'lastStatusCheck' => $partnership->getLastStatusCheck() ? $partnership->getLastStatusCheck()->format(DateTime::ISO8601) : null,
            ];
        }

        return $outputPartnerships;
    }

    protected function fromPartnership(Partnership $partnership)
    {
        return [
            'code' => $partnership->getCode(),
            'name' => $partnership->getName(),
        ];
    }
}
