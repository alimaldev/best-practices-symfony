<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\RecalculatedOrder;

interface OrderOutput
{
    /**
     * @param RecalculatedOrder $recalculatedOrder
     *
     * @return array
     */
    public function fromRecalculatedOrder(RecalculatedOrder $recalculatedOrder): array;

    /**
     * @param CompletedOrder $completedOrder
     *
     * @return array
     */
    public function fromCompletedOrder(CompletedOrder $completedOrder): array;
}
