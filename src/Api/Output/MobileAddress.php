<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Customer\Order\Shipping\Address as ShippingAddress;
use Linio\Frontend\Location\Address;

class MobileAddress implements AddressOutput
{
    /**
     * {@inheritdoc}
     */
    public function fromAddress(Address $address): array
    {
        return [
            'id' => $address->getId(),
            'title' => $address->getTitle(),
            'type' => $address->getType(),
            'firstName' => $address->getFirstName(),
            'lastName' => $address->getLastName(),
            'prefix' => $address->getPrefix(),
            'line1' => $address->getLine1(),
            'line2' => $address->getLine2(),
            'betweenStreet1' => $address->getBetweenStreet1(),
            'betweenStreet2' => $address->getBetweenStreet2(),
            'streetNumber' => $address->getStreetNumber(),
            'apartment' => $address->getApartment(),
            'lot' => $address->getLot(),
            'neighborhood' => $address->getNeighborhood(),
            'department' => $address->getDepartment(),
            'municipality' => $address->getMunicipality(),
            'urbanization' => $address->getUrbanization(),
            'city' => $address->getCity(),
            'cityId' => $address->getCityId(),
            'cityName' => $address->getCityName(),
            'region' => $address->getRegion(),
            'regionId' => $address->getRegionId(),
            'regionCode' => $address->getRegionCode(),
            'regionName' => $address->getRegionName(),
            'postcode' => $address->getPostcode(),
            'additionalInformation' => $address->getAdditionalInformation(),
            'phone' => $address->getPhone(),
            'mobilePhone' => $address->getMobilePhone(),
            'countryId' => $address->getCountryId(),
            'countryCode' => $address->getCountryCode(),
            'countryName' => $address->getCountryName(),
            'taxIdentificationNumber' => $address->getTaxIdentificationNumber(),
            'defaultBilling' => $address->isDefaultBilling(),
            'defaultShipping' => $address->isDefaultShipping(),
            'maternalName' => $address->getMaternalName(),
            'invoiceType' => $address->getInvoiceType(),
            'country' => $address->getCountryName(),
            'company' => $address->getCompany(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function fromAddresses(array $addresses): array
    {
        $addressList = [];

        foreach ($addresses as $address) {
            $addressList[$address->getId()] = $this->fromAddress($address);
        }

        return $addressList;
    }

    /**
     * {@inheritdoc}
     */
    public function fromShippingAddress(ShippingAddress $shippingAddress): array
    {
        $address = $this->fromAddress($shippingAddress);

        $address['alternativeRecipientName'] = $shippingAddress->getAlternativeRecipientName();
        $address['alternativeRecipientId'] = $shippingAddress->getAlternativeRecipientId();
        $address['directionPrefix'] = $shippingAddress->getDirectionPrefix();
        $address['pickupStore'] = null;

        if ($shippingAddress->isStorePickUp()) {
            $address['pickupStore'] = [
                'id' => $shippingAddress->getPickupStore()->getId(),
                'name' => $shippingAddress->getPickupStore()->getName(),
                'description' => $shippingAddress->getPickupStore()->getDescription(),
                'geohash' => $shippingAddress->getPickupStore()->getGeohash(),
                'itemCapacity' => $shippingAddress->getPickupStore()->getItemCapacity(),
                'weightCapacity' => $shippingAddress->getPickupStore()->getWeightCapacity(),
            ];
        }

        $address['nationalRegistrationNumber'] = $shippingAddress->getNationalRegistrationNumber();

        return $address;
    }
}
