<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\LinioPlusMembership;
use Linio\Frontend\Entity\PaginatedResult;
use Linio\Frontend\Product\ProductImageTransformerAware;

class MobileCustomer implements CustomerOutput
{
    use ProductImageTransformerAware;

    /**
     * {@inheritdoc}
     */
    public function fromCustomer(Customer $customer): array
    {
        $profile = [];

        $profile['id'] = $customer->getId();
        $profile['linio_id'] = $customer->getLinioId();
        $profile['first_name'] = $customer->getFirstName();
        $profile['last_name'] = $customer->getLastName();
        $profile['gender'] = $customer->getGender();
        $profile['bornDate'] = $customer->getBornDate() ? $customer->getBornDate()->format('Y-m-d H:i:s') : null;
        $profile['email'] = $customer->getEmail();
        $profile['tax_identification_number'] = $customer->getTaxIdentificationNumber();
        $profile['national_registration_number'] = $customer->getNationalRegistrationNumber();
        $profile['loyalty_program'] = null;
        $profile['loyalty_id'] = null;

        if ($customer->getLoyaltyProgram()) {
            $profile['loyalty_program'] = $customer->getLoyaltyProgram()->getName();
            $profile['loyalty_id'] = $customer->getLoyaltyProgram()->getId();
        }

        $profile['is_linio_plus'] = $customer->isSubscribedToLinioPlus();

        return $profile;
    }

    /**
     * {@inheritdoc}
     */
    public function fromLoyaltyProgram(LoyaltyProgram $loyaltyProgram): array
    {
        $loyaltyProgramData = [
            'name' => $loyaltyProgram->getName(),
            'accountNumber' => $loyaltyProgram->getId(),
        ];

        return $loyaltyProgramData;
    }

    /**
     * {@inheritdoc}
     */
    public function fromPlusMembership(LinioPlusMembership $plusMembership): array
    {
        return [
            'id' => $plusMembership->getId(),
            'reference' => $plusMembership->getReference(),
            'isActive' => $plusMembership->isActive(),
            'balance' => $plusMembership->getBalance() ? $plusMembership->getBalance()->getMoneyAmount() :  null,
            'startDate' => $plusMembership->getStartDate()->format(DateTime::ISO8601),
            'endDate' => $plusMembership->getExpirationDate()->format(DateTime::ISO8601),
            'canceledAt' => $plusMembership->getCancellationDate() ? $plusMembership->getCancellationDate()->format(DateTime::ISO8601) : null,
            'walletSubscriptionId' => $plusMembership->getWalletSubscription(),
            'createdAt' => $plusMembership->getCreateDate() ? $plusMembership->getCreateDate()->format(DateTime::ISO8601) : null,
            'updatedAt' => $plusMembership->getLastUpdated() ? $plusMembership->getLastUpdated()->format(DateTime::ISO8601) : null,
        ];
    }

    /**
     * @param array $customerReviews
     *
     * @return array $reviews
     */
    public function fromCustomerReviews(array $customerReviews): array
    {
        $reviews = [];

        foreach ($customerReviews as $review => $reviewValue) {
            $reviews[$review]['orderNumber'] = $reviewValue['orderNumber'];
            $reviews[$review]['datePurchase'] = !empty($reviewValue['orderDate']) ? $reviewValue['orderDate']->format(DateTime::ISO8601) : null;

            foreach ($reviewValue['sellers'] as $seller => $sellerValue) {
                $reviews[$review]['sellers'][$seller]['name'] = $sellerValue['name'];
                $reviews[$review]['sellers'][$seller]['slug'] = $sellerValue['seller']->getSlug();
                $sellerReview = $sellerValue['review'];

                foreach ($sellerValue['products'] as $product => $productValues) {
                    $imageMain = $productValues->getImages() ? $this->transformMainImage($productValues->getImages()) : null;

                    $reviews[$review]['sellers'][$seller]['products'][] = [
                        'sku' => $productValues->getSku(),
                        'name' => $productValues->getName(),
                        'slug' => $productValues->getSlug(),
                        'imageUrl' => $imageMain['url'] ?? '',
                    ];
                }

                if (!empty($sellerReview)) {
                    $reviews[$review]['sellers'][$seller]['review'] = [
                        'title' => $sellerReview->getTitle(),
                        'detail' => $sellerReview->getDetail(),
                        'rating' => $sellerReview->getRating(),
                    ];
                }
            }
        }

        return $reviews;
    }

    /**
     * @param PaginatedResult $paginatedCustomerReviews
     *
     * @return array
     */
    public function fromPaginatedCustomerReviews(PaginatedResult $paginatedCustomerReviews): array
    {
        return [
            'currentPage' => $paginatedCustomerReviews->getCurrent(),
            'totalPages' => $paginatedCustomerReviews->getPages(),
            'customerReviews' => $this->fromCustomerReviews($paginatedCustomerReviews->getResult()),
        ];
    }
}
