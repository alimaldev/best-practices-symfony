<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Customer\Order;
use Linio\Frontend\Customer\Order\Shipping\Shipment;
use Linio\Frontend\Entity\PaginatedResult;

interface CustomerOrderOutput
{
    /**
     * @param Order $order
     *
     * @return array
     */
    public function fromOrder(Order $order): array;

    /**
     * @param PaginatedResult $paginatedResult
     *
     * @return array
     */
    public function fromPaginatedOrderList(PaginatedResult $paginatedResult): array;

    /**
     * @param Shipment[] $shipments
     *
     * @return array
     */
    public function fromShipments(array $shipments): array;

    /**
     * @param Order[]
     *
     * @return array
     */
    public function fromPendingOrders(array $orders): array;
}
