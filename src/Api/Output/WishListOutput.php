<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\WishList\WishList;

interface WishListOutput
{
    /**
     * @param WishList $wishList
     *
     * @return array
     */
    public function fromWishList(WishList $wishList): array;

    /**
     * @param array $wishLists
     *
     * @return array
     */
    public function fromWishLists(array $wishLists): array;
}
