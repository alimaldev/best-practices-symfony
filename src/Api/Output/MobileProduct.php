<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use DateTime;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Bundle;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Product\MarketplaceChild;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Product\ProductImageTransformerAware;
use Symfony\Component\Translation\TranslatorInterface;

class MobileProduct implements ProductOutput
{
    use ProductImageTransformerAware;

    /**
     * @var CacheService
     */
    protected $attributeCacheService;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var FilteredProductDescriptionOutput
     */
    protected $filteredProductDescriptionOutput;

    /**
     * @var SellerOutput
     */
    protected $sellerOutput;

    /**
     * @var CacheService
     */
    protected $configurationCacheService;

    /**
     * @param CacheService $configurationCacheService
     */
    public function setConfigurationCacheService(CacheService $configurationCacheService)
    {
        $this->configurationCacheService = $configurationCacheService;
    }

    /**
     * @param CacheService $attributeCacheService
     */
    public function setAttributeCacheService(CacheService $attributeCacheService)
    {
        $this->attributeCacheService = $attributeCacheService;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param FilteredProductDescriptionOutput $filteredProductDescriptionOutput
     */
    public function setFilteredProductDescriptionOutput(FilteredProductDescriptionOutput $filteredProductDescriptionOutput)
    {
        $this->filteredProductDescriptionOutput = $filteredProductDescriptionOutput;
    }

    /**
     * @param SellerOutput $sellerOutput
     */
    public function setSellerOutput(SellerOutput $sellerOutput)
    {
        $this->sellerOutput = $sellerOutput;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    public function fromProduct(Product $product): array
    {
        $outputProduct = [];

        $outputProduct['bundles'] = [];
        $outputProduct['sku'] = $product->getSku();
        $outputProduct['name'] = $product->getName();
        $outputProduct['slug'] = $product->getSlug();
        $outputProduct['brand'] = $this->getBrandAsArray($product->getBrand());
        $outputProduct['seller'] = $this->sellerOutput->fromSeller($product->getSeller());
        $outputProduct['description'] = $this->filteredProductDescriptionOutput->filteredProductDescription($product->getDescription());
        $outputProduct['linioPlusLevel'] = (int) $product->getLinioPlusLevel();
        $outputProduct['simples'] = $this->getSimplesAsArray($product->getSimples());
        $outputProduct['marketplaceChildren'] = $this->getMarketplaceChildrenAsArray($product->getMarketplaceChildren());
        $outputProduct['quantity'] = $product->getQuantity();
        $outputProduct['stock'] = (int) $product->getStock();
        $outputProduct['shippingTimeFrom'] = $product->getShippingTimeFrom();
        $outputProduct['shippingTimeTo'] = $product->getShippingTimeTo();
        $outputProduct['attributes'] = $this->getFilteredAttributes($product->getAttributes()->toArray());
        $outputProduct['rating'] = $this->getProductRatingAsArray($product->getRating());
        $outputProduct['outOfStock'] = $product->isOutOfStock();
        $outputProduct['images'] = $this->getImagesTransformed($product->getImages());
        $outputProduct['category'] = $this->getCategoryAsArray($product->getCategory());
        $outputProduct['imported'] = $product->isImported();
        $outputProduct['returnAllowed'] = $product->isReturnAllowed();
        $outputProduct['postPaymentAllowed'] = $product->isPostPaymentAllowed();
        $outputProduct['groupedProducts'] = $product->getGroupedProducts();
        $outputProduct['marketplace'] = $product->isMarketPlace();
        $outputProduct['variationType'] = $product->getVariationType();
        $outputProduct['fulfillmentType'] = $product->getFulfillmentType();
        $outputProduct['marketplaceParentSku'] = $product->getMarketplaceParentSku();
        $outputProduct['unitPrice'] = $product->getUnitPrice()->getMoneyAmount();
        $outputProduct['price'] = $product->getPrice()->getMoneyAmount();
        $outputProduct['originalPrice'] = $product->getOriginalPrice()->getMoneyAmount();
        $outputProduct['percentageOff'] = $product->getPercentageOff();
        $outputProduct['savedAmount'] = $product->getSavedAmount()->getMoneyAmount();
        $outputProduct['installmentPrice'] = $product->getInstallmentPrice()->getMoneyAmount();
        $outputProduct['installments'] = $product->getInstallments();
        $outputProduct['configId'] = $product->getConfigId();
        $outputProduct['attributeSet'] = $product->getAttributeSet();
        $outputProduct['campaignSlug'] = $product->getCampaignSlug();
        $outputProduct['attributeLabels'] = $this->getAttributeLabels($product);
        $outputProduct['inWishLists'] = $product->isInWishLists();
        $outputProduct['oversized'] = $product->isOversized();
        $outputProduct['freeStorePickup'] = $product->hasFreeStorePickup();
        $outputProduct['fulfillmentLabel'] = $this->getFulfillmentLabel($product->getSeller(), $product->getFulfillmentType());
        $outputProduct = array_merge($outputProduct, $this->getProductDeliveryTime($product));
        $outputProduct = array_merge($outputProduct, $this->getProductDisclaimers($product));

        if ($product->getBundles()) {
            foreach ($product->getBundles() as $bundle) {
                $outputProduct['bundles'][] = $this->getBundleAsArray($bundle);
            }
        }

        return $outputProduct;
    }

    /**
     * @param MarketplaceChild $child
     *
     * @return array
     */
    public function fromFilteredMarketplaceChild(MarketplaceChild $child): array
    {
        return [
            'sku' => $child->getSku(),
            'seller' => $this->sellerOutput->fromSeller($child->getSeller()),
            'stock' => $child->getStock(),
            'active' => $child->isActive(),
            'attributes' => $this->getFilteredAttributes($child->getAttributes()),
            'price' => $child->getPrice()->getMoneyAmount(),
            'originalPrice' => $child->getOriginalPrice()->getMoneyAmount(),
            'hasSpecialPrice' => $child->hasSpecialPrice(),
            'percentageOff' => $child->getPercentageOff(),
            'savedAmount' => $child->getSavedAmount()->getMoneyAmount(),
            'linioPlusLevel' => $child->getLinioPlusLevel(),
            'hasFreeShipping' => $child->hasFreeShipping(),
        ];
    }

    /**
     * @param Simple $simple
     *
     * @return array
     */
    public function fromFilteredSimple(Simple $simple): array
    {
        return [
            'sku' => $simple->getSku(),
            'stock' => $simple->getStock(),
            'active' => $simple->isActive(),
            'attributes' => $this->getFilteredAttributes($simple->getAttributes()),
            'price' => $simple->getPrice()->getMoneyAmount(),
            'originalPrice' => $simple->getOriginalPrice()->getMoneyAmount(),
            'hasSpecialPrice' => $simple->hasSpecialPrice(),
            'percentageOff' => $simple->getPercentageOff(),
            'savedAmount' => $simple->getSavedAmount()->getMoneyAmount(),
            'linioPlusLevel' => $simple->getLinioPlusLevel(),
            'hasFreeShipping' => $simple->hasFreeShipping(),
        ];
    }

    /**
     * @param string[] $attributes
     *
     * @return array
     */
    protected function getFilteredAttributes(array $attributes): array
    {
        if (isset($attributes['short_description'])) {
            $attributes['short_description'] = $this->filteredProductDescriptionOutput
                ->filteredShortDescription($attributes['short_description']);
        }

        return $attributes;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function getProductDisclaimers(Product $product): array
    {
        $disclaimerCategories = $this->configurationCacheService->get('disclaimer_categories');

        $disclaiming = [
            'disclaimersStatus' => false,
            'disclaimersMessages' => [],
        ];

        if (!isset($disclaimerCategories)) {
            return $disclaiming;
        }

        foreach ($disclaimerCategories as $disclaimerBlock) {
            if (in_array($product->getCategory()->getId(), $disclaimerBlock['categories'])) {
                $disclaiming['disclaimersStatus'] = true;
                $disclaiming['disclaimersMessages'][] = $this->translator->trans($disclaimerBlock['message']);
            }
        }

        return $disclaiming;
    }

    /**
     * @param Brand $brand
     *
     * @return array
     */
    protected function getBrandAsArray(Brand $brand): array
    {
        return [
            'id' => $brand->getId(),
            'name' => $brand->getName(),
            'slug' => $brand->getSlug(),
        ];
    }

    /**
     * @param Simple[] $simples
     *
     * @return array
     */
    protected function getSimplesAsArray(array $simples): array
    {
        $arraySimples = [];

        foreach ($simples as $simple) {
            $arraySimples[] = [
                'sku' => $simple->getSku(),
                'stock' => (int) $simple->getStock(),
                'active' => $simple->isActive(),
                'attributes' => $this->getFilteredAttributes($simple->getAttributes()),
                'price' => $simple->getPrice()->getMoneyAmount(),
                'originalPrice' => $simple->getOriginalPrice()->getMoneyAmount(),
                'hasSpecialPrice' => $simple->hasSpecialPrice(),
                'percentageOff' => $simple->getPercentageOff(),
                'savedAmount' => $simple->getSavedAmount()->getMoneyAmount(),
                'linioPlusLevel' => $simple->getLinioPlusLevel(),
                'hasFreeShipping' => (bool) $simple->hasFreeShipping(),
            ];
        }

        return $arraySimples;
    }

    /**
     * @param MarketplaceChild[] $marketplaceChilds
     *
     * @return array
     */
    protected function getMarketplaceChildrenAsArray(array $marketplaceChilds): array
    {
        $arrayMarketplaceChilds = [];

        foreach ($marketplaceChilds as $marketplaceChild) {
            $arrayMarketplaceChilds[] = [
                'sku' => $marketplaceChild->getSku(),
                'seller' => $this->sellerOutput->fromSeller($marketplaceChild->getSeller()),
                'stock' => (int) $marketplaceChild->getStock(),
                'active' => $marketplaceChild->isActive(),
                'attributes' => $this->getFilteredAttributes($marketplaceChild->getAttributes()),
                'price' => $marketplaceChild->getPrice()->getMoneyAmount(),
                'originalPrice' => $marketplaceChild->getOriginalPrice()->getMoneyAmount(),
                'hasSpecialPrice' => $marketplaceChild->hasSpecialPrice(),
                'percentageOff' => $marketplaceChild->getPercentageOff(),
                'savedAmount' => $marketplaceChild->getSavedAmount()->getMoneyAmount(),
                'linioPlusLevel' => $marketplaceChild->getLinioPlusLevel(),
                'hasFreeShipping' => (bool) $marketplaceChild->hasFreeShipping(),
                'fulfillmentLabel' => $this->getFulfillmentLabel($marketplaceChild->getSeller(), $marketplaceChild->getFulfillmentType()),
            ];
        }

        return $arrayMarketplaceChilds;
    }

    /**
     * @param Rating $rating
     *
     * @return array
     */
    protected function getProductRatingAsArray(Rating $rating): array
    {
        $reviews = [];

        $transformedRating = [
            'averagePoint' => $rating->getAveragePoint(),
            'reviewsQuantity' => $rating->getReviewsQuantity(),
            'voteSummaryTotals' => $rating->getVoteSummaryTotals(),
        ];

        foreach ($rating->getReviews() as $review) {
            $reviews[] = $this->getReviewAsArray($review);
        }

        $transformedRating['reviews'] = $reviews;

        return $transformedRating;
    }

    /**
     * @param Review $review
     *
     * @return array
     */
    protected function getReviewAsArray(Review $review): array
    {
        return [
            'title' => $review->getTitle(),
            'comment' => $review->getComment(),
            'customerName' => $review->getCustomerName(),
            'createdAt' => $review->getCreatedAt()->format(DateTime::ISO8601),
            'experienceAverage' => $review->getExperienceAverage(),
        ];
    }

    /**
     * @param Category $category
     *
     * @return array
     */
    protected function getCategoryAsArray(Category $category): array
    {
        return [
            'id' => $category->getId(),
            'name' => $category->getName(),
            'slug' => $category->getSlug(),
            'urlKey' => $category->getUrlKey(),
            'isCurrent' => $category->isCurrent(),
            'count' => $category->getCount(),
        ];
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function getProductDeliveryTime(Product $product): array
    {
        return [
            'deliveredByChristmas' => $product->isDeliveredByChristmas(),
            'deliveryTime' => $product->getDeliveryTime(),
            'deliveryTimeFromFirstSimple' => $product->getDeliveryTimeFromFirstSimple(),
        ];
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function getAttributeLabels(Product $product): array
    {
        $attributeLabels = [];
        $attributeSet = $this->attributeCacheService->get('attributes');

        // Product attributes
        foreach ($product->getAttributes()->getKeys() as $attribute) {
            if (isset($attributeSet[$attribute])) {
                $attributeLabels[$attribute] = $attributeSet[$attribute];
            }
        }

        // Simples attributes
        foreach ($product->getSimples() as $simple) {
            foreach ($simple->getAttributes() as $key => $attribute) {
                if (isset($attributeSet[$key])) {
                    $attributeLabels[$key] = $attributeSet[$key];
                }
            }
        }

        // MarketplaceChildren attributes
        foreach ($product->getMarketplaceChildren() as $marketplaceChild) {
            foreach ($marketplaceChild->getAttributes() as $key => $attribute) {
                if (isset($attributeSet[$key])) {
                    $attributeLabels[$key] = $attributeSet[$key];
                }
            }
        }

        return $attributeLabels;
    }

    /**
     * @param Seller $seller
     * @param string|null $fulfillmentType
     *
     * @return string
     */
    public function getFulfillmentLabel(Seller $seller, string $fulfillmentType = null): string
    {
        if ($seller->getType() === Seller::SELLER_TYPE_MERCHANT) {
            $fulfillmentType = $fulfillmentType ?? Product::FULFILLMENT_TYPE_SOLD;

            return $this->translator->trans('product_details.' . $fulfillmentType);
        }

        return $this->translator->trans('product_details.sold_and_fulfilled_by_linio');
    }

    /**
     * @param Bundle $bundle
     *
     * @return array
     */
    protected function getBundleAsArray(Bundle $bundle): array
    {
        $transformedBundle = [
            'name' => $bundle->getName(),
            'discount' => $bundle->getDiscount(),
            'startDate' => $bundle->getStartDate()->format(DateTime::ISO8601),
            'endDate' => $bundle->getEndDate()->format(DateTime::ISO8601),
            'products' => [],
            'savingPrice' => 20,
            'type' => 'bundle',
        ];

        foreach ($bundle->getProducts() as $product) {
            $transformedBundle['products'][] = [
                'name' => $product->getName(),
                'originalPrice' => $product->getOriginalPrice()->getMoneyAmount(),
                'price' => $product->getPrice()->getMoneyAmount(),
                'selected' => $product->isSelected(),
                'sku' => $product->getSku(),
                'sprite' => $product->getSprite(),
            ];
        }

        return $transformedBundle;
    }
}
