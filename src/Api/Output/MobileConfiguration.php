<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Request\PlatformInformation;
use Symfony\Component\Translation\TranslatorInterface;

class MobileConfiguration implements MobileConfigurationOutput
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param PlatformInformation $platformInformation
     */
    public function setPlatformInformation(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    /**
     * @param array $parseData
     *
     * @return array
     */
    public function fromStoreConfiguration(array $parseData): array
    {
        $resultParameters = [];
        $formNames = array_keys($parseData['dynamic_form']);

        if ($parseData['parameters']) {
            $resultParameters = [
                'countryId' => $parseData['parameters']['country_id'] ?? null,
                'storeId' => $this->platformInformation->getStoreId(),
                'minimumVersionSupported' => $parseData['minimumVersionSupported'] ?? null,
                'locale' => $parseData['parameters']['locale'] ?? '',
                'countryName' => $parseData['parameters']['country_name'] ?? '',
                'availablePaymentMethods' => $parseData['parameters']['available_payment_methods'] ?? null,
                'displayTaxes' => $parseData['parameters']['display_taxes'] ?? false,
                'displayDiscounts' => $parseData['parameters']['display_discounts'] ?? false,
                'displayPriceLabels' => $parseData['parameters']['display_price_labels'] ?? false,
                'loyaltyProgram' => $parseData['parameters']['loyalty_program'] ?? '',
                'linioPlusEnabled' => $parseData['parameters']['linio_plus_enabled'] ?? false,
                'extraCheckoutFieldsEnabled' => $parseData['parameters']['extra_checkout_fields_enabled'] ?? false,
                'requestInvoiceEnabled' => $parseData['parameters']['request_invoice_enabled'] ?? false,
                'walletEnabled' => $parseData['parameters']['wallet_enabled'] ?? false,
                'taxIdRequired' => $parseData['parameters']['tax_id_required'] ?? false,
                'newsletterFrequency' => $parseData['parameters']['newsletter_frequency'] ?? null,
                'dateFormats' => $parseData['formatter']['date'] ?? null,
                'currencyFormats' => $parseData['formatter']['currency'] ?? null,
                'forms' => $formNames ?? null,
            ];
        }

        return $resultParameters;
    }

    /**
     * @param array $parseData
     * @param string $formName
     *
     * @return array
     */
    public function fromDynamicFormConfiguration(array $parseData, string $formName = ''): array
    {
        $resultParameters['dynamic_form'][$formName] = $parseData['dynamic_form'][$formName] ?? null;

        return $this->formsArray($resultParameters, $formName);
    }

    /**
     * @param array $resultParameters
     * @param string $formName
     *
     * @return array
     */
    protected function formsArray(array $resultParameters, $formName): array
    {
        $finalResultParameters = [];

        foreach ($resultParameters['dynamic_form'][$formName] as $key => &$field) {
            $finalResultParameters['name'] = $formName;
            $field['name'] = $key;

            if (array_key_exists('attr', $field['options'])) {
                foreach ($field['options']['attr'] as $index => $attribute) {
                    if (!in_array($index, ['placeholder', 'mask'])) {
                        unset($field['options']['attr'][$index]);
                    } elseif ($index == 'placeholder') {
                        $field['options']['attr']['placeholder'] = $this->translator->trans($attribute);
                    }
                }

                if (empty($field['options']['attr'])) {
                    $field['options']['attr'] = null;
                }
            }

            if (isset($field['validation'])) {
                $field['validation'] = $this->validationForms($field['validation']);
            } else {
                $field['validation'] = null;
            }

            $field['options']['label'] = strip_tags($this->translator->trans($field['options']['label'] ?? ''));

            if (isset($field['options']['choices'])) {
                foreach ($field['options']['choices'] as $choiceKey => $choice) {
                    $field['options']['choices'][$choiceKey] = $this->translator->trans($choice);
                }
            }

            if (isset($field['options']['first_options'])) {
                foreach (['first_options', 'second_options'] as $optionField) {
                    $field['options'][$optionField]['label'] = strip_tags($this->translator->trans($field['options'][$optionField]['label']));
                }
            }

            $finalResultParameters['fields'][] = $field;
        }

        return $finalResultParameters;
    }

    /**
     * @param array $validation
     *
     * @return array
     */
    protected function validationForms(array $validation)
    {
        foreach ($validation as $key => &$validate) {
            if (strpos($key, 'Regex')) {
                $validation['Regex'] = $validate['pattern'];
                unset($validation[$key]);
                break;
            }

            if (strpos($key, 'Length')) {
                $validation['max'] = $validate['max'];
                unset($validation[$key]);
                break;
            }

            unset($validation[$key]);
        }

        return empty($validation) ? null : $validation;
    }

    public function fromItemReturnConfiguration(array $parseData): array
    {
        $itemReturnConfigurationOutput = [];

        foreach ($parseData['actions'] as $actionId => $action) {
            $actionData['id'] = $actionId;
            $actionData['label'] = $action;

            $actions[] = $actionData;
        }

        foreach ($parseData['reasons'] as $reasonId => $reason) {
            $reasonData['id'] = $reasonId;
            $reasonData['label'] = $reason;

            $reasons[] = $reasonData;
        }

        $itemReturnConfigurationOutput['actions'] = $actions;
        $itemReturnConfigurationOutput['reasons'] = $reasons;

        return $itemReturnConfigurationOutput;
    }
}
