<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Customer\CreditCard;

interface StoredCreditCardOutput
{
    /**
     * @param CreditCard $creditCard
     *
     * @return array
     */
    public function fromCreditCard(CreditCard $creditCard): array;

    /**
     * @param CreditCard[] $creditCards
     *
     * @return array
     */
    public function fromCreditCards(array $creditCards): array;
}
