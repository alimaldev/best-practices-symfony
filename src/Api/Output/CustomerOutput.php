<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Entity\Customer\LinioPlusMembership;
use Linio\Frontend\Entity\PaginatedResult;

interface CustomerOutput
{
    /**
     * @param Customer $customer
     *
     * @return array
     */
    public function fromCustomer(Customer $customer): array;

    /**
     * @param LoyaltyProgram $loyaltyProgram
     *
     * @return array
     */
    public function fromLoyaltyProgram(LoyaltyProgram $loyaltyProgram): array;

    /**
     * @param LinioPlusMembership $plusMembership
     *
     * @return array
     */
    public function fromPlusMembership(LinioPlusMembership $plusMembership): array;

    /**
     * @param array $customerReviews
     *
     * @return array
     */
    public function fromCustomerReviews(array $customerReviews): array;

    /**
     * @param PaginatedResult $paginatedCustomerReviews
     *
     * @return array
     */
    public function fromPaginatedCustomerReviews(PaginatedResult $paginatedCustomerReviews): array;
}
