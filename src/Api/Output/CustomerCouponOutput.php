<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Output;

use Linio\Frontend\Entity\Customer\Coupon;

interface CustomerCouponOutput
{
    /**
     * @param Coupon[] $coupons
     *
     * @return array
     */
    public function fromCoupons(array $coupons): array;

    /**
     * @param Coupon $coupon
     *
     * @return array
     */
    public function fromCoupon(Coupon $coupon): array;
}
