<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Exception\InputException;

interface SellerInput
{
    /**
     * @param array $data
     *
     * @throws InputException
     *
     * @return SellerReview
     */
    public function toSellerReview(array $data) : SellerReview;
}
