<?php

namespace Linio\Frontend\Api\Input;

use Linio\Controller\FormAware;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Form\Customer\LoyaltyProgramForm;

class MobileLoyaltyProgram implements LoyaltyProgramInput
{
    use FormAware;

    /**
     * @var string
     */
    protected $loyaltyProgram;

    /**
     * @param string $loyaltyProgram
     */
    public function setLoyaltyProgram(string $loyaltyProgram)
    {
        $this->loyaltyProgram = $loyaltyProgram;
    }

    public function fromLoyaltyProgram(array $loyaltyProgramData): LoyaltyProgram
    {
        $form = $this->formFactory->create(LoyaltyProgramForm::class, null, ['csrf_protection' => false]);
        $form->submit($loyaltyProgramData);

        if (!$form->isValid()) {
            throw new FormValidationException($form, ExceptionMessage::INVALID_DATA);
        }

        $formData = $form->getData();
        $loyaltyProgram = new LoyaltyProgram();
        $loyaltyProgram->setName($this->loyaltyProgram);
        $loyaltyProgram->setId($formData['loyaltyId']);

        return $loyaltyProgram;
    }
}
