<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Membership\Partnership;
use Linio\Frontend\Exception\InputException;

interface PartnershipInput
{
    /**
     * @param array $data
     *
     * @throws InputException
     *
     * @return Partnership
     */
    public function fromPartnershipData(array $data): Partnership;
}
