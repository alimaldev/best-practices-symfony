<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Controller\FormAware;
use Linio\Frontend\Customer\Order\SellerReview;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Form\SellerReviewForm;
use Linio\Frontend\Seller\SellerService;

class MobileSeller implements SellerInput
{
    use FormAware;

    /**
     * @var SellerService
     */
    protected $sellerService;

    /**
     * @param SellerService $sellerService
     */
    public function setSellerService(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * @param array $data
     *
     * @throws InputException
     *
     * @return SellerReview
     */
    public function toSellerReview(array $data) : SellerReview
    {
        $form = $this->formFactory->create(SellerReviewForm::class, null, ['csrf_protection' => false]);
        $form->submit($data);

        if (!$form->isValid()) {
            throw new FormValidationException($form, ExceptionMessage::INVALID_DATA);
        }

        $seller = $this->sellerService->get($data['sellerId']);

        $sellerReview = $form->getData();
        $sellerReview->setSeller($seller);

        return $sellerReview;
    }
}
