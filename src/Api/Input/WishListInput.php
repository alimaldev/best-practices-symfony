<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\WishList\WishList;

interface WishListInput
{
    /**
     * @param array $data
     * @param Customer $customer
     *
     * @throws InputException
     *
     * @return WishList
     */
    public function fromCreateWishList(array $data, Customer $customer): WishList;

    /**
     * @param int $wishListId
     * @param array $data
     * @param Customer $customer
     *
     * @throws InputException
     *
     * @return WishList
     */
    public function fromEditWishList(int $wishListId, array $data, Customer $customer): WishList;

    /**
     * @param int $wishListId
     * @param Customer $customer
     *
     * @return WishList
     */
    public function fromId(int $wishListId, Customer $customer): WishList;
}
