<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Newsletter\NewsletterSubscription;

interface NewsletterInput
{
    /**
     * @param array $data
     * @param NewsletterSubscription $newsletterSubscription
     *
     * @return NewsletterSubscription
     */
    public function fromNewsletterSettings(array $data, NewsletterSubscription $newsletterSubscription): NewsletterSubscription;
}
