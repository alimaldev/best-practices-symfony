<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Location\Address;

interface AddressInput
{
    /**
     * @param array $data
     *
     * @return Address
     */
    public function fromLocationParts(array $data): Address;

    /**
     * @param array $data
     *
     * @return Address
     */
    public function fromPartialAddress(array $data): Address;
}
