<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Order\Order;

interface OrderInput
{
    /**
     * @param array $data
     * @param Order $order
     *
     * @return Order
     */
    public function fromPlaceOrder(array $data, Order $order): Order;
}
