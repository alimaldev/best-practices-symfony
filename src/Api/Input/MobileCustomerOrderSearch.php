<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use DateTime;
use Linio\Frontend\Customer\Order\Search;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Type\Date;

class MobileCustomerOrderSearch implements CustomerOrderSearchInput
{
    /**
     * @param array $data
     *
     * @throws InputException
     *
     * @return Search
     */
    public function fromCustomerOrderSearch(array $data): Search
    {
        $fromDate = isset($data['fromDate']) ? DateTime::createFromFormat(DateTime::ISO8601, $data['fromDate']) : null;
        $toDate = isset($data['toDate']) ? DateTime::createFromFormat(DateTime::ISO8601, $data['toDate']) : null;

        if (!isset($data['page']) || $fromDate === false || $toDate === false) {
            throw new InputException(ExceptionMessage::ORDER_SEARCH_PAGE_INVALID);
        }

        $search = new Search();
        $search->setPage($data['page']);

        if (isset($fromDate)) {
            $search->setFromDate(Date::createFromDateTime($fromDate));
        }

        if (isset($toDate)) {
            $search->setToDate(Date::createFromDateTime($toDate));
        }

        return $search;
    }
}
