<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Controller\FormAware;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Form\Customer\ItemReturnRequestForm;

class MobileItemReturn implements ItemReturnInput
{
    use FormAware;

    /**
     * {@inheritdoc}
     */
    public function fromItemReturn(array $data): array
    {
        $form = $this->createForm(ItemReturnRequestForm::class, null, ['csrf_protection' => false]);
        $form->submit($data);

        if (!$form->isValid()) {
            throw new FormValidationException($form);
        }

        return $form->getData();
    }
}
