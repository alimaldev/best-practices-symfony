<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Exception\InputException;
use Linio\Frontend\WishList\Product;

interface WishListProductInput
{
    /**
     * @param array $data
     *
     * @throws InputException
     *
     * @return Product
     */
    public function fromAddProduct(array $data): Product;

    /**
     * @param array $data
     *
     * @throws InputException
     *
     * @return Product
     */
    public function fromProductToRemove(array $data): Product;
}
