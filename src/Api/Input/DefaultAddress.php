<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Location\Address;

class DefaultAddress implements AddressInput
{
    /**
     * {@inheritdoc}
     */
    public function fromLocationParts(array $data): Address
    {
        $address = new Address();

        if (isset($data['postcode'])) {
            $address->setPostcode($data['postcode']);
        }

        if (isset($data['region'])) {
            $address->setRegion($data['region']);
        }

        if (isset($data['municipality'])) {
            $address->setMunicipality($data['municipality']);
        }

        if (isset($data['city'])) {
            $address->setCity($data['city']);
        }

        return $address;
    }

    /**
     * {@inheritdoc}
     */
    public function fromPartialAddress(array $data): Address
    {
        $address = new Address();

        if (isset($data['id'])) {
            $address->setId($data['id']);
        }

        if (isset($data['title'])) {
            $address->setTitle($data['title']);
        }

        if (isset($data['type'])) {
            $address->setType($data['type']);
        }

        if (isset($data['firstName'])) {
            $address->setFirstName($data['firstName']);
        }

        if (isset($data['lastName'])) {
            $address->setLastName($data['lastName']);
        }

        if (isset($data['prefix'])) {
            $address->setPrefix($data['prefix']);
        }

        if (isset($data['line1'])) {
            $address->setLine1($data['line1']);
        }

        if (isset($data['line2'])) {
            $address->setLine2($data['line2']);
        }

        if (isset($data['betweenStreet1'])) {
            $address->setBetweenStreet1($data['betweenStreet1']);
        }

        if (isset($data['betweenStreet2'])) {
            $address->setBetweenStreet2($data['betweenStreet2']);
        }

        if (isset($data['streetNumber'])) {
            $address->setStreetNumber($data['streetNumber']);
        }

        if (isset($data['apartment'])) {
            $address->setApartment($data['apartment']);
        }

        if (isset($data['lot'])) {
            $address->setLot($data['lot']);
        }

        if (isset($data['neighborhood'])) {
            $address->setNeighborhood($data['neighborhood']);
        }

        if (isset($data['department'])) {
            $address->setDepartment($data['department']);
        }

        if (isset($data['municipality'])) {
            $address->setMunicipality($data['municipality']);
        }

        if (isset($data['urbanization'])) {
            $address->setUrbanization($data['urbanization']);
        }

        if (isset($data['city'])) {
            $address->setCity($data['city']);
        }

        if (isset($data['cityId'])) {
            $address->setCityId($data['cityId']);
        }

        if (isset($data['cityName'])) {
            $address->setCityName($data['cityName']);
        }

        if (isset($data['region'])) {
            $address->setRegion($data['region']);
        }

        if (isset($data['regionId'])) {
            $address->setRegionId($data['regionId']);
        }

        if (isset($data['regionCode'])) {
            $address->setRegionCode($data['regionCode']);
        }

        if (isset($data['regionName'])) {
            $address->setRegionName($data['regionName']);
        }

        if (isset($data['postcode'])) {
            $address->setPostcode($data['postcode']);
        }

        if (isset($data['additionalInformation'])) {
            $address->setAdditionalInformation($data['additionalInformation']);
        }

        if (isset($data['phone'])) {
            $address->setPhone($data['phone']);
        }

        if (isset($data['mobilePhone'])) {
            $address->setMobilePhone($data['mobilePhone']);
        }

        if (isset($data['countryId'])) {
            $address->setCountryId($data['countryId']);
        }

        if (isset($data['countryCode'])) {
            $address->setCountryCode($data['countryCode']);
        }

        if (isset($data['countryName'])) {
            $address->setCountryName($data['countryName']);
        }

        if (isset($data['taxIdentificationNumber'])) {
            $address->setTaxIdentificationNumber($data['taxIdentificationNumber']);
        }

        if (isset($data['defaultBilling'])) {
            $address->setDefaultBilling($data['defaultBilling']);
        }

        if (isset($data['defaultShipping'])) {
            $address->setDefaultShipping($data['defaultShipping']);
        }

        if (isset($data['maternalName'])) {
            $address->setMaternalName($data['maternalName']);
        }

        if (isset($data['invoiceType'])) {
            $address->setInvoiceType($data['invoiceType']);
        }

        if (isset($data['company'])) {
            $address->setCompany($data['company']);
        }

        return $address;
    }
}
