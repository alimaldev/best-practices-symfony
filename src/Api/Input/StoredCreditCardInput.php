<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer\CreditCard;
use Linio\Frontend\Exception\FormValidationException;

interface StoredCreditCardInput
{
    /**
     * @param array $creditCardData
     *
     * @throws FormValidationException
     *
     * @return CreditCard
     */
    public function fromCreditCard(array $creditCardData): CreditCard;
}
