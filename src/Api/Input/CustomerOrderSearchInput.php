<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Order\Search;

interface CustomerOrderSearchInput
{
    /**
     * @param array $data
     *
     * @return Search
     */
    public function fromCustomerOrderSearch(array $data): Search;
}
