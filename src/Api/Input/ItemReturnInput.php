<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

interface ItemReturnInput
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function fromItemReturn(array $data): array;
}
