<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Controller\FormAware;
use Linio\Frontend\Customer\Membership\Partnership;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\Form\PartnershipForm;

class MobilePartnership implements PartnershipInput
{
    use FormAware;

    /**
     * {@inheritdoc}
     */
    public function fromPartnershipData(array $data): Partnership
    {
        $form = $this->formFactory->create(PartnershipForm::class, null, ['csrf_protection' => false]);
        $form->submit($data);

        if (!$form->isValid()) {
            throw new FormValidationException($form, ExceptionMessage::INVALID_DATA);
        }

        $formData = $form->getData();

        $partnership = new Partnership($formData['code'], $formData['accountNumber']);

        if (isset($formData['level'])) {
            $partnership->setLevel($formData['level']);
        }

        return $partnership;
    }
}
