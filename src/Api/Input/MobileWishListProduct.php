<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use DateTime;
use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\WishList\Product;
use Linio\Type\Money;

class MobileWishListProduct implements WishListProductInput
{
    /**
     * {@inheritdoc}
     */
    public function fromAddProduct(array $data): Product
    {
        $product = $this->fromProductToRemove($data);

        if (!isset($data['price'])) {
            throw new InputException(ExceptionMessage::WISH_LIST_INVALID_PRODUCT);
        }

        $product->setWishListPrice(new Money($data['price']));
        $product->setAddedOn(new DateTime());
        $product->setName($data['name']);

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function fromProductToRemove(array $data): Product
    {
        if (!isset($data['simpleSku'])) {
            throw new InputException(ExceptionMessage::WISH_LIST_INVALID_PRODUCT);
        }

        $simple = new Simple();
        $simple->setSku($data['simpleSku']);

        $product = new Product($simple);

        return $product;
    }
}
