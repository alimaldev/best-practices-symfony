<?php

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Newsletter\NewsletterSubscription;

class DefaultNewsletter implements NewsletterInput
{
    /**
     * {@inheritdoc}
     */
    public function fromNewsletterSettings(array $data, NewsletterSubscription $newsletterSubscription): NewsletterSubscription
    {
        $newsletterSubscription->setFrequency($data['frequency']);

        if ($data['subscribedToSms']) {
            $newsletterSubscription->subscribeToSms();
            $newsletterSubscription->setPhoneNumber($data['phoneNumber']);
        } else {
            $newsletterSubscription->unsubscribeFromSms();
        }

        foreach ($newsletterSubscription->getPreferences() as $preference) {
            if (empty($data['preferences'])) {
                $preference->disable();
                continue;
            }

            if (in_array($preference->getId(), $data['preferences'])) {
                $preference->enable();
            } else {
                $preference->disable();
            }
        }

        return $newsletterSubscription;
    }
}
