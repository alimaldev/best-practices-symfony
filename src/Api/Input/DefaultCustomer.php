<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer\BankTransferConfirmation;
use Linio\Frontend\Exception\InputException;

class DefaultCustomer implements CustomerInput
{
    /**
     * @param array $transferData
     *
     * @throws InputException
     *
     * @return BankTransferConfirmation
     */
    public function fromConfirmBankTransfer(array $transferData): BankTransferConfirmation
    {
        $bankTransferConfirmation = new BankTransferConfirmation();
        $bankTransferConfirmation->setBankId($transferData['bankId']);
        $bankTransferConfirmation->setDate($transferData['date']);
        $bankTransferConfirmation->setNumber($transferData['transferNumber']);
        $bankTransferConfirmation->setAmount($transferData['amount']);

        return $bankTransferConfirmation;
    }
}
