<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer\BankTransferConfirmation;

interface CustomerInput
{
    /**
     * @param array $transferData
     *
     * @return BankTransferConfirmation
     */
    public function fromConfirmBankTransfer(array $transferData): BankTransferConfirmation;
}
