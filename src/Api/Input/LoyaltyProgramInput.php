<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Customer\Membership\LoyaltyProgram;

interface LoyaltyProgramInput
{
    /**
     * @param array $loyaltyProgramData
     *
     * @return LoyaltyProgram
     */
    public function fromLoyaltyProgram(array $loyaltyProgramData): LoyaltyProgram;
}
