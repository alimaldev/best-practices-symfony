<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Controller\FormAware;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Linio\Frontend\WishList\ValidateWishList;
use Linio\Frontend\WishList\WishList;

class MobileWishList implements WishListInput
{
    use FormAware;

    /**
     * {@inheritdoc}
     */
    public function fromCreateWishList(array $data, Customer $customer): WishList
    {
        $form = $this->formFactory->create(ValidateWishList::class, null, ['csrf_protection' => false]);

        $form->submit($data);

        if (!$form->isValid()) {
            throw new FormValidationException($form, ExceptionMessage::WISH_LIST_INVALID_DATA);
        }

        $wishListData = $form->getData();

        $wishList = new WishList($wishListData['name'], $customer);
        $wishList->setDescription($wishListData['description'] ?? '');

        return $wishList;
    }

    /**
     * {@inheritdoc}
     */
    public function fromEditWishList(int $wishListId, array $data, Customer $customer): WishList
    {
        $wishList = $this->fromCreateWishList($data, $customer);

        $wishList->setId($wishListId);

        return $wishList;
    }

    /**
     * {@inheritdoc}
     */
    public function fromId(int $wishListId, Customer $customer): WishList
    {
        $wishList = new WishList('', $customer);
        $wishList->setId($wishListId);

        return $wishList;
    }
}
