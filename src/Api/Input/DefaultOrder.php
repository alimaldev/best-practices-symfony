<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use DateTime;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\Packages;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Linio\Frontend\Order\Shipping\ShippingQuotes;
use Linio\Frontend\Order\Wallet;
use Linio\Type\Money;

class DefaultOrder implements OrderInput
{
    /**
     * @param array $data
     * @param Order $order
     *
     * @throws InputException
     *
     * @return Order
     */
    public function fromPlaceOrder(array $data, Order $order): Order
    {
        $order->setGrandTotal(new Money($data['grandTotal']));

        if (!empty($data['coupon'])) {
            $order->applyCoupon(new Coupon($data['coupon']));
        }

        if (!empty($data['pickupStoreId'])) {
            $pickupStore = new PickupStore($data['pickupStoreId']);

            $order->getPickupStores()->add($pickupStore);
            $order->getPickupStores()->select($data['pickupStoreId']);
        }

        if (!empty($data['installments'])) {
            $order->setSelectedInstallmentQuantity($data['installments']);
        }

        $packages = new Packages();

        foreach ($data['packages'] as $packageId => $selectedQuote) {
            $packages->add($this->createPackage($packageId, $selectedQuote));
        }

        $order->setPackages($packages);

        $order->getItems()->clear();
        foreach ($data['items'] as $sku => $quantity) {
            $order->addItem(new Item($sku, $quantity));
        }

        $shippingAddress = $this->createAddress($data['shippingAddress']);
        $order->setShippingAddress($shippingAddress);

        if (!empty($data['billingAddress'])) {
            $billingAddress = $this->createAddress($data['billingAddress']);
            $order->setBillingAddress($billingAddress);
        }

        if (empty($data['paymentMethod'])) {
            throw new InputException(ExceptionMessage::PAYMENT_METHOD_NOT_ALLOWED, ['required' => 'paymentMethod']);
        }

        $order->setPaymentMethod(PaymentMethodFactory::fromName($data['paymentMethod']));

        if (!empty($data['dependentPaymentMethod'])) {
            $order->setDependentPaymentMethod($data['dependentPaymentMethod']);
        }

        if (!empty($data['paymentData'])) {
            $order->getPaymentMethod()->setPaymentData($data['paymentData']);
        }

        if ($order->getPaymentMethod() instanceof CreditCard && isset($data['creditCardBinNumber'])) {
            $order->getPaymentMethod()->setBinNumber($data['creditCardBinNumber']);
        }

        if (!empty($data['extraFields'])) {
            $order->setMetadata($data['extraFields']);
        }

        if (isset($data['walletPoints'])) {
            $wallet = new Wallet();
            $wallet->setTotalPointsUsed(Money::fromCents($data['walletPoints']));
            $order->setWallet($wallet);
        }

        return $order;
    }

    protected function createAddress(array $data): Address
    {
        $address = new Address();

        if (isset($data['id'])) {
            $address->setId($data['id']);
        }

        if (isset($data['postcode'])) {
            $address->setPostcode($data['postcode']);
        }

        if (isset($data['region'])) {
            $address->setRegion($data['region']);
        }

        if (isset($data['municipality'])) {
            $address->setMunicipality($data['municipality']);
        }

        if (isset($data['city'])) {
            $address->setCity($data['city']);
        }

        return $address;
    }

    /**
     * @param int $packageId
     * @param array $data
     *
     * @throws InputException
     *
     * @return Package
     */
    protected function createPackage(int $packageId, array $data): Package
    {
        if (!isset($data['shippingMethod'])) {
            throw new InputException(ExceptionMessage::INVALID_REQUEST, ['missingField' => ['shippingMethod']]);
        }

        $shippingQuote = new ShippingQuote($data['shippingMethod'], new DateTime());
        $shippingQuote->select();

        $shippingQuotes = new ShippingQuotes();
        $shippingQuotes->add($shippingQuote);

        $package = new Package();
        $package->setId($packageId);
        $package->setShippingQuotes($shippingQuotes);

        return $package;
    }
}
