<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Input;

use Linio\Frontend\Entity\Customer\CreditCard;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Exception\FormValidationException;
use Symfony\Component\Form\FormInterface;

class MobileStoredCreditCard implements StoredCreditCardInput
{
    /**
     * @var FormInterface
     */
    protected $addCreditCardForm;

    /**
     * @param FormInterface $addCreditCardForm
     */
    public function setAddCreditCardForm(FormInterface $addCreditCardForm)
    {
        $this->addCreditCardForm = $addCreditCardForm;
    }

    /**
     * {@inheritdoc}
     */
    public function fromCreditCard(array $creditCardData): CreditCard
    {
        $this->addCreditCardForm->submit($creditCardData);

        if (!$this->addCreditCardForm->isValid()) {
            throw new FormValidationException($this->addCreditCardForm, ExceptionMessage::INVALID_DATA);
        }

        /** @var CreditCard $creditCard */
        $creditCard = $this->addCreditCardForm->getData();

        $creditCard->setCardholderName(
            sprintf('%s %s', $creditCard->getCardholderFirstName(), $creditCard->getCardholderLastName())
        );

        return $creditCard;
    }
}
