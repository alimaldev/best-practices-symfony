<?php

declare(strict_types=1);

namespace Linio\Frontend\Api;

use Linio\Frontend\Api\Input\NewsletterInput;
use Linio\Frontend\Api\Output\NewsletterOutput;

trait NewsletterTransputAware
{
    /**
     * @var NewsletterInput
     */
    protected $newsletterInput;

    /**
     * @var NewsletterOutput
     */
    protected $newsletterOutput;

    /**
     * @param NewsletterInput $newsletterInput
     */
    public function setNewsletterInput(NewsletterInput $newsletterInput)
    {
        $this->newsletterInput = $newsletterInput;
    }

    /**
     * @param NewsletterOutput $newsletterOutput
     */
    public function setNewsletterOutput(NewsletterOutput $newsletterOutput)
    {
        $this->newsletterOutput = $newsletterOutput;
    }
}
