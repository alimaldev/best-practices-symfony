<?php

declare(strict_types=1);

namespace Linio\Frontend\Api\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorJsonResponse extends JsonResponse
{
    public function __construct(
        string $message,
        $code = JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
        array $context = null
    ) {
        if (is_array($context) && empty($context)) {
            $context = null;
        }

        parent::__construct(['message' => $message, 'context' => $context], $code);
    }
}
