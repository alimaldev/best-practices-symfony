<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Carbon\Carbon;
use DateTime;
use Linio\Frontend\Customer\AddressBookAware;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Communication\BuildOrder\BuildOrderAdapter;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Exception\PaymentMethodNotAllowedException;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Linio\Frontend\Order\Storage\Storage;
use Linio\Frontend\Security\GuestUser;
use Linio\Type\Money;

class BuildOrder
{
    use AddressBookAware;

    /**
     * @var BuildOrderAdapter
     */
    protected $adapter;

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @param BuildOrderAdapter $adapter
     */
    public function setAdapter(BuildOrderAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param Storage $storage
     */
    public function setStorage(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function addProduct(Order $order, Item $item): RecalculatedOrder
    {
        $order->addItem($item);

        $recalculatedOrder = $this->adapter->addProduct($order, $item);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function removeProduct(Order $order, Item $item): RecalculatedOrder
    {
        $order->getItems()->remove($item->getSku());

        $recalculatedOrder = $this->adapter->removeProduct($order, $item);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function updateQuantity(Order $order, Item $item): RecalculatedOrder
    {
        $orderItem = $order->getItems()->get($item->getSku());

        if (!$orderItem) {
            throw new OrderException('order.product_not_found');
        }

        $orderItem->setQuantity($item->getQuantity());
        $recalculatedOrder = $this->adapter->updateQuantity($order, $item);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param Coupon $coupon
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function applyCoupon(Order $order, Coupon $coupon): RecalculatedOrder
    {
        $order->applyCoupon($coupon);

        $recalculatedOrder = $this->adapter->applyCoupon($order, $coupon);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function removeCoupon(Order $order): RecalculatedOrder
    {
        $order->removeCoupon();

        $recalculatedOrder = $this->adapter->removeCoupon($order);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param Address $shippingAddress
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function setShippingAddress(Order $order, Address $shippingAddress): RecalculatedOrder
    {
        $order->setShippingAddress($shippingAddress);

        $recalculatedOrder = $this->adapter->setShippingAddress($order, $shippingAddress);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param int $packageId
     * @param string $shippingMethod
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function updateShippingMethod(Order $order, int $packageId, string $shippingMethod): RecalculatedOrder
    {
        $recalculatedOrder = $this->adapter->updateShippingMethod($order, $packageId, $shippingMethod);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param PaymentMethod $paymentMethod
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function updatePaymentMethod(Order $order, PaymentMethod $paymentMethod): RecalculatedOrder
    {
        $order->setPaymentMethod($paymentMethod);

        $recalculatedOrder = $this->adapter->updatePaymentMethod($order, $paymentMethod);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param int $points
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function useWalletPoints(Order $order, int $points): RecalculatedOrder
    {
        $wallet = new Wallet();
        $wallet->setTotalPointsUsed(Money::fromCents($points));
        $order->setWallet($wallet);

        $recalculatedOrder = $this->adapter->useWalletPoints($order, $points);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order
     * @param int $quantity
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function selectInstallmentQuantity(Order $order, int $quantity): RecalculatedOrder
    {
        $order->setSelectedInstallmentQuantity($quantity);

        $recalculatedOrder = $this->adapter->selectInstallmentQuantity($order, $quantity);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param string $geohash
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setGeohash(Order $order, string $geohash): RecalculatedOrder
    {
        $order->getPickupStores()->clear();
        $order->setGeohash($geohash);

        $recalculatedOrder = $this->adapter->setGeohash($order, $geohash);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param int $pickupStoreId
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setPickupStore(Order $order, int $pickupStoreId): RecalculatedOrder
    {
        $pickupStore = new PickupStore($pickupStoreId);

        $order->getPickupStores()->add($pickupStore);
        $order->getPickupStores()->select($pickupStoreId);

        $recalculatedOrder = $this->adapter->setPickupStore($order, $pickupStoreId);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param string $binNumber
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws PaymentMethodNotAllowedException Thrown for invalid payment methods
     *
     * @return RecalculatedOrder
     */
    public function setBinNumber(Order $order, string $binNumber): RecalculatedOrder
    {
        $paymentMethod = $order->getPaymentMethod();

        if (!$paymentMethod instanceof CreditCard) {
            throw new PaymentMethodNotAllowedException();
        }

        $paymentMethod->setBinNumber($binNumber);

        $recalculatedOrder = $this->adapter->setBinNumber($order, $binNumber);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function recalculateOrder(Order $order): RecalculatedOrder
    {
        $recalculatedOrder = $this->adapter->recalculateOrder($order);
        $this->setDefaultShippingAddress($recalculatedOrder->getOrder(), $order->getCustomer());

        return $recalculatedOrder;
    }

    /**
     * @param Customer $customer
     * @param bool $updateAddresses
     *
     * @return Order
     */
    public function getOrderInProgress(Customer $customer, bool $updateAddresses = true): Order
    {
        try {
            $order = $this->storage->retrieve($customer->getLinioId());
        } catch (OrderException $exception) {
            $order = new Order($customer->getLinioId());
        }

        $order->setCustomer($customer);

        if ($updateAddresses) {
            $this->setDefaultShippingAddress($order, $customer);
        }

        return $order;
    }

    /**
     * @param Order $order
     */
    public function saveOrderInProgress(Order $order)
    {
        $order->setUpdatedAt(new Carbon());
        $this->storage->store($order);
    }

    /**
     * @param Customer $customer
     * @param array|null $partialOrder
     *
     * @return Order
     */
    public function mergeOrderWithCache(Customer $customer, array $partialOrder = null): Order
    {
        $order = $this->getOrderInProgress($customer, false);

        if (empty($partialOrder)) {
            $this->setDefaultShippingAddress($order, $customer);

            return $order;
        }

        if (isset($partialOrder['pickupStoreId'])) {
            $pickupStore = new PickupStore($partialOrder['pickupStoreId']);

            $order->getPickupStores()->add($pickupStore);
            $order->getPickupStores()->select($partialOrder['pickupStoreId']);
        }

        if (isset($partialOrder['shippingAddressId'])) {
            $shippingAddress = new Address();
            $shippingAddress->setId($partialOrder['shippingAddressId']);

            $order->setShippingAddress($shippingAddress);
        }

        if (isset($partialOrder['paymentMethod'])) {
            $paymentMethod = PaymentMethodFactory::fromName($partialOrder['paymentMethod']);

            if ($paymentMethod instanceof CreditCard && isset($partialOrder['creditCardBinNumber'])) {
                $paymentMethod->setBinNumber($partialOrder['creditCardBinNumber']);
            }

            $order->setPaymentMethod($paymentMethod);
        }

        if (isset($partialOrder['selectedShippingOptions'])) {
            foreach ($partialOrder['selectedShippingOptions'] as $packageId => $selectedShippingOption) {
                $package = new Package();
                $package->setId($packageId);

                $shippingQuote = new ShippingQuote($selectedShippingOption, new Datetime());

                $package->getShippingQuotes()->add($shippingQuote);
                $package->getShippingQuotes()->select($selectedShippingOption);

                $order->getPackages()->add($package);
            }
        }

        if (isset($partialOrder['installments'])) {
            $order->setSelectedInstallmentQuantity($partialOrder['installments']);
        }

        if (isset($partialOrder['walletPoints'])) {
            $wallet = new Wallet();
            $wallet->setTotalPointsUsed(Money::fromCents($partialOrder['walletPoints']));
            $order->setWallet($wallet);
        }

        $this->setDefaultShippingAddress($order, $customer);

        return $order;
    }

    /**
     * @param string $anonymousLinioId
     * @param Customer $customer
     */
    public function mergeItems(string $anonymousLinioId, Customer $customer)
    {
        try {
            $anonymousOrder = $this->storage->retrieve($anonymousLinioId);
            $this->storage->clear($anonymousOrder->getId());
        } catch (OrderException $exception) {
            return;
        }

        $newOrder = $this->getOrderInProgress($customer);

        /** @var Item $item */
        foreach ($anonymousOrder->getItems() as $item) {
            if ($newOrder->getItems()->hasItem($item->getSku())) {
                $newOrder->getItems()->get($item->getSku())->setQuantity($item->getQuantity());
            } else {
                $newOrder->addItem($item);
            }
        }

        $recalculatedOrder = $this->adapter->recalculateOrder($newOrder);
        $this->saveOrderInProgress($recalculatedOrder->getOrder());
    }

    /**
     * @param Order $order
     * @param Customer $customer
     */
    protected function setDefaultShippingAddress(Order $order, Customer $customer)
    {
        if ($customer instanceof GuestUser) {
            return;
        }

        $addresses = $this->addressBook->getAddresses($customer);
        $defaultShipping = null;

        foreach ($addresses as $address) {
            if ($order->getShippingAddress() && $order->getShippingAddress()->getId() == $address->getId()) {
                $order->setShippingAddress($address);

                return;
            }

            if ($address->isDefaultShipping()) {
                $defaultShipping = $address;
            }
        }

        if ($defaultShipping) {
            $order->setShippingAddress($defaultShipping);
        }
    }
}
