<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Exception;

class OrderPaymentFailureException extends CheckoutException
{
    const MESSAGE = 'ORDER_PAYMENT_FAILURE';
}
