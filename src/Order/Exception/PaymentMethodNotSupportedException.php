<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Exception;

class PaymentMethodNotSupportedException extends PaymentMethodException
{
    const PAYMENT_METHOD_NOT_SUPPORTED = 'PAYMENT METHOD NOT SUPPORTED';
    const MAPPER_NOT_FOUND = 'PAYMENT METHOD MAPPER NOT FOUND';
}
