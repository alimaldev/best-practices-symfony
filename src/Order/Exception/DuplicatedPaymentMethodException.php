<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Exception;

class DuplicatedPaymentMethodException extends PaymentMethodException
{
    const DUPLICATED_PAYMENT_METHOD = 'DUPLICATED PAYMENT METHOD';
}
