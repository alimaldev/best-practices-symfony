<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Exception;

use Linio\Frontend\Exception\ExceptionMessage;

class PaymentMethodNotAllowedException extends OrderException
{
    public function __construct()
    {
        parent::__construct(ExceptionMessage::PAYMENT_METHOD_NOT_ALLOWED);
    }
}
