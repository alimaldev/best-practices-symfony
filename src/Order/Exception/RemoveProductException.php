<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Exception;

class RemoveProductException extends OrderException
{
}
