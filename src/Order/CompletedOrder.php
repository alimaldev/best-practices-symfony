<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Frontend\Order\Payment\PaymentRedirect;

class CompletedOrder extends Order
{
    /**
     * @var PaymentRedirect
     */
    protected $paymentRedirect;

    /**
     * @var array
     */
    protected $paymentResult;

    public function getPaymentRedirect(): PaymentRedirect
    {
        return $this->paymentRedirect;
    }

    public function setPaymentRedirect(PaymentRedirect $paymentRedirect)
    {
        $this->paymentRedirect = $paymentRedirect;
    }

    public function hasPaymentRedirect(): bool
    {
        return (bool) $this->paymentRedirect;
    }

    public function getPaymentResult(): array
    {
        return $this->paymentResult;
    }

    public function setPaymentResult(array $paymentResult)
    {
        $this->paymentResult = $paymentResult;
    }
}
