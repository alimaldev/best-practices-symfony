<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Carbon\Carbon;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Payment\InstallmentOptions;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\Shipping\Packages;
use Linio\Frontend\Order\Shipping\PickupStores;
use Linio\Type\Money;

class Order
{
    /**
     * @var string Cart or Linio ID
     */
    protected $id;

    /**
     * @var int
     */
    protected $salesOrderId;

    /**
     * @var string
     */
    protected $orderNumber;

    /**
     * @var string
     */
    protected $transactionId;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var Items
     */
    protected $items;

    /**
     * @var Address
     */
    protected $billingAddress;

    /**
     * @var Address
     */
    protected $shippingAddress;

    /**
     * @var string
     */
    protected $geohash;

    /**
     * @var PickupStores
     */
    protected $pickupStores;

    /**
     * @var Coupon
     */
    protected $coupon;

    /**
     * @var InstallmentOptions
     */
    protected $installmentOptions;

    /**
     * @var int
     */
    protected $selectedInstallmentQuantity;

    /**
     * @var Money
     */
    protected $subTotal;

    /**
     * @var Money
     */
    protected $grandTotal;

    /**
     * @var Money
     */
    protected $taxAmount;

    /**
     * @var Money|null
     */
    protected $shippingAmount;

    /**
     * @var Money|null
     */
    protected $originalShippingAmount;

    /**
     * @var Money
     */
    protected $shippingDiscountAmount;

    /**
     * @var Money
     */
    protected $totalDiscountAmount;

    /**
     * @var Money
     */
    protected $linioPlusSavedAmount;

    /**
     * @var Money
     */
    protected $partnershipDiscount;

    /**
     * @var int
     */
    protected $loyaltyPointsAccrued = 0;

    /**
     * @var Packages
     */
    protected $packages;

    /**
     * @var PaymentMethod[]
     */
    protected $availablePaymentMethods = [];

    /**
     * @var PaymentMethod
     */
    protected $paymentMethod;

    /**
     * @var string
     */
    protected $dependentPaymentMethod;

    /**
     * @var bool
     */
    protected $displayTaxes = false;

    /**
     * @var bool This is needed to flag whether or not to show the calculate text on cart
     */
    protected $shippingAmountCalculated = false;

    /**
     * @var Partnership[]
     */
    protected $partnerships = [];

    /**
     * @var Wallet
     */
    protected $wallet;

    /**
     * @var array
     */
    protected $metadata = [];

    /**
     * @var string
     */
    protected $ipAddress;

    /**
     * @var Carbon
     */
    protected $updatedAt;

    /**
     * @var bool
     */
    protected $storePickupEnabled = false;

    /**
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
        $this->subTotal = new Money();
        $this->grandTotal = new Money();
        $this->taxAmount = new Money();
        $this->shippingDiscountAmount = new Money();
        $this->totalDiscountAmount = new Money();
        $this->linioPlusSavedAmount = new Money();
        $this->partnershipDiscount = new Money();
        $this->packages = new Packages();
        $this->installmentOptions = new InstallmentOptions();
        $this->items = new Items();
        $this->updatedAt = new Carbon();
        $this->pickupStores = new PickupStores();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSalesOrderId()
    {
        return $this->salesOrderId;
    }

    /**
     * @param int $salesOrderId
     */
    public function setSalesOrderId(int $salesOrderId)
    {
        $this->salesOrderId = $salesOrderId;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber(string $orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param string $transactionId
     */
    public function setTransactionId(string $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Items
     */
    public function getItems(): Items
    {
        return $this->items;
    }

    /**
     * @param Items $items
     */
    public function setItems(Items $items)
    {
        $this->items = $items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->items->add($item);
    }

    /**
     * @return Money
     */
    public function getSubTotal(): Money
    {
        return $this->subTotal;
    }

    /**
     * @param Money $subTotal
     */
    public function setSubTotal(Money $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return Money
     */
    public function getGrandTotal(): Money
    {
        return $this->grandTotal;
    }

    /**
     * @param Money $grandTotal
     */
    public function setGrandTotal(Money $grandTotal)
    {
        $this->grandTotal = $grandTotal;
    }

    /**
     * @return Money
     */
    public function getTaxAmount(): Money
    {
        return $this->taxAmount;
    }

    /**
     * @param Money $taxAmount
     */
    public function setTaxAmount(Money $taxAmount)
    {
        $this->taxAmount = $taxAmount;
    }

    /**
     * @return Money|null
     */
    public function getShippingAmount()
    {
        return $this->shippingAmount;
    }

    /**
     * @param Money $shippingAmount
     */
    public function setShippingAmount(Money $shippingAmount)
    {
        $this->shippingAmount = $shippingAmount;
        $this->shippingAmountCalculated = true;
    }

    /**
     * @return Money|null
     */
    public function getOriginalShippingAmount()
    {
        return $this->originalShippingAmount;
    }

    /**
     * @param Money $originalShippingAmount
     */
    public function setOriginalShippingAmount(Money $originalShippingAmount)
    {
        $this->originalShippingAmount = $originalShippingAmount;
    }

    /**
     * @return Money
     */
    public function getShippingDiscountAmount(): Money
    {
        return $this->shippingDiscountAmount;
    }

    /**
     * @param Money $shippingDiscountAmount
     */
    public function setShippingDiscountAmount(Money $shippingDiscountAmount)
    {
        $this->shippingDiscountAmount = $shippingDiscountAmount;
    }

    /**
     * @return Coupon|null
     */
    public function getCoupon()
    {
        return $this->coupon;
    }

    /**
     * @param Coupon $coupon
     */
    public function applyCoupon(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    public function removeCoupon()
    {
        $this->coupon = null;
    }

    /**
     * @return InstallmentOptions
     */
    public function getInstallmentOptions()
    {
        return $this->installmentOptions;
    }

    /**
     * @param InstallmentOptions $installmentOptions
     */
    public function setInstallmentOptions(InstallmentOptions $installmentOptions)
    {
        $this->installmentOptions = $installmentOptions;
    }

    /**
     * @return int|null
     */
    public function getSelectedInstallmentQuantity()
    {
        return $this->selectedInstallmentQuantity;
    }

    /**
     * @param int $selectedInstallmentQuantity
     */
    public function setSelectedInstallmentQuantity(int $selectedInstallmentQuantity)
    {
        $this->selectedInstallmentQuantity = $selectedInstallmentQuantity;
    }

    /**
     * @return Packages
     */
    public function getPackages(): Packages
    {
        return $this->packages;
    }

    /**
     * @param Packages $packages
     */
    public function setPackages(Packages $packages)
    {
        $this->packages = $packages;
    }

    /**
     * @return Money
     */
    public function getTotalDiscountAmount(): Money
    {
        return $this->totalDiscountAmount;
    }

    /**
     * @param Money $totalDiscountAmount
     */
    public function setTotalDiscountAmount(Money $totalDiscountAmount)
    {
        $this->totalDiscountAmount = $totalDiscountAmount;
    }

    /**
     * @return Money
     */
    public function getLinioPlusSavedAmount(): Money
    {
        return $this->linioPlusSavedAmount;
    }

    /**
     * @param Money $linioPlusSavedAmount
     */
    public function setLinioPlusSavedAmount(Money $linioPlusSavedAmount)
    {
        $this->linioPlusSavedAmount = $linioPlusSavedAmount;
    }

    /**
     * @return Money
     */
    public function getPartnershipDiscount(): Money
    {
        return $this->partnershipDiscount;
    }

    /**
     * @param Money $partnershipDiscount
     */
    public function setPartnershipDiscount(Money $partnershipDiscount)
    {
        $this->partnershipDiscount = $partnershipDiscount;
    }

    /**
     * @return int
     */
    public function getLoyaltyPointsAccrued(): int
    {
        return $this->loyaltyPointsAccrued;
    }

    /**
     * @param int $loyaltyPointsAccrued
     */
    public function setLoyaltyPointsAccrued(int $loyaltyPointsAccrued)
    {
        $this->loyaltyPointsAccrued = $loyaltyPointsAccrued;
    }

    /**
     * @return bool
     */
    public function displayTaxes(): bool
    {
        return $this->displayTaxes;
    }

    /**
     * @param bool $displayTaxes
     */
    public function setDisplayTaxes(bool $displayTaxes)
    {
        $this->displayTaxes = $displayTaxes;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param Address $billingAddress
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return Address
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @param Address $shippingAddress
     */
    public function setShippingAddress(Address $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * @return string|null
     */
    public function getGeohash()
    {
        return $this->geohash;
    }

    /**
     * @param string $geohash
     */
    public function setGeohash(string $geohash)
    {
        $this->geohash = $geohash;
    }

    /**
     * @return PickupStores
     */
    public function getPickupStores(): PickupStores
    {
        return $this->pickupStores;
    }

    /**
     * @param PickupStores $pickupStores
     */
    public function setPickupStores(PickupStores $pickupStores)
    {
        $this->pickupStores = $pickupStores;
    }

    /**
     * @return PaymentMethod[]
     */
    public function getAvailablePaymentMethods(): array
    {
        return $this->availablePaymentMethods;
    }

    /**
     * @param PaymentMethod[] $availablePaymentMethods
     */
    public function setAvailablePaymentMethods(array $availablePaymentMethods)
    {
        $this->availablePaymentMethods = $availablePaymentMethods;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param PaymentMethod $paymentMethod
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getDependentPaymentMethod()
    {
        return $this->dependentPaymentMethod;
    }

    /**
     * @param string $dependentPaymentMethod
     */
    public function setDependentPaymentMethod(string $dependentPaymentMethod)
    {
        $this->dependentPaymentMethod = $dependentPaymentMethod;
    }

    /**
     * @return bool
     */
    public function isShippingAmountCalculated(): bool
    {
        return $this->shippingAmountCalculated;
    }

    /**
     * @return Wallet|null
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param Wallet $wallet
     */
    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;
    }

    /**
     * @return Partnership[]
     */
    public function getPartnerships(): array
    {
        return $this->partnerships;
    }

    /**
     * @param Partnership[] $partnerships
     */
    public function setPartnerships(array $partnerships)
    {
        $this->partnerships = $partnerships;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     */
    public function setUpdatedAt(Carbon $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->items->isEmpty();
    }

    /**
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata(array $metadata)
    {
        if (isset($metadata['invoiceType'])) {
            $metadata['invoiceRequired'] = true;
        }

        if (isset($metadata['taxIdentificationNumberPrefix'])) {
            $metadata['taxIdentificationNumber'] = $metadata['taxIdentificationNumberPrefix'] . $metadata['taxIdentificationNumber'];
        }

        $this->metadata = $metadata;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     */
    public function setIpAddress(string $ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    public function enableStorePickup()
    {
        $this->storePickupEnabled = true;
    }

    /**
     * @return bool
     */
    public function isStorePickupEnabled(): bool
    {
        return $this->storePickupEnabled;
    }

    /**
     * @return bool
     */
    public function isFullWalletPayment(): bool
    {
        if (!$this->wallet) {
            return false;
        }

        if ($this->wallet->getTotalPointsUsed()->isPositive() && $this->getGrandTotal()->isZero()) {
            return true;
        }

        return false;
    }
}
