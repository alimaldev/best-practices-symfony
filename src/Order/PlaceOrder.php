<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Order\Communication\PlaceOrder\PlaceOrderAdapter;
use Linio\Frontend\Order\Exception\CheckoutException;
use Linio\Frontend\Order\Exception\DuplicatedPaymentMethodException;
use Linio\Frontend\Order\Exception\PaymentMethodNotSupportedException;
use Linio\Frontend\Order\Payment\ExternalPaymentResult;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\Storage\Storage;

class PlaceOrder
{
    /**
     * @var PlaceOrderAdapter
     */
    protected $adapter;

    /**
     * @var Storage
     */
    protected $storage;

    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @var PaymentMethod[]
     */
    protected $paymentMethods = [];

    /**
     * @codeCoverageIgnore
     *
     * @param PlaceOrderAdapter $adapter
     */
    public function setAdapter(PlaceOrderAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param Storage $storage
     */
    public function setStorage(Storage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @throws DuplicatedPaymentMethodException
     */
    public function addPaymentMethod(PaymentMethod $paymentMethod)
    {
        if (in_array($paymentMethod->getName(), array_keys($this->paymentMethods))) {
            throw new DuplicatedPaymentMethodException(DuplicatedPaymentMethodException::DUPLICATED_PAYMENT_METHOD);
        }

        $this->paymentMethods[$paymentMethod->getName()] = $paymentMethod;
    }

    /**
     * @param string $paymentMethod
     *
     * @return PaymentMethod
     */
    public function getPaymentMethod($paymentMethod)
    {
        if (!in_array($paymentMethod, array_keys($this->paymentMethods))) {
            throw new PaymentMethodNotSupportedException(PaymentMethodNotSupportedException::PAYMENT_METHOD_NOT_SUPPORTED);
        }

        return $this->paymentMethods[$paymentMethod];
    }

    /**
     * @return PaymentMethod[]
     */
    public function getPaymentMethods(): array
    {
        return $this->paymentMethods;
    }

    /**
     * Finish the checkout process and, in success case, return order data.
     *
     * @param Order $order
     *
     * @throws InputException Thrown for client errors
     * @throws CheckoutException Thrown for server errors
     *
     * @return CompletedOrder
     */
    public function placeOrder(Order $order): CompletedOrder
    {
        if ($order->getPaymentMethod() instanceof CreditCard && $order->isFullWalletPayment()) {
            $order->getPaymentMethod()->clearBinNumber();
        }

        $completedOrder = $this->adapter->placeOrder($order);

        if (!$completedOrder->hasPaymentRedirect()) {
            $this->storage->clear($completedOrder->getId());
        }

        return $completedOrder;
    }

    /**
     * @param string $orderNumber
     * @param string $completedOrderId
     * @param int $customerId
     * @param array $parameters
     *
     * @return ExternalPaymentResult
     */
    public function processExternalPayment(string $orderNumber, string $completedOrderId, int $customerId, array $parameters): ExternalPaymentResult
    {
        $externalPaymentResult = $this->adapter->processExternalPayment($orderNumber, $customerId, $parameters);

        if ($externalPaymentResult->isSuccess()) {
            $this->storage->clear($completedOrderId);
        }

        return $externalPaymentResult;
    }

    /**
     * @param string $paymentMethodName
     * @param array $parameters
     *
     * @return array
     */
    public function postProcessExternalPayment(string $paymentMethodName, array $parameters): array
    {
        return $this->adapter->postProcessExternalPayment($paymentMethodName, $parameters);
    }

    /**
     * @param string $paymentMethodName
     * @param string $body
     * @param array $headers
     * @param null|string $action
     *
     * @return array
     */
    public function updatePaymentStatus(string $paymentMethodName, string $body, array $headers, string $action = null): array
    {
        return $this->adapter->updatePaymentStatus($paymentMethodName, $body, $headers, $action);
    }
}
