<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

class RecalculatedOrder
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @var string
     */
    protected $successMessage;

    /**
     * @var array<string, array<string>>
     */
    protected $warnings = [];

    /**
     * @var array<string, array<string>>
     */
    protected $errors = [];

    /**
     * @var Items
     */
    protected $undeliverables;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->undeliverables = new Items();
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getSuccessMessage()
    {
        return $this->successMessage;
    }

    /**
     * @param string $successMessage
     */
    public function setSuccessMessage(string $successMessage)
    {
        $this->successMessage = $successMessage;
    }

    /**
     * @return array<string, array<string>>
     */
    public function getWarnings(): array
    {
        return $this->warnings;
    }

    /**
     * @param array<string, array<string>> $warnings
     */
    public function setWarnings(array $warnings)
    {
        $this->warnings = $warnings;
    }

    /**
     * @return array<string, array<string>>
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array<string, array<string>> $errors
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return Items
     */
    public function getUndeliverables(): Items
    {
        return $this->undeliverables;
    }

    /**
     * @param Items $undeliverables
     */
    public function setUndeliverables(Items $undeliverables)
    {
        $this->undeliverables = $undeliverables;
    }

    /**
     * @param Item $undeliverable
     */
    public function addUndeliverable(Item $undeliverable)
    {
        $this->undeliverables->add($undeliverable);
    }
}
