<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use InvalidArgumentException;
use Linio\Collection\TypedCollection;

class Items extends TypedCollection
{
    /**
     * {@inheritdoc}
     */
    public function add($value)
    {
        $sku = $value->getSku();

        if (!$sku) {
            throw new InvalidArgumentException('The Item object must have a SKU.');
        }

        $this->offsetSet(strtoupper($sku), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if ($this->offsetExists($offset)) {
            $item = $this->get($offset);
            $item->setQuantity($item->getQuantity() + $value->getQuantity());

            return;
        }

        parent::offsetSet(strtoupper($offset), $value);
    }

    /**
     * @param string $sku
     *
     * @return bool
     */
    public function hasItem(string $sku): bool
    {
        return $this->containsKey(strtoupper($sku));
    }

    /**
     * @return int
     */
    public function countItems(): int
    {
        $itemCount = 0;

        /** @var Item $item */
        foreach ($this->getValues() as $item) {
            $itemCount += $item->getQuantity();
        }

        return $itemCount;
    }

    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof Item;
    }
}
