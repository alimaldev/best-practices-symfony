<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use DateTime;
use Linio\Frontend\Entity\Product;
use Linio\Type\Money;

class Item
{
    /**
     * @var string Simple or MarketPlaceChild SKU
     */
    protected $sku;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Money
     */
    protected $unitPrice;

    /**
     * @var Money
     */
    protected $originalPrice;

    /**
     * @var Money
     */
    protected $paidPrice;

    /**
     * @var Money
     */
    protected $taxAmount;

    /**
     * @var Money
     */
    protected $shippingAmount;

    /**
     * @var int
     */
    protected $quantity = 1;

    /**
     * @var int
     */
    protected $availableQuantity = 0;

    /**
     * @var int
     */
    protected $linioPlusLevel = 0;

    /**
     * The quantity of this item that are being shipped with Linio Plus.
     *
     * @var int
     */
    protected $linioPlusEnabledQuantity = 0;

    /**
     * @var int
     */
    protected $maxItemsToSell = 1;

    /**
     * @var DateTime
     */
    protected $minimumDeliveryDate;

    /**
     * @param string $sku Simple or MarketPlaceChild SKU
     * @param int $quantity
     * @param Product $product
     */
    public function __construct(string $sku, int $quantity = 1, Product $product = null)
    {
        $this->sku = strtoupper($sku);
        $this->quantity = $quantity;
        $this->product = $product ?? new Product(explode('-', $this->sku)[0]);
        $this->unitPrice = new Money();
        $this->originalPrice = new Money();
        $this->paidPrice = new Money();
        $this->taxAmount = new Money();
        $this->shippingAmount = new Money();
    }

    /**
     * @return string Simple or MarketPlaceChild SKU
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Money
     */
    public function getUnitPrice(): Money
    {
        return $this->unitPrice;
    }

    /**
     * @param Money $unitPrice
     */
    public function setUnitPrice(Money $unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return Money
     */
    public function getOriginalPrice(): Money
    {
        return $this->originalPrice;
    }

    /**
     * @param Money $originalPrice
     */
    public function setOriginalPrice(Money $originalPrice)
    {
        $this->originalPrice = $originalPrice;
    }

    /**
     * @return Money
     */
    public function getPaidPrice(): Money
    {
        return $this->paidPrice;
    }

    /**
     * @param Money $paidPrice
     */
    public function setPaidPrice(Money $paidPrice)
    {
        $this->paidPrice = $paidPrice;
    }

    /**
     * @return Money
     */
    public function getSubtotal(): Money
    {
        return $this->unitPrice->multiply($this->quantity);
    }

    /**
     * @return Money
     */
    public function getTaxAmount(): Money
    {
        return $this->taxAmount;
    }

    /**
     * @param Money $taxAmount
     */
    public function setTaxAmount(Money $taxAmount)
    {
        $this->taxAmount = $taxAmount;
    }

    /**
     * @return Money
     */
    public function getShippingAmount(): Money
    {
        return $this->shippingAmount;
    }

    /**
     * @param Money $shippingAmount
     */
    public function setShippingAmount(Money $shippingAmount)
    {
        $this->shippingAmount = $shippingAmount;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getAvailableQuantity(): int
    {
        return $this->availableQuantity;
    }

    /**
     * @param int $availableQuantity
     */
    public function setAvailableQuantity(int $availableQuantity)
    {
        $this->availableQuantity = $availableQuantity;
    }

    /**
     * @return int
     */
    public function getMaxItemsToSell(): int
    {
        return $this->maxItemsToSell;
    }

    /**
     * @param int $maxItemsToSell
     */
    public function setMaxItemsToSell(int $maxItemsToSell)
    {
        $this->maxItemsToSell = $maxItemsToSell;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        $selectedImage = null;
        $images = $this->product->getImages();

        foreach ($images as $image) {
            if ($image->isMain()) {
                $selectedImage = $image;
            }
        }

        // If no image is marked as main, use the first image
        if (!empty($images)) {
            $selectedImage = reset($images);
        }

        if (!$selectedImage) {
            return '';
        }

        return sprintf('%s-cart.jpg', $selectedImage->getSlug(), 'cart');
    }

    /**
     * @return int
     */
    public function getLinioPlusLevel(): int
    {
        return $this->linioPlusLevel;
    }

    /**
     * @param int $linioPlusLevel
     */
    public function setLinioPlusLevel(int $linioPlusLevel)
    {
        $this->linioPlusLevel = $linioPlusLevel;
    }

    /**
     * @return int
     */
    public function getLinioPlusEnabledQuantity(): int
    {
        return $this->linioPlusEnabledQuantity;
    }

    /**
     * @param int $linioPlusEnabledQuantity
     */
    public function setLinioPlusEnabledQuantity(int $linioPlusEnabledQuantity)
    {
        $this->linioPlusEnabledQuantity = $linioPlusEnabledQuantity;
    }

    /**
     * @return DateTime|null
     */
    public function getMinimumDeliveryDate()
    {
        return $this->minimumDeliveryDate;
    }

    public function setMinimumDeliveryDate(DateTime $minimumDeliveryDate)
    {
        $this->minimumDeliveryDate = $minimumDeliveryDate;
    }
}
