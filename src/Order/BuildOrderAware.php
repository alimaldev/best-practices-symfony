<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

trait BuildOrderAware
{
    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }
}
