<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\PlaceOrder;

use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Exception\CheckoutException;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\ExternalPaymentResult;

/**
 * Interface CheckoutAdapterInterface.
 */
interface PlaceOrderAdapter
{
    /**
     * Finish the checkout process and, in success case, return order data.
     *
     * @param Order $order
     *
     * @throws InputException Thrown for client errors
     * @throws CheckoutException Thrown for server errors
     *
     * @return CompletedOrder
     */
    public function placeOrder(Order $order): CompletedOrder;

    /**
     * @param string $orderNumber
     * @param int $customerId
     * @param array $parameters
     *
     * @return ExternalPaymentResult
     */
    public function processExternalPayment(string $orderNumber, int $customerId, array $parameters): ExternalPaymentResult;

    /**
     * @param string $paymentMethodName
     * @param array $parameters
     *
     * @return array
     */
    public function postProcessExternalPayment(string $paymentMethodName, array $parameters): array;

    /**
     * @param string $paymentMethodName
     * @param string $body
     * @param array $headers
     * @param null|string $action
     *
     * @return array
     */
    public function updatePaymentStatus(string $paymentMethodName, string $body, array $headers, string $action = null): array;
}
