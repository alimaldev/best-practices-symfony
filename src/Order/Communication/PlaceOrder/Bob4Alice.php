<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\PlaceOrder;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Controller\SessionAware;
use Linio\Frontend\Exception\InputException;
use Linio\Frontend\Order\Communication\BuildOrder\Input\OrderInput;
use Linio\Frontend\Order\Communication\BuildOrder\Output\OrderOutput;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Exception\CheckoutException;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\ExternalPaymentResult;
use Linio\Frontend\Order\Payment\PaymentRedirect;

class Bob4Alice implements PlaceOrderAdapter
{
    use SessionAware;

    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var OrderOutput
     */
    protected $orderOutput;

    /**
     * @var OrderInput
     */
    protected $orderInput;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param OrderOutput $orderOutput
     */
    public function setOrderOutput(OrderOutput $orderOutput)
    {
        $this->orderOutput = $orderOutput;
    }

    /**
     * @param OrderInput $orderInput
     */
    public function setOrderInput(OrderInput $orderInput)
    {
        $this->orderInput = $orderInput;
    }

    /**
     * Finish the checkout process and, in success case, return order data.
     *
     * @param Order $order
     *
     * @throws InputException Thrown for client errors
     * @throws CheckoutException Thrown for server errors
     *
     * @return CompletedOrder
     */
    public function placeOrder(Order $order): CompletedOrder
    {
        $recalculateCartRequest = $this->orderOutput->toRecalculateCart($order);

        $placeOrderRequest = [
            'cart' => $recalculateCartRequest,
            'ipAddress' => $order->getIpAddress(),
            'isLinioPromise' => false,
            'requestClient' => 'alice',
            'source' => 'WEB',
            'utmSource' => null,
            'utmMedium' => null,
            'utmCampaign' => null,
            'browserDigitalFingerprint' => $this->session->getId(),
            'affiliateCode' => null,
            'shippingAddress' => [
                'id' => $order->getShippingAddress()->getId(),
                'alternativeRecipientName' => '',
                'alternativeRecipientIdentification' => '',
            ],
            'billingAddress' => [
                'id' => $order->getBillingAddress() ? $order->getBillingAddress()->getId() : null,
            ],
            'paymentParameters' => $order->getPaymentMethod()->getPaymentData(),
            'metadata' => $order->getMetadata(),
        ];

        $placeOrderRequest = $this->addTelesalesUtmData($order, $placeOrderRequest);

        try {
            $response = $this->client->request('POST', '/order/place', ['json' => $placeOrderRequest]);
        } catch (ClientException $exception) {
            $this->handleClientException($exception);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CheckoutException($responseBody['code'], $responseBody);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $completedOrder = $this->orderInput->fromPlaceOrder($order, $responseBody['cart']);
        $completedOrder->setOrderNumber($responseBody['orderNumber']);
        $completedOrder->setSalesOrderId($responseBody['id']);

        if (isset($responseBody['payment'])) {
            $completedOrder->setPaymentResult($responseBody['payment']);
        }

        if ($responseBody['status'] == 'redirect') {
            $redirect = new PaymentRedirect();
            $redirect->setMethod($responseBody['redirect']['method'] ?? 'GET');
            $redirect->setTarget($responseBody['redirect']['url']);
            $redirect->setBody($responseBody['redirect']['body'] ?? []);
            $completedOrder->setPaymentRedirect($redirect);
        }

        return $completedOrder;
    }

    protected function handleClientException(ClientException $exception)
    {
        $responseBody = Json::decode((string) $exception->getResponse()->getBody());

        if ($responseBody['code'] == 'INVALID_REQUEST') {
            $errorMessage = $responseBody['code'];

            if (!empty($responseBody['errors'])) {
                $errorMessage = $responseBody['errors'][0]['message'];
            }

            throw new InputException($errorMessage);
        }

        throw new CheckoutException($responseBody['code'], $responseBody);
    }

    /**
     * @param string $orderNumber
     * @param int $customerId
     * @param array $parameters
     *
     * @return ExternalPaymentResult
     */
    public function processExternalPayment(string $orderNumber, int $customerId, array $parameters): ExternalPaymentResult
    {
        $requestBody = [
            'orderNumber' => $orderNumber,
            'customerId' => $customerId,
            'parameters' => $parameters,
        ];

        $externalPaymentResult = new ExternalPaymentResult();
        $externalPaymentResult->setIsSuccess(true);

        try {
            $response = $this->client->request('POST', '/order/process-result', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $externalPaymentResult->setIsSuccess(false);
        } catch (ServerException $exception) {
            $externalPaymentResult->setIsSuccess(false);
        }

        return $externalPaymentResult;
    }

    /**
     * @param string $paymentMethodName
     * @param array $parameters
     *
     * @return array
     */
    public function postProcessExternalPayment(string $paymentMethodName, array $parameters): array
    {
        $requestBody = [
            'paymentMethodName' => $paymentMethodName,
            'parameters' => $parameters,
        ];

        try {
            $response = $this->client->request('POST', '/order/post-process', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CheckoutException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CheckoutException($responseBody['code']);
        }

        return Json::decode((string) $response->getBody());
    }

    /**
     * @param string $paymentMethodName
     * @param string $body
     * @param array $headers
     * @param null|string $action
     *
     * @return array
     */
    public function updatePaymentStatus(string $paymentMethodName, string $body, array $headers, string $action = null): array
    {
        $requestBody = [
            'action' => $action,
            'paymentMethodName' => $paymentMethodName,
            'body' => $body,
            'headers' => $headers,
        ];

        try {
            $response = $this->client->request('POST', '/order/update-payment-status', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CheckoutException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new CheckoutException($responseBody['code']);
        }

        return Json::decode((string) $response->getBody());
    }

    /**
     * This is ugly, but it's what the business wants.
     *
     * @param Order $order
     * @param array $requestData
     *
     * @return array
     */
    protected function addTelesalesUtmData(Order $order, array $requestData): array
    {
        $utm = $this->session->get('telesales/utm');

        if (!$order->getCustomer()->isImpersonated()) {
            return $requestData;
        }

        if (!is_array($utm)) {
            return $requestData;
        }

        $requestData['utmSource'] = $utm['source'];
        $requestData['utmMedium'] = $utm['medium'];
        $requestData['utmCampaign'] = $utm['campaign'];

        $this->session->remove('telesales');

        return $requestData;
    }
}
