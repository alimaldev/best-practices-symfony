<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder\Output;

use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Request\PlatformInformation;
use Linio\Frontend\Security\GuestUser;

class Bob4Alice implements OrderOutput
{
    /**
     * @var PlatformInformation
     */
    protected $platformInformation;

    /**
     * @param PlatformInformation $platformInformation
     */
    public function __construct(PlatformInformation $platformInformation)
    {
        $this->platformInformation = $platformInformation;
    }

    public function toRecalculateCart(Order $order): array
    {
        $assistedSalesOperator = null;

        if (!$order->getCustomer() instanceof GuestUser && $order->getCustomer()->isImpersonated()) {
            $assistedSalesOperator = $order->getCustomer()->getImpersonatedBy();
        }

        $request = [
            'customer' => [
                'id' => !$order->getCustomer() instanceof GuestUser ? $order->getCustomer()->getId() : null,
                'assistedSalesOperator' => $assistedSalesOperator,
            ],
            'store' => [
                'id' => $this->platformInformation->getStoreId(),
            ],
            'coupon' => [
                'code' => $order->getCoupon() ? $order->getCoupon()->getCode() : null,
            ],
            'payment' => [
                'paymentMethodName' => $order->getPaymentMethod() ? $order->getPaymentMethod()->getName() : null,
                'dependentPaymentMethodName' => $order->getDependentPaymentMethod(),
                'creditCardBinNumber' => $order->getPaymentMethod() instanceof CreditCard ? $order->getPaymentMethod()->getBinNumber() : null,
                'installments' => [
                    'quantity' => $order->getSelectedInstallmentQuantity(),
                ],
            ],
            'shipping' => [
                'postcode' => $order->getShippingAddress() ? $order->getShippingAddress()->getPostcode() : null,
                'region' => $order->getShippingAddress() ? $order->getShippingAddress()->getRegion() : null,
                'municipality' => $order->getShippingAddress() ? $order->getShippingAddress()->getMunicipality() : null,
                'city' => $order->getShippingAddress() ? $order->getShippingAddress()->getCity() : null,
                'packages' => [],
                'pickupStore' => [
                    'id' => null,
                    'geoHash' => null,
                ],
                'fixedShipping' => [
                    'amount' => null,
                    'cost' => null,
                ],
            ],
            'wallet' => [
                'totalPointsUsed' => $order->getWallet() ? $order->getWallet()->getTotalPointsUsed()->getAmount() : null,
            ],
            'items' => [],
            'grandTotal' => $order->getGrandTotal()->getAmount(),
            'isFastLane' => false,
        ];

        if ($order->getPickupStores()->getSelected()) {
            $request['shipping']['pickupStore']['id'] = $order->getPickupStores()->getSelected()->getId();
        } elseif ($order->getGeohash()) {
            $request['shipping']['pickupStore']['geoHash'] = $order->getGeohash();
        }

        foreach ($order->getPackages() as $package) {
            $request['shipping']['packages'][$package->getId()] = [
                'selectedShippingMethod' => $package->getSelectedShippingQuote()->getShippingMethod(),
            ];
        }

        foreach ($order->getItems() as $item) {
            $request['items'][$item->getSku()]['quantity'] = $item->getQuantity();
        }

        return $request;
    }
}
