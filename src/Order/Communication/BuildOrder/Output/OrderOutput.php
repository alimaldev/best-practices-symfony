<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder\Output;

use Linio\Frontend\Order\Order;

interface OrderOutput
{
    public function toRecalculateCart(Order $order): array;
}
