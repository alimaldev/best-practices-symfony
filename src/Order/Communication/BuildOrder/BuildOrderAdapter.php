<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder;

use DomainException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\RecalculatedOrder;

interface BuildOrderAdapter
{
    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function addProduct(Order $order, Item $item): RecalculatedOrder;

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function removeProduct(Order $order, Item $item): RecalculatedOrder;

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function updateQuantity(Order $order, Item $item): RecalculatedOrder;

    /**
     * @param Order $order
     * @param Coupon $coupon
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function applyCoupon(Order $order, Coupon $coupon): RecalculatedOrder;

    /**
     * @param Order $order
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function removeCoupon(Order $order): RecalculatedOrder;

    /**
     * @param Order $order
     * @param Address $shippingAddress
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function setShippingAddress(Order $order, Address $shippingAddress): RecalculatedOrder;

    /**
     * @param Order $order
     * @param int $packageId
     * @param string $shippingMethod
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function updateShippingMethod(Order $order, int $packageId, string $shippingMethod): RecalculatedOrder;

    /**
     * @param Order $order
     * @param PaymentMethod $paymentMethod
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function updatePaymentMethod(Order $order, PaymentMethod $paymentMethod): RecalculatedOrder;

    /**
     * @param Order $order
     * @param int $points
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function useWalletPoints(Order $order, int $points): RecalculatedOrder;

    /**
     * @param Order $order
     * @param int $quantity
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function selectInstallmentQuantity(Order $order, int $quantity): RecalculatedOrder;

    /**
     * @param Order $order
     * @param int $storeId
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setPickupStore(Order $order, int $storeId): RecalculatedOrder;

    /**
     * @param Order $order
     * @param string $geohash
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setGeohash(Order $order, string $geohash): RecalculatedOrder;

    /**
     * @param Order $order
     * @param string $binNumber
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setBinNumber(Order $order, string $binNumber): RecalculatedOrder;

    /**
     * @param Order $order
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function recalculateOrder(Order $order): RecalculatedOrder;
}
