<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder\Input;

use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Order;

interface OrderInput
{
    public function fromRecalculateCart(Order $order, array $data): Order;

    public function fromPlaceOrder(Order $order, array $data): CompletedOrder;
}
