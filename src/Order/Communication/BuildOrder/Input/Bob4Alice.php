<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder\Input;

use Carbon\Carbon;
use Linio\Frontend\Customer\Membership\LoyaltyProgram;
use Linio\Frontend\Entity\Customer\CreditCard as SavedCreditCard;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Partnership;
use Linio\Frontend\Order\Payment\InstallmentOption;
use Linio\Frontend\Order\Payment\InstallmentOptions;
use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\PaymentMethodFactory;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Order\Shipping\PickupStore;
use Linio\Frontend\Order\Shipping\ShippingQuote;
use Linio\Frontend\Order\Shipping\ShippingQuotes;
use Linio\Frontend\Order\Wallet;
use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductAware;
use Linio\Type\Money;

class Bob4Alice implements OrderInput
{
    use ProductAware;

    public function fromRecalculateCart(Order $order, array $data): Order
    {
        $newOrder = new Order($order->getId());

        $this->mapResponse($order, $newOrder, $data);

        return $newOrder;
    }

    public function fromPlaceOrder(Order $order, array $data): CompletedOrder
    {
        $completedOrder = new CompletedOrder($order->getId());

        $this->mapResponse($order, $completedOrder, $data);

        return $completedOrder;
    }

    /**
     * @param Order $order
     * @param Order|CompletedOrder $newOrder
     * @param array $responseBody
     */
    protected function mapResponse(Order $order, Order $newOrder, array $responseBody)
    {
        $newOrder->setCustomer($order->getCustomer());

        // The customer's location doesn't change between calls.
        if ($order->getGeohash()) {
            $newOrder->setGeohash($order->getGeohash());
        }

        if ($order->getShippingAddress()) {
            $newOrder->setShippingAddress($order->getShippingAddress());
        }

        if (!empty($responseBody['coupon']['code'])) {
            $coupon = new Coupon($responseBody['coupon']['code']);
            $coupon->setDiscount(Money::fromCents($responseBody['coupon']['discount']));
            $newOrder->applyCoupon($coupon);
        }

        $availablePaymentMethods = [];
        foreach ($responseBody['payment']['paymentMethod']['available'] as $paymentMethodName) {
            $availablePaymentMethods[$paymentMethodName] = PaymentMethodFactory::fromName($paymentMethodName);
        }

        foreach ($responseBody['payment']['paymentMethod']['allowed'] as $paymentMethodName) {
            if (!isset($availablePaymentMethods[$paymentMethodName])) {
                continue;
            }

            $availablePaymentMethods[$paymentMethodName]->allow();
        }

        foreach ($responseBody['payment']['paymentMethod']['allowedByCoupon'] as $paymentMethodName) {
            if (!isset($availablePaymentMethods[$paymentMethodName])) {
                continue;
            }

            $availablePaymentMethods[$paymentMethodName]->allowByCoupon();
        }

        foreach ($responseBody['payment']['paymentMethod']['billingAddressRequired'] as $paymentMethodName) {
            if (!isset($availablePaymentMethods[$paymentMethodName])) {
                continue;
            }

            $availablePaymentMethods[$paymentMethodName]->requireBillingAddress();
        }

        $newOrder->setAvailablePaymentMethods(array_values($availablePaymentMethods));

        if (isset($responseBody['payment']['paymentMethod']['name'])) {
            $selectedPaymentMethod = PaymentMethodFactory::fromName($responseBody['payment']['paymentMethod']['name']);

            if ($selectedPaymentMethod instanceof CreditCard && isset($responseBody['payment']['creditCardBinNumber'])) {
                $selectedPaymentMethod->setBinNumber($responseBody['payment']['creditCardBinNumber']);
            }

            if ($selectedPaymentMethod instanceof CreditCard && isset($responseBody['payment']['allowedCreditCards'])) {
                $savedCards = [];

                foreach ($responseBody['payment']['allowedCreditCards'] as $cardData) {
                    $savedCreditCard = new SavedCreditCard();
                    $savedCreditCard->setId($cardData['id']);
                    $savedCreditCard->setCardholderName($cardData['holder']);
                    $savedCreditCard->setCardNumber($cardData['maskedNumber']);
                    $savedCreditCard->setBrand($cardData['brand']);
                    $savedCreditCard->setExpirationDate(new \DateTime($cardData['expiration']));

                    $savedCards[] = $savedCreditCard;
                }

                $selectedPaymentMethod->setSavedCards($savedCards);
            }

            $newOrder->setPaymentMethod($selectedPaymentMethod);
        }

        $installmentOptions = new InstallmentOptions();

        foreach ($responseBody['payment']['installments'] as $quantity => $installmentData) {
            $installmentOption = new InstallmentOption();
            $installmentOption->setInstallments($quantity);
            $installmentOption->setAmount(Money::fromCents($installmentData['amount']));
            $installmentOption->setInterestFee(Money::fromCents($installmentData['interestFee']));
            $installmentOption->setTotalInterest(Money::fromCents($installmentData['totalInterest']));
            $installmentOption->setTotal(Money::fromCents($installmentData['grandTotal']));

            if (isset($installmentData['hasBankInterestFee']) && $installmentData['hasBankInterestFee']) {
                $installmentOption->enableBankInterestFee();
            }

            if (isset($installmentData['paymentMethodName'])) {
                $installmentOption->setPaymentMethodName($installmentData['paymentMethodName']);
            }

            $installmentOptions->add($installmentOption);

            if ($installmentData['selected'] === true) {
                $installmentOptions->select($quantity);
            }
        }

        $newOrder->setInstallmentOptions($installmentOptions);

        if (isset($responseBody['shipping']['amount'])) {
            $newOrder->setShippingAmount(Money::fromCents($responseBody['shipping']['amount']));
            $newOrder->setOriginalShippingAmount(Money::fromCents($responseBody['shipping']['originalAmount']));
            $newOrder->setShippingDiscountAmount(Money::fromCents($responseBody['shipping']['discount'] ?? 0));
            $newOrder->setLinioPlusSavedAmount(Money::fromCents($responseBody['shipping']['linioPlusSavings']));
        }

        foreach ($responseBody['items'] as $sku => $itemData) {
            $item = new Item($sku, $itemData['quantity']);
            $item->setAvailableQuantity($itemData['availableStock']);
            $item->setMaxItemsToSell($itemData['maxItemsToSell']);
            $item->setOriginalPrice(Money::fromCents($itemData['price']['originalPrice']));
            $item->setPaidPrice(Money::fromCents($itemData['price']['paidPrice']));
            $item->setUnitPrice(Money::fromCents($itemData['price']['unitPrice']));
            $item->setTaxAmount(Money::fromCents($itemData['tax']['amount']));
            $item->setShippingAmount(Money::fromCents($itemData['shipping']['amount'] ?? 0));
            $item->setLinioPlusEnabledQuantity($itemData['linioPlus']['quantity']);
            $item->setLinioPlusLevel($itemData['linioPlus']['level']);
            $item->getProduct()->setName($itemData['name']);

            if (isset($itemData['shipping']['minimumDeliveryDate'])) {
                $item->setMinimumDeliveryDate(new Carbon($itemData['shipping']['minimumDeliveryDate']));
            }

            // TODO: Change all this to be from b4a instead of cache
            if (!$item->getProduct()->getDescription()) {
                try {
                    $item->setProduct($this->productService->getBySku($item->getSku()));
                } catch (ProductNotFoundException $exception) {
                    // Do nothing since we use the name as backup
                }
            }

            $newOrder->addItem($item);
        }

        foreach ($responseBody['shipping']['packages'] as $packageId => $packageData) {
            $package = new Package();
            $package->setId($packageId);

            $shippingQuotes = new ShippingQuotes();

            foreach ($packageData['items'] as $sku => $quantity) {
                // Fetch and clone from items so we have all the properties and can change
                // the quantity without affecting the original item.
                $item = clone $newOrder->getItems()->get($sku);
                $item->setQuantity($quantity);

                $package->getItems()->add($item);
            }

            foreach ($packageData['quotes'] as $id => $quoteData) {
                $estimatedDeliveryDate = new Carbon($quoteData['estimatedDeliveryDate']);
                $fee = Money::fromCents($quoteData['fee']);
                $shippingMethod = $quoteData['shippingMethod'];

                $shippingQuote = new ShippingQuote($shippingMethod, $estimatedDeliveryDate, $fee);

                $shippingQuotes->add($shippingQuote);

                if ($quoteData['selected'] === true) {
                    $shippingQuotes->select($shippingQuote->getShippingMethod());
                }
            }

            $package->setShippingQuotes($shippingQuotes);

            $newOrder->getPackages()->add($package);
        }

        foreach ($responseBody['shipping']['pickupStores'] as $pickupStoreData) {
            $pickupStore = new PickupStore($pickupStoreData['id']);

            // TODO: Remove these issets when PHP gets nullables
            // The database is horribly designed at the moment thus we have to do this.
            if (isset($pickupStoreData['name'])) {
                $pickupStore->setName($pickupStoreData['name']);
            }

            if (isset($pickupStoreData['description'])) {
                $pickupStore->setDescription($pickupStoreData['description']);
            }

            if (isset($pickupStoreData['geoHash'])) {
                $pickupStore->setGeohash($pickupStoreData['geoHash']);
            }

            if (isset($pickupStoreData['postcode'])) {
                $pickupStore->setPostcode($pickupStoreData['postcode']);
            }

            if (isset($pickupStoreData['region'])) {
                $pickupStore->setRegion($pickupStoreData['region']);
            }

            if (isset($pickupStoreData['municipality'])) {
                $pickupStore->setMunicipality($pickupStoreData['municipality']);
            }

            if (isset($pickupStoreData['city'])) {
                $pickupStore->setCity($pickupStoreData['city']);
            }

            if (isset($pickupStoreData['sublocality'])) {
                $pickupStore->setSubLocality($pickupStoreData['sublocality']);
            }

            if (isset($pickupStoreData['neighborhood'])) {
                $pickupStore->setNeighborhood($pickupStoreData['neighborhood']);
            }

            if (isset($pickupStoreData['streetNumber'])) {
                $pickupStore->setStreetNumber($pickupStoreData['streetNumber']);
            }

            if (isset($pickupStoreData['addressLine1'])) {
                $pickupStore->setLine1($pickupStoreData['addressLine1']);
            }

            if (isset($pickupStoreData['addressLine2'])) {
                $pickupStore->setLine2($pickupStoreData['addressLine2']);
            }

            if (isset($pickupStoreData['apartment'])) {
                $pickupStore->setApartment($pickupStoreData['apartment']);
            }

            if (isset($pickupStoreData['referencePoint'])) {
                $pickupStore->setReferencePoint($pickupStoreData['referencePoint']);
            }

            if (isset($pickupStoreData['network']['id'])) {
                $pickupStore->setNetworkId($pickupStoreData['network']['id']);
            }

            if (isset($pickupStoreData['network']['name'])) {
                $pickupStore->setNetworkName($pickupStoreData['network']['name']);
            }

            // This could have been done outside the foreach, but if the store that is picked isn't returned
            // for some reason, we would get an error
            if ($order->getPickupStores()->getSelected() && $order->getPickupStores()->getSelected()->getId() == $pickupStoreData['id']) {
                $pickupStore->select();
            }

            $newOrder->getPickupStores()->add($pickupStore);
        }

        if ($responseBody['shipping']['allowStorePickup']) {
            $newOrder->enableStorePickup();
        }

        if ($responseBody['wallet']['isActive']) {
            $wallet = new Wallet();
            $wallet->setConversionRate($responseBody['wallet']['conversionRate'] ?? 0);
            $wallet->setPointsBalance(Money::fromCents($responseBody['wallet']['pointsBalance']) ?? 0);
            $wallet->setPointsUsedForShipping(Money::fromCents($responseBody['wallet']['pointsUsedShipping']) ?? 0);
            $wallet->setMaxPointsForOrder(Money::fromCents($responseBody['wallet']['maxPointsForOrder']) ?? 0);
            $wallet->setTotalPointsUsed(Money::fromCents($responseBody['wallet']['totalPointsUsed']) ?? 0);
            $wallet->setTotalDiscount(Money::fromCents($responseBody['wallet']['totalDiscount'] ?? 0));
            $wallet->setShippingDiscount(Money::fromCents($responseBody['wallet']['shippingDiscount'] ?? 0));
            $newOrder->setWallet($wallet);
        }

        $partnerships = [];

        foreach ($responseBody['partnership']['available'] as $partnershipData) {
            $partnership = new Partnership($partnershipData['code'], $partnershipData['identifier']);
            $partnership->setName($partnershipData['name']);

            if (isset($partnershipData['level'])) {
                $partnership->setLevel($partnershipData['level']);
            }

            if (isset($partnershipData['appliedDiscount'])) {
                $partnership->setDiscountApplied($partnershipData['appliedDiscount']);
            }

            if ($partnershipData['isActive'] === true) {
                $partnership->enable();
            }

            $partnerships[] = $partnership;
        }

        $newOrder->setPartnerships($partnerships);

        if (isset($responseBody['partnership']['discount'])) {
            $newOrder->setPartnershipDiscount(Money::fromCents($responseBody['partnership']['discount']));
        }

        if (isset($responseBody['loyalty']['registration'])) {
            $loyalty = new LoyaltyProgram();
            $loyalty->setName($responseBody['loyalty']['programName']);
            $loyalty->setId($responseBody['loyalty']['registration']);
            $newOrder->setLoyaltyPointsAccrued($responseBody['loyalty']['points']);
            $newOrder->getCustomer()->setLoyaltyProgram($loyalty);
        }

        $newOrder->setTaxAmount(Money::fromCents($responseBody['tax']['amount'] ?? 0));
        $newOrder->setSubTotal(Money::fromCents($responseBody['subtotal']));
        $newOrder->setGrandTotal(Money::fromCents($responseBody['grandTotal']));
    }
}
