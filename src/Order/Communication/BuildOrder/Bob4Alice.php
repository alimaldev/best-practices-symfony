<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Communication\BuildOrder;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Linio\Component\Util\Json;
use Linio\Frontend\Exception\DomainException;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Communication\BuildOrder\Input\OrderInput;
use Linio\Frontend\Order\Communication\BuildOrder\Output\OrderOutput;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Payment\PaymentMethod;
use Linio\Frontend\Order\RecalculatedOrder;

class Bob4Alice implements BuildOrderAdapter
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var OrderOutput
     */
    protected $orderOutput;

    /**
     * @var OrderInput
     */
    protected $orderInput;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param OrderOutput $orderOutput
     */
    public function setOrderOutput(OrderOutput $orderOutput)
    {
        $this->orderOutput = $orderOutput;
    }

    /**
     * @param OrderInput $orderInput
     */
    public function setOrderInput(OrderInput $orderInput)
    {
        $this->orderInput = $orderInput;
    }

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function addProduct(Order $order, Item $item): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        if (isset($recalculatedOrder->getErrors()[$item->getSku()])) {
            throw new OrderException($recalculatedOrder->getErrors()[$item->getSku()]);
        }

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function removeProduct(Order $order, Item $item): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     * @param Item $item
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function updateQuantity(Order $order, Item $item): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        if (isset($recalculatedOrder->getErrors()[$item->getSku()])) {
            throw new OrderException($recalculatedOrder->getErrors()[$item->getSku()]);
        }

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param Coupon $coupon
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function applyCoupon(Order $order, Coupon $coupon): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        if (isset($recalculatedOrder->getWarnings()[$coupon->getCode()])) {
            // We need to find the keys if they exist so we can do an unset
            $requirementNotMet = array_search('WARNING_COUPON_REQUIREMENT_NOT_MET', $recalculatedOrder->getWarnings()[$coupon->getCode()]);
            $wasRemoved = array_search('WARNING_COUPON_WAS_REMOVED', $recalculatedOrder->getWarnings()[$coupon->getCode()]);

            if ($requirementNotMet !== false && $wasRemoved !== false) {
                unset($recalculatedOrder->getWarnings()[$coupon->getCode()][$wasRemoved]);
            }
        }

        if (isset($recalculatedOrder->getErrors()[$coupon->getCode()])) {
            throw new OrderException($recalculatedOrder->getErrors()[$coupon->getCode()]);
        }

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function removeCoupon(Order $order): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     * @param Address $shippingAddress
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function setShippingAddress(Order $order, Address $shippingAddress): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        $this->throwExceptionIfErrorFound(['ERROR_POSTCODE_NOT_RESOLVED'], $recalculatedOrder);

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param int $packageId
     * @param string $shippingMethod
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function updateShippingMethod(Order $order, int $packageId, string $shippingMethod): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $requestBody['shipping']['packages'][$packageId]['selectedShippingMethod'] = $shippingMethod;

        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        $this->throwExceptionIfErrorFound(['ERROR_SHIPPING_METHOD_NOT_AVAILABLE'], $recalculatedOrder);

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param PaymentMethod $paymentMethod
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function updatePaymentMethod(Order $order, PaymentMethod $paymentMethod): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     * @param int $points
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function useWalletPoints(Order $order, int $points): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        $this->throwExceptionIfErrorFound(['ERROR_WALLET_SUBSCRIPTION_NOT_FOUND'], $recalculatedOrder);

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param int $quantity
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     * @throws OrderException
     *
     * @return RecalculatedOrder
     */
    public function selectInstallmentQuantity(Order $order, int $quantity): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);
        $recalculatedOrder = $this->sendBuildOrderRequest($order, $requestBody);

        $this->throwExceptionIfErrorFound(['ERROR_SELECTED_INSTALLMENT_OPTION_NOT_FOUND'], $recalculatedOrder);

        return $recalculatedOrder;
    }

    /**
     * @param Order $order
     * @param int $storeId
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setPickupStore(Order $order, int $storeId): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     * @param string $geohash
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setGeohash(Order $order, string $geohash): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     * @param string $binNumber
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function setBinNumber(Order $order, string $binNumber): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    public function recalculateOrder(Order $order): RecalculatedOrder
    {
        $requestBody = $this->buildBuildOrderRequest($order);

        return $this->sendBuildOrderRequest($order, $requestBody);
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function buildBuildOrderRequest(Order $order): array
    {
        return $this->orderOutput->toRecalculateCart($order);
    }

    /**
     * @param Order $order
     * @param array $requestBody
     *
     * @throws DomainException Thrown for client errors
     * @throws DomainException Thrown for server errors
     *
     * @return RecalculatedOrder
     */
    protected function sendBuildOrderRequest(Order $order, array $requestBody): RecalculatedOrder
    {
        try {
            $response = $this->client->request('POST', '/cart/recalculate', ['json' => $requestBody]);
        } catch (ClientException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        } catch (ServerException $exception) {
            $responseBody = Json::decode((string) $exception->getResponse()->getBody());

            throw new DomainException($responseBody['code']);
        }

        $responseBody = Json::decode((string) $response->getBody());

        $newOrder = $this->orderInput->fromRecalculateCart($order, $responseBody);

        $recalculatedOrder = new RecalculatedOrder($newOrder);
        $recalculatedOrder->setErrors($responseBody['messages']['error']);
        $recalculatedOrder->setWarnings($responseBody['messages']['warning']);
        $this->handleUndeliverablePackageError($recalculatedOrder);

        return $recalculatedOrder;
    }

    protected function handleUndeliverablePackageError(RecalculatedOrder $recalculatedOrder)
    {
        $errors = $recalculatedOrder->getErrors();

        foreach ($errors as $errorContext => $errorKey) {
            if ($errorKey != 'ERROR_UNDELIVERABLE_PACKAGE') {
                continue;
            }

            $packageId = (int) substr($errorContext, 16);
            $undeliverablesPackage = $recalculatedOrder->getOrder()->getPackages()->get($packageId);

            if (!$undeliverablesPackage) {
                continue;
            }

            foreach ($undeliverablesPackage->getItems() as $undeliverable) {
                $recalculatedOrder->addUndeliverable($undeliverable);
            }

            unset($errors[$errorContext]);
        }

        $recalculatedOrder->setErrors($errors);
    }

    /**
     * Checks to see if an error exists and if throws an exception.
     *
     * This is needed because not all of
     *
     * @param array $errorsToCheck
     * @param RecalculatedOrder $recalculatedOrder
     *
     * @throws OrderException
     */
    protected function throwExceptionIfErrorFound(array $errorsToCheck, RecalculatedOrder $recalculatedOrder)
    {
        $errors = $recalculatedOrder->getErrors();

        foreach ($errorsToCheck as $error) {
            $errorFound = array_search($error, $errors);

            if ($errorFound !== false) {
                unset($errors[$errorFound]);
                $recalculatedOrder->setErrors($errors);

                throw new OrderException($error, ['context' => $errorFound]);
            }
        }
    }
}
