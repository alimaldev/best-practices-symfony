<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Frontend\Customer\Membership\Partnership as CustomerPartnership;

class Partnership extends CustomerPartnership
{
    /**
     * @var bool
     */
    protected $discountApplied;

    /**
     * @return bool
     */
    public function isDiscountApplied()
    {
        return $this->discountApplied;
    }

    /**
     * @param bool $discountApplied
     */
    public function setDiscountApplied(bool $discountApplied)
    {
        $this->discountApplied = $discountApplied;
    }
}
