<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

trait PlaceOrderAware
{
    /**
     * @var PlaceOrder
     */
    protected $placeOrder;

    /**
     * @param PlaceOrder $placeOrder
     */
    public function setPlaceOrder(PlaceOrder $placeOrder)
    {
        $this->placeOrder = $placeOrder;
    }
}
