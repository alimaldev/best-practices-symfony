<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

use InvalidArgumentException;
use Linio\Collection\TypedCollection;

class InstallmentOptions extends TypedCollection
{
    /**
     * @param int $installmentQuantity
     *
     * @throws InvalidArgumentException
     */
    public function select(int $installmentQuantity)
    {
        /** @var InstallmentOption $installmentOption */
        $installmentOption = $this->get($installmentQuantity);

        if (!$installmentOption) {
            throw new InvalidArgumentException('Invalid installment.');
        }

        $this->clearSelection();
        $installmentOption->select();
    }

    /**
     * @return InstallmentOption|null
     */
    public function getSelected()
    {
        /** @var InstallmentOption $installmentOption */
        foreach ($this as $installmentOption) {
            if ($installmentOption->isSelected()) {
                return $installmentOption;
            }
        }
    }

    /**
     * @return InstallmentOption|null
     */
    public function getHighest()
    {
        $installments = $this->getSortedInstallments();

        return array_pop($installments);
    }

    /**
     * @return InstallmentOption|null
     */
    public function getLowest()
    {
        $installments = $this->getSortedInstallments();

        return array_shift($installments);
    }

    protected function getSortedInstallments(): array
    {
        $installments = $this->toArray();

        usort($installments, function ($installmentA, $installmentB) {
            return $installmentA->getInstallments() <=> $installmentB->getInstallments();
        });

        return $installments;
    }

    public function clearSelection()
    {
        /** @var InstallmentOption $installmentOption */
        foreach ($this as $installmentOption) {
            $installmentOption->deselect();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function add($value)
    {
        $this->validateType($value);
        $this->set($value->getInstallments(), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof InstallmentOption;
    }
}
