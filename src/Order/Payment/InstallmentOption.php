<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

use Linio\Type\Money;

class InstallmentOption
{
    /**
     * @var int
     */
    protected $installments = 1;

    /**
     * @var Money
     */
    protected $interestFee;

    /**
     * @var Money
     */
    protected $total;

    /**
     * @var Money
     */
    protected $totalInterest;

    /**
     * @var Money
     */
    protected $amount;

    /**
     * @var bool
     */
    protected $selected;

    /**
     * @var string
     */
    protected $paymentMethodName;

    /**
     * @var bool
     */
    protected $hasBankInterestFee = false;

    public function __construct()
    {
        $this->interestFee = new Money();
        $this->total = new Money();
        $this->totalInterest = new Money();
        $this->amount = new Money();
        $this->selected = false;
    }

    /**
     * @return int
     */
    public function getInstallments(): int
    {
        return $this->installments;
    }

    /**
     * @param int $installments
     */
    public function setInstallments(int $installments)
    {
        $this->installments = $installments;
    }

    /**
     * @return Money
     */
    public function getInterestFee(): Money
    {
        return $this->interestFee;
    }

    /**
     * @param Money $interestFee
     */
    public function setInterestFee(Money $interestFee)
    {
        $this->interestFee = $interestFee;
    }

    /**
     * @return bool
     */
    public function hasInterest(): bool
    {
        return $this->interestFee->getMoneyAmount() > 0;
    }

    /**
     * @return Money
     */
    public function getTotal(): Money
    {
        return $this->total;
    }

    /**
     * @param Money $total
     */
    public function setTotal(Money $total)
    {
        $this->total = $total;
    }

    /**
     * @return Money
     */
    public function getTotalInterest(): Money
    {
        return $this->totalInterest;
    }

    /**
     * @param Money $totalInterest
     */
    public function setTotalInterest(Money $totalInterest)
    {
        $this->totalInterest = $totalInterest;
    }

    /**
     * @return Money
     */
    public function getAmount(): Money
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     */
    public function setAmount(Money $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->selected;
    }

    public function select()
    {
        $this->selected = true;
    }

    public function deselect()
    {
        $this->selected = false;
    }

    public function getPaymentMethodName()
    {
        return $this->paymentMethodName;
    }

    public function setPaymentMethodName(string $paymentMethodName)
    {
        $this->paymentMethodName = $paymentMethodName;
    }

    public function enableBankInterestFee()
    {
        $this->hasBankInterestFee = true;
    }

    public function hasBankInterestFee(): bool
    {
        return $this->hasBankInterestFee;
    }
}
