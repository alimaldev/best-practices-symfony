<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

class PaymentRedirect
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $target;

    /**
     * @var array
     */
    protected $body = [];

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function setTarget(string $target)
    {
        $this->target = $target;
    }

    public function getBody(): array
    {
        return $this->body;
    }

    public function setBody(array $body = [])
    {
        $this->body = $body;
    }
}
