<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

class ExternalPaymentResult
{
    /**
     * @var bool
     */
    protected $isSuccess;

    /**
     * @var string
     */
    protected $redirectUrl;

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * @param bool $isSuccess
     */
    public function setIsSuccess($isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return $this->redirectUrl;
    }

    /**
     * @param string $redirectUrl
     */
    public function setRedirectUrl($redirectUrl)
    {
        $this->redirectUrl = $redirectUrl;
    }

    /**
     * @return bool
     */
    public function shouldRedirect()
    {
        return !empty($this->redirectUrl);
    }
}
