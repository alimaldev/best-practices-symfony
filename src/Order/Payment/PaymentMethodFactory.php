<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

use Linio\Frontend\Order\Payment\Method\CreditCard;
use Linio\Frontend\Order\Payment\Method\ExternalCheckout;
use Linio\Frontend\Order\Payment\Method\Regular;
use Linio\Frontend\Order\Payment\Method\ZeroPayment;

class PaymentMethodFactory
{
    protected static $classMap = [
        'CreditCard' => CreditCard::class,
        'PayClub_HostedPaymentPage' => ExternalCheckout::class,
        'Zero_Payment' => ZeroPayment::class,
    ];

    public static function fromName(string $name): PaymentMethod
    {
        if (!isset(static::$classMap[$name])) {
            return new Regular($name);
        }

        return new static::$classMap[$name]($name);
    }
}
