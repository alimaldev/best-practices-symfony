<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment\Method;

use Linio\Frontend\Order\Payment\PaymentMethod;

class Regular implements PaymentMethod
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $allowed = false;

    /**
     * @var bool
     */
    protected $allowedByCoupon = false;

    /**
     * @var bool
     */
    protected $requireBillingAddress = false;

    /**
     * @var array
     */
    protected $paymentData = [];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPaymentData(): array
    {
        return $this->paymentData;
    }

    public function setPaymentData(array $paymentData)
    {
        $this->paymentData = $paymentData;
    }

    public function allow()
    {
        $this->allowed = true;
    }

    public function isAllowed(): bool
    {
        return $this->allowed;
    }

    public function allowByCoupon()
    {
        $this->allowedByCoupon = true;
    }

    public function isAllowedByCoupon(): bool
    {
        return $this->allowedByCoupon;
    }

    public function requireBillingAddress()
    {
        $this->requireBillingAddress = true;
    }

    public function isBillingAddressRequired(): bool
    {
        return $this->requireBillingAddress;
    }

    public function isVisible(): bool
    {
        return true;
    }
}
