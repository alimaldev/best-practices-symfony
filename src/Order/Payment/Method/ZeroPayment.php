<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment\Method;

class ZeroPayment extends Regular
{
    public function isVisible(): bool
    {
        return false;
    }
}
