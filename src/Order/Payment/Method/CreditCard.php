<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment\Method;

use Linio\Frontend\Entity\Customer\CreditCard as SavedCreditCard;

class CreditCard extends Regular
{
    /**
     * @var string
     */
    protected $binNumber;

    /**
     * @var SavedCreditCard[]
     */
    protected $savedCards = [];

    public function getBinNumber()
    {
        return $this->binNumber;
    }

    public function setBinNumber(string $binNumber)
    {
        $this->binNumber = $binNumber;
    }

    public function clearBinNumber()
    {
        $this->binNumber = null;
    }

    public function getSavedCards()
    {
        return $this->savedCards;
    }

    public function setSavedCards(array $savedCards)
    {
        $this->savedCards = $savedCards;
    }
}
