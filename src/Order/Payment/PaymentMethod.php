<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Payment;

interface PaymentMethod
{
    public function __construct(string $name);
    public function getName(): string;
    public function getPaymentData(): array;
    public function setPaymentData(array $paymentData);
    public function isVisible(): bool;
    public function allow();
    public function isAllowed(): bool;
    public function allowByCoupon();
    public function isAllowedByCoupon(): bool;
    public function requireBillingAddress();
    public function isBillingAddressRequired(): bool;
}
