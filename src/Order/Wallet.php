<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Type\Money;

class Wallet
{
    /**
     * @var Money
     */
    protected $totalDiscount;

    /**
     * @var Money
     */
    protected $totalPointsUsed;

    /**
     * @var Money
     */
    protected $pointsBalance;

    /**
     * @var Money
     */
    protected $shippingDiscount;

    /**
     * @var Money
     */
    protected $pointsUsedForShipping;

    /**
     * @var Money
     */
    protected $maxPointsForOrder;

    /**
     * @var int
     */
    protected $conversionRate = 0;

    public function __construct()
    {
        $this->totalDiscount = new Money();
        $this->totalPointsUsed = new Money();
        $this->pointsBalance = new Money();
        $this->shippingDiscount = new Money();
        $this->pointsUsedForShipping = new Money();
        $this->maxPointsForOrder = new Money();
    }

    /**
     * @return Money
     */
    public function getTotalDiscount(): Money
    {
        return $this->totalDiscount;
    }

    /**
     * @param Money $totalDiscount
     */
    public function setTotalDiscount(Money $totalDiscount)
    {
        $this->totalDiscount = $totalDiscount;
    }

    /**
     * @return Money
     */
    public function getTotalPointsUsed(): Money
    {
        return $this->totalPointsUsed;
    }

    /**
     * @param Money $totalPointsUsed
     */
    public function setTotalPointsUsed(Money $totalPointsUsed)
    {
        $this->totalPointsUsed = $totalPointsUsed;
    }

    /**
     * @return Money
     */
    public function getPointsBalance(): Money
    {
        return $this->pointsBalance;
    }

    /**
     * @param Money $pointsBalance
     */
    public function setPointsBalance(Money $pointsBalance)
    {
        $this->pointsBalance = $pointsBalance;
    }

    /**
     * @return Money
     */
    public function getShippingDiscount(): Money
    {
        return $this->shippingDiscount;
    }

    /**
     * @param Money $shippingDiscount
     */
    public function setShippingDiscount(Money $shippingDiscount)
    {
        $this->shippingDiscount = $shippingDiscount;
    }

    /**
     * @return Money
     */
    public function getPointsUsedForShipping(): Money
    {
        return $this->pointsUsedForShipping;
    }

    /**
     * @param Money $pointsUsedForShipping
     */
    public function setPointsUsedForShipping(Money $pointsUsedForShipping)
    {
        $this->pointsUsedForShipping = $pointsUsedForShipping;
    }

    /**
     * @return Money
     */
    public function getMaxPointsForOrder(): Money
    {
        return $this->maxPointsForOrder;
    }

    /**
     * @param Money $maxPointsForOrder
     */
    public function setMaxPointsForOrder(Money $maxPointsForOrder)
    {
        $this->maxPointsForOrder = $maxPointsForOrder;
    }

    /**
     * @return int
     */
    public function getConversionRate(): int
    {
        return $this->conversionRate;
    }

    /**
     * @param int $conversionRate
     */
    public function setConversionRate(int $conversionRate)
    {
        $this->conversionRate = $conversionRate;
    }
}
