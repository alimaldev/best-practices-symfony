<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Storage;

use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Order;

interface Storage
{
    /**
     * @param string $orderId
     *
     * @throws OrderException
     *
     * @return Order
     */
    public function retrieve(string $orderId): Order;

    /**
     * @param Order $order
     */
    public function store(Order $order);

    /**
     * Clears the Order from storage.
     *
     * @param string $orderId
     */
    public function clear(string $orderId);
}
