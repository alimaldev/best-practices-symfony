<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Storage;

use Carbon\Carbon;
use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Customer;
use Linio\Frontend\Location\Address;
use Linio\Frontend\Order\Coupon;
use Linio\Frontend\Order\Exception\OrderException;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;

class Cache implements Storage
{
    use CacheAware;

    const ORDER_CACHE_KEY = 'order';

    /**
     * @param string $orderId
     *
     * @throws OrderException
     *
     * @return Order
     */
    public function retrieve(string $orderId): Order
    {
        $orderData = $this->cacheService->get($this->getOrderCacheKey($orderId));

        if (!$orderData) {
            throw new OrderException('Order does not exist in cache.');
        }

        return $this->transformFromCache($orderId, $orderData);
    }

    /**
     * @param Order $order
     */
    public function store(Order $order)
    {
        if ($order->getItems()->count() == 0) {
            $this->clear($order->getId());

            return;
        }

        $this->cacheService->set($this->getOrderCacheKey($order->getId()), $this->transformForCache($order));
    }

    /**
     * Clears the Order from storage.
     *
     * @param string $orderId
     */
    public function clear(string $orderId)
    {
        $this->cacheService->delete($this->getOrderCacheKey($orderId));
    }

    /**
     * @param string $orderId
     *
     * @return string
     */
    protected function getOrderCacheKey(string $orderId)
    {
        return sprintf('%s:%s', self::ORDER_CACHE_KEY, $orderId);
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    protected function transformForCache(Order $order): array
    {
        if ($order->getShippingAddress()) {
            $shippingAddress = [
                'postcode' => $order->getShippingAddress()->getPostcode() ?? null,
                'region' => $order->getShippingAddress()->getRegion() ?? null,
                'municipality' => $order->getShippingAddress()->getMunicipality() ?? null,
                'city' => $order->getShippingAddress()->getCity() ?? null,
            ];
        }

        $cache = [
            'coupon' => $order->getCoupon() ? $order->getCoupon()->getCode() : null,
            'customer' => $order->getCustomer() ? $order->getCustomer()->getId() : null,
            'shippingAddress' => $shippingAddress ?? null,
            'updatedAt' => $order->getUpdatedAt()->toDateTimeString(),
        ];

        /** @var Item $item */
        foreach ($order->getItems() as $item) {
            $cache['items'][$item->getSku()] = $item->getQuantity();
        }

        return $cache;
    }

    /**
     * @param string $orderId
     * @param array $orderData
     *
     * @return Order
     */
    protected function transformFromCache(string $orderId, array $orderData): Order
    {
        $order = new Order($orderId);

        if (!empty($orderData['coupon'])) {
            $order->applyCoupon(new Coupon($orderData['coupon']));
        }

        foreach ($orderData['items'] as $sku => $quantity) {
            $order->addItem(new Item($sku, $quantity));
        }

        if (!empty($orderData['shippingAddress'])) {
            $this->setShippingAddressInOrderFromData($order, $orderData);
        }

        $order->setUpdatedAt(new Carbon($orderData['updatedAt']));

        return $order;
    }

    /**
     * @param Order $order
     * @param array $orderData
     */
    protected function setShippingAddressInOrderFromData(Order $order, array $orderData)
    {
        $shippingAddress = new Address();

        if (isset($orderData['shippingAddress']['postcode'])) {
            $shippingAddress->setPostcode($orderData['shippingAddress']['postcode']);
        }

        if (isset($orderData['shippingAddress']['region'])) {
            $shippingAddress->setRegion($orderData['shippingAddress']['region']);
        }

        if (isset($orderData['shippingAddress']['municipality'])) {
            $shippingAddress->setMunicipality($orderData['shippingAddress']['municipality']);
        }

        if (isset($orderData['shippingAddress']['city'])) {
            $shippingAddress->setCity($orderData['shippingAddress']['city']);
        }

        $order->setShippingAddress($shippingAddress);
    }
}
