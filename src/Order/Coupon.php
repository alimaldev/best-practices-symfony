<?php

declare(strict_types=1);

namespace Linio\Frontend\Order;

use Linio\Type\Money;

class Coupon
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var Money
     */
    protected $discount;

    public function __construct(string $code)
    {
        $this->code = $code;
        $this->discount = new Money();
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function getDiscount(): Money
    {
        return $this->discount;
    }

    public function setDiscount(Money $discount)
    {
        $this->discount = $discount;
    }
}
