<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Shipping;

use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Items;

class Package
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var Items
     */
    protected $items;

    /**
     * @var ShippingQuotes
     */
    protected $shippingQuotes;

    public function __construct()
    {
        $this->items = new Items();
        $this->shippingQuotes = new ShippingQuotes();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Items
     */
    public function getItems(): Items
    {
        return $this->items;
    }

    /**
     * @param Items $items
     */
    public function setItems(Items $items)
    {
        $this->items = $items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->items[$item->getSku()] = $item;
    }

    /**
     * @return Item[]
     */
    public function getItemsGroupedBySku(): array
    {
        /** @var Item[] $groupedItems */
        $groupedItems = [];

        foreach ($this->items as $item) {
            if (!isset($groupedItems[$item->getSku()])) {
                $groupedItems[$item->getSku()] = clone $item;
                continue;
            }

            $groupedItem = $groupedItems[$item->getSku()];
            $groupedItem->setQuantity($groupedItem->getQuantity() + 1);
            $groupedItem->setShippingAmount($groupedItem->getShippingAmount()->add($item->getShippingAmount()));
        }

        return array_values($groupedItems);
    }

    /**
     * @return ShippingQuotes
     */
    public function getShippingQuotes(): ShippingQuotes
    {
        return $this->shippingQuotes;
    }

    /**
     * @param ShippingQuotes $shippingQuotes
     */
    public function setShippingQuotes(ShippingQuotes $shippingQuotes)
    {
        $this->shippingQuotes = $shippingQuotes;
    }

    /**
     * @return ShippingQuote|null
     */
    public function getSelectedShippingQuote()
    {
        return $this->shippingQuotes->getSelected();
    }
}
