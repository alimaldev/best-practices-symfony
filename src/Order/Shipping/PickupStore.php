<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Shipping;

use Linio\Frontend\Location\Address;

class PickupStore extends Address
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $geohash;

    /**
     * @var string
     */
    protected $subLocality;

    /**
     * @var string
     */
    protected $referencePoint;

    /**
     * @var int
     */
    protected $networkId;

    /**
     * @var string
     */
    protected $networkName;

    /**
     * @var bool
     */
    protected $selected = false;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getGeohash()
    {
        return $this->geohash;
    }

    /**
     * @param string $geohash
     */
    public function setGeohash(string $geohash)
    {
        $this->geohash = $geohash;
    }

    /**
     * @return string
     */
    public function getSubLocality()
    {
        return $this->subLocality;
    }

    /**
     * @param string $subLocality
     */
    public function setSubLocality(string $subLocality)
    {
        $this->subLocality = $subLocality;
    }

    /**
     * @return string
     */
    public function getReferencePoint()
    {
        return $this->referencePoint;
    }

    /**
     * @param string $referencePoint
     */
    public function setReferencePoint(string $referencePoint)
    {
        $this->referencePoint = $referencePoint;
    }

    /**
     * @return int
     */
    public function getNetworkId()
    {
        return $this->networkId;
    }

    /**
     * @param int $networkId
     */
    public function setNetworkId(int $networkId)
    {
        $this->networkId = $networkId;
    }

    /**
     * @return string
     */
    public function getNetworkName()
    {
        return $this->networkName;
    }

    /**
     * @param string $networkName
     */
    public function setNetworkName(string $networkName)
    {
        $this->networkName = $networkName;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->selected;
    }

    public function select()
    {
        $this->selected = true;
    }

    public function deselect()
    {
        $this->selected = false;
    }
}
