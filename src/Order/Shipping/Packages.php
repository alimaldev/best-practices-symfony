<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Shipping;

use Linio\Collection\TypedCollection;

class Packages extends TypedCollection
{
    /**
     * {@inheritdoc}
     */
    public function add($value)
    {
        $this->offsetSet($value->getId(), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof Package;
    }
}
