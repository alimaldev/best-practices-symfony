<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Shipping;

use InvalidArgumentException;
use Linio\Collection\TypedCollection;

class ShippingQuotes extends TypedCollection
{
    /**
     * @param int|string $shippingMethod
     */
    public function select(string $shippingMethod)
    {
        /** @var ShippingQuote $shipmentQuote */
        $shipmentQuote = $this->get($shippingMethod);

        if (!$shipmentQuote) {
            throw new InvalidArgumentException('Invalid shipment quote.');
        }

        $this->clearSelection();
        $shipmentQuote->select();
    }

    /**
     * @return ShippingQuote|null
     */
    public function getSelected()
    {
        /** @var ShippingQuote $shipmentQuote */
        foreach ($this as $shipmentQuote) {
            if ($shipmentQuote->isSelected()) {
                return $shipmentQuote;
            }
        }
    }

    public function clearSelection()
    {
        /** @var ShippingQuote $shipmentQuote */
        foreach ($this as $shipmentQuote) {
            $shipmentQuote->deselect();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function add($value)
    {
        $this->validateType($value);
        $this->set($value->getShippingMethod(), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function isValidType($value)
    {
        return $value instanceof ShippingQuote;
    }
}
