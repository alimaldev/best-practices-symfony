<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Shipping;

use InvalidArgumentException;
use Linio\Collection\TypedCollection;

class PickupStores extends TypedCollection
{
    /**
     * @param int $pickupStoreId
     */
    public function select(int $pickupStoreId)
    {
        /** @var PickupStore $pickupStore */
        $pickupStore = $this->get($pickupStoreId);

        if (!$pickupStore) {
            throw new InvalidArgumentException('Invalid pickup store.');
        }

        $this->clearSelection();
        $pickupStore->select();
    }

    /**
     * @return PickupStore|null
     */
    public function getSelected()
    {
        /** @var PickupStore $pickupStore */
        foreach ($this as $pickupStore) {
            if ($pickupStore->isSelected()) {
                return $pickupStore;
            }
        }
    }

    public function clearSelection()
    {
        /** @var PickupStore $pickupStore */
        foreach ($this as $pickupStore) {
            $pickupStore->deselect();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function add($value)
    {
        $this->validateType($value);
        $this->set($value->getId(), $value);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function isValidType($value)
    {
        return $value instanceof PickupStore;
    }
}
