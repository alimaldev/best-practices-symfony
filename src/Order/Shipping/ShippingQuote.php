<?php

declare(strict_types=1);

namespace Linio\Frontend\Order\Shipping;

use Carbon\Carbon;
use DateTime;
use Linio\Type\Money;

class ShippingQuote
{
    /**
     * This also acts as the ID.
     *
     * @var string
     */
    protected $shippingMethod;

    /**
     * @var Money
     */
    protected $fee;

    /**
     * @var DateTime
     */
    protected $estimatedDeliveryDate;

    /**
     * @var bool
     */
    protected $selected;

    /**
     * @var int Number of days before Christmas when guarantee starts
     */
    protected $christmasGuaranteeThreshold = 30;

    /**
     * @param string $shippingMethod
     * @param DateTime $estimatedDeliveryDate
     * @param Money $fee
     */
    public function __construct(string $shippingMethod, DateTime $estimatedDeliveryDate, Money $fee = null)
    {
        $this->shippingMethod = $shippingMethod;
        $this->fee = $fee ?? new Money();
        $this->estimatedDeliveryDate = $estimatedDeliveryDate;
        $this->selected = false;
    }

    /**
     * @return string
     */
    public function getShippingMethod(): string
    {
        return $this->shippingMethod;
    }

    /**
     * @param string $shippingMethod
     */
    public function setShippingMethod(string $shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * @return Money
     */
    public function getFee(): Money
    {
        return $this->fee;
    }

    /**
     * @param Money $fee
     */
    public function setFee(Money $fee)
    {
        $this->fee = $fee;
    }

    /**
     * @return DateTime
     */
    public function getEstimatedDeliveryDate(): DateTime
    {
        return $this->estimatedDeliveryDate;
    }

    /**
     * @param DateTime $estimatedDeliveryDate
     */
    public function setEstimatedDeliveryDate(DateTime $estimatedDeliveryDate)
    {
        $this->estimatedDeliveryDate = $estimatedDeliveryDate;
    }

    /**
     * @return bool
     */
    public function isDeliveredByChristmas(): bool
    {
        $estimatedDeliveryDay = Carbon::instance($this->estimatedDeliveryDate);
        $lastDeliveryDayBeforeChristmas = Carbon::createFromDate(null, 12, 23);
        $firstDayOfChristmasGuarantee = $lastDeliveryDayBeforeChristmas->copy()->subDays(
            $this->christmasGuaranteeThreshold
        );

        return $estimatedDeliveryDay->between($firstDayOfChristmasGuarantee, $lastDeliveryDayBeforeChristmas);
    }

    /**
     * @return bool
     */
    public function hasFreeShipping(): bool
    {
        return $this->fee->isZero();
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->selected;
    }

    public function select()
    {
        $this->selected = true;
    }

    public function deselect()
    {
        $this->selected = false;
    }
}
