<?php

declare(strict_types=1);

namespace Linio\Frontend\Request;

class PlatformInformation
{
    /**
     * @var string
     */
    protected $platform = 'desktop';

    /**
     * @var int
     */
    protected $storeId = 1;

    /**
     * @return string
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform(string $platform)
    {
        $this->platform = $platform;
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->storeId;
    }

    /**
     * @param int $storeId
     */
    public function setStoreId(int $storeId)
    {
        $this->storeId = $storeId;
    }
}
