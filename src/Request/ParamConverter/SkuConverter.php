<?php

namespace Linio\Frontend\Request\ParamConverter;

use Linio\Frontend\Product\Exception\ProductNotFoundException;
use Linio\Frontend\Product\ProductAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SkuConverter implements ParamConverterInterface
{
    use ProductAware;

    /**
     * @param Request        $request
     * @param ParamConverter $configuration
     *
     * @throws NotFoundHttpException
     *
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $sku = $request->get($configuration->getName());

        if (!$sku) {
            throw new NotFoundHttpException($request->getUri() . ' from ' . $request->headers->get('referer'));
        }

        try {
            $product = $this->productService->getBySku($sku);
        } catch (ProductNotFoundException $exception) {
            throw new NotFoundHttpException($request->getUri() . ' from ' . $request->headers->get('referer'));
        }

        $request->attributes->set($configuration->getName(), $product);

        return true;
    }

    /**
     * @codeCoverageIgnore
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @param ParamConverter $configuration
     *
     * @return bool
     */
    public function supports(ParamConverter $configuration)
    {
        return true;
    }
}
