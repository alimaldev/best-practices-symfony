<?php

declare(strict_types=1);

namespace Linio\Frontend\Request\ParamConverter;

use Linio\Frontend\SlugResolver\Exception\SlugResolverException;
use Linio\Frontend\SlugResolver\SlugResolverAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SlugConverter implements ParamConverterInterface
{
    use SlugResolverAware;

    /**
     * @param Request        $request
     * @param ParamConverter $configuration
     *
     * @throws NotFoundHttpException
     *
     * @return bool
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $slug = $request->attributes->get($configuration->getName());
        $notFoundHttpException = new NotFoundHttpException($request->getUri() . ' from ' . $request->headers->get('referer'));

        if (!$slug) {
            throw $notFoundHttpException;
        }

        if (is_array($slug)) {
            $slug = reset($slug);
        }

        $slug = rtrim($slug, '/');
        $resolverName = $configuration->getOptions()['resolver'];

        if (!$resolverName) {
            throw $notFoundHttpException;
        }

        try {
            $data = $this->slugResolverService->resolve($resolverName, $slug);
        } catch (SlugResolverException $exception) {
            throw $notFoundHttpException;
        }

        $request->attributes->set($configuration->getName(), $data);

        return true;
    }

    /**
     * @param ParamConverter $configuration
     *
     * @return bool
     */
    public function supports(ParamConverter $configuration)
    {
        return in_array(
            $configuration->getOptions()['resolver'],
            $this->slugResolverService->getAvailableResolvers()
        );
    }
}
