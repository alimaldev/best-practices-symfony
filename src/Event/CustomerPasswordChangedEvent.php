<?php

declare(strict_types=1);

namespace Linio\Frontend\Event;

use Linio\Frontend\Security\User;
use Symfony\Component\EventDispatcher\Event;

class CustomerPasswordChangedEvent extends Event
{
    const NAME = 'customer.password_changed';

    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
