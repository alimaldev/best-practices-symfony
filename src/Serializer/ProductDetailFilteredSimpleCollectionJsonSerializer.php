<?php

namespace Linio\Frontend\Serializer;

use Linio\Frontend\Entity\Product\Simple;
use Linio\Frontend\Transformer\ProductDetailFilteredSimpleTransformer as SimpleTransformer;

class ProductDetailFilteredSimpleCollectionJsonSerializer
{
    /**
     * @var SimpleTransformer
     */
    protected $transformer;

    public function __construct()
    {
        $this->transformer = new SimpleTransformer();
    }

    /**
     * @param Simple[]|Simple $simples
     *
     * @return string
     */
    public function serialize($simples)
    {
        if ($simples instanceof Simple) {
            return json_encode($this->transformer->transform($simples));
        }

        $filteredSimples = [];

        foreach ($simples as $simple) {
            if ($simple instanceof Simple) {
                $filteredSimples[$simple->getSku()] = $this->transformer->transform($simple);
            }
        }

        return json_encode($filteredSimples);
    }
}
