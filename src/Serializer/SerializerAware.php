<?php

namespace Linio\Frontend\Serializer;

use Symfony\Component\Serializer\SerializerInterface;

/**
 * @codeCoverageIgnore
 */
trait SerializerAware
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @return SerializerInterface
     */
    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
}
