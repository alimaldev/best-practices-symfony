<?php

namespace Linio\Frontend\Serializer;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @codeCoverageIgnore
 */
class JsonSerializer extends Serializer
{
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new GetSetMethodNormalizer();

        $normalizer->setCircularReferenceHandler(function ($entity) {
            return $entity;
        });

        return parent::__construct([$normalizer], [$encoder]);
    }
}
