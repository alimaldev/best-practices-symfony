<?php

namespace Linio\Frontend\Rating;

trait RatingAware
{
    /**
     * @var RatingService
     */
    protected $ratingService;

    /**
     * @codeCoverageIgnore
     *
     * @return RatingService
     */
    public function getRatingService()
    {
        return $this->ratingService;
    }

    /**
     * @codeCoverageIgnore
     *
     * @param RatingService $ratingService
     */
    public function setRatingService(RatingService $ratingService)
    {
        $this->ratingService = $ratingService;
    }
}
