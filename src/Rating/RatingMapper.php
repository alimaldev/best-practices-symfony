<?php

namespace Linio\Frontend\Rating;

use DateTime;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Product\Rating;
use Linio\Frontend\Entity\Product\Review;
use Linio\Frontend\Entity\Product\Review\Vote;
use Linio\Frontend\Entity\Product\Review\VoteSummary;

class RatingMapper implements MapperInterface
{
    use RatingAware;

    /**
     * @param array $data
     *
     * @return Rating
     */
    public function map(array $data)
    {
        $reviews = [];

        foreach ($data['reviews'] as $reviewData) {
            $review = new Review();
            $review->setTitle($reviewData['title']);
            $review->setComment($reviewData['detail']);
            $review->setCustomerName($reviewData['nickname']);
            $review->setCreatedAt(new DateTime($reviewData['created_at']));
            $review->setExperienceAverage($reviewData['experience_average']);
            $review->setConfirmedPurchase((bool) ($reviewData['confirmed_purchase'] ?? false));
            $review->setCustomerVerified($reviewData['is_customer'] ?? false);

            $voteSum = 0;
            foreach ($reviewData['votes'] as $voteData) {
                $voteSum += $voteData['option_value'];
            }

            $voteData = reset($reviewData['votes']);

            $voteAverage = intval(round($voteSum / count($reviewData['votes']), 0));

            $vote = new Vote();
            $vote->setType($voteData['type_code']);
            $vote->setTitle($voteData['type_title']);
            $vote->setValue($voteAverage);
            $review->addVote($vote);

            $reviews[] = $review;
        }

        $rating = new Rating();
        $rating->setReviews($reviews);
        $rating->setReviewsQuantity(count($reviews));

        $voteSummary = [
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
        ];
        foreach ($data['vote_summary'] as $summaryData) {
            $voteSummary[$summaryData['stars']]++;
        }

        $totalVotes = 0;
        $totalStars = 0;
        $starGroups = [];

        foreach ($voteSummary as $star => $votes) {
            $summary = new VoteSummary();
            $summary->setStars($star);
            $summary->setVotes($votes);
            $starGroups[] = $summary;

            $totalStars += $star * $votes;
            $totalVotes += $votes;
        }

        $rating->setAveragePoint(round(($totalStars / $totalVotes), 1));

        $rating->setVoteSummary($starGroups);

        return $rating;
    }
}
