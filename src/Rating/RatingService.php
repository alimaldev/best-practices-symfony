<?php

declare(strict_types=1);

namespace Linio\Frontend\Rating;

use Linio\Component\Cache\CacheAware;
use Linio\Frontend\Entity\Product\Rating;

class RatingService
{
    use CacheAware;

    const CACHE_PREFIX = 'ratings:';

    /**
     * @var RatingMapper
     */
    protected $ratingMapper;

    /**
     * @codeCoverageIgnore
     *
     * @param RatingMapper $ratingMapper
     */
    public function setRatingMapper(RatingMapper $ratingMapper)
    {
        $this->ratingMapper = $ratingMapper;
    }

    /**
     * @param string $sku
     *
     * @return Rating|false
     */
    public function getBySku(string $sku)
    {
        $rating = $this->cacheService->get(self::CACHE_PREFIX . $sku);

        if (!$rating) {
            return false;
        }

        return $this->ratingMapper->map($rating);
    }
}
