<?php

declare(strict_types=1);

namespace Linio\Frontend\Rating\Exception;

use Linio\Frontend\Exception\DomainException;

class RatingException extends DomainException
{
}
