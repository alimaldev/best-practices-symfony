<?php

declare(strict_types=1);

namespace Linio\Frontend\Store;

use Linio\Component\Cache\CacheService;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Yaml\Yaml;

class MobileConfigurationService
{
    /**
     * @var string
     */
    protected $countryConfigurationPath;

    /**
     * @var string
     */
    protected $paymentFormsPath;

    /**
     * @var CacheService
     */
    protected $cacheService;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var int
     */
    protected $mobileApiMinimumVersion;

    /**
     * @param string $countryConfigurationPath
     * @param string $paymentFormPath
     */
    public function __construct(string $countryConfigurationPath, string $paymentFormPath)
    {
        $this->countryConfigurationPath = $countryConfigurationPath;
        $this->paymentFormsPath = $paymentFormPath;
    }

    /**
     * @param CacheService $cacheService
     */
    public function setCacheService(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function setMobileApiMinimumVersion(int $mobileApiMinimumVersion)
    {
        $this->mobileApiMinimumVersion = $mobileApiMinimumVersion;
    }

    /**
     * @param string $countryCode
     *
     * @return array
     */
    public function getConfigParsedData(string $countryCode): array
    {
        $data = $this->cacheService->get('store');

        if (empty($data)) {
            $file = sprintf($this->countryConfigurationPath, $countryCode);
            $data = file_get_contents($file);
            $data = Yaml::parse($data);

            $paymentData = file_get_contents($this->paymentFormsPath);
            $paymentData = Yaml::parse($paymentData);

            $data['dynamic_form'] = array_replace_recursive($paymentData['dynamic_form'], $data['dynamic_form']);

            $minimumVersion = [
                'version' => $this->mobileApiMinimumVersion,
                'title' => $this->translator->trans('MOBILE_API_MINIMUM_VERSION_TITLE'),
                'message' => $this->translator->trans('MOBILE_API_MINIMUM_VERSION_MESSAGE'),
            ];

            $data['minimumVersionSupported'] = $minimumVersion;

            $this->cacheService->set('store', $data);
        }

        return $data;
    }
}
