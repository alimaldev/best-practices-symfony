<?php

namespace Linio\Frontend\Catalog;

use DateTime;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Catalog\Exception\CampaignSegmentNotFoundException;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Catalog\Campaign;

class CampaignService
{
    /**
     * @var CacheService
     */
    protected $cacheService;

    /**
     * @var MapperInterface
     */
    protected $campaignMapper;

    /**
     * @param MapperInterface $campaignMapper
     */
    public function setCampaignMapper(MapperInterface $campaignMapper)
    {
        $this->campaignMapper = $campaignMapper;
    }

    /**
     * @param CacheService $cacheService
     */
    public function setCacheService(CacheService $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * @param int $id
     * @param string $slug
     *
     * @throws CampaignSegmentNotFoundException
     *
     * @return Campaign
     */
    public function getCampaign($id, $slug)
    {
        $data = $this->cacheService->get($id);

        $currentDate = new DateTime();

        if (new DateTime($data['start_time']) > $currentDate || new DateTime($data['end_time']) < $currentDate) {
            throw new CampaignSegmentNotFoundException($slug);
        }

        $data['uri_slug'] = $slug;

        return $this->campaignMapper->map($data);
    }
}
