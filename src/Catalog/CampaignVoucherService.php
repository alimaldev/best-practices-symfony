<?php

namespace Linio\Frontend\Catalog;

use DateTime;
use Linio\Component\Cache\CacheService;
use Linio\Frontend\Entity\Catalog\CampaignVoucher;
use Linio\Frontend\Entity\Product;

class CampaignVoucherService
{
    /**
     * @var CacheService
     */
    protected $voucherCache;

    /**
     * @param CacheService $voucherCache
     */
    public function setVoucherCache(CacheService $voucherCache)
    {
        $this->voucherCache = $voucherCache;
    }

    /**
     * @param string $sku
     *
     * @return array
     */
    public function getVouchersBySku($sku)
    {
        $vouchers = $this->voucherCache->get('vouchers');

        if (empty($vouchers['products'][$sku])) {
            return [];
        }

        $voucherId = reset($vouchers['products'][$sku]);

        return $vouchers['vouchers'][$voucherId];
    }

    /**
     * @param Product $product
     */
    public function mapVouchersFromCampaign(Product $product)
    {
        /** @var Product\MarketplaceChild $marketplaceChild */
        foreach ($product->getMarketplaceChildren() as $marketplaceChild) {
            $cacheVoucher = $this->getVouchersBySku($marketplaceChild->getConfigSku());

            if (empty($cacheVoucher)) {
                continue;
            }

            $campaignVoucher = new CampaignVoucher();
            $campaignVoucher->setId($cacheVoucher['id']);
            $campaignVoucher->setName($cacheVoucher['name']);
            $campaignVoucher->setCode($cacheVoucher['code']);
            $campaignVoucher->setDiscount($cacheVoucher['discount']);
            $campaignVoucher->setFromDate(new DateTime($cacheVoucher['from_date']));
            $campaignVoucher->setToDate(new DateTime($cacheVoucher['to_date']));

            $currentDateTime = new DateTime();

            if ($currentDateTime >= $campaignVoucher->getFromDate() && $currentDateTime <= $campaignVoucher->getToDate()) {
                $marketplaceChild->setCampaignVoucher($campaignVoucher);
            }
        }
    }
}
