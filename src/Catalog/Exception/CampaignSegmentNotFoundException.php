<?php

declare(strict_types=1);

namespace Linio\Frontend\Catalog\Exception;

use Linio\Frontend\Exception\DomainException;

class CampaignSegmentNotFoundException extends DomainException
{
}
