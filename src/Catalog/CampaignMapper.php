<?php

declare(strict_types=1);

namespace Linio\Frontend\Catalog;

use Linio\Frontend\Catalog\Exception\CampaignSegmentNotFoundException;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Catalog\Campaign;

class CampaignMapper implements MapperInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function map(array $data)
    {
        return $this->createCampaign($data);
    }

    /**
     * @param array $data
     * @param Campaign $parent
     *
     * @throws CampaignSegmentNotFoundException
     *
     * @return Campaign
     */
    protected function createCampaign(array $data, Campaign $parent = null): Campaign
    {
        $campaign = new Campaign();
        $campaign->setId($data['id']);
        $campaign->setName($data['name']);
        $campaign->setSlug($data['slug']);
        $campaign->setSkus($data['skus']);
        $campaign->setCount(count($data['skus']));
        $campaign->setFilters($data['filters'] ?? []);

        if ($parent) {
            $campaign->setParent($parent);

            return $campaign;
        }

        $campaign->addPath($campaign);

        $this->buildSegments($campaign, $data['uri_slug'], $data['segments'] ?? []);

        return $campaign;
    }

    /**
     * @param Campaign $campaign
     * @param string $slug
     * @param array $segments
     *
     * @throws CampaignSegmentNotFoundException
     */
    protected function buildSegments(Campaign $campaign, string $slug, array $segments)
    {
        $parts = explode('/', $slug);

        $segmentSlug = null;

        if (count($parts) === 2) {
            $segmentSlug = $parts[1];
        } elseif (count($parts) > 2) {
            throw new CampaignSegmentNotFoundException($slug);
        } else {
            $campaign->setIsCurrent(true);
        }

        foreach ($segments as $segmentData) {
            $segment = $this->createCampaign($segmentData, $campaign);
            $campaign->addChild($segment);

            if ($segment->getSlug() === $segmentSlug) {
                $campaign->setCurrentSegment($segment);
                $campaign->setSkus($segment->getSkus());
                $campaign->addPath($segment);
                $segment->setIsCurrent(true);
            } elseif (!$segmentSlug) {
                $campaign->addSkus($segment->getSkus());
            }

            $campaign->setCount($campaign->getCount() + $segment->getCount());
        }
    }
}
