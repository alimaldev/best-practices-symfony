<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms;

use Carbon\Carbon;
use Linio\Component\Cache\CacheAware;
use Linio\Controller\RouterAware;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Cms\ContentType\MenuMapper as DesktopMenuMapper;
use Linio\Frontend\Cms\ContentType\Mobile\UrlMapper;
use Linio\Frontend\Cms\Exception\InvalidLinioUrlInContentException;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class CmsService
{
    const CONTENT_TYPE_BANNER = 'banner';
    const CONTENT_TYPE_BANNER_MENU = 'banner_menu';
    const CONTENT_TYPE_CAROUSEL = 'carousel';
    const CONTENT_TYPE_TEXT = 'simple_text';
    const CONTENT_TYPE_MARKDOWN = 'markdown_text';
    const CONTENT_TYPE_DFA = 'dfa';
    const CONTENT_TYPE_GRID = 'grid';
    const CONTENT_TYPE_HTML = 'html';
    const CONTENT_TYPE_MENU = 'menu';
    const CONTENT_TYPE_RECOMMENDATION = 'recommendations';
    const CONTENT_TYPE_SIMPLE_IMAGE = 'simple_image';
    const CONTENT_TYPE_CATEGORY_MENU = 'category_menu';
    const CONTENT_TYPE_PAGE_CONTENT = 'page_content';
    const CONTENT_TYPE_TOP_MENU = 'top_menu';
    const CONTENT_TYPE_CUSTOM = 'customized';
    const BANNER_TYPE_CATEGORY = 'category';
    const BANNER_TYPE_CUSTOMIZED = 'customized';
    const CATEGORY_MENU = 'category';
    const MENU_CUSTOMIZED = 'customized';
    const CATALOG_MAX_GRID_ROWS = 3;
    const CONTENT_MOBILE = 'mobile';
    const CONTENT_NAMESPACE_GLOBAL = 'global';
    const CONTENT_NAMESPACE_HOME = 'home';
    const CONTENT_NAMESPACE_CATEGORY = 'category';
    const CONTENT_TYPE_PLACEMENT = 'placement';

    use CategoryAware;
    use CacheAware;
    use RouterAware;

    /**
     * @var bool
     */
    protected $isDebugLogEnabled = false;

    /**
     * @var string
     */
    protected $debugLog = '';

    /**
     * @var MapperInterface
     */
    protected $menuMapper;

    /**
     * @var MapperInterface
     */
    protected $recommendationMapper;

    /**
     * @var DesktopMenuMapper
     */
    protected $desktopMenuMapper;

    /**
     * @var UrlMapper
     */
    protected $urlMapper;

    /**
     * @var string
     */
    protected $timezone;

    /**
     * @param MapperInterface $menuMapper
     */
    public function setMenuMapper(MapperInterface $menuMapper)
    {
        $this->menuMapper = $menuMapper;
    }

    /**
     * @param MapperInterface $recommendationMapper
     */
    public function setRecommendationMapper(MapperInterface $recommendationMapper)
    {
        $this->recommendationMapper = $recommendationMapper;
    }

    /**
     * @param DesktopMenuMapper $desktopMenuMapper
     */
    public function setDesktopMenuMapper(DesktopMenuMapper $desktopMenuMapper)
    {
        $this->desktopMenuMapper = $desktopMenuMapper;
    }

    /**
     * @param UrlMapper $mapper
     */
    public function setUrlMapper(UrlMapper $mapper)
    {
        $this->urlMapper = $mapper;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone(string $timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @param string $namespace
     * @param int $id
     * @param string $context
     *
     * @return array
     */
    public function simpleImage(string $namespace, int $id, string $context = 'desktop'): array
    {
        $imageKey = sprintf('%s:%s:%d', $context, $namespace, $id);

        $image = $this->getContentFromCache($imageKey);

        return $image ? $image : [];
    }

    /**
     * @param string $namespace
     * @param string $slug
     * @param string $context
     *
     * @return array
     */
    public function simpleText(string $namespace, string $slug, string $context = 'desktop'): array
    {
        $simpleTextKey = sprintf('%s:%s:%s', $context, $namespace, $slug);

        $simpleText = $this->getContentFromCache($simpleTextKey);

        return $simpleText ? $simpleText : [];
    }

    /**
     * @param string $namespace
     * @param int $id
     * @param string $context
     *
     * @return string
     */
    public function multilineText(string $namespace, int $id, string $context = 'desktop'): string
    {
        $multilineKey = sprintf('%s:multiline_text:%s:%d', $context, $namespace, $id);

        $multiline = $this->getContentFromCache($multilineKey);

        return $multiline ? $multiline : '';
    }

    /**
     * @param string $namespace
     * @param int $id
     * @param string $context
     *
     * @return array
     */
    public function banner(string $namespace, int $id, string $context = 'desktop'): array
    {
        $bannerKey = sprintf('%s:banner:%s:%d', $context, $namespace, $id);

        $banner = $this->getContentFromCache($bannerKey);

        return $banner ? $banner : [];
    }

    /**
     * @param string $placementKey
     *
     * @return array
     */
    public function placement(string $placementKey): array
    {
        /* TODO: We need to find a better way to treat the complete keys coming from cache. */
        $placementKey = str_replace($this->cacheService->getNamespace() . ':', '', $placementKey);
        $placements = $this->cacheService->get($placementKey) ?? [];

        $today = new Carbon(null, $this->timezone);

        foreach (array_reverse($placements) as &$placement) {
            $placement['start_date'] = new Carbon($placement['start_date'], $this->timezone);

            if ($today >= $placement['start_date']) {
                return $placement;
            }
        }

        return [];
    }

    /**
     * @param string $namespace
     * @param int $id
     * @param string $context
     *
     * @return array
     */
    public function dfa(string $namespace, int $id, string $context = 'desktop'): array
    {
        $dfaKey = sprintf('%s:dfa:%s:%d', $context, $namespace, $id);

        $dfa = $this->getContentFromCache($dfaKey);

        return $dfa ? $dfa : [];
    }

    /**
     * @param string $namespace
     * @param string $id
     * @param string $context
     *
     * @return array
     */
    public function markdownText(string $namespace, string $id, string $context = 'desktop'): array
    {
        $key = sprintf('%s:markdown_text:%s:%s', $context, $namespace, $id);

        return $this->getContentFromCache($key) ?: [];
    }

    /**
     * @param string $namespace
     * @param string $row
     * @param string $context
     *
     * @return array
     */
    public function menu(string $namespace, string $row, string $context = 'desktop'): array
    {
        $menuKey = sprintf('%s:menu:%s:%s', $context, $namespace, $row);

        $menu = $this->getContentFromCache($menuKey);

        return $menu = $menu ? $menu : [];
    }

    /**
     * @param string $namespace
     * @param int $row
     * @param string $context
     *
     * @return array
     */
    public function grid(string $namespace, int $row, string $context = 'desktop'): array
    {
        $gridKey = sprintf('%s:grid:%s:row-%d', $context, $namespace, $row);

        $grid = $this->getContentFromCache($gridKey);

        return empty($grid) ? [] : $grid;
    }

    /**
     * @param string $namespace
     * @param string  $slug
     * @param string $context
     * @param int $id
     *
     * @return array
     */
    public function carousel(string $namespace, string $slug, string $context = 'desktop', $id = 0): array
    {
        $key = '';

        switch ($namespace) {
            case self::CONTENT_NAMESPACE_GLOBAL:
                $key = sprintf('%s:carousel:%s:%s', $context, $namespace, $slug);
                break;
            case self::CONTENT_NAMESPACE_HOME:
            case self::CONTENT_NAMESPACE_CATEGORY:
                $key = sprintf('%s:carousel:%s:row-%d', $context, $namespace, $slug);
                break;
        }

        if ($id > 0) {
            $key = sprintf('%s:%d', $key, $id);
        }

        $carousel = $this->getContentFromCache($key);

        return empty($carousel) ? [] : $carousel;
    }

    /**
     * @param string $row
     * @param string $context
     *
     * @return array
     */
    public function mainMenuCustomGrid(string $row, string $context = 'desktop'): array
    {
        $grid = $this->getContentFromCache(
            sprintf('%s:main_menu_grid:home:row-%s', $context, $row)
        );

        return empty($grid) ? [] : $grid;
    }

    /**
     * @param int $row
     * @param string $context
     *
     * @return array
     */
    public function mainMenuCategoryGrid(int $row, string $context = 'desktop'): array
    {
        $grid = $this->getContentFromCache(
            sprintf('%s:main_menu_grid:category:%s', $context, $row)
        );

        return empty($grid) ? [] : $grid;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    private function getContentFromCache(string $key)
    {
        $content = $this->cacheService->get($key);

        if ($this->isDebugLogEnabled) {
            $this->debugLog .= "\n" . $key . ' = ' . var_export($content, true);
        }

        return $content;
    }

    public function enableDebugLog()
    {
        $this->isDebugLogEnabled = true;
    }

    /**
     * @param string $namespace
     * @param string $id
     * @param string $context
     *
     * @return array
     */
    public function getCatalogGrids(string $namespace, string $id, string $context = 'desktop'): array
    {
        $catalogGrids = [];

        for ($i = 1; $i <= self::CATALOG_MAX_GRID_ROWS; $i++) {
            $catalogGridKey = sprintf('%s:grid:%s:row-%s:%s', $context, $namespace, $i, $id);
            $catalogGrids[] = $this->getContentFromCache($catalogGridKey);
        }

        return $catalogGrids;
    }

    /**
     * @return string
     */
    public function getDebugLog(): string
    {
        return $this->debugLog;
    }

    /**
     * @param string $id
     * @param string $namespace
     * @param string $context
     *
     * @return array
     */
    public function staticPage(string $id, string $namespace = 'static_page', string $context = 'desktop'): array
    {
        $catalogPageKey = sprintf('%s:static_page:%s:%s', $context, $namespace, $id);

        return $this->getContentFromCache($catalogPageKey) ?? [];
    }

    /**
     * @param string $namespace
     * @param int $row
     *
     * @return array
     */
    public function getRecommendationsFromCache(string $namespace, int $row): array
    {
        $recommendationsKey = sprintf('desktop:recommendations:%s:row-%d', $namespace, $row);

        $content = $this->getContentFromCache($recommendationsKey);

        return empty($content) ? [] : $content;
    }

    /**
     * @param string $slug
     * @param string $context
     *
     * @return array
     */
    public function getGlobal(string $slug, string $context = 'desktop'): array
    {
        $global = $this->getContentFromCache(
            sprintf('%s:markdown_text:global:%s', $context, $slug)
        );

        return empty($global) ? [] : $global;
    }

    /**
     * @param string $context
     *
     * @return array
     */
    public function getMainMenuFromCache(string $context = 'desktop'): array
    {
        $mainMenu = $this->getContentFromCache($context . ':icons_menu:global');

        if (!empty($mainMenu)) {
            unset($mainMenu['revision_number']);
            ksort($mainMenu);

            return $mainMenu;
        }

        return [];
    }

    /**
     * @throws InvalidLinioUrlInContentException
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     *
     * @return array
     */
    public function buildMobileTopMenu(): array
    {
        $key = sprintf(
            '%s:%s:%s',
            self::CONTENT_MOBILE,
            self::CONTENT_TYPE_TOP_MENU,
            self::CONTENT_NAMESPACE_GLOBAL
        );

        $menu = $this->getContentFromCache($key) ?? [];

        $mappedTopMenu = [];

        if (isset($menu['revision_number'])) {
            unset($menu['revision_number']);
        }

        foreach ($menu as $item) {
            if (!is_array($item)) {
                continue;
            }

            $mappedTopMenu[] = $this->mapTopMenu($item);
        }

        return $mappedTopMenu;
    }

    /**
     * @param string $slug
     *
     * @return array
     */
    public function getUspMenuFromCache(string $slug): array
    {
        $menu = $this->getContentFromCache('desktop:usp_menu:global:' . $slug);

        return empty($menu) ? [] : $menu;
    }

    /**
     * @return array
     */
    public function getCategoryMenu(): array
    {
        $menuItems = $this->getContentFromCache(
            sprintf('%s:%s:global', self::CONTENT_MOBILE, self::CONTENT_TYPE_CATEGORY_MENU)
        );

        if (!isset($menuItems['revision_number'])) {
            unset($menuItems['revision_number']);
        }

        if (!$menuItems) {
            $menuItems = [];
        }

        ksort($menuItems);

        $categoryMenu = [];

        foreach ($menuItems as $item) {
            if (!is_array($item)) {
                continue;
            }

            $categoryMenu[] = $this->menuMapper->map($item);
        }

        return $categoryMenu;
    }

    /**
     * @return array
     */
    public function buildSiteDirectory()
    {
        $siteDirectory = [];

        $homeMenu = $this->getMainMenuFromCache();

        foreach ($homeMenu as $menuItem) {
            $entryDirectory = $this->mapSiteDirectoryEntry($menuItem);

            if (!empty($entryDirectory)) {
                $siteDirectory[] = $entryDirectory;
            }
        }

        return $siteDirectory;
    }

    /**
     * @param array $menu
     *
     * @return array
     */
    protected function mapSiteDirectoryEntry(array $menu)
    {
        switch ($menu['type']) {
            case self::CATEGORY_MENU:
                $grid = $this->mainMenuCategoryGrid($menu['attributes']['category_id']);
                break;
            case self::MENU_CUSTOMIZED:
                $grid = $this->mainMenuCustomGrid($menu['attributes']['main_menu_grid_slug']);
                break;
        }

        if (empty($grid['menu'])) {
            return [];
        }

        $directoryEntry = $this->desktopMenuMapper->map($menu);

        foreach ($grid['menu'] as $menus) {
            foreach ($menus as $menu) {
                $directoryEntry['menu'][] = $this->desktopMenuMapper->map($menu);
            }
        }

        return $directoryEntry;
    }

    /**
     * @param string $slug
     * @param string $namespace
     *
     * @return array
     */
    public function buildMobilePageContent(string $slug, string $namespace = self::CONTENT_NAMESPACE_GLOBAL): array
    {
        $key = sprintf(
            '%s:%s:%s:%s',
            self::CONTENT_MOBILE,
            self::CONTENT_TYPE_PAGE_CONTENT,
            $namespace,
            $slug
        );

        $pageContent = $this->cacheService->get($key) ?? ['page_content' => []];

        $builtPageContent = [];

        foreach ($pageContent['page_content'] as $content) {
            $builtPageContent[] = $this->mapMobileContent($content);
        }

        return $builtPageContent;
    }

    /**
     * @param array $content
     *
     * @return array
     */
    protected function mapMobileCarousel(array $content): array
    {
        $carousel = [
            'title' => $content['title'],
            'elements' => [],
        ];

        foreach ($content['elements'] as $element) {
            $carousel['elements'][] = $this->mapMobileContent($element);
        }

        return $carousel;
    }

    /**
     * @param array $item
     *
     * @throws InvalidLinioUrlInContentException
     *
     * @return array
     */
    protected function mapTopMenu(array $item): array
    {
        try {
            $mappedItem = $this->menuMapper->map($item);
        } catch (InvalidLinioUrlInContentException $exception) {
            $exception->addContext(['menuItem' => $item]);

            throw $exception;
        }

        $mappedItem['content'] = [];

        $pageContentKey = $item['attributes']['page_content_key'] ?? '';
        $pageContentKey = str_replace($this->cacheService->getNamespace() . ':', '', $pageContentKey);

        if (!empty($pageContentKey)) {
            $pageContent = $this->getContentFromCache($pageContentKey);

            foreach ($pageContent['page_content'] as $content) {
                $mappedItem['content'][] = $this->mapMobileContent($content);
            }
        }

        return $mappedItem;
    }

    /**
     * @param array $content
     *
     * @return array
     */
    protected function mapMobileGrid(array $content): array
    {
        $grid = [
            'title' => $content['title'],
            'cells' => [],
        ];

        foreach ($content['cells'] as $cell) {
            $grid['cells'][] = array_merge(
                $this->mapMobileContent($cell),
                ['cols' => $cell['cols'], 'rows' => $cell['rows']]
            );
        }

        return $grid;
    }

    /**
     * @param array $content
     *
     * @return array
     */
    protected function mapMobileMenu(array $content): array
    {
        $menu = [
            'items' => [],
        ];

        foreach ($content as $menuItem) {
            $menu['items'][] = $this->menuMapper->map($menuItem);
        }

        return $menu;
    }

    /**
     * @param array $content
     *
     * @return array
     */
    protected function mapMobileRecommendation(array $content): array
    {
        $recommendation = [
            'title' => $content['title'],
            'type' => $content['type'],
        ];

        $recommendation = array_merge($recommendation, $this->recommendationMapper->map($content));

        return $recommendation;
    }

    /**
     * @param array $content
     *
     * @return array
     */
    protected function mapMobileBanner(array $content): array
    {
        $urlSlugData = $this->urlMapper->map($content);

        return array_merge($content, $urlSlugData);
    }

    /**
     * @param array $content
     *
     * @return array
     */
    protected function mapMobileContent(array $content): array
    {
        switch ($content['content_type']) {
            case self::CONTENT_TYPE_BANNER:
                $mappedContent = $this->mapMobileBanner($content['data']);
                break;
            case self::CONTENT_TYPE_CAROUSEL:
                $mappedContent = $this->mapMobileCarousel($content['data']);
                break;
            case self::CONTENT_TYPE_GRID:
                $mappedContent = $this->mapMobileGrid($content['data']);
                break;
            case self::CONTENT_TYPE_MENU:
                $mappedContent = $this->mapMobileMenu($content['data']);
                break;
            case self::CONTENT_TYPE_RECOMMENDATION:
                $mappedContent = $this->mapMobileRecommendation($content['data']);
                break;
            case self::CONTENT_TYPE_HTML:
                $mappedContent = ['data' => $content['data']['text']];
                break;
            default:
                $mappedContent = [];
        }

        return array_merge($mappedContent, ['content_type' => $content['content_type']]);
    }
}
