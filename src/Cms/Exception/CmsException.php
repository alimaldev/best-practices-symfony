<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\Exception;

use Linio\Frontend\Exception\DomainException;

class CmsException extends DomainException
{
}
