<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\Exception;

class ContentNotFoundException extends CmsException
{
}
