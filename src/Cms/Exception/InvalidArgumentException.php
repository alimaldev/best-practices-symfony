<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\Exception;

class InvalidArgumentException extends CmsException
{
}
