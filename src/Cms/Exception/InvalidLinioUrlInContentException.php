<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\Exception;

use Linio\Frontend\Exception\ExceptionMessage;
use Throwable;

class InvalidLinioUrlInContentException extends CmsException
{
    /**
     * @param string|null $message
     * @param array $context
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = null, array $context = [], int $code = 0, Throwable $previous = null)
    {
        $message = $message ?? ExceptionMessage::CMS_INVALID_LINIO_URL_IN_CONTENT;

        parent::__construct($message, $context, $code, $previous);
    }
}
