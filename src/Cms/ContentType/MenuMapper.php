<?php

namespace Linio\Frontend\Cms\ContentType;

use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class MenuMapper
{
    const CATEGORY_MENU = 'category';

    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param CategoryService $categoryService
     * @param RouterInterface $router
     */
    public function __construct(CategoryService $categoryService, RouterInterface $router)
    {
        $this->categoryService = $categoryService;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function map(array $data)
    {
        $menu = [];

        $menu['type'] = $data['type'];
        $menu['label'] = $data['attributes']['label'];

        if ($data['type'] === self::CATEGORY_MENU) {
            $menu['href'] = $this->getMenuUrlFromCategoryId($data['attributes']['category_id']);
        } else {
            $menu['href'] = $data['attributes']['href'];
        }

        $menu['children'] = [];

        if (isset($data['children'])) {
            foreach ($data['children'] as $child) {
                $menu['children'][] = $this->map($child);
            }
        }

        return $menu;
    }

    /**
     * @param int $categoryId
     *
     * @return string
     */
    protected function getMenuUrlFromCategoryId($categoryId)
    {
        try {
            $category = $this->categoryService->getCategory($categoryId);
        } catch (CategoryNotFoundException $exception) {
            return $this->router->generate('frontend.default.index');
        }

        return $this->router->generate(
            'frontend.catalog.index.category', [
            'category' => $category->getSlug(),
        ]);
    }
}
