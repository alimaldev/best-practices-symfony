<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\ContentType\Mobile;

use Linio\Frontend\Category\Exception\CategoryNotFoundException;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Product\ProductImageTransformerAware;
use Linio\Frontend\Product\ProductService;
use Symfony\Component\Routing\RouterInterface;

class RecommendationMapper implements MapperInterface
{
    use ProductImageTransformerAware;

    const MANUAL = 'manual';
    const MANUAL_PRODUCT = 'sku';
    const MANUAL_CATEGORY = 'category';

    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * @var UrlMapper
     */
    protected $urlMapper;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @param ProductService $productService
     * @param UrlMapper $urlMapper
     * @param RouterInterface $router
     */
    public function __construct(ProductService $productService, UrlMapper $urlMapper, RouterInterface $router)
    {
        $this->productService = $productService;
        $this->urlMapper = $urlMapper;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function map(array $data): array
    {
        $recommendation = [];

        if ($data['type'] === self::MANUAL) {
            $rule = array_shift($data['rules']);

            switch ($rule['type']) {
                case self::MANUAL_PRODUCT:
                    $recommendation['type'] = 'product';
                    $recommendation['products'] = $this->mapManualRecommendation($rule['criteria']);
                    break;
                case self::MANUAL_CATEGORY:
                    try {
                        $categoryId = array_shift($rule['criteria']);
                        $categoryData = $this->urlMapper->map(['type' => self::MANUAL_CATEGORY, 'href' => $categoryId]);
                        $recommendation = array_merge($recommendation, $categoryData);
                    } catch (CategoryNotFoundException $exception) {
                        // Do nothing
                    }
                    break;
            }
        }

        return $recommendation;
    }

    /**
     * @param array $skus
     *
     * @return array
     */
    protected function mapManualRecommendation(array $skus): array
    {
        $products = $this->productService->getBySkus($skus);

        $mappedProducts = [];

        foreach ($products as $product) {
            $mappedProducts[] = $this->mapProduct($product);
        }

        return $mappedProducts;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function mapProduct(Product $product): array
    {
        $simpleData = $product->getFirstSimple();

        $productImage['url'] = '';

        if (!empty($product->getImages())) {
            $productImage = $this->transformMainImage($product->getImages());
        }

        $data = [
            'title' => $product->getName(),
            'brand' => $product->getBrand()->getName(),
            'originalPrice' => $simpleData->getOriginalPrice()->getMoneyAmount(),
            'percentageOff' => $simpleData->getPercentageOff(),
            'price' => $simpleData->getPrice()->getMoneyAmount(),
            'image' => $productImage['url'],
            'color' => null,
            'sku' => $product->getSku(),
        ];

        $url = $this->router->generate('frontend.catalog.detail', ['productSlug' => $product->getSlug()], RouterInterface::ABSOLUTE_URL);

        $urlData = $this->urlMapper->map(['type' => 'customized', 'href' => $url]);

        return array_merge($data, $urlData);
    }
}
