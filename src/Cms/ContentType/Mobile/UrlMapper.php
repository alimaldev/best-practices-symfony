<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\ContentType\Mobile;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Category\CategoryService;
use Linio\Frontend\Cms\Exception\CmsException;
use Linio\Frontend\Cms\Exception\InvalidLinioUrlInContentException;
use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;
use Linio\Frontend\Entity\Catalog\Recommendation\Product as RecommendationProduct;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Entity\Seller;
use Linio\Frontend\Exception\ExceptionMessage;
use Linio\Frontend\Product\ProductService;
use Linio\Frontend\SlugResolver\Adapter\BrandResolver;
use Linio\Frontend\SlugResolver\Exception\SlugNotFoundException;
use Linio\Frontend\SlugResolver\SlugResolverService;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RouterInterface;

class UrlMapper implements MapperInterface
{
    const DOMAIN_PATTERN = '/linio(-staging)?\./';

    const URL_TYPE_CATEGORY = 'category';
    const URL_TYPE_CUSTOM = 'customized';
    const URL_TYPE_BRAND = 'brand';
    const URL_TYPE_CAMPAIGN = 'campaign';
    const URL_TYPE_SEGMENT = 'segment';
    const URL_TYPE_SKU = 'sku';
    const URL_TYPE_SUPPLIER = 'supplier';

    const ROUTE_PRODUCT_DETAIL = 'detail';
    const ROUTE_SELLER = 'seller';
    const ROUTE_CATEGORY = 'category';
    const ROUTE_BRAND = 'brand';
    const ROUTE_CAMPAIGN = 'campaign';
    const ROUTE_CATEGORY_BRAND = 'category_brand';
    const ROUTE_CATEGORY_SELLER = 'category_seller';
    const ROUTE_SEARCH = 'search';

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * @var SlugResolverService
     */
    protected $slugResolver;

    /**
     * @var CacheService
     */
    protected $brandCacheService;

    /**
     * @var CacheService
     */
    protected $campaignCacheService;

    /**
     * @var CacheService
     */
    protected $segmentCacheService;

    /**
     * @var CacheService
     */
    protected $productCacheService;

    /**
     * @var CacheService
     */
    protected $sellerCacheService;

    /**
     * @param CacheService $sellerCacheService
     */
    public function setSellerCacheService(CacheService $sellerCacheService)
    {
        $this->sellerCacheService = $sellerCacheService;
    }

    /**
     * @param CacheService $productCacheService
     */
    public function setProductCacheService(CacheService $productCacheService)
    {
        $this->productCacheService = $productCacheService;
    }

    /**
     * @param CacheService $segmentCacheService
     */
    public function setSegmentCacheService(CacheService $segmentCacheService)
    {
        $this->segmentCacheService = $segmentCacheService;
    }

    /**
     * @param CacheService $campaignCacheService
     */
    public function setCampaignCacheService(CacheService $campaignCacheService)
    {
        $this->campaignCacheService = $campaignCacheService;
    }

    /**
     * @param CacheService $brandCacheService
     */
    public function setBrandCacheService(CacheService $brandCacheService)
    {
        $this->brandCacheService = $brandCacheService;
    }

    /**
     * @param RouterInterface $router
     * @param CategoryService $categoryService
     * @param SlugResolverService $slugResolver
     */
    public function __construct(
        RouterInterface $router,
        CategoryService $categoryService,
        SlugResolverService $slugResolver
    ) {
        $this->router = $router;
        $this->categoryService = $categoryService;
        $this->slugResolver = $slugResolver;
    }

    /**
     * Expects an array with the following data:
     * [
     *   'type' => 'category',
     *   'href' => 1234,
     * ]
     * and returns an array like this:
     * [
     *   'type' => 'category',
     *   'href' => 'http://linio.com.ec/c/test-category/test',
     *   'slugs' => [
     *     'category' => 'test-category/test',
     *   ],
     *   'ids' => [
     *     'category' => 1234,
     *   ]
     * ]
     * Unless the expected data is not there in which case it returns what was passed in.
     *
     * @param array $data
     *
     * @throws InvalidLinioUrlInContentException
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     *
     * @return array
     */
    public function map(array $data)
    {
        if (
            empty($data['type']) ||
            empty($data['href']) ||
            ((is_string($data['href']) && !$this->isValidLinioDomain($data) && $data['type'] != 'sku'))
        ) {
            return $data;
        }

        switch ($data['type']) {
            case self::URL_TYPE_CATEGORY:
                $urlSlugData = $this->getCategoryData((int) $data['href']);
                $urlSlugData['type'] = $data['type'];
                break;
            case self::URL_TYPE_CUSTOM:
                $urlSlugData = $this->getUrlData((string) $data['href']);
                break;
            case self::URL_TYPE_BRAND:
                $urlSlugData = $this->getBrandData((int) $data['href']);
                break;
            case self::URL_TYPE_CAMPAIGN:
                $urlSlugData = $this->getCampaignData((int) $data['href']);
                break;
            case self::URL_TYPE_SEGMENT:
                $urlSlugData = $this->getSegmentData((int) $data['href']);
                break;
            case self::URL_TYPE_SKU:
                $urlSlugData = $this->getProductData($data['href']);
                break;
            case self::URL_TYPE_SUPPLIER:
                $urlSlugData = $this->getSellerData((int) $data['href']);
                break;
            default:
                throw new CmsException(ExceptionMessage::CMS_URL_TYPE_NOT_SUPPORTED, ['type' => $data['type']]);
        }

        return $urlSlugData;
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    protected function isValidLinioDomain(array $data): bool
    {
        return preg_match(self::DOMAIN_PATTERN, $data['href']) === 1;
    }

    /**
     * @param int $id
     *
     * @throws RouteNotFoundException
     * @throws MissingMandatoryParametersException
     * @throws InvalidParameterException
     *
     * @return array
     */
    protected function getCategoryData(int $id): array
    {
        $category = $this->categoryService->getCategory($id);

        $slugMap = ['category' => $category->getSlug()];

        return [
            'href' => $this->router->generate('frontend.catalog.index.category', $slugMap),
            'slugs' => $slugMap,
            'urlKey' => $category->getUrlKey(),
            'ids' => [
                'category' => $id,
            ],
            'query' => null,
        ];
    }

    /**
     * @param string $url
     *
     * @throws InvalidLinioUrlInContentException
     *
     * @return array
     */
    protected function getUrlData(string $url): array
    {
        $parts = parse_url($url);
        $path = $parts['path'] ?? null;
        $query = null;

        if (empty($path) || $path === '/') {
            return [
                'type' => self::URL_TYPE_CUSTOM,
                'href' => $url,
                'slugs' => null,
                'ids' => null,
                'query' => $query,
            ];
        }

        try {
            $route = $this->router->match($path);
        } catch (ResourceNotFoundException $exception) {
            throw new InvalidLinioUrlInContentException(ExceptionMessage::CMS_INVALID_LINIO_URL_IN_CONTENT, ['url' => $url]);
        }

        $routeParts = explode('.', $route['_route']);
        $routeName = array_pop($routeParts);

        $slugMap = $routeName === self::ROUTE_SEARCH ? null : $this->getSlugsFromRoute($routeName, $route);
        $idMap = $routeName === self::ROUTE_SEARCH ? null : $this->getIdsFromSlugs($slugMap);

        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
        }

        return [
            'type' => $routeName === self::ROUTE_PRODUCT_DETAIL ? 'product' : $routeName,
            'href' => $path,
            'slugs' => $slugMap,
            'ids' => $idMap,
            'query' => $query,
        ];
    }

    /**
     * @param string $routeName
     * @param array $route
     *
     * @throws InvalidLinioUrlInContentException
     *
     * @return array
     */
    protected function getSlugsFromRoute(string $routeName, array $route): array
    {
        switch ($routeName) {
            case self::ROUTE_PRODUCT_DETAIL:
                $data = [
                    'product' => $route['productSlug'],
                ];
                break;
            case self::ROUTE_SELLER:
                $data = [
                    'seller' => $route['seller'],
                ];
                break;
            case self::ROUTE_CATEGORY:
                $data = [
                    'category' => $route['category'],
                ];
                break;
            case self::ROUTE_BRAND:
                $data = [
                    'brand' => $route['brandSlug'],
                ];
                break;
            case self::ROUTE_CAMPAIGN:
                $data = [
                    'campaign' => $route['campaign'],
                ];
                break;
            case self::ROUTE_CATEGORY_BRAND:
                $data = [
                    'category' => $route['category'],
                    'brand' => $route['brand'],
                ];
                break;
            case self::ROUTE_CATEGORY_SELLER:
                $data = [
                    'category' => $route['category'],
                    'seller' => $route['seller'],
                ];
                break;
            default:
                throw new InvalidLinioUrlInContentException(
                    sprintf('Route "%s" not supported.', $routeName),
                    ['route' => $route]
                );
        }

        return $data;
    }

    /**
     * @param array $slugs
     *
     * @return array
     */
    protected function getIdsFromSlugs(array $slugs): array
    {
        $data = [];

        foreach ($slugs as $resolverName => $slug) {
            try {
                $object = $this->slugResolver->resolve($resolverName, $slug);

                switch (true) {
                    case $object instanceof RecommendationProduct:
                    case $object instanceof Product:
                        $data[$resolverName] = $object->getSku();
                        break;
                    case $object instanceof Seller:
                        $data[$resolverName] = $object->getSellerId();
                        break;
                    default:
                        $data[$resolverName] = $object->getId();
                }
            } catch (SlugNotFoundException $exception) {
                $data[$resolverName] = null;
            }
        }

        return $data;
    }

    /**
     * @param int $id
     *
     * @return array
     */
    protected function getBrandData(int $id): array
    {
        $brand = $this->brandCacheService->get(sprintf('%s:%s', BrandResolver::BRAND_CACHE_KEY, $id));

        $slugMap = ['brandSlug' => $brand['slug']];

        return [
            'type' => self::URL_TYPE_BRAND,
            'href' => $this->router->generate('frontend.catalog.index.brand', $slugMap),
            'slugs' => [
                'brand' => $brand['slug'],
            ],
            'ids' => [
                'brand' => $id,
            ],
            'query' => null,
        ];
    }

    /**
     * @param int $id
     *
     * @return array
     */
    protected function getCampaignData(int $id): array
    {
        $campaign = $this->campaignCacheService->get((string) $id);

        $slugMap = ['campaign' => $campaign['slug']];

        return [
            'type' => self::URL_TYPE_CAMPAIGN,
            'href' => $this->router->generate('frontend.catalog.index.campaign', $slugMap),
            'slugs' => $slugMap,
            'ids' => [
                'campaign' => $id,
            ],
            'query' => null,
        ];
    }

    /**
     * @param int $segmentId
     *
     * @return array
     */
    protected function getSegmentData(int $segmentId): array
    {
        $segment = $this->segmentCacheService->get((string) $segmentId);
        $slugMap = ['campaign' => $segment['slug']];

        return [
            'type' => self::URL_TYPE_SEGMENT,
            'href' => $this->router->generate('frontend.catalog.index.campaign', $slugMap),
            'slugs' => $slugMap,
            'ids' => [
                'segment' => $segmentId,
            ],
            'query' => null,
        ];
    }

    /**
     * @param string $sku
     *
     * @return array
     */
    protected function getProductData(string $sku): array
    {
        $product = $this->productCacheService->get(sprintf('%s%s', ProductService::CACHE_PREFIX, $sku));

        $slugMap = ['productSlug' => $product['slug']];

        return [
            'type' => self::URL_TYPE_SKU,
            'href' => $this->router->generate('frontend.catalog.detail', $slugMap),
            'slugs' => [
                'product' => $product['slug'],
            ],
            'ids' => [
                'product' => $sku,
            ],
            'query' => null,
        ];
    }

    /**
     * @param int $id
     *
     * @return array
     */
    protected function getSellerData(int $id): array
    {
        $seller = $this->sellerCacheService->get(sprintf('%s:%s', 'data', $id));

        $slugMap = ['seller' => $seller['slug']];

        return [
            'type' => self::URL_TYPE_SUPPLIER,
            'href' => $this->router->generate('frontend.catalog.index.seller', $slugMap),
            'slugs' => $slugMap,
            'ids' => [
                'seller' => $id,
            ],
            'query' => null,
        ];
    }
}
