<?php

declare(strict_types=1);

namespace Linio\Frontend\Cms\ContentType\Mobile;

use Linio\Frontend\Communication\Customer\ResultSetMapper\MapperInterface;

class MenuMapper implements MapperInterface
{
    /**
     * @var UrlMapper
     */
    protected $urlMapper;

    /**
     * @param UrlMapper $urlMapper
     */
    public function __construct(UrlMapper $urlMapper)
    {
        $this->urlMapper = $urlMapper;
    }

    /**
     * {@inheritdoc}
     */
    public function map(array $data): array
    {
        $mappedMenu = [];

        $mappedMenu['type'] = $data['type'];
        $mappedMenu['label'] = $data['attributes']['label'];

        $mappedMenu['href'] = $data['attributes']['category_id'] ?? $data['attributes']['href'] ?? '';

        $mappedMenu = array_merge($mappedMenu, $this->urlMapper->map($mappedMenu));

        if (isset($data['attributes']['image'])) {
            $mappedMenu['image'] = $data['attributes']['image'];
        }

        $mappedMenu['children'] = [];

        if (isset($data['children'])) {
            foreach ($data['children'] as $child) {
                $mappedMenu['children'][] = $this->map($child);
            }
        }

        return $mappedMenu;
    }
}
