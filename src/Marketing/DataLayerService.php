<?php

namespace Linio\Frontend\Marketing;

use Linio\Controller\SessionAware;
use Linio\Frontend\Bundle\FormatterBundle\Formatter\MoneyFormatter;
use Linio\Frontend\Category\CategoryAware;
use Linio\Frontend\Entity\Brand;
use Linio\Frontend\Entity\Catalog\Category;
use Linio\Frontend\Entity\Catalog\SearchResult;
use Linio\Frontend\Entity\Product;
use Linio\Frontend\Order\BuildOrder;
use Linio\Frontend\Order\CompletedOrder;
use Linio\Frontend\Order\Item;
use Linio\Frontend\Order\Order;
use Linio\Frontend\Order\Shipping\Package;
use Linio\Frontend\Product\ProductAware;
use Linio\Frontend\Security\GuestUser;
use Linio\Frontend\Security\TokenStorageAware;
use Linio\Type\Dictionary;
use Symfony\Component\HttpFoundation\ParameterBag;

class DataLayerService
{
    use TokenStorageAware;
    use CategoryAware;
    use ProductAware;
    use SessionAware;

    /**
     * @var Dictionary
     */
    protected $storage;

    /**
     * @var MoneyFormatter
     */
    protected $moneyFormatter;

    /**
     * @var BuildOrder
     */
    protected $buildOrder;

    /**
     * @return array
     */
    public function getData()
    {
        return $this->storage->toArray();
    }

    /**
     * @param array $data
     */
    public function addData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->storage->set($key, $value);
        }
    }

    /**
     * @param MoneyFormatter $formatter
     */
    public function setMoneyFormatter(MoneyFormatter $formatter)
    {
        $this->moneyFormatter = $formatter;
    }

    /**
     * @param BuildOrder $buildOrder
     */
    public function setBuildOrder(BuildOrder $buildOrder)
    {
        $this->buildOrder = $buildOrder;
    }

    public function __construct()
    {
        $this->storage = new Dictionary();
    }

    public function populateHome()
    {
        $this->storage->clear();

        $homeVars = $this->getCommonVariables();
        $homeVars['new_user'] = $this->getUserType();
        $homeVars['pageType'] = 'home';
        $homeVars['mail_hash'] = $this->getHashedEmail();

        $this->addData($homeVars);
    }

    /**
     * @param Brand $brand
     */
    public function populateBrand(Brand $brand)
    {
        $this->storage->clear();

        $brandVars = $this->getCommonVariables();
        $brandVars['brand'] = $brand->getName();
        $brandVars['pageType'] = 'brand';
        $brandVars['mail_hash'] = $this->getHashedEmail();
        $brandVars['boost'] = $this->getBoostData($brand);

        $this->addData($brandVars);
    }

    /**
     * @param Category $category
     * @param SearchResult $searchResult
     */
    public function populateCategory(Category $category, SearchResult $searchResult)
    {
        $this->storage->clear();

        $categoryVars = array_merge($this->getCommonVariables(), $this->getCategoryData($category));

        $topProducts = array_slice($searchResult->getProducts(), 0, 3);

        $productIds = $this->getProductIds($topProducts);
        $products = $searchResult->getProducts();

        $categoryVars['c_plist'] = $productIds;
        $categoryVars['pageType'] = 'category';

        $eCommerce = ['impressions' => $this->setECommerceCatalogData($products, $category)];

        $categoryVars['ecommerce'] = $eCommerce;
        $categoryVars['mail_hash'] = $this->getHashedEmail();

        $this->addData($categoryVars);
    }

    /**
     * @param Category $category
     * @param SearchResult $searchResult
     * @param Brand $brand
     */
    public function populateCategoryBrand(Category $category, SearchResult $searchResult, Brand $brand)
    {
        $this->populateCategory($category, $searchResult);
        $this->storage->set('brand', $brand->getName());
        $this->storage->set('pageType', 'categoryBrand');
    }

    public function populateSearch()
    {
        $this->populateHome();
        $this->storage->set('pageType', 'search');
    }

    /**
     * @param Product $product
     */
    public function populateProduct(Product $product)
    {
        $this->storage->clear();

        $images = $product->getImages();
        $images = array_values($images);

        $largeImage = !empty($images) ? $images[0]->getSlug() . '-product.jpg' : '';
        $smallImage = count($images) > 1 ? $images[1]->getSlug() . '-product.jpg' : '';

        $productVars = array_merge($this->getCommonVariables(), $this->getProductdata($product));
        $productVars['large_image'] = $largeImage;
        $productVars['small_image'] = $smallImage;
        $productVars['new_user'] = $this->getUserType();
        $productVars['pageType'] = 'product';

        $eCommerce = ['detail' => ['products' => [$this->setECommerceProductDetailData($product)]]];

        $productVars['ecommerce'] = $eCommerce;

        $productVars['mail_hash'] = $this->getHashedEmail();

        $this->addData($productVars);
    }

    public function populateCart(Order $order)
    {
        $this->storage->clear();

        $cartVars = $this->getCommonVariables();
        $cartVars['cartTotalValue'] = $order->getGrandTotal()->getMoneyAmount();
        $cartVars['identifier_hash'] = $this->getIdentifierHash();
        $cartVars['mail_hash'] = $this->getHashedEmail();
        $cartVars['new_user'] = $this->getUserType();
        $cartVars['pageType'] = 'cart';
        $cartVars['cartProducts'] = [];

        foreach ($order->getItems() as $key => $item) {
            $cartVars['cartProducts'][] = $this->getItemData($item);
        }

        $cartVars['sku_simple_list'] = array_column($cartVars['cartProducts'], 'sku');
        $cartVars['product_id_list'] = array_column($cartVars['cartProducts'], 'product_id');

        $this->addData($cartVars);
    }

    public function populateCheckout(Order $order)
    {
        $this->storage->clear();

        $checkoutVars = $this->getCommonVariables();
        $checkoutVars['identifier_hash'] = $this->getIdentifierHash();
        $checkoutVars['mail_hash'] = $this->getHashedEmail();
        $checkoutVars['new_user'] = $this->getUserType();
        $checkoutVars['pageType'] = 'checkout';
        $checkoutVars['checkoutProducts'] = [];
        $checkoutVars['orderValue'] = $order->getGrandTotal()->getMoneyAmount();
        $checkoutVars['orderShippingValue'] = $order->getShippingAmount() ? $order->getShippingAmount()->getMoneyAmount() : 0;

        $orderValue = 0.0;

        foreach ($order->getItems() as $key => $item) {
            $checkoutVars['checkoutProducts'][] = $this->getItemData($item);

            $orderValue += $item->getPaidPrice()->getMoneyAmount() ?: $item->getUnitPrice()->getMoneyAmount() * $item->getQuantity();
        }

        $checkoutVars['sku_simple_list'] = array_column($checkoutVars['checkoutProducts'], 'sku');
        $checkoutVars['product_id_list'] = array_column($checkoutVars['checkoutProducts'], 'product_id');

        if ($order->getShippingAddress()) {
            $checkoutVars['orderDeliveryZipCode'] = $order->getShippingAddress()->getPostcode();
        }

        $checkoutVars['orderProductsValue'] = $orderValue;

        $this->addData($checkoutVars);
    }

    /**
     * @param CompletedOrder $completedOrder
     * @param ParameterBag $utmParams
     */
    public function populateSuccess(CompletedOrder $completedOrder, ParameterBag $utmParams = null)
    {
        $this->storage->clear();

        $checkoutVars = $this->getCommonVariables();
        $checkoutVars['identifier_hash'] = $this->getIdentifierHash();
        $checkoutVars['mail_hash'] = $this->getHashedEmail();
        $checkoutVars['new_user'] = $this->getUserType();
        $checkoutVars['pageType'] = 'success';
        $checkoutVars['transactionProducts'] = [];
        $checkoutVars['is_recurrent'] = 0; // Installment payments?
        $checkoutVars['order_number'] = $completedOrder->getOrderNumber();

        foreach ($completedOrder->getItems() as $key => $item) {
            $checkoutVars['transactionProducts'][] = $this->getItemData($item);
        }

        $checkoutVars['sku_simple_list'] = array_column($checkoutVars['transactionProducts'], 'sku');
        $checkoutVars['product_id_list'] = array_column($checkoutVars['transactionProducts'], 'product_id');

        if ($completedOrder->getShippingAddress()) {
            $checkoutVars['transactionDeliveryZipCode'] = $completedOrder->getShippingAddress()->getPostcode();
        }

        // SHIPPING METHODS - One per item
        $shippingMethods = [];

        /** @var Package $package */
        foreach ($completedOrder->getPackages() as $package) {
            $shippingQuote = $package->getShippingQuotes()->getSelected();
            $shippingMethods[] = $shippingQuote->getShippingMethod();
        }

        // TRANSACTION DATA
        $checkoutVars['transactionDate'] = date('d/m/Y H:i:s');
        $checkoutVars['transactionPaymentType'] = $completedOrder->getPaymentMethod();
        $checkoutVars['transactionShippingMethod'] = rtrim(implode(', ', $shippingMethods), ',\ ');
        $checkoutVars['transactionShippingValue'] = $completedOrder->getShippingAmount() ? $completedOrder->getShippingAmount()->getMoneyAmount() : 0;
        $checkoutVars['transactionCurrency'] = $this->moneyFormatter->getCurrencyCode();
        $checkoutVars['transactionId'] = $completedOrder->getOrderNumber();
        $checkoutVars['transactionAffiliation'] = 'linio';
        $checkoutVars['transactionTotal'] = $completedOrder->getGrandTotal()->getMoneyAmount();
        $checkoutVars['transactionShipping'] = $completedOrder->getShippingAmount() ? $completedOrder->getShippingAmount()->getMoneyAmount() : 0;
        $checkoutVars['transactionTax'] = $completedOrder->getTaxAmount()->getMoneyAmount();
        $checkoutVars['is_recurrent'] = $completedOrder->getCustomer()->isReturningCustomer() ? 2 : 1;

        $eCommerce = ['purchase' => $this->setECommerceCheckoutSuccessData($completedOrder, $completedOrder)];
        $checkoutVars['ecommerce'] = $eCommerce;

        // UTM DATA
        if (!is_null($utmParams)) {
            $utmValues = $this->getUtmValues($utmParams);

            $checkoutVars = array_merge($checkoutVars, $utmValues);
        }

        $this->addData($checkoutVars);
    }

    /**
     * @return array
     */
    protected function getCommonVariables()
    {
        return [
            'mobile_app_log' => 0,
            'is_linio_plus' => $this->getLinioPlus(),
            'gender' => $this->getUserGender(),
            'nls' => $this->session->get('data_layer/nls'),
        ];
    }

    /**
     * @return int
     */
    protected function getLinioPlus()
    {
        if ($this->getCustomer() instanceof GuestUser) {
            return 0;
        }

        return (int) $this->getCustomer()->isSubscribedToLinioPlus();
    }

    /**
     * @return string
     */
    protected function getUserGender()
    {
        $customer = $this->getCustomer();

        if (($customer instanceof GuestUser) || !$customer->getGender()) {
            return 'n/a';
        }

        return $customer->getGender();
    }

    /**
     * @return int
     */
    protected function getUserType()
    {
        $customer = $this->getCustomer();

        if ($customer instanceof GuestUser) {
            return 2;
        }

        return 1;
    }

    /**
     * @param Category $category
     *
     * @return array
     */
    protected function getCategoryData(Category $category)
    {
        $categoryData = [];

        $categoryTree = $this->categoryService->buildTree($category->getId());

        foreach ($categoryTree->getPath() as $key => $path) {
            $categoryData[sprintf('category%d', $key + 1)] = $path->getUrlKey();
        }

        $categoryData['category_id'] = $category->getId();
        $categoryData['category_full'] = $this->buildFullCategory($categoryTree);
        $categoryData['categoryKey1'] = $category->getSlug();
        $categoryData['categoryKey2'] = $categoryTree->getSlug();

        return $categoryData;
    }

    /**
     * @param Category $categoryTree
     *
     * @return string
     */
    protected function buildFullCategory(Category $categoryTree)
    {
        $categoryFull = '';

        foreach ($categoryTree->getPath() as $category) {
            $categoryFull .= $category->getUrlKey() . '/';
        }

        $categoryFull = rtrim($categoryFull, '/');

        return $categoryFull;
    }

    /**
     * @param Product[] $products
     *
     * @return array
     */
    protected function getProductIds(array $products)
    {
        $productIds = [];

        foreach ($products as $product) {
            $productIds[] = $product->getConfigId();
        }

        return $productIds;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function getProductData(Product $product)
    {
        $productData = [];

        if ($product->getCategory()) {
            $productData = array_merge($productData, $this->getCategoryData($product->getCategory()));
        }

        $simple = $product->getFirstSimple();
        $productData['sku_simple'] = $simple ? $simple->getSku() : $product->getSku();
        $productData['sku_config'] = $simple ? $simple->getConfigSku() : $product->getSku();

        $productData['price'] = $simple ? $simple->getOriginalPrice()->getMoneyAmount() : 0;
        $productData['msrpPrice'] = $simple ? $simple->getOriginalPrice()->getMoneyAmount() : 0;
        $productData['special_price'] = $simple ? $simple->getPrice()->getMoneyAmount() : 0;
        $productData['product_id'] = $product->getConfigId();
        $productData['product_name'] = $product->getName();
        $productData['is_international'] = (int) $product->isImported();
        $productData['ean_code'] = $product->getEanCode();
        $productData['is_master'] = (int) $this->isMasterSkuData($product);
        $productData['brand'] = $product->getBrand()->getName();

        return $productData;
    }

    /**
     * @return string
     */
    protected function getHashedEmail()
    {
        $customer = $this->getCustomer();

        if (($customer instanceof GuestUser) || !$customer->getEmail()) {
            return 'n/a';
        }

        return hash('sha256', $this->getCustomer()->getEmail());
    }

    /**
     * @return string
     */
    protected function getIdentifierHash()
    {
        $customer = $this->getCustomer();

        if (($customer instanceof GuestUser) || !$customer->getId()) {
            return 'n/a';
        }

        return hash('sha256', $this->getCustomer()->getId());
    }

    /**
     * @param ParameterBag $params
     *
     * @return array
     */
    protected function getUtmValues(ParameterBag $params)
    {
        $utmValues = [];

        if ($params->has('__utm_full')) {
            $utmParts = explode('&', $params->get('__utm_full'));

            foreach ($utmParts as $part) {
                list($utmKey, $value) = explode('=', $part);

                $utmValues[$utmKey] = $value;
            }
        }

        return $utmValues;
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    protected function getItemData(Item $item)
    {
        $itemData = [];

        if ($item->getProduct()->getCategory()) {
            $categoryData = $this->getCategoryData($item->getProduct()->getCategory());
            $categoryData['category'] = $categoryData['category_full'];

            unset($categoryData['category_full']);
            $itemData = array_merge($itemData, $categoryData);
        }

        $itemData['sku'] = $item->getSku();
        $itemData['price'] = $item->getOriginalPrice()->getMoneyAmount();
        $itemData['special_price'] = $item->getUnitPrice()->getMoneyAmount();
        $itemData['msrpPrice'] = $item->getOriginalPrice()->getMoneyAmount();
        $itemData['product_id'] = $item->getProduct()->getConfigId();
        $itemData['sku_config'] = explode('-', $item->getSku())[0];
        $itemData['name'] = $item->getProduct()->getName();
        $itemData['brand'] = $item->getProduct()->getBrand()->getName();
        $itemData['quantity'] = $item->getQuantity();
        $itemData['attrset'] = $item->getProduct()->getAttributeSet();
        $itemData['tax_cost'] = number_format($item->getTaxAmount()->getMoneyAmount(), 2);
        $itemData['ean_code'] = $item->getProduct()->getEanCode();
        $itemData['is_master'] = (int) $this->isMasterSkuData($item->getProduct());

        return $itemData;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    protected function setECommerceProductDetailData(Product $product)
    {
        $simple = $simple = $product->getFirstSimple();

        $data = [];
        $data['name'] = $product->getName();
        $data['id'] = $product->getSku();
        $data['price'] = $simple ? $simple->getOriginalPrice()->getMoneyAmount() : 0;
        $data['brand'] = $product->getBrand()->getName() ? $product->getBrand()->getName() : 'null';
        $data['category'] = $product->getCategory()->getName() ? $product->getCategory()->getName() : 'null';
        $data['variant'] = $product->getVariationType();

        return $data;
    }

    /**
     * @param Product[] $products
     * @param Category $category
     *
     * @return array
     */
    protected function setECommerceCatalogData($products, $category)
    {
        $filteredProducts = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $filteredProduct = [
                'name' => $product->getName(),
                'id' => $product->getSku(),
                'price' => $product->getPrice()->getMoneyAmount(),
                'brand' => $product->getBrand()->getName() ? $product->getBrand()->getName() : 'null',
                'category' => $category->getName() ?: 'null',
                'variant' => $product->getVariationType() ? $product->getVariationType() : 'null',
            ];

            array_push($filteredProducts, $filteredProduct);
        }

        return $filteredProducts;
    }

    /**
     * @param Order $order
     * @param CompletedOrder $completedOrder
     *
     * @return array
     */
    protected function setECommerceCheckoutSuccessData(Order $order, CompletedOrder $completedOrder)
    {
        $actionField = [
            'id' => $completedOrder->getOrderNumber(),
            'affiliation' => 'Online Store',
            'revenue' => $completedOrder->getGrandTotal()->getMoneyAmount(),
            'tax' => $completedOrder->getTaxAmount()->getMoneyAmount(),
            'shipping' => $completedOrder->getShippingAmount() ? $completedOrder->getShippingAmount()->getMoneyAmount() : 0,
        ];
        $products = [];

        /** @var Item $item */
        foreach ($order->getItems() as $item) {
            $product = [
                'name' => $item->getProduct()->getName(),
                'id' => $item->getSku(),
                'price' => $item->getUnitPrice()->getMoneyAmount(),
                'brand' => $item->getProduct()->getBrand()->getName(),
                'quantity' => $item->getQuantity(),
            ];
            $products[] = $product;
        }

        $data = [
            'actionField' => $actionField,
            'products' => $products,
        ];

        return $data;
    }

    /**
     * @param Brand $brand
     *
     * @return array
     */
    protected function getBoostData(Brand $brand)
    {
        $data = [
            'factor' => 5,
            'keywords' => [$brand->getName()],
        ];

        return $data;
    }

    /**
     * @param Product $product
     *
     * @return bool
     */
    protected function isMasterSkuData(Product $product)
    {
        if ($product->getMarketplaceParentSku() != null) {
            return true;
        }

        if (!empty($product->getMarketplaceChildren())) {
            return true;
        }

        return false;
    }
}
