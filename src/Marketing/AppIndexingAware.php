<?php

namespace Linio\Frontend\Marketing;

trait AppIndexingAware
{
    /**
     * @var AppIndexingService
     */
    protected $appIndexingService;

    /**
     * @return AppIndexingService
     */
    public function getAppIndexing()
    {
        return $this->appIndexingService;
    }

    /**
     * @param AppIndexingService $appIndexingService
     */
    public function setAppIndexing(AppIndexingService $appIndexingService)
    {
        $this->appIndexingService = $appIndexingService;
    }
}
