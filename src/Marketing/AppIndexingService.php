<?php

namespace Linio\Frontend\Marketing;

use Linio\Component\Cache\CacheService;
use Linio\Frontend\Communication\Country\CountryAware;

class AppIndexingService
{
    use CountryAware;

    const PREFIX_ANDROID = 'android-app';
    const PREFIX_IOS = 'ios-app';
    const TYPE_CATEGORY = 'c';
    const TYPE_PRODUCT = 'd';
    const ADJUST_CAMPAIGN = 'AppIndexing';

    /**
     * @var array
     */
    protected $androidData;

    /**
     * @var array
     */
    protected $iosData;

    /**
     * @param CacheService $cacheService
     */
    public function __construct(CacheService $cacheService)
    {
        $this->androidData['prefix'] = self::PREFIX_ANDROID;
        $this->iosData['prefix'] = self::PREFIX_IOS;

        $cache = $cacheService->get('mobile');

        if (!empty($cache['android_package_id'])) {
            $this->androidData['app_id'] = $cache['android_package_id'];
        }

        if (!empty($cache['android_adjust_tracker'])) {
            $this->androidData['app_tracker'] = $cache['android_adjust_tracker'];
        }

        if (!empty($cache['ios_itunes_id'])) {
            $this->iosData['app_id'] = $cache['ios_itunes_id'];
        }

        if (!empty($cache['ios_adjust_tracker'])) {
            $this->iosData['app_tracker'] = $cache['ios_adjust_tracker'];
        }
    }

    /**
     * @param string $sku
     *
     * @return array
     */
    public function getProductData($sku)
    {
        $data = ['type' => self::TYPE_PRODUCT, 'content' => $sku];

        return $this->buildHrefLinks($data);
    }

    /**
     * @param string $slug
     *
     * @return array
     */
    public function getCategoryData($slug)
    {
        $data = ['type' => self::TYPE_CATEGORY, 'content' => $slug];

        return $this->buildHrefLinks($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function buildHrefLinks(array $data)
    {
        $links['android'] = $this->getHref($data, $this->androidData);
        $links['ios'] = $this->getHref($data, $this->iosData);

        return $links;
    }

    /**
     * @param string $adjustTracker
     *
     * @return string
     */
    protected function getCommonParameters($adjustTracker)
    {
        $params = ['adjust_tracker' => $adjustTracker, 'adjust_campaign' => self::ADJUST_CAMPAIGN];

        return http_build_query($params, '', '&');
    }

    /**
     * @param array $data
     * @param array $appData
     *
     * @return string
     */
    protected function getHref(array $data, array $appData)
    {
        return sprintf('%s://%s/linio/%s/%s/%s?%s', $appData['prefix'], $appData['app_id'], $this->getCountryCode(), $data['type'], $data['content'], $this->getCommonParameters($appData['app_tracker']));
    }
}
