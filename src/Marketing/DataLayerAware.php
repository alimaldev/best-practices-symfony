<?php

namespace Linio\Frontend\Marketing;

trait DataLayerAware
{
    /**
     * @var DataLayerService
     */
    protected $dataLayerService;

    /**
     * @return DataLayerService
     */
    public function getDataLayer()
    {
        return $this->dataLayerService;
    }

    /**
     * @param DataLayerService $dataLayerService
     */
    public function setDataLayer(DataLayerService $dataLayerService)
    {
        $this->dataLayerService = $dataLayerService;
    }
}
